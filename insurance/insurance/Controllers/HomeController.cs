﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using static insurance.Models.Common.CommonModel;
using static insurance.Models.Custom.FrontAgencyModel;
using ecommerce.Models.Common;
using insurance.Helper.DataBase;
using insurance.Models.Common;
using insurance.Service;
using insurance.Service.CommonService;
using Post_Utility.Utility;

namespace insurance.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InsurenceDetails()
        {
            return View();
        }
        public ActionResult InsurenceSelectPlane()
        {
            return View();
        }
        public ActionResult Registration(string agencyid = null)
        {
            CreateAgency model = new CreateAgency();

            try
            {

                if (agencyid != null && !string.IsNullOrEmpty(agencyid.Trim()))
                {
                    model = FrontAgencyService.GetAgencyList(agencyid).FirstOrDefault();
                    model.RegUserId = model.UserId;
                    model.RegPassword = model.Password;
                    model.ConfPassword = model.Password;
                    model.CountryList = CommonClass.PopulateCountry(FrontAgencyService.CountryList(null, "1"));
                    model.StateList = CommonClass.PopulateState(FrontAgencyService.StateList(null, model.CountryID.ToString(), "1"));
                    model.CityList = CommonClass.PopulateCity(FrontAgencyService.CityList(null, null, model.StateID.ToString(), "1"));
                    model.RefByList = CommonClass.PopulateRefBy(FrontAgencyService.RefByList(null, "1"));
                    model.ConfPassword = model.Password;
                }
                else
                {
                    model.CountryList = CommonClass.PopulateCountry(FrontAgencyService.CountryList(null, "1"));
                    model.StateList = CommonClass.PopulateState(FrontAgencyService.StateList(null, null, "1"));
                    model.CityList = CommonClass.PopulateCity(FrontAgencyService.CityList(null, null, null, "1"));
                    model.RefByList = CommonClass.PopulateRefBy(FrontAgencyService.RefByList(null, "1"));
                    model.Status = true;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateAgency(int? agencyid, CreateAgency model, IEnumerable<HttpPostedFileBase> files)
        {
            bool isSuccess = false;
            int agencyID = 0;

            try
            {
                if (agencyid > 0)
                {
                    agencyID = agencyid.Value;
                    isSuccess = FrontAgencyService.UpdateAgencyDetail(agencyid, model);
                }
                else
                {
                    agencyID = FrontAgencyService.InserNewAgency(model);
                }

                //For image insert in folder
                if (agencyID > 0)
                {
                    CreateAgency tempModel = new CreateAgency();

                    Directory.CreateDirectory(Server.MapPath(UtilityClass.GetFullPathWithAppData("Agency", agencyID)));

                    int index = 0;

                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string ext = Path.GetExtension(file.FileName);
                            string filePath = Server.MapPath(UtilityClass.GetFullPathWithAppData("Agency", agencyID) + CommonClass.SetDocumentName(index, ext));

                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            file.SaveAs(filePath);

                            if (index == 0)
                            {
                                tempModel.BusinessAddressProofImg = CommonClass.SetDocumentName(index, ext);
                            }
                            else if (index == 1)
                            {
                                tempModel.BusinessPanCardImg = CommonClass.SetDocumentName(index, ext);
                            }
                            else if (index == 2)
                            {
                                tempModel.PersonalAddressProofImg = CommonClass.SetDocumentName(index, ext);
                            }
                            //else if (index == 3)
                            //{
                            //    tempModel.PersonalAddressProofImg2 = Common.SetDocumentName(index, ext);
                            //}
                            else if (index == 3)
                            {
                                tempModel.CompanyLogoImg = CommonClass.SetDocumentName(index, ext);
                            }
                        }

                        index++;
                    }

                    if (!string.IsNullOrEmpty(tempModel.BusinessAddressProofImg) || !string.IsNullOrEmpty(tempModel.BusinessPanCardImg) || !string.IsNullOrEmpty(tempModel.PersonalAddressProofImg) || !string.IsNullOrEmpty(tempModel.CompanyLogoImg))
                    {
                        isSuccess = FrontAgencyService.UpdateImagesAgency(agencyID, tempModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            TempData["Message"] = "success";
            return RedirectToAction("Registration", "Home");
        }
        #region [Json Section]
        public JsonResult GetGender()
        {
            string result = string.Empty;

            try
            {
                StringBuilder sbGender = new StringBuilder();
                List<Gender> genderList = CommonServe.GetGender(null, "true");

                sbGender.Append("<option value='0'>Gender</option>");
                if (genderList.Count > 0)
                {
                    foreach (var item in genderList)
                    {
                        sbGender.Append("<option value='" + item.LookupDataValueKey + "'>" + item.LookupDataCD + "</option>");
                    }

                    result = sbGender.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Json Section Age]
        public JsonResult GetAge()
        {
            string result = string.Empty;

            try
            {
                StringBuilder sbGender = new StringBuilder();
                List<Gender> genderList = CommonServe.GetGender(null, "true");

                sbGender.Append("<option value='0'>Gender</option>");
                if (genderList.Count > 0)
                {
                    foreach (var item in genderList)
                    {
                        sbGender.Append("<option value='" + item.LookupDataValueKey + "'>" + item.LookupDataCD + "</option>");
                    }

                    result = sbGender.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region[ASCIIEncoder]
        public ActionResult ASCIIEncoder()
        {
            List<string> catalogs = new List<string>();

            for (int i = 0; i < ConfigurationManager.ConnectionStrings.Count; i++)
            {
                catalogs.Add(ConfigurationManager.ConnectionStrings[i].ToString());
            }
            return View(catalogs);
        }
        public JsonResult GetCatalogProperties(string catalogVal, string catalogPara = null)
        {
            List<string> result = new List<string>();

            string cat = string.Empty;
            if (catalogVal == "seeinsuredapi_test")
            {
                cat = "api";
            }
            else
            {
                cat = (catalogVal == "seeinsuredmotor_test") ? "motor" : "seeinsured";
            }

            if (catalogPara == null || catalogPara == string.Empty)
            {
                DataTable dt = ConnectToDataBase.GetRecordFromTable("TABLE_NAME", "INFORMATION_SCHEMA.TABLES", "TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = '" + catalogVal + "'", cat);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        result.Add(dt.Rows[i]["TABLE_NAME"].ToString());
                    }

                }
            }
            else
            {
                DataTable dt = ConnectToDataBase.GetRecordFromTable("COLUMN_NAME", "INFORMATION_SCHEMA.COLUMNS", "TABLE_NAME = '" + catalogPara + "'", cat);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        result.Add(dt.Rows[i]["COLUMN_NAME"].ToString());
                    }

                }
            }


            return Json(result);
        }
        public JsonResult GetCatalogValues(string catalogVal, string catalogPara, string offset, string fetch, string orderPara, string orderBy)
        {
            string cat = string.Empty;
            if (catalogVal == "seeinsuredapi_test")
            {
                cat = "api";
            }
            else
            {
                cat = (catalogVal == "seeinsuredmotor_test") ? "motor" : "seeinsured";
            }

            offset = string.IsNullOrEmpty(offset) ? "0" : offset;
            fetch = string.IsNullOrEmpty(fetch) ? "10" : fetch;



            string tValue = catalogPara + " order by " + orderPara + " " + orderBy;
            tValue = tValue + " OFFSET " + offset + " ROWS";
            tValue = tValue + " FETCH NEXT " + fetch + " ROWS ONLY";

            DataTable dt = ConnectToDataBase.GetRecordFromTable("*", tValue , " ", cat);

            return Json(JsonConvert.SerializeObject(dt), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateCatalogValues(string tab, string set, string where, string cat)
        {
            string result = "Failed";

            if (cat == "seeinsuredapi_test")
            {
                cat = "api";
            }
            else
            {
                cat = (cat == "seeinsuredmotor_test") ? "motor" : "seeinsured";
            }

            bool isSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tab, set, where, cat);
            if (isSuccess)
            {
                result = "success";
            }
            return Json(result);
        }
        #endregion
    }
}