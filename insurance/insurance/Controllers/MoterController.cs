﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using ecommerce.Models.Common;
using insurance.Helper.DataBase;
using insurance.Models.Common;
using insurance.Models.Motor;
using insurance.Models.Motor.PrivateCarProposal;
using insurance.MotorApi;
using insurance.Service;
using insurance.Service.MotorService;
using Newtonsoft.Json;

namespace insurance.Controllers
{
    public class MoterController : Controller
    {
        // GET: Moter
        public ActionResult MoterSearch(string enquiry_id)
        {
            //MotorModel model = new MotorModel();
            //model.Enquiry_Id = enquiry_id;
            return View();
        }
        public ActionResult MoterRejistration()
        {
            MotorRTOModel model = new MotorRTOModel();
            model.MotorRTOList = CommonClass.PopulateRTO(Motor_Service.GetRTODetails());
            return View(model);
        }
        public ActionResult proposalsummary()
        {
            return View();
        }
        public ActionResult MoterPremiumSummary()
        {
            return View();
        }
        public ActionResult proposaldetails()
        {
            return View();
        }
        public ActionResult MoterSearchList(string enquiry_id)
        {
            return View();
        }
        public ActionResult MotorEnquiryDetails(string enquiry_id)
        {
            enquiry_id = Motor_Service.DecryptGenrateEnquiryId(enquiry_id);
            List<object> objDetail = Motor_Service.MotorEnquiryDetails(enquiry_id);

            string res = objDetail.ElementAt(2).GetType().GetProperty("response").GetValue(objDetail.ElementAt(2), null).ToString();
            MotorRoot quoteRespo = JsonConvert.DeserializeObject<MotorRoot>(res);
            objDetail.Add(quoteRespo);

            return View(objDetail);
        }

        [HttpPost]
        public ActionResult PaymentConfirmation(MotorPaymentSuccess psuccess)
        {
            try
            {
                //psuccess.EnquiryId = psuccess.enquiry = Motor_Service.DecryptGenrateEnquiryId(psuccess.enquiry);

                MotorModel enq = Motor_Service.GetEnquiryDetails(psuccess.enquiry);
                MotorProposal proposal = Motor_Service.GetMotorProposal(psuccess.enquiry);
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(psuccess.enquiry);
                string imgurl = Motor_Service.GetImageUrlByCHainId(enqRespo.chainid);

                if (psuccess.Status.ToLower() == "failure")
                {
                    psuccess.EnquiryId = psuccess.enquiry;
                    psuccess.TotalPremium = enqRespo.suminsured;
                    psuccess.CompanyImage = imgurl;
                    bool isupdated = Motor_Service.UpdatePaymentTransId(psuccess.EnquiryId, psuccess.PolicySysID, psuccess.PolicyNumber, psuccess.Status);
                }
                else
                {
                    psuccess.EnquiryId = psuccess.enquiry;
                    psuccess.TotalPremium = enqRespo.suminsured;
                    psuccess.CompanyImage = imgurl;
                    if (psuccess.PolicySysID != null)
                    {
                        if (Motor_Service.UpdatePaymentTransId(psuccess.EnquiryId, psuccess.PolicySysID, psuccess.PolicyNumber, psuccess.Status))
                        {
                            string linkUrl = "http://shriramgi.net.in/GenerateDigitalSign/CreateDS_PDF.aspx?PolSysId=" + psuccess.PolicySysID + "&LogoYN=Y";
                            var request = System.Net.WebRequest.Create(linkUrl);
                            request.Method = "GET";

                            using (var response = request.GetResponse())
                            {
                                using (var stream = response.GetResponseStream())
                                {
                                    string path = Server.MapPath("~/PDF/Motor/" + psuccess.EnquiryId + ".pdf");
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        stream.CopyTo(fileStream);
                                        bool ispdflink = Motor_Service.UpdatePolicyPdfLink(psuccess.EnquiryId, "PDF/Motor/" + psuccess.EnquiryId + ".pdf");
                                        if (ispdflink)
                                        {
                                            psuccess.PaymentLink = Motor_Service.GetPdfDocumentByEnqId(psuccess.EnquiryId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        psuccess.Status = "alreadysuccess";
                        psuccess.PolicySysID = enqRespo.transactionnumber;
                        psuccess.PolicyNumber = enqRespo.policynumber;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(psuccess);
        }
        //public ActionResult PaymentConfirmation(MotorPaymentSuccess psuccess, string enquiry)
        //{
        //    try
        //    {
        //        if (psuccess.Status.ToLower() == "failure")
        //        {
        //            List<string> enqAmout = Motor_Service.GetDetailsByTransId(psuccess.ProposalNumber, psuccess.ProposalSysID.Replace("_", "/"));
        //            if (enqAmout != null && enqAmout.Count > 0)
        //            {
        //                psuccess.EnquiryId = enqAmout[0];
        //                psuccess.TotalPremium = enqAmout[1];
        //                psuccess.CompanyImage = "http://motors.seeinsured.com" + enqAmout[2];
        //            }
        //        }
        //        else
        //        {
        //            List<string> enqAmout = Motor_Service.GetDetailsByTransId(psuccess.ProposalSysID.Replace("_", "/"), psuccess.ProposalNumber);
        //            if (enqAmout != null && enqAmout.Count > 0)
        //            {
        //                psuccess.EnquiryId = enqAmout[0];
        //                psuccess.TotalPremium = enqAmout[1];
        //                psuccess.CompanyImage = "http://motors.seeinsured.com" + enqAmout[2];
        //                if (Motor_Service.UpdatePaymentTransId(psuccess.EnquiryId, psuccess.PolicySysID, psuccess.PolicyNumber))
        //                {
        //                    string linkUrl = "http://shriramgi.net.in/GenerateDigitalSign/CreateDS_PDF.aspx?PolSysId=" + psuccess.PolicySysID + "&LogoYN=Y";
        //                    var request = System.Net.WebRequest.Create(linkUrl);
        //                    request.Method = "GET";

        //                    using (var response = request.GetResponse())
        //                    {
        //                        using (var stream = response.GetResponseStream())
        //                        {
        //                            string path = Server.MapPath("~/PDF/Motor/" + psuccess.EnquiryId + ".pdf");
        //                            using (var fileStream = System.IO.File.Create(path))
        //                            {
        //                                stream.CopyTo(fileStream);
        //                                bool ispdflink = Motor_Service.UpdatePolicyPdfLink(psuccess.EnquiryId, "PDF/Motor/" + psuccess.EnquiryId + ".pdf");
        //                                if (ispdflink)
        //                                {
        //                                    psuccess.PaymentLink = Motor_Service.GetPdfDocumentByEnqId(psuccess.EnquiryId);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //    return View(psuccess);
        //}

        [HttpGet]
        public ActionResult PaymentConfirmation(string transactionNumber, string enquiry, string ITGIResponse)
        {
            TempData["NotShowPdf"] = null;
            string paymentStatus = string.Empty; string policyNumber = string.Empty;
            if (!string.IsNullOrEmpty(transactionNumber))
            {
                paymentStatus = "SUCCESS";
            }
            else if (!string.IsNullOrEmpty(ITGIResponse))
            {
                TempData["NotShowPdf"] = "Yes";
                string[] paymentDetails = ITGIResponse.Split('|');
                transactionNumber = !string.IsNullOrEmpty(paymentDetails[1]) ? paymentDetails[1] : string.Empty;
                enquiry = paymentDetails[5];
                policyNumber = paymentDetails[2];
                if (paymentDetails[4].Contains("SUCCESS"))
                {
                    paymentStatus = "SUCCESS";
                }
                else
                {
                    paymentStatus = "FAILED";
                }
            }
            else
            {
                paymentStatus = "FAILED";
            }

            MotorPaymentSuccess psuccess = new MotorPaymentSuccess();
            psuccess.enquiry = enquiry;
            psuccess.EnquiryId = enquiry;
            //psuccess.EnquiryId = psuccess.enquiry = Motor_Service.DecryptGenrateEnquiryId(psuccess.enquiry);

            MotorModel enq = Motor_Service.GetEnquiryDetails(psuccess.enquiry);
            MotorProposal proposal = Motor_Service.GetMotorProposal(psuccess.enquiry);
            MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(psuccess.enquiry);
            string imgurl = Motor_Service.GetImageUrlByCHainId(enqRespo.chainid);

            psuccess.TotalPremium = enqRespo.suminsured;
            psuccess.CompanyImage = imgurl;
            psuccess.ProposalNumber = !string.IsNullOrEmpty(policyNumber) ? policyNumber : enqRespo.proposalnumber;

            if (!string.IsNullOrEmpty(transactionNumber))
            {
                if (!string.IsNullOrEmpty(enquiry) && !string.IsNullOrEmpty(transactionNumber))
                {
                    if (Motor_Service.UpdatePaymentTransId(enquiry, transactionNumber, psuccess.ProposalNumber, paymentStatus))
                    {
                        psuccess.Status = paymentStatus;
                        psuccess.PolicySysID = transactionNumber;
                        psuccess.PolicyNumber = psuccess.ProposalNumber;

                        GetPerposalPdf(enquiry);
                    }
                }
            }
            else
            {
                bool isupdated = Motor_Service.UpdatePaymentTransId(enquiry, "", "", paymentStatus);
            }

            return View(psuccess);
        }

        private void GetPerposalPdf(string enquiryId)
        {
            try
            {
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    string pdfurl = MotorAPIHelper.GetPerposalPdf(lu.UserId, lu.Password, enquiryId);
                    if (!string.IsNullOrEmpty(pdfurl))
                    {
                        SaveMotorPdfFile(enquiryId, pdfurl, "Motor");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        private void SaveMotorPdfFile(string enquiryid, string linkUrl, string folder)
        {
            try
            {
                if (!string.IsNullOrEmpty(linkUrl))
                {
                    var request = System.Net.WebRequest.Create(linkUrl);
                    request.Method = "GET";

                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            string path = Server.MapPath("~/PDF/" + folder + "/" + enquiryid + ".pdf");
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                                bool ispdflink = Motor_Service.UpdatePolicyPdfLink(enquiryid, "PDF/" + folder + "/" + enquiryid + ".pdf");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        #region [Json Section]
        public JsonResult GetVehicleDetail(string enqid)
        {
            enqid = Motor_Service.DecryptGenrateEnquiryId(enqid);
            return Json(Motor_Service.GetEnquiryDetails(enqid));
        }
        public JsonResult GetMotorRegDetails(string enqid)
        {
            List<object> response = new List<object>();
            enqid = Motor_Service.DecryptGenrateEnquiryId(enqid);

            MotorModel model = new MotorModel();
            model = Motor_Service.GetEnquiryDetails(enqid);
            if (model != null)
            {
                response = GetMotorApiRespoDetails(model);
            }
            return Json(response);
        }
        public List<object> GetMotorApiRespoDetails(MotorModel motorenq)
        {
            List<object> response = new List<object>();

            try
            {
                MotorRoot model = new MotorRoot();
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    StringBuilder sbLeftSec = new StringBuilder();
                    sbLeftSec.Append("<div class='row'>");
                    sbLeftSec.Append("<div class='col-sm-12 boxborder' style='background: #f3f7f9;'>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Policy For : <b><label id='lblPolicyFor'>" + motorenq.VechileType + "</label></b></p>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Insurance Type : <b><label id='lblPolicyFor'>" + motorenq.insurancetype + "</label></b></p>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Brand : <b><label id='lblPolicyFor'>" + motorenq.BrandName + "</label></b></p>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Model : <b><label id='lblPolicyFor'>" + motorenq.ModelName + "</label></b></p>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Fuel : <b><label id='lblPolicyFor'>" + motorenq.Fuel + "</label></b></p>");
                    sbLeftSec.Append("<p data-toggle='tooltip' title='Policy For' style='font-size: 13px;'>Variant : <b><label id='lblPolicyFor'>" + motorenq.VarientName + "</label></b></p>");
                    sbLeftSec.Append("<p style='font-size: 13px;'>Product Count  : <b><label id='lblProductCount' style='color:#e71820;'>0 Records</label></b></p>");
                    sbLeftSec.Append("</div>");
                    sbLeftSec.Append("</div>");

                    response.Add(sbLeftSec.ToString());

                    model = MotorAPIHelper.GetMotorApiResponse(lu.UserId, lu.Password, motorenq);
                    if (model != null && model.PrivateCarQuotesRS != null)
                    {
                        int loopcount = 1;
                        StringBuilder sbMotor = new StringBuilder();
                        StringBuilder sbMotorPopup = new StringBuilder();
                        foreach (var item in model.PrivateCarQuotesRS.Where(p => p.enquiryId != null).OrderBy(p => Convert.ToDecimal(p.ProductDetails.grossPremium)))
                        {
                            if (!string.IsNullOrEmpty(item.enquiryId))
                            {
                                if (Convert.ToDecimal(item.ProductDetails.grossPremium) > 0)
                                {
                                    sbMotor.Append("<div class='col-sm-12 listbox'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-2 borderright'>");
                                    sbMotor.Append("<img src='" + item.image + "' class='img-responsive img-center margin_top_15 company_logo' style='margin-top:0px;'>");
                                    sbMotor.Append("</div>");
                                    sbMotor.Append("<div class='col-sm-8'>");
                                    sbMotor.Append("<div class='row borderbotm'>");
                                    sbMotor.Append("<div class='col-sm-3 borderright wdth50'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-2'><p> <i class='fa fa-inr fonticon'></i></p></div>");
                                    sbMotor.Append("<div class='col-sm-10'><p class='fontdetails'>IDV</p>");
                                    if (item.vehicle != null)
                                    {
                                        if (item.vehicle.vehicleIDV != null)
                                        {
                                            sbMotor.Append("<p class='fontdetails'>" + CommonClass.IndianMoneyFormat(item.vehicle.vehicleIDV.defaultIdv.ToString()) + "</p>");
                                        }
                                    }
                                    sbMotor.Append("</div></div></div>");
                                    sbMotor.Append("<div class='col-sm-3 wdth50 borderright'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-2'><p> <i class='fa fa-gift  fonticon'></i></p></div>");
                                    sbMotor.Append("<div class='col-sm-10'><p class='fontdetails'>NCB</p>");
                                    bool isncb = false;
                                    if (item.discounts != null)
                                    {
                                        if (item.discounts.otherDiscounts != null && item.discounts.otherDiscounts.Count > 0)
                                        {
                                            foreach (var disoth in item.discounts.otherDiscounts)
                                            {
                                                if (disoth.discountType.Trim().ToLower() == "ncb_discount" || disoth.discountType.Trim().ToLower() == "no claim bonus")
                                                {
                                                    isncb = true;
                                                    sbMotor.Append("<p class='fontdetails'>" + disoth.discountPercent + " %</p>");
                                                }
                                            }
                                        }

                                    }
                                    if (!isncb)
                                    {
                                        sbMotor.Append("<p class='fontdetails'>0 %</p>");
                                    }
                                    sbMotor.Append("</div></div></div>");
                                    sbMotor.Append("<div class='col-sm-3 wdth50 borderright'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-5'><a><img src='/Content/images/ZeroDepreciation.jpeg' class='wdth50' style='margin-top: -6px;' /></a></div>");
                                    sbMotor.Append("<div class='col-sm-7'><p class='fontdetails'>Zero Dep</p><p class='fontdetails'>Available</p></div>");
                                    sbMotor.Append("</div></div>");
                                    sbMotor.Append("<div class='col-sm-3 wdth50 borderright'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-3'><a><img src='/Content/images/car-in-garage.png' class='width24' style='width:40px;' /></a></div>");
                                    sbMotor.Append("<div class='col-sm-9'><p class='fontdetails'>Cashless Garages</p><p class='fontdetails'>5000+</p></div>");
                                    sbMotor.Append("</div></div></div>");
                                    sbMotor.Append("<div class='row' style='margin-top:20px;'>");
                                    sbMotor.Append("<div class='col-sm-3'><a href='' class='seedetails' data-tabclickid='" + loopcount + "' data-toggle='modal' data-target='#productDetails_" + loopcount + "' style='font-size:17px;color:#e71820'><span class='fa fa-align-justify'></span>&nbsp; See Details</a></div>");
                                    sbMotor.Append("<div class='col-sm-4'>ProductName: " + item.ProductName + "</div>");
                                    sbMotor.Append("<div class='col-sm-5'>");
                                    sbMotor.Append("<h6 class='checkbox-inline checboxwidth'>");
                                    sbMotor.Append("<label class='seeingo chech' style='color: #777777 !important;'>Compare Product");
                                    sbMotor.Append("<input type='checkbox' class='submenuclass' id='commit' name='commit'><span class='checkmark'></span></label></h6>");
                                    sbMotor.Append("</div></div></div>");
                                    sbMotor.Append("<div class='col-sm-2'>");
                                    sbMotor.Append("<div class='row'>");
                                    sbMotor.Append("<div class='col-sm-12' style='margin-top:20px;text-align: center;'>");
                                    //sbMotor.Append("<p style='text-align: center;margin-top: 5px; color: #383838;margin-bottom: 5px;font-size: 14px;font-weight: bold;'>&nbsp;</p>");
                                    string price = (item.ProductDetails.grossPremium != null ? CommonClass.IndianMoneyFormat(item.ProductDetails.grossPremium.Replace("INR", "")) : "₹ 0.00");
                                    sbMotor.Append("<button class='default-btn bookbtn' id='btnMotorPerposal_" + loopcount + "' data-count='" + loopcount + "' data-chainid='" + item.Chainid + "' data-proposalno='" + item.ProposalNo + "' data-suminsured='" + item.ProductDetails.grossPremium + "' data-productcode='" + item.ProductDetails.insuranceProductCode + "' data-price='" + price + "' onclick='return ClickMotorPerposal(" + loopcount + ");'>" + price + "</button>");
                                    sbMotor.Append("<p style='text-align: center;font-size: 13px; margin-top:5px;color: #262566;'>Inclusive of GST</p>");
                                    sbMotor.Append("</div></div></div></div></div>");

                                    //see details section
                                    string thispopup = BindSeeDetails(item, loopcount);
                                    sbMotorPopup.Append(thispopup);
                                    loopcount = loopcount + 1;
                                }
                            }
                        }
                        response.Add(sbMotor.ToString() + sbMotorPopup.ToString());
                        response.Add((loopcount - 1).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }

        private string BindSeeDetails(PrivateCarQuotesR item, int loopcount)
        {
            StringBuilder sbSee = new StringBuilder();

            try
            {
                sbSee.Append("<div class='sidebar-modal'>");
                sbSee.Append("<div class='modal top fade' id='productDetails_" + loopcount + "'>");
                sbSee.Append("<div class='modal-dialog modal-lg' role='document' style='max-width: 1200px !important;'>");
                sbSee.Append("<div class='modal-content'>");

                sbSee.Append("<div class='modal-header'>");
                sbSee.Append("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'><i class='bx bx-x'></i></span></button>");
                sbSee.Append("<h2 class='modal-title' style='padding: 7px;background: #fff;border-bottom: 1px solid #ccc;font-size: 20px;text-align: center;'>Product Details</h2>");
                sbSee.Append("</div>");

                sbSee.Append("<div class='modal-body'>");
                sbSee.Append("<div class='col-sm-12 listbox listboxmobile' style='width: 97.5%; margin-left: 16px;'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-2 borderright'>");
                sbSee.Append("<img src='" + item.image + "' class='img-responsive img-center margin_top_15 company_logo' style='margin-top:0px;'>");
                sbSee.Append("</div>");

                sbSee.Append("<div class='col-sm-8'>");
                sbSee.Append("<div class='row borderbotm'>");
                sbSee.Append("<div class='col-sm-3 borderright wdth50'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-2'><p> <i class='fa fa-inr fonticon'></i></p></div>");
                sbSee.Append("<div class='col-sm-10'><p class='fontdetails'>IDV</p>");
                if (item.vehicle != null)
                {
                    if (item.vehicle.vehicleIDV != null)
                    {
                        sbSee.Append("<p class='fontdetails'>" + CommonClass.IndianMoneyFormat(item.vehicle.vehicleIDV.defaultIdv.ToString()) + "</p>");
                    }
                }
                sbSee.Append("</div></div></div>");
                sbSee.Append("<div class='col-sm-3  borderright wdth50'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-2'><p> <i class='fa fa-gift  fonticon'></i></p></div>");
                sbSee.Append("<div class='col-sm-10'><p class='fontdetails'>NCB</p>");
                bool isncb = false;
                if (item.discounts != null)
                {
                    if (item.discounts.otherDiscounts != null && item.discounts.otherDiscounts.Count > 0)
                    {
                        foreach (var disoth in item.discounts.otherDiscounts)
                        {
                            if (disoth.discountType.Trim().ToLower() == "ncb_discount")
                            {
                                isncb = true;
                                sbSee.Append("<p class='fontdetails'>" + disoth.discountPercent + " %</p>");
                            }
                        }
                    }

                }
                if (!isncb)
                {
                    sbSee.Append("<p class='fontdetails'>0 %</p>");
                }
                sbSee.Append("</div></div></div>");
                sbSee.Append("<div class='col-sm-3  borderright wdth50'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-5'><a><img src='/Content/images/ZeroDepreciation.jpeg' class='wdth50' style='margin-top: -6px;'></a></div>");
                sbSee.Append("<div class='col-sm-7'><p class='fontdetails'>Zero Dep</p><p class='fontdetails'>Available</p></div>");
                sbSee.Append("</div></div>");
                sbSee.Append("<div class='col-sm-3  borderright wdth50'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-3'><a><img src='/Content/images/car-in-garage.png' class='width24' style='width:40px;'></a></div>");
                sbSee.Append("<div class='col-sm-9'><p class='fontdetails'>Cashless Garages</p><p class='fontdetails'>5000+</p></div>");
                sbSee.Append("</div></div>");
                sbSee.Append("</div>");

                sbSee.Append("<div class='row' style='margin-top: 10px;'>");
                sbSee.Append("<div class='col-sm-6'><div class='row'><h5 style='padding-left: 10px;'>" + item.ProductName + "</h5></div></div>");

                string b_link = Motor_Service.GetMotor_BrochureLink(item.ProductType, item.PolicyType, item.ProductName, item.ProductDetails.insuranceProductCode, item.ProductDetails.subInsuranceProductCode, "b");
                if (!string.IsNullOrEmpty(b_link))
                {
                    sbSee.Append("<div class='col-sm-3'><div class='row'><h5 style='padding-left: 10px;'><a href='" + b_link + "' target='_blank' class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Brochure</a></h5></div></div>");
                }
                else
                {
                    sbSee.Append("<div class='col-sm-3'><div class='row'><h5 style='padding-left: 10px;'><span class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Brochure</span></h5></div></div>");
                }
                string c_link = Motor_Service.GetMotor_BrochureLink(item.ProductType, item.PolicyType, item.ProductName, item.ProductDetails.insuranceProductCode, item.ProductDetails.subInsuranceProductCode, "c");
                if (!string.IsNullOrEmpty(c_link))
                {
                    sbSee.Append("<div class='col-sm-3'><div class='row'><h5 style='padding-left: 10px;'><a href='" + c_link + "' target='_blank' class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Clauses</a></h5></div></div>");
                }
                else
                {
                    sbSee.Append("<div class='col-sm-3'><div class='row'><h5 style='padding-left: 10px;'><span class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Clauses</a></h5></div></div>");
                }

                sbSee.Append("</div>");

                sbSee.Append("</div>");
                sbSee.Append("<div class='col-sm-2'>");
                sbSee.Append("<div class='row'>");
                sbSee.Append("<div class='col-sm-12' style='margin-top:20px;'>");
                string price = (item.ProductDetails.grossPremium != null ? CommonClass.IndianMoneyFormat(item.ProductDetails.grossPremium.Replace("INR", "")) : "₹ 0.00");
                //sbSee.Append("<button class='default-btn bookbtn' id='btnMotorPerposal_2' data-chainid='2' data-proposalno='10064/31/21/P/025023' data-suminsured='14378.00' data-price='₹ 14,378.00' onclick='return ClickMotorPerposal(2);'>₹ 14,378.00</button><p style='text-align: center;font-size: 13px; margin-top:5px;color: #262566;'>Inclusive of GST</p>");
                sbSee.Append("<button class='default-btn bookbtn bookbtnpopup_" + loopcount + "' id='btnMotorPerposal_" + loopcount + "' data-count='" + loopcount + "' data-chainid='" + item.Chainid + "' data-proposalno='" + item.ProposalNo + "' data-suminsured='" + item.ProductDetails.grossPremium + "' data-productcode='" + item.ProductDetails.insuranceProductCode + "' data-price='" + price + "' onclick='return ClickMotorPerposal(" + loopcount + ");'>" + price + "</button><p style='text-align: center;font-size: 13px; margin-top:5px;color: #262566;'>Inclusive of GST</p>");
                sbSee.Append("</div>");
                sbSee.Append("</div></div>");

                sbSee.Append("</div></div>");

                if (item.Benefit != null)
                {
                    sbSee.Append("<div class='col-sm-12 form-group policycover'>");
                    sbSee.Append("<div class='tab'>");
                    foreach (var bnft in item.Benefit.categories)
                    {
                        string catname = bnft.categoryName;
                        sbSee.Append("<button class='tablinks btnmotorseedelt active " + catname.Replace(" ", "_") + "_" + loopcount + "' data-thiscount='" + loopcount + "' data-thisevent='event' data-tabname='" + catname.Replace(" ", "_") + "_" + loopcount + "''>" + catname + "</button>");
                    }
                    sbSee.Append("</div>");

                    Category policycover = item.Benefit.categories.Where(p => p.categoryName.ToLower().Trim().Contains("policy cover")).FirstOrDefault();
                    sbSee.Append("<div id='" + policycover.categoryName.Replace(" ", "_") + "_" + loopcount + "' class='tabcontent' style='display:block;'>");
                    sbSee.Append("<div class='row'>");
                    sbSee.Append("<div class='col-sm-12'>");
                    if (policycover.menus != null && policycover.menus.Count > 0)
                    {
                        foreach (var menus in policycover.menus)
                        {
                            sbSee.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + menus.menuName + " - " + menus.destriptions + "");
                            // sbSee.Append(!string.IsNullOrEmpty(menus.destriptions) ? "<br/><span style='margin-left: 25px;'>(" + menus.destriptions + ")</span></p>" : "</p>");
                        }
                    }
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");

                    Category policydoesnotcover = item.Benefit.categories.Where(p => p.categoryName.ToLower().Trim().Contains("policy does not cover")).FirstOrDefault();
                    sbSee.Append("<div id='" + policydoesnotcover.categoryName.Replace(" ", "_") + "_" + loopcount + "' class='tabcontent'>");
                    sbSee.Append("<div class='row'>");
                    sbSee.Append("<div class='col-sm-12'>");
                    if (policydoesnotcover.menus != null && policydoesnotcover.menus.Count > 0)
                    {
                        foreach (var menus in policydoesnotcover.menus)
                        {
                            sbSee.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + menus.menuName + " - " + menus.destriptions + "");
                            //sbSee.Append(!string.IsNullOrEmpty(menus.destriptions) ? "<br/><span style='margin-left: 25px;'>(" + menus.destriptions + ")</span></p>" : "</p>");
                        }
                    }
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");

                    Category policyaddon = item.Benefit.categories.Where(p => p.categoryName.ToLower().Trim().Contains("add-on")).FirstOrDefault();
                    sbSee.Append("<div id='" + policyaddon.categoryName.Replace(" ", "_") + "_" + loopcount + "' class='tabcontent'>");
                    sbSee.Append("<div class='row'>");
                    sbSee.Append("<div class='col-sm-12'>");
                    if (policyaddon.menus != null && policyaddon.menus.Count > 0)
                    {
                        foreach (var menus in policyaddon.menus)
                        {
                            sbSee.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + menus.menuName + " - " + menus.destriptions + "");
                            //sbSee.Append(!string.IsNullOrEmpty(menus.destriptions) ? "<br/><span style='margin-left: 25px;'>(" + menus.destriptions + ")</span></p>" : "</p>");
                        }
                    }
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");

                    Category keyfeaturesofinsurer = item.Benefit.categories.Where(p => p.categoryName.ToLower().Trim().Contains("key features of insurer")).FirstOrDefault();
                    sbSee.Append("<div id='" + keyfeaturesofinsurer.categoryName.Replace(" ", "_") + "_" + loopcount + "' class='tabcontent'>");
                    sbSee.Append("<div class='row'>");
                    sbSee.Append("<div class='col-sm-12'>");
                    if (keyfeaturesofinsurer.menus != null && keyfeaturesofinsurer.menus.Count > 0)
                    {
                        foreach (var menus in keyfeaturesofinsurer.menus)
                        {
                            sbSee.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + menus.menuName + " - " + menus.destriptions + "");
                            //sbSee.Append(!string.IsNullOrEmpty(menus.destriptions) ? "<br/><span style='margin-left: 25px;'>(" + menus.destriptions + ")</span></p>" : "</p>");
                        }
                    }
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");
                    sbSee.Append("</div>");

                    sbSee.Append("</div>");
                }
                sbSee.Append("</div>");

                sbSee.Append("</div>");
                sbSee.Append("</div>");
                sbSee.Append("</div>");
                sbSee.Append("</div>");
                //sbSee.Append("</div>");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return sbSee.ToString();
        }
        private string SetProductPremiumBreakup()
        {
            StringBuilder sbStr = new StringBuilder();

            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");
            sbStr.Append("");

            return sbStr.ToString();
        }
        public JsonResult GenrateMotorEnquiryId()
        {
            string result = "/moter/motersearch?enquiry_id=" + Motor_Service.GenrateMotorEnquiryId();
            return Json(result);
        }
        public JsonResult SaveMotorGenratedQuotes(string vehreg, string mobile, string enqid, int pincode, string state, string vechtype, string insurancetype)
        {
            string result = string.Empty;

            try
            {
                MotorModel model = new MotorModel();
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    model.AgencyId = lu.AgencyID;
                    model.AgencyName = lu.AgencyName;
                }

                model.VechileRegNo = vehreg;
                model.MobileNo = mobile;
                model.Enquiry_Id = Motor_Service.DecryptGenrateEnquiryId(enqid);
                model.PinCode = pincode;
                model.State = state;
                model.VechileType = vechtype;
                model.InsuranceType = insurancetype;
                model.EnquiryBookingType = "online";

                if (Motor_Service.SaveMoterGenratedQuotes(model))
                {
                    if (vechtype.ToLower() == "private_car")
                    {
                        result = "/moter/moterrejistration?enquiry_id=" + enqid;

                    }
                    else if (vechtype.ToLower() == "bike")
                    {
                        result = "/bike/bikerejistration?enquiry_id=" + enqid;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateMotorRegistration(string enqid, string rtoid, string rtoname, string dateofreg, string dateofmfg, bool IsBike = false)
        {
            string result = string.Empty;

            MotorModel model = new MotorModel();
            model.Enquiry_Id = Motor_Service.DecryptGenrateEnquiryId(enqid);
            model.RtoId = rtoid;
            model.RtoName = rtoname;
            model.DateOfReg = dateofreg;
            string[] dateofmfgsplit = dateofmfg.Split('-');
            model.DateOfMfg = "01/" + dateofmfgsplit[1] + "/" + dateofmfgsplit[0];

            if (Motor_Service.UpdateMoterRegDetail(model))
            {
                result = GetBrandDetails();
            }
            return Json(result);
        }
        public string GetBrandDetails()
        {
            string result = string.Empty;

            try
            {
                List<string> brandList = Motor_Service.GetBrandList(false);
                if (brandList != null && brandList.Count > 0)
                {
                    foreach (var item in brandList)
                    {
                        result = result + " <div id='Brnad_" + item.Trim() + "' class='col-sm-3 form-group branddyn' data-brand='" + item.Trim() + "'> <span id='BrandSected_" + item.Trim().Replace(" ", "_") + "' class='default-btn pull-right selectbtn brandselectsec'>" + item.Trim() + "</span> </div>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public JsonResult GetModelDetails(string brand, string enqid)
        {
            string result = string.Empty;

            try
            {
                List<string> modelList = Motor_Service.GetModelList(brand, Motor_Service.DecryptGenrateEnquiryId(enqid), false);
                if (modelList != null && modelList.Count > 0)
                {
                    foreach (var item in modelList)
                    {
                        result = result + " <div id='Model_" + item.Trim().Replace(" ", "_") + "' class='col-sm-3 form-group modeldyn' data-model='" + item.Trim() + "'> <span id='ModelSected_" + item.Trim().Replace(" ", "_") + "' class='default-btn pull-right selectbtn modelselectsec'>" + item.Trim() + "</span> </div>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetFuelDetails(string brand, string model, string enqid)
        {
            string result = string.Empty;

            try
            {
                List<string> fuelList = Motor_Service.GetFuelList(brand, model, Motor_Service.DecryptGenrateEnquiryId(enqid), false);
                if (fuelList != null && fuelList.Count > 0)
                {
                    foreach (var item in fuelList)
                    {
                        result = result + "<span class='default-btn pull-right selectbtn fueldyn' data-fuel='" + item.Trim() + "' id='Fuel_" + item + "'>" + item.ToUpper() + "</span>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetVarientDetails(string brand, string model, string fuel, string enqid)
        {
            string result = string.Empty;

            try
            {
                List<string> varientList = Motor_Service.GetVarientList(brand, model, fuel, Motor_Service.DecryptGenrateEnquiryId(enqid), false);
                if (varientList != null && varientList.Count > 0)
                {
                    foreach (var item in varientList)
                    {
                        string makeId = Regex.Replace(item.Trim().Replace(" ", "_"), @"[^\w\d]", "_");
                        result = result + "<div id='Varient_" + item + "' class='col-sm-3 form-group varientdyn' data-varient='" + item.Trim() + "'> <span id='VarientSected_" + item.Trim().Replace(" ", "_") + "' class='default-btn pull-right selectbtn varientselectsec'>" + item.Trim() + "</span> </div>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateVarientDetails(string varient, string enqid)
        {
            return Json(Motor_Service.UpdateVarientDetails(varient, Motor_Service.DecryptGenrateEnquiryId(enqid)));
        }
        public JsonResult UpdateOtherDetails(string isprevious, string policynumber, string expdate, string lastyr, string noclaim, string enqid, string pptype, string phtype)
        {
            return Json(Motor_Service.UpdateOtherDetails(isprevious, "", "", policynumber, expdate, lastyr, noclaim, Motor_Service.DecryptGenrateEnquiryId(enqid), pptype, phtype));
        }
        public JsonResult GetPreviousInsurerCodes_GoDigit()
        {
            return Json(CommonClass.PopulatePreviousInsurerCodes(Motor_Service.GetPreviousInsurerCodes_GoDigit()));
        }
        public JsonResult GetNCBPercentage()
        {
            return Json(CommonClass.PopulateGetNCBPercentage(Motor_Service.GetNCBPercentage()));
        }
        public JsonResult InsertAndUpdateMotoResponse(string enquiryid, string chianid, string proposalno, string suminsured, string productcode)
        {
            return Json(Convert.ToBoolean(Motor_Service.InsertAndUpdateMotoResponse(Motor_Service.DecryptGenrateEnquiryId(enquiryid), chianid, proposalno, suminsured, productcode)));
        }
        public JsonResult GetAddonsDetails(string enquiryid)
        {
            StringBuilder sbResult = new StringBuilder();

            try
            {
                if (!string.IsNullOrEmpty(enquiryid))
                {
                    var addOnsDetails = Motor_Service.InsertAndUpdateMotoResponse(enquiryid, "", "", "");

                    if (addOnsDetails != null)
                    {
                        DataTable dtDetails = (DataTable)addOnsDetails;
                        string chainid = dtDetails.Rows[0]["chainid"].ToString();
                        string proposalno = dtDetails.Rows[0]["proposalno"].ToString();
                        string suminsured = dtDetails.Rows[0]["suminsured"].ToString();
                        string response = dtDetails.Rows[0]["response"].ToString();

                        MotorRoot model = JsonConvert.DeserializeObject<MotorRoot>(response);

                        if (model != null && model.PrivateCarQuotesRS != null)
                        {
                            int loopcount = 1;
                            foreach (var item in model.PrivateCarQuotesRS.Where(p => p.Chainid == chainid && p.enquiryId == enquiryid && p.ProductDetails.grossPremium == suminsured))
                            {
                                if (loopcount % 2 == 0)
                                {
                                    sbResult.Append("<div class='col-sm-6' style='border-right: 1px solid #ccc;'>");
                                }
                                else
                                {
                                    sbResult.Append("<div class='col-sm-6'>");
                                }

                                sbResult.Append("<label class='seeingo' style='color: #777777!important;'>");
                                sbResult.Append("Personal Accident Cover of 15 Lakhs");
                                sbResult.Append("<input type='checkbox' class='submenuclass' id='chkIsProposerInsured' name='chkIsProposerInsured' checked='checked' />");
                                sbResult.Append("<span class='checkmark'></span>");
                                sbResult.Append("<span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer' title='Message...'></span>");
                                sbResult.Append("<p> ₹ 220</p>");
                                sbResult.Append("</label>");
                                sbResult.Append("</div>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(sbResult.ToString());
        }
        public JsonResult BindVehicleDetails(string enquiryid)
        {
            List<string> result = new List<string>();
            try
            {
                enquiryid = Motor_Service.DecryptGenrateEnquiryId(enquiryid);
                MotorModel model = Motor_Service.GetEnquiryDetails(enquiryid);
                if (model != null)
                {
                    result.Add("<div class='col-sm-12'><div class='row'>"
                        + "<div class='col-sm-3'>"
                        + "<p class='filterhed' style='font-size: 13px;'>RTO No : <span class='filterhed' style='font-size: 13px;'>" + model.VechileRegNo + "</span></p></div>"
                        + "<div class='col-sm-3'>"
                        + "<p class='filterhed' style='font-size: 13px;'>Reg. Date : <span class='filterhed' style='font-size: 13px;'>" + model.DateOfReg + "</span></p></div>"
                        + "<div class='col-sm-2'>"
                        + "<p class='filterhed' style='font-size: 13px;' id='policyfor'>Model : <span class='filterhed' style='font-size: 13px;'>" + model.ModelName + "</span> </p></div>"
                        + "<div class='col-sm-4'>"
                        + "<p class='filterhed' style='font-size: 13px;' id='plantype'>Variant Type : <span class='filterhed' style='font-size: 13px;'>" + model.VarientName + "</span> </p></div>"
                        + "</div></div>");
                    result.Add(YourPlanSummary(enquiryid, "nopay"));

                    MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                    MotorRoot quoteRespo = JsonConvert.DeserializeObject<MotorRoot>(enqRespo.response);
                    PrivateCarQuotesR pcDetail = quoteRespo.PrivateCarQuotesRS.Where(p => p.Chainid == enqRespo.chainid && p.ProductDetails.grossPremium == enqRespo.suminsured && p.ProductDetails.insuranceProductCode == enqRespo.productcode).FirstOrDefault();
                    if (pcDetail.enquiryId != null)
                    {
                        //StringBuilder sbAdons = new StringBuilder();
                        //if (pcDetail.ProductDetails.BaseCovers != null)
                        //{
                        //    int addon = 1;
                        //    sbAdons.Append("<div class='col-sm-12'><div class='row form-group' style='border: 1px solid #ccc;padding: 10px;'>");
                        //    sbAdons.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;margin-bottom: 10px;'><h6>Basic Covers</h6></div>");

                        //    foreach (var item in pcDetail.ProductDetails.BaseCovers)
                        //    {
                        //        if (addon % 2 == 0)
                        //        {
                        //            sbAdons.Append("<div class='col-sm-6'>");
                        //        }
                        //        else
                        //        {
                        //            sbAdons.Append("<div class='col-sm-6' style='border-right: 1px solid #ccc;'>");
                        //        }

                        //        sbAdons.Append("<label class='seeingo' style='color: #777777 !important;'>");
                        //        sbAdons.Append(item.covername);
                        //        sbAdons.Append("<input type='checkbox' class='submenuclass' " + (item.selection == true ? "checked='checked' disabled" : string.Empty) + " />");
                        //        sbAdons.Append("<span class='checkmark'></span>");
                        //        sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer' title='Message...'></span><p> ₹ " + item.netPremium + "</p>");
                        //        sbAdons.Append("</label>");
                        //        sbAdons.Append("</div>");
                        //        addon = addon + 1;
                        //    }

                        //    sbAdons.Append("</div></div>");
                        //}                    



                        StringBuilder sbAdons = new StringBuilder();
                        if (pcDetail.ProductDetails.AddOnCovers != null)
                        {
                            int addon = 1;
                            //sbAdons.Append("<div class='col-sm-12'><div class='row form-group' style='border: 1px solid #ccc;padding: 10px;'>");
                            //sbAdons.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;margin-bottom: 10px;'><h6>Add On Covers</h6></div>");
                            sbAdons.Append("<div class='row'>");
                            foreach (var item in pcDetail.ProductDetails.AddOnCovers)
                            {
                                if (addon % 2 == 0)
                                {
                                    sbAdons.Append("<div class='col-sm-6'>");
                                }
                                else
                                {
                                    sbAdons.Append("<div class='col-sm-6' style='border-right: 1px solid #ccc;'>");
                                }

                                if (item.netPremium != null)
                                {
                                    //if (item.netPremium != "XXXXX" && Convert.ToDouble(item.netPremium) > 0)
                                    //{
                                    sbAdons.Append("<label class='seeingo' style='color: #777777 !important;'>");
                                    sbAdons.Append(item.covername);
                                    sbAdons.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='AddOnCovers' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' " + (item.selection == true ? "checked='checked' disabled" : string.Empty) + " />");
                                    sbAdons.Append("<span class='checkmark'></span>");
                                    sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer' title='Message...'></span><p> ₹ " + item.netPremium + "</p>");
                                    sbAdons.Append("</label>");
                                    sbAdons.Append("</div>");
                                    //}
                                    //else
                                    //{
                                    //    sbAdons.Append("<label class='seeingo' style='color: #cacaca !important;'>");
                                    //    sbAdons.Append(item.covername);
                                    //    sbAdons.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='AddOnCovers' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' disabled />");
                                    //    sbAdons.Append("<span class='checkmark' style='border: 1px solid #dfdfdf!important;'></span>");
                                    //    sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #dfdfdf!important;cursor:pointer' title='Message...'></span><p> ₹ 0.00</p>");
                                    //    sbAdons.Append("</label>");
                                    //    sbAdons.Append("</div>");
                                    //}
                                }
                                else
                                {
                                    sbAdons.Append("<label class='seeingo' style='color: #cacaca !important;'>");
                                    sbAdons.Append(item.covername);
                                    sbAdons.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='AddOnCovers' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' disabled />");
                                    sbAdons.Append("<span class='checkmark' style='border: 1px solid #dfdfdf!important;'></span>");
                                    sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #dfdfdf!important;cursor:pointer' title='Message...'></span><p> ₹ 0.00</p>");
                                    sbAdons.Append("</label>");
                                    sbAdons.Append("</div>");
                                }

                                addon = addon + 1;
                            }

                            sbAdons.Append("</div>");
                        }
                        else
                        {
                            sbAdons.Append("<div class='col-sm-12 text-center' style='padding:20px;'><h6 class='text-danger'>Add on covers not available!</h6></div>");
                        }

                        result.Add(sbAdons.ToString());

                        StringBuilder sbLL = new StringBuilder();
                        if (pcDetail.ProductDetails.legalLiabilitys != null)
                        {
                            int addon = 1;
                            //sbLL.Append("<div class='col-sm-12'><div class='row form-group' style='border: 1px solid #ccc;padding: 10px;'>");
                            //sbLL.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;margin-bottom: 10px;'><h6>Legal Liabilities</h6></div>");

                            sbLL.Append("<div class='row'>");
                            foreach (var item in pcDetail.ProductDetails.legalLiabilitys)
                            {
                                if (addon % 2 == 0)
                                {
                                    sbLL.Append("<div class='col-sm-6'>");
                                }
                                else
                                {
                                    sbLL.Append("<div class='col-sm-6' style='border-right: 1px solid #ccc;'>");
                                }

                                if (item.netPremium != null)
                                {
                                    //if (item.netPremium != "XXXXX" && Convert.ToDouble(item.netPremium) > 0)
                                    //{
                                    sbLL.Append("<label class='seeingo' style='color: #777777 !important;'>");
                                    sbLL.Append(item.covername);
                                    sbLL.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='LegalLiabilitys' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' " + (item.selection == true ? "checked='checked' disabled" : string.Empty) + " />");
                                    sbLL.Append("<span class='checkmark'></span>");
                                    sbLL.Append("<span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer' title='Message...'></span><p> ₹ " + item.netPremium + "</p>");
                                    sbLL.Append("</label>");
                                    sbLL.Append("</div>");
                                    //}
                                    //else
                                    //{
                                    //    sbAdons.Append("<label class='seeingo' style='color: #cacaca !important;'>");
                                    //    sbAdons.Append(item.covername);
                                    //    sbAdons.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='LegalLiabilitys' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' disabled />");
                                    //    sbAdons.Append("<span class='checkmark' style='border: 1px solid #dfdfdf!important;'></span>");
                                    //    sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #dfdfdf!important;cursor:pointer' title='Message...'></span><p> ₹ 0.00</p>");
                                    //    sbAdons.Append("</label>");
                                    //    sbAdons.Append("</div>");
                                    //}
                                }
                                else
                                {
                                    sbAdons.Append("<label class='seeingo' style='color: #cacaca !important;'>");
                                    sbAdons.Append(item.covername);
                                    sbAdons.Append("<input type='checkbox' class='submenuclass addoncovers' data-coverheading='LegalLiabilitys' data-covername='" + item.covername + "' data-amount='" + item.netPremium + "' disabled />");
                                    sbAdons.Append("<span class='checkmark' style='border: 1px solid #dfdfdf!important;'></span>");
                                    sbAdons.Append("<span class='fa fa-info-circle' style='float: right;color: #dfdfdf!important;cursor:pointer' title='Message...'></span><p> ₹ 0.00</p>");
                                    sbAdons.Append("</label>");
                                    sbAdons.Append("</div>");
                                }

                                addon = addon + 1;
                            }

                            sbLL.Append("</div>");
                        }
                        else
                        {
                            sbLL.Append("<div class='col-sm-12 text-center' style='padding:20px;'><h6 class='text-danger'>Legal liabilities not available!</h6></div>");
                        }

                        result.Add(sbLL.ToString());

                        StringBuilder sbAC = new StringBuilder();
                        if (pcDetail.ProductDetails.AccessoriesCovers != null)
                        {
                            int addon = 1;
                            //sbAC.Append("<div class='col-sm-12'><div class='row form-group' style='border: 1px solid #ccc;padding: 10px;'>");
                            //sbAC.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;margin-bottom: 10px;'><h6>Accessories Covers</h6></div>");

                            sbAC.Append("<div class='row'>");
                            foreach (var item in pcDetail.ProductDetails.AccessoriesCovers)
                            {
                                if (addon % 2 == 0)
                                {
                                    sbAC.Append("<div class='col-sm-6'>");
                                }
                                else
                                {
                                    sbAC.Append("<div class='col-sm-6' style='border-right: 1px solid #ccc;'>");
                                }

                                sbAC.Append("<div class='row'>");
                                sbAC.Append("<div class='col-sm-6'><label class='seeingo' style='color: #777777 !important;'>" + item.covername);
                                sbAC.Append("<input type='checkbox' data-coverheading='AccessoriesCovers' data-rangcovername='" + item.covername.Replace(" ", "_") + "' data-covername='" + item.covername + "' class='submenuclass acccovers' " + (item.selection == true ? "checked='checked' disabled" : string.Empty) + " data-minamt='" + item.minallow + "' data-maxamt='" + item.maxallow + "'><span class='checkmark'></span><p> ₹ " + item.minallow + "-" + item.maxallow + "</p></label></div>");

                                sbAC.Append("<div class='col-sm-5'><input style='display:none;' id='txtamtrang_" + item.covername.Replace(" ", "_") + "' data-covername='" + item.covername.Replace(" ", "_") + "' maxlength='" + CommonClass.CharactersCount(item.maxallow.ToString()) + "' type='text' data-minamount='" + item.minallow + "' data-maxamount='" + item.maxallow + "' onkeypress='return isNumberOnlyKeyNoDot(event)' onkeyup='return CheckMinMaxAccessory(this)'/>");
                                sbAC.Append("<p id='perrormsg_" + item.covername.Replace(" ", "_") + "' style='font-size: 9px;color: red;text-align: right;margin-right: 50px;'></p>");
                                sbAC.Append("<div></div></div>");
                                sbAC.Append("<div class='col-sm-1'><span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer;position: absolute;' title='Message...'></span></div>");
                                sbAC.Append("</div>");

                                //sbAC.Append("<label class='seeingo' style='color: #777777 !important;'>");
                                //sbAC.Append(item.covername);
                                //sbAC.Append("<input type='checkbox' class='submenuclass addoncovers' " + (item.selection == true ? "checked='checked' disabled" : string.Empty) + " />");
                                //sbAC.Append("<span class='checkmark'></span>");
                                //sbAC.Append("<span class='fa fa-info-circle' style='float: right;color: #2c4da8;cursor:pointer' title='Message...'></span><p> ₹ " + item.minallow + "-" + item.maxallow + "</p>");
                                //sbAC.Append("</label>");
                                sbAC.Append("</div>");
                                addon = addon + 1;
                            }

                            sbAC.Append("</div>");
                        }
                        else
                        {
                            sbAC.Append("<div class='col-sm-12 text-center' style='padding:20px;'><h6 class='text-danger'>Legal liabilities not available!</h6></div>");
                        }

                        result.Add(sbAC.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetYourPlanSummary(string enquiryid)
        {
            return Json(YourPlanSummary(enquiryid, "pay"));
        }
        //public JsonResult BindAllProposalDetails(string enquiryid)
        //{
        //    List<string> result = new List<string>();

        //    if (!string.IsNullOrEmpty(enquiryid))
        //    {
        //        StringBuilder sbResult = new StringBuilder();
        //        MotorModel enq = Motor_Service.GetEnquiryDetails(enquiryid);
        //        MotorProposal proposal = Motor_Service.GetMotorProposal(enquiryid);
        //        MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
        //        MotorRoot quoteRespo = JsonConvert.DeserializeObject<MotorRoot>(enqRespo.response);

        //        sbResult.Append("<div id='VehicleDetails' class='tabcontent' style='display:block;'>");
        //        sbResult.Append("<table class='table table-striped summary_data_table'>");
        //        sbResult.Append("<tbody>");
        //        sbResult.Append("<tr><td>Vehicle Registration Number</td> <td><strong>:</strong>&nbsp;" + enq.VechileRegNo + "<br></td> </tr>");
        //        sbResult.Append("<tr><td>Insurance Type</td> <td><strong>:</strong> &nbsp;" + enq.insurancetype + "</td> </tr>");
        //        sbResult.Append("<tr><td>RTO</td> <td><strong>:</strong> &nbsp;" + enq.RtoName + "</td> </tr>");
        //        sbResult.Append("<tr><td>Date of Registraion</td> <td><strong>:</strong> &nbsp;" + enq.DateOfReg + "</td> </tr>");
        //        sbResult.Append("<tr><td>Brand</td> <td><strong>:</strong> &nbsp;" + enq.BrandName + "</td> </tr>");
        //        sbResult.Append("<tr><td>Model</td> <td><strong>:</strong> &nbsp;" + enq.ModelName + "</td> </tr>");
        //        sbResult.Append("<tr><td>Fuel</td> <td><strong>:</strong> &nbsp;" + enq.Fuel + "</td> </tr>");
        //        sbResult.Append("<tr><td>Variant</td> <td><strong>:</strong> &nbsp;" + enq.VarientName + "</td> </tr>");
        //        sbResult.Append("</tbody>");
        //        sbResult.Append("</table>");
        //        sbResult.Append("</div>");

        //        PrivateCarQuotesR quotsRespo = quoteRespo.PrivateCarQuotesRS.Where(p => p.Chainid == enqRespo.chainid && p.ProductDetails.grossPremium == enqRespo.suminsured && p.ProductDetails.insuranceProductCode == enqRespo.productcode).FirstOrDefault();

        //        sbResult.Append("<div id='InsuredDetails' class='tabcontent'>");
        //        sbResult.Append("<table class='table table-striped summary_data_table'>");
        //        sbResult.Append("<tbody>");
        //        if (quotsRespo != null)
        //        {
        //            sbResult.Append("<tr><td>IDV</td> <td><strong>:</strong> &nbsp;" + CommonClass.IndianMoneyFormat(quotsRespo.vehicle.vehicleIDV.idv.ToString()) + "</td> </tr>");
        //            if (quotsRespo.discounts != null)
        //            {
        //                sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;" + (quotsRespo.discounts.otherDiscounts != null ? "- - -" : "- - -") + " %</td> </tr>");
        //            }
        //            else
        //            {
        //                sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;0 %</td> </tr>");
        //            }
        //            sbResult.Append("<tr><td>Addon Details</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Plan Type</td> <td><strong>:</strong> &nbsp; " + enq.insurancetype + "</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Holder Type</td> <td><strong>:</strong> &nbsp; " + enq.policyholder + "</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Period</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Start Date</td> <td><strong>:</strong> &nbsp; " + quotsRespo.ProductDetails.startDate + "</td> </tr>");
        //            sbResult.Append("<tr><td>Policy End Date</td> <td><strong>:</strong> &nbsp; " + quotsRespo.ProductDetails.endDate + "</td> </tr>");
        //        }
        //        else
        //        {
        //            sbResult.Append("<tr><td>IDV</td> <td><strong>:</strong> &nbsp;- - -</td> </tr>");
        //            sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;- - -</td> </tr>");
        //            sbResult.Append("<tr><td>Addon Details</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Plan Type</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Holder Type</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Period</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Policy Start Date</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //            sbResult.Append("<tr><td>Policy End Date</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
        //        }
        //        sbResult.Append("</tbody>");
        //        sbResult.Append("</table>");
        //        sbResult.Append("</div>");

        //        sbResult.Append("<div id='OwnerDetails' class='tabcontent'>");
        //        sbResult.Append("<table class='table table-striped summary_data_table'>");
        //        sbResult.Append("<tbody>");
        //        sbResult.Append("<tr> <td>Owner Name</td> <td><strong>:</strong> &nbsp; " + proposal.firstname + " " + proposal.lastname + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Date of Birth</td> <td><strong>:</strong>  &nbsp; " + proposal.dob + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Mobile Numer</td> <td><strong>:</strong>  &nbsp; " + proposal.mobileno + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Email ID</td> <td><strong>:</strong>  &nbsp; " + proposal.emailid + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Address</td> <td><strong>:</strong>  &nbsp; " + proposal.address1 + ", " + proposal.address2 + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Nominee Name</td> <td><strong>:</strong> &nbsp; " + proposal.nfirstname + " " + proposal.nlastname + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Nominee Date of Birth</td> <td><strong>:</strong> &nbsp; " + proposal.ndob + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Relationship with Nominee</td> <td><strong>:</strong> &nbsp; " + proposal.nrelation + "</td> </tr>");
        //        sbResult.Append("</tbody>");
        //        sbResult.Append("</table>");
        //        sbResult.Append("</div>");

        //        sbResult.Append("<div id='VehicleHistory' class='tabcontent'>");
        //        sbResult.Append("<table class='table table-striped summary_data_table'>");
        //        sbResult.Append("<tbody>");
        //        sbResult.Append("<tr> <td>Policy Holder</td> <td><strong>:</strong> &nbsp; " + enq.policyholder + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Select Yer of Make</td> <td><strong>:</strong> &nbsp; " + enq.YearOfMake + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Previous Insurer</td> <td><strong>:</strong> &nbsp; " + enq.ispreviousinsurer.ToUpper() + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Previous Policy Number</td> <td><strong>:</strong> &nbsp; " + enq.policynumber + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Previous Policy Expire Date</td> <td><strong>:</strong> &nbsp; " + enq.policyexpirydate + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Last Yaer Claim</td> <td><strong>:</strong> &nbsp; " + enq.isclaiminlastyear + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Previous year NCB</td> <td><strong>:</strong> &nbsp; " + enq.previousyearnoclaim + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Engine Number</td> <td><strong>:</strong> &nbsp; " + proposal.engineno + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Chassis Number</td> <td><strong>:</strong> &nbsp; " + proposal.chasisno + "</td> </tr>");
        //        sbResult.Append("<tr> <td>Financed By</td> <td><strong>:</strong>  &nbsp; - - -</td> </tr>");
        //        sbResult.Append("</tbody>");
        //        sbResult.Append("</table>");
        //        sbResult.Append("</div>");
        //        result.Add(sbResult.ToString());

        //        string paymenthtml = "";
        //        MotorPayment paymentSec = JsonConvert.DeserializeObject<MotorPayment>(enqRespo.paymentcreationresponse);
        //        if (paymentSec != null)
        //        {
        //            TempData["enquiryid"] = paymentSec.enquiryId;
        //            TempData.Keep("enquiryid");

        //            paymenthtml = "<a href='" + paymentSec.Paymenturl + "' target='_blank' title='Payment' style='float: right;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>payment</p></a>";
        //        }

        //        result.Add(paymenthtml);
        //        result.Add(enqRespo.applicationid);
        //    }

        //    return Json(result);
        //}
        public string YourPlanSummary(string enquiryid, string ispay)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(enquiryid))
            {
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                if (enqRespo.id > 0)
                {
                    MotorRoot resop = JsonConvert.DeserializeObject<MotorRoot>(enqRespo.response);
                    StringBuilder sbRespo = new StringBuilder();
                    foreach (var item in resop.PrivateCarQuotesRS)
                    {
                        if (item.Chainid == enqRespo.chainid && item.ProductDetails.grossPremium == enqRespo.suminsured && item.ProductDetails.insuranceProductCode == enqRespo.productcode)
                        {
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-12'>");
                            sbRespo.Append("<h4 class='text-center'>Your Plan Summary</h4>");
                            sbRespo.Append(" <div class='row'>");
                            sbRespo.Append("<div class='col-sm-4'>");
                            sbRespo.Append(" <img src='" + item.image + "'>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-8'>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>IDV</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            if (item.vehicle != null)
                            {
                                if (item.vehicle.vehicleIDV != null)
                                {
                                    sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(item.vehicle.vehicleIDV.defaultIdv.ToString()) + "</label>");
                                }
                            }
                            //sbRespo.Append("<label>₹ 1,27,000</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>NCB</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            bool isncb = false;
                            if (item.discounts != null)
                            {
                                if (item.discounts.otherDiscounts != null && item.discounts.otherDiscounts.Count > 0)
                                {
                                    foreach (var disoth in item.discounts.otherDiscounts)
                                    {
                                        if (disoth.discountType.Trim().ToLower() == "ncb_discount")
                                        {
                                            isncb = true;
                                            sbRespo.Append("<label>" + disoth.discountPercent + " %</label>");
                                        }
                                    }
                                }
                            }

                            if (!isncb)
                            {
                                sbRespo.Append("<label>0 %</label>");
                            }
                            //sbRespo.Append("<label>45%</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>Plan Type</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>" + item.ProductName + "</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                            sbRespo.Append("<label class='texthideShow'>Basic Plan Covered</label>");
                            sbRespo.Append("<div class='row textdet' style='padding-bottom: 10px;'>");
                            sbRespo.Append("<div class='col-sm-12'>");
                            if (item.ProductDetails.BaseCovers != null)
                            {
                                double totalBaseCover = 0;

                                foreach (var bcover in item.ProductDetails.BaseCovers)
                                {
                                    totalBaseCover = totalBaseCover + Convert.ToDouble(bcover.netPremium);
                                    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>" + bcover.covername + "</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(bcover.netPremium) + "</p></div></div>");
                                }

                                sbRespo.Append("<div class='row' style='border-top: 1px solid #ccc;padding-top: 10px;'><div class='col-sm-8'><p>Base Covers</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(totalBaseCover.ToString()) + "</p></div></div>");

                                if (item.discounts != null)
                                {
                                    if (item.discounts.otherDiscounts != null && item.discounts.otherDiscounts.Count > 0)
                                    {
                                        foreach (var disoth in item.discounts.otherDiscounts)
                                        {
                                            sbRespo.Append("<div class='row'><div class='col-sm-8'><p>" + disoth.discountType.Replace("_", " ") + "</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(disoth.discountAmount) + "</p></div></div>");
                                        }
                                    }

                                    //if (item.discounts.otherDiscounts != null && item.discounts.otherDiscounts.Count > 0)
                                    //{
                                    //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>NCB Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(item.discounts.otherDiscounts[0].discountAmount) + "</p></div></div>");
                                    //}
                                    //else if (item.discounts.discountAmount != null)
                                    //{
                                    //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>NCB Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(item.discounts.discountAmount.ToString()) + "</p></div></div>");
                                    //}

                                    //if (!string.IsNullOrEmpty(item.discounts.specialDiscountAmount) && item.discounts.specialDiscountAmount != "0.00")
                                    //{
                                    //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>Other Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(item.discounts.specialDiscountAmount) + "</p></div></div>");
                                    //}
                                }

                                //sbRespo.Append("<div class='row' style='font-weight:bold;border-top: 1px solid #ccc;padding-top: 5px;'><div class='col-sm-8'><p>Total Amount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat((totalBaseCover + Convert.ToDouble(item.ProductDetails.totalTax)).ToString()) + "</p></div></div>");
                            }

                            //sbRespo.Append("<b>Important Plan Benefits</b>");

                            //sbRespo.Append("<p>2.Free Pick-up & Drop</p>");
                            //sbRespo.Append("<p>3.80% Advance Payment</p>");
                            //sbRespo.Append("<p>6-Month Repair Warranty</p>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>Premium Amount</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                            sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(item.ProductDetails.netPremium) + "</label>");

                            //sbRespo.Append("<label>₹ 2,836</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            //sbRespo.Append("<div class='row'>");
                            //sbRespo.Append("<div class='col-sm-6'>");
                            //sbRespo.Append("<label>Addon</label>");
                            //sbRespo.Append("</div>");
                            //sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                            //sbRespo.Append("<label>₹ 0.00</label>");
                            //sbRespo.Append("</div>");
                            //sbRespo.Append("</div>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label>GST@18%</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                            double tempTax = Convert.ToDouble(item.ProductDetails.netPremium) * 0.18;
                            sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(tempTax.ToString()) + "</label>");
                            //sbRespo.Append("<label>₹ 510</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                            sbRespo.Append("<div class='row'>");
                            sbRespo.Append("<div class='col-sm-6'>");
                            sbRespo.Append("<label style='font-weight:bold;font-size:15px;'>You'll Pay</label>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                            sbRespo.Append("<label class='text-danger' style='font-weight:bold;font-size:15px;'>");
                            sbRespo.Append(CommonClass.IndianMoneyFormat((Math.Ceiling(Convert.ToDouble(item.ProductDetails.netPremium) + tempTax)).ToString()));
                            //sbRespo.Append("₹ 3,724");
                            sbRespo.Append("</label><input type='hidden' id='hdnGrossPrice' value='" + item.ProductDetails.grossPremium + "' />");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                            if (ispay == "pay")
                            {
                                //MotorPayment paymentSec = JsonConvert.DeserializeObject<MotorPayment>(enqRespo.paymentcreationresponse);
                                //if (paymentSec != null)
                                //{
                                //    TempData["enquiryid"] = paymentSec.enquiryId;
                                //    TempData.Keep("enquiryid");

                                //    sbRespo.Append("<div class='col-sm-12'>");
                                //    sbRespo.Append("<div class='row' style='border-bottom: 1px solid #ccc;border-top: 1px solid #cccc; width: 103%; margin-left: -4px;'>");
                                //    sbRespo.Append("<div class='row col-sm-12'>");
                                //    sbRespo.Append("<h6 class='checkbox-inline' style='margin-top: 13px;margin-bottom: 13px;'>");
                                //    sbRespo.Append("<label class='seeingo'>");
                                //    sbRespo.Append("I accept the Terms &amp; Conditions");
                                //    sbRespo.Append("<input type='checkbox' class='submenuclass' id='commit' name='commit'  checked='checked'/>");
                                //    sbRespo.Append("<span class='checkmark'></span>");
                                //    sbRespo.Append("</label>");
                                //    sbRespo.Append("</h6>");
                                //    sbRespo.Append("</div>");
                                //    sbRespo.Append("</div>");
                                //    sbRespo.Append("<div class='row' style='width: 100%; margin-left: -4px;'>");
                                //    sbRespo.Append("<div class='col-sm-6'>");
                                //    sbRespo.Append("<a href='" + paymentSec.Paymenturl + "' target='_blank' style='float: right;border: 1px solid #e71820;background: #e71820;margin-bottom: 5px;padding: 5px 10px;margin-top: 5px;border-radius: 30px;    color: #fff;'>Make Payment</a>");
                                //    sbRespo.Append("</div>");
                                //    sbRespo.Append("<div class='col-sm-6'>");
                                //    sbRespo.Append("<a href='#' style='border: 1px solid #0e74bb;background: #0e74bb;margin-bottom: 5px;padding: 5px 10px;margin-top: 5px;border-radius: 30px;    color: #fff;'>Send Proposal</a>");
                                //    sbRespo.Append("</div>");
                                //    sbRespo.Append("</div>");
                                //}
                            }
                            sbRespo.Append("</div>");
                            sbRespo.Append("</div>");
                        }
                    }
                    result = sbRespo.ToString();
                }
            }
            return result;
        }
        public JsonResult SaveMotorProposalData(MotorProposal motorProposalDetail)
        {
            MotorNomineeRelation result = new MotorNomineeRelation();
            motorProposalDetail.enquiry_id = Motor_Service.DecryptGenrateEnquiryId(motorProposalDetail.enquiry_id);

            bool isSuccess = Motor_Service.SaveMotorProposalData(motorProposalDetail);
            if (isSuccess)
            {
                result.issuccess = "true";
                List<string> relationList = new List<string>();
                MotorEnquiryResponse enqDel = Motor_Service.GetMotorEnquiryResponse(motorProposalDetail.enquiry_id);
                if (enqDel.id > 0)
                {
                    result.relation = Motor_Service.MotorNomineeRelation(enqDel.chainid);
                }
            }
            return Json(result);
        }
        public JsonResult SaveMotorNomineeData(string enquiry_id, string relation, string title, string nfirstname, string nlastname, string ndob)
        {
            string result = "Fail";

            bool isSuccess = Motor_Service.SaveMotorNomineeData(Motor_Service.DecryptGenrateEnquiryId(enquiry_id), relation, title, nfirstname, nlastname, ndob);
            if (isSuccess)
            {
                result = "Success";
            }
            return Json(result);
        }
        public JsonResult SaveVehicleHistoryData(string enquiry_id, string vehicleregno, string vehiclemfdyear, string engineno, string chasisno, string valuntarydeductible, string previousinsured, string policyholder, string islonelease, string gstno, string panno)
        {
            string result = string.Empty;
            enquiry_id = Motor_Service.DecryptGenrateEnquiryId(enquiry_id);
            bool isSuccess = Motor_Service.SaveVehicleHistoryData(enquiry_id, vehicleregno, vehiclemfdyear, engineno, chasisno, valuntarydeductible, previousinsured, islonelease, policyholder, gstno, panno);
            if (isSuccess)
            {
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    PrivateCarProposal response = MotorAPIHelper.GetMotorApiPrivateCarProposalResponse(lu.UserId, lu.Password, enquiry_id, false);
                    if (!string.IsNullOrEmpty(response.applicationId))
                    {
                        if (Motor_Service.UpdatePerposalPremiumAmount(enquiry_id, response.ProductDetails.grossPremium, response.policyNumber))
                        {
                            bool isFetched = MotorPayment(enquiry_id, response.applicationId, response.Chainid);
                            //result = "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel'>Proposal Details</h5></div>"
                            //    + "<div class='modal-body'><p class='text-center' style='color: green;'><span class='fa fa-check-circle' style='font-size: 100px;'></span></p><h6 class='text-center' style='word-wrap: break-word!important;color: green;'>Proposal Created Successfully.</h6><h6 class='text-center' style='word-wrap: break-word!important;margin-top: 30px;'>Application Id : " + response.applicationId + "</h6><h6 class='text-center'>Policy Number : " + response.policyNumber + "</h6></div><div class='modal-footer'><button type='button' class='btn btn-primary' onclick='CheckPerposalDetailSummary();'>Check Proposal Summary</button>"
                            //     + "</div>";
                            result = "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel'>Proposal Details</h5></div>"
                                + "<div class='modal-body'><p class='text-center' style='color: green;'><span class='fa fa-check-circle' style='font-size: 100px;'></span></p><h6 class='text-center' style='word-wrap: break-word!important;color: green;'>Proposal Created Successfully.</h6></div><div class='modal-footer'><button type='button' class='btn btn-primary' onclick='CheckPerposalDetailSummary();'>Go To Proposal Summary</button>"
                                 + "</div>";
                        }
                    }
                    else
                    {
                        if (response.error != null && !string.IsNullOrEmpty(response.error.errorStackTrace))
                        {
                            result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>" + response.error.errorStackTrace + "</p></div>";
                            //<div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div>
                        }
                        else
                        {
                            result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>OOps! Something went wrong, Please try again.</p></div><div class='modal-footer'><a type='button' class='btn btn-primary' href='/moter/motersearch'>Go To Search</a></div>";
                        }
                    }
                }
            }
            return Json(result);
        }
        private bool MotorPayment(string enqId, string appid, string chainid)
        {
            insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                MotorPayment payment = MotorAPIHelper.GetMotorPaymentDetails(enqId, appid, chainid, lu.UserId, lu.Password);
                if (!string.IsNullOrEmpty(payment.Status) && payment.Status.ToLower() == "success")
                {
                    return true;
                }
            }
            return false;
        }

        //public JsonResult GetMotorEnquiryDetails(string enqid)
        //{
        //    MotorNomineeRelation result = new MotorNomineeRelation();

        //    MotorModel model = Motor_Service.GetEnquiryDetails(enqid);
        //    if (model.Id > 0)
        //    {
        //        result.issuccess = model.VechileRegNo;
        //        MotorEnquiryResponse enqDel = Motor_Service.GetMotorEnquiryResponse(enqid);
        //        if (enqDel.id > 0)
        //        {
        //            result.relation = Motor_Service.GetVolunatryDeductible(enqDel.chainid);
        //        }
        //    }
        //    return Json(result);
        //}
        public JsonResult GetValuntaryAndPreviousInsured(string enqid)
        {
            List<string> result = new List<string>();

            try
            {
                enqid = Motor_Service.DecryptGenrateEnquiryId(enqid);
                MotorModel model = Motor_Service.GetEnquiryDetails(enqid);
                if (model.Id > 0)
                {
                    MotorEnquiryResponse enqDel = Motor_Service.GetMotorEnquiryResponse(enqid);

                    result.Add(model.VechileRegNo);
                    result.Add(Motor_Service.GetVolunatryDeductible(enqDel.chainid, "car"));
                    result.Add(Motor_Service.GetPreviousInsured(enqDel.chainid));
                    result.Add(Motor_Service.GetPreviousPolicyType(model.insurancetype, "PC"));
                    result.Add(model.insurancetype);
                    result.Add(model.policyholdertype);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult CheckAndGetDetails(string pincode)
        {
            List<string> result = new List<string>();
            if (!string.IsNullOrEmpty(pincode))
            {
                result = Motor_Service.CheckAndGetDetails(pincode);
            }
            return Json(result);
        }

        #endregion
        public JsonResult GetPdfDocument(string enquiryid)
        {
            enquiryid = enquiryid.Contains("sim") ? enquiryid : Motor_Service.DecryptGenrateEnquiryId(enquiryid);
            return Json(Motor_Service.GetPdfDocumentByEnqId(enquiryid));
        }
        public JsonResult BindAllProposalDetails(string enquiryid)
        {
            List<string> result = new List<string>();

            if (!string.IsNullOrEmpty(enquiryid))
            {
                enquiryid = Motor_Service.DecryptGenrateEnquiryId(enquiryid);
                StringBuilder sbResult = new StringBuilder();
                MotorModel enq = Motor_Service.GetEnquiryDetails(enquiryid);
                MotorProposal proposal = Motor_Service.GetMotorProposal(enquiryid);
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                PrivateCarProposal perpoRespo = JsonConvert.DeserializeObject<PrivateCarProposal>(enqRespo.proposalresponse);

                sbResult.Append("<div id='VehicleDetails' class='tabcontent' style='display:block;'>");
                sbResult.Append("<table class='table table-striped summary_data_table'>");
                sbResult.Append("<tbody>");
                sbResult.Append("<tr><td>Vehicle Registration Number</td> <td><strong>:</strong>&nbsp;" + enq.VechileRegNo + "<br></td> </tr>");
                sbResult.Append("<tr><td>Insurance Type</td> <td><strong>:</strong> &nbsp;" + enq.insurancetype + "</td> </tr>");
                sbResult.Append("<tr><td>RTO</td> <td><strong>:</strong> &nbsp;" + enq.RtoName + "</td> </tr>");
                sbResult.Append("<tr><td>Date of Registraion</td> <td><strong>:</strong> &nbsp;" + enq.DateOfReg + "</td> </tr>");
                sbResult.Append("<tr><td>Brand</td> <td><strong>:</strong> &nbsp;" + enq.BrandName + "</td> </tr>");
                sbResult.Append("<tr><td>Model</td> <td><strong>:</strong> &nbsp;" + enq.ModelName + "</td> </tr>");
                sbResult.Append("<tr><td>Fuel</td> <td><strong>:</strong> &nbsp;" + enq.Fuel + "</td> </tr>");
                sbResult.Append("<tr><td>Variant</td> <td><strong>:</strong> &nbsp;" + enq.VarientName + "</td> </tr>");
                sbResult.Append("</tbody>");
                sbResult.Append("</table>");
                sbResult.Append("</div>");

                sbResult.Append("<div id='OtherDetails' class='tabcontent'>");
                sbResult.Append("<table class='table table-striped summary_data_table'>");
                sbResult.Append("<tbody>");
                if (perpoRespo.ProductDetails.BaseCovers != null)
                {
                    sbResult.Append("<tr style='text-align:center;'><td colspan='2' class='text-danger'><strong>Base Covers</strong></td></tr>");
                    foreach (var basecover in perpoRespo.ProductDetails.BaseCovers)
                    {
                        sbResult.Append("<tr><td>" + basecover.covername + "</td><td><strong>:</strong>&nbsp;" + CommonClass.IndianMoneyFormat(basecover.netPremium) + "<br></td></tr>");
                    }
                }

                if (perpoRespo.ProductDetails.AddOnCovers != null)
                {
                    bool isaddon = false;
                    foreach (var addon in perpoRespo.ProductDetails.AddOnCovers)
                    {
                        if (addon.selection)
                        {
                            if (!isaddon)
                            {
                                sbResult.Append("<tr style='text-align:center;'><td colspan='2' class='text-danger'><strong>Add On Covers</strong></td></tr>");
                            }

                            sbResult.Append("<tr><td>" + addon.covername + "</td><td><strong>:</strong>&nbsp;" + CommonClass.IndianMoneyFormat(addon.netPremium) + "<br></td></tr>");
                            isaddon = true;
                        }
                    }
                }

                if (perpoRespo.ProductDetails.legalLiabilitys != null)
                {
                    bool islegal = false;
                    foreach (var legal in perpoRespo.ProductDetails.legalLiabilitys)
                    {
                        if (legal.selection)
                        {
                            if (!islegal)
                            {
                                sbResult.Append("<tr style='text-align:center;'><td colspan='2' class='text-danger'><strong>Legal Liability</strong></td></tr>");
                            }

                            sbResult.Append("<tr><td>" + legal.covername + "</td><td><strong>:</strong>&nbsp;" + CommonClass.IndianMoneyFormat(legal.netPremium) + "<br></td></tr>");
                            islegal = true;
                        }
                    }
                }

                if (perpoRespo.ProductDetails.AccessoriesCovers != null)
                {
                    bool isacc = false;
                    foreach (var acc in perpoRespo.ProductDetails.AccessoriesCovers)
                    {
                        if (acc.selection)
                        {
                            if (!isacc)
                            {
                                sbResult.Append("<tr style='text-align:center;'><td colspan='2' class='text-danger'><strong>Accessories Covers</strong></td></tr>");
                            }

                            sbResult.Append("<tr><td>" + acc.covername + "</td><td><strong>:</strong>&nbsp;" + CommonClass.IndianMoneyFormat(acc.insuredAmount.ToString()) + "<br></td></tr>");
                            isacc = true;
                        }
                    }
                }

                //if (perpoRespo.ProductDetails.legalLiabilitys != null)
                //{
                //    sbResult.Append("<tr style='text-align:center;'><td colspan='2' class='text-danger'><strong>Legal Liabilies</strong></td></tr>");
                //    foreach (var legalliabilitys in perpoRespo.ProductDetails.legalLiabilitys)
                //    {
                //        sbResult.Append("<tr><td>" + legalliabilitys.covername + "</td><td><strong>:</strong>&nbsp;" + CommonClass.IndianMoneyFormat(legalliabilitys.netPremium) + "<br></td></tr>");
                //    }
                //}
                sbResult.Append("</tbody>");
                sbResult.Append("</table>");
                sbResult.Append("</div>");

                sbResult.Append("<div id='InsuredDetails' class='tabcontent'>");
                sbResult.Append("<table class='table table-striped summary_data_table'>");
                sbResult.Append("<tbody>");
                if (perpoRespo != null)
                {
                    sbResult.Append("<tr><td>IDV</td> <td><strong>:</strong> &nbsp;" + CommonClass.IndianMoneyFormat(perpoRespo.vehicle.vehicleIDV.idv.ToString()) + "</td> </tr>");
                    if (perpoRespo.discounts != null)
                    {
                        sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;" + (perpoRespo.discounts.discountPercent > 0 ? "- - -" : "- - -") + " %</td> </tr>");
                    }
                    else
                    {
                        sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;0 %</td> </tr>");
                    }
                    sbResult.Append("<tr><td>Addon Details</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Plan Type</td> <td><strong>:</strong> &nbsp; " + enq.insurancetype + "</td> </tr>");
                    sbResult.Append("<tr><td>Policy Holder Type</td> <td><strong>:</strong> &nbsp; " + proposal.policyholdertype + "</td> </tr>");
                    sbResult.Append("<tr><td>Policy Period</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Policy Start Date</td> <td><strong>:</strong> &nbsp; " + perpoRespo.ProductDetails.startDate + "</td> </tr>");
                    sbResult.Append("<tr><td>Policy End Date</td> <td><strong>:</strong> &nbsp; " + perpoRespo.ProductDetails.endDate + "</td> </tr>");
                }
                else
                {
                    sbResult.Append("<tr><td>IDV</td> <td><strong>:</strong> &nbsp;- - -</td> </tr>");
                    sbResult.Append("<tr><td>NCB</td> <td><strong>:</strong> &nbsp;- - -</td> </tr>");
                    sbResult.Append("<tr><td>Addon Details</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Plan Type</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Policy Holder Type</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Policy Period</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Policy Start Date</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                    sbResult.Append("<tr><td>Policy End Date</td> <td><strong>:</strong> &nbsp; - - -</td> </tr>");
                }
                sbResult.Append("</tbody>");
                sbResult.Append("</table>");
                sbResult.Append("</div>");

                sbResult.Append("<div id='OwnerDetails' class='tabcontent'>");
                sbResult.Append("<table class='table table-striped summary_data_table'>");
                sbResult.Append("<tbody>");
                sbResult.Append("<tr> <td>Owner Name</td> <td><strong>:</strong> &nbsp; " + proposal.firstname + " " + proposal.lastname + "</td> </tr>");
                sbResult.Append("<tr> <td>Date of Birth</td> <td><strong>:</strong>  &nbsp; " + proposal.dob + "</td> </tr>");
                sbResult.Append("<tr> <td>Mobile Numer</td> <td><strong>:</strong>  &nbsp; " + proposal.mobileno + "</td> </tr>");
                sbResult.Append("<tr> <td>Email ID</td> <td><strong>:</strong>  &nbsp; " + proposal.emailid + "</td> </tr>");
                sbResult.Append("<tr> <td>Address</td> <td><strong>:</strong>  &nbsp; " + proposal.address1 + ", " + proposal.address2 + "</td> </tr>");
                sbResult.Append("<tr> <td>Nominee Name</td> <td><strong>:</strong> &nbsp; " + proposal.nfirstname + " " + proposal.nlastname + "</td> </tr>");
                sbResult.Append("<tr> <td>Nominee Date of Birth</td> <td><strong>:</strong> &nbsp; " + proposal.ndob + "</td> </tr>");
                sbResult.Append("<tr> <td>Relationship with Nominee</td> <td><strong>:</strong> &nbsp; " + proposal.nrelation + "</td> </tr>");
                sbResult.Append("</tbody>");
                sbResult.Append("</table>");
                sbResult.Append("</div>");

                sbResult.Append("<div id='VehicleHistory' class='tabcontent'>");
                sbResult.Append("<table class='table table-striped summary_data_table'>");
                sbResult.Append("<tbody>");
                sbResult.Append("<tr> <td>Policy Holder</td> <td><strong>:</strong> &nbsp; " + proposal.policyholdertype + "</td> </tr>");
                sbResult.Append("<tr> <td>Date of Manufacture</td> <td><strong>:</strong> &nbsp; " + enq.DateOfMfg + "</td> </tr>");
                sbResult.Append("<tr> <td>Previous Insurer</td> <td><strong>:</strong> &nbsp; " + enq.ispreviousinsurer.ToUpper() + "</td> </tr>");
                sbResult.Append("<tr> <td>Previous Policy Number</td> <td><strong>:</strong> &nbsp; " + enq.policynumber + "</td> </tr>");
                sbResult.Append("<tr> <td>Previous Policy Expire Date</td> <td><strong>:</strong> &nbsp; " + enq.policyexpirydate + "</td> </tr>");
                sbResult.Append("<tr> <td>Last Yaer Claim</td> <td><strong>:</strong> &nbsp; " + enq.isclaiminlastyear + "</td> </tr>");
                sbResult.Append("<tr> <td>Previous year NCB</td> <td><strong>:</strong> &nbsp; " + enq.previousyearnoclaim + "</td> </tr>");
                sbResult.Append("<tr> <td>Engine Number</td> <td><strong>:</strong> &nbsp; " + proposal.engineno + "</td> </tr>");
                sbResult.Append("<tr> <td>Chassis Number</td> <td><strong>:</strong> &nbsp; " + proposal.chasisno + "</td> </tr>");
                sbResult.Append("<tr> <td>Financed By</td> <td><strong>:</strong>  &nbsp; - - -</td> </tr>");
                sbResult.Append("</tbody>");
                sbResult.Append("</table>");
                sbResult.Append("</div>");
                result.Add(sbResult.ToString());

                string paymenthtml = "";
                MotorPayment paymentSec = JsonConvert.DeserializeObject<MotorPayment>(enqRespo.paymentcreationresponse);
                if (paymentSec != null)
                {
                    //paymenthtml = "<a href='" + paymentSec.Paymenturl + "&enquiryid=" + enquiryid + "' target='_blank' title='Payment' style='float: right;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>payment</p></a>";
                    paymenthtml = "<a href='" + paymentSec.Paymenturl + "' target='_blank' title='Payment' style='float: right;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>payment</p></a>";

                    //List<string> strPayList = Motor_Service.CheckPaymentStatusDetails(enquiryid);
                    //if (strPayList != null && strPayList.Count > 0)
                    //{
                    //    paymenthtml = "<div onclick='CheckPaymentDetails();' style='cursor:pointer;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>Payment</p></div>";
                    //}
                    //else
                    //{
                    //    paymenthtml = "<a href='" + paymentSec.Paymenturl + "&enquiryid=" + enquiryid + "' target='_blank' title='Payment' style='float: right;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>payment</p></a>";
                    //}
                }

                result.Add(paymenthtml);
                result.Add(enqRespo.proposalnumber);
            }

            return Json(result);
        }
        public JsonResult GetProposalPlanSummary(string enquiryid)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(enquiryid))
            {
                enquiryid = Motor_Service.DecryptGenrateEnquiryId(enquiryid);
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                if (enqRespo.id > 0)
                {
                    PrivateCarProposal perpoRespo = JsonConvert.DeserializeObject<PrivateCarProposal>(enqRespo.proposalresponse);
                    StringBuilder sbRespo = new StringBuilder();

                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-12'>");
                    sbRespo.Append("<h4 class='text-center'>Your Plan Summary</h4>");
                    sbRespo.Append(" <div class='row'>");
                    sbRespo.Append("<div class='col-sm-4'>");
                    sbRespo.Append(" <img src='" + perpoRespo.image + "'>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-8'>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>IDV</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    if (perpoRespo.vehicle != null)
                    {
                        if (perpoRespo.vehicle.vehicleIDV != null)
                        {
                            sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(perpoRespo.vehicle.vehicleIDV.defaultIdv.ToString()) + "</label>");
                        }
                    }
                    //sbRespo.Append("<label>₹ 1,27,000</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>NCB</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    bool isncb = false;
                    if (perpoRespo.discounts != null)
                    {
                        if (perpoRespo.discounts.otherDiscounts != null && perpoRespo.discounts.otherDiscounts.Count > 0)
                        {
                            foreach (var disoth in perpoRespo.discounts.otherDiscounts)
                            {
                                if (disoth.discountType.Trim().ToLower() == "ncb_discount" || disoth.discountType.Trim().ToLower() == "no claim bonus")
                                {
                                    isncb = true;
                                    sbRespo.Append("<label>" + disoth.discountPercent + " %</label>");
                                }
                            }
                        }
                    }

                    if (!isncb)
                    {
                        sbRespo.Append("<label>0 %</label>");
                    }

                    //sbRespo.Append("<label>45%</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>Plan Type</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>" + perpoRespo.ProductName + "</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                    //sbRespo.Append("<label class='texthideShow'>Plan Features <span id='spnbar' class='fa fa-angle-down' style='font-size: 18px; color: red;'></span></label>");
                    sbRespo.Append("<div class='row textdet'>");
                    sbRespo.Append("<div class='col-sm-12'>");
                    if (perpoRespo.ProductDetails.BaseCovers != null)
                    {
                        double totalBaseCover = 0;

                        foreach (var bcover in perpoRespo.ProductDetails.BaseCovers)
                        {
                            totalBaseCover = totalBaseCover + Convert.ToDouble(bcover.netPremium);
                            sbRespo.Append("<div class='row'><div class='col-sm-8'><p>" + bcover.covername + "</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(bcover.netPremium) + "</p></div></div>");
                        }

                        sbRespo.Append("<div class='row' style='border-top: 1px solid #ccc;padding-top: 10px;'><div class='col-sm-8'><p>BASE COVER</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(totalBaseCover.ToString()) + "</p></div></div>");

                        if (perpoRespo.discounts != null)
                        {
                            if (perpoRespo.discounts.otherDiscounts != null && perpoRespo.discounts.otherDiscounts.Count > 0)
                            {
                                foreach (var disoth in perpoRespo.discounts.otherDiscounts)
                                {
                                    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>" + disoth.discountType.Replace("_", " ") + "</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(disoth.discountAmount) + "</p></div></div>");
                                }
                            }

                            //if (perpoRespo.discounts.otherDiscounts != null && perpoRespo.discounts.otherDiscounts.Count > 0)
                            //{
                            //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>NCB Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(perpoRespo.discounts.otherDiscounts[0].discountAmount) + "</p></div></div>");
                            //}
                            //else if (perpoRespo.discounts.discountAmount != null)
                            //{
                            //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>NCB Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(perpoRespo.discounts.discountAmount.ToString()) + "</p></div></div>");
                            //}

                            //if (!string.IsNullOrEmpty(perpoRespo.discounts.specialDiscountAmount) && perpoRespo.discounts.specialDiscountAmount != "0.00")
                            //{
                            //    sbRespo.Append("<div class='row'><div class='col-sm-8'><p>Other Discount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat(perpoRespo.discounts.specialDiscountAmount) + "</p></div></div>");
                            //}
                        }

                        //sbRespo.Append("<div class='row' style='font-weight:bold;border-top: 1px solid #ccc;padding-top: 5px;'><div class='col-sm-8'><p>Total Amount</p></div><div class='col-sm-4'><p class='pull-right'>" + CommonClass.IndianMoneyFormat((totalBaseCover + Convert.ToDouble(item.ProductDetails.totalTax)).ToString()) + "</p></div></div>");
                    }
                    //sbRespo.Append("<b>Important Plan Benefits</b>");
                    //sbRespo.Append("<p>1.Cashless Claims or Reimbursement Within 24 hours Across India</p>");
                    //sbRespo.Append("<p>2.Free Pick-up & Drop</p>");
                    //sbRespo.Append("<p>3.80% Advance Payment</p>");
                    //sbRespo.Append("<p>6-Month Repair Warranty</p>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>Premium Amount</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                    sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(perpoRespo.ProductDetails.netPremium) + "</label>");

                    //sbRespo.Append("<label>₹ 2,836</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    //sbRespo.Append("<div class='row'>");
                    //sbRespo.Append("<div class='col-sm-6'>");
                    //sbRespo.Append("<label>Addon</label>");
                    //sbRespo.Append("</div>");
                    //sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                    //sbRespo.Append("<label>₹ 0.00</label>");
                    //sbRespo.Append("</div>");
                    //sbRespo.Append("</div>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label>GST@18%</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                    double tempTax = Convert.ToDouble(perpoRespo.ProductDetails.netPremium) * 0.18;
                    sbRespo.Append("<label>" + CommonClass.IndianMoneyFormat(tempTax.ToString()) + "</label>");
                    //sbRespo.Append("<label>₹ 510</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-12' style='border: 1px solid #ccc; border-left: none; border-right: none; flex: 0 0 95%; margin-left: 10px;'>");
                    sbRespo.Append("<div class='row'>");
                    sbRespo.Append("<div class='col-sm-6'>");
                    sbRespo.Append("<label style='font-weight:bold;font-size:15px;'>You'll Pay</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("<div class='col-sm-6' style='text-align: right;'>");
                    sbRespo.Append("<label class='text-danger' style='font-weight:bold;font-size:15px;'>");
                    sbRespo.Append(CommonClass.IndianMoneyFormat((Math.Ceiling(Convert.ToDouble(perpoRespo.ProductDetails.netPremium) + tempTax)).ToString()));
                    //sbRespo.Append("₹ 3,724");
                    sbRespo.Append("</label>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");
                    sbRespo.Append("</div>");

                    result = sbRespo.ToString();
                }
            }
            return Json(result);
        }
        public JsonResult CheckPaymentStatusDetails(string enquiryid)
        {
            string result = string.Empty;

            List<string> strPayList = Motor_Service.CheckPaymentStatusDetails(Motor_Service.DecryptGenrateEnquiryId(enquiryid));
            if (strPayList != null && strPayList.Count > 0)
            {
                result = "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel'>Transaction Details</h5></div>"
                                   + "<div class='modal-body'><h5 class='text-center' style='word-wrap: break-word!important;'>Transaction Number : " + strPayList[0] + "</h5><h5 class='text-center'>Policy Number : " + (!string.IsNullOrEmpty(strPayList[1]) ? strPayList[1] : "N/A") + "</h5></div>"
                                   + "<div class='modal-footer'><button type='button' class='btn btn-primary' data-dismiss='modal'>Close</button>"
                                    + "</div>";
            }

            return Json(result);
        }
        public JsonResult AddUpdateSelectedAddon(List<AddUpdateSelectedAddon> addon, string enqId)
        {
            enqId = Motor_Service.DecryptGenrateEnquiryId(enqId);
            List<object> result = new List<object>();
            //if (addon.Count > 0)
            //{
            result.Add(Motor_Service.AddUpdateSelectedAddon(addon, enqId));
            MotorModel enqDetail = Motor_Service.GetEnquiryDetails(enqId);
            result.Add(enqDetail.PinCode);
            //}
            return Json(result);
        }
    }
}