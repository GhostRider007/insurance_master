﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ecommerce.Models.Common;
using insurance.HealthApi;
using insurance.Models;
using insurance.Models.Common;
using insurance.Service;
using insurance.Service.HealthService;
using Newtonsoft.Json;
using static insurance.Models.Health.HealthModel;
using static insurance.Models.Health.HealthComparisonModel;
using insurance.Models.Health;
using insurance.Areas.AdminArea.Models;
using static insurance.Models.Health.HealthPerposal;
using System.Text.RegularExpressions;
using System.Net;
using Post_Utility.Controller_Page;

namespace insurance.Controllers
{
    public class HealthController : Controller
    {
        public ActionResult HelthSearch()
        {
            return View();
        }
        public ActionResult HelthSearchList()
        {
            Health_Service.RemoveHealth_UrlParaFromCache();
            HealthAPIHelper.RemoveHealth_RespoFromCache();
            return View();
        }
        public ActionResult ProposalDetails(string enquiry_id, string product)
        {
            return View();
        }
        public ActionResult Proposalsummary()
        {
            return View();
        }

        #region [Payment Confirmation Page Details]
        [AcceptVerbs(WebRequestMethods.Http.Get, WebRequestMethods.Http.Post)]
        public ActionResult PaymentConfirmation(string purchaseToken, PaymentSuccessDel caresuccess, string Inquery_id)
        {
            PaymentSuccessDel starpayDel = new PaymentSuccessDel();
            if (!string.IsNullOrEmpty(purchaseToken))
            {
                starpayDel = StarHealthConfirmationSection(purchaseToken, TempData["EnqId"].ToString());
            }
            else if (!string.IsNullOrEmpty(Inquery_id))
            {
                Inquery_id = Health_Service.DecryptGenrateEnquiryId(Inquery_id);
                CreateHealth_Proposal perposalDel = Health_Service.GetProposalDetail(Inquery_id);

                if (!string.IsNullOrEmpty(caresuccess.merchantTxnId) && !string.IsNullOrEmpty(Inquery_id) && perposalDel.companyid == "4")
                {
                    starpayDel = AdityBirlaHealthConfirmationSection(caresuccess, Inquery_id);
                }
                else if (!string.IsNullOrEmpty(Inquery_id) && perposalDel.companyid == "2")
                {
                    starpayDel = CareHealthConfirmationSection(caresuccess, Inquery_id);
                }
            }

            return View(starpayDel);
        }
        private PaymentSuccessDel StarHealthConfirmationSection(string purchaseToken, string enquiryid)
        {
            PaymentSuccessDel starpayDel = new PaymentSuccessDel();

            string tempEnqId = (TempData["EnqId"] != null ? TempData["EnqId"].ToString() : enquiryid);
            if (string.IsNullOrEmpty(tempEnqId))
            {
                bool isEnqIdExist = HealthAPIHelper.IsCurrentEnquiryIdExist(ref tempEnqId);
            }

            if (!string.IsNullOrEmpty(tempEnqId))
            {
                Dictionary<string, string> iDictionary = Health_Controller.StarHealthConfirmationSection(purchaseToken, tempEnqId);
                starpayDel.Status = iDictionary.FirstOrDefault(x => x.Key == "status").Value;
                starpayDel.purchaseToken = iDictionary.FirstOrDefault(x => x.Key == "purchaseToken").Value;
                starpayDel.enquiryid = iDictionary.FirstOrDefault(x => x.Key == "enquiryid").Value;
                starpayDel.imgurl = iDictionary.FirstOrDefault(x => x.Key == "imgurl").Value;
                starpayDel.companyid = iDictionary.FirstOrDefault(x => x.Key == "companyid").Value;
                starpayDel.premium = iDictionary.FirstOrDefault(x => x.Key == "premium").Value;
                starpayDel.proposalNum = iDictionary.FirstOrDefault(x => x.Key == "proposalNum").Value;
            }
            else
            {
                starpayDel.Status = "No Captured - " + tempEnqId;
            }
            return starpayDel;
        }
        private PaymentSuccessDel CareHealthConfirmationSection(PaymentSuccessDel caresuccess, string Inquery_id)
        {
            try
            {
                Dictionary<string, string> iDictionary = Health_Controller.CareHealthConfirmationSection(caresuccess.policyNumber, caresuccess.transactionRefNum, caresuccess.uwDecision, caresuccess.errorFlag, caresuccess.errorMsg, caresuccess.Status, caresuccess.enquiryid, Inquery_id);

                caresuccess.enquiryid = Inquery_id;
                caresuccess.Status = iDictionary.FirstOrDefault(x => x.Key == "status").Value;
                caresuccess.imgurl = iDictionary.FirstOrDefault(x => x.Key == "imgurl").Value;
                caresuccess.companyid = iDictionary.FirstOrDefault(x => x.Key == "companyid").Value;
                caresuccess.premium = iDictionary.FirstOrDefault(x => x.Key == "premium").Value;
                caresuccess.proposalNum = iDictionary.FirstOrDefault(x => x.Key == "proposalNum").Value;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return caresuccess;
        }
        private PaymentSuccessDel AdityBirlaHealthConfirmationSection(PaymentSuccessDel caresuccess, string Inquery_id)
        {
            try
            {
                Dictionary<string, string> iDictionary = Health_Controller.AdityBirlaHealthConfirmationSection(caresuccess.amount, caresuccess.TxRefNo, caresuccess.TxStatus, caresuccess.TxMsg, caresuccess.Status, caresuccess.enquiryid, caresuccess.txnDateTime, caresuccess.QuoteId, Inquery_id);

                caresuccess.enquiryid = Inquery_id;
                caresuccess.Status = iDictionary.FirstOrDefault(x => x.Key == "status").Value;
                caresuccess.imgurl = iDictionary.FirstOrDefault(x => x.Key == "imgurl").Value;
                caresuccess.companyid = iDictionary.FirstOrDefault(x => x.Key == "companyid").Value;
                caresuccess.premium = iDictionary.FirstOrDefault(x => x.Key == "premium").Value;
                caresuccess.proposalNum = iDictionary.FirstOrDefault(x => x.Key == "proposalNum").Value;
                caresuccess.transactionRefNum = caresuccess.TxRefNo;

                string productid = iDictionary.FirstOrDefault(x => x.Key == "productid").Value;
                string companyid = iDictionary.FirstOrDefault(x => x.Key == "companyid").Value;

                NewEnquiry_Health quotes = Health_Service.GetNewEnquiryHealthDetails(Inquery_id);

                if (!string.IsNullOrEmpty(quotes.login_userid))
                {
                    ABHISaveProposal saveProposal_Respo = HealthAPIHelper.API_Save_Perposal(productid, companyid, caresuccess.enquiryid, quotes.login_userid, quotes.login_password, quotes.AgencyId);
                    if (saveProposal_Respo.success)
                    {
                        if (saveProposal_Respo.response.data != null)
                        {
                            caresuccess.proposalNum = saveProposal_Respo.response.data.proposalNum;
                            caresuccess.policyNumber = saveProposal_Respo.response.data.policyNumber;
                            caresuccess.premium = saveProposal_Respo.response.data.totalPremium;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return caresuccess;
        }
        #endregion

        private SearchUrlParaMeter GetSearchUrlParaMeter(string enquiry_id)
        {
            return Health_Service.GetSearchUrlParaMeter(enquiry_id);
        }

        #region [Json Section]
        public JsonResult GenrateEnquiryId()
        {
            string result = "/health/helthsearch?enquiry_id=" + Health_Service.GenrateEnquiryId();
            return Json(result);
        }
        public JsonResult GetSumInsuredPriceList()
        {
            List<SumInsuredMaster> sumInsuredList = new List<SumInsuredMaster>();

            try
            {
                sumInsuredList = Health_Service.GetSumInsuredPriceList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(sumInsuredList);
        }
        public JsonResult SaveGenratedQuotes(NewEnquiry_Health quotes)
        {
            string result = string.Empty;

            try
            {
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    quotes.AgencyId = lu.AgencyID;
                    quotes.AgencyName = lu.AgencyName;

                    string enqid = quotes.Enquiry_Id;
                    quotes.Enquiry_Id = Health_Service.DecryptGenrateEnquiryId(enqid);
                    string verificationCode = string.Empty;
                    if (Health_Service.SaveGenratedQuotes(quotes, lu.UserId, lu.Password))
                    {
                        result = "/health/helthsearchlist?enquiry_id=" + enqid;

                        //result = "/health/helthsearchlist?policy_type=" + quotes.Policy_Type + "&sum_insured=" + quotes.Sum_Insured + "&self=" + quotes.Self + "&selfage=" + quotes.SelfAge + "&spouse=" + quotes.Spouse + "&spouseage=" + quotes.SpouseAge + "&son=" + quotes.Son + "&son1age=" + quotes.Son1Age + "&son2age=" + quotes.Son2Age + "&son3age=" + quotes.Son3Age + "&son4age=" + quotes.Son4Age + "&daughter=" + quotes.Daughter + "&daughter1age=" + quotes.Daughter1Age + "&daughter2age=" + quotes.Daughter2Age + "&daughter3age=" + quotes.Daughter3Age + "&daughter4age=" + quotes.Daughter4Age + "&father=" + quotes.Father + "&fatherage=" + quotes.FatherAge + "&mother=" + quotes.Mother + "&motherage=" + quotes.MotherAge + "&gender=" + quotes.Gender + "&first_name=" + quotes.First_Name + "&last_name=" + quotes.Last_Name + "&email_id=" + quotes.Email_Id + "&mobile_no=" + quotes.Mobile_No + "&pin_code=" + quotes.Pin_Code + "&policy_type_text=" + quotes.Policy_Type_Text + "";
                        HealthAPIHelper.RemoveHealth_RespoFromCache();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetUrlDetails(string enqid)
        {
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(enqid))
                {
                    NewEnquiry_Health quotes = Health_Service.GetNewEnquiryHealthDetails(Health_Service.DecryptGenrateEnquiryId(enqid));
                    if (quotes != null)
                    {
                        result = "/health/helthsearchlist?policy_type=" + quotes.Policy_Type + "&sum_insured=" + quotes.Sum_Insured + "&self=" + quotes.Self + "&selfage=" + quotes.SelfAge + "&spouse=" + quotes.Spouse + "&spouseage=" + quotes.SpouseAge + "&son=" + quotes.Son + "&son1age=" + quotes.Son1Age + "&son2age=" + quotes.Son2Age + "&son3age=" + quotes.Son3Age + "&son4age=" + quotes.Son4Age + "&daughter=" + quotes.Daughter + "&daughter1age=" + quotes.Daughter1Age + "&daughter2age=" + quotes.Daughter2Age + "&daughter3age=" + quotes.Daughter3Age + "&daughter4age=" + quotes.Daughter4Age + "&father=" + quotes.Father + "&fatherage=" + quotes.FatherAge + "&mother=" + quotes.Mother + "&motherage=" + quotes.MotherAge + "&gender=" + quotes.Gender + "&first_name=" + quotes.First_Name + "&last_name=" + quotes.Last_Name + "&email_id=" + quotes.Email_Id + "&mobile_no=" + quotes.Mobile_No + "&pin_code=" + quotes.Pin_Code + "&policy_type_text=" + quotes.Policy_Type_Text + "";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult FetchHealthApiListing(string enqid, string adult, string child)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(enqid))
                {
                    enqid = Health_Service.DecryptGenrateEnquiryId(enqid);
                    NewEnquiry_Health enqDetail = Health_Service.GetNewEnquiryHealthDetails(enqid);

                    Models.Custom.FrontAgencyModel.Agency_Detail lu = new Models.Custom.FrontAgencyModel.Agency_Detail();
                    if (AdministrationService.IsAgencyLogin(ref lu))
                    {
                        HealthApiResponse apiResponse = HealthAPIHelper.GetHealthApiResponse(lu.UserId, lu.Password, lu.AgencyID, enqDetail);
                        if (apiResponse != null)
                        {
                            if (apiResponse.success)
                            {
                                result = GenrateHealthProductList(apiResponse, adult, child);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        private List<string> GenrateHealthProductList(HealthApiResponse respo, string adult, string child)
        {
            List<string> result = new List<string>();

            try
            {
                if (respo != null)
                {
                    if (respo.success)
                    {
                        StringBuilder sbResult = new StringBuilder();

                        List<decimal> priceOrder = new List<decimal>();
                        List<object> tenureCount = new List<object>();
                        List<object> sumInsuCount = new List<object>();
                        List<object> providerCount = new List<object>();

                        int totalcount = 0;

                        foreach (var item in respo.response.data.OrderBy(p => p.totalPremium))
                        {
                            if (item.insuranceCompany != null && (!string.IsNullOrEmpty(item.totalPremium) && Convert.ToDecimal(item.totalPremium) > 0) && item.suminsured > 0)
                            {
                                if (!providerCount.Contains(item.insuranceCompany)) { providerCount.Add(item.insuranceCompany); }
                                string finalAmount = "0"; string tenure = "1"; string productDetailsId = string.Empty;
                                string sumInsuredId = item.suminsuredId.ToString();
                                if (item.product != null)
                                {
                                    //if (item.product.insureds != null)
                                    //{
                                    //    sumInsuredId = item.product.insureds[0].sumInsuredId.ToString();
                                    //}

                                    productDetailsId = item.product.productDetailId != null ? item.product.productDetailId.ToString() : string.Empty;
                                    tenure = Convert.ToInt32(item.product.period) > 0 ? item.product.period.ToString() : "1";
                                    if (!tenureCount.Contains(tenure)) { tenureCount.Add(tenure); }
                                }

                                if (tenure == "1")
                                {
                                    totalcount = totalcount + 1;
                                    sbResult.Append("<div class='col-sm-12 listbox'>");
                                    //sbResult.Append("<span class='label label-large arrowed-in-right my_badge'><i></i> COVID-19 Covered</span>");
                                    sbResult.Append("<div class='row'>");

                                    sbResult.Append("<div class='col-sm-2 borderright'>");
                                    sbResult.Append("<img src='" + item.logoUrl + "' class='img-responsive img-center margin_top_15 company_logo'  style='margin-top:27px;'/>");
                                    //sbResult.Append("<p style='font-size:12px;font-weight:bold; text-align:center'> " + item.policyType + " </p>");
                                    //sbResult.Append("<p style='font-size:12px;font-weight:bold; text-align:center'>(" + item.insuranceCompany + ")</p>");
                                    sbResult.Append("</div>");

                                    sbResult.Append("<div class='col-sm-8'>");

                                    sbResult.Append("<div class='row borderbotm'>");

                                    sbResult.Append("<div class='col-sm-4 borderright'>");
                                    sbResult.Append("<div class='row'>");

                                    sbResult.Append("<div class='col-sm-2'>");
                                    sbResult.Append("<p><span class='fa fa-bed fonticon'></span></p>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("<div class='col-sm-10'>");
                                    sbResult.Append("<p class='fontdetails'>Room Rent</p>");
                                    if (item.benefits != null && item.benefits.categories != null)
                                    {
                                        BenefitCategory rromrent = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("basic features")).FirstOrDefault();
                                        if (rromrent != null && rromrent.menus.Count > 0)
                                        {
                                            BenefitMenu room_menu = rromrent.menus.Where(p => p.menuName.ToLower().Trim().Contains("room rent eligibility")).FirstOrDefault();
                                            sbResult.Append("<p class='fontdetails'> " + room_menu.details + "</p>");
                                        }
                                        else
                                        {
                                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                        }
                                    }
                                    else
                                    {
                                        sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                    }
                                    sbResult.Append("</div>");

                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("<div class='col-sm-4 borderright'>");
                                    sbResult.Append("<div class='row'>");
                                    sbResult.Append("<div class='col-sm-2'>");
                                    sbResult.Append("<p><span class='fa fa-heartbeat fonticon '></span></p>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("<div class='col-sm-10'>");
                                    sbResult.Append("<p class='fontdetails'>Pre-Existing Disease</p>");
                                    if (item.benefits != null && item.benefits.categories != null)
                                    {
                                        BenefitCategory predisease = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("special features")).FirstOrDefault();
                                        if (predisease != null && predisease.menus.Count > 0)
                                        {
                                            BenefitMenu predeas_menu = predisease.menus.Where(p => p.menuName.ToLower().Trim().Contains("existing disease waiting")).FirstOrDefault();
                                            sbResult.Append("<p class='fontdetails'>  " + predeas_menu.details + "</p>");
                                        }
                                        else
                                        {
                                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                        }
                                    }
                                    else
                                    {
                                        sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                    }
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("<div class='col-sm-4 borderright'>");
                                    sbResult.Append("<div class='row'>");
                                    sbResult.Append("<div class='col-sm-2'>");
                                    sbResult.Append("<p><span class='fa fa-building-o fonticon'></span></p>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("<div class='col-sm-10'>");
                                    sbResult.Append("<p class='fontdetails'>Network Hospitals</p>");
                                    if (item.benefits != null && item.benefits.categories != null)
                                    {
                                        BenefitCategory nethospital = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("usp")).FirstOrDefault();
                                        if (nethospital != null && nethospital.menus.Count > 0)
                                        {
                                            BenefitMenu nothos_menu = nethospital.menus.Where(p => p.menuName.ToLower().Trim().Contains("hospital network")).FirstOrDefault();
                                            sbResult.Append("<p class='fontdetails'>  " + nothos_menu.details + "</p>");
                                        }
                                        else
                                        {
                                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                        }
                                    }
                                    else
                                    {
                                        sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                    }
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");

                                    sbResult.Append("<div class='row' style='margin-top:20px;'>");
                                    sbResult.Append("<div class='col-sm-3'>");
                                    if (item.benefits != null && item.benefits.categories != null)
                                    {
                                        sbResult.Append("<a href='#' class='btnseedelt' data-toggle='modal' data-thiscount='" + totalcount + "' data-target='#ProductDetail_" + (item.product.period + "_" + item.product.productDetailId + "_" + totalcount) + "' style='font-size:17px;color:#e71820'><span class='fa fa-align-justify'></span>&nbsp; View Detail</a>");
                                    }
                                    sbResult.Append("</div>");
                                    string product_del_name = (item.product != null ? item.product.policyName : "- - -") + (item.plan != null && (!string.IsNullOrEmpty(item.plan.ToString()) && item.plan.ToString().ToLower() != "na") ? (" - " + item.plan) : string.Empty);
                                    sbResult.Append("<div class='col-sm-4'>" + product_del_name + "</div>");
                                    sbResult.Append("<div class='col-sm-5'>");
                                    sbResult.Append("<h6 class='checkbox-inline checboxwidth'>");
                                    sbResult.Append("<label class='seeingo chech' style='color: #777777 !important;'>Compare Product");

                                    sbResult.Append("<input type='checkbox' class='submenuclass healthInsuranceProducts' data-identity='hip" + totalcount + "' id='commit' onchange='CompareCheck(this," + totalcount + ")' name='commit' />");
                                    sbResult.Append("<span class='hidden' id='compData" + totalcount + "' data-sum_insured='" + item.suminsured + "' data-plan='" + item.plan + "' data-companyid='" + item.insuranceCompanyId + "' data-productid='" + item.product.productDetailId + "'></span>");

                                    sbResult.Append("<span class='checkmark'></span>");
                                    sbResult.Append("</label>");
                                    sbResult.Append("</h6>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");

                                    sbResult.Append("</div>");

                                    sbResult.Append("<div class='col-sm-2' style='text-transform: capitalize;'>");
                                    sbResult.Append("<div class='row'>");
                                    sbResult.Append("<div class='col-sm-12'>");
                                    //sbResult.Append("<p style=' text-align: center;font-size: 13px;'>Includes GST</p>");

                                    if (!sumInsuCount.Contains(Convert.ToDecimal(item.suminsured))) { sumInsuCount.Add(Convert.ToDecimal(item.suminsured)); }
                                    sbResult.Append("<p style='text-align: center;margin-top: -6px;font-size: 14px;font-weight: bold;'> Cover:&nbsp;<span id='coverCount" + totalcount + "'><span class='fa fa-rupee'></span>" + item.suminsured + " / " + tenure + " Year</span></p>");
                                    //if (!string.IsNullOrEmpty(item.totalPremium))
                                    //{
                                    //    if (item.totalPremium.Contains("."))
                                    //    {
                                    //        finalAmount = item.totalPremium;
                                    //    }
                                    //    else
                                    //    {
                                    //        finalAmount = item.totalPremium + ".00";
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    finalAmount = "0.00";
                                    //}

                                    finalAmount = Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString();

                                    priceOrder.Add(!string.IsNullOrEmpty(finalAmount) ? Math.Ceiling(Convert.ToDecimal(finalAmount)) : 0);

                                    sbResult.Append("<button class='default-btn bookbtn' id='premiumCount" + totalcount + "' data-thiscount='" + totalcount + "' data-suminsuredid='" + sumInsuredId + "' data-productid='" + productDetailsId + "' data-suminsured='" + item.suminsured + "' data-totalamt='" + item.totalPremium + "' data-planname='" + item.plan + "' data-tenure='" + tenure + "' data-companyid='" + item.insuranceCompanyId + "' data-adult='" + adult + "' data-child='" + child + "'>" + CommonClass.IndianMoneyFormat(finalAmount.Replace("INR", "")) + "</button>");
                                    sbResult.Append("<p style='text-align: center;font-size: 12px;'>All Inclusive</p>");
                                    sbResult.Append(GetPerDayAmount(item.totalPremium, tenure));
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");

                                    sbResult.Append("</div>");
                                    sbResult.Append("</div>");

                                    sbResult.Append(BindBenefitsByProduct(item.benefits, (item.product.period + "_" + item.product.productDetailId + "_" + totalcount), totalcount, item.plan, tenure, sumInsuredId, adult, child, item.totalPremium, CommonClass.IndianMoneyFormat(finalAmount.Replace("INR", "")), GetPerDayAmount(item.totalPremium, tenure), product_del_name));
                                }
                            }
                        }

                        result.Add(sbResult.ToString());

                        priceOrder.Sort();
                        result.Add((Math.Ceiling(priceOrder[0])).ToString());
                        result.Add((Math.Ceiling(priceOrder[priceOrder.Count() - 1])).ToString());

                        tenureCount.Sort();
                        result.Add(tenureCount[0] + "-" + tenureCount[tenureCount.Count() - 1]);

                        sumInsuCount.Sort();
                        string sumInsuStr = string.Empty;
                        for (int s = 0; s < sumInsuCount.Count(); s++)
                        {
                            sumInsuStr = string.IsNullOrEmpty(sumInsuStr) ? sumInsuCount[s].ToString() : sumInsuStr + "," + sumInsuCount[s].ToString();
                        }
                        result.Add(sumInsuStr);
                        string providerStr = string.Empty;
                        for (int p = 0; p < providerCount.Count(); p++)
                        {
                            providerStr = string.IsNullOrEmpty(providerStr) ? providerCount[p].ToString() : providerStr + "," + providerCount[p].ToString();
                        }
                        result.Add(providerStr);
                        result.Add((totalcount).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        private string BindBenefitsByProduct(Benefits bnft, string productId, int totalcount, string planname, string tenure, string suminsuredid, string adult, string child, string rowamount, string amount, string perday, string product_del_name)
        {
            StringBuilder benefit = new StringBuilder();
            if (bnft != null && bnft.categories != null)
            {
                benefit.Append("<div class='sidebar-modal'>");
                benefit.Append("<div class='modal top fade' id='ProductDetail_" + productId + "'>");
                benefit.Append("<div class='modal-dialog modal-lg' role='document' style='max-width: 1200px !important;'>");
                benefit.Append("<div class='modal-content'>");
                benefit.Append("<div class='modal-header'>");
                benefit.Append("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'><i class='bx bx-x'></i></span></button>");
                benefit.Append("<h2 class='modal-title' style='padding: 7px;background: #fff;border-bottom: 1px solid #ccc;font-size: 20px;text-align: center;'>Product Details</h2>");
                benefit.Append("</div>");

                benefit.Append("<div class='modal-body'>");
                benefit.Append("<div class='col-sm-12 listbox'>");
                benefit.Append("<div class='row'>");

                benefit.Append("<div class='col-sm-2' style='border-right: 1px solid #ccc;'>");
                benefit.Append("<img src='" + bnft.logoUrl + "' class='img-responsive img-center margin_top_15 company_logo' style='margin-top:27px;'>");
                benefit.Append("</div>");

                benefit.Append("<div class='col-sm-8'>");

                BenefitCategory rromrent = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("basic features")).FirstOrDefault();
                benefit.Append("<div class='row' style='border-bottom: 1px solid #ccc;padding: 3px;'>");
                benefit.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-2'><p><span class='fa fa-bed fonticon'></span></p></div>");
                benefit.Append("<div class='col-sm-10'><p class='fontdetails'>Room Rent</p><p class='fontdetails'>");
                BenefitMenu room_menu = rromrent.menus.Where(p => p.menuName.ToLower().Trim().Contains("room rent eligibility")).FirstOrDefault();
                benefit.Append(room_menu.details);
                benefit.Append("</p></div></div>");
                benefit.Append("</div>");

                BenefitCategory predisease = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("special features")).FirstOrDefault();
                benefit.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-2'><p><span class='fa fa-heartbeat fonticon '></span></p></div>");
                benefit.Append("<div class='col-sm-10'><p class='fontdetails'>Pre-Existing Disease</p><p class='fontdetails'>");
                BenefitMenu predeas_menu = predisease.menus.Where(p => p.menuName.ToLower().Trim().Contains("existing disease waiting")).FirstOrDefault();
                benefit.Append(predeas_menu.details);
                benefit.Append("</p></div></div>");
                benefit.Append("</div>");

                BenefitCategory nethospital = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("usp")).FirstOrDefault();
                benefit.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-2'><p><span class='fa fa-building-o fonticon'></span></p></div>");
                benefit.Append("<div class='col-sm-10'><p class='fontdetails'>Network Hospitals</p><p class='fontdetails'>");
                BenefitMenu nothos_menu = nethospital.menus.Where(p => p.menuName.ToLower().Trim().Contains("hospital network")).FirstOrDefault();
                benefit.Append(nothos_menu.details);
                benefit.Append("</p></div></div>");
                benefit.Append("</div>");

                benefit.Append("</div>");

                benefit.Append("<div class='row' style='margin-top:20px;'>");
                benefit.Append("<div class='col-sm-4'>" + product_del_name + "</div>");
                string b_link = Health_Service.GetHealth_BrochureLink(bnft.productid, bnft.companyId.ToString(), "b");
                if (!string.IsNullOrEmpty(b_link))
                {
                    benefit.Append("<div class='col-sm-4'><a href='" + b_link + "' target='_blank' class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Brochure</a></div>");
                }
                else
                {
                    benefit.Append("<div class='col-sm-4'><span class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Brochure</span></div>");
                }
                string c_link = Health_Service.GetHealth_BrochureLink(bnft.productid, bnft.companyId.ToString(), "c");
                if (!string.IsNullOrEmpty(c_link))
                {
                    benefit.Append("<div class='col-sm-4'><a href='" + c_link + "' target='_blank' class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Policy Clauses</a></div>");
                }
                else
                {
                    benefit.Append("<div class='col-sm-4'><span class='text-danger'><i class='fa fa-external-link'></i>&nbsp;View Policy Clauses</span></div>");
                }
                benefit.Append("</div>");
                benefit.Append("</div>");

                benefit.Append("<div class='col-sm-2' style='text-transform: capitalize;'>");
                benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'><p style='text-align: center;margin-top: -6px;font-size: 14px;font-weight: bold;'> Cover:&nbsp;<span class='fa fa-rupee'></span>" + bnft.sumInsured + " / " + tenure + " Year</p><button class='default-btn bookbtn' id='premiumCount" + totalcount + "' data-thiscount='" + totalcount + "' data-suminsuredid='" + suminsuredid + "' data-productid='" + bnft.productid + "' data-suminsured='" + bnft.sumInsured + "' data-totalamt='" + rowamount + "' data-planname='" + planname + "' data-tenure='" + tenure + "' data-companyid='" + bnft.companyId + "' data-adult='" + adult + "' data-child='" + child + "'>" + amount + "</button><p style='text-align: center;font-size: 12px;'>All Inclusive</p>" + perday + "</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                benefit.Append("<div class='col-sm-12 form-group'>");
                benefit.Append("<div class='row tab'>");
                foreach (var item in bnft.categories)
                {
                    string idusp = item.categoryName.ToLower();
                    benefit.Append("<button class='tablinks' id='TabLinkId_" + item.categoryName.Replace(" ", "_") + totalcount + "' data-tabidname='" + item.categoryName.Replace(" ", "_") + "_" + totalcount + "' onclick='openCity(event, \"" + item.categoryName.Replace(" ", "_") + "_" + totalcount + "\")'>" + item.categoryName + "</button>");
                }
                //benefit.Append("<button class='tablinks active' onclick='openCity(event, \"highlightsh\")'>Highlights</button>");
                //benefit.Append("<button class='tablinks' onclick='openCity(event, \"featuresh\")'>Product Features</button>");
                //benefit.Append("<button class='tablinks' onclick='openCity(event, \"abouth\")'>About Company</button>");
                //benefit.Append("<button class='tablinks' onclick='openCity(event, \"Fareh\")'>Perimum Breakup</button>");
                benefit.Append("</div>");

                BenefitCategory b_usp = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("usp")).FirstOrDefault();
                string usp_id = b_usp.categoryName.Replace(" ", "_") + "_" + totalcount;

                benefit.Append("<div id='" + usp_id + "' class='row tabcontent' style='display:block;'>");
                //benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'>");
                foreach (var usp in b_usp.menus)
                {
                    benefit.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + usp.menuName + " - " + usp.details + "");
                    benefit.Append("<br/><span style='margin-left: 25px;'>(" + usp.destriptions + ")</span></p>");
                }
                //benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                BenefitCategory b_BasicFeatures = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("basic features")).FirstOrDefault();
                string id_b_bf = b_BasicFeatures.categoryName.Replace(" ", "_") + "_" + totalcount;
                benefit.Append("<div id='" + id_b_bf + "' class='row tabcontent'>");
                //benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'>");
                foreach (var bbf in b_BasicFeatures.menus)
                {
                    benefit.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + bbf.menuName + " - " + bbf.details + "");
                    benefit.Append("<br/><span style='margin-left: 25px;'>(" + bbf.destriptions + ")</span></p>");
                }
                //benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                BenefitCategory b_SpecialFeatures = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("special features")).FirstOrDefault();
                string id_b_sf = b_SpecialFeatures.categoryName.Replace(" ", "_") + "_" + totalcount;
                benefit.Append("<div id='" + id_b_sf + "' class='row tabcontent'>");
                //benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'>");
                foreach (var bsf in b_SpecialFeatures.menus)
                {
                    benefit.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + bsf.menuName + " - " + bsf.details + "");
                    benefit.Append("<br/><span style='margin-left: 25px;'>(" + bsf.destriptions + ")</span></p>");
                }
                //benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                BenefitCategory b_EmergencyCoverage = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("emergency coverage")).FirstOrDefault();
                string id_b_ec = b_EmergencyCoverage.categoryName.Replace(" ", "_") + "_" + totalcount;
                benefit.Append("<div id='" + id_b_ec + "' class='row tabcontent'>");
                //benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'>");
                foreach (var bec in b_EmergencyCoverage.menus)
                {
                    benefit.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + bec.menuName + " - " + bec.details + "");
                    benefit.Append("<br/><span style='margin-left: 25px;'>(" + bec.destriptions + ")</span></p>");
                }
                //benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                BenefitCategory b_AdditionalFeatures = bnft.categories.Where(p => p.categoryName.ToLower().Trim().Contains("additional features")).FirstOrDefault();
                string id_b_af = b_AdditionalFeatures.categoryName.Replace(" ", "_") + "_" + totalcount;
                benefit.Append("<div id='" + id_b_af + "' class='row tabcontent'>");
                //benefit.Append("<div class='row'>");
                benefit.Append("<div class='col-sm-12'>");
                foreach (var baf in b_AdditionalFeatures.menus)
                {
                    benefit.Append("<p style='border-bottom: 1px dotted #ccc;padding: 5px;'><span class='fa fa-check'></span>&nbsp;" + baf.menuName + " - " + baf.details + "");
                    benefit.Append("<br/><span style='margin-left: 25px;'>(" + baf.destriptions + ")</span></p>");
                }
                //benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");

                benefit.Append("</div>");
                benefit.Append("<div class='col-sm-12 text-center text-danger'><i class='fa fa-hand-o-right'></i>&nbsp;Note : Above details just for reference, Kindly download Brochure for accurate information</div>");
                benefit.Append("</div>");

                benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");
                benefit.Append("</div>");
            }
            return benefit.ToString();
        }
        private string GetPerDayAmount(string totalPremium, string tenure)
        {
            if (!string.IsNullOrEmpty(totalPremium))
            {
                decimal amount = Convert.ToDecimal(totalPremium);
                if (amount > 0)
                {
                    int totalDaysInAYear = DateTime.IsLeapYear(DateTime.Now.Year) ? 366 : 365;
                    string perDayAmt = (Math.Ceiling((Math.Ceiling(amount / totalDaysInAYear)) / Convert.ToInt32(tenure))).ToString();
                    return "<p style='text-align:center;font-weight: bold;font-size: 14px;'><span class='fa fa-rupee'></span>&nbsp;" + perDayAmt + "/day</p>";
                }
            }

            return string.Empty;
        }
        public JsonResult InsertSearchUrlParaDetail(string enquiryid, string urlpara)
        {
            StringBuilder sbJson = new StringBuilder();
            string enqid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
            urlpara = urlpara.Replace(enquiryid, enqid);
            enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
            Health_Service.RemoveHealth_UrlParaFromCache();

            string respo = HealthProductAPI.GetHealth_RespoFromCache();
            if (!string.IsNullOrEmpty(respo))
            {
                HealthApiResponse resRespo = JsonConvert.DeserializeObject<HealthApiResponse>(respo);
                sbJson.Append("[");
                foreach (var item in resRespo.response.data)
                {
                    string[] spliturlpara = urlpara.Split('&');

                    if (item.product != null)
                    {
                        string productDetailsId = item.product.productDetailId != null ? item.product.productDetailId.ToString() : string.Empty;
                        if (!string.IsNullOrEmpty(productDetailsId))
                        {
                            if ((productDetailsId.Trim().ToLower() == spliturlpara[1].Split('=')[1].Trim().ToLower()) && (item.insuranceCompanyId.ToString().Trim().ToLower() == spliturlpara[2].Split('=')[1].Trim())
                               && (item.plan.ToString().Trim().ToLower() == spliturlpara[7].Split('=')[1].Trim().ToLower()) && (item.suminsured.ToString().Trim().ToLower() == spliturlpara[4].Split('=')[1].Trim()))
                            {
                                sbJson.Append("{\"inquiry_id\":\"" + item.inquiry_id + "\",\"premium\":\"" + item.premium + "\",\"basePremium\":\"" + item.basePremium + "\",\"serviceTax\":\"" + item.serviceTax + "\",\"totalPremium\":\"" + item.totalPremium + "\",\"discountPercent\":" + item.discountPercent + ",");
                                sbJson.Append("\"logoUrl\":\"" + item.logoUrl + "\",\"schemaId\":\"" + item.schemaId + "\",\"insuranceCompany\":\"" + item.insuranceCompany + "\",\"insuranceCompanyId\":" + item.insuranceCompanyId + ",");
                                sbJson.Append("\"policyType\":\"" + item.policyType + "\",\"suminsured\":" + item.suminsured + ",\"plan\":\"" + item.plan + "\",\"ageBracket\":\"" + item.ageBracket + "\",\"suminsuredId\":" + item.suminsuredId + ",\"quoteId\":\"" + item.quoteId + "\",\"id\":" + item.id + ",");

                                sbJson.Append("\"product\":{\"policyTypeName\":\"" + item.product.policyTypeName + "\",\"policyName\":\"" + item.product.policyName + "\",\"postalCode\":" + item.product.postalCode + ",\"period\":" + item.product.period + ",\"productDetailId\":\"" + item.product.productDetailId + "\",");
                                sbJson.Append("\"insureds\":[");

                                int insuredcount = item.product.insureds.Count;
                                int insuredloop = 1;
                                foreach (var insured in item.product.insureds)
                                {
                                    if (insuredloop != insuredcount)
                                    {
                                        sbJson.Append("{\"dob\":\"" + insured.dob + "\",\"sumInsuredId\":" + insured.sumInsuredId + ",\"buyBackPED\":\"" + insured.buyBackPED + "\"},");
                                    }
                                    else
                                    {
                                        if (insuredloop == insuredcount)
                                        {
                                            sbJson.Append("{\"dob\":\"" + insured.dob + "\",\"sumInsuredId\":" + insured.sumInsuredId + ",\"buyBackPED\":\"" + insured.buyBackPED + "\"}");
                                        }
                                    }

                                    insuredloop = insuredloop + 1;
                                }

                                sbJson.Append("]},");

                                sbJson.Append("\"addOnes\":[");
                                if (item.addOnes.Count > 0)
                                {
                                    int addonloopcount = 1;
                                    int totaladdons = item.addOnes.Count;
                                    foreach (var addone in item.addOnes)
                                    {
                                        sbJson.Append("{");
                                        sbJson.Append("\"addOnsId\":" + addone.addOnsId);
                                        sbJson.Append(",\"addOns\":\"" + addone.addOns + "\"");
                                        sbJson.Append(",\"code\":\"" + addone.code + "\"");
                                        sbJson.Append(",\"value\":\"" + addone.value + "\"");
                                        sbJson.Append(",\"calculation\":\"" + addone.calculation + "\"");
                                        sbJson.Append(",\"addOnValue\":\"" + addone.addOnValue + "\"");
                                        sbJson.Append(",\"productDetailID\":\"" + addone.productDetailID + "\"");
                                        sbJson.Append(",\"inquiry_id\":\"" + addone.inquiry_id + "\"");

                                        if (addonloopcount != totaladdons) { sbJson.Append("},"); } else { sbJson.Append("}"); }

                                        addonloopcount = addonloopcount + 1;
                                    }
                                }
                                sbJson.Append("],");

                                sbJson.Append("\"productTerms\":[");

                                int termcount = item.productTerms.Count;
                                int termloop = 1;
                                if (termcount > 0)
                                {
                                    foreach (var terms in item.productTerms)
                                    {
                                        if (termloop != termcount)
                                        {
                                            sbJson.Append("{\"planName\":\"" + terms.planName + "\", \"sumInsuredMin\":" + terms.sumInsuredMin + ", \"sumInsuredMax\":" + terms.sumInsuredMax + ", \"ageGroupMin\":" + terms.ageGroupMin + ",\"ageGroupMax\":" + terms.ageGroupMax + ", \"medical_Tests\":\"" + terms.medical_Tests + "\", \"isCritical_Ilness\":" + terms.isCritical_Ilness.ToString().ToLower() + "},");
                                        }
                                        else
                                        {
                                            if (termloop == termcount)
                                            {
                                                sbJson.Append("{\"planName\":\"" + terms.planName + "\", \"sumInsuredMin\":" + terms.sumInsuredMin + ", \"sumInsuredMax\":" + terms.sumInsuredMax + ", \"ageGroupMin\":" + terms.ageGroupMin + ",\"ageGroupMax\":" + terms.ageGroupMax + ", \"medical_Tests\":\"" + terms.medical_Tests + "\", \"isCritical_Ilness\":" + terms.isCritical_Ilness.ToString().ToLower() + "}");
                                            }
                                        }
                                        termloop = termloop + 1;
                                    }
                                }
                                sbJson.Append("]");
                                sbJson.Append("},");
                            }
                        }
                    }
                }

                sbJson.Append("]");
            }

            return Json(Health_Service.InsertSearchUrlParaDetail(enquiryid, urlpara, sbJson.Replace("},]", "}]").ToString()));
        }
        public JsonResult CheckPincode(string pincode)
        {
            string result = string.Empty;

            try
            {
                string cityCode = Health_Service.CheckPinCodeExist(pincode);
                if (!string.IsNullOrEmpty(cityCode))
                {
                    result = cityCode;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        //public JsonResult GetDataByProductId(string enquiry_id, string productid, string companyid, string suminsured)
        public JsonResult GetDataByProductId(string enquiry_id)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(enquiry_id))
                {
                    enquiry_id = Health_Service.DecryptGenrateEnquiryId(enquiry_id);
                    NewEnquiry_Health enquiryDel = Health_Service.GetNewEnquiryHealthDetails(enquiry_id);

                    if (!string.IsNullOrEmpty(enquiryDel.selectedproductjson) && enquiryDel.selectedproductjson != "[]" && enquiryDel.selectedproductjson != "[{}]")
                    {
                        string[] spliturlpara = enquiryDel.searchurl.Split('&');

                        string urlproduct = spliturlpara[1].Split('=')[1];
                        string urlcompanyid = spliturlpara[2].Split('=')[1];
                        string urlsuminsuredid = spliturlpara[3].Split('=')[1];
                        string urlsuminsured = spliturlpara[4].Split('=')[1];
                        string urladult = spliturlpara[5].Split('=')[1];
                        string urlchild = spliturlpara[6].Split('=')[1];
                        string urlplanname = spliturlpara[7].Split('=')[1];
                        string urltotalpremamt = spliturlpara[8].Split('=')[1];
                        string urltenure = spliturlpara[9].Split('=')[1];

                        StringBuilder sbStr = new StringBuilder();
                        sbStr.Append("<div class='row form-group'>");

                        sbStr.Append("<div class='col-sm-12 form-group'  style='border: 1px solid #ccc;'>");
                        sbStr.Append("<div class='row form-group' style='border-bottom: 1px solid #ccc;'>");
                        sbStr.Append("<div class='col-sm-12'><p class='filterhed'>Select Tenure</p></div>");
                        //sbStr.Append("<div class='col-sm-3' style='border-left: 1px solid #ccc;display:none;'><p class='filterhed'>Select Addon</p></div>");
                        sbStr.Append("</div>");

                        sbStr.Append("<div class='row'>");
                        sbStr.Append("<div class='col-sm-12' style='padding: 9px; border-right: none; border-left: none;'>");
                        sbStr.Append("<div class='row form-group'>");

                        List<Datum> productList = new List<Datum>();
                        int loopCount = 1;
                        string selectedPrice = string.Empty;
                        int selectedperiod = 1;

                        CreateHealth_Proposal perposalDel = Health_Service.GetProposalDetail(enquiry_id);
                        List<Datum> resRespo = JsonConvert.DeserializeObject<List<Datum>>(enquiryDel.selectedproductjson);
                        if (resRespo != null && resRespo.Count > 0)
                        {
                            foreach (var item in resRespo.OrderBy(p => p.product.period))
                            {
                                if (item.product != null)
                                {
                                    string productDetailsId = item.product.productDetailId != null ? item.product.productDetailId.ToString() : string.Empty;
                                    if (!string.IsNullOrEmpty(productDetailsId))
                                    {
                                        sbStr.Append("<div class='col-sm-4'>");
                                        sbStr.Append("<div class='panel panel-default text-center rounded' style='border: 1px solid #ddd;'>");
                                        sbStr.Append("<div class='panel-heading'><h3 class='panel-title'>" + item.product.period + " Year Premium</h3><p class='no-margin-bottom color-purple font-bold'>Discount : " + (item.discountPercent > 0 ? item.discountPercent.ToString() : "0%") + "</p></div>");
                                        sbStr.Append("<div class='panel-body no-padding'><h3 class='color-orange'>");

                                        if (!string.IsNullOrEmpty(perposalDel.periodfor))
                                        {
                                            if (perposalDel.productid == item.product.productDetailId.ToString() && perposalDel.periodfor == item.product.period.ToString())
                                            {
                                                selectedperiod = item.product.period;
                                                selectedPrice = item.totalPremium;
                                                result.Add(CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")));
                                                sbStr.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")) + "' data-premium='" + item.totalPremium + "' data-basepremium='" + item.basePremium + "' data-periodfor='" + item.product.period + "' data-totalamt='" + Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString() + "' data-discount='" + item.discountPercent + "' data-servicetax='" + item.serviceTax + "' checked='checked'/><br>");
                                            }
                                            else
                                            {
                                                sbStr.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")) + "' data-premium='" + item.totalPremium + "' data-basepremium='" + item.basePremium + "' data-periodfor='" + item.product.period + "' data-totalamt='" + Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString() + "' data-discount='" + item.discountPercent + "' data-servicetax='" + item.serviceTax + "'/><br>");
                                            }
                                        }
                                        else
                                        {
                                            if (item.product.period.ToString().Trim() == urltenure.Trim())
                                            {
                                                selectedperiod = item.product.period;
                                                selectedPrice = item.totalPremium;
                                                result.Add(CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")));
                                                sbStr.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")) + "' data-premium='" + item.totalPremium + "' data-basepremium='" + item.basePremium + "' data-periodfor='" + item.product.period + "' data-totalamt='" + Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString() + "' data-discount='" + item.discountPercent + "' data-servicetax='" + item.serviceTax + "' checked='checked'/><br>");
                                            }
                                            else
                                            {
                                                sbStr.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")) + "' data-premium='" + item.totalPremium + "' data-basepremium='" + item.basePremium + "' data-periodfor='" + item.product.period + "' data-totalamt='" + Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString() + "' data-discount='" + item.discountPercent + "' data-servicetax='" + item.serviceTax + "'/><br>");
                                            }
                                        }

                                        sbStr.Append("<strong>" + CommonClass.IndianMoneyFormat(item.totalPremium.Replace("INR", "")) + "</strong></h3>");
                                        sbStr.Append("</div>");
                                        sbStr.Append("</div>");
                                        sbStr.Append("</div>");

                                        loopCount = loopCount + 1;
                                    }
                                }
                            }
                        }

                        sbStr.Append("</div>");
                        sbStr.Append("</div>");//close col-sm-9  

                        sbStr.Append("</div>");
                        //sbStr.Append("</div>");
                        sbStr.Append("<div class='row form-group'><div class='col-sm-12' style='border: 1px solid #ccc;'><p class='filterhed'>Select Addon</p></div>");

                        sbStr.Append("<div class='col-sm-12'");
                        sbStr.Append("<div class='col-sm-3'>");
                        sbStr.Append("<div class='row' style='margin-top: 10px;' id='divAddonSection'>");

                        if (resRespo != null && resRespo.Count > 0)
                        {
                            foreach (var item in resRespo.OrderBy(p => p.product.period))
                            {
                                if (item.product.period == selectedperiod)
                                {
                                    if (item.addOnes != null && item.addOnes.Count > 0)
                                    {
                                        foreach (var addone in item.addOnes)
                                        {
                                            if (Convert.ToDecimal(addone.addOnValue) > 0)
                                            {
                                                sbStr.Append(Health_Controller.AddonsHtml(addone.addOnsId, addone.addOns, addone.code, addone.value, addone.calculation.Trim(), addone.addOnValue, addone.productDetailID, addone.inquiry_id, item.basePremium, item.premium, item.discountPercent, item.serviceTax, item.totalPremium, item.product.period));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        sbStr.Append("</div>");
                        sbStr.Append("</div>");
                        sbStr.Append("</div>");

                        sbStr.Append("</div>");

                        result.Add(sbStr.ToString());

                        StringBuilder sbProductDel = new StringBuilder();
                        foreach (var item in resRespo)
                        {
                            if (item.totalPremium == selectedPrice)
                            {
                                sbProductDel.Append("<div class='col-sm-12'> <input type='hidden' id='hdnProductId' value='" + item.insuranceCompanyId + "' />");
                                sbProductDel.Append("<img src='" + item.logoUrl + "' style='margin-left: auto;margin-right: auto;display: block;'>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='col-sm-12'>");

                                sbProductDel.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Product  Name</label></div>");
                                sbProductDel.Append("<div class='col-sm-6'><label>" + item.product.policyName + "</label></div>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Plan Type</label></div>");
                                sbProductDel.Append("<div class='col-sm-6'><label>" + item.plan + "</label></div>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Policy Type</label></div>");
                                sbProductDel.Append("<div class='col-sm-6'><label>" + item.policyType + "</label></div>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Member</label></div>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Adult-" + urladult + ", Child-" + urlchild + "</label></div>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                                sbProductDel.Append("<div class='col-sm-6'><label>Sum Insred</label></div>");
                                sbProductDel.Append("<div class='col-sm-6'><label>" + CommonClass.IndianMoneyFormat(item.suminsured.ToString().Replace("INR", "")) + "</label></div>");
                                sbProductDel.Append("</div>");

                                sbProductDel.Append("<div class='col-sm-12' style=\"margin-top: 2%;margin-bottom: 2%;font-size: 20px;text-align: center;color: red;\"><b class='text-center'>Premium Breakup</b></div>");

                                sbProductDel.Append("<div id='divPremiumBreakup' class=\"row\">");

                                sbProductDel.Append(Health_Service.SetPremiumBreakup(item.basePremium, item.premium, item.discountPercent.ToString(), item.serviceTax, item.totalPremium, item.product.period.ToString(), enquiry_id));
                            }
                        }
                        sbProductDel.Append("</div>");

                        sbProductDel.Append("</div>");
                        result.Add(sbProductDel.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetNSaveSelectedPlanData(CreateHealth_Proposal proposal, List<AddonChecked> addonlist)
        {
            proposal.enquiryid = Health_Service.DecryptGenrateEnquiryId(proposal.enquiryid);
            SearchUrlParaMeter para = GetSearchUrlParaMeter(proposal.enquiryid);
            if (!string.IsNullOrEmpty(para.productid))
            {
                proposal.productid = para.productid;
                proposal.companyid = para.companyid;
                proposal.suminsuredid = para.suminsuredid;
                proposal.suminsured = para.suminsured;
            }
            NewEnquiry_Health heathEnqDetail = Health_Service.GetNewEnquiryHealthDetails(proposal.enquiryid);
            if (!string.IsNullOrEmpty(heathEnqDetail.Enquiry_Id))
            {
                if (!string.IsNullOrEmpty(heathEnqDetail.selectedproductjson))
                {
                    List<Datum> selectedHeath = JsonConvert.DeserializeObject<List<Datum>>(heathEnqDetail.selectedproductjson);
                    if (selectedHeath.Count > 0)
                    {
                        foreach (var selheath in selectedHeath)
                        {
                            if (selheath.product.period.ToString() == proposal.periodfor && selheath.totalPremium == proposal.premium)
                            {
                                proposal.row_premium = selheath.premium;
                                proposal.row_basePremium = selheath.basePremium;
                                proposal.row_serviceTax = selheath.serviceTax;
                                proposal.row_totalPremium = selheath.totalPremium;
                                proposal.row_discountPercent = selheath.discountPercent.ToString();
                                proposal.row_insuranceCompany = selheath.insuranceCompany;
                                proposal.row_policyType = selheath.policyType;
                                proposal.row_planname = selheath.plan;
                                proposal.row_policyTypeName = selheath.product.policyTypeName;
                                proposal.row_policyName = selheath.product.policyName;
                            }
                        }
                    }
                }
            }

            bool isSuccess = Health_Service.InsertSelectedPlanProposal(proposal);
            CreateHealth_Proposal proposalDetail = new CreateHealth_Proposal();
            if (isSuccess)
            {
                bool isAddonUpdated = Health_Service.UpdateSelectedAddons(addonlist);
                proposalDetail = Health_Service.GetProposalDetailsBySearchParaMeter(para);
                if (string.IsNullOrEmpty(proposalDetail.firstname))
                {
                    NewEnquiry_Health henq = Health_Service.GetNewEnquiryHealthDetails(proposal.enquiryid);

                    proposalDetail.firstname = henq.First_Name;
                    proposalDetail.lastname = henq.Last_Name;
                    proposalDetail.gender = henq.Gender;
                    proposalDetail.pincode = henq.Pin_Code;
                    proposalDetail.mobile = henq.Mobile_No;
                    proposalDetail.emailid = henq.Email_Id;
                    proposalDetail.isproposerinsured = henq.Is_Term_Accepted;
                }
            }
            return Json(proposalDetail);
        }
        public JsonResult GetNSaveProposerDetailsData(CreateHealth_Proposal proposal)
        {
            proposal.enquiryid = Health_Service.DecryptGenrateEnquiryId(proposal.enquiryid);
            CreateHealth_Proposal result = new CreateHealth_Proposal();

            SearchUrlParaMeter para = GetSearchUrlParaMeter(proposal.enquiryid);
            if (!string.IsNullOrEmpty(para.productid))
            {
                proposal.productid = para.productid;
                proposal.companyid = para.companyid;
                proposal.suminsured = para.suminsured;
            }
            return Json(Health_Service.InsertProposerDetailsProposal(proposal));
        }
        public JsonResult GetT_Health_Proposal(CreateHealth_Proposal proposal)
        {
            proposal.enquiryid = Health_Service.DecryptGenrateEnquiryId(proposal.enquiryid);
            SearchUrlParaMeter para = GetSearchUrlParaMeter(proposal.enquiryid);
            if (!string.IsNullOrEmpty(para.productid))
            {
                proposal.productid = para.productid;
                proposal.companyid = para.companyid;
                proposal.suminsured = para.suminsured;
            }
            return Json(Health_Service.GetProposalDetail(proposal));
        }
        public JsonResult BindInsuredDetails(CreateHealth_Proposal proposal)
        {
            List<string> result = new List<string>();
            try
            {
                proposal.enquiryid = Health_Service.DecryptGenrateEnquiryId(proposal.enquiryid);
                SearchUrlParaMeter para = GetSearchUrlParaMeter(proposal.enquiryid);
                if (!string.IsNullOrEmpty(para.productid))
                {
                    proposal.productid = para.productid;
                    proposal.companyid = para.companyid;
                    proposal.suminsured = para.suminsured;
                }

                string companyId = proposal.companyid;
                if (!string.IsNullOrEmpty(companyId))
                {
                    int totalPerson = 0;
                    NewEnquiry_Health enquiryDel = Health_Service.GetNewEnquiryHealthDetails(proposal.enquiryid);
                    List<RelationMaster> relationList = Health_Service.GetRelation(proposal.companyid, proposal.productid, enquiryDel.Policy_Type_Text);
                    CreateHealth_Proposal proposalDetail = Health_Service.GetProposalDetail(proposal);
                    List<Health_Insured> insuredDel = Health_Service.GetInsuredDetails(para);
                    int total_Adults = 0; int total_Childs = 0;

                    if (enquiryDel.Self) { total_Adults = total_Adults + 1; }
                    if (enquiryDel.Spouse) { total_Adults = total_Adults + 1; }
                    if (enquiryDel.Father) { total_Adults = total_Adults + 1; }
                    if (enquiryDel.Mother) { total_Adults = total_Adults + 1; }
                    if (enquiryDel.Son)
                    {
                        if (enquiryDel.Son1Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Son2Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Son3Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Son4Age > 0) { total_Childs = total_Childs + 1; }
                    }
                    if (enquiryDel.Daughter)
                    {
                        if (enquiryDel.Daughter1Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Daughter2Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Daughter3Age > 0) { total_Childs = total_Childs + 1; }
                        if (enquiryDel.Daughter4Age > 0) { total_Childs = total_Childs + 1; }
                    }

                    totalPerson = total_Adults + total_Childs;

                    StringBuilder sbStr = new StringBuilder();
                    sbStr.Append("<div class='col-sm-12'>");
                    sbStr.Append("<h6>Insured Details</h6 >");////<h6>Insured Details</ h6 >
                    int i = 1;
                    for (i = 1; i <= total_Adults; i++)
                    {
                        Health_Insured tempInsured = new Health_Insured();
                        if (insuredDel != null && insuredDel.Count > 0)
                        {
                            tempInsured = insuredDel[i - 1];
                        }

                        sbStr.Append("<div class='row form-group' " + (totalPerson > 1 ? "style='padding:10px; border: 1px dotted #ccc;border-radius: 5px;'" : "style='padding:10px;'") + ">");
                        if (!string.IsNullOrEmpty(tempInsured.id.ToString()))
                        {
                            sbStr.Append("<input type='hidden' id='hdnInsuredId_" + i + "' value='" + tempInsured.id + "' />");
                        }
                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6'><div class='row'>");
                        sbStr.Append("<div class='col-sm-7 form-validation'><select class='form-control formcontlbord' name='ddlRelation_" + i + "' id='ddlRelation_" + i + "' onchange='InsuredTitleChange(" + i + ");'>");
                        sbStr.Append("<option value=''>Relation</option>");
                        foreach (var item in relationList)
                        {
                            if (tempInsured.id > 0)
                            {
                                sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "' " + (tempInsured.relationid == item.RelationshipId ? "selected" : "") + ">" + item.RelationshipName + "</option>");
                            }
                            else
                            {
                                if (proposalDetail.isproposerinsured && i == 1)
                                {
                                    if (item.RelationshipName.ToString().ToLower().Contains("self"))
                                    {
                                        sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "' " + (item.RelationshipName.ToString().ToLower().Contains("self") ? "selected" : "") + " > " + item.RelationshipName + " </ option > ");
                                    }
                                    else
                                    {
                                        sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "'>" + item.RelationshipName + "</option>");
                                    }
                                }
                                else
                                {
                                    sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "'>" + item.RelationshipName + "</option>");
                                }
                            }

                            //sbStr.Append("<option value='" + item.RelationshipId + "'>" + item.RelationshipName + "</option>");
                        }
                        sbStr.Append("</select></div>");
                        sbStr.Append("<div class='col-sm-5 form-validation'><select name='ddlTitle_" + i + "' id='ddlTitle_" + i + "' class='form-control formcontlbord'><option value=''>Title</option>");

                        if (proposalDetail.isproposerinsured && i == 1)
                        {
                            if (!string.IsNullOrEmpty(tempInsured.title))
                            {
                                sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                            }
                            else
                            {
                                sbStr.Append("<option value='Mr.' " + (proposalDetail.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (proposalDetail.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (proposalDetail.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");

                                //sbStr.Append("<option value='Mr.' selected='selected'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.title))
                            {
                                sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                            }
                            else
                            {
                                sbStr.Append("<option value='Mr.'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                            }
                        }

                        sbStr.Append("</select></div>");
                        sbStr.Append("</div></div>");

                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6 form-validation'>");

                        if (proposalDetail.isproposerinsured)
                        {
                            if (i == 1)
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)'  value='" + proposalDetail.firstname + "' />");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempInsured.firstname))
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)' value='" + tempInsured.firstname + "' />");
                                }
                                else
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)' />");
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.firstname))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)' value='" + tempInsured.firstname + "' />");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)' />");
                            }
                        }

                        //if (!proposalDetail.isproposerinsured)
                        //{
                        //    if (!string.IsNullOrEmpty(tempInsured.firstname))
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' value='" + tempInsured.firstname + "' />");
                        //    }
                        //    else
                        //    {
                        //        if (i == 1 && proposalDetail.isproposerinsured)
                        //        {
                        //            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name'  value='" + proposalDetail.firstname + "' />");
                        //        }
                        //        else
                        //        {
                        //            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' />");
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    if (i == 1 && proposalDetail.isproposerinsured)
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name'  value='" + proposalDetail.firstname + "' />");
                        //    }
                        //    else
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' />");
                        //    }
                        //}

                        sbStr.Append("</div><div class='col-sm-6 form-validation'>");

                        if (proposalDetail.isproposerinsured)
                        {
                            if (i == 1)
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)' value='" + proposalDetail.lastname + "' />");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempInsured.firstname))
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)'  value='" + tempInsured.lastname + "' />");
                                }
                                else
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)' />");
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.firstname))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)'  value='" + tempInsured.lastname + "' />");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)' />");
                            }
                        }

                        sbStr.Append("</div></div></div></div></div>");

                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-4 form-validation'>");

                        if (proposalDetail.isproposerinsured)
                        {
                            if (i == 1)
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + proposalDetail.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempInsured.dob))
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                                }
                                else
                                {
                                    sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.dob))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                            }
                        }

                        //if (!proposalDetail.isproposerinsured)
                        //{
                        //    if (!string.IsNullOrEmpty(tempInsured.dob))
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        //    }
                        //    else
                        //    {
                        //        if (i == 1 && proposalDetail.isproposerinsured)
                        //        {
                        //            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + proposalDetail.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        //        }
                        //        else
                        //        {
                        //            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    if (i == 1 && proposalDetail.isproposerinsured)
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + proposalDetail.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        //    }
                        //    else
                        //    {
                        //        sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        //    }
                        //}


                        if (tempInsured.height > 0)
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input type='text' onkeypress='return isNumberValidationPrevent(event);' class='form-control formcontlbord' id='txtInHeight_" + i + "' placeholder='height (cm)' maxlength='3' minlength='2' value='" + tempInsured.height + "' onchange='return CheckHeight(" + i + ");' /><span class='cmposion'>CM</span></div>");
                            sbStr.Append("<span class='notheight_" + i + "' style='color: rgb(0, 163, 0); display:none;'></span>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input type='text' onkeypress='return isNumberValidationPrevent(event);' class='form-control formcontlbord' id='txtInHeight_" + i + "' placeholder='height (cm)' maxlength='3' minlength='2' onchange='return CheckHeight(" + i + ");' /><span class='cmposion'>CM</span></div>");
                            sbStr.Append("<span class='notheight_" + i + "' style='color: rgb(0, 163, 0); display:none;'></span>");
                        }

                        if (tempInsured.weight > 0)
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input name='txtInWeight_" + i + "' onkeypress='return isNumberValidationPrevent(event);' id='txtInWeight_" + i + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' value='" + tempInsured.weight + "' onchange='return CheckWeight(" + i + ");'><span class='cmposion'>KG</span></div>");
                            sbStr.Append("<span class='notweight_" + i + "' style='color: rgb(0, 163, 0);display:none;'></span>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input name='txtInWeight_" + i + "' onkeypress='return isNumberValidationPrevent(event);' id='txtInWeight_" + i + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' onchange='return CheckWeight(" + i + ");'><span class='cmposion'>KG</span></div>");
                            sbStr.Append("<span class='notweight_" + i + "' style='color: rgb(0, 163, 0);display:none;'></span>");
                        }

                        sbStr.Append("</div></div>");

                        sbStr.Append("<div class='col-sm-12'>");
                        sbStr.Append("<div class='row' style='margin-top: 12px;'>");
                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlOccupation_" + i + "' id='ddlOccupation_" + i + "'>");
                        sbStr.Append("<option value=''>Select Occupation</option>");

                        string occupationStr = Health_Service.GetOccupationDdl(proposalDetail.companyid, (insuredDel.Count > 0 ? insuredDel[i - 1].occupationId : string.Empty));
                        sbStr.Append(occupationStr);

                        sbStr.Append("</select></div>");

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlIllness_" + i + "' id='ddlIllness_" + i + "' onchange='return FillExistingIllness(" + i + ");'>");
                        sbStr.Append("<option value=''>Existing Illness</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].illness == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].illness == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");

                        if (insuredDel.Count > 0 && insuredDel[i - 1].illness == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation ExistingIllness_" + i + "'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + i + "' placeholder='Describe Existing Illness' value='" + insuredDel[i - 1].illnessdesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow ExistingIllness_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + i + "' placeholder='Describe Existing Illness' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageManualLabour_" + i + "' id='ddlEngageManualLabour_" + i + "' onchange='return FillEngageManualLabour(" + i + ");'>");
                        sbStr.Append("<option value=''>Is Engage Manual Labour</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].engageManualLabour == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].engageManualLabour == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");

                        if (insuredDel.Count > 0 && insuredDel[i - 1].engageManualLabour == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation EngageManualLabour_" + i + "'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour' value='" + insuredDel[i - 1].engageManualLabourDesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow EngageManualLabour_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageWinterSports_" + i + "' id='ddlEngageWinterSports_" + i + "' onchange='return FillEngageWinterSports(" + i + ");'>");
                        sbStr.Append("<option value=''>Is Engage Winter Sports</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].engageWinterSports == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[i - 1].engageWinterSports == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");
                        if (insuredDel.Count > 0 && insuredDel[i - 1].engageWinterSports == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation EngageWinterSports_" + i + "'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' value='" + insuredDel[i - 1].engageWinterSportDesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow EngageWinterSports_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        sbStr.Append("</div></div>");

                        sbStr.Append("</div>");
                    }

                    for (int j = i; j <= totalPerson; j++)
                    {
                        Health_Insured tempInsured = new Health_Insured();
                        if (insuredDel != null && insuredDel.Count > 0)
                        {
                            tempInsured = insuredDel[j - 1];
                        }

                        sbStr.Append("<div class='row form-group' " + (totalPerson > 1 ? "style='padding:10px; border: 1px dotted #ccc;border-radius: 5px;'" : "style='padding:10px;'") + ">");
                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6'><div class='row'>");
                        sbStr.Append("<div class='col-sm-7 form-validation'><select class='form-control formcontlbord' name='ddlRelation_" + j + "' id='ddlRelation_" + j + "' onchange='InsuredTitleChange(" + j + ");'>");
                        sbStr.Append("<option value=''>Relation</option>");
                        foreach (var item in relationList)
                        {
                            if (tempInsured.id > 0)
                            {
                                sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "' " + (tempInsured.relationid == item.RelationshipId ? "selected" : "") + ">" + item.RelationshipName + "</option>");
                            }
                            else
                            {
                                sbStr.Append("<option value='" + item.RelationshipId + "' data-title='" + item.Title + "'>" + item.RelationshipName + "</option>");
                            }
                        }
                        sbStr.Append("</select></div>");
                        sbStr.Append("<div class='col-sm-5 form-validation'><select name='ddlTitle_" + j + "' id='ddlTitle_" + j + "' class='form-control formcontlbord'><option value=''>Title</option>");

                        if (!string.IsNullOrEmpty(tempInsured.title))
                        {
                            sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                        }
                        else
                        {
                            sbStr.Append("<option value='Mr.'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                        }

                        sbStr.Append("</select></div>");
                        sbStr.Append("</div></div>");

                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6 form-validation'>");

                        if (!string.IsNullOrEmpty(tempInsured.firstname))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + j + "' placeholder='first name' onkeypress='return IsAlphabet(event)' value='" + tempInsured.firstname + "' />");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + j + "' placeholder='first name' onkeypress='return IsAlphabet(event)' />");
                        }

                        sbStr.Append("</div><div class='col-sm-6 form-validation'>");
                        if (!string.IsNullOrEmpty(tempInsured.lastname))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + j + "' placeholder='last name' onkeypress='return IsAlphabet(event)'  value='" + tempInsured.lastname + "' />");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + j + "' placeholder='last name' onkeypress='return IsAlphabet(event)' />");
                        }

                        sbStr.Append("</div></div></div></div></div>");

                        sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-4 form-validation'>");
                        if (!string.IsNullOrEmpty(tempInsured.dob))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + j + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateChildNomineeAge(\"txtInDOB_" + j + "\",\"notchilddobvalid" + j + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notchilddobvalid" + j + "'></span></div>");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + j + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateChildNomineeAge(\"txtInDOB_" + j + "\",\"notchilddobvalid" + j + "\",\"position: absolute;top: 40px;right: 15px;font-size: 10px;\")' /><span class='notchilddobvalid" + j + "'></span></div>");
                        }

                        if (tempInsured.height > 0)
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' type='text' class='form-control formcontlbord' id='txtInHeight_" + j + "' placeholder='height (cm)' maxlength='3' minlength='2' value='" + tempInsured.height + "' onchange='return CheckHeight(" + j + ");' /><span class='cmposion'>CM</span></div>");
                            sbStr.Append("<span class='notheight_" + j + "' style='color: rgb(0, 163, 0); display:none;'></span>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' type='text' class='form-control formcontlbord' id='txtInHeight_" + j + "' placeholder='height (cm)' maxlength='3' minlength='2' onchange='return CheckHeight(" + j + ");'/><span class='cmposion'>CM</span></div>");
                            sbStr.Append("<span class='notheight_" + j + "' style='color: rgb(0, 163, 0); display:none;'></span>");
                        }

                        if (tempInsured.weight > 0)
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' name='txtInWeight_" + j + "' id='txtInWeight_" + j + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' value='" + tempInsured.weight + "' onchange='return CheckWeight(" + j + ");'><span class='cmposion'>KG</span></div>");
                            sbStr.Append("<span class='notweight_" + j + "' style='color: rgb(0, 163, 0);display:none;'></span>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' name='txtInWeight_" + j + "' id='txtInWeight_" + j + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' onchange='return CheckWeight(" + j + ");'><span class='cmposion'>KG</span></div>");
                            sbStr.Append("<span class='notweight_" + j + "' style='color: rgb(0, 163, 0);display:none;'></span>");
                        }

                        sbStr.Append("</div></div>");

                        sbStr.Append("<div class='col-sm-12'>");
                        sbStr.Append("<div class='row' style='margin-top: 12px;'>");
                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlOccupation_" + j + "' id='ddlOccupation_" + j + "'>");
                        sbStr.Append("<option value=''>Select Occupation</option>");

                        string occupationStr = Health_Service.GetOccupationDdl(proposalDetail.companyid, (insuredDel.Count > 0 ? insuredDel[j - 1].occupationId : string.Empty));
                        sbStr.Append(occupationStr);

                        sbStr.Append("</select></div>");

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlIllness_" + j + "' id='ddlIllness_" + j + "' onchange='return FillExistingIllness(" + j + ");'>");
                        sbStr.Append("<option value=''>Existing Illness</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].illness == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].illness == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");

                        if (insuredDel.Count > 0 && insuredDel[j - 1].illness == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation ExistingIllness_" + j + "'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + j + "' placeholder='Describe Existing Illness' value='" + insuredDel[j - 1].illnessdesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow ExistingIllness_" + j + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + j + "' placeholder='Describe Existing Illness' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageManualLabour_" + j + "' id='ddlEngageManualLabour_" + j + "'>");
                        sbStr.Append("<option value=''>Is Engage Manual Labour</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].engageManualLabour == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].engageManualLabour == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");

                        if (insuredDel.Count > 0 && insuredDel[j - 1].engageManualLabour == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation EngageManualLabour_" + j + "'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + j + "' placeholder='Describe Engage Manual Labour' value='" + insuredDel[j - 1].engageManualLabourDesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow EngageManualLabour_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + j + "' placeholder='Describe Engage Manual Labour' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }

                        sbStr.Append("<div class='col-sm-3 form-validation'>");
                        sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageWinterSports_" + j + "' id='ddlEngageWinterSports_" + j + "'>");
                        sbStr.Append("<option value=''>Is Engage Winter Sports</option>");
                        sbStr.Append("<option value='true' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].engageWinterSports == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredDel.Count > 0 ? (insuredDel[j - 1].engageWinterSports == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                        sbStr.Append("</select></div>");
                        if (insuredDel.Count > 0 && insuredDel[j - 1].engageWinterSports == true)
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation EngageWinterSports_" + j + "'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + j + "' placeholder='Describe Engage Winter Sports' value='" + insuredDel[j - 1].engageWinterSportDesc + "' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        else
                        {
                            sbStr.Append("<div class='col-sm-6 form-validation notshow EngageWinterSports_" + j + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + j + "' placeholder='Describe Engage Winter Sports' onkeypress='return isAlphaNumericWithSpace(event)'></div>");
                        }
                        sbStr.Append("</div></div>");

                        sbStr.Append("</div>");
                    }

                    sbStr.Append("</div>");

                    //Nominee Section
                    sbStr.Append("<div class=\"col-sm-12\" style=\"border-top: 1px solid #ccc;\">");
                    sbStr.Append("<div class=\"row form-group\" style=\"padding:10px;\">");

                    sbStr.Append("<div class=\"col-sm-12\"><h6>Nominee Details</h6></div>");
                    sbStr.Append("<div class=\"col-sm-3 form-validation\">");
                    sbStr.Append("<select class=\"form-control formcontlbord\" name=\"ddlnRelation\" id=\"ddlnRelation\">");

                    List<ApiNominee_Common> nomineeList = Health_Service.GetNominee_CommonDetails(proposal.companyid, "Nominee");
                    sbStr.Append("<option value = \"\" >Relation </option >");
                    if (nomineeList != null && nomineeList.Count > 0)
                    {
                        foreach (var nominee in nomineeList)
                        {
                            sbStr.Append("<option value = \"" + nominee.RelationshipCode + "\" " + (proposalDetail.nRelation == "" + nominee.RelationshipCode + "" ? "selected" : "") + " data-title='" + nominee.Title + "'> " + nominee.Relationship + " </option >");
                        }
                        //sbStr.Append("<option value = \"Mother\" " + (proposalDetail.nRelation == "Mother" ? "selected" : "") + "> Mother </option >");
                        //sbStr.Append("<option value = \"Brother\" " + (proposalDetail.nRelation == "Brother" ? "selected" : "") + "> Brother </option >");
                        //sbStr.Append("<option value = \"Sister\" " + (proposalDetail.nRelation == "Sister" ? "selected" : "") + "> Sister </option >");
                        //sbStr.Append("<option value = \"Grandfather\" " + (proposalDetail.nRelation == "Grandfather" ? "selected" : "") + "> Grandfather </option >");
                        //sbStr.Append("<option value = \"GrandMother\" " + (proposalDetail.nRelation == "GrandMother" ? "selected" : "") + "> GrandMother </option >");
                    }
                    sbStr.Append("</select >");
                    //sbStr.Append("</select >");
                    sbStr.Append("</div>");
                    sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                    sbStr.Append("<select name = \"ddlnTitle\" id=\"ddlnTitle\" class=\"form-control formcontlbord\">");
                    sbStr.Append("<option value =''>Title</option>");
                    if (proposalDetail.ispropnominee && string.IsNullOrEmpty(proposalDetail.nTitle))
                    {
                        sbStr.Append("<option value = \"Mr.\" " + (proposalDetail.title.ToLower() == "mr." ? "selected" : "") + ">Mr.</option>");
                        sbStr.Append("<option value = \"Mrs.\" " + (proposalDetail.title.ToLower() == "mrs." ? "selected" : "") + "> Mrs.</option>");
                        sbStr.Append("<option value=\"Ms.\" " + (proposalDetail.title.ToLower() == "ms." ? "selected" : "") + ">Ms.</option>");
                    }
                    else
                    {
                        sbStr.Append("<option value = \"Mr.\" " + (proposalDetail.nTitle.ToLower() == "mr." ? "selected" : "") + ">Mr.</option>");
                        sbStr.Append("<option value = \"Mrs.\" " + (proposalDetail.nTitle.ToLower() == "mrs." ? "selected" : "") + "> Mrs.</option>");
                        sbStr.Append("<option value=\"Ms.\" " + (proposalDetail.nTitle.ToLower() == "ms." ? "selected" : "") + ">Ms.</option>");
                    }
                    sbStr.Append("</select>");
                    sbStr.Append("</div>");

                    sbStr.Append("<div class=\"col-sm-3 form-validation\">");
                    if (proposalDetail.ispropnominee && string.IsNullOrEmpty(proposalDetail.nFirstName))
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nFirstName\"  onkeypress=\"return IsAlphabet(event)\" placeholder=\"First Name\" value=\"" + proposalDetail.firstname + "\">");
                    }
                    else
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nFirstName\" placeholder=\"First Name\"  onkeypress=\"return IsAlphabet(event)\" value=\"" + proposalDetail.nFirstName + "\">");
                    }
                    sbStr.Append("</div>");
                    sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                    if (proposalDetail.ispropnominee && string.IsNullOrEmpty(proposalDetail.nLastName))
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nLastName\" placeholder=\"Last Name\"  onkeypress=\"return IsAlphabet(event)\" value=\"" + proposalDetail.lastname + "\">");
                    }
                    else
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nLastName\" placeholder=\"Last Name\"  onkeypress=\"return IsAlphabet(event)\" value=\"" + proposalDetail.nLastName + "\">");
                    }
                    sbStr.Append("</div>");
                    sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                    if (proposalDetail.ispropnominee && string.IsNullOrEmpty(proposalDetail.nDOB))
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord DynamicCommanDateWithYr\" id=\"nDOB\" maxlength='10' readonly autocomplete=\"off\" placeholder=\"Date Of Birth\" onchange=\"return CalculateNomineeAge('nDOB','ndobvalid','position: absolute;top: 40px;right: 15px;font-size: 10px;')\" value=\"" + proposalDetail.dob + "\"><span class=\"ndobvalid\"></span>");
                    }
                    else
                    {
                        sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord DynamicCommanDateWithYr\" id=\"nDOB\" maxlength='10' readonly autocomplete=\"off\" placeholder=\"Date Of Birth\" onchange=\"return CalculateNomineeAge('nDOB','ndobvalid','position: absolute;top: 40px;right: 15px;font-size: 10px;')\" value=\"" + proposalDetail.nDOB + "\"><span class=\"ndobvalid\"></span>");
                    }
                    sbStr.Append("</div>");

                    sbStr.Append("</div>");
                    sbStr.Append("</div>");
                    //Nominee Section End


                    sbStr.Append("<div class='col-sm-12'>");
                    sbStr.Append("<button class='next1 default-btn pull-right btnwdth designbtn' id='btnInsuredNext' onclick='return NextInsuredDetails();'>NEXT</button>");
                    sbStr.Append("<button class='previous2 prvbtn btn btn-success btnwdth pull-left prev designbtn' id='btnInsuredPrev' onclick='return GoToProposerDetail()'>PREVIOUS</button>");
                    sbStr.Append("</div>");
                    sbStr.Append("</div>");

                    result.Add(sbStr.ToString());
                    result.Add(totalPerson.ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetNSaveInsuredDetail(CreateHealth_Proposal proposal)
        {
            proposal.enquiryid = Health_Service.DecryptGenrateEnquiryId(proposal.enquiryid);
            SearchUrlParaMeter para = GetSearchUrlParaMeter(proposal.enquiryid);
            if (!string.IsNullOrEmpty(para.productid))
            {
                proposal.productid = para.productid;
                proposal.companyid = para.companyid;
                proposal.suminsured = para.suminsured;
            }

            bool isSuccess = Health_Service.InsertInsuredDetails(proposal);
            return Json(GetMedicalHistoryDetail(proposal));
        }
        private string GetMedicalHistoryDetail(CreateHealth_Proposal proposal)
        {
            StringBuilder sbStr = new StringBuilder();

            try
            {
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    HealthQuestionaries questionar = HealthAPIHelper.GetHealthQuestionaries(proposal, lu.UserId, lu.Password, lu.AgencyID);
                    if (questionar != null)
                    {
                        if (questionar.success)
                        {
                            if (questionar.response.Count > 0)
                            {
                                List<Health_Insured> insuredDel = Health_Service.GetInsuredDetail(proposal.enquiryid.ToString());

                                sbStr.Append("<div class='col-sm-12'>");
                                sbStr.Append("<div class='tabmedi row'>");
                                int insuredloop = 0;
                                foreach (var insured in insuredDel)
                                {
                                    sbStr.Append("<button class='tablinks butntab " + (insuredloop == 0 ? "active" : "") + "' id='tab" + (insured.firstname + "_" + insured.lastname) + "' onclick='openCity(event, \"" + (insured.firstname + "_" + insured.lastname).Trim() + "\")'>" + insured.firstname + " " + insured.lastname + "</button>");
                                    insuredloop = insuredloop + 1;
                                }
                                sbStr.Append("</div>");
                                sbStr.Append("</div>");

                                sbStr.Append("<input type=\"hidden\" id=\"hdnQuestionCount\" value=\"" + questionar.response.Count + "\" />");
                                sbStr.Append("<input type=\"hidden\" id=\"hdnInsuredCount\" value=\"" + insuredDel.Count + "\" />");
                                insuredloop = 0;
                                foreach (var insured in insuredDel)
                                {
                                    sbStr.Append("<input type=\"hidden\" id=\"hdnInsuredName_" + insuredloop + "\" value=\"" + insured.firstname + "," + insured.lastname + "\" />");
                                    sbStr.Append("<div id='" + insured.firstname + "_" + insured.lastname + "' class='tabcontent " + (insuredloop == 0 ? "active" : "") + "' " + (insuredloop == 0 ? "style='display:block;'" : "style='display:none;'") + ">");
                                    sbStr.Append("<div class='col-sm-12 text-center' style='padding: 10px;text-align: left;'>");

                                    foreach (var item in questionar.response)
                                    {
                                        if (!string.IsNullOrEmpty(item.response))
                                        {
                                            item.questionCode = item.questionCode.ToString().Trim();

                                            bool issubquestionexist = item.subQuestion.Count > 0 ? true : false;
                                            bool lagaobeta = false;
                                            sbStr.Append("<div class='row' style='border-bottom: 1px solid #ccc;padding: 5px;text-align: left;'>");
                                            sbStr.Append("<div class='col-sm-10' id='divMainQuestion_" + insuredloop + "_" + item.questionCode + "' data-subquestionexist='" + issubquestionexist + "' >");
                                            sbStr.Append(item.questionDescription);
                                            sbStr.Append("</div>");
                                            sbStr.Append("<div class='col-sm-2 colsmMargin'>");
                                            if (item.response.ToLower() == "boolean")
                                            {
                                                sbStr.Append("<label class='seeingo' style='color: #777777!important;float: right;'><input type='checkbox' class='mainquestionar' data-insuredcount='" + insuredloop + "' id='chkQues_" + insuredloop + "_" + item.questionCode + "' name='chkQues_" + insuredloop + "_" + item.questionCode + "' data-repotype='" + item.response + "' data-isrequired='" + item.required + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "'><span class='checkmark'></span></label>");
                                            }
                                            else if (item.response.ToLower() == "datetime")
                                            {
                                                sbStr.Append("<input type='text' placeholder='DD/MM/YYYY' maxlength='7' class='mainquestionar questiondatetime' id='dateQues_" + insuredloop + "_" + item.questionCode + "' name='dateQues_" + insuredloop + "_" + item.questionCode + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-isrequired='" + item.required + "' data-quescode='" + item.questionCode + "' style='width: 175px;' data-repotype='" + item.response + "' style='width: 100%;'/>");
                                            }
                                            else if (item.response.ToLower() == "date")
                                            {
                                                sbStr.Append("<input type='text' placeholder='MM/YYYY' maxlength='7' class='mainquestionar questiondate' id='dateQues_" + insuredloop + "_" + item.questionCode + "' name='dateQues_" + insuredloop + "_" + item.questionCode + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-isrequired='" + item.required + "' data-quescode='" + item.questionCode + "' style='width: 175px;' data-repotype='" + item.response + "' style='width: 100%;'/>");
                                            }
                                            else if (item.response.ToLower() == "integer")
                                            {
                                                sbStr.Append("<input type='text' class='mainquestionar' id='intQues_" + insuredloop + "_" + item.questionCode + "' name='intQues_" + insuredloop + "_" + item.questionCode + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "' data-isrequired='" + item.required + "' data-repotype='" + item.response + "' style='width: 175px;' onkeypress='return isNumberValidationPrevent(event);' />");
                                            }
                                            else if (item.response.ToLower() == "free text")
                                            {
                                                sbStr.Append("<textarea type='text' class='mainquestionar' id='strQues_" + insuredloop + "_" + item.questionCode + "' name='strQues_" + insuredloop + "_" + item.questionCode + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "' data-isrequired='" + item.required + "' data-repotype='" + item.response + "' style='width: 175px;'></textarea>");
                                            }
                                            else if (item.response.ToLower() == "year")
                                            {
                                                sbStr.Append("<input type='text' class='mainquestionar' id='strQues_" + insuredloop + "_" + item.questionCode + "' name='strQues_" + insuredloop + "_" + item.questionCode + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "' data-isrequired='" + item.required + "' data-repotype='" + item.response + "' style='width: 175px;' maxlength='4' placeholder='enter year (yyyy)' onkeypress='return isNumberValidationPrevent(event);' />");
                                            }
                                            else if (item.response.ToLower() == "dropdown")
                                            {
                                                sbStr.Append("<select class='custdropdown mainquestionar' id='ddlQues_" + insuredloop + "_" + item.questionCode + "' name='ddlQues_" + insuredloop + "_" + item.questionCode + "'  data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "' data-repotype='" + item.response + "'>");
                                                sbStr.Append("<option>Select</option>");
                                                foreach (var itemdrop in item.dropDownData)
                                                {
                                                    sbStr.Append("<option value='" + itemdrop.value + "'>" + itemdrop.text + "</option>");
                                                }
                                                sbStr.Append("</select>");
                                            }
                                            sbStr.Append("</div>");

                                            foreach (var subitem in item.subQuestion)
                                            {
                                                if (!string.IsNullOrEmpty(subitem.response))
                                                {
                                                    bool issub2questionexist = subitem.subQuestion.Count > 0 ? true : false;
                                                    sbStr.Append("<div class='col-sm-12 DivSubQuestionSection_" + insuredloop + "_" + item.questionCode + "' style='padding-top: 5px;display:none;' " + (lagaobeta == false ? "style='padding-top: 10px;'" : "") + ">");
                                                    sbStr.Append("<div class='row'>");
                                                    sbStr.Append("<div class='col-sm-10'>" + subitem.questionDescription + "</div>");
                                                    sbStr.Append("<div class='col-sm-2'>");


                                                    if (subitem.response.ToLower() == "boolean")
                                                    {
                                                        sbStr.Append("<label class='seeingo' style='color: #777777!important;    float: right;'>");
                                                        //sbStr.Append("<label class='seeingo' style='color: #777777!important;'><input type='checkbox' class='submenuclass' id='chkSubQues_" + subitem.questionCode + "' name='chkQues_" + item.questionCode + "'><span class='checkmark'></span></label>");
                                                        sbStr.Append("<input type='checkbox' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' class='submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "'><span class='subcheckmark'></span>");
                                                        sbStr.Append("</label>");
                                                    }
                                                    else if (subitem.response.ToLower() == "datetime")
                                                    {
                                                        sbStr.Append("<input type='text' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' placeholder='DD/MM/YYYY' maxlength='7' class='questiondatetime submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "'  style='width: 175px;'/>");
                                                    }
                                                    else if (subitem.response.ToLower() == "date")
                                                    {
                                                        sbStr.Append("<input type='text' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' placeholder='MM/YYYY' maxlength='7' class='questiondate submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-isrequired='" + subitem.required + "' data-isrequired='" + subitem.required + "' data-subquestionsetcode='" + subitem.questionSetCode + "'  style='width: 175px;'/>");
                                                    }
                                                    else if (subitem.response.ToLower() == "integer")
                                                    {
                                                        sbStr.Append("<input type='text' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' class='submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "' style='width: 175px;' onkeypress='return isNumberValidationPrevent(event);' />");
                                                    }
                                                    else if (subitem.response.ToLower() == "free text")
                                                    {
                                                        sbStr.Append("<textarea id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' class='submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "' style='width: 175px;'></textarea>");
                                                    }
                                                    else if (subitem.response.ToLower() == "year")
                                                    {
                                                        sbStr.Append("<input type='text' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' class='submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "' style='width: 175px;' maxlength='4' placeholder='enter year (yyyy)' onkeypress='return isNumberValidationPrevent(event);' />");
                                                    }
                                                    else if (subitem.response.ToLower() == "dropdown")
                                                    {
                                                        sbStr.Append("<select id='" + insuredloop + "_" + item.questionCode + "_" + (subitem.questionCode != null ? subitem.questionCode.Trim() : subitem.questionSetCode.Trim().Replace(" ", "_")) + "' class='custdropdown submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-isrequired='" + subitem.required + "' data-subquestioncode='" + (subitem.questionCode != null ? subitem.questionCode.Trim() : "") + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "'>");
                                                        sbStr.Append("<option>Select</option>");
                                                        foreach (var subitemdrop in subitem.dropDownData)
                                                        {
                                                            sbStr.Append("<option value='" + subitemdrop.value + "'>" + subitemdrop.text + "</option>");
                                                        }
                                                        sbStr.Append("</select>");
                                                    }

                                                    sbStr.Append("</div>");
                                                    sbStr.Append("</div>");
                                                    sbStr.Append("</div>");
                                                    lagaobeta = true;
                                                }
                                            }

                                            sbStr.Append("</div>");
                                        }
                                    }

                                    sbStr.Append("</div>");
                                    sbStr.Append("</div>");
                                    insuredloop = insuredloop + 1;
                                }
                                //<style>.ui-datepicker-calendar {display: none!important;}</style>                                
                            }
                            else
                            {
                                sbStr.Append("<input type=\"hidden\" id=\"hdnQuestionCount\" value=\"" + questionar.response.Count + "\" />");
                                sbStr.Append("<p style=\"color: red\">No medical details available!</p>");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            sbStr.Append("<script>$('.questiondatetime').datepicker({ changeMonth: true,maxDate: -0, changeYear: true, showButtonPanel: true, dateFormat: 'dd/mm/yy',yearRange: '1922:' + (new Date).getFullYear(), onClose: function (dateText, inst) {var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val(); var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val(); $(this).datepicker('setDate', new Date(year, month, 1));} });$('.questiondate').datepicker({ changeMonth: true,maxDate: -0, changeYear: true, showButtonPanel: true, dateFormat: 'mm/yy',yearRange: '1922:' + (new Date).getFullYear(), onClose: function (dateText, inst) {var month = $('#ui-datepicker-div .ui-datepicker-month :selected').val(); var year = $('#ui-datepicker-div .ui-datepicker-year :selected').val(); $(this).datepicker('setDate', new Date(year, month, 1));} });</script>");

            return sbStr.ToString();
        }
        public JsonResult CompareInsuranceProducts(List<CompareRequest> compReqArray, string enqId)
        {
            string strResult = string.Empty;
            try
            {
                Models.Custom.FrontAgencyModel.Agency_Detail lu = new Models.Custom.FrontAgencyModel.Agency_Detail();

                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    StringBuilder sbStr = new StringBuilder();
                    List<string> result = HealthAPIHelper.GetCompareInsuranceProducts(compReqArray, enqId, lu.UserId, lu.Password, lu.AgencyID);

                    if (result != null && result.Count > 0)
                    {
                        //List<BenefitsRoot> respoList = new List<BenefitsRoot>();

                        sbStr.Append("<div class='row'>");
                        sbStr.Append("<div class='col-sm-12 no-padding'>");
                        sbStr.Append("<div class='table-responsive'>");
                        sbStr.Append("<table class='table no-bottom-space text-center compare_table'>");
                        sbStr.Append("<tbody>");

                        List<BenefitsRoot> respoList = new List<BenefitsRoot>();
                        foreach (var item in result)
                        {
                            BenefitsRoot respo = JsonConvert.DeserializeObject<BenefitsRoot>(item);
                            respoList.Add(respo);
                        }
                        int respoCount = respoList.Count;
                        int CategoryCount = 0;

                        sbStr.Append("<tr>");
                        sbStr.Append("<td style='width: 200px;'> <span id='img_quote' style='cursor:pointer' class='img_quote'> <label><i class='fa fa-envelope' aria-hidden='true'></i></label> <span img_id='#health_product_comparison' id='btn_Email' page='Mediclaim Product Comparison' class='btn_send_quote_with_img'>Email</span> </span> </br><span class='mailform hidden'><input type='text' class='form-control' id='mailaddress' placeholder='email id..' style='height: 30px; '><span class='btn btn-danger btn-sm sendbtn' id='compareemai' style='margin-top: 15px;'>Send mail</span> <span class='btn btn-danger btn-sm temsend hidden' style='margin-top: 15px;'> Sending mail.. <i class='fa fa-spinner fa-pulse'></i></span></span> </td>");
                        for (int i = 0; i < respoCount; i++)
                        {
                            sbStr.Append("<td class='compare_" + (i + 1) + "'> <img src='http://api.seeinsured.com" + respoList[i].response.logoUrl + "' class='img-responsive img-center img-thumbnail compare_" + (i + 1) + "_product_image' height='80' width='80' alt=''> <h5 class='no-margin padding-top-20 compare_" + i + "_product_name'>" + (!string.IsNullOrEmpty(respoList[i].response.productName) ? (respoList[i].response.productName + (!string.IsNullOrEmpty(respoList[i].response.plan) ? " - " + respoList[i].response.plan : string.Empty)) : string.Empty) + "</h5><h6>Cover : " + compReqArray[i].cover + "</h6><h6>Premium : " + compReqArray[i].premium + "/-</h6></td>");
                        }
                        sbStr.Append("</tr>");

                        List<List<string>> FullMenuAndSubMenuList = new List<List<string>>();

                        for (int i = 0; i < respoCount; i++)
                        {
                            if (respoList[i].message.ToLower() == "success")
                            {
                                CategoryCount = respoList[i].response.categories.Count;
                                for (int j = 0; j < respoList[i].response.categories.Count; j++)
                                {
                                    int MenuAndSubMenuCount = respoList[i].response.categories[j].menus.Count + 1;
                                    List<string> MenuAndSubMenuList = new List<string>();
                                    MenuAndSubMenuList.Add(respoList[i].response.categories[j].categoryName);
                                    for (int k = 0; k < MenuAndSubMenuCount - 1; k++)
                                    {
                                        MenuAndSubMenuList.Add(respoList[i].response.categories[j].menus[k].menuName);
                                    }
                                    FullMenuAndSubMenuList.Add(MenuAndSubMenuList);
                                }
                                break;
                            }
                        }

                        for (int i = 0; i < CategoryCount; i++)
                        {
                            int rCount = 0;
                            sbStr.Append("<tr>");
                            sbStr.Append("<td colspan='4' class='compare_feature_heading font-bold' style='background-color: #1f77b4;color: #fff;'>" + FullMenuAndSubMenuList[i][0] + "</td>");
                            sbStr.Append("</tr>");
                            for (int j = 1; j < FullMenuAndSubMenuList[i].Count; j++)
                            {
                                sbStr.Append("<tr>");

                                sbStr.Append("<td>" + FullMenuAndSubMenuList[i][j] + "</td>");

                                for (int k = 0; k < respoCount; k++)
                                {
                                    sbStr.Append("<td title='" + respoList[rCount].response.categories[i].menus[j - 1].destriptions + "'>" + respoList[k].response.categories[i].menus[j - 1].details + "</td>");
                                }
                                sbStr.Append("</tr>");
                            }
                            sbStr.Append("</tr>");
                            rCount++;
                        }

                        sbStr.Append("</tr>");



                        sbStr.Append("</tbody>");
                        sbStr.Append("</table>");
                        sbStr.Append("</div>");
                        sbStr.Append("</div>");
                        sbStr.Append("</div>");

                        strResult = sbStr.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(strResult);
        }
        public JsonResult CompareInsuranceProductsEmail(List<CompareRequest> compReqArray, string enqId, string toemail)
        {
            string strResult = string.Empty;
            Models.Custom.FrontAgencyModel.Agency_Detail lu = new Models.Custom.FrontAgencyModel.Agency_Detail();

            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                StringBuilder sbStr = new StringBuilder();
                List<string> result = HealthAPIHelper.GetCompareInsuranceProducts(compReqArray, enqId, lu.UserId, lu.Password, lu.AgencyID);

                if (result != null && result.Count > 0)
                {
                    //List<BenefitsRoot> respoList = new List<BenefitsRoot>();


                    sbStr.Append(" <table  style='width: 900px; border-collapse: initial; border: 1px solid #eb233a;font-family: Verdana,Geneva,sans-serif;font-size: 12px; border-spacing: 0px; padding: 0px;'>");
                    sbStr.Append("<tbody>");

                    List<BenefitsRoot> respoList = new List<BenefitsRoot>();
                    foreach (var item in result)
                    {
                        BenefitsRoot respo = JsonConvert.DeserializeObject<BenefitsRoot>(item);
                        respoList.Add(respo);
                    }
                    int respoCount = respoList.Count;
                    int CategoryCount = 0;

                    sbStr.Append("<tr>");
                    sbStr.Append("<td style='width: 200px;border-bottom: 1px solid #b3b3b3;'></td>");
                    for (int i = 0; i < respoCount; i++)
                    {
                        sbStr.Append("<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 5px;' class='compare_" + (i + 1) + "'> <img src='http://api.seeinsured.com" + respoList[i].response.logoUrl + "' class='img-responsive img-center img-thumbnail compare_" + (i + 1) + "_product_image' height='80' width='80' alt=''> <h5 style='margin-bottom: 0px;margin-top: 0px;' class='no-margin padding-top-20 compare_" + i + "_product_name'>" + (!string.IsNullOrEmpty(respoList[i].response.productName) ? (respoList[i].response.productName + (!string.IsNullOrEmpty(respoList[i].response.plan) ? " - " + respoList[i].response.plan : string.Empty)) : string.Empty) + "</h5><h6 style='margin-bottom: 0px;margin-top: 0px;'>Cover : " + compReqArray[i].cover + "</h6><h6 style='margin-bottom: 0px;margin-top: 0px;'>Premium : " + compReqArray[i].premium + "/-</h6></td>");
                    }
                    sbStr.Append("</tr>");

                    List<List<string>> FullMenuAndSubMenuList = new List<List<string>>();

                    for (int i = 0; i < respoCount; i++)
                    {
                        if (respoList[i].message.ToLower() == "success")
                        {
                            CategoryCount = respoList[i].response.categories.Count;
                            for (int j = 0; j < respoList[i].response.categories.Count; j++)
                            {
                                int MenuAndSubMenuCount = respoList[i].response.categories[j].menus.Count + 1;
                                List<string> MenuAndSubMenuList = new List<string>();
                                MenuAndSubMenuList.Add(respoList[i].response.categories[j].categoryName);
                                for (int k = 0; k < MenuAndSubMenuCount - 1; k++)
                                {
                                    MenuAndSubMenuList.Add(respoList[i].response.categories[j].menus[k].menuName);
                                }
                                FullMenuAndSubMenuList.Add(MenuAndSubMenuList);
                            }
                            break;
                        }
                    }

                    for (int i = 0; i < CategoryCount; i++)
                    {
                        int rCount = 0;
                        sbStr.Append("<tr>");
                        sbStr.Append("<td colspan='4' style='text-align:left; border-right:1px solid #b3b3b3;background-color: #1f77b4; border-bottom:1px solid #b3b3b3;color:#fff;padding: 5px;' class='compare_feature_heading font-bold' style='background-color: #1f77b4;color: #fff;'>" + FullMenuAndSubMenuList[i][0] + "</td>");
                        sbStr.Append("</tr>");
                        for (int j = 1; j < FullMenuAndSubMenuList[i].Count; j++)
                        {
                            sbStr.Append("<tr>");

                            sbStr.Append("<td style='border-right:1px solid #b3b3b3;background-color: #f5f5f5; border-bottom:1px solid #b3b3b3;color:#484848;padding: 5px;'>" + FullMenuAndSubMenuList[i][j] + "</td>");

                            for (int k = 0; k < respoCount; k++)
                            {
                                sbStr.Append("<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 5px;' title='" + respoList[rCount].response.categories[i].menus[j - 1].destriptions + "'>" + respoList[k].response.categories[i].menus[j - 1].details + "</td>");
                            }
                            sbStr.Append("</tr>");
                        }
                        sbStr.Append("</tr>");
                        rCount++;
                    }

                    sbStr.Append("</tr>");



                    sbStr.Append("</tbody>");
                    sbStr.Append("</table>");

                    strResult = sbStr.ToString();
                }
            }

            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                // string AgencyEmail = lu.BusinessEmailID;
                string AgencyName = lu.AgencyName;
                Email.SendCompareDetail(strResult, toemail, AgencyName);
            }
            return Json(strResult);

        }
        public JsonResult CreateHealthPerposal(string enquiry_id, List<PerposalQuestion> medhistory)
        {
            string result = string.Empty;
            string encyEnquiryid = enquiry_id;
            enquiry_id = Health_Service.DecryptGenrateEnquiryId(enquiry_id);
            if (Health_Service.UpdateQuestionerDetails(enquiry_id, medhistory))
            {
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    HealthPerposalResponse response = HealthAPIHelper.GetHealthPerposalResponse(lu.UserId, lu.Password, lu.AgencyID, enquiry_id, encyEnquiryid);
                    if (response != null)
                    {
                        if (response.success)
                        {
                            if (response.response.data != null)
                            {
                                string referenceId = response.response.data.referenceId != null ? response.response.data.referenceId.ToString() : string.Empty;
                                string totalPremium = response.response.data.totalPremium != null ? response.response.data.totalPremium.ToString() : string.Empty;

                                if (Health_Service.InsertPerposalDetails(enquiry_id, referenceId, response.response.data.premium, totalPremium, response.response.data.proposalNum, response.response.data.productDetailId.ToString(), response.response.data.companyId.ToString(), response.response.urls, response.response.data.serviceTax.ToString()))
                                {
                                    bool ispaylinkCReated = Health_Service.Create_PaymentLinkByProposal(lu.UserId, lu.Password, lu.AgencyID, enquiry_id);

                                    result = "<div class='modal-header'><h5 class='modal-title' id='exampleModalLabel'>Proposal Details</h5></div>"
                                + "<div class='modal-body'><p class='text-center' style='color: green;'><span class='fa fa-check-circle' style='font-size: 100px;'></span></p><h6 class='text-center' style='word-wrap: break-word!important;color: green;'>Proposal Created Successfully.</h6></div><div class='modal-footer'><button type='button' class='btn btn-primary' onclick='CheckPerposalDetailSummary();'>Go To Proposal Summary</button>"
                                 + "</div>";
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(response.response.message))
                                {
                                    result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger' style='word-break: break-word;'>" + response.response.message + "</p></div>";
                                }
                            }
                        }
                        else if (response.status == 400)
                        {
                            result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger' style='word-break: break-word;'>" + response.title + "</p></div><div class='modal-footer'><a type='button' class='btn btn-primary' href='/health/helthsearch'>Search Again</a></div>";
                        }
                        else
                        {
                            result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>" + response.message + "</p></div><div class='modal-footer'><a type='button' class='btn btn-primary' href='/health/helthsearch'>Search Again</a></div>";

                            //if (response.error_code > 0)
                            //{
                            //    result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>" + response.response.message + "</p></div>";
                            //    //<div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button></div>
                            //}
                            //else
                            //{
                            //    result = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>"+ response.response.message + "</p></div><div class='modal-footer'><a type='button' class='btn btn-primary' href='/health/helthsearch'>Search Again</a></div>";
                            //}
                        }
                    }
                }
            }

            return Json(result);
        }
        public JsonResult SetChangedPremiumBreakup(string totalpremium, string enquiryid)
        {
            List<string> strPremuim = new List<string>();
            try
            {
                enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
                NewEnquiry_Health enquiryDel = Health_Service.GetNewEnquiryHealthDetails(enquiryid);
                List<Datum> resRespo = JsonConvert.DeserializeObject<List<Datum>>(enquiryDel.selectedproductjson);

                foreach (var item in resRespo)
                {
                    if (item.totalPremium == totalpremium)
                    {
                        strPremuim.Add(Health_Service.SetPremiumBreakup(item.basePremium, item.premium, item.discountPercent.ToString(), item.serviceTax, item.totalPremium, item.product.period.ToString(), enquiryid));

                        string addonsHtml = string.Empty;
                        if (item.addOnes != null && item.addOnes.Count > 0)
                        {
                            foreach (var addone in item.addOnes)
                            {
                                if (Convert.ToDecimal(addone.addOnValue) > 0)
                                {
                                    addonsHtml = addonsHtml + Health_Controller.AddonsHtml(addone.addOnsId, addone.addOns, addone.code, addone.value, addone.calculation, addone.addOnValue, addone.productDetailID, addone.inquiry_id, item.basePremium, item.premium, item.discountPercent, item.serviceTax, item.totalPremium, item.product.period);
                                }
                            }
                        }
                        strPremuim.Add(addonsHtml);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(strPremuim);
        }
        public JsonResult AddLessAddon(string[] addonlist)
        {
            bool issuccess = Health_Service.AddLessAddon(addonlist);
            return Json(Health_Service.SetPremiumBreakup(addonlist[9], addonlist[10], addonlist[11], addonlist[12], addonlist[13], addonlist[14], addonlist[7]));
        }

        #endregion

        #region [Proposal details section]
        public JsonResult GetAnnualIncomeDetails(string enquiryid)
        {
            string result = string.Empty;

            try
            {
                result = Health_Service.GetAnnualIncomeDetails(enquiryid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetproposalAllDetails(string enquiryid)
        {
            List<string> result = new List<string>();

            try
            {
                enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
                if (!string.IsNullOrEmpty(enquiryid))
                {
                    insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                    bool isAgencyDel = AdministrationService.IsAgencyLogin(ref lu);

                    NewEnquiry_Health enquiryDel = Health_Service.GetNewEnquiryHealthDetails(enquiryid);
                    if (isAgencyDel == true && (lu.AgencyID == enquiryDel.AgencyId || lu.Type != "Agency"))
                    {
                        result = GetProposalDetailbyId(enquiryid);
                    }
                    else
                    {
                        result.Add("logout");
                        result.Add(Config.WebsiteUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }

        public JsonResult Admin_GetproposalAllDetails(string enquiryid)
        {
            return Json(GetProposalDetailbyId(enquiryid));
        }

        private List<string> GetProposalDetailbyId(string enquiryid)
        {
            List<string> result = new List<string>();
            try
            {
                NewEnquiry_Health enquiryDel = Health_Service.GetNewEnquiryHealthDetails(enquiryid);
                CreateHealth_Proposal perposalDel = Health_Service.GetProposalDetail(enquiryid);
                List<Health_Insured> insuredDetail = Health_Service.GetInsuredDetail(enquiryid);
                Health_Payment payment = Health_Service.GetHealthPaymentTableDetails(enquiryid);
                List<Datum> resRespo = JsonConvert.DeserializeObject<List<Datum>>(enquiryDel.selectedproductjson);
                List<AddonChecked> addonList = Health_Service.GetSelectedAddLessAddon(enquiryid);

                string[] spliturlpara = enquiryDel.searchurl.Split('&');

                string urlproduct = spliturlpara[1].Split('=')[1];
                string urlcompanyid = spliturlpara[2].Split('=')[1];
                string urlsuminsuredid = spliturlpara[3].Split('=')[1];
                string urlsuminsured = spliturlpara[4].Split('=')[1];
                string urladult = spliturlpara[5].Split('=')[1];
                string urlchild = spliturlpara[6].Split('=')[1];
                string urlplanname = spliturlpara[7].Split('=')[1];
                string urltotalpremamt = spliturlpara[8].Split('=')[1];

                string planType = string.Empty; string productName = string.Empty; string period = string.Empty;
                string suminsured = string.Empty;
                foreach (var item in resRespo)
                {
                    if (item.totalPremium == perposalDel.premium)
                    {
                        planType = item.plan;
                        productName = item.product.policyName;
                        period = item.product.period.ToString();
                        suminsured = item.suminsured.ToString();
                        break;
                    }
                }

                StringBuilder sbLeft = new StringBuilder();
                sbLeft.Append("<table class='table table-responsive table-striped summary_data_table'>");
                sbLeft.Append("<tbody>");
                sbLeft.Append("<tr> <td>Policy Type</td> <td><strong>:</strong> " + enquiryDel.Policy_Type_Text.ToUpper() + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Plan Type</td> <td><strong>:</strong> " + (!string.IsNullOrEmpty(planType) ? planType : "- - -") + "</td> </tr>");
                sbLeft.Append("<tr> <td>Product Name</td> <td><strong>:</strong> " + productName + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Policy For</td> <td><strong>:</strong> Adult-" + urladult + ", Child-" + urlchild + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Policy Period</td> <td><strong>:</strong> " + period + " year(s)</td> </tr>");
                //sbLeft.Append("<tr> <td>Policy Start Date</td> <td><strong>:</strong> 14-Jan-2021</td> </tr>");
                //sbLeft.Append("<tr> <td>Policy End Date</td> <td><strong>:</strong> 13-Jan-2022</td> </tr>");
                //sbLeft.Append("<tr> <td>Sum Insured</td> <td><strong>:</strong> <i class='fa fa-inr'></i> 4,00,000</td> </tr>");
                //sbLeft.Append("<tr> <td>Base Premium</td> <td><strong>:</strong> <i class='fa fa-inr'></i> 5,485</td> </tr>");
                //sbLeft.Append("<tr> <td>Service Tax</td> <td><strong>:</strong> <i class='fa fa-inr'></i> 987</td> </tr>");
                //sbLeft.Append("<tr> <td>Total Premium Amount</td> <td><strong>:</strong> <i class='fa fa-inr'></i> 6,472</td> </tr>");
                sbLeft.Append("</tbody>");
                sbLeft.Append("</table>");

                result.Add(sbLeft.ToString());

                sbLeft = new StringBuilder();
                sbLeft.Append("<table class='table table-responsive table-striped summary_data_table'>");
                sbLeft.Append("<tbody>");
                sbLeft.Append("<tr> <td>Gender</td> <td><strong>:</strong> " + (perposalDel.title == "mr." ? "MALE" : "FEMALE") + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Name</td> <td><strong>:</strong> " + (perposalDel.title == "mr." ? "Mr." : perposalDel.title) + " " + perposalDel.firstname + " " + perposalDel.lastname + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Mobile</td> <td><strong>:</strong> " + perposalDel.mobile + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Email</td> <td><strong>:</strong> " + perposalDel.emailid + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Pan No</td> <td><strong>:</strong>" + (!string.IsNullOrEmpty(perposalDel.panno) ? perposalDel.panno : " N/A") + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Aadhar No</td> <td><strong>:</strong>" + (!string.IsNullOrEmpty(perposalDel.aadharno) ? perposalDel.aadharno : " N/A") + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Annual Income</td> <td><strong>:</strong> " + CommonClass.IndianMoneyFormat(perposalDel.annualincome) + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Address</td> <td><strong>:</strong> " + perposalDel.address1 + ", " + perposalDel.address1 + (!string.IsNullOrEmpty(perposalDel.landmark) ? " (" + perposalDel.landmark + ")" : "") + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>Pincode</td> <td><strong>:</strong> " + perposalDel.pincode + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>State</td> <td><strong>:</strong> " + perposalDel.statename + "<br></td> </tr>");
                sbLeft.Append("<tr> <td>City</td> <td><strong>:</strong> " + perposalDel.cityname + "<br></td> </tr>");
                sbLeft.Append("</tbody>");
                sbLeft.Append("</table>");

                result.Add(sbLeft.ToString());

                sbLeft = new StringBuilder();
                sbLeft.Append("<table class='table table-responsive table-striped summary_data_table'>");
                sbLeft.Append("<tbody>");
                foreach (var addon in addonList)
                {
                    if (addon.value == "percentage") { addon.addOnValue = ((Convert.ToDecimal(addon.BasePremium) * Convert.ToDecimal(addon.addOnValue)) / 100).ToString(); }
                    sbLeft.Append("<tr> <td>" + addon.addOns + "</td> <td><strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + CommonClass.IndianMoneyFormat(addon.addOnValue) + "</strong><br></td> </tr>");
                }
                sbLeft.Append("</tbody>");
                sbLeft.Append("</table>");
                result.Add(sbLeft.ToString());

                sbLeft = new StringBuilder();
                sbLeft.Append("<table class='table table-responsive table-striped summary_data_table'>");
                sbLeft.Append("<tbody>");

                sbLeft.Append("<tr style=\"background-color: #f9f9f9\">");
                sbLeft.Append("<th>Relation</th>");
                sbLeft.Append("<th>Name</th>");
                sbLeft.Append("<th>Gender</th>");
                sbLeft.Append("<th>Occupation</th>");
                sbLeft.Append("<th>Date of Birth</th>");
                sbLeft.Append("<th>Height</th>");
                sbLeft.Append("<th>Weight</th>");
                sbLeft.Append("</tr>");

                foreach (var item in insuredDetail)
                {
                    sbLeft.Append("<tr>");
                    sbLeft.Append("<td> " + item.relationname + " </td>");
                    sbLeft.Append("<td> " + char.ToUpper(item.title[0]) + item.title.Substring(1) + " " + item.firstname + " " + item.lastname + " </td>");
                    sbLeft.Append("<td> " + (item.title.ToLower() == "mr" ? "Male" : "Female") + " </td>");
                    sbLeft.Append("<td> " + item.occupation + " </td>");
                    sbLeft.Append("<td> " + item.dob + " </td>");
                    sbLeft.Append("<td> " + item.height + " </td>");
                    sbLeft.Append("<td> " + item.weight + " </td>");
                    sbLeft.Append("</tr>");
                }

                sbLeft.Append("</tbody>");
                sbLeft.Append("</table>");

                result.Add(sbLeft.ToString());

                sbLeft = new StringBuilder();
                foreach (var item in insuredDetail)
                {
                    if (!string.IsNullOrEmpty(item.medicalhistory))
                    {
                        List<QuestionaireList> questionaries = JsonConvert.DeserializeObject<List<QuestionaireList>>(item.medicalhistory);

                        sbLeft.Append("<table class='table table-responsive table-striped' style=\"border: 1px black solid;\">");
                        sbLeft.Append("<tbody>");

                        sbLeft.Append("<tr style=\"background-color: #f9f9f9\">");
                        sbLeft.Append("<td> <span class=\"color-orange font-bold\">Medical Details of</span> </td>");
                        sbLeft.Append("<td class=\"text-center font-bold\"> " + item.firstname + " " + item.lastname + " </td>");
                        sbLeft.Append("</tr>");

                        foreach (var ques in questionaries)
                        {
                            string mainRespo = ques.response;

                            sbLeft.Append("<tr>");
                            sbLeft.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\"><b>" + ques.questionDescription + "</b></td>");
                            if (mainRespo == "true")
                            {
                                sbLeft.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\" class=\"text-center\"><b> YES</b> </td>");
                            }
                            else
                            {
                                sbLeft.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\" class=\"text-center\"><b> NO</b> </td>");
                            }
                            sbLeft.Append("</tr>");

                            foreach (var subques in ques.subQuestion)
                            {
                                sbLeft.Append("<tr>");
                                sbLeft.Append("<td > " + subques.questionDescription + "</td>");
                                sbLeft.Append("<td >" + subques.response + "</td>");
                                sbLeft.Append("</tr>");
                            }
                        }

                        sbLeft.Append("</tbody>");
                        sbLeft.Append("</table>");
                    }
                    else
                    {
                        sbLeft.Append("<table class='table '>");
                        sbLeft.Append("<tbody>");
                        sbLeft.Append("<tr>");
                        sbLeft.Append("<td style='text-align: center;color: red;border-top: none;'>No medical record found!</td>");
                        sbLeft.Append("</tr>");
                        sbLeft.Append("</tbody>");
                        sbLeft.Append("</table>");
                    }
                }
                result.Add(sbLeft.ToString());
                //----------------------
                sbLeft = new StringBuilder();


                sbLeft.Append("<table class='table table-responsive table-striped summary_data_table'>");
                sbLeft.Append("<tbody>");

                sbLeft.Append("<tr>");
                sbLeft.Append("<td>Nominee Name</td>");
                sbLeft.Append("<td><strong>:</strong> " + char.ToUpper(perposalDel.nTitle[0]) + perposalDel.nTitle.Substring(1) + " " + perposalDel.nFirstName + " " + perposalDel.nLastName + "</td>");
                sbLeft.Append("</tr>");

                sbLeft.Append("<tr>");
                sbLeft.Append("<td>Nominee Relation</td>");
                sbLeft.Append("<td><strong>:</strong> " + perposalDel.nRelation + "</td>");
                sbLeft.Append("</tr>");

                sbLeft.Append("<tr>");
                sbLeft.Append("<td>Nominee DOB</td>");
                sbLeft.Append("<td><strong>:</strong> " + perposalDel.nDOB + "</td>");
                sbLeft.Append("</tr>");

                //sbLeft.Append("<tr>");
                //sbLeft.Append("<td>Unlimited Automatic Recharge</td>");
                //sbLeft.Append("<td><strong>:</strong> XXX </td>");
                //sbLeft.Append("</tr>");

                //sbLeft.Append("<tr>");
                //sbLeft.Append("<td>Personal Accident Cover</td>");
                //sbLeft.Append("<td><strong>:</strong> XXX </td>");
                //sbLeft.Append("</tr>");


                sbLeft.Append("</tbody>");
                sbLeft.Append("</table>");

                //------------------------
                result.Add(sbLeft.ToString());

                StringBuilder sbRight = new StringBuilder();

                foreach (var item in resRespo)
                {
                    if (item.totalPremium == perposalDel.premium)
                    {
                        sbRight.Append("<div class='col-sm-12'>");
                        sbRight.Append("<img src='" + item.logoUrl + "' style='margin-left: auto;margin-right: auto;display: block;'>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='col-sm-12'>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6'><label>Product  Name</label></div>");
                        sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + item.product.policyName + "</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6'><label>Plan Type</label></div>");
                        sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + (!string.IsNullOrEmpty(item.plan) ? item.plan : "- - -") + "</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6'><label>Policy Type</label></div>");
                        sbRight.Append("<div class='col-sm-6'  style='text-align: right;'><label>" + item.policyType + "</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6' ><label>Policy Period</label></div>");
                        sbRight.Append("<div class='col-sm-6'  style='text-align: right;'><label>" + item.product.period + " year(s)</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6'><label>Member</label></div>");
                        sbRight.Append("<div class='col-sm-6'  style='text-align: right;'><label>Adult-" + urladult + ", Child-" + urlchild + "</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class='col-sm-6'><label>Sum Insred</label></div>");
                        sbRight.Append("<div class='col-sm-6'  style='text-align: right;'><label>" + CommonClass.IndianMoneyFormat(item.suminsured.ToString().Replace("INR", "")) + "</label></div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='col-sm-12' style=\"margin-top: 2%;margin-bottom: 2%;font-size: 20px;text-align: center;color: red;\"><b class='text-center'>Premium Breakup</b></div>");

                        sbRight.Append("<div id='divPremiumBreakup' class=\"row\">");

                        //sbRight.Append(SetPremiumBreakup(item.basePremium, item.premium, item.discountPercent.ToString(), item.serviceTax, item.totalPremium));

                        //-------------

                        sbRight.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class=\"row\">");
                        sbRight.Append("<div class='col-sm-6'><label>Basic</label></div>");
                        sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + CommonClass.IndianMoneyFormat(payment.pp_premium) + "</label></div>");
                        sbRight.Append("</div>");
                        sbRight.Append("</div>");

                        //string disAmt = (Convert.ToDouble(basePremium.Replace("INR", "")) - Convert.ToDouble(premium.Replace("INR", ""))).ToString();
                        //double disPer = Convert.ToDouble(discount.Replace("%", ""));

                        //sbRight.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                        //sbRight.Append("<div class=\"row\">");
                        //sbRight.Append("<div class='col-sm-6'><label>Discount (" + (disPer > 0 ? disPer.ToString() : "0.0") + " %)</label></div>");
                        //sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + CommonClass.IndianMoneyFormat(disAmt) + "</label></div>");
                        //sbRight.Append("</div>");
                        //sbRight.Append("</div>");

                        sbRight.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                        sbRight.Append("<div class=\"row\">");
                        sbRight.Append("<div class='col-sm-6'><label>GST 18%</label></div>");
                        sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + CommonClass.IndianMoneyFormat(payment.pp_serviceTax) + "</label></div>");
                        sbRight.Append("</div>");
                        sbRight.Append("</div>");

                        sbRight.Append("<div class='col-sm-12'>");
                        sbRight.Append("<div class=\"row\">");
                        sbRight.Append("<div class='col-sm-6'><label>You'll Pay</label></div>");
                        sbRight.Append("<div class='col-sm-6' style='text-align: right;'><label>" + CommonClass.IndianMoneyFormat(payment.pp_totalPremium) + "</label></div>");
                        sbRight.Append("</div>");
                        sbRight.Append("</div>");

                        //-------------


                        sbRight.Append("</div>");
                        break;
                    }
                }
                result.Add(sbRight.ToString());

                //if (!string.IsNullOrEmpty(payment.pp_paymenturl))
                //{
                //    result.Add("<a href='" + payment.pp_paymenturl + "/" + enquiryid + "' target='_blank' title='Payment' style='float: right;'><p class='motericon caractive'><span class='fa fa-credit-card'></span></p><p class='carhed' style='margin-right:-84px'>payment</p></a>");

                //}
                result.Add(payment.pp_proposalNum);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion

        #region [Genrate Payment Section]
        public JsonResult GenratePaymentUrl(string enquiryid)
        {
            string paymenturl = string.Empty;

            if (!string.IsNullOrEmpty(enquiryid))
            {
                enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
                string userid = string.Empty;
                string password = string.Empty;
                string agencyId = string.Empty;
                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    userid = lu.UserId;
                    password = lu.Password;
                    agencyId = lu.AgencyID;
                }

                Health_Payment payment = Health_Service.GetHealthPaymentTableDetails(enquiryid);
                if (payment.payment_status == false)
                {
                    if (!string.IsNullOrEmpty(payment.pp_paymenturl))
                    {
                        paymenturl = payment.pp_paymenturl;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(payment.pp_rowpaymenturl))
                        {
                            paymenturl = HealthAPIHelper.GenrateHealthPaymentUrl(userid, password, agencyId, payment.pp_rowpaymenturl, enquiryid, payment.pp_companyId);
                        }
                    }
                    TempData["EnqId"] = enquiryid;
                    TempData.Keep("EnqId");

                    HealthAPIHelper.InitializeCurrentEnquiryId(enquiryid);
                }
                else
                {
                    paymenturl = "paid";
                }
            }

            return Json(paymenturl);
        }
        public JsonResult CheckHealthPaymentDetails(string enquiryid)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(enquiryid))
            {
                enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
                Health_Payment payment = Health_Service.GetHealthPaymentTableDetails(enquiryid);
                if (payment.payment_status)
                {
                    if (!string.IsNullOrEmpty(payment.pp_paymenturl))
                    {
                        string userid = string.Empty;
                        string password = string.Empty;
                        string agencyid = string.Empty;

                        insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                        if (AdministrationService.IsAgencyLogin(ref lu))
                        {
                            userid = lu.UserId;
                            password = lu.Password;
                            agencyid = lu.AgencyID;
                        }

                        if (string.IsNullOrEmpty(userid))
                        {
                            NewEnquiry_Health quotes = Health_Service.GetNewEnquiryHealthDetails(enquiryid);
                            userid = quotes.login_userid;
                            password = quotes.login_password;
                            agencyid = quotes.AgencyId;
                        }
                        result = HealthAPIHelper.GetProposalAndPolicy(userid, password, agencyid, payment.pp_referenceId, enquiryid, payment.pp_companyId, payment.payment_status);
                    }
                }
                else
                {
                    result = "notpay";
                }
            }
            return Json(result);
        }
        public JsonResult GetHealthPdfDocument(string enquiryid)
        {
            string documenturl = string.Empty;
            if (!string.IsNullOrEmpty(enquiryid))
            {
                enquiryid = Health_Service.DecryptGenrateEnquiryId(enquiryid);
                Health_Payment payment = Health_Service.GetHealthPaymentTableDetails(enquiryid);
                if (payment.payment_status)
                {
                    string userid = string.Empty;
                    string password = string.Empty;
                    string agencyid = string.Empty;

                    insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                    if (AdministrationService.IsAgencyLogin(ref lu))
                    {
                        userid = lu.UserId;
                        password = lu.Password;
                        agencyid = lu.AgencyID;
                    }

                    if (string.IsNullOrEmpty(userid))
                    {
                        NewEnquiry_Health quotes = Health_Service.GetNewEnquiryHealthDetails(enquiryid);
                        userid = quotes.login_userid;
                        password = quotes.login_password;
                        agencyid = quotes.AgencyId;
                    }

                    HealthDocumentRespo doc_Respo = HealthAPIHelper.Genrate_HealthPdfDocument(userid, password, agencyid, enquiryid);
                    if (doc_Respo != null)
                    {
                        if (doc_Respo.success == false && doc_Respo.documenturl == null)
                        {
                            documenturl = "";
                        }
                        else if (doc_Respo.documenturl.Contains("failed") || doc_Respo.documenturl.Contains("notpay"))
                        {
                            documenturl = doc_Respo.documenturl;
                        }
                        else
                        {
                            var request = System.Net.WebRequest.Create(doc_Respo.documenturl);
                            request.Method = "GET";

                            using (var response = request.GetResponse())
                            {
                                using (var stream = response.GetResponseStream())
                                {
                                    string path = Server.MapPath("~/PDF/Health/" + enquiryid + ".pdf");
                                    using (var fileStream = System.IO.File.Create(path))
                                    {
                                        stream.CopyTo(fileStream);
                                        bool ispdflink = Health_Service.UpdateHealthPolicyPdfLink(enquiryid, "PDF/Health/" + enquiryid + ".pdf");
                                        if (ispdflink)
                                        {
                                            documenturl = Config.WebsiteUrl + ("PDF/Health/" + enquiryid + ".pdf");
                                        }
                                    }
                                }
                            }
                        }

                        //else
                        //{
                        //    documenturl = "<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>" + response.message + "</p></div>";
                        //}
                    }

                }
                else
                {
                    documenturl = "notpay";
                }
            }
            return Json(documenturl);
        }
        #endregion
        public JsonResult FilterSection(FilterSection filter, string adult, string child)
        {
            List<string> result = new List<string>();

            try
            {
                NewEnquiry_Health enqDetail = Health_Service.GetNewEnquiryHealthDetails(filter.EnquiryId);

                insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    HealthApiResponse apiResponse = HealthAPIHelper.GetHealthApiResponse(lu.UserId, lu.Password, lu.AgencyID, enqDetail);

                    if (apiResponse != null)
                    {
                        if (apiResponse.success)
                        {
                            bool isFilter = false;
                            List<Datum> dataList = apiResponse.response.data;

                            if (filter.MinPrice > 0)
                            {
                                isFilter = true;
                                dataList = dataList.Where(p => Convert.ToDecimal(p.totalPremium) >= Convert.ToDecimal(filter.MinPrice)).ToList();
                            }
                            if (filter.MaxPrice > 0)
                            {
                                isFilter = true;
                                dataList = dataList.Where(p => Convert.ToDecimal(p.totalPremium) <= Convert.ToDecimal(filter.MaxPrice)).ToList();
                            }

                            if (!string.IsNullOrEmpty(filter.Tenure))
                            {
                                isFilter = true;
                                dataList = dataList.Where(p => p.product.period.ToString().Contains(filter.Tenure)).ToList();
                            }

                            if (!string.IsNullOrEmpty(filter.SumInsured))
                            {
                                isFilter = true;
                                dataList = dataList.Where(p => p.suminsured.ToString().Contains(filter.SumInsured)).ToList();
                            }

                            if (filter.Provider != null)
                            {
                                isFilter = true;
                                dataList = dataList.Where(p => filter.Provider.Contains(p.insuranceCompany.ToString())).ToList();
                            }

                            if (string.IsNullOrEmpty(filter.Tenure))
                            {
                                dataList = dataList.Where(p => p.product.period.ToString().Contains("1")).ToList();
                            }

                            if (isFilter)
                            {
                                apiResponse.response.data = null;
                                apiResponse.response.data = dataList;

                                //result = GenrateHealthProductList(apiResponse, adult, child);

                                StringBuilder sbResult = new StringBuilder();
                                List<decimal> priceOrder = new List<decimal>();
                                int loopCount = 0;
                                if (dataList != null && dataList.Count > 0)
                                {
                                    foreach (var item in dataList.OrderBy(p => p.totalPremium))
                                    {
                                        if (item.insuranceCompany != null && (item.totalPremium != null && Convert.ToDecimal(item.totalPremium) > 0) && item.suminsured > 0)
                                        {
                                            string finalAmount = "0"; string tenure = "1"; string productDetailsId = string.Empty;
                                            string sumInsuredId = item.suminsuredId.ToString();
                                            if (item.product != null)
                                            {
                                                productDetailsId = item.product.productDetailId != null ? item.product.productDetailId.ToString() : string.Empty;
                                                tenure = Convert.ToInt32(item.product.period) > 0 ? item.product.period.ToString() : "1";
                                            }

                                            sbResult.Append("<div class='col-sm-12 listbox'>");
                                            sbResult.Append("<div class='row'>");

                                            sbResult.Append("<div class='col-sm-2' style='border-right: 1px solid #ccc;'>");
                                            sbResult.Append("<img src='" + item.logoUrl + "' class='img-responsive img-center margin_top_15 company_logo'  style='margin-top:27px;'/>");
                                            sbResult.Append("</div>");

                                            sbResult.Append("<div class='col-sm-8'>");

                                            sbResult.Append("<div class='row' style='border-bottom: 1px solid #ccc;padding: 3px;'>");

                                            sbResult.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                                            sbResult.Append("<div class='row'>");
                                            sbResult.Append("<div class='col-sm-2'>");
                                            sbResult.Append("<p><span class='fa fa-bed fonticon'></span></p>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("<div class='col-sm-10'>");
                                            sbResult.Append("<p class='fontdetails'>Room Rent</p>");
                                            if (item.benefits != null && item.benefits.categories != null)
                                            {
                                                BenefitCategory rromrent = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("basic features")).FirstOrDefault();
                                                if (rromrent != null && rromrent.menus.Count > 0)
                                                {
                                                    BenefitMenu room_menu = rromrent.menus.Where(p => p.menuName.ToLower().Trim().Contains("room rent eligibility")).FirstOrDefault();
                                                    sbResult.Append("<p class='fontdetails'> " + room_menu.details + "</p>");
                                                }
                                                else
                                                {
                                                    sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                                }
                                            }
                                            else
                                            {
                                                sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                            }
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                                            sbResult.Append("<div class='row'>");
                                            sbResult.Append("<div class='col-sm-2'>");
                                            sbResult.Append("<p><span class='fa fa-heartbeat fonticon '></span></p>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("<div class='col-sm-10'>");
                                            sbResult.Append("<p class='fontdetails'>Pre-Existing Disease</p>");
                                            if (item.benefits != null && item.benefits.categories != null)
                                            {
                                                BenefitCategory predisease = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("special features")).FirstOrDefault();
                                                if (predisease != null && predisease.menus.Count > 0)
                                                {
                                                    BenefitMenu predeas_menu = predisease.menus.Where(p => p.menuName.ToLower().Trim().Contains("existing disease waiting")).FirstOrDefault();
                                                    sbResult.Append("<p class='fontdetails'>  " + predeas_menu.details + "</p>");
                                                }
                                                else
                                                {
                                                    sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                                }
                                            }
                                            else
                                            {
                                                sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                            }
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("<div class='col-sm-4' style='border-right: 1px solid #ccc;'>");
                                            sbResult.Append("<div class='row'>");
                                            sbResult.Append("<div class='col-sm-2'>");
                                            sbResult.Append("<p><span class='fa fa-building-o fonticon'></span></p>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("<div class='col-sm-10'>");
                                            sbResult.Append("<p class='fontdetails'>Network Hospitals</p>");
                                            if (item.benefits != null && item.benefits.categories != null)
                                            {
                                                BenefitCategory nethospital = item.benefits.categories.Where(p => p.categoryName.ToLower().Trim().Contains("usp")).FirstOrDefault();
                                                if (nethospital != null && nethospital.menus.Count > 0)
                                                {
                                                    BenefitMenu nothos_menu = nethospital.menus.Where(p => p.menuName.ToLower().Trim().Contains("hospital network")).FirstOrDefault();
                                                    sbResult.Append("<p class='fontdetails'>  " + nothos_menu.details + "</p>");
                                                }
                                                else
                                                {
                                                    sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                                }
                                            }
                                            else
                                            {
                                                sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                                            }
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");

                                            sbResult.Append("<div class='row' style='margin-top:20px;'>");
                                            sbResult.Append("<div class='col-sm-3'>");
                                            if (item.benefits != null && item.benefits.categories != null)
                                            {
                                                sbResult.Append("<a href='#' class='btnseedelt' data-toggle='modal' data-thiscount='" + loopCount + "' data-target='#ProductDetail_" + (item.product.period + "_" + item.product.productDetailId + "_" + loopCount) + "' style='font-size:17px;color:#e71820'><span class='fa fa-align-justify'></span>&nbsp; View Detail</a>");
                                            }
                                            sbResult.Append("</div>");
                                            //sbResult.Append("<div class='col-sm-4'>" + (item.product != null ? item.product.policyName : "- - -") + "</div>");
                                            string product_del_name = (item.product != null ? item.product.policyName : "- - -") + (item.plan != null && (!string.IsNullOrEmpty(item.plan.ToString()) && item.plan.ToString().ToLower() != "na") ? (" - " + item.plan) : string.Empty);
                                            sbResult.Append("<div class='col-sm-4'>" + product_del_name + "</div>");
                                            sbResult.Append("<div class='col-sm-5'>");
                                            sbResult.Append("<h6 class='checkbox-inline' style='border: 1px solid #e71820;border-radius: 17px; padding: 1px 17px 5px; width: 70%;float:right'>");
                                            sbResult.Append("<label class='seeingo chech' style='color: #777777 !important;'>Compare Product");

                                            sbResult.Append("<input type='checkbox' class='submenuclass healthInsuranceProducts' data-identity='hip" + loopCount + "' id='commit' onchange='CompareCheck(this," + loopCount + ")' name='commit' />");
                                            sbResult.Append("<span class='hidden' id='compData" + loopCount + "' data-sum_insured='" + item.suminsured + "' data-plan='" + item.plan + "' data-companyid='" + item.insuranceCompanyId + "' data-productid='" + item.product.productDetailId + "'></span>");

                                            sbResult.Append("<span class='checkmark'></span>");
                                            sbResult.Append("</label>");
                                            sbResult.Append("</h6>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");

                                            sbResult.Append("</div>");

                                            sbResult.Append("<div class='col-sm-2' style='text-transform: capitalize;'>");
                                            sbResult.Append("<div class='row'>");
                                            sbResult.Append("<div class='col-sm-12'>");

                                            sbResult.Append("<p style='text-align: center;margin-top: -6px;font-size: 14px;font-weight: bold;'> Cover:&nbsp;<span id='coverCount" + loopCount + "'><span class='fa fa-rupee'></span>" + item.suminsured + " / " + tenure + " Year</span></p>");

                                            finalAmount = Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString();

                                            priceOrder.Add(!string.IsNullOrEmpty(finalAmount) ? Math.Ceiling(Convert.ToDecimal(finalAmount)) : 0);

                                            sbResult.Append("<button class='default-btn bookbtn' id='premiumCount" + loopCount + "' data-suminsuredid='" + sumInsuredId + "' data-productid='" + productDetailsId + "' data-suminsured='" + item.suminsured + "'  data-totalamt='" + item.totalPremium + "'  data-planname='" + item.plan + "' data-tenure='" + tenure + "' data-companyid='" + item.insuranceCompanyId + "' data-adult='" + adult + "' data-child='" + child + "'>" + CommonClass.IndianMoneyFormat(finalAmount.Replace("INR", "")) + "</button>");
                                            sbResult.Append("<p style='text-align: center;font-size: 12px;'>All Inclusive</p>");
                                            sbResult.Append(GetPerDayAmount(item.totalPremium, tenure));
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");

                                            sbResult.Append("</div>");
                                            sbResult.Append("</div>");

                                            sbResult.Append(BindBenefitsByProduct(item.benefits, (item.product.period + "_" + item.product.productDetailId + "_" + loopCount), loopCount, item.plan, tenure, sumInsuredId, adult, child, item.totalPremium, CommonClass.IndianMoneyFormat(finalAmount.Replace("INR", "")), GetPerDayAmount(item.totalPremium, tenure), product_del_name));
                                        }
                                        loopCount += 1;
                                    }
                                }
                                else
                                {
                                    sbResult.Append("");
                                }

                                result.Add(sbResult.ToString());
                                result.Add(dataList.Count().ToString());
                            }
                            else
                            {
                                result = GenrateHealthProductList(apiResponse, adult, child);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetCityListFromStarHealth(string pincode)
        {
            string cityList = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(pincode))
                {
                    insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                    if (AdministrationService.IsAgencyLogin(ref lu))
                    {
                        cityList = HealthAPIHelper.GetCityListFromStarHealth(lu.UserId, lu.Password, lu.AgencyID, pincode);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(cityList);
        }

        public JsonResult GetAreaListFromStarHealth(string pincode, string cityid)
        {
            string areaList = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(pincode))
                {
                    insurance.Models.Custom.FrontAgencyModel.Agency_Detail lu = new insurance.Models.Custom.FrontAgencyModel.Agency_Detail();
                    if (AdministrationService.IsAgencyLogin(ref lu))
                    {
                        areaList = HealthAPIHelper.GetAreaListFromStarHealth(lu.UserId, lu.Password, lu.AgencyID, pincode, cityid);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(areaList);
        }
    }
}