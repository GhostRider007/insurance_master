﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.HealthApi;
using insurance.Helper;
using insurance.Models.Common;
using insurance.Service;
using insurance.Service.CommonService;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Controllers
{
    public class AdministrationController : Controller
    {

        public ActionResult AgencyCertificate()
        {
            Agency_Detail model = new Agency_Detail();
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                model.AgencyID = lu.AgencyID;
                model.AgencyName = lu.AgencyName;              
                model.UpdatedDate = Convert.ToDateTime(lu.UpdatedDate).ToString("dd/MM/yyyy");
            }
            return View(model);
        }
        public ActionResult AgencyBankDetails()
        {
            BankDeatil model = new BankDeatil();
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                model.AgencyID = lu.AgencyID;
            }
            model = AdminArea_EnquiryService.GetBankDetailsList(model.AgencyID).FirstOrDefault();
            return View(model);
        }

        public ActionResult UpdateBankDetails()
        {
            BankDeatil model = new BankDeatil();
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                model.AgencyID = lu.AgencyID;
            }           
            model = AdminArea_EnquiryService.GetBankDetailsList(model.AgencyID).FirstOrDefault();
            return View(model);
        }
        [HttpPost]
        public ActionResult UpdateBankDetails(BankDeatil model)
        {
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(model.AccountNo) && !string.IsNullOrEmpty(model.IFSC))
            {
                if (AdministrationService.UpdateBankDetails(model, ref msg))
                {
                    TempData["Message"] = msg;
                    return Redirect("UpdateBankDetails");
                }
                else
                {
                    ViewBag.Msg = "<div class='col-md-12 text-danger' style='margin-top:10px;'>" + msg + "</div>";
                }
            }
            return View(model);
        }

        public ActionResult HealthRenewalList(string EnquiryId = null, string PolicyNo = null, string Insurer = null, string Product = null, string PolicyType = null, string CustomerName = null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                Agency_Detail lu = new Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {                   
                    list = AdminArea_EnquiryService.GetRenewalList_Health(EnquiryId, PolicyNo, lu.AgencyID, Insurer, Product, PolicyType, CustomerName, "agency");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult HealthPolicyList(string EnquiryId = null, string PolicyNo = null, string Insurer = null, string Product = null, string PolicyType = null, string FromDate = null, string ToDate = null, string CustomerName=null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                Agency_Detail lu = new Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        FromDate = (Convert.ToDateTime(FromDate)).ToString("yyyy-MM-dd");
                    }
                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        ToDate = (Convert.ToDateTime(ToDate)).ToString("yyyy-MM-dd");
                    }

                    list = AdminArea_EnquiryService.GetEnquiryDetailsByAgencyHealth("policy", EnquiryId, null, PolicyNo, lu.AgencyID, Insurer, Product, PolicyType, FromDate, ToDate, CustomerName, "agency");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult HealthProposalList(string EnquiryId = null, string ProposalNo = null, string Insurer = null, string Product = null, string PolicyType = null, string FromDate = null, string ToDate = null,string CustomerName=null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                Agency_Detail lu = new Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        FromDate = (Convert.ToDateTime(FromDate)).ToString("yyyy-MM-dd");
                    }
                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        ToDate = (Convert.ToDateTime(ToDate)).ToString("yyyy-MM-dd");
                    }
                    list = AdminArea_EnquiryService.GetEnquiryDetailsByAgencyHealth("proposal", EnquiryId, ProposalNo, null, lu.AgencyID, Insurer, Product, PolicyType, FromDate, ToDate, CustomerName, "agency");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult PolicyList(string FromDate = null, string ToDate = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string EnquiryId = null, string PolicyNo = null, string CustomerName=null)
        {
            List<ProposalDetailsMotor> enq = new List<ProposalDetailsMotor>();
            try
            {
                Agency_Detail lu = new Agency_Detail();
                string AgencyID = string.Empty;
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    AgencyID = lu.AgencyID;
                }
                //enq = AdministrationService.GetEnquiryDetailsbyAgency(model.AgencyID);
                enq = AdminArea_EnquiryService.GetEnquiryDetailsMotor("Policy", FromDate, ToDate, AgencyId, Insurer, Product, PolicyType, EnquiryId, null, PolicyNo, CustomerName, "agency");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(enq);
        }
        public ActionResult ProposalList(string FromDate = null, string ToDate = null, string Insurer = null, string Product = null, string PolicyType = null, string EnquiryId = null, string ProposalNo = null, string CustomerName=null)
        {
            List<ProposalDetailsMotor> enq = new List<ProposalDetailsMotor>();
            try
            {
                Agency_Detail lu = new Agency_Detail();
                string AgencyID = string.Empty;
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    AgencyID = lu.AgencyID;
                }
                //enq = AdministrationService.GetEnquiryDetailsByAgencypro(model.AgencyID);
                enq = AdminArea_EnquiryService.GetEnquiryDetailsMotor("Proposal", FromDate, ToDate, AgencyID, Insurer, Product, PolicyType, EnquiryId, ProposalNo, null, CustomerName, "agency");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(enq);
        }

        public ActionResult EmailProposal(string enquiry_id)
        {
            TrackEnquirydetail enq = new TrackEnquirydetail();
            try
            {
                enq = AdminArea_EnquiryService.EmailProposalDetailsMotor(enquiry_id).FirstOrDefault();

                if (Email.SendProposal(enq))
                {
                    TempData["message"] = "Email send successfully";
                    return Redirect("ProposalList");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("ProposalList");
        }

        public ActionResult HealthEmailProposal(string enquiry_id)
        {
            TrackHealthEnquirydetail enq = new TrackHealthEnquirydetail();
            try
            {
                enq = AdminArea_EnquiryService.EmailProposalDetailsHealth(enquiry_id).FirstOrDefault();

                if (Email.SendHealthProposal(enq))
                {
                    TempData["message"] = "Email send successfully";
                    return Redirect("HealthProposalList");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("HealthProposalList");
        }
        public ActionResult Index(LeadRequestModel model)
        {
            if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.ContactNo))
            {
                Agency_Detail lu = new Agency_Detail();
                if (AdministrationService.IsAgencyLogin(ref lu))
                {
                    model.AgencyID = lu.AgencyID.ToString();
                    model.AgencyName = lu.AgencyName;
                }
                if (AdministrationService.InsertLeadReq(model))
                {
                    TempData["Msg"] = "Quotes has been Sent Sucessfully ...";
                    ModelState.Clear();
                    model.Name = "";
                    model.Email = "";
                    model.ContactNo = "";
                    model.Service = "";
                    model.Remark = "";
                    model.Age = "";
                }
            }
            return View(model);
        }
        public ActionResult AgencyDashbord()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AgencyChangePassword()
        {
            CreateAgency model = new CreateAgency();
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                model.AgencyID = lu.AgencyID;
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AgencyChangePassword(CreateAgency model)
        {
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(model.CurrentPassword) && !string.IsNullOrEmpty(model.Password))
            {
                if (AdministrationService.AgencyResetPassword(model, ref msg))
                {
                    TempData["Message"] = msg;
                }
                else
                {
                    ViewBag.Msg = "<div class='col-md-12 text-danger' style='margin-top:10px;'>" + msg + "</div>";
                }
            }
            return View(model);
        }
        public ActionResult AgencyProfile(CreateAgency model)
        {
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                model.FirstName = lu.FirstName;
                model.LastName = lu.LastName;
                model.AgencyName = lu.AgencyName;
                model.BusinessEmailID = lu.BusinessEmailID;
                model.UserId = lu.UserId;
                model.Mobile = lu.Mobile;
                model.GSTNo = lu.GSTNo;
                model.BusinessPhone = lu.BusinessPhone;
                model.EmailID = lu.EmailID;
                model.Address = lu.Address;
            }
            return View(model);
        }

        #region[Booking Report]
        public ActionResult BookingReport(string fromDate = null, string toDate = null, string InsuranceType = null, string Insurer = null, string Product = null, string CustomerName=null)
        {
            List<BookingDetails> list = new List<BookingDetails>();
            string AgencyId = string.Empty;
            Agency_Detail lu = new Agency_Detail();

            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                AgencyId = lu.AgencyID;
            }
            list = AdminArea_AccountService.GetBookingDetails(fromDate, toDate, AgencyId, InsuranceType, Insurer, Product, CustomerName);
            return View(list);
        }
        public JsonResult BindInsurers(string insuranceType)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindInsurers(insuranceType);
            return Json(result);
        }
        public JsonResult BindProducts(string insuranceType, string insuranceId)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindProducts(insuranceType, insuranceId);
            return Json(result);
        }
        #endregion

        #region [Json Section]
        public JsonResult AgencyLogin(string userid, string password)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;

                if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(password))
                {
                    if (AdministrationService.AgencyLogin(userid, password, ref msg))
                    {
                        result.Add("true");
                        result.Add("/agency/dashboard");
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(result);
        }

        public JsonResult DashbordList()
        {
            string agencyid = "";
            List<string> result = new List<string>();
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                agencyid = lu.AgencyID;
            }

            try
            {
                string hPolicy = string.Empty;
                string hProposal = string.Empty;
                string hLead = string.Empty;
                string MPolicy = string.Empty;
                string MProposal = string.Empty;
                string MLead = string.Empty;
                List<DashboardDetails> details = AdministrationService.GetDasboardHelathdetails(agencyid);

                if (details.Count > 0)
                {
                    foreach (var item in details)
                    {
                        if (item.service.ToLower() == "health")
                        {
                            if (item.servicetype.ToLower() == "policy")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='/health/proposalsummary?enquiry_id=" + item.Enquiry_Id.ToUpper() + "'>" + item.firstname + "  " + item.lastname + "  (SI-" + item.suminsured + ")</a>");
                                stb.Append("<p class='pmar2'>" + item.CompanyName + " - " + item.Prodcutname + "</p>");
                                stb.Append("<p class='pmar2'><strong>₹ " + item.totalPremium + "</strong>  Policy Type <strong>" + item.Policy_Type + " </strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");

                                hPolicy = hPolicy + stb.ToString();
                                continue;
                            }

                            if (item.servicetype.ToLower() == "proposal")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='/health/proposalsummary?enquiry_id=" + item.Enquiry_Id.ToUpper() + "'>" + item.firstname + "  " + item.lastname + "  (SI-" + item.suminsured + ")</a>");
                                stb.Append("<p class='pmar2'>" + item.CompanyName + " - " + item.Prodcutname + "</p>");
                                stb.Append("<p class='pmar2'><strong>₹ " + item.totalPremium + "</strong>  Policy Type <strong>" + item.Policy_Type + " </strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");

                                hProposal = hProposal + stb.ToString();
                                continue;
                            }

                            if (item.servicetype.ToLower() == "lead")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='#'>" + item.firstname + "  " + item.lastname + " </a>");
                                stb.Append("<p class='pmar2'>Suminsured :₹ " + item.Sum_Insured + "</p>");
                                stb.Append("<p class='pmar2'><strong>₹ " + item.totalPremium + "</strong>  Policy Type <strong>" + item.Policy_Type + " </strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");
                                hLead = hLead + stb.ToString();
                                continue;
                            }
                        }

                        if (item.service.ToLower() == "motor")
                        {

                            if (item.servicetype.ToLower() == "policy")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='/moter/proposalsummary?enquiry_id=" + item.Enquiry_Id.ToUpper() + "'>" + item.ModelName + " - " + item.VechileRegNo + " - " + item.vehicletype + "</a>");
                                stb.Append("<p class='pmar2'>" + item.CompanyName + "</p>");
                                stb.Append("<p class='pmar2'><strong>₹ " + item.suminsured + " ,</strong> Name : <strong>" + item.firstname + "  " + item.lastname + "</strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");

                                MPolicy = MPolicy + stb.ToString();
                                continue;
                            }

                            if (item.servicetype.ToLower() == "proposal")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='/moter/proposalsummary?enquiry_id=" + item.Enquiry_Id.ToUpper() + "'>" + item.ModelName + " - " + item.VechileRegNo + " - " + item.vehicletype + "</a>");
                                stb.Append("<p class='pmar2'>" + item.CompanyName + "</p>");
                                stb.Append("<p class='pmar2'><strong>₹ " + item.suminsured + " ,</strong> Name : <strong>" + item.firstname + "  " + item.lastname + "</strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");

                                MProposal = MProposal + stb.ToString();
                                continue;
                            }

                            if (item.servicetype.ToLower() == "lead")
                            {
                                StringBuilder stb = new StringBuilder();
                                stb.Append("<li class='media event'>");
                                stb.Append("<a class='pull-left border-aero profile_thumb'>");
                                stb.Append("<i class='fa fa-user aero'></i>");
                                stb.Append("</a>");
                                stb.Append("<div class='media-body'>");
                                stb.Append("<a class='title' href='#'>" + item.BrandName + " - " + item.ModelName + " - " + item.VechileRegNo + "</a>");
                                stb.Append("<p class='pmar2'>" + item.VarientName + " - " + item.Fuel + "</p>");
                                stb.Append("<p class='pmar2'><strong> Policy Type " + item.Policy_Type + " ---  </strong></p>");
                                stb.Append("<p class='pmar'>");
                                stb.Append("<small>" + item.Enquiry_Id.ToUpper() + "  Date " + item.createddate + "</small>");
                                stb.Append("</p>");
                                stb.Append("</div>");
                                stb.Append("</li>");
                                MLead = MLead + stb.ToString();
                                continue;
                            }


                        }
                    }
                }

                result.Add(hPolicy);
                result.Add(hProposal);
                result.Add(hLead);
                result.Add(MPolicy);
                result.Add(MProposal);
                result.Add(MLead);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult AgencyLogout(string agencyid)
        {
            string result = string.Empty;

            if (AdministrationService.LogoutAgency(agencyid))
            {
                result = "true";
            }

            return Json(result);
        }

        public JsonResult GetTotalPremium(string duration)
        {
            string agencyid = "";
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                agencyid = lu.AgencyID;
            }
            string durationValue = "-" + duration.Substring(0, 2);
            string period = duration.Substring(3);
            decimal[] result = AdministrationHelper.GetTotalPremium(agencyid, period, durationValue);
            return Json(result);
        }
        public JsonResult policyGraphbyAgency(string duration)
        {
            string agencyid = "";
            Agency_Detail lu = new Agency_Detail();
            if (AdministrationService.IsAgencyLogin(ref lu))
            {
                agencyid = lu.AgencyID;
            }
            string durationValue = "-" + duration.Substring(0, 2);
            string period = duration.Substring(3);
            int[] result = AdministrationService.GetPolicyCountAgency(agencyid, period, durationValue);
            return Json(result);
        }

        public JsonResult verifyMobileNo(string mobileno)
        {
            string tempotp = string.Empty;
            int OTP;
            string msg = string.Empty;
            OTP = CommonServe.GenerateRandomNum();
            msg = "Your One time password(otp) is  " + OTP + " for mobile verification from seeinsured.";
            string result = CommonServe.SmsMessage(mobileno, msg);
            Session["tempotp"] = OTP;
            return Json(result);
        }
        public JsonResult VerifyOtp(string otp, string mobileno)
        {
            bool ismatched = false;
            if (!string.IsNullOrEmpty(otp) && !string.IsNullOrEmpty(mobileno))
            {
                if (Session["tempotp"].ToString() == otp)
                {
                    AdministrationService.UpdateIsverif(mobileno);
                    ismatched = true;
                }
            }

            return Json(ismatched);
        }
        #endregion
    }
}