﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using static insurance.Models.Health.HealthComparisonModel;
using static insurance.Models.Health.HealthModel;
using static insurance.Models.Health.HealthPerposal;
using ecommerce.Models.Common;
using insurance.Helper.DataBase;
using insurance.Helper.HealthHelper;
using insurance.Models.Health;
using Newtonsoft.Json;
using Post_Utility.Utility;
using Post_Utility.Health;
using insurance.Service;

namespace insurance.HealthApi
{
    public static class HealthAPIHelper
    {
        public static string GetrAuthenticateToken(string userid, string password, string agencyId)
        {
            try
            {
                string trackId = string.Empty;
                return HealthProductAPI.GetAuthenticate(userid, password, agencyId, "Authenticate/Login");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }

        public static void InitializeCurrentEnquiryId(string enquiryId)
        {
            HealthProductAPI.InitializeCurrentEnquiryId(enquiryId);
        }
        public static bool IsCurrentEnquiryIdExist(ref string enquiryId)
        {
            return HealthProductAPI.IsCurrentEnquiryIdExist(ref enquiryId);
        }
        public static HealthApiResponse GetHealthApiResponse(string userid, string password, string agencyId, NewEnquiry_Health enqDetail)
        {
            HealthApiResponse result = new HealthApiResponse();

            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    string respo = HealthProductAPI.GetHealthApiResponse(agencyId, enqDetail, tokenKey);
                    if (!string.IsNullOrEmpty(respo))
                    {
                        result = JsonConvert.DeserializeObject<HealthApiResponse>(respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static void InitializeAccessTokenSession(string accessToken)
        {
            if (!string.IsNullOrEmpty(accessToken))
            {
                List<string> objAccessToken = new List<string>();
                objAccessToken.Add(accessToken);
                objAccessToken.Add(DateTime.Now.ToString());
                objAccessToken.Add("exist");
                HttpContext.Current.Session["accessToken"] = objAccessToken;
            }
        }
        public static bool IsAccessTokenExist(ref string token)
        {
            if (HttpContext.Current.Session["accessToken"] != null)
            {
                List<string> objAccessToken = (List<string>)HttpContext.Current.Session["accessToken"];
                TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
                if (TS.Minutes < 59)
                {
                    token = objAccessToken[0];
                    return true;
                }
                else
                {
                    HttpContext.Current.Session["accessToken"] = null;
                }
            }

            return false;
        }
        public static void RemoveHealth_RespoFromCache()
        {
            HealthProductAPI.RemoveHealth_RespoFromCache();
        }
        public static HealthQuestionaries GetHealthQuestionaries(CreateHealth_Proposal proposal, string userid, string password, string agencyId)
        {
            HealthQuestionaries result = new HealthQuestionaries();

            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    string respo = HealthProductAPI.GetHealthQuestionaries(proposal, agencyId, tokenKey);

                    if (!string.IsNullOrEmpty(respo))
                    {
                        result = JsonConvert.DeserializeObject<HealthQuestionaries>(respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static List<string> GetCompareInsuranceProducts(List<CompareRequest> compReqArray, string enqId, string userid, string password, string agencyId)
        {
            List<string> result = new List<string>();
            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    foreach (var item in compReqArray)
                    {
                        result.Add(HealthApiUtility.GetCompareInsuranceProducts(item.companyId, item.productid.ToString(), item.plan, item.sumInsured, agencyId, tokenKey));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static HealthPerposalResponse GetHealthPerposalResponse(string userid, string password, string agencyId, string enquiryid,string encyEnquiryid)
        {
            HealthPerposalResponse result = new HealthPerposalResponse();

            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    string respo = HealthProductAPI.GetHealthPerposalResponse(agencyId, enquiryid, tokenKey, encyEnquiryid);
                    if (!string.IsNullOrEmpty(respo))
                    {
                        result = JsonConvert.DeserializeObject<HealthPerposalResponse>(respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static HealthDocumentRespo Genrate_HealthPdfDocument(string userid, string password, string agencyId, string enquiryid)
        {
            HealthDocumentRespo result = new HealthDocumentRespo();

            CreateHealth_Proposal PerposorDetail = Health_Helper.GetProposalDetail(enquiryid);
            if (!string.IsNullOrEmpty(PerposorDetail.enquiryid))
            {
                try
                {
                    string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                    if (!string.IsNullOrEmpty(tokenKey))
                    {
                        string pdfdocurl = string.Empty;
                        if (PerposorDetail.companyid == "2")
                        {
                            if (!string.IsNullOrEmpty(PerposorDetail.pay_policy_Number))
                            {
                                string respo = HealthProductAPI.Genrate_HealthPdfDocument(agencyId, enquiryid, tokenKey, PerposorDetail.pay_policy_Number, PerposorDetail.companyid, ref pdfdocurl);
                                if (!string.IsNullOrEmpty(respo))
                                {
                                    try
                                    {
                                        CarePdfRoot carepdf_del = JsonConvert.DeserializeObject<CarePdfRoot>(respo);
                                        if (carepdf_del.success)
                                        {
                                            result.success = false;
                                            result.documenturl = carepdf_del.response.responseData.message.ToString().ToLower().Trim();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        result.success = true;
                                        result.documenturl = pdfdocurl;
                                        ex.ToString();
                                    }
                                }
                            }
                            else
                            {
                                result.documenturl = "notpay";
                            }
                        }
                        else
                        {
                            string respo = HealthProductAPI.Genrate_HealthPdfDocument(agencyId, enquiryid, tokenKey, PerposorDetail.p_referenceId, PerposorDetail.companyid, ref pdfdocurl);
                            if (!string.IsNullOrEmpty(respo))
                            {
                                result = JsonConvert.DeserializeObject<HealthDocumentRespo>(respo);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result.documenturl = "http://api.seeinsured.com/api/StarHealth/PolicyDocument/" + PerposorDetail.p_referenceId + "/" + enquiryid;
                    ex.ToString();
                }
            }

            return result;
        }
        public static string GetCityListFromStarHealth(string userid, string password, string agencyId, string pincode)
        {
            StringBuilder sbCity = new StringBuilder();
            try
            {
                CityList cityList = new CityList();
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);
                if (!string.IsNullOrEmpty(tokenKey))
                {
                    string respo = HealthProductAPI.GetCityListFromStarHealth(tokenKey, pincode);
                    if (!string.IsNullOrEmpty(respo))
                    {
                        cityList = JsonConvert.DeserializeObject<CityList>(respo);
                        if (cityList.success)
                        {
                            if (cityList.response.data.city.Count > 0)
                            {
                                sbCity.Append("<option value='0'>Select City</option>");
                                foreach (var city in cityList.response.data.city)
                                {
                                    sbCity.Append("<option value='" + city.city_id + "'>" + city.city_name + "</option>");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return sbCity.ToString();
        }
        public static string GetAreaListFromStarHealth(string userid, string password, string agencyId, string pincode, string cityid)
        {
            StringBuilder sbCity = new StringBuilder();
            try
            {
                AreaList areaList = new AreaList();
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);
                if (!string.IsNullOrEmpty(tokenKey))
                {
                    string respo = HealthProductAPI.GetAreaListFromStarHealth(tokenKey, pincode, cityid);
                    if (!string.IsNullOrEmpty(respo))
                    {
                        areaList = JsonConvert.DeserializeObject<AreaList>(respo);
                        if (areaList.success)
                        {
                            if (areaList.response.data.area.Count > 0)
                            {
                                sbCity.Append("<option value='0'>Select Area</option>");
                                foreach (var city in areaList.response.data.area)
                                {
                                    sbCity.Append("<option value='" + city.areaID + "'>" + city.areaName + "</option>");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return sbCity.ToString();
        }

        #region [Genrate Payment Section]
        public static string GenrateHealthPaymentUrl(string userid, string password, string agencyId, string raw_paymenturl, string enquiryid, string companyid)
        {
            string finalpaymenturl = string.Empty;
            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    if (companyid == "4")
                    {
                        finalpaymenturl = raw_paymenturl;
                    }
                    else
                    {
                        if (companyid != "2")
                        {
                            raw_paymenturl = raw_paymenturl + "/" + enquiryid;
                        }

                        string respo = HealthProductAPI.GenrateHealthPaymentUrl(agencyId, enquiryid, raw_paymenturl, tokenKey);
                        if (!string.IsNullOrEmpty(respo))
                        {
                            if (companyid == "2")
                            {
                                CarePaymentRoot carepaymentDel = new CarePaymentRoot();
                                carepaymentDel = JsonConvert.DeserializeObject<CarePaymentRoot>(respo);
                                if (carepaymentDel.success)
                                {
                                    finalpaymenturl = carepaymentDel.response + "/" + enquiryid;
                                }
                            }
                            else
                            {
                                StarPaymentRoot startpaymentDel = new StarPaymentRoot();
                                startpaymentDel = JsonConvert.DeserializeObject<StarPaymentRoot>(respo);
                                if (startpaymentDel.success)
                                {
                                    finalpaymenturl = startpaymentDel.response.urls.paymentUrl;
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(finalpaymenturl))
                    {
                        ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "p_paymenturl='" + finalpaymenturl + "'", "enquiryid='" + enquiryid + "'", "api");
                        ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Payment", "pp_paymenturl='" + finalpaymenturl + "'", "enquiryid='" + enquiryid + "'", "api");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return finalpaymenturl;
        }
        #endregion
        public static string GetProposalAndPolicy(string userId, string password, string agencyID, string pp_referenceId, string enquiryid, string companyid, bool payment_status)
        {
            try
            {
                string tokenKey = GetrAuthenticateToken(userId, password, agencyID);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    return HealthProductAPI.GetProposalAndPolicy(agencyID, enquiryid, pp_referenceId, tokenKey, companyid, payment_status);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static ABHISaveProposal API_Save_Perposal(string productid, string companyid, string enquiryid, string userid, string password, string agencyId)
        {
            ABHISaveProposal response = new ABHISaveProposal();
            try
            {
                string tokenKey = GetrAuthenticateToken(userid, password, agencyId);
                string repo_json = HealthProductAPI.AdityBirla_SaveProposal(productid, companyid, enquiryid, agencyId, tokenKey);
                if (!string.IsNullOrEmpty(repo_json))
                {
                    response = JsonConvert.DeserializeObject<ABHISaveProposal>(repo_json);
                    if (response.success)
                    {
                        if (response.response.data != null)
                        {
                            string referenceId = response.response.data.referenceId;
                            string proposalNum = response.response.data.proposalNum;
                            string receiptAmount = response.response.data.receiptAmount;
                            string receiptNumber = response.response.data.receiptNumber;
                            string policyNumber = response.response.data.policyNumber;
                            string policyStatus = response.response.data.policyStatus;
                            string inquiry_id = response.response.data.inquiry_id;

                            bool isuccess = Post_Health.UpdateAdityBirlaSaveProposal_Respo(referenceId, proposalNum, receiptAmount, receiptNumber, policyNumber, policyStatus, inquiry_id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
    }
}