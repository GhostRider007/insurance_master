﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using insurance.Models.Common;

namespace insurance.HealthApi
{
    public static class ConnectToDatabase
    {
        #region [Connection Strings]
        public static SqlConnection HealthDBConnection = new SqlConnection(Config.SeeInsuredConString);
        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }
        public static void HealthOpenConnection()
        {
            if (HealthDBConnection.State == ConnectionState.Closed)
            {
                HealthDBConnection.Open();
            }
        }
        public static void HealthCloseConnection()
        {
            if (HealthDBConnection.State == ConnectionState.Open)
            {
                HealthDBConnection.Close();
            }
        }
        #endregion

        #region [Common Section]       
        public static bool InsertRecordToTable(string fields, string values, string tableName)
        {
            try
            {
                sqlCommand = new SqlCommand("usp_InsertRecordToTable", HealthDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("fileds", fields);
                sqlCommand.Parameters.AddWithValue("values", values);
                sqlCommand.Parameters.AddWithValue("tablename", tableName);

                HealthOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                HealthCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static int InsertRecordIntoAnyTableGetID(string fields, string values, string tableName)
        {
            try
            {
                sqlCommand = new SqlCommand("usp_InsertRecordToTableReturnID", HealthDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@fileds", fields));
                sqlCommand.Parameters.Add(new SqlParameter("@values", values));
                sqlCommand.Parameters.Add(new SqlParameter("@tablename", tableName));
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                HealthOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                HealthCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool UpdateRecordIntoAnyTable(string tableName, string fieldsWithValue, string whereCondition)
        {
            try
            {
                sqlCommand = new SqlCommand("ups_UpdateRecordToTable", HealthDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("TableName", tableName);
                sqlCommand.Parameters.AddWithValue("FieldsWithValue", fieldsWithValue);
                sqlCommand.Parameters.AddWithValue("WhereCondition", whereCondition);

                HealthOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                HealthCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetSimpleRecord(string columnName, string tableName, string whereCondition)
        {
            try
            {
                sqlCommand = new SqlCommand("usp_GetRecordFromTable", HealthDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("fileds", columnName);
                sqlCommand.Parameters.AddWithValue("tablename", tableName);
                sqlCommand.Parameters.AddWithValue("where", whereCondition);
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool DeleteRecordFromAnyTable(string tableName, string whereCondition)
        {
            try
            {
                sqlCommand = new SqlCommand("usp_DeleteRecordFromTable", HealthDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("tablename", tableName);
                sqlCommand.Parameters.AddWithValue("where", whereCondition);

                HealthOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                HealthCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [Log Section]
        public static bool Logger(string status, string postUrl, string requestRemark, string requestJson, string responseJson, string serviceType, string agencyId, string trackId, string errorMsg = null)
        {
            try
            {
                string fields = "Status,PostUrl,RequestRemark,RequestJson,ResponseJson,ServiceType,AgencyId,TrackId,ErrorMsg";
                string values = "'" + status + "','" + postUrl + "','" + requestRemark + "','" + requestJson + "','" + responseJson + "','" + serviceType + "','" + agencyId + "','" + trackId + "','" + errorMsg + "'";
                string tableName = "T_APILogInfo";

                return InsertRecordToTable(fields, values, tableName);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion
    }
}