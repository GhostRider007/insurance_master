﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Web;
using System.Xml;
using ecommerce.Models.Common;
using insurance.Helper.DataBase;
using insurance.Helper.HealthHelper;
using insurance.Models.Common;
using Newtonsoft.Json;
using Post_Utility.Health;
using Post_Utility.Utility;
using static insurance.Models.Health.HealthModel;

namespace insurance.HealthApi
{
    public static class HealthProductAPI
    {
        public static string GetAuthenticate(string username, string password, string agencyId, string postUrl)
        {
            try
            {
                string access_token = string.Empty;
                if (IsAccessTokenExist(ref access_token))
                {
                    return access_token;
                }
                else
                {
                    string response = HealthApiUtility.GetrAuthenticateToken(username, password, agencyId, postUrl);
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic json_access_token = JsonConvert.DeserializeObject(response);
                        access_token = json_access_token.response.tokenvalue;
                        InitializeAccessTokenSession(access_token);
                        return access_token;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static void InitializeAccessTokenSession(string accessToken)
        {
            if (!string.IsNullOrEmpty(accessToken))
            {
                List<string> objAccessToken = new List<string>();
                objAccessToken.Add(accessToken);
                objAccessToken.Add(DateTime.Now.ToString());
                objAccessToken.Add("exist");
                HttpContext.Current.Session["accessToken"] = objAccessToken;
            }
        }
        public static bool IsAccessTokenExist(ref string token)
        {
            if (HttpContext.Current.Session["accessToken"] != null)
            {
                List<string> objAccessToken = (List<string>)HttpContext.Current.Session["accessToken"];
                TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
                if (TS.Minutes < 59)
                {
                    token = objAccessToken[0];
                    return true;
                }
                else
                {
                    HttpContext.Current.Session["accessToken"] = null;
                }
            }

            return false;
        }
        public static void InitializeCurrentEnquiryId(string enquiryId)
        {
            if (!string.IsNullOrEmpty(enquiryId))
            {
                List<string> objAccessToken = new List<string>();
                objAccessToken.Add(enquiryId);
                objAccessToken.Add(DateTime.Now.ToString());
                objAccessToken.Add("exist");
                HttpContext.Current.Session["enquiryId"] = objAccessToken;
            }
        }
        public static bool IsCurrentEnquiryIdExist(ref string enquiryId)
        {
            if (HttpContext.Current.Session["enquiryId"] != null)
            {
                List<string> objAccessToken = (List<string>)HttpContext.Current.Session["enquiryId"];
                TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
                if (TS.Minutes < 59)
                {
                    enquiryId = objAccessToken[0];
                    return true;
                }
                else
                {
                    HttpContext.Current.Session["enquiryId"] = null;
                }
            }

            return false;
        }
        public static string GetHealthApiResponse(string agencyId, NewEnquiry_Health enqDetail, string tokenKey)
        {
            try
            {
                string healthCacheRespo = GetHealth_RespoFromCache();
                if (!string.IsNullOrEmpty(healthCacheRespo))
                {
                    return healthCacheRespo;
                }
                else
                {
                    string requestJson = GenrateHealthRequestRowData(enqDetail);
                    string response = HealthApiUtility.GetHealthSearchListing(requestJson, agencyId, tokenKey);
                    if (!string.IsNullOrEmpty(response))
                    {
                        var isSuccess = ConnectToDataBase.InsertAndUpdateHealthResponse(enqDetail.Enquiry_Id, requestJson, response, "search", "api");

                        if (Convert.ToBoolean(isSuccess))
                        {
                            StoreHealth_RespoInCache(response);
                        }

                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetHealthPerposalResponse(string agencyId, string enquiryid, string tokenKey, string encyEnquiryid)
        {
            try
            {
                string policyStartEndDetail = string.Empty;
                string requestJson = GenrateHealthPerposalRowData(enquiryid, encyEnquiryid, ref policyStartEndDetail);
                if (Post_Health.UpdateRowRequest_EnquiryResponse(requestJson.Replace("'", "''"), enquiryid))
                {
                    if (Post_Health.UpdateRowRequest_HealthProposal(requestJson.Replace("'", "''"), enquiryid))
                    {
                        string response = HealthApiUtility.CreateHealthProposalResponse(requestJson, agencyId, tokenKey);
                        if (!string.IsNullOrEmpty(response))
                        {
                            if (Post_Health.UpdateResponse_EnquiryResponse(response.Replace("'", "''"), enquiryid))
                            {
                                if (Post_Health.UpdateStartEndPolicyDate(enquiryid, policyStartEndDetail))
                                {
                                    bool issuccess = Post_Health.UpdateResponse_HealthProposal(response.Replace("'", "''"), enquiryid);
                                }
                            }
                            return response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GenrateHealthPaymentUrl(string agencyId, string enquiryid, string raw_paymenturl, string tokenKey)
        {
            if (Post_Health.UpdateRaw_Paymenturl_EnquiryResponse(raw_paymenturl, enquiryid))
            {
                string respo = HealthApiUtility.PostHealtApiWithoutBody(raw_paymenturl, tokenKey);
                if (!string.IsNullOrEmpty(respo))
                {
                    if (Post_Health.UpdatePaymentResponse_EnquiryResponse(respo, enquiryid))
                    {
                        return respo;
                    }
                }
            }

            return string.Empty;
        }
        public static string GetProposalAndPolicy(string agencyId, string enquiryid, string referenceId, string tokenKey, string companyid, bool payment_status)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(referenceId))
                {
                    if (companyid == "2")
                    {
                        string resqUrl = "http://api.seeinsured.com/api/Care/PolicyStatus/" + referenceId + "/" + enquiryid;
                        if (Post_Health.UpdateProposalAndPolicyRequset_EnquiryResponse(resqUrl, enquiryid))
                        {
                            string respo = HealthApiUtility.PostHealtApiWithoutBody(resqUrl, tokenKey);
                            if (!string.IsNullOrEmpty(respo))
                            {
                                if (Post_Health.UpdateProposalAndPolicyResponse_EnquiryResponse(respo, enquiryid))
                                {
                                    CarePaymentStatusCheck respoPAP = JsonConvert.DeserializeObject<CarePaymentStatusCheck>(respo);
                                    if (respoPAP.response.intGetPolicyStatusIO != null)
                                    {
                                        string proposal_Number = respoPAP.response.intGetPolicyStatusIO.proposalNum;
                                        string policy_Number = respoPAP.response.intGetPolicyStatusIO.policyNum;
                                        string policyStatus = respoPAP.response.intGetPolicyStatusIO.policyStatus;

                                        if (Post_Health.UpdatePaymentDetail_HealthProposal(proposal_Number, policy_Number, enquiryid))
                                        {
                                            if (payment_status)
                                            {
                                                if (!string.IsNullOrEmpty(proposal_Number) || !string.IsNullOrEmpty(policy_Number))
                                                {
                                                    result = "<h5 class='text-success'>Proposal Number : " + (!string.IsNullOrEmpty(proposal_Number) ? proposal_Number : "- - -") + "</h5><h5 class='text-success'>Policy Number : " + (!string.IsNullOrEmpty(policy_Number) ? policy_Number : "- - -") + "</h5>";
                                                }
                                                else
                                                {
                                                    result = "failed";
                                                }
                                            }
                                            else
                                            {
                                                result = "<h6 class='text-danger'>You haven't payment yet, Please payment first.</h6>";
                                            }

                                            //if (policyStatus.ToLower().Trim().Contains("inforce"))
                                            //{
                                            //    if (!string.IsNullOrEmpty(proposal_Number) || !string.IsNullOrEmpty(policy_Number))
                                            //    {
                                            //        result = "<h5 class='text-success'>Proposal Number : " + (!string.IsNullOrEmpty(proposal_Number) ? proposal_Number : "- - -") + "</h5><h5 class='text-success'>Policy Number : " + (!string.IsNullOrEmpty(policy_Number) ? policy_Number : "- - -") + "</h5>";
                                            //    }
                                            //}
                                            //else if (policyStatus.ToLower().Trim().Contains("pending"))
                                            //{
                                            //    result = "<h5 class='text-danger'>You haven't payment yet, Please payment first.</h5>";
                                            //}
                                            //else
                                            //{
                                            //    result = "failed";
                                            //}
                                        }
                                    }
                                    else
                                    {
                                        result = "notpay";
                                    }
                                }
                            }
                        }
                    }
                    else if (companyid == "4")
                    {
                        CreateHealth_Proposal perposal = Health_Helper.GetProposalDetail(enquiryid);
                        if (!string.IsNullOrEmpty(perposal.pay_proposal_Number) || !string.IsNullOrEmpty(perposal.pay_policy_Number))
                        {
                            result = "<h5 class='text-success'>Proposal Number : " + perposal.pay_proposal_Number + "</h5><h5 class='text-success'>Policy Number : " + perposal.pay_policy_Number + "</h5>";
                        }
                        else
                        {
                            result = "failed";
                        }
                    }
                    else
                    {
                        string resqUrl = "http://api.seeinsured.com/api/StarHealth/GetProposalAndPolicy/" + referenceId + "/" + enquiryid;
                        if (Post_Health.UpdateProposalAndPolicyRequset_EnquiryResponse(resqUrl, enquiryid))
                        {
                            string respo = HealthApiUtility.PostHealtApiWithoutBody(resqUrl, tokenKey);
                            if (!string.IsNullOrEmpty(respo))
                            {
                                if (Post_Health.UpdateProposalAndPolicyResponse_EnquiryResponse(respo, enquiryid))
                                {
                                    ProposalAndPolicy respoPAP = JsonConvert.DeserializeObject<ProposalAndPolicy>(respo);
                                    if (respoPAP.response != null)
                                    {
                                        if (payment_status)
                                        {
                                            if (respoPAP.response.data != null)
                                            {
                                                string proposal_Number = respoPAP.response.data.proposal_Number;
                                                string policy_Number = respoPAP.response.data.policy_Number;
                                                if (Post_Health.UpdatePaymentDetail_HealthProposal(proposal_Number, policy_Number, enquiryid))
                                                {
                                                    if (!string.IsNullOrEmpty(proposal_Number) || !string.IsNullOrEmpty(policy_Number))
                                                    {
                                                        result = "<h5 class='text-success'>Proposal Number : " + (!string.IsNullOrEmpty(proposal_Number) ? proposal_Number : "- - -") + "</h5><h5 class='text-success'>Policy Number : " + (!string.IsNullOrEmpty(policy_Number) ? policy_Number : "- - -") + "</h5>";
                                                    }
                                                    else
                                                    {
                                                        result = "failed";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            result = "notpay";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                result = "failed";
            }
            return result;
        }
        public static string Genrate_HealthPdfDocument(string agencyId, string enquiryid, string tokenKey, string p_referenceId, string companyid, ref string pdfdocurl)
        {
            return HealthApiUtility.Genrate_HealthPdfDocument(enquiryid, tokenKey, p_referenceId, companyid, ref pdfdocurl);
        }
        private static string GenrateHealthRequestRowData(NewEnquiry_Health enqDetail)
        {
            StringBuilder requestJson = new StringBuilder();

            try
            {
                requestJson.Append("{\"inquiry_id\": \"" + enqDetail.Enquiry_Id + "\",");
                requestJson.Append("\"selfAge\":" + enqDetail.SelfAge);
                requestJson.Append(",\"spouseAge\":" + enqDetail.SpouseAge);

                if (enqDetail.Son)
                {
                    requestJson.Append(",\"sonAge\":[");
                    if (enqDetail.Son1Age > 0)
                    {
                        requestJson.Append(enqDetail.Son1Age);
                    }
                    if (enqDetail.Son2Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son2Age);
                    }
                    if (enqDetail.Son3Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son3Age);
                    }
                    if (enqDetail.Son4Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son4Age);
                    }
                    requestJson.Append("]");
                }
                else
                {
                    requestJson.Append(",\"sonAge\":[");
                    requestJson.Append(enqDetail.Son1Age);
                    requestJson.Append("]");
                }
                if (enqDetail.Daughter)
                {
                    requestJson.Append(",\"daughterAge\":[");
                    if (enqDetail.Daughter1Age > 0)
                    {
                        requestJson.Append(enqDetail.Daughter1Age);
                    }
                    if (enqDetail.Daughter2Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter2Age);
                    }
                    if (enqDetail.Daughter3Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter3Age);
                    }
                    if (enqDetail.Daughter4Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter4Age);
                    }
                    requestJson.Append("]");
                }
                else
                {
                    requestJson.Append(",\"daughterAge\":[");
                    requestJson.Append(enqDetail.Daughter1Age);
                    requestJson.Append("]");
                }
                requestJson.Append(",\"fatherAge\":" + enqDetail.FatherAge);
                requestJson.Append(",\"motherAge\":" + enqDetail.MotherAge);
                requestJson.Append(",\"suminsured\":\"" + enqDetail.Sum_Insured + "\"");
                requestJson.Append(",\"policytype\":\"" + enqDetail.Policy_Type.ToUpper() + "\"");
                requestJson.Append(",\"postalcode\":" + enqDetail.Pin_Code);
                requestJson.Append("}");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return requestJson.ToString();
        }
        private static string GenrateHealthPerposalRowData(string enquiryid, string encyEnquiryid, ref string policyStartEnd)
        {
            StringBuilder rowData = new StringBuilder();

            NewEnquiry_Health EnquryDetails = Health_Helper.GetNewEnquiryHealthDetails(enquiryid);
            CreateHealth_Proposal PerposorDetail = Health_Helper.GetProposalDetail(enquiryid);
            List<Health_Insured> InsuredList = Health_Helper.GetInsuredDetail(enquiryid);
            List<Datum> resRespo = JsonConvert.DeserializeObject<List<Datum>>(EnquryDetails.selectedproductjson);
            List<AddOne> addOneList = Health_Helper.GetAddOneList(enquiryid);
            if (resRespo != null && resRespo.Count > 0)
            {
                foreach (var item in resRespo)
                {
                    if (item.totalPremium == PerposorDetail.premium)
                    {
                        int tempPeriod = item.product.period;
                        string policyStartDate = DateTime.Now.AddDays(2).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
                        string policyEndDate = DateTime.Now.AddDays(tempPeriod * 365).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");

                        policyStartEnd = "starton_date='" + DateTime.Now.AddDays(2).ToString("dd-MM-yyyy") + "',endon_date='" + DateTime.Now.AddDays(tempPeriod * 365).ToString("dd-MM-yyyy") + "'";

                        rowData.Append("{");
                        //rowData.Append("\"id\": " + item.id + ",\"productDetailId\": " + item.product.productDetailId + ",\"quoteId\": \"" + item.quoteId + "\",");
                        rowData.Append("\"id\": " + item.id + ",\"productDetailId\": \"" + item.product.productDetailId + "\",\"quoteId\": \"" + item.quoteId + "\",");

                        string pgender = PerposorDetail.title.ToLower() == "mr." ? "MALE" : "FEMALE";

                        rowData.Append("\"proposerDetails\": {\"firstName\": \"" + PerposorDetail.firstname + "\",\"lastName\": \"" + PerposorDetail.lastname + "\",\"middleName\": \"\",\"gender\": \"" + pgender + "\",\"mobileNumber\": \"" + PerposorDetail.mobile + "\",");
                        rowData.Append("\"email\": \"" + PerposorDetail.emailid + "\",\"addLine1\": \"" + PerposorDetail.address1 + "\",\"addLine2\": \"" + PerposorDetail.address2 + "\",\"landmark\": \"" + PerposorDetail.landmark + "\",\"postalCode\": " + PerposorDetail.pincode + ",\"areaId\": \"" + PerposorDetail.areaid + "\",\"zoneCd\": \"\", ");
                        rowData.Append("\"state\": \"" + PerposorDetail.statename + "\",\"city\": \"" + PerposorDetail.cityname + "\",\"proposerDob\": \"" + CommonClass.ConvertDateToISOFormate(PerposorDetail.dob) + "\",\"MaritalStatus\":\"" + PerposorDetail.mstatus + "\",\"panNumber\": \"" + PerposorDetail.panno + "\",\"aadharNumber\": \"" + PerposorDetail.aadharno + "\",");
                        rowData.Append("\"previousMedicalInsurance\": \"\",\"annualIncome\": \"" + PerposorDetail.annualincome + "\",\"criticalIllness\": \"false\"},");

                        rowData.Append("\"starton\": \"" + policyStartDate + "\",\"endon\": \"" + policyEndDate + "\",");

                        rowData.Append("\"nomineeDetails\": {\"nomineeFirstName\": \"" + PerposorDetail.nFirstName + "\",\"nomineeLastName\": \"" + PerposorDetail.nLastName + "\",\"nomineeRelation\": \"" + PerposorDetail.nRelation + "\",\"nomineeAge\": \"" + PerposorDetail.nAge + "\"},");

                        rowData.Append("\"insureds\": [");

                        int insurloop = 1;
                        foreach (var insured in InsuredList)
                        {
                            string kkMedHistory = string.Empty;
                            if (!string.IsNullOrEmpty(insured.medicalhistory))
                            {
                                kkMedHistory = insured.medicalhistory.Replace("\n", "").Replace("\r", "");
                            }

                            string gender = insured.title.ToLower() == "mr." ? "MALE" : "FEMALE";
                            rowData.Append("{\"firstName\": \"" + insured.firstname + "\",\"lastName\": \"" + insured.lastname + "\",\"dob\": \"" + CommonClass.ConvertDateToISOFormate(insured.dob) + "\",\"gender\": \"" + gender + "\",\"illness\": \"" + (insured.illness == true ? insured.illnessdesc : insured.illness.ToString()) + "\",\"relationshipId\": \"" + insured.relationid + "\",\"occupationId\": \"" + insured.occupationId + "\",\"ispersonalAccidentApplicable\": \"" + (insurloop == 1 ? "true" : "false") + "\",\"engageManualLabour\": \"" + (insured.engageManualLabour == true ? insured.engageManualLabourDesc : insured.engageManualLabour.ToString()) + "\",\"engageWinterSports\": \"" + (insured.engageWinterSports == true ? insured.engageWinterSportDesc : insured.engageWinterSports.ToString()) + "\",\"height\": " + insured.height + ",\"weight\": " + insured.weight + ",\"buyBackPED\": 0,\"questionsList\": " + (!string.IsNullOrEmpty(kkMedHistory) ? kkMedHistory : "[]") + "}");

                            if (insurloop != InsuredList.Count)
                            {
                                rowData.Append(",");
                            }
                            insurloop = insurloop + 1;
                        }
                        rowData.Append("],");
                        //rowData = rowData.Replace("},", "}],");

                        rowData.Append("\"discountPercent\": \"" + item.discountPercent + "\",");
                        rowData.Append("\"premium\": \"" + item.premium + "\",");
                        rowData.Append("\"serviceTax\": \"" + item.serviceTax + "\",");
                        rowData.Append("\"totalPremium\": \"" + item.totalPremium + "\",");
                        rowData.Append("\"schemaId\": \"" + item.schemaId + "\",");
                        //rowData.Append("\"insuranceCompany\": \"" + item.insuranceCompany + "\",");
                        rowData.Append("\"insuranceCompanyId\": " + item.insuranceCompanyId + ",");
                        rowData.Append("\"policyType\": \"" + item.policyType + "\",");
                        rowData.Append("\"suminsured\": " + item.suminsured + ",");
                        rowData.Append("\"suminsuredId\": " + item.suminsuredId + ",");

                        rowData.Append("\"products\": {\"policyTypeName\": \"" + item.product.policyTypeName + "\",\"policyName\": \"" + item.product.policyName + "\",\"period\": \"" + item.product.period + "\"},");

                        rowData.Append("\"addOns\":\"");

                        if (addOneList != null && addOneList.Count > 0)
                        {
                            int addonloop = 1;
                            foreach (var addon in addOneList)
                            {
                                if (addonloop == addOneList.Count())
                                {
                                    rowData.Append(addon.code);
                                }
                                else
                                {
                                    rowData.Append(addon.code + ",");
                                }
                                addonloop = addonloop + 1;
                            }
                        }

                        rowData.Append("\",");

                        rowData.Append("\"productTerms\":[");

                        int termcount = item.productTerms.Count;
                        int termloop = 1;
                        if (termcount > 0)
                        {
                            foreach (var terms in item.productTerms)
                            {
                                if (termloop != termcount)
                                {
                                    rowData.Append("{\"planName\":\"" + terms.planName + "\", \"sumInsuredMin\":" + terms.sumInsuredMin + ", \"sumInsuredMax\":" + terms.sumInsuredMax + ", \"ageGroupMin\":" + terms.ageGroupMin + ",\"ageGroupMax\":" + terms.ageGroupMax + ", \"medical_Tests\":\"" + terms.medical_Tests + "\", \"isCritical_Ilness\":" + terms.isCritical_Ilness.ToString().ToLower() + "},");
                                }
                                else
                                {
                                    if (termloop == termcount)
                                    {
                                        rowData.Append("{\"planName\":\"" + terms.planName + "\", \"sumInsuredMin\":" + terms.sumInsuredMin + ", \"sumInsuredMax\":" + terms.sumInsuredMax + ", \"ageGroupMin\":" + terms.ageGroupMin + ",\"ageGroupMax\":" + terms.ageGroupMax + ", \"medical_Tests\":\"" + terms.medical_Tests + "\", \"isCritical_Ilness\":" + terms.isCritical_Ilness.ToString().ToLower() + "}");
                                    }
                                }
                                termloop = termloop + 1;
                            }
                        }
                        rowData.Append("],");

                        rowData.Append("\"inquiry_id\": \"" + encyEnquiryid + "\"");

                        rowData.Append("}");
                        break;
                    }
                }
            }

            return rowData.ToString();
        }
        public static string AdityBirla_SaveProposal(string productid, string companyid, string enquiryid, string agencyId, string tokenKey)
        {
            return HealthApiUtility.AdityBirla_Save_Perposal(productid, companyid, enquiryid, agencyId, tokenKey);
        }

        private static string _key = "health_respo";
        private static readonly MemoryCache _cache = MemoryCache.Default;
        public static void StoreHealth_RespoInCache(string healthResponse)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);

            _cache.Add(_key, healthResponse, cacheItemPolicy);
        }
        public static string GetHealth_RespoFromCache()
        {
            if (!_cache.Contains(_key))
            {
                RemoveHealth_RespoFromCache();
                return (string)_cache.Get(_key);
            }

            return _cache.Get(_key) as string;
        }
        public static void RemoveHealth_RespoFromCache()
        {
            if (string.IsNullOrEmpty(_key))
            {
                _cache.Dispose();
            }
            else
            {
                _cache.Remove(_key);
            }
        }
        public static string GetHealthQuestionaries(CreateHealth_Proposal proposal, string agencyId, string tokenKey)
        {
            string strRequest = HealthApiUtility.GetQuestionariesUrl(proposal.companyid, proposal.productid);
            var isSuccess = Post_Health.UpdateQuestionnaireRequest_EnquiryResponse(strRequest, proposal.enquiryid);

            string respo = HealthApiUtility.GetHealthQuestionariesList(strRequest, tokenKey);

            if (!string.IsNullOrEmpty(respo))
            {
                var isRespoSuccess = Post_Health.UpdateQuestionnaireResponse_EnquiryResponse(respo.Replace("'", "''"), proposal.enquiryid);
            }

            return respo;
        }
        public static string GetCityListFromStarHealth(string tokenKey, string pincode)
        {
            return HealthApiUtility.GetCityListFromStarHealth(tokenKey, pincode);
        }
        public static string GetAreaListFromStarHealth(string tokenKey, string pincode, string cityid)
        {
            return HealthApiUtility.GetAreaListFromStarHealth(tokenKey, pincode, cityid);
        }
    }
}