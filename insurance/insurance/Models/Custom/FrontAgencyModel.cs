﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Models.Custom
{
    public class FrontAgencyModel
    {
        public class Agency_Detail
        {
            public string AgencyID { get; set; }
            public string AgencyIdNew { get; set; }
            public string AgencyName { get; set; }
            public string Address { get; set; }
            public string CountryID { get; set; }
            public string CountryName { get; set; }
            public string StateID { get; set; }
            public string StateName { get; set; }
            public string CityID { get; set; }
            public string CityName { get; set; }
            public string Pincode { get; set; }
            public string GSTNo { get; set; }
            public string BusinessEmailID { get; set; }
            public string BusinessPhone { get; set; }
            public int ReferenceBy { get; set; }
            public string ReferenceByName { get; set; }
            public string BusinessAddressProofImg { get; set; }
            public string BusinessPanCardImg { get; set; }
            public string PanNo { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailID { get; set; }
            public string Mobile { get; set; }
            public string PersonalAddressProofImg { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string CompanyLogoImg { get; set; }
            public string Type { get; set; }
            public string CreditLimit { get; set; }
            public string TempCreditLimit { get; set; }
            public string TempCreditDate { get; set; }
            public string GroupType { get; set; }
            public string LastLoginDate { get; set; }
            public bool IsApproved { get; set; }
            public bool BookingStatus { get; set; }
            public string TempApiKey { get; set; }
            public string EncryKey { get; set; }
            public string IVKey { get; set; }
            public bool Status { get; set; }
            public string CreatedDate { get; set; }
            public string UpdatedDate { get; set; }
        }
        public class EmulateLogin
        {
            public string EmulateId { get; set; }
            public int Agencyid { get; set; }
            public string AgencyName { get; set; }
            public string staffid { get; set; }
            public string StaffName { get; set; }
            public string Remarks { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string TempOTP { get; set; }
            public string OTP { get; set; }
            public string OTPExpTime { get; set; }
            public string LoginToDate { get; set; }
            public string LoginFormDate { get; set; }
            public DateTime loginDate { get; set; }
            public string BusinessEmailID { get; set; }
            public string AgencyAddress { get; set; }
            public string BusinessPhone { get; set; }
            public string EncryKey { get; set; }
            public string actionType { get; set; }
            public List<EmulateLogin> EmulateLoginList { get; set; }
        }

        public class BankDeatil
        {
            public int id { get; set; }

            public string AgencyID { get; set; }

            public string BankName { get; set; }

            public string Branch { get; set; }

            public string AccountNo { get; set; }

            public string MobileNo { get; set; }
            public string IFSC { get; set; }

            public bool Isverified { get; set; }
            public bool Status { get; set; }
            public List<SelectListItem> BankNameList { get; set; }
        }

        public class BankNameModel
        {
            public int id { get; set; }

            public string Type { get; set; }

            public string BankName { get; set; }

           
        }

        public class CreateAgency
        {
            public int IntAgencyID { get; set; }
            public string AgencyID { get; set; }
            public string AgencyName { get; set; }
            public string Address { get; set; }
            public string Branch { get; set; }
            public string BranchName { get; set; }
            public string FullAddress { get; set; }
            public int CountryID { get; set; }
            public string CountryName { get; set; }
            public string StateName { get; set; }
            public string CityName { get; set; }
            public int StateID { get; set; }
            public int CityID { get; set; }
            public string Pincode { get; set; }
            public string GSTNo { get; set; }
            public string BusinessEmailID { get; set; }
            public string BusinessPhone { get; set; }
            public string RefreceByName { get; set; }
            public int ReferenceBy { get; set; }
            public string BusinessAddressProofImg { get; set; }
            public string BusinessAddressProofImgShow { get; set; }
            public string BusinessPanCardImg { get; set; }
            public string BusinessPanCardImgShow { get; set; }
            public string PanNo { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailID { get; set; }
            public string Mobile { get; set; }
            public string PersonalAddressProofImg { get; set; }
            public string PersonalAddressProofImg2 { get; set; }
            public string PersonalAddressProofImgShow { get; set; }
            public string UserName { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string CurrentPassword { get; set; }
            public string RegUserId { get; set; }
            public string RegPassword { get; set; }
            public string ConfPassword { get; set; }
            public string CompanyLogoImg { get; set; }
            public string CompanyLogoImgShow { get; set; }
            public bool Status { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? UpdatedDate { get; set; }
            public decimal? CreditLimit { get; set; }
            public decimal? TempCreditLimit { get; set; }
            public DateTime? TempCreditDate { get; set; }
            public string GroupType { get; set; }
            public string GroupTypeName { get; set; }
            public string LastLoginDate { get; set; }
            public string AgencyLastLoginDate { get; set; }

            public bool IsApproved { get; set; }
            public bool BookingStatus { get; set; }
            public string TempApiKey { get; set; }
            public string AgencyCode { get; set; }
            public string Remarks { get; set; }
            public List<SelectListItem> CountryList { get; set; }
            public List<SelectListItem> StateList { get; set; }
            public List<SelectListItem> CityList { get; set; }
            public List<SelectListItem> RefByList { get; set; }
            public List<SelectListItem> GroupTypeList { get; set; }
            public List<SelectListItem> branchList { get; set; }
            public List<CreateAgency> AgencyList { get; set; }
        }
        public class Country
        {
            public int CountryID { get; set; }

            public string CountryName { get; set; }

            public string CountryCode { get; set; }

            public bool Status { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime UpdatedDate { get; set; }
        }
        public class State
        {
            public int StateID { get; set; }

            public string StateName { get; set; }

            public string StateCode { get; set; }

            public bool Status { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime UpdatedDate { get; set; }

            public int CountryID { get; set; }
        }
        public class City
        {
            public int CityID { get; set; }

            public string CityName { get; set; }

            public string CityCode { get; set; }

            public int StateID { get; set; }
            public string StateName { get; set; }

            public bool Status { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime UpdatedDate { get; set; }
        }
        public class StaffModel
        {
            public int StaffID { get; set; }

            public string StaffName { get; set; }

            public string StaffRole { get; set; }

            public bool Status { get; set; }

            public DateTime CreatedDate { get; set; }

            public DateTime UpdatedDate { get; set; }

            public string Mobile { get; set; }

            public string EmailID { get; set; }

            public string UserID { get; set; }

            public string Password { get; set; }

            public string Type { get; set; }

            public bool IsWelcome { get; set; }

            public string ImageUrl { get; set; }

            public string PostingPlace { get; set; }
            public List<StaffModel> StaffList { get; set; }
        }
        public class LeadRequestModel
        {
            public int RequestId { get; set; }
            public string AgencyName { get; set; }
            public string AgencyID { get; set; }
            public string CreatedDate { get; set; }
            public string TempDate { get; set; }
            public string StaffID { get; set; }
            public string StaffName { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string ContactNo { get; set; }
            public string Age { get; set; }
            public string Gender { get; set; }
            public string Service { get; set; }
            public string Remark { get; set; }
            public string FormDate { get; set; }
            public string ToDate { get; set; }

            public List<LeadRequestModel> LeadRequestList{ get; set; }
        }

        public class ProposalDetails
        {
            public int Id { get; set; }
            public string policypdflink { get; set; }
            public string policynumber { get; set; }
            public string AgencyID { get; set; }
            public string AgencyName { get; set; }
            public string CreatedDate { get; set; }
            public string ProposalNo { get; set; }
            public string enquiry_id { get; set; }
            public string BlockID { get; set; }
            public string ModelName { get; set; }
            public string VarientName { get; set; }
            public string vehicletype { get; set; }
            public string FirstName { get; set; }
            public string lastName { get; set; }
            public string ChainId { get; set; }
            public string PremimumAmount { get; set; }
            public string CreatedDateFrom { get; set; }
            public string CreatedDateTo { get; set; }
            public string Policy_Type_Text { get; set; }
            public string suminsured { get; set; }
            public string premium { get; set; }
            public string companyid { get; set; }
            public List<ProposalDetails> Proposallist { get; set; }
        }

        public class DashboardDetails
        {

            public string Fuel { get; set; }
            public string VarientName { get; set; }
            public string BrandName { get; set; }
            public string vehicletype { get; set; }
            public string VechileRegNo { get; set; }
            public string ModelName { get; set; }
            public string Enquiry_Id { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string totalPremium { get; set; }
            public string Policy_Type { get; set; }
            public string service { get; set; }
            public string servicetype { get; set; }            
            public string createddate { get; set; }
            public string suminsured { get; set; }
            public string Sum_Insured { get; set; }
            public string Prodcutname { get; set; }

            public string CompanyName { get; set; }

        }

        public class ProposalDetailsHealth
        {
            
            public string AgencyID { get; set; }
            public string AgencyName { get; set; }
            public string Enquiry_Id { get; set; }
            public string BlockId { get; set; }
            public string CustomerName { get; set; } 
            public string CreatedDate_Proposal { get; set; }
            public string CreatedDate_Policy { get; set; }
            public string p_proposalNum { get; set; }
            public string pay_proposal_Number { get; set; }
            public string pay_policy_Number { get; set; }
            public string PremiumAmount { get; set; }            
            public string ProductName { get; set; }
            public string Policy_Type { get; set; }
            public string SumInsured { get; set; }
            public string companyid { get; set; }
            public string CompanyName { get; set; }
            public string policyStartDate { get; set; }
            public string policyExpiryDate { get; set; }
            public int daysLeft_expiry { get; set; }
        }

        public class ProposalDetailsMotor
        {

            public string AgencyID { get; set; }
            public string AgencyName { get; set; }
            public string Enquiry_Id { get; set; }
            public string BlockId { get; set; }
            public string CustomerName { get; set; }
            public string CreatedDate_Proposal { get; set; }
            public string CreatedDate_Policy { get; set; }
            public string ProposalNumber { get; set; }
            public string PolicyNumber { get; set; }
            public string PremiumAmount { get; set; }
            public string ProductName { get; set; }
            public string Policy_Type { get; set; }
            public string SumInsured { get; set; }
            public string CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string VehicleClass { get; set; }
            public string VehicleName { get; set; }
            public string PolicyPDFLink { get; set; }
        }

        public class TwowheelermapModel
        {
            public string MapID { get; set; }
            public string vehicleMaincode { get; set; }
            public string Make { get; set; }
            public string vehicleModel { get; set; }
            public string FuelType { get; set; }
            public string Variant { get; set; }
            public string BodyType { get; set; }
            public string SeatingCapacity { get; set; }
            public string GoDigit { get; set; }
            public string ShriRam { get; set; }
            public string IFFCOTokio { get; set; }
            public string Power { get; set; }
            public string CubicCapacity { get; set; }
            public string GrossVehicleWeight { get; set; }
            public string NoOfWheels { get; set; }
            public string Abs { get; set; }
            public string AirBags { get; set; }
            public string Length { get; set; }
            public string ExShowroomPrice { get; set; }
            public string PriceYear { get; set; }
            public string Production { get; set; }
            public string Manufacturing { get; set; }
            public string VehicleType { get; set; }
            public List<TwowheelermapModel> MapingList { get; set; }
            public List<SelectListItem> Makelist { get; set; }
            public List<SelectListItem> ModelList { get; set; }
        }
    }
}
