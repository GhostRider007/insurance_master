﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Models.Motor.BikeModel
{
    #region[BIKE API RESPONSE]

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class BaseCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class AddOnCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class LegalLiability
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class AccessoriesCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string maxallow { get; set; }
        public string minallow { get; set; }
    }

    public class ProductDetails
    {
        public string insuranceProductCode { get; set; }
        public string subInsuranceProductCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string policyHolderType { get; set; }
        public bool isNCBTransfer { get; set; }
        public string quotationDate { get; set; }
        public object purchaseDate { get; set; }
        public string pincode { get; set; }
        public string grossPremium { get; set; }
        public string netPremium { get; set; }
        public string totalTax { get; set; }
        public List<BaseCover> BaseCovers { get; set; }
        public List<AddOnCover> AddOnCovers { get; set; }
        public List<LegalLiability> legalLiabilitys { get; set; }
        public List<AccessoriesCover> AccessoriesCovers { get; set; }
    }

    public class OtherDiscount
    {
        public string discountType { get; set; }
        public double discountPercent { get; set; }
        public string discountAmount { get; set; }
    }

    public class Discounts
    {
        public object discountType { get; set; }
        public double discountPercent { get; set; }
        public object discountAmount { get; set; }
        public string specialDiscountAmount { get; set; }
        public List<OtherDiscount> otherDiscounts { get; set; }
    }

    public class Error
    {
        public int errorCode { get; set; }
        public int httpCode { get; set; }
        public List<object> validationMessages { get; set; }
        public object errorLink { get; set; }
        public object errorStackTrace { get; set; }
    }

    public class Loadings
    {
        public string totalLoadingAmount { get; set; }
        public List<object> otherLoadings { get; set; }
    }

    public class MotorBreakIn
    {
        public bool isBreakin { get; set; }
        public bool isPreInspectionWaived { get; set; }
        public bool isPreInspectionCompleted { get; set; }
        public bool isDocumentUploaded { get; set; }
    }

    public class PospInfo
    {
        public bool isPOSP { get; set; }
    }

    public class PreInspection
    {
        public bool isPreInspectionOpted { get; set; }
        public bool isPreInspectionRequired { get; set; }
        public bool isPreInspectionEligible { get; set; }
        public bool isPreInspectionWaived { get; set; }
    }

    public class PreviousInsurer
    {
        public bool isPreviousInsurerKnown { get; set; }
        public string previousPolicyExpiryDate { get; set; }
        public string previousNoClaimBonus { get; set; }
    }

    public class ServiceTax
    {
        public string cgst { get; set; }
        public string sgst { get; set; }
        public string igst { get; set; }
        public string utgst { get; set; }
        public string totalTax { get; set; }
        public string taxType { get; set; }
    }

    public class VehicleIDV
    {
        public double idv { get; set; }
        public double defaultIdv { get; set; }
        public double minimumIdv { get; set; }
        public double maximumIdv { get; set; }
    }

    public class Vehicle
    {
        public bool isVehicleNew { get; set; }
        public string vehicleMaincode { get; set; }
        public string licensePlateNumber { get; set; }
        public string manufactureDate { get; set; }
        public string registrationDate { get; set; }
        public VehicleIDV vehicleIDV { get; set; }
        public List<object> trailers { get; set; }
        public int annualMileage { get; set; }
        public string make { get; set; }
        public string model { get; set; }
    }

    public class TwoWheelerquoteR
    {
        public string enquiryId { get; set; }
        public string PolicyType { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public object cesses { get; set; }
        public string Chainid { get; set; }
        public object ProposalNo { get; set; }
        public object Policysysid { get; set; }
        public ProductDetails ProductDetails { get; set; }
        public Discounts discounts { get; set; }
        public Error error { get; set; }
        public List<object> informationMessages { get; set; }
        public Loadings loadings { get; set; }
        public MotorBreakIn motorBreakIn { get; set; }
        public List<object> motorTransits { get; set; }
        public PospInfo pospInfo { get; set; }
        public PreInspection preInspection { get; set; }
        public PreviousInsurer previousInsurer { get; set; }
        public ServiceTax serviceTax { get; set; }
        public Vehicle vehicle { get; set; }
        public string image { get; set; }
    }

    public class BikeRoot
    {
        public List<TwoWheelerquoteR> TwoWheelerquoteRS { get; set; }
    }






    #endregion
}