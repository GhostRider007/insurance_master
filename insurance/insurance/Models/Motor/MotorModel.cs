﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Models.Motor
{
    public class MotorPaymentSuccess
    {
        public string ProposalNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicySysID { get; set; }

        public string ProposalSysID { get; set; }
        public string Status { get; set; }
        //public string errorFlag { get; set; }
        //public string errorMsg { get; set; }
        public string EnquiryId { get; set; }
        public string TotalPremium { get; set; }
        public string PaymentLink { get; set; }
        public string CompanyImage { get; set; }
        public string enquiry { get; set; }
    }
    public class MotorModel
    {
        public int Id { get; set; }
        public string Enquiry_Id { get; set; }
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string VechileRegNo { get; set; }
        public string VechileType { get; set; }
        public string InsuranceType { get; set; }
        public string MobileNo { get; set; }
        public bool Is_Term_Accepted { get; set; }
        public string RtoId { get; set; }
        public string RtoName { get; set; }
        public string DateOfReg { get; set; }
        public string DateOfMfg { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string ModelId { get; set; }
        public string ModelName { get; set; }
        public string Fuel { get; set; }
        public string VarientId { get; set; }
        public string VarientName { get; set; }
        public int PinCode { get; set; }
        public string State { get; set; }
        public bool Status { get; set; }
        public string ispreviousinsurer { get; set; }
        public string insurerid { get; set; }
        public string insurername { get; set; }
        public string policynumber { get; set; }
        public string policyexpirydate { get; set; }
        public string isclaiminlastyear { get; set; }
        public string previousyearnoclaim { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string GoDigitId { get; set; }
        public string ShriRamId { get; set; }
        public string originalpreviouspolicytype { get; set; }
        public string insurancetype { get; set; }
        public string policyholdertype { get; set; }
        public string EnquiryBookingType { get; set; }
    }

    public class MotorRTOModel
    {
        public string RTO_Code_A { get; set; }
        public string RTO_Name { get; set; }
        public string RTO_City { get; set; }
        public string RTO_State { get; set; }
        public string RTO_Country { get; set; }
        public string RTO_Code_B { get; set; }
        public List<SelectListItem> MotorRTOList { get; set; }
    }

    public class AddUpdateSelectedAddon
    {
        public string Enquiry_Id { get; set; }
        public string AddonName { get; set; }
        public string InnerName { get; set; }
        public double MinAmt { get; set; }
        public double MaxAmt { get; set; }
        public string InsuredAmt { get; set; }
    }
    public class MotorProposal
    {
        public int id { get; set; }
        public string enquiry_id { get; set; }

        public string chainid { get; set; }
        public string proposalno { get; set; }
        public string suminsured { get; set; }
        public string insurance_product { get; set; }
        public string basic { get; set; }
        public string gst { get; set; }
        public string totalpremium { get; set; }
        public string title { get; set; }
        public string gender { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mobileno { get; set; }
        public string dob { get; set; }
        public string emailid { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string landmark { get; set; }
        public string pincode { get; set; }
        public int stateid { get; set; }
        public string statename { get; set; }
        public int cityid { get; set; }
        public string cityname { get; set; }
        public bool isproposer { get; set; }
        public bool status { get; set; }
        public string nrelation { get; set; }
        public string ntitle { get; set; }
        public string nfirstname { get; set; }
        public string nlastname { get; set; }
        public string ndob { get; set; }
        public string vehicleregno { get; set; }
        public string vehiclemfdyear { get; set; }
        public string engineno { get; set; }
        public string chasisno { get; set; }
        public string valuntarydeductible { get; set; }
        public string previousinsured { get; set; }
        public string policyholdertype { get; set; }
        public bool islonelease { get; set; }
        public DateTime createddate { get; set; }
        public string gstno { get; set; }
        public string panno { get; set; }
    }
    public class MotorEnquiryResponse
    {
        public int id { get; set; }
        public string enquiry_id { get; set; }
        public string searchrequest { get; set; }
        public string response { get; set; }
        public string chainid { get; set; }
        public string preproposalno { get; set; }
        public string suminsured { get; set; }
        public string proposalrequest { get; set; }
        public string proposalresponse { get; set; }
        public string paymentrequest { get; set; }
        public string paymentcreationresponse { get; set; }
        public string applicationid { get; set; }
        public string transactionnumber { get; set; }
        public string policypdflink { get; set; }
        public string productcode { get; set; }
        public string policynumber { get; set; }
        public string proposalnumber { get; set; }
        public string paymentstatus { get; set; }
    }

    public class MotorNomineeRelation
    {
        public string issuccess { get; set; }
        public List<string> relation { get; set; }

    }

    //********************************************************
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class BaseCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class AddOnCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class LegalLiability
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string netPremium { get; set; }
    }

    public class AccessoriesCover
    {
        public bool selection { get; set; }
        public string covername { get; set; }
        public string maxallow { get; set; }
        public string minallow { get; set; }
    }

    public class ProductDetails
    {
        public string insuranceProductCode { get; set; }
        public string subInsuranceProductCode { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string policyHolderType { get; set; }
        public bool isNCBTransfer { get; set; }
        public string quotationDate { get; set; }
        public object purchaseDate { get; set; }
        public string pincode { get; set; }
        public string grossPremium { get; set; }
        public string netPremium { get; set; }
        public string totalTax { get; set; }
        public List<BaseCover> BaseCovers { get; set; }
        public List<AddOnCover> AddOnCovers { get; set; }
        public List<LegalLiability> legalLiabilitys { get; set; }
        public List<AccessoriesCover> AccessoriesCovers { get; set; }
    }

    public class OtherDiscount
    {
        public string discountType { get; set; }
        public double discountPercent { get; set; }
        public string discountAmount { get; set; }
    }

    public class Discounts
    {
        public string discountType { get; set; }
        public double discountPercent { get; set; }
        public string discountAmount { get; set; }
        public string specialDiscountAmount { get; set; }
        public List<OtherDiscount> otherDiscounts { get; set; }
    }

    public class Error
    {
        public int errorCode { get; set; }
        public int httpCode { get; set; }
        public List<object> validationMessages { get; set; }
        public object errorLink { get; set; }
        public object errorStackTrace { get; set; }
    }

    public class Loadings
    {
        public string totalLoadingAmount { get; set; }
        public List<object> otherLoadings { get; set; }
    }

    public class MotorBreakIn
    {
        public bool isBreakin { get; set; }
        public bool isPreInspectionWaived { get; set; }
        public bool isPreInspectionCompleted { get; set; }
        public bool isDocumentUploaded { get; set; }
    }

    public class PospInfo
    {
        public bool isPOSP { get; set; }
    }

    public class PreInspection
    {
        public bool isPreInspectionOpted { get; set; }
        public bool isPreInspectionRequired { get; set; }
        public bool isPreInspectionEligible { get; set; }
        public bool isPreInspectionWaived { get; set; }
    }

    public class PreviousInsurer
    {
        public bool isPreviousInsurerKnown { get; set; }
        public string previousPolicyExpiryDate { get; set; }
        public string previousNoClaimBonus { get; set; }
    }

    public class ServiceTax
    {
        public string cgst { get; set; }
        public string sgst { get; set; }
        public string igst { get; set; }
        public string utgst { get; set; }
        public string totalTax { get; set; }
        public string taxType { get; set; }
    }

    public class VehicleIDV
    {
        public double idv { get; set; }
        public double defaultIdv { get; set; }
        public double minimumIdv { get; set; }
        public double maximumIdv { get; set; }
    }

    public class Vehicle
    {
        public bool isVehicleNew { get; set; }
        public string vehicleMaincode { get; set; }
        public string licensePlateNumber { get; set; }
        public string manufactureDate { get; set; }
        public string registrationDate { get; set; }
        public VehicleIDV vehicleIDV { get; set; }
        public List<object> trailers { get; set; }
        public int annualMileage { get; set; }
        public string make { get; set; }
        public string model { get; set; }
    }

    public class PrivateCarQuotesR
    {
        public string enquiryId { get; set; }
        public string PolicyType { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public object cesses { get; set; }
        public string Chainid { get; set; }
        public string ProposalNo { get; set; }
        public string Policysysid { get; set; }
        public ProductDetails ProductDetails { get; set; }
        public Discounts discounts { get; set; }
        public Error error { get; set; }
        public List<object> informationMessages { get; set; }
        public Loadings loadings { get; set; }
        public MotorBreakIn motorBreakIn { get; set; }
        public List<object> motorTransits { get; set; }
        public PospInfo pospInfo { get; set; }
        public PreInspection preInspection { get; set; }
        public PreviousInsurer previousInsurer { get; set; }
        public ServiceTax serviceTax { get; set; }
        public Vehicle vehicle { get; set; }
        public string image { get; set; }
        public Benefit Benefit { get; set; }
        //public string enquiryId { get; set; }
        //public string PolicyType { get; set; }
        //public string ProductName { get; set; }
        //public string ProductType { get; set; }
        //public object cesses { get; set; }
        //public string Chainid { get; set; }
        //public string ProposalNo { get; set; }
        //public string Policysysid { get; set; }
        //public ProductDetails ProductDetails { get; set; }
        //public Discounts discounts { get; set; }
        //public Error error { get; set; }
        //public List<object> informationMessages { get; set; }
        //public Loadings loadings { get; set; }
        //public MotorBreakIn motorBreakIn { get; set; }
        //public List<object> motorTransits { get; set; }
        //public PospInfo pospInfo { get; set; }
        //public PreInspection preInspection { get; set; }
        //public PreviousInsurer previousInsurer { get; set; }
        //public ServiceTax serviceTax { get; set; }
        //public Vehicle vehicle { get; set; }
        //public string image { get; set; }
    }
    public class Benefit
    {
        public List<Category> categories { get; set; }
    }
    public class Category
    {
        public string categoryName { get; set; }
        public int categoryID { get; set; }
        public List<Menu> menus { get; set; }
    }
    public class Menu
    {
        public string menuName { get; set; }
        public string details { get; set; }
        public int menuID { get; set; }
        public string destriptions { get; set; }
    }
    public class MotorRoot
    {
        public List<PrivateCarQuotesR> PrivateCarQuotesRS { get; set; }
    }
    //----------------------------------------------------------------------
    public class MotorPayment
    {
        public string Chainid { get; set; }
        public string enquiryId { get; set; }
        public object message { get; set; }
        public string Status { get; set; }
        public object timestamp { get; set; }
        public string Paymenturl { get; set; }
    }
    public class MotorGeneratePolicy
    {
        public string policyId { get; set; }
        public string Chainid { get; set; }
        public string enquiryId { get; set; }
        public string schedulePathHC { get; set; }
        public string schedulePath { get; set; }
        public string pdfSource { get; set; }
        public string Status { get; set; }
        public object message { get; set; }
    }
}