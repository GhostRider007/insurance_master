﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Models.Health
{
    public class HealthModel
    {
        public class PaymentSuccessDel
        {
            public string csrf { get; set; }
            public string policyNumber { get; set; }
            public string transactionRefNum { get; set; }
            public string uwDecision { get; set; }
            public string errorFlag { get; set; }
            public string errorMsg { get; set; }
            public string enquiryid { get; set; }
            public string companyName { get; set; }
            public string Status { get; set; }
            public string imgurl { get; set; }
            public string companyid { get; set; }
            public string premium { get; set; }
            public string proposalNum { get; set; }
            public string purchaseToken { get; set; }
            //-----------------Adity Birla---------------------------------
            public string merchantTxnId { get; set; }
            public string amount { get; set; }
            public string currency { get; set; }
            public string email { get; set; }
            public string mobileNo { get; set; }
            public string paymentMode { get; set; }
            public string pgRespCode { get; set; }
            public string QuoteId { get; set; }
            public string SourceTxnId { get; set; }
            public string TxMsg { get; set; }
            public string TxStatus { get; set; }
            public string TxRefNo { get; set; }
            public string txnDateTime { get; set; }
            public string signature { get; set; }
        }
        public class NewEnquiry_Health
        {
            public string AgencyId { get; set; }
            public string AgencyName { get; set; }
            public string Policy_Type { get; set; }
            public string Policy_Type_Text { get; set; }
            public string Sum_Insured { get; set; }
            public bool Self { get; set; }
            public int SelfAge { get; set; }
            public bool Spouse { get; set; }
            public int SpouseAge { get; set; }
            public bool Son { get; set; }
            public int Son1Age { get; set; }
            public int Son2Age { get; set; }
            public int Son3Age { get; set; }
            public int Son4Age { get; set; }
            public bool Daughter { get; set; }
            public int Daughter1Age { get; set; }
            public int Daughter2Age { get; set; }
            public int Daughter3Age { get; set; }
            public int Daughter4Age { get; set; }
            public bool Father { get; set; }
            public int FatherAge { get; set; }
            public bool Mother { get; set; }
            public int MotherAge { get; set; }
            public string Gender { get; set; }
            public string First_Name { get; set; }
            public string Last_Name { get; set; }
            public string Email_Id { get; set; }
            public string Mobile_No { get; set; }
            public string Pin_Code { get; set; }
            public bool Is_Term_Accepted { get; set; }
            public string Enquiry_Id { get; set; }
            public string searchurl { get; set; }
            public string selectedproductjson { get; set; }
            public string EnquiryBookingType { get; set; }
            public string login_userid { get; set; }
            public string login_password { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime UpdatedDate { get; set; }
        }
        public class SumInsuredMaster
        {
            public int SumInsuredId { get; set; }
            public string MinSumInsured { get; set; }
            public string MaxSumInsured { get; set; }
            public string SumInsuredText { get; set; }
            public string SumInsuredValue { get; set; }
        }
        public class FilterSection
        {
            public int MinPrice { get; set; }
            public int MaxPrice { get; set; }
            public string Type { get; set; }
            public string EnquiryId { get; set; }
            public string Tenure { get; set; }
            public string SumInsured { get; set; }
            public List<string> Provider { get; set; }
        }
        public class CreateHealth_Proposal
        {
            public int id { get; set; }
            public string enquiryid { get; set; }
            public string productid { get; set; }
            public string companyid { get; set; }
            public string suminsuredid { get; set; }
            public string suminsured { get; set; }
            public string periodfor { get; set; }
            public string policyfor { get; set; }
            public string plantype { get; set; }
            public string premium { get; set; }
            public string mstatus { get; set; }
            public string title { get; set; }
            public string discount { get; set; }
            public string dob { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string landmark { get; set; }
            public int stateid { get; set; }
            public string statename { get; set; }
            public int cityid { get; set; }
            public string cityname { get; set; }
            public int areaid { get; set; }
            public string areaname { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string mobile { get; set; }
            public string emailid { get; set; }
            public string pincode { get; set; }
            public string gender { get; set; }
            public bool isproposerinsured { get; set; }
            public bool criticalillness { get; set; }
            public bool ispropnominee { get; set; }
            public string proposaljson { get; set; }
            public List<InsuredDetails> InsuredDetails { get; set; }
            public string nRelation { get; set; }
            public string nTitle { get; set; }
            public string nFirstName { get; set; }
            public string nLastName { get; set; }
            public string nDOB { get; set; }
            public int nAge { get; set; }
            public string panno { get; set; }
            public string aadharno { get; set; }
            public string annualincome { get; set; }
            public string p_referenceId { get; set; }
            public string p_proposalNum { get; set; }
            public string p_totalPremium { get; set; }
            public string p_paymenturl { get; set; }
            public string logoUrl { get; set; }
            public string p_rowpaymenturl { get; set; }
            public string policypdflink { get; set; }
            public string pay_proposal_Number { get; set; }
            public string pay_policy_Number { get; set; }
            public string row_premium { get; set; }
            public string row_basePremium { get; set; }
            public string row_serviceTax { get; set; }
            public string row_totalPremium { get; set; }
            public string row_discountPercent { get; set; }
            public string row_insuranceCompany { get; set; }
            public string row_policyType { get; set; }
            public string row_planname { get; set; }
            public string row_policyTypeName { get; set; }
            public string row_policyName { get; set; }
        }
        public class InsuredDetails
        {
            public int id { get; set; }
            public string relationid { get; set; }
            public string relationname { get; set; }
            public string title { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string dob { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
            public bool isinsured { get; set; }
            public string occupationid { get; set; }
            public string occupation { get; set; }
            public string isillness { get; set; }
            public string illnessdesc { get; set; }
            public string isengagemanuallabour { get; set; }
            public string isengagewintersports { get; set; }
            public string engageManualLabourDesc { get; set; }
            public string engageWinterSportDesc { get; set; }
        }
        public class RelationMaster
        {
            public string RelationshipId { get; set; }
            public string RelationshipName { get; set; }
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string RelationType { get; set; }
            public string Title { get; set; }
            public List<RelationMaster> RelationList { get; set; }
        }
        public class Health_Insured
        {
            public int id { get; set; }
            public string enquiryid { get; set; }
            public string productid { get; set; }
            public int companyid { get; set; }
            public string relationid { get; set; }
            public string relationname { get; set; }
            public string title { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string dob { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
            public bool isinsured { get; set; }
            public string illnessdesc { get; set; }
            public bool issame { get; set; }
            public string medicalhistory { get; set; }
            public string occupationId { get; set; }
            public string occupation { get; set; }
            public bool engageManualLabour { get; set; }
            public bool engageWinterSports { get; set; }
            public bool illness { get; set; }
            public string engageManualLabourDesc { get; set; }
            public string engageWinterSportDesc { get; set; }
        }
        public class SearchUrlParaMeter
        {
            public string enquiry_id { get; set; }
            public string productid { get; set; }
            public string companyid { get; set; }
            public string suminsuredid { get; set; }
            public string suminsured { get; set; }
            public string adult { get; set; }
            public string child { get; set; }
            public string planname { get; set; }
            public string totalamt { get; set; }
        }
        public class T_Enquiry_Response
        {
            public int id { get; set; }
            public string enquiry_id { get; set; }
            public string searchrequest { get; set; }
            public string searchresponse { get; set; }
            public string productid { get; set; }
            public string suminsuredid { get; set; }
            public string suminsured { get; set; }
            public string companyid { get; set; }
        }
        public class PerposalQuestion
        {
            public string enquiryid { get; set; }
            public string firstname { get; set; }
            public string lastnane { get; set; }
            public string questionjson { get; set; }
        }
        public class Health_Payment
        {
            public int id { get; set; }
            public string enquiryId { get; set; }
            public string company { get; set; }
            public string pp_companyId { get; set; }
            public string pp_premium { get; set; }
            public string pp_totalPremium { get; set; }
            public string pp_proposalNum { get; set; }
            public string pp_productDetailId { get; set; }
            public string pp_paymenturl { get; set; }
            public string pp_rowpaymenturl { get; set; }
            public string cc_policyNumber { get; set; }
            public string cc_transactionRefNum { get; set; }
            public string cc_uwDecision { get; set; }
            public string cc_errorFlag { get; set; }
            public string cc_errorMsg { get; set; }
            public string ss_ORDERID { get; set; }
            public string ss_MID { get; set; }
            public string ss_TXNID { get; set; }
            public string ss_TXNAMOUNT { get; set; }
            public string ss_PAYMENTMODE { get; set; }
            public string ss_CURRENCY { get; set; }
            public string ss_TXNDATE { get; set; }
            public string ss_STATUS { get; set; }
            public string ss_RESPCODE { get; set; }
            public string ss_RESPMSG { get; set; }
            public string ss_GATEWAYNAME { get; set; }
            public string ss_BANKTXNID { get; set; }
            public string ss_BANKNAME { get; set; }
            public string ss_CHECKSUMHASH { get; set; }
            public string Status { get; set; }
            public string logoUrl { get; set; }
            public string pp_referenceId { get; set; }
            public string pp_serviceTax { get; set; }
            public bool payment_status { get; set; }
            public string CreatedDate { get; set; }
            public string UpdatedDate { get; set; }
        }

        public class ApiNominee_Common
        {

            public string NomineeId { get; set; }
            public string RelationshipCode { get; set; }
            public string Relationship { get; set; }
            public string Title { get; set; }
            public string CompanyId { get; set; }
            public string CompanyName { get; set; }
            public string Status { get; set; }
            public string Type { get; set; }
            public string Gender { get; set; }
        }
        //================================================================
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Insured
        {
            public object dob { get; set; }
            public int sumInsuredId { get; set; }
            public object buyBackPED { get; set; }
        }
        public class Product
        {
            public string policyTypeName { get; set; }
            public string policyName { get; set; }
            public int postalCode { get; set; }
            public int period { get; set; }
            public List<Insured> insureds { get; set; }
            public string productDetailId { get; set; }
        }
        public class Datum
        {
            public string inquiry_id { get; set; }
            public string premium { get; set; }
            public string basePremium { get; set; }
            public string serviceTax { get; set; }
            public string totalPremium { get; set; }
            public double discountPercent { get; set; }
            public string logoUrl { get; set; }
            public string schemaId { get; set; }
            public string insuranceCompany { get; set; }
            public int insuranceCompanyId { get; set; }
            public string policyType { get; set; }
            public int suminsured { get; set; }
            public string plan { get; set; }
            public string ageBracket { get; set; }
            public int suminsuredId { get; set; }
            public Product product { get; set; }
            public List<ProductTerm> productTerms { get; set; }
            public string quoteId { get; set; }
            public int id { get; set; }
            public Benefits benefits { get; set; }
            public List<AddOne> addOnes { get; set; }
        }
        public class AddOne
        {
            public int addOnsId { get; set; }
            public string addOns { get; set; }
            public string code { get; set; }
            public string value { get; set; }
            public string calculation { get; set; }
            public string addOnValue { get; set; }
            public string productDetailID { get; set; }
            public string inquiry_id { get; set; }
        }
        public class BenefitMenu
        {
            public string menuName { get; set; }
            public string details { get; set; }
            public int menuID { get; set; }
            public string destriptions { get; set; }
        }
        public class BenefitCategory
        {
            public string categoryName { get; set; }
            public int categoryID { get; set; }
            public List<BenefitMenu> menus { get; set; }
        }
        public class Benefits
        {
            public List<BenefitCategory> categories { get; set; }
            public string logoUrl { get; set; }
            public string productName { get; set; }
            public string insuranceCompany { get; set; }
            public int companyId { get; set; }
            public string productid { get; set; }
            public string plan { get; set; }
            public int sumInsured { get; set; }
        }
        public class SchemesMaster
        {
            public string schemeId { get; set; }
            public string schemes { get; set; }
            public int companyId { get; set; }
            public string productid { get; set; }
        }
        public class Response
        {
            public List<Datum> data { get; set; }
            public object urls { get; set; }
            public List<SchemesMaster> schemesMaster { get; set; }
        }
        public class ProductTerm
        {
            public string planName { get; set; }
            public int sumInsuredMin { get; set; }
            public int sumInsuredMax { get; set; }
            public int ageGroupMin { get; set; }
            public int ageGroupMax { get; set; }
            public string medical_Tests { get; set; }
            public bool isCritical_Ilness { get; set; }
        }
        public class HealthApiResponse
        {
            public bool success { get; set; }
            public string message { get; set; }
            public Response response { get; set; }
            public int error_code { get; set; }
        }
        public class SingleSearchResponse
        {
            public string premium { get; set; }
            public string basePremium { get; set; }
            public string serviceTax { get; set; }
            public string totalPremium { get; set; }
            public string discountPercent { get; set; }
            public string logoUrl { get; set; }
            public int schemaId { get; set; }
            public string insuranceCompany { get; set; }
            public int insuranceCompanyId { get; set; }
            public string policyType { get; set; }
            public int suminsured { get; set; }
            public string plan { get; set; }
            public string ageBracket { get; set; }
            public int suminsuredId { get; set; }
            public Product product { get; set; }
            public object productTerms { get; set; }
            public string quoteId { get; set; }
        }
        //======================================================

        #region [Check pyament Status]
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class PAP_Data
        {
            public string proposal_Number { get; set; }
            public string policy_Number { get; set; }
        }

        public class PAP_Response
        {
            public PAP_Data data { get; set; }
            public object urls { get; set; }
            public object schemesMaster { get; set; }
        }

        public class ProposalAndPolicy
        {
            public bool success { get; set; }
            public string message { get; set; }
            public PAP_Response response { get; set; }
            public int error_code { get; set; }
        }

        //--------------Care payment check----------------------
        public class CarePaymentStatusCheckResponseData
        {
            public string status { get; set; }
            public string message { get; set; }
        }

        public class CarePaymentStatusCheckIntGetPolicyStatusIO
        {
            public string proposalNum { get; set; }
            public string policyNum { get; set; }
            public string policyCommencementDt { get; set; }
            public string policyMaturityDt { get; set; }
            public string policyStatus { get; set; }
            public int policyPremium { get; set; }
            public string parentAgentId { get; set; }
            public string policyCommencementTimestamp { get; set; }
            public List<object> errorLists { get; set; }
            public List<object> listErrorListList { get; set; }
        }

        public class CarePaymentStatusCheckResponse
        {
            public CarePaymentStatusCheckResponseData responseData { get; set; }
            public CarePaymentStatusCheckIntGetPolicyStatusIO intGetPolicyStatusIO { get; set; }
        }

        public class CarePaymentStatusCheck
        {
            public bool success { get; set; }
            public string message { get; set; }
            public CarePaymentStatusCheckResponse response { get; set; }
            public int error_code { get; set; }
        }

        #endregion

        #region [Crae Pdf Genrate]
        public class CarePdfResponseData
        {
            public string status { get; set; }
            public string message { get; set; }
        }

        public class CarePdfListErrorListList
        {
            public string guid { get; set; }
            public string status { get; set; }
            public object exceptionCd { get; set; }
            public string errDescription { get; set; }
        }

        public class CarePdfErrorList
        {
            public string guid { get; set; }
            public string status { get; set; }
            public object exceptionCd { get; set; }
            public string errDescription { get; set; }
        }

        public class CarePdfIntFaveoGetPolicyPDFIO
        {
            public string policyNum { get; set; }
            public object dataPDF { get; set; }
            public string ltype { get; set; }
            public string policyPDFStatus { get; set; }
            public string parentAgentId { get; set; }
            public List<CarePdfListErrorListList> listErrorListList { get; set; }
            public List<CarePdfErrorList> errorLists { get; set; }
        }

        public class CarePdfResponse
        {
            public CarePdfResponseData responseData { get; set; }
            public CarePdfIntFaveoGetPolicyPDFIO intFaveoGetPolicyPDFIO { get; set; }
        }

        public class CarePdfRoot
        {
            public bool success { get; set; }
            public string message { get; set; }
            public CarePdfResponse response { get; set; }
            public int error_code { get; set; }
        }
        #endregion

        #region [Adity Birla Save_Perposal]
        public class ABHIData
        {
            public string referenceId { get; set; }
            public string premium { get; set; }
            public string serviceTax { get; set; }
            public string totalPremium { get; set; }
            public string proposalNum { get; set; }
            public string productDetailId { get; set; }
            public int companyId { get; set; }
            public string inquiry_id { get; set; }
            public int id { get; set; }
            public string receiptAmount { get; set; }
            public string receiptNumber { get; set; }
            public string policyNumber { get; set; }
            public string policyStatus { get; set; }
        }
        public class ABHIResponse
        {
            public ABHIData data { get; set; }
            public object urls { get; set; }
            public string message { get; set; }
        }
        public class ABHISaveProposal
        {
            public bool success { get; set; }
            public string message { get; set; }
            public ABHIResponse response { get; set; }
            public int error_code { get; set; }
        }
        #endregion

        public class AddonChecked
        {
            public string addOnsId { get; set; }
            public string addOns { get; set; }
            public string code { get; set; }
            public string value { get; set; }
            public string calculation { get; set; }
            public string addOnValue { get; set; }
            public string BasePremium { get; set; }
            public string PlanAmount { get; set; }
            public string PlanPeriod { get; set; }
            public string inquiryid { get; set; }
        }
    }
}