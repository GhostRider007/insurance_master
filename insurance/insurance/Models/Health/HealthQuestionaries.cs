﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Models.Health
{
    public class HealthQuestionaries
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<Response> response { get; set; }
        public int error_code { get; set; }
    }
    public class DropDownData
    {
        public string text { get; set; }
        public string value { get; set; }
    }
    public class SubQuestion
    {
        public string questionSetCode { get; set; }
        public string questionCode { get; set; }
        public string questionDescription { get; set; }
        public string response { get; set; }
        public string required { get; set; }
        public List<object> subQuestion { get; set; }
        public List<DropDownData> dropDownData { get; set; }
    }

    public class Response
    {
        public string questionSetCode { get; set; }
        public string questionCode { get; set; }
        public string questionDescription { get; set; }
        public string response { get; set; }
        public string required { get; set; }
        public List<SubQuestion> subQuestion { get; set; }
        public List<DropDownData> dropDownData { get; set; }
    }

    public class QuestionaireList
    {
        public string questionSetCode { get; set; }
        public string questionCode { get; set; }
        public string questionDescription { get; set; }
        public string response { get; set; }
        public List<SubQuestion> subQuestion { get; set; }
    }
}