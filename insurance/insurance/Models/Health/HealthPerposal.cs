﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Models.Health
{
    public class HealthPerposal
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Data
        {
            public object referenceId { get; set; }
            public string premium { get; set; }
            public object serviceTax { get; set; }
            public object totalPremium { get; set; }
            public string proposalNum { get; set; }
            public int productDetailId { get; set; }
            public int companyId { get; set; }
        }

        public class Response
        {
            public Data data { get; set; }
            public string urls { get; set; }
            public string message { get; set; }
        }

        public class HealthPerposalResponse
        {
            public bool success { get; set; }
            public string message { get; set; }
            public Response response { get; set; }
            public int error_code { get; set; }
            public int status { get; set; }
            public string title { get; set; }
        }

        public class HealthDocumentRespo
        {
            public bool success { get; set; }
            public string message { get; set; }
            public string response { get; set; }
            public int error_code { get; set; }
            public string documenturl { get; set; }
        }


        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class PaymentData
        {
            public string referenceId { get; set; }
            public string redirectToken { get; set; }
        }

        public class Urls
        {
            public string paymentUrl { get; set; }
        }

        public class PaymentResponse
        {
            public PaymentData data { get; set; }
            public Urls urls { get; set; }
            public object schemesMaster { get; set; }
        }

        public class StarPaymentRoot
        {
            public bool success { get; set; }
            public string message { get; set; }
            public PaymentResponse response { get; set; }
            public int error_code { get; set; }
        }

        public class CarePaymentRoot
        {
            public bool success { get; set; }
            public string message { get; set; }
            public string response { get; set; }
            public int error_code { get; set; }
        }


        #region [City List From Star Health api]
        public class City
        {
            public int city_id { get; set; }
            public string city_name { get; set; }
        }

        public class CityData
        {
            public string state_name { get; set; }
            public List<City> city { get; set; }
        }

        public class CityResponse
        {
            public CityData data { get; set; }
            public object urls { get; set; }
            public object schemesMaster { get; set; }
        }

        public class CityList
        {
            public bool success { get; set; }
            public string message { get; set; }
            public CityResponse response { get; set; }
            public int error_code { get; set; }
        }

        public class Area
        {
            public int areaID { get; set; }
            public string areaName { get; set; }
            public string areaClassification { get; set; }
        }

        public class AreaData
        {
            public List<Area> area { get; set; }
        }

        public class AreaResponse
        {
            public AreaData data { get; set; }
            public object urls { get; set; }
            public object schemesMaster { get; set; }
        }

        public class AreaList
        {
            public bool success { get; set; }
            public string message { get; set; }
            public AreaResponse response { get; set; }
            public int error_code { get; set; }
        }


        #endregion
    }
}