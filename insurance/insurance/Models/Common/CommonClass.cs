﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Models.Motor;
using insurance.Service;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Models.Common
{
    public static class CommonClass
    {
        public static List<SelectListItem> PopulateCountry(List<Country> countryList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in countryList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CountryName.ToString(),
                    Value = item.CountryID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateBank(List<BankNameModel> BankList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in BankList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BankName.ToString(),
                    Value = item.id.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateState(List<State> stateList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in stateList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.StateName.ToString(),
                    Value = item.StateID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateCity(List<City> cityList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in cityList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CityName.ToString(),
                    Value = item.CityID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateGroupType(List<GroupDetailModel> branchList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in branchList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GroupName.ToString(),
                    Value = item.GroupId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> Populatebranch(List<Branch> branchList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in branchList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.BranchName.ToString(),
                    Value = item.Id.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateMake(List<TwowheelermapModel> models)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in models)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Make.ToString(),
                    Value = item.Make.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateModel(List<TwowheelermapModel> models)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in models)
            {
                items.Add(new SelectListItem
                {
                    Text = item.vehicleModel.ToString(),
                    Value = item.vehicleModel.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateRefBy(List<MemberLogin> staffList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in staffList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.FirstName.ToString(),
                    Value = item.LoginId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateRefBy(List<StaffModel> staffList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in staffList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.StaffName.ToString(),
                    Value = item.StaffID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateMenu(List<MenuModel> countryList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in countryList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.MenuName.ToString(),
                    Value = item.MenuId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateRole(List<RoleModel> roleList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in roleList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.RoleName.ToString(),
                    Value = item.RoleID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateRTO(List<MotorRTOModel> countryList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in countryList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.RTO_Code_A.ToString() + " - " + item.RTO_Name.ToString(),
                    Value = item.RTO_Code_A.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulatePreviousInsurerCodes(IDictionary<int, string> insurerList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in insurerList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Value,
                    Value = item.Key.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateGetNCBPercentage(IDictionary<int, string> insurerList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in insurerList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.Value,
                    Value = item.Key.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateGroup(List<GroupDetailModel> groupList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in groupList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.GroupName.ToString(),
                    Value = item.GroupId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateAgency(List<CreateAgency> agencyList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in agencyList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.AgencyName.ToString(),
                    Value = item.AgencyID.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateInsurer(List<CompanyModel> insurerList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in insurerList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.CompanyName.ToString(),
                    Value = item.CompanyId.ToString()
                });
            }

            return items;
        }
        public static List<SelectListItem> PopulateProduct(List<Product> productList)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem
            {
                Text = "All",
                Value = "all"
            });
            foreach (var item in productList)
            {
                items.Add(new SelectListItem
                {
                    Text = item.ProductName.ToString(),
                    Value = item.ProductId.ToString()
                });
            }

            return items;
        }
        public static string GetCountryName(string countryid)
        {
            string tempCountryName = string.Empty;

            var country = FrontAgencyService.CountryList(countryid, "1");

            if (country != null)
            {
                tempCountryName = country[0].CountryName;
            }

            return tempCountryName;
        }
        public static string GetStateName(string stateid, string countryid)
        {
            string tempStateName = string.Empty;

            var state = FrontAgencyService.StateList(stateid, countryid, "1");

            if (state != null)
            {
                tempStateName = state[0].StateName;
            }

            return tempStateName;
        }
        public static string GetCityName(string cityid, string stateid)
        {
            string tempCityName = string.Empty;

            var city = FrontAgencyService.CityList(null, cityid, stateid, "1");

            if (city != null)
            {
                tempCityName = city[0].CityName;
            }

            return tempCityName;
        }
        public static string GetStaffName(string staffID)
        {
            string tempStaffName = string.Empty;

            var staff = FrontAgencyService.RefByList(null, "1", staffID);

            if (staff != null)
            {
                tempStaffName = staff[0].StaffName;
            }

            return tempStaffName;
        }
        public static string SetDocumentName(int i, string ext)
        {
            string fileName = string.Empty;

            if (i == 0)
            {
                fileName = "Company_Address_Proof ";
            }
            else if (i == 1)
            {
                fileName = "Pan_Card";
            }
            else if (i == 2)
            {
                fileName = "Address_Proof1";
            }
            //else if (i == 3)
            //{
            //    fileName = "Address_Proof2";
            //}
            else if (i == 3)
            {
                fileName = "Company_Logo";
            }

            fileName = fileName + ext;

            return fileName;
        }
        public static string IndianMoneyFormat(string fare)
        {
            decimal parsed = decimal.Parse((Math.Ceiling(Convert.ToDecimal(fare)).ToString()), CultureInfo.InvariantCulture);
            CultureInfo hindi = new CultureInfo("hi-IN");
            return string.Format(hindi, "{0:c}", parsed).Replace("₹", "₹ ");
        }
        public static string ConvertDateYYMMDD(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
            }
            return rutDate;
        }
        public static string ConvertDateToISOFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                DateTime dtCDate = Convert.ToDateTime(rutDate);
                rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }
        public static int CharactersCount(string input)
        {
            int Count = 0;

            try
            {
                //char charCout = Convert.ToChar(input);
                foreach (char chr in input)
                {
                    Count++;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Count;
        }

        public static DataTable GetBankName()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SeeInsured"].ConnectionString);
            SqlCommand cmd = new SqlCommand("GetBankName", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();           
            da.SelectCommand = cmd;
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
    }
}