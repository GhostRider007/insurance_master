﻿using insurance.Models.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ecommerce.Models.Common
{
    public static class Utility
    {
        public static string ReadHTMLTemplate(string fileName)
        {
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplates" + "/" + fileName))
            {
                try
                {
                    string xmlString = streamReader.ReadToEnd();
                    return xmlString;
                }
                finally
                {
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
        }
    }
}