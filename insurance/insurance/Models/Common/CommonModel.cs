﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Models.Common
{
    public class CommonModel
    {
        public class Gender
        {
            public int GenderId { get; set; }
            public string LookupKey { get; set; }
            public string LookupDataCD { get; set; }
            public string LookupDataValueKey { get; set; }
            public bool Status { get; set; }
            public DateTime CreatedDate { get; set; }
            public List<Gender> GenderList { get; set; }
        }

        public class ddlBindClass 
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public string Data1 { get; set; }
        }
    }
}