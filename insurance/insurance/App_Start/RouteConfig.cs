﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace insurance
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");            

            routes.MapRoute(name: "AgencyIndex", url: "agency/dashboard", defaults: new { controller = "Administration", action = "Index" });
            routes.MapRoute(name: "HealthPaymentConfirmation", url: "health/paymentsuccess", defaults: new { controller = "Health", action = "PaymentConfirmation" });
            routes.MapRoute(name: "MotorPaymentConfirmation", url: "motor/paymentsuccess", defaults: new { controller = "Moter", action = "PaymentConfirmation" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
