﻿using insurance.Helper.DataBase;
using insurance.Models.Common;
using insurance.Models.Motor;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace insurance.Helper.MoterHelper
{
    public static class Motor_Helper
    {
        public static bool SaveMoterGenratedQuotes(MotorModel model)
        {
            try
            {
                bool isdeleted = ConnectToDataBase.DeleteRecordFromAnyTable("T_NewEnquiry_Motor", "Enquiry_Id='" + model.Enquiry_Id + "'", "motor");

                string fileds = "Enquiry_Id,AgencyId,AgencyName,VechileRegNo,MobileNo,PinCode,statename,vehicletype,insurancetype,EnquiryBookingType";
                string fieldValue = "'" + model.Enquiry_Id + "','" + model.AgencyId + "','" + model.AgencyName + "','" + model.VechileRegNo + "','" + model.MobileNo + "'," + model.PinCode + ",'" + model.State + "','" + model.VechileType + "','" + model.InsuranceType + "','"+ model.EnquiryBookingType + "'";
                string tableName = "T_NewEnquiry_Motor";

                return ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static List<MotorRTOModel> GetRTODetails()
        {
            List<MotorRTOModel> result = new List<MotorRTOModel>();

            try
            {
                string fileds = "RTOCode,RTO_Name";
                string tablename = "tblMaster_RTO";
                string whereCondition = "RTOCode is not null order by RTOCode";

                DataTable dtRto = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
                if (dtRto != null && dtRto.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRto.Rows.Count; i++)
                    {
                        MotorRTOModel ddl = new MotorRTOModel();
                        ddl.RTO_Code_A = dtRto.Rows[i]["RTOCode"].ToString();
                        ddl.RTO_Name = dtRto.Rows[i]["RTO_Name"].ToString();
                        result.Add(ddl);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static bool UpdatePaymentTransId(string enquiryid, string transactionNumber, string policynumber, string status)
        {
            try
            {
                string fieldsWithValue = "transactionnumber='" + transactionNumber + "',policynumber='" + policynumber + "',paymentstatus='" + status + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Enquiry_Response", fieldsWithValue, ("enquiry_id='" + enquiryid + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdatePolicyPdfLink(string enquiryid, string policypdflink)
        {
            try
            {
                string fieldsWithValue = "policypdflink='" + (Config.WebsiteUrl + policypdflink) + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Enquiry_Response", fieldsWithValue, ("enquiry_id='" + enquiryid + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateMoterRegDetail(MotorModel model)
        {
            try
            {
                string fieldsWithValue = "RtoId='" + model.RtoId + "',RtoName='" + model.RtoName + "',DateOfReg='" + model.DateOfReg + "',DateOfMfg='" + model.DateOfMfg + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + model.Enquiry_Id + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateMoterBrandDetail(MotorModel model)
        {
            try
            {
                string fieldsWithValue = "BrandId='" + model.BrandId + "',BrandName='" + model.BrandName + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + model.Enquiry_Id + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateMoterModelDetail(MotorModel model)
        {
            try
            {
                string fieldsWithValue = "ModelId='" + model.ModelId + "',ModelName='" + model.ModelName + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + model.Enquiry_Id + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateMoterFuelDetail(MotorModel model)
        {
            try
            {
                string fieldsWithValue = "Fuel='" + model.Fuel + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + model.Enquiry_Id + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static List<string> GetBrandList(bool IsBike)
        {
            List<string> brandList = new List<string>();
            try
            {
                DataTable dtBrand = new DataTable();
                if (!IsBike)
                {
                    dtBrand = ConnectToDataBase.GetRecordFromTable("distinct Make", "tblMaster_Car", "", "motor");
                }
                else
                {
                    dtBrand = ConnectToDataBase.GetRecordFromTable("distinct Make", "tblMaster_TwoWheeler", "", "motor");
                }

                if (dtBrand != null && dtBrand.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBrand.Rows.Count; i++)
                    {
                        string brand = string.Empty;
                        brand = dtBrand.Rows[i]["Make"].ToString();
                        brandList.Add(brand);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return brandList;
        }
        public static List<string> GetModelList(string brand, string enqId, bool IsBike)
        {
            List<string> modelList = new List<string>();
            try
            {
                MotorModel model = new MotorModel();
                model.BrandId = brand;
                model.BrandName = brand;
                model.Enquiry_Id = enqId;

                if (!string.IsNullOrEmpty(enqId))
                {
                    UpdateMoterBrandDetail(model);
                }

                DataTable dtModel = new DataTable();
                if (IsBike)
                {
                    dtModel = ConnectToDataBase.GetRecordFromTable("distinct Model", "tblMaster_TwoWheeler", "Make='" + brand + "' order by model", "motor");
                }
                else
                {
                    dtModel = ConnectToDataBase.GetRecordFromTable("distinct Model", "tblMaster_Car", "Make='" + brand + "' order by model", "motor");
                }

                if (dtModel != null && dtModel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtModel.Rows.Count; i++)
                    {
                        string tempModel = string.Empty;
                        tempModel = dtModel.Rows[i]["Model"].ToString();
                        modelList.Add(tempModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return modelList;
        }
        public static List<string> GetFuelList(string brand, string model, string enqId, bool IsBike)
        {
            List<string> modelList = new List<string>();
            try
            {
                MotorModel modelClass = new MotorModel();
                modelClass.ModelId = model;
                modelClass.ModelName = model;
                modelClass.Enquiry_Id = enqId;
                if (!string.IsNullOrEmpty(enqId))
                {
                    UpdateMoterModelDetail(modelClass);
                }
                DataTable dtFuel = new DataTable();
                if (IsBike)
                {
                    dtFuel = ConnectToDataBase.GetRecordFromTable("distinct FuelType", "tblMaster_TwoWheeler", "Make='" + brand + "' and Model='" + model + "' order by FuelType", "motor");
                }
                else
                {
                    dtFuel = ConnectToDataBase.GetRecordFromTable("distinct FuelType", "tblMaster_Car", "Make='" + brand + "' and Model='" + model + "' order by FuelType", "motor");
                }

                if (dtFuel != null && dtFuel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtFuel.Rows.Count; i++)
                    {
                        string fuel = string.Empty;
                        fuel = dtFuel.Rows[i]["FuelType"].ToString();
                        modelList.Add(fuel);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return modelList;
        }
        public static List<string> GetVarientList(string brand, string model, string fuel, string enqId, bool IsBike)
        {
            List<string> variantList = new List<string>();
            try
            {
                MotorModel modelClass = new MotorModel();
                modelClass.Fuel = fuel;
                modelClass.Enquiry_Id = enqId;
                if (!string.IsNullOrEmpty(enqId))
                {
                    UpdateMoterFuelDetail(modelClass);
                }

                DataTable dtVariant = new DataTable();
                if (IsBike)
                {
                    dtVariant = ConnectToDataBase.GetRecordFromTable("distinct Variant", "tblMaster_TwoWheeler", "Make='" + brand + "' and Model='" + model + "' and FuelType='" + fuel + "' order by Variant", "motor");
                }
                else
                {
                    dtVariant = ConnectToDataBase.GetRecordFromTable("distinct Variant", "tblMaster_Car", "Make='" + brand + "' and Model='" + model + "' and FuelType='" + fuel + "' order by Variant", "motor");
                }

                if (dtVariant != null && dtVariant.Rows.Count > 0)
                {
                    for (int i = 0; i < dtVariant.Rows.Count; i++)
                    {
                        string variant = string.Empty;
                        variant = dtVariant.Rows[i]["Variant"].ToString();
                        variantList.Add(variant);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return variantList;
        }
        public static string GetVehicleMainCode(string brand, string model, string fuel, string variant, bool IsBike)
        {
            string result = string.Empty;
            try
            {
                DataTable dtVMC = new DataTable();
                if (IsBike)
                {
                    dtVMC = ConnectToDataBase.GetRecordFromTable("vehicleMaincode", "tblMaster_TwoWheeler", "Make='" + brand + "' and Model='" + model + "' and FuelType='" + fuel + "' and Variant='" + variant + "'", "motor");
                }
                else
                {
                    dtVMC = ConnectToDataBase.GetRecordFromTable("vehicleMaincode", "tblMaster_Car", "Make='" + brand + "' and Model='" + model + "' and FuelType='" + fuel + "' and Variant='" + variant + "'", "motor");
                }

                if (dtVMC != null && dtVMC.Rows.Count > 0)
                {
                    result = dtVMC.Rows[0]["vehicleMaincode"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static bool UpdateVarientDetails(string varient, string enqId)
        {
            try
            {
                string fieldsWithValue = "VarientId='" + varient + "',VarientName='" + varient + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + enqId + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetEnquiryDetails(string enqId)
        {
            //DataTable dtDetails = new DataTable();

            //try
            //{
            //    //dtDetails = ConnectToDataBase.GetRecordFromTable("*", "T_NewEnquiry_Motor", "enquiry_id = '" + enqId + "'", "motor");
            //}
            //catch (Exception ex)
            //{
            //    ex.ToString();
            //}

            return ConnectToDataBase.GetEnqDel_ApiIds(enqId, "motor");
        }
        public static DataTable GetMotorProposal(string enqId)
        {
            return ConnectToDataBase.GetRecordFromTable("*", "T_Motor_Proposal", "enquiry_id = '" + enqId + "'", "motor");
        }
        public static DataTable GetMotorEnquiryResponse(string enqId)
        {
            return ConnectToDataBase.GetRecordFromTable("*", "T_Motor_Enquiry_Response", "enquiry_id = '" + enqId + "'", "motor");
        }
        public static bool UpdateOtherDetails(string isprevious, string insurerid, string insurername, string policynumber, string expdate, string lastyr, string noclaim, string enqid, string pptype, string phtype = "")
        {
            bool isUpdated = false;

            try
            {
                string fieldsWithValue = "ispreviousinsurer='" + isprevious + "',insurerid='" + insurerid + "',insurername='" + insurername + "',policynumber='" + policynumber + "',policyexpirydate='" + expdate + "',isclaiminlastyear='" + lastyr + "',previousyearnoclaim='" + noclaim + "',originalpreviouspolicytype='" + pptype + "',policyholdertype='" + phtype + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValue, ("Enquiry_Id='" + enqid + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isUpdated;
        }
        public static DataTable GetPreviousInsurerCodes_GoDigit()
        {
            string fileds = "*";
            string tablename = "tbl_PreviousInsurerCodes_GoDigit";
            string whereCondition = "code is not null order by Name";

            return ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
        }
        public static DataTable GetNCBPercentage()
        {
            string fileds = "*";
            string tablename = "tblMaster_NCBPercentage";
            string whereCondition = "ncbpercentage is not null order by ncbpercentage";

            return ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
        }
        public static object InsertAndUpdateMotoResponse(string enquiryid, string chainid, string proposalno, string suminsured, string productcode)
        {
            return ConnectToDataBase.InsertAndUpdateMotoResponse(enquiryid, "", "", "perposal", "motor", chainid, proposalno, suminsured, "", productcode);
        }
        public static bool SaveMotorProposalData(MotorProposal model)
        {
            bool isSuccess = false;
            try
            {
                string tablename = "T_Motor_Proposal";

                DataTable existingDataCount = ConnectToDataBase.GetRecordFromTable("count(*) as count", tablename, "enquiry_id = '" + model.enquiry_id + "'", "motor");

                if (Convert.ToInt32(existingDataCount.Rows[0]["count"].ToString()) > 0)
                {
                    string fieldsWithValues = "enquiry_id='" + model.enquiry_id + "',title='" + model.title + "',gender='" + model.gender + "',firstname='" + model.firstname + "',lastname='" + model.lastname + "',mobileno='" + model.mobileno + "',dob='" + model.dob + "',emailid='" + model.emailid + "',address1='" + model.address1 + "',address2='" + model.address2 + "',landmark='" + model.landmark + "',pincode='" + model.pincode + "',stateid=" + model.stateid + ",statename='" + model.statename + "',cityid=" + model.cityid + ",cityname='" + model.cityname + "',isproposer=" + (model.isproposer == true ? 1 : 0);

                    isSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldsWithValues, "enquiry_id = '" + model.enquiry_id + "'", "motor");
                }
                else
                {
                    string fields = "enquiry_id, title, gender, firstname, lastname, mobileno, dob, emailid, address1, address2, landmark, pincode, stateid, statename, cityid, cityname, isproposer";
                    string values = "'" + model.enquiry_id + "','" + model.title + "','" + model.gender + "','" + model.firstname + "','" + model.lastname + "','" + model.mobileno + "','" + model.dob + "','" + model.emailid + "','" + model.address1 + "','" + model.address2 + "','" + model.landmark + "','" + model.pincode + "'," + model.stateid + ",'" + model.statename + "'," + model.cityid + ",'" + model.cityname + "'," + (model.isproposer == true ? 1 : 0) + "";

                    isSuccess = ConnectToDataBase.InsertRecordToTable(fields, values, tablename, "motor");
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        public static bool SaveMotorNomineeData(string enquiry_id, string relation, string title, string nfirstname, string nlastname, string ndob)
        {
            bool isSuccess = false;
            try
            {
                string fieldsWithValues = "nrelation='" + relation + "',ntitle='" + title + "',nfirstname='" + nfirstname + "',nlastname='" + nlastname + "',ndob='" + ndob + "'";

                isSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Proposal", fieldsWithValues, "enquiry_id = '" + enquiry_id + "'", "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        public static bool SaveVehicleHistoryData(string enquiry_id, string vehicleregno, string vehiclemfdyear, string engineno, string chasisno, string valuntarydeductible, string previousinsured, string islonelease, string policyholdertype = "", string gstno = "", string panno = "")
        {
            bool isSuccess = false;
            try
            {
                string fieldsWithValues = "vehicleregno='" + vehicleregno + "',vehiclemfdyear='" + vehiclemfdyear + "',engineno='" + engineno + "',chasisno='" + chasisno + "',valuntarydeductible='" + valuntarydeductible + "',previousinsured='" + previousinsured + "',policyholdertype='" + policyholdertype + "',islonelease=" + (islonelease.ToLower() == "yes" ? 1 : 0) + ",gstno='" + gstno + "',panno='" + panno + "'";

                isSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Proposal", fieldsWithValues, "enquiry_id = '" + enquiry_id + "'", "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return isSuccess;
        }
        public static List<string> CheckAndGetDetails(string pincode)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(pincode))
                {
                    string fileds = "top 1 *";
                    string tablename = "tblMaster_PinCode";

                    DataTable dtPinCode = ConnectToDataBase.GetRecordFromTable(fileds, tablename, ("Pincode=" + pincode), "motor");
                    if (dtPinCode != null && dtPinCode.Rows.Count > 0)
                    {
                        result.Add(dtPinCode.Rows[0]["State"].ToString());
                        result.Add(dtPinCode.Rows[0]["City"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static DataTable MotorNomineeRelation(string tablename)
        {
            return ConnectToDataBase.GetRecordFromTable("*", tablename, "", "motor");
        }
        public static DataTable GetVolunatryDeductible(string tablename, string wherecon)
        {
            return ConnectToDataBase.GetRecordFromTable("*", tablename, wherecon, "motor");
        }
        public static DataTable MotorPreviousInsured(string tablename, string chainid)
        {
            string whereCondition = string.Empty;
            switch (chainid)
            {
                case "1":
                    whereCondition = "InsuranceCompanyCode is not null";
                    break;
                case "2":
                    whereCondition = "CC_DESC is not null";
                    break;
                case "3":
                    whereCondition = "PREVIOUSINSURERS is not null";
                    break;
            }
            return ConnectToDataBase.GetRecordFromTable("*", tablename, whereCondition, "motor");
        }
        public static DataTable MotorPreviousPolicyType(string tablename, string insurancetype, string ptype)
        {
            return ConnectToDataBase.GetRecordFromTable("*", tablename, "InsuranceType='" + insurancetype + "' and ProductCode='" + ptype + "' and ProductName is not null order by ProductName", "motor");
        }
        public static bool UpdateCheckPaymentStatus(string enquiryId, string Req, string respo)
        {
            bool isUpdated = false;

            try
            {
                string fieldsWithValue = "checkpaymentstatusrequest='" + Req + "',checkpaymentstatusresponse='" + respo + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Enquiry_Response", fieldsWithValue, ("Enquiry_Id='" + enquiryId + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isUpdated;
        }
        public static string GetPdfDocumentByEnqId(string enquiryid)
        {
            string pdflinkstr = string.Empty;

            DataTable dtPdf = ConnectToDataBase.GetRecordFromTable("policypdflink", "T_Motor_Enquiry_Response", "enquiry_id = '" + enquiryid + "'", "motor");
            if (dtPdf != null && dtPdf.Rows.Count > 0)
            {
                pdflinkstr = dtPdf.Rows[0]["policypdflink"].ToString();
            }
            return pdflinkstr;
        }

        public static string GetImageUrlByCHainId(string chainid)
        {
            string imgurl = string.Empty;

            DataTable dtMER = ConnectToDataBase.GetRecordFromTable("[image]", "T_MotorCredential", "CompanyId=" + chainid + "", "motor");
            if (dtMER != null && dtMER.Rows.Count > 0)
            {
                //imgurl = "http://motors.seeinsured.com" + dtMER.Rows[0]["image"].ToString();
                imgurl = dtMER.Rows[0]["image"].ToString();
            }

            return imgurl;
        }
        public static List<string> GetDetailsByTransId(string proposalSysID, string proposalNumber)
        {
            List<string> enqAmount = new List<string>();

            DataTable dtMER = ConnectToDataBase.GetRecordFromTable("enquiry_id,suminsured,(select [image] from T_MotorCredential where CompanyId=mer.chainid) as [compimage]", "T_Motor_Enquiry_Response mer", "applicationid = '" + proposalSysID + "' and perposalnumber='" + proposalNumber + "'", "motor");
            if (dtMER != null && dtMER.Rows.Count > 0)
            {
                enqAmount.Add(dtMER.Rows[0]["enquiry_id"].ToString());
                enqAmount.Add(dtMER.Rows[0]["suminsured"].ToString());
                enqAmount.Add(dtMER.Rows[0]["compimage"].ToString());
            }
            return enqAmount;
        }
        public static bool UpdatePerposalPremiumAmount(string enquiry_id, string grossPremium, string perposalNumber)
        {
            bool isUpdated = false;

            try
            {
                string fieldsWithValue = "suminsured='" + grossPremium + "',proposalnumber='" + perposalNumber + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Enquiry_Response", fieldsWithValue, ("Enquiry_Id='" + enquiry_id + "'"), "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isUpdated;
        }

        public static List<string> CheckPaymentStatusDetails(string enquiryid)
        {
            List<string> enqPayment = new List<string>();

            DataTable dtPaymentDetail = ConnectToDataBase.GetRecordFromTable("transactionnumber,policynumber", "T_Motor_Enquiry_Response", "enquiry_id='" + enquiryid + "' and policynumber is not null and transactionnumber is not null", "motor");
            if (dtPaymentDetail != null && dtPaymentDetail.Rows.Count > 0)
            {
                enqPayment.Add(dtPaymentDetail.Rows[0]["transactionnumber"].ToString());
                enqPayment.Add(dtPaymentDetail.Rows[0]["policynumber"].ToString());
            }

            return enqPayment;
        }
        public static bool AddUpdateSelectedAddon(List<AddUpdateSelectedAddon> addon, string enqId)
        {
            bool issuccess = false;
            //if (addon.Count > 0)
            //{
            try
            {
                bool isdeleted = ConnectToDataBase.DeleteRecordFromAnyTable("T_Motor_SelectedAddon", "Enquiry_Id='" + enqId + "'", "motor");
                if (addon != null && addon.Count > 0)
                {
                    foreach (var item in addon)
                    {
                        string fileds = "Enquiry_Id,AddonName,InnerName,MinAmt,MaxAmt,InsuredAmt";
                        string fieldValue = "'" + enqId + "','" + item.AddonName + "','" + item.InnerName + "'," + item.MinAmt + "," + item.MaxAmt + ",'" + item.InsuredAmt + "'";
                        if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, "T_Motor_SelectedAddon", "motor"))
                        {
                            issuccess = true;
                        }
                        else
                        {
                            issuccess = false;
                            break;
                        }
                    }
                }
                else
                {
                    issuccess = isdeleted;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            //}

            return issuccess;
        }
        public static List<AddUpdateSelectedAddon> GetAddonList(string enquiryid)
        {
            List<AddUpdateSelectedAddon> addonList = new List<AddUpdateSelectedAddon>();
            try
            {
                string fileds = "*";
                string tablename = "T_Motor_SelectedAddon";
                string whereCondition = "enquiry_id='" + enquiryid + "'";

                DataTable dt = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AddUpdateSelectedAddon dtaddon = new AddUpdateSelectedAddon();

                        dtaddon.AddonName = dt.Rows[i]["AddonName"].ToString();
                        dtaddon.InnerName = dt.Rows[i]["InnerName"].ToString();
                        dtaddon.MinAmt = Convert.ToInt64(dt.Rows[i]["MinAmt"].ToString());
                        dtaddon.MaxAmt = Convert.ToInt64(dt.Rows[i]["MaxAmt"].ToString());
                        dtaddon.InsuredAmt = dt.Rows[i]["InsuredAmt"].ToString();
                        addonList.Add(dtaddon);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return addonList;
        }
        public static string GetMotor_BrochureLink(string productType, string policyType, string productName, string productCode, string policyCode, string linktype)
        {
            string brochureLink = string.Empty;
            try
            {
                DataTable dtDetails = ConnectToDataBase.GetRecordFromTable("BrochureLink,ClausesLink", "tblMaster_ProductDetails", "ProductType='" + productType + "' and PolicyType='" + policyType + "' and ProductName='" + productName + "' and ProductCode='" + productCode + "' and PolicyCode='" + policyCode + "'", "motor");
                if (dtDetails != null && dtDetails.Rows.Count > 0)
                {
                    if (linktype == "b")
                    {
                        brochureLink = dtDetails.Rows[0]["BrochureLink"].ToString();
                    }
                    else
                    {
                        brochureLink = dtDetails.Rows[0]["ClausesLink"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return brochureLink;
        }

        #region[Extranet]
        public static bool ExtranetSaveDetails_Motor(MotorModel enquiryDetails, MotorProposal proposal)
        {
            bool IsSaved = false;
            try
            {
                bool isSuccess_proposal = false;
                bool isSuccess_enquiry = false;
                DataTable existingDataCount = new DataTable();

                #region[Enquiry]

                existingDataCount = ConnectToDataBase.GetRecordFromTable("count(*) as count", "T_NewEnquiry_Motor", "Enquiry_Id = '" + enquiryDetails.Enquiry_Id + "'", "motor");

                if (Convert.ToInt32(existingDataCount.Rows[0]["count"].ToString()) > 0)
                {
                    string fieldsWithValues = "Enquiry_Id='" + enquiryDetails.Enquiry_Id + "',AgencyId='" + enquiryDetails.AgencyId + "',AgencyName='" + enquiryDetails.AgencyName + "',VechileRegNo='" + enquiryDetails.VechileRegNo + "',VechileType='" + enquiryDetails.VechileType + "',MobileNo='" + enquiryDetails.MobileNo + "',Is_Term_Accepted='" + (enquiryDetails.Is_Term_Accepted == true ? 1 : 0) + "',RtoId='" + enquiryDetails.RtoId + "',RtoName='" + enquiryDetails.RtoName + "',DateOfReg='" + enquiryDetails.DateOfReg + "',DateOfMfg='" + enquiryDetails.DateOfMfg + "',BrandId=" + enquiryDetails.BrandId + ",BrandName='" + enquiryDetails.BrandName + "',ModelId=" + enquiryDetails.ModelId + ",ModelName='" + enquiryDetails.ModelName + "', Fuel='" + enquiryDetails.Fuel + "',VarientId='" + enquiryDetails.VarientId + "',VarientName='" + enquiryDetails.VarientName + "',PinCode=" + enquiryDetails.PinCode + ",ispreviousinsurer='" + enquiryDetails.ispreviousinsurer + "',insurerid='" + enquiryDetails.insurerid + "',insurername='" + enquiryDetails.insurername + "',policynumber='" + enquiryDetails.policynumber + "',policyexpirydate='" + enquiryDetails.policyexpirydate + "',isclaiminlastyear='" + enquiryDetails.isclaiminlastyear + "',previousyearnoclaim='" + enquiryDetails.previousyearnoclaim + "',originalpreviouspolicytype='" + enquiryDetails.originalpreviouspolicytype + "',insurancetype='" + enquiryDetails.insurancetype + "',EnquiryBookingType='" + enquiryDetails.EnquiryBookingType + "'";

                    isSuccess_enquiry = ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Motor", fieldsWithValues, "Enquiry_Id = '" + enquiryDetails.Enquiry_Id + "'", "motor");
                }
                else
                {
                    string fields = "Enquiry_Id, AgencyId, AgencyName, VechileRegNo, VechileType, MobileNo, Is_Term_Accepted, RtoId, RtoName, DateOfReg, DateOfMfg, BrandId, BrandName, ModelId, ModelName, Fuel, VarientId, VarientName, PinCode, ispreviousinsurer, insurerid, insurername, policynumber, policyexpirydate, isclaiminlastyear, previousyearnoclaim, originalpreviouspolicytype, insurancetype, EnquiryBookingType";
                    string values = "'" + enquiryDetails.Enquiry_Id + "','" + enquiryDetails.AgencyId + "','" + enquiryDetails.AgencyName + "','" + enquiryDetails.VechileRegNo + "','" + enquiryDetails.VechileType + "','" + enquiryDetails.MobileNo + "','" + (enquiryDetails.Is_Term_Accepted == true ? 1 : 0) + "','" + enquiryDetails.RtoId + "','" + enquiryDetails.RtoName + "','" + enquiryDetails.DateOfReg + "','" + enquiryDetails.DateOfMfg + "'," + enquiryDetails.BrandId + ",'" + enquiryDetails.BrandName + "'," + enquiryDetails.ModelId + ",'" + enquiryDetails.ModelName + "'," + enquiryDetails.Fuel + ",'" + enquiryDetails.VarientId + "','" + enquiryDetails.VarientName + "'," + enquiryDetails.PinCode + ",'" + enquiryDetails.ispreviousinsurer + "','" + enquiryDetails.insurerid + "','" + enquiryDetails.insurername + "','" + enquiryDetails.policynumber + "','" + enquiryDetails.policyexpirydate + "','" + enquiryDetails.isclaiminlastyear + "','" + enquiryDetails.previousyearnoclaim + "','" + enquiryDetails.originalpreviouspolicytype + "','" + enquiryDetails.insurancetype + "','" + enquiryDetails.EnquiryBookingType + "'";
                    isSuccess_enquiry = ConnectToDataBase.InsertRecordToTable(fields, values, "T_NewEnquiry_Motor", "motor");
                }

                #endregion

                #region[Proposal]

                existingDataCount = new DataTable();
                existingDataCount = ConnectToDataBase.GetRecordFromTable("count(*) as count", "T_Motor_Proposal", "enquiry_id = '" + proposal.enquiry_id + "'", "motor");

                if (Convert.ToInt32(existingDataCount.Rows[0]["count"].ToString()) > 0)
                {
                    string fieldsWithValues = "enquiry_id='" + proposal.enquiry_id + "',title='" + proposal.title + "',gender='" + proposal.gender + "',firstname='" + proposal.firstname + "',lastname='" + proposal.lastname + "',mobileno='" + proposal.mobileno + "',dob='" + proposal.dob + "',emailid='" + proposal.emailid + "',address1='" + proposal.address1 + "',address2='" + proposal.address2 + "',landmark='" + proposal.landmark + "',pincode='" + proposal.pincode + "',stateid=" + proposal.stateid + ",statename='" + proposal.statename + "',cityid=" + proposal.cityid + ",cityname='" + proposal.cityname + "',isproposer=" + (proposal.isproposer == true ? 1 : 0) + "', nrelation='" + proposal.nrelation + "',ntitle='" + proposal.ntitle + "',nfirstname='" + proposal.nfirstname + "',nlastname='" + proposal.nlastname + "',ndob='" + proposal.ndob + "',vehicleregno='" + proposal.vehicleregno + "',engineno='" + proposal.engineno + "',chasisno='" + proposal.chasisno + "',valuntarydeductible='" + proposal.valuntarydeductible + "',previousinsured='" + proposal.previousinsured + "',policyholdertype='" + proposal.policyholdertype + "',gstno='" + proposal.gstno + "',panno='" + proposal.panno + "',product='" + proposal.insurance_product + "',chainid=" + proposal.chainid + ",suminsured=" + proposal.suminsured + ",basic=" + proposal.basic + ",gst=" + proposal.gst + ",totalpremium=" + proposal.totalpremium;

                    isSuccess_proposal = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Motor_Proposal", fieldsWithValues, "enquiry_id = '" + proposal.enquiry_id + "'", "motor");
                }
                else
                {
                    string fields = "enquiry_id, title, gender, firstname, lastname, mobileno, dob, emailid, address1, address2, landmark, pincode, stateid, statename, cityid, cityname, isproposer, nrelation, ntitle, nfirstname, nlastname, ndob, vehicleregno, engineno, chasisno, valuntarydeductible, previousinsured, policyholdertype, gstno, panno, product, chainid, suminsured, basic, gst, totalpremium";
                    string values = "'" + proposal.enquiry_id + "','" + proposal.title + "','" + proposal.gender + "','" + proposal.firstname + "','" + proposal.lastname + "','" + proposal.mobileno + "','" + proposal.dob + "','" + proposal.emailid + "','" + proposal.address1 + "','" + proposal.address2 + "','" + proposal.landmark + "','" + proposal.pincode + "'," + proposal.stateid + ",'" + proposal.statename + "'," + proposal.cityid + ",'" + proposal.cityname + "'," + (proposal.isproposer == true ? 1 : 0) + ",'" + proposal.nrelation + "','" + proposal.ntitle + "','" + proposal.nfirstname + "','" + proposal.nlastname + "','" + proposal.ndob + "','" + proposal.vehicleregno + "','" + proposal.engineno + "','" + proposal.chasisno + "','" + proposal.valuntarydeductible + "','" + proposal.previousinsured + "','" + proposal.policyholdertype + "','" + proposal.gstno + "','" + proposal.panno + "','" + proposal.insurance_product + "'," + proposal.chainid + "," + proposal.suminsured + "," + proposal.basic + "," + proposal.gst + "," + proposal.totalpremium;

                    isSuccess_proposal = ConnectToDataBase.InsertRecordToTable(fields, values, "T_Motor_Proposal", "motor");
                }

                #endregion

                if (isSuccess_enquiry && isSuccess_proposal)
                {
                    IsSaved = true;
                }
                else {
                    ConnectToDataBase.DeleteRecordFromAnyTable("T_NewEnquiry_Motor", "Enquiry_Id = '" + enquiryDetails.Enquiry_Id + "'", "motor");
                    ConnectToDataBase.DeleteRecordFromAnyTable("T_Motor_Proposal", "enquiry_id = '" + proposal.enquiry_id + "'", "motor");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return IsSaved;
        }
        #endregion
    }
}