﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using static insurance.Models.Health.HealthModel;
using insurance.Helper.DataBase;
using insurance.Models;
using insurance.Models.Common;
using Post_Utility.Controller_Page;
using Post_Utility.Health;

namespace insurance.Helper.HealthHelper
{
    public static class Health_Helper
    {
        public static List<SumInsuredMaster> GetSumInsuredPriceList()
        {
            List<SumInsuredMaster> sumInsuredList = new List<SumInsuredMaster>();

            try
            {
                DataTable dtPriceList = Post_Health.GetSumInsuredPriceList();

                if (dtPriceList.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPriceList.Rows.Count; i++)
                    {
                        SumInsuredMaster sumins = new SumInsuredMaster();
                        sumins.SumInsuredId = Convert.ToInt32(dtPriceList.Rows[i]["SumInsuredId"].ToString());
                        sumins.MinSumInsured = dtPriceList.Rows[i]["minsuminsured"].ToString();
                        sumins.MaxSumInsured = dtPriceList.Rows[i]["maxsuminsured"].ToString();
                        sumins.SumInsuredText = "₹ " + dtPriceList.Rows[i]["minsuminsured"].ToString() + " - ₹ " + dtPriceList.Rows[i]["maxsuminsured"].ToString();
                        sumins.SumInsuredValue = dtPriceList.Rows[i]["minsuminsured"].ToString() + "-" + dtPriceList.Rows[i]["maxsuminsured"].ToString();
                        sumInsuredList.Add(sumins);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return sumInsuredList;
        }
        public static bool SaveGenratedQuotes(NewEnquiry_Health quotes, string userid = "", string password = "")
        {
            try
            {
                return ConnectToDataBase.InsertSaveGenratedQuotes(quotes, "api", userid, password);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static NewEnquiry_Health GetNewEnquiryHealthDetails(string enqid)
        {
            NewEnquiry_Health helDetails = new NewEnquiry_Health();

            try
            {
                DataTable dtHealthDetail = Post_Health.GetNewEnquiryHealthDetails(enqid);

                if (dtHealthDetail != null && dtHealthDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtHealthDetail.Rows.Count; i++)
                    {
                        //helDetails.EnquiryId = Convert.ToInt32(dtHealthDetail.Rows[i]["EnquiryId"].ToString());
                        helDetails.AgencyId = dtHealthDetail.Rows[i]["AgencyId"].ToString();
                        helDetails.AgencyName = dtHealthDetail.Rows[i]["AgencyName"].ToString();
                        helDetails.Policy_Type = dtHealthDetail.Rows[i]["Policy_Type"].ToString();
                        helDetails.Policy_Type_Text = dtHealthDetail.Rows[i]["Policy_Type_Text"].ToString();
                        helDetails.Sum_Insured = dtHealthDetail.Rows[i]["Sum_Insured"].ToString();
                        helDetails.Self = Convert.ToBoolean(dtHealthDetail.Rows[i]["Self"].ToString());
                        helDetails.SelfAge = Convert.ToInt32(dtHealthDetail.Rows[i]["SelfAge"].ToString());
                        helDetails.Spouse = Convert.ToBoolean(dtHealthDetail.Rows[i]["Spouse"].ToString());
                        helDetails.SpouseAge = Convert.ToInt32(dtHealthDetail.Rows[i]["SpouseAge"].ToString());
                        helDetails.Son = Convert.ToBoolean(dtHealthDetail.Rows[i]["Son"].ToString());
                        helDetails.Son1Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Son1Age"].ToString());
                        helDetails.Son2Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Son2Age"].ToString());
                        helDetails.Son3Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Son3Age"].ToString());
                        helDetails.Son4Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Son4Age"].ToString());
                        helDetails.Daughter = Convert.ToBoolean(dtHealthDetail.Rows[i]["Daughter"].ToString());
                        helDetails.Daughter1Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Daughter1Age"].ToString());
                        helDetails.Daughter2Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Daughter2Age"].ToString());
                        helDetails.Daughter3Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Daughter3Age"].ToString());
                        helDetails.Daughter4Age = Convert.ToInt32(dtHealthDetail.Rows[i]["Daughter4Age"].ToString());
                        helDetails.Father = Convert.ToBoolean(dtHealthDetail.Rows[i]["Father"].ToString());
                        helDetails.FatherAge = Convert.ToInt32(dtHealthDetail.Rows[i]["FatherAge"].ToString());
                        helDetails.Mother = Convert.ToBoolean(dtHealthDetail.Rows[i]["Mother"].ToString());
                        helDetails.MotherAge = Convert.ToInt32(dtHealthDetail.Rows[i]["MotherAge"].ToString());
                        helDetails.Gender = dtHealthDetail.Rows[i]["Gender"].ToString();
                        helDetails.First_Name = dtHealthDetail.Rows[i]["First_Name"].ToString();
                        helDetails.Last_Name = dtHealthDetail.Rows[i]["Last_Name"].ToString();
                        helDetails.Email_Id = dtHealthDetail.Rows[i]["Email_Id"].ToString();
                        helDetails.Mobile_No = dtHealthDetail.Rows[i]["Mobile_No"].ToString();
                        helDetails.Pin_Code = dtHealthDetail.Rows[i]["Pin_Code"].ToString();
                        helDetails.Enquiry_Id = dtHealthDetail.Rows[i]["Enquiry_Id"].ToString();
                        helDetails.searchurl = dtHealthDetail.Rows[i]["searchurl"].ToString();
                        helDetails.selectedproductjson = dtHealthDetail.Rows[i]["selectedproductjson"].ToString();
                        helDetails.login_userid = dtHealthDetail.Rows[i]["login_userid"].ToString();
                        helDetails.login_password = dtHealthDetail.Rows[i]["login_password"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return helDetails;
        }
        public static T_Enquiry_Response GetEnquiry_Response(string enqid)
        {
            T_Enquiry_Response respo = new T_Enquiry_Response();

            DataTable dtRespoDetail = Post_Health.GetEnquiry_Response(enqid);
            if (dtRespoDetail != null && dtRespoDetail.Rows.Count > 0)
            {
                respo.id = Convert.ToInt32(dtRespoDetail.Rows[0]["id"].ToString());
                respo.enquiry_id = dtRespoDetail.Rows[0]["enquiry_id"].ToString();
                respo.searchrequest = dtRespoDetail.Rows[0]["searchrequest"].ToString();
                respo.searchresponse = dtRespoDetail.Rows[0]["searchresponse"].ToString();
                respo.productid = dtRespoDetail.Rows[0]["productid"].ToString();
                respo.suminsuredid = dtRespoDetail.Rows[0]["suminsuredid"].ToString();
                respo.suminsured = dtRespoDetail.Rows[0]["suminsured"].ToString();
                respo.companyid = dtRespoDetail.Rows[0]["companyid"].ToString();
            }
            return respo;
        }
        public static SearchUrlParaMeter GetSearchUrlParaMeter(string enquiry_id)
        {
            SearchUrlParaMeter result = new SearchUrlParaMeter();

            string urlPara = GetHealth_UrlParaFromCache();
            if (!string.IsNullOrEmpty(urlPara))
            {
                var splitPara = urlPara.Split('&');

                result.enquiry_id = splitPara[0].Split('=')[1];
                result.productid = splitPara[1].Split('=')[1];
                result.companyid = splitPara[2].Split('=')[1];
                result.suminsuredid = splitPara[3].Split('=')[1];
                result.suminsured = splitPara[4].Split('=')[1];
                result.adult = splitPara[5].Split('=')[1];
                result.child = splitPara[6].Split('=')[1];
                result.planname = splitPara[7].Split('=')[1];
                result.totalamt = splitPara[8].Split('=')[1];
            }
            else
            {
                DataTable dtHealthDetail = Post_Health.GetNewEnquiry_Health(enquiry_id);
                if (dtHealthDetail != null && dtHealthDetail.Rows.Count > 0)
                {
                    string urlpara = dtHealthDetail.Rows[0]["searchurl"].ToString();
                    StoreUrlParaInCache(urlpara);

                    var splitPara = urlpara.Split('&');

                    result.enquiry_id = splitPara[0].Split('=')[1];
                    result.productid = splitPara[1].Split('=')[1];
                    result.companyid = splitPara[2].Split('=')[1];
                    result.suminsuredid = splitPara[3].Split('=')[1];
                    result.suminsured = splitPara[4].Split('=')[1];
                    result.adult = splitPara[5].Split('=')[1];
                    result.child = splitPara[6].Split('=')[1];
                    result.planname = splitPara[7].Split('=')[1];
                    result.totalamt = splitPara[8].Split('=')[1];
                }
            }

            return result;
        }
        public static string CheckPinCodeExist(string pincode)
        {
            string cityCode = string.Empty;

            if (!string.IsNullOrEmpty(pincode))
            {
                DataTable dtPinCode = Post_Health.GetMaster_PinCode(pincode);
                if (dtPinCode != null && dtPinCode.Rows.Count > 0)
                {
                    cityCode = dtPinCode.Rows[0]["City"].ToString();
                }
            }
            return cityCode;
        }
        public static bool InsertSelectedPlanProposal(CreateHealth_Proposal proposal)
        {
            try
            {
                if (IsEnquiryDetailExistOrNot(proposal.enquiryid, proposal.productid, proposal.companyid))
                {
                    string fieldsWithValue = "suminsured='" + proposal.suminsured + "',periodfor='" + proposal.periodfor + "',premium='" + proposal.premium + "',discount='" + proposal.discount + "',policyfor='" + proposal.policyfor + "',plantype='" + proposal.plantype + "',row_premium='" + proposal.row_premium + "',row_basePremium='" + proposal.row_basePremium + "',row_serviceTax='" + proposal.row_serviceTax + "',row_totalPremium='" + proposal.row_totalPremium + "',row_discountPercent='" + proposal.row_discountPercent + "',row_insuranceCompany='" + proposal.row_insuranceCompany + "',row_policyType='" + proposal.row_policyType + "',row_planname='" + proposal.row_planname + "',row_policyTypeName='" + proposal.row_policyTypeName + "',row_policyName='" + proposal.row_policyName + "'";
                    string whereCondition = "enquiryid='" + proposal.enquiryid + "' and productid='" + proposal.productid + "' and companyid='" + proposal.companyid + "'";

                    return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, whereCondition, "api");
                }
                else
                {
                    string fileds = "enquiryid,productid,companyid,suminsured,periodfor,premium,discount,policyfor,plantype,row_premium,row_basePremium,row_serviceTax,row_totalPremium,row_discountPercent,row_insuranceCompany,row_policyType,row_planname,row_policyTypeName,row_policyName";
                    string fieldValue = "'" + proposal.enquiryid + "','" + proposal.productid + "','" + proposal.companyid + "','" + proposal.suminsured + "','" + proposal.periodfor + "','" + proposal.premium + "','" + proposal.discount + "','" + proposal.policyfor + "','" + proposal.plantype + "','" + proposal.row_premium + "','" + proposal.row_basePremium + "','" + proposal.row_serviceTax + "','" + proposal.row_totalPremium + "','" + proposal.row_discountPercent + "','" + proposal.row_insuranceCompany + "','" + proposal.row_policyType + "','" + proposal.row_planname + "','" + proposal.row_policyTypeName + "','" + proposal.row_policyName + "'";

                    return ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, "T_Health_Proposal", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateSelectedAddons(List<AddonChecked> addonlist)
        {
            bool isssucess = false;
            try
            {
                string inquiryid = addonlist[0].inquiryid;
                bool isUncheckedAll = ConnectToDataBase.UpdateRecordIntoAnyTable("T_HealthAddon", "isselected=0", "inquiryid='" + inquiryid + "'", "api");
                if (addonlist != null && addonlist.Count > 0)
                {
                    foreach (var addon in addonlist)
                    {
                        string whereCondition = "addOnsId='" + addon.addOnsId + "' and code='" + addon.code + "' and PlanAmount='" + addon.PlanAmount + "' and PlanPeriod='" + addon.PlanPeriod + "' and inquiryid='" + addon.inquiryid + "'";
                        isssucess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_HealthAddon", "isselected=1", whereCondition, "api");
                    }

                    if (isssucess)
                    {
                        ConnectToDataBase.UpdateRecordIntoAnyTable("T_HealthAddon", "status=0", "isselected=0 and inquiryid='" + inquiryid + "'", "api");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return isssucess;
        }
        public static bool InsertProposerDetailsProposal(CreateHealth_Proposal proposal)
        {
            try
            {
                if (IsEnquiryDetailExistOrNot(proposal.enquiryid, proposal.productid, proposal.companyid))
                {
                    string fieldsWithValue = "dob='" + proposal.dob + "',address1='" + proposal.address1 + "',address2='" + proposal.address2 + "',landmark='" + proposal.landmark + "',statename='" + proposal.statename + "',title='" + proposal.title + "',cityid='" + proposal.cityid + "',cityname='" + proposal.cityname + "',firstname='" + proposal.firstname + "',lastname='" + proposal.lastname + "',mobile='" + proposal.mobile + "',emailid='" + proposal.emailid + "',pincode='" + proposal.pincode + "',gender='" + proposal.gender + "',areaid='" + proposal.areaid + "',areaname='" + proposal.areaname + "',criticalillness=" + (proposal.criticalillness ? 1 : 0) + ",isproposerinsured=" + (proposal.isproposerinsured ? 1 : 0) + ",ispropnominee=" + (proposal.ispropnominee ? 1 : 0) + ",panno='" + proposal.panno + "',aadharno='" + proposal.aadharno + "',annualincome='" + proposal.annualincome + "',maritalstatus='" + proposal.mstatus + "'";
                    string whereCondition = "enquiryid='" + proposal.enquiryid + "' and productid='" + proposal.productid + "' and companyid='" + proposal.companyid + "'";

                    return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, whereCondition, "api");
                }
                else
                {
                    string fileds = "dob,address1,address2,landmark,stateid,statename,cityid,cityname,firstname,lastname,mobile,emailid,pincode,gender,isproposerinsured,title,panno,aadharno,annualincome,ispropnominee";
                    string fieldValue = "'" + proposal.dob + "','" + proposal.address1 + "','" + proposal.address2 + "','" + proposal.landmark + "','" + proposal.stateid + "','" + proposal.statename + "','" + proposal.cityid + "','" + proposal.cityname + "','" + proposal.firstname + "','" + proposal.lastname + "','" + proposal.mobile + "','" + proposal.emailid + "','" + proposal.pincode + "','" + proposal.gender + "'," + proposal.isproposerinsured + ",'" + proposal.title + "','" + proposal.panno + "','" + proposal.aadharno + "','" + proposal.annualincome + "'," + proposal.ispropnominee + "";

                    return ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, "T_Health_Proposal", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool ExtranetInsertProposalDetails(CreateHealth_Proposal proposal)
        {
            try
            {
                proposal.statename = ConnectToDataBase.GetRecordFromTable("STATECD", "T_City", "PINCODE='" + proposal.pincode + "'", "seeinsured").Rows[0]["STATECD"].ToString();
                if (IsEnquiryDetailExistOrNot(proposal.enquiryid, proposal.productid, proposal.companyid))
                {
                    string fieldsWithValue = "enquiryid='" + proposal.enquiryid + "', productid='" + proposal.productid + "', companyid='" + proposal.companyid + "', suminsured='" + proposal.suminsured + "', periodfor='" + proposal.periodfor + "', premium='" + proposal.premium + "', dob='" + proposal.dob + "', address1='" + proposal.address1 + "', address2='" + proposal.address2 + "', landmark='" + proposal.landmark + "', stateid='" + proposal.stateid + "', statename='" + proposal.statename + "', cityid='" + proposal.cityid + "', cityname='" + proposal.cityname + "', firstname='" + proposal.firstname + "', lastname='" + proposal.lastname + "', mobile='" + proposal.mobile + "', emailid='" + proposal.emailid + "', pincode='" + proposal.pincode + "', gender='" + proposal.gender + "', isproposerinsured='" + (proposal.isproposerinsured ? 1 : 0) + "', title='" + proposal.title + "', discount='" + proposal.discount + "', ntitle='" + proposal.nTitle + "', nfirstname='" + proposal.firstname + "', nlastname='" + proposal.nLastName + "', ndob='" + proposal.nDOB + "', nage='" + proposal.nAge + "', nrelation='" + proposal.nRelation + "', aadharno='" + proposal.aadharno + "', panno='" + proposal.panno + "', annualincome='" + proposal.annualincome + "', areaid='" + proposal.areaid + "', areaname='" + proposal.areaname + "', criticalillness='" + (proposal.criticalillness ? 1 : 0) + "', ispropnominee='" + (proposal.ispropnominee ? 1 : 0) + "', row_premium='" + proposal.row_basePremium + "', row_basePremium='" + proposal.row_basePremium + "', row_serviceTax='" + proposal.row_serviceTax + "', row_totalPremium='" + proposal.row_totalPremium + "', row_discountPercent='" + proposal.row_discountPercent + "', row_insuranceCompany='" + proposal.row_insuranceCompany + "', row_policyType='" + proposal.row_policyType + "', row_planname='" + proposal.row_planname + "', row_policyTypeName='" + proposal.row_policyTypeName + "', row_policyName='" + proposal.row_policyName + "'";

                    string whereCondition = "enquiryid='" + proposal.enquiryid + "'";
                    return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, whereCondition, "api");
                }
                else
                {
                    string fields = "enquiryid, productid, companyid, suminsured, periodfor, premium, dob, address1, address2, landmark, stateid, statename, cityid, cityname, firstname, lastname, mobile, emailid, pincode, gender, isproposerinsured, title, discount, ntitle, nfirstname, nlastname, ndob, nage, nrelation, aadharno, panno, annualincome, areaid, areaname, criticalillness, ispropnominee, row_premium, row_basePremium, row_serviceTax, row_totalPremium, row_discountPercent, row_insuranceCompany, row_policyType, row_planname, row_policyTypeName, row_policyName";
                    string fieldValue = "'" + proposal.enquiryid + "','" + proposal.productid + "','" + proposal.companyid + "','" + proposal.suminsured + "','" + proposal.periodfor + "','" + proposal.premium + "','" + proposal.dob + "','" + proposal.address1 + "','" + proposal.address2 + "','" + proposal.landmark + "','" + proposal.stateid + "','" + proposal.statename + "','" + proposal.cityid + "','" + proposal.cityname + "','" + proposal.firstname + "','" + proposal.lastname + "','" + proposal.mobile + "','" + proposal.emailid + "','" + proposal.pincode + "','" + proposal.gender + "','" + (proposal.isproposerinsured ? 1 : 0) + "','" + proposal.title + "','" + proposal.discount + "','" + proposal.nTitle + "','" + proposal.nFirstName + "','" + proposal.nLastName + "','" + proposal.nDOB + "','" + proposal.nAge + "','" + proposal.nRelation + "','" + proposal.aadharno + "','" + proposal.panno + "','" + proposal.annualincome + "','" + proposal.areaid + "','" + proposal.areaname + "'," + (proposal.criticalillness ? 1 : 0) + ",'" + (proposal.ispropnominee ? 1 : 0) + "','" + proposal.row_basePremium + "','" + proposal.row_basePremium + "','" + proposal.row_serviceTax + "'," + proposal.row_totalPremium + ",'" + proposal.row_discountPercent + "','" + proposal.row_insuranceCompany + "','" + proposal.row_policyType + "','" + proposal.row_planname + "','" + proposal.row_policyTypeName + "','" + proposal.row_policyName + "'";

                    return ConnectToDataBase.InsertRecordToTable(fields, fieldValue, "T_Health_Proposal", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        private static bool IsEnquiryDetailExistOrNot(string enquiryid, string productid, string companyid)
        {
            string fileds = "*";
            string tablename = "T_Health_Proposal";

            DataTable dtProposal = ConnectToDataBase.GetRecordFromTable(fileds, tablename, "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'", "api");

            if (dtProposal != null && dtProposal.Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        public static CreateHealth_Proposal GetProposalDetail(CreateHealth_Proposal proposal)
        {
            CreateHealth_Proposal result = new CreateHealth_Proposal();

            try
            {
                string fileds = "*";
                string tablename = "T_Health_Proposal";

                //DataTable dtProposal = ConnectToDataBase.GetRecordFromTable(fileds, tablename, "enquiryid='" + proposal.enquiryid + "' and productid='" + proposal.productid + "' and companyid='" + proposal.companyid + "' and suminsured='" + proposal.suminsured + "'", "seeinsured");
                DataTable dtProposal = ConnectToDataBase.GetRecordFromTable(fileds, tablename, "enquiryid='" + proposal.enquiryid + "' and productid='" + proposal.productid + "' and companyid='" + proposal.companyid + "'", "api");

                if (dtProposal != null && dtProposal.Rows.Count > 0)
                {
                    result.id = Convert.ToInt32(dtProposal.Rows[0]["id"].ToString());
                    result.enquiryid = dtProposal.Rows[0]["enquiryid"].ToString();
                    result.productid = dtProposal.Rows[0]["productid"].ToString();
                    result.companyid = dtProposal.Rows[0]["companyid"].ToString();
                    result.suminsured = dtProposal.Rows[0]["suminsured"].ToString();
                    result.periodfor = dtProposal.Rows[0]["periodfor"].ToString();
                    result.policyfor = dtProposal.Rows[0]["policyfor"].ToString();
                    result.plantype = dtProposal.Rows[0]["plantype"].ToString();
                    result.premium = dtProposal.Rows[0]["premium"].ToString();
                    result.discount = dtProposal.Rows[0]["discount"].ToString();
                    result.dob = dtProposal.Rows[0]["dob"].ToString();
                    result.address1 = dtProposal.Rows[0]["address1"].ToString();
                    result.address2 = dtProposal.Rows[0]["address2"].ToString();
                    result.landmark = dtProposal.Rows[0]["landmark"].ToString();
                    //result.stateid = !string.IsNullOrEmpty(dtProposal.Rows[0]["stateid"].ToString()) ? Convert.ToInt32(dtProposal.Rows[0]["stateid"].ToString()) : 0;
                    result.statename = dtProposal.Rows[0]["statename"].ToString();
                    result.cityid = Convert.ToInt32(dtProposal.Rows[0]["cityid"].ToString());
                    result.cityname = dtProposal.Rows[0]["cityname"].ToString();
                    result.areaid = Convert.ToInt32(dtProposal.Rows[0]["areaid"].ToString());
                    result.areaname = dtProposal.Rows[0]["areaname"].ToString();
                    result.firstname = dtProposal.Rows[0]["firstname"].ToString();
                    result.lastname = dtProposal.Rows[0]["lastname"].ToString();
                    result.mobile = dtProposal.Rows[0]["mobile"].ToString();
                    result.emailid = dtProposal.Rows[0]["emailid"].ToString();
                    result.pincode = dtProposal.Rows[0]["pincode"].ToString();
                    result.gender = dtProposal.Rows[0]["gender"].ToString();
                    result.mstatus = dtProposal.Rows[0]["maritalstatus"].ToString();
                    result.title = dtProposal.Rows[0]["title"].ToString();
                    result.isproposerinsured = Convert.ToBoolean(dtProposal.Rows[0]["isproposerinsured"].ToString());
                    result.criticalillness = Convert.ToBoolean(dtProposal.Rows[0]["criticalillness"].ToString());
                    result.ispropnominee = Convert.ToBoolean(dtProposal.Rows[0]["ispropnominee"].ToString());

                    result.nTitle = dtProposal.Rows[0]["ntitle"].ToString();
                    result.nFirstName = dtProposal.Rows[0]["nfirstname"].ToString();
                    result.nLastName = dtProposal.Rows[0]["nlastname"].ToString();
                    result.nDOB = dtProposal.Rows[0]["ndob"].ToString();
                    result.nAge = !string.IsNullOrEmpty(dtProposal.Rows[0]["nage"].ToString()) ? Convert.ToInt32(dtProposal.Rows[0]["nage"].ToString()) : 0;
                    result.nRelation = dtProposal.Rows[0]["nrelation"].ToString();

                    result.panno = dtProposal.Rows[0]["panno"].ToString();
                    result.aadharno = dtProposal.Rows[0]["aadharno"].ToString();
                    result.annualincome = dtProposal.Rows[0]["annualincome"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static CreateHealth_Proposal GetProposalDetail(string enquiryid)
        {
            CreateHealth_Proposal result = new CreateHealth_Proposal();

            try
            {
                string fileds = "*";
                string tablename = "T_Health_Proposal";

                DataTable dtProposal = ConnectToDataBase.GetRecordFromTable(fileds, tablename, "enquiryid='" + enquiryid + "'", "api");

                if (dtProposal != null && dtProposal.Rows.Count > 0)
                {
                    result.id = Convert.ToInt32(dtProposal.Rows[0]["id"].ToString());
                    result.enquiryid = dtProposal.Rows[0]["enquiryid"] != null ? dtProposal.Rows[0]["enquiryid"].ToString() : string.Empty;
                    result.productid = dtProposal.Rows[0]["productid"] != null ? dtProposal.Rows[0]["productid"].ToString() : string.Empty;
                    result.companyid = dtProposal.Rows[0]["companyid"] != null ? dtProposal.Rows[0]["companyid"].ToString() : string.Empty;
                    result.suminsured = dtProposal.Rows[0]["suminsured"] != null ? dtProposal.Rows[0]["suminsured"].ToString() : string.Empty;
                    result.periodfor = dtProposal.Rows[0]["periodfor"] != null ? dtProposal.Rows[0]["periodfor"].ToString() : string.Empty;
                    result.policyfor = dtProposal.Rows[0]["policyfor"] != null ? dtProposal.Rows[0]["policyfor"].ToString() : string.Empty;
                    result.plantype = dtProposal.Rows[0]["plantype"] != null ? dtProposal.Rows[0]["plantype"].ToString() : string.Empty;
                    result.premium = dtProposal.Rows[0]["premium"] != null ? dtProposal.Rows[0]["premium"].ToString() : string.Empty;
                    result.discount = dtProposal.Rows[0]["discount"] != null ? dtProposal.Rows[0]["discount"].ToString() : string.Empty;
                    result.dob = dtProposal.Rows[0]["dob"] != null ? dtProposal.Rows[0]["dob"].ToString() : string.Empty;
                    result.address1 = dtProposal.Rows[0]["address1"] != null ? dtProposal.Rows[0]["address1"].ToString() : string.Empty;
                    result.address2 = dtProposal.Rows[0]["address2"] != null ? dtProposal.Rows[0]["address2"].ToString() : string.Empty;
                    result.landmark = dtProposal.Rows[0]["landmark"] != null ? dtProposal.Rows[0]["landmark"].ToString() : string.Empty;
                    //result.stateid = !string.IsNullOrEmpty(dtProposal.Rows[0]["stateid"].ToString()) ? Convert.ToInt32(dtProposal.Rows[0]["stateid"].ToString()) : 0;
                    result.statename = dtProposal.Rows[0]["statename"] != null ? dtProposal.Rows[0]["statename"].ToString() : string.Empty;

                    result.panno = dtProposal.Rows[0]["panno"] != null ? dtProposal.Rows[0]["panno"].ToString() : string.Empty;
                    result.aadharno = dtProposal.Rows[0]["aadharno"] != null ? dtProposal.Rows[0]["aadharno"].ToString() : string.Empty;
                    result.annualincome = dtProposal.Rows[0]["annualincome"] != null ? dtProposal.Rows[0]["annualincome"].ToString() : string.Empty;

                    result.p_referenceId = dtProposal.Rows[0]["p_referenceId"] != null ? dtProposal.Rows[0]["p_referenceId"].ToString() : string.Empty;
                    result.p_proposalNum = dtProposal.Rows[0]["p_proposalNum"] != null ? dtProposal.Rows[0]["p_proposalNum"].ToString() : string.Empty;
                    result.pay_policy_Number = dtProposal.Rows[0]["pay_policy_Number"] != null ? dtProposal.Rows[0]["pay_policy_Number"].ToString() : string.Empty;
                    result.pay_proposal_Number = dtProposal.Rows[0]["pay_proposal_Number"] != null ? dtProposal.Rows[0]["pay_proposal_Number"].ToString() : string.Empty;
                    result.p_totalPremium = dtProposal.Rows[0]["p_totalPremium"] != null ? dtProposal.Rows[0]["p_totalPremium"].ToString() : string.Empty;
                    result.p_paymenturl = dtProposal.Rows[0]["p_paymenturl"] != null ? dtProposal.Rows[0]["p_paymenturl"].ToString() : string.Empty;
                    result.logoUrl = dtProposal.Rows[0]["logoUrl"] != null ? dtProposal.Rows[0]["logoUrl"].ToString() : string.Empty;
                    result.p_rowpaymenturl = dtProposal.Rows[0]["p_rowpaymenturl"] != null ? dtProposal.Rows[0]["p_rowpaymenturl"].ToString() : string.Empty;

                    result.row_premium = dtProposal.Rows[0]["row_premium"] != null ? dtProposal.Rows[0]["row_premium"].ToString() : string.Empty;
                    result.row_basePremium = dtProposal.Rows[0]["row_basePremium"] != null ? dtProposal.Rows[0]["row_basePremium"].ToString() : string.Empty;
                    result.row_serviceTax = dtProposal.Rows[0]["row_serviceTax"] != null ? dtProposal.Rows[0]["row_serviceTax"].ToString() : string.Empty;
                    result.row_totalPremium = dtProposal.Rows[0]["row_totalPremium"] != null ? dtProposal.Rows[0]["row_totalPremium"].ToString() : string.Empty;
                    result.row_discountPercent = dtProposal.Rows[0]["row_discountPercent"] != null ? dtProposal.Rows[0]["row_discountPercent"].ToString() : string.Empty;
                    result.row_insuranceCompany = dtProposal.Rows[0]["row_insuranceCompany"] != null ? dtProposal.Rows[0]["row_insuranceCompany"].ToString() : string.Empty;
                    result.row_policyType = dtProposal.Rows[0]["row_policyType"] != null ? dtProposal.Rows[0]["row_policyType"].ToString() : string.Empty;
                    result.row_planname = dtProposal.Rows[0]["row_planname"] != null ? dtProposal.Rows[0]["row_planname"].ToString() : string.Empty;
                    result.row_policyTypeName = dtProposal.Rows[0]["row_policyTypeName"] != null ? dtProposal.Rows[0]["row_policyTypeName"].ToString() : string.Empty;
                    result.row_policyName = dtProposal.Rows[0]["row_policyName"] != null ? dtProposal.Rows[0]["row_policyName"].ToString() : string.Empty;

                    result.cityid = dtProposal.Rows[0]["cityid"] != null ? Convert.ToInt32(dtProposal.Rows[0]["cityid"].ToString()) : 0;
                    result.cityname = dtProposal.Rows[0]["cityname"] != null ? dtProposal.Rows[0]["cityname"].ToString() : string.Empty;
                    result.areaid = dtProposal.Rows[0]["areaid"] != null ? Convert.ToInt32(dtProposal.Rows[0]["areaid"].ToString()) : 0;
                    result.areaname = dtProposal.Rows[0]["areaname"] != null ? dtProposal.Rows[0]["areaname"].ToString() : string.Empty;
                    result.firstname = dtProposal.Rows[0]["firstname"] != null ? dtProposal.Rows[0]["firstname"].ToString() : string.Empty;
                    result.lastname = dtProposal.Rows[0]["lastname"] != null ? dtProposal.Rows[0]["lastname"].ToString() : string.Empty;
                    result.mobile = dtProposal.Rows[0]["mobile"] != null ? dtProposal.Rows[0]["mobile"].ToString() : string.Empty;
                    result.emailid = dtProposal.Rows[0]["emailid"] != null ? dtProposal.Rows[0]["emailid"].ToString() : string.Empty;
                    result.pincode = dtProposal.Rows[0]["pincode"] != null ? dtProposal.Rows[0]["pincode"].ToString() : string.Empty;
                    result.gender = dtProposal.Rows[0]["gender"] != null ? dtProposal.Rows[0]["gender"].ToString() : string.Empty;
                    result.mstatus = dtProposal.Rows[0]["maritalstatus"] != null ? dtProposal.Rows[0]["maritalstatus"].ToString() : string.Empty;
                    result.title = dtProposal.Rows[0]["title"] != null ? dtProposal.Rows[0]["title"].ToString() : string.Empty;
                    result.isproposerinsured = dtProposal.Rows[0]["isproposerinsured"] != null ? Convert.ToBoolean(dtProposal.Rows[0]["isproposerinsured"].ToString()) : false;
                    result.criticalillness = dtProposal.Rows[0]["criticalillness"] != null ? Convert.ToBoolean(dtProposal.Rows[0]["criticalillness"].ToString()) : false;
                    result.ispropnominee = dtProposal.Rows[0]["ispropnominee"] != null ? Convert.ToBoolean(dtProposal.Rows[0]["ispropnominee"].ToString()) : false;
                    result.proposaljson = dtProposal.Rows[0]["perposalrespo"] != null ? dtProposal.Rows[0]["perposalrespo"].ToString() : string.Empty;

                    result.nTitle = dtProposal.Rows[0]["ntitle"] != null ? dtProposal.Rows[0]["ntitle"].ToString() : string.Empty;
                    result.nFirstName = dtProposal.Rows[0]["nfirstname"] != null ? dtProposal.Rows[0]["nfirstname"].ToString() : string.Empty;
                    result.nLastName = dtProposal.Rows[0]["nlastname"] != null ? dtProposal.Rows[0]["nlastname"].ToString() : string.Empty;
                    result.nDOB = dtProposal.Rows[0]["ndob"] != null ? dtProposal.Rows[0]["ndob"].ToString() : string.Empty;
                    result.nAge = dtProposal.Rows[0]["nage"] != null ? Convert.ToInt32(dtProposal.Rows[0]["nage"].ToString()) : 0;
                    result.nRelation = dtProposal.Rows[0]["nrelation"] != null ? dtProposal.Rows[0]["nrelation"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static List<RelationMaster> GetRelation(string companyid, string productId, string policyfor)
        {
            List<RelationMaster> relationList = new List<RelationMaster>();

            //string wherecon = "CompanyId='" + companyid + "' and status=1 and ProductDetailId='" + productId + "'";
            string wherecon = "CompanyId='" + companyid + "' and status=1";
            if (companyid == "3")
            {
                wherecon = wherecon + " and RelationType like '%" + policyfor + "%'";
            }

            if (companyid == "1")
            {
                wherecon = wherecon + " and productdetailid='" + productId + "'";
            }

            DataTable dtRelation = ConnectToDataBase.GetRecordFromTable("*", "T_Relationship_Common", wherecon, "api");

            if (dtRelation != null && dtRelation.Rows.Count > 0)
            {
                for (int i = 0; i < dtRelation.Rows.Count; i++)
                {
                    RelationMaster relation = new RelationMaster();

                    relation.RelationshipId = dtRelation.Rows[i]["RelationshipId"].ToString().Trim();
                    relation.RelationshipName = dtRelation.Rows[i]["RelationshipName"].ToString().Trim();
                    relation.CompanyId = Convert.ToInt32(dtRelation.Rows[i]["CompanyId"].ToString().Trim());
                    relation.CompanyName = dtRelation.Rows[i]["CompanyName"].ToString().Trim();
                    relation.RelationType = dtRelation.Rows[i]["RelationType"].ToString().Trim();
                    relation.Title = dtRelation.Rows[i]["Title"].ToString().Trim();
                    relationList.Add(relation);
                }
            }

            return relationList;
        }
        public static bool InsertSearchUrlParaDetail(string enquiryid, string urlpara, string selectedproductjson)
        {
            string fieldsWithValue = "searchurl='" + urlpara + "',selectedproductjson='" + selectedproductjson + "'";
            string whereCondition = "Enquiry_Id='" + enquiryid + "'";

            string[] paraSplit = urlpara.Split('&');

            string respoFieldsWithValue = "productid='" + paraSplit[1].Split('=')[1] + "',suminsuredid='" + paraSplit[3].Split('=')[1] + "',suminsured='" + paraSplit[4].Split('=')[1] + "',companyid='" + paraSplit[2].Split('=')[1] + "'";
            bool isRespoTab = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Enquiry_Response", respoFieldsWithValue, "enquiry_id='" + enquiryid + "'", "api");

            return ConnectToDataBase.UpdateRecordIntoAnyTable("T_NewEnquiry_Health", fieldsWithValue, whereCondition, "api");
        }

        private static string _urlparakey = "health_urlpara";
        private static readonly MemoryCache _urlparacache = MemoryCache.Default;
        private static void StoreUrlParaInCache(string urlpara)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);

            _urlparacache.Add(_urlparakey, urlpara, cacheItemPolicy);
        }
        private static string GetHealth_UrlParaFromCache()
        {
            if (!_urlparacache.Contains(_urlparakey))
            {
                RemoveHealth_UrlParaFromCache();
                return (string)_urlparacache.Get(_urlparakey);
            }

            return _urlparacache.Get(_urlparakey) as string;
        }
        public static void RemoveHealth_UrlParaFromCache()
        {
            if (string.IsNullOrEmpty(_urlparakey))
            {
                _urlparacache.Dispose();
            }
            else
            {
                _urlparacache.Remove(_urlparakey);
            }
        }
        private static bool IsInsuredDetailExistOrNot(string enquiryid)
        {
            string fileds = "*";
            string tablename = "T_Health_Insured";

            DataTable dtProposal = ConnectToDataBase.GetRecordFromTable(fileds, tablename, "enquiryid='" + enquiryid + "'", "api");

            if (dtProposal != null && dtProposal.Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        public static bool InsertInsuredDetails(CreateHealth_Proposal proposal)
        {
            bool issuccess = false;
            try
            {
                if (proposal.InsuredDetails != null && proposal.InsuredDetails.Count > 0)
                {
                    //bool isDeleted = ConnectToDataBase.DeleteRecordFromAnyTable("T_Health_Insured", "enquiryid='" + proposal.enquiryid + "'", "api");
                    if (!IsInsuredDetailExistOrNot(proposal.enquiryid))
                    {
                        foreach (var item in proposal.InsuredDetails)
                        {
                            string fileds = "enquiryid,productid,companyid,relationid,relationname,title,firstname,lastname,dob,height,weight,isinsured,issame,occupationId,occupation,engageManualLabour,engageWinterSports,illness,illnessdesc,engageManualLabourDesc,engageWinterSportDesc";
                            string fieldValue = "'" + proposal.enquiryid + "','" + proposal.productid + "','" + proposal.companyid + "','" + item.relationid + "','" + item.relationname + "','" + item.title + "','" + item.firstname + "','" + item.lastname + "','" + item.dob + "','" + item.height + "','" + item.weight + "'," + (item.isinsured ? "1" : "0") + "," + (item.isinsured ? "1" : "0") + ",'" + item.occupationid + "','" + item.occupation + "'," + (Convert.ToBoolean(item.isengagemanuallabour.ToLower()) ? "1" : "0") + "," + (Convert.ToBoolean(item.isengagewintersports.ToLower()) ? "1" : "0") + ", " + (Convert.ToBoolean(item.isillness.ToLower()) ? "1" : "0") + ",'" + item.illnessdesc + "','" + item.engageManualLabourDesc + "','" + item.engageWinterSportDesc + "'";

                            if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, "T_Health_Insured", "api"))
                            {
                                issuccess = true;
                            }
                            else
                            {
                                issuccess = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in proposal.InsuredDetails)
                        {
                            string fieldsWithValues = "enquiryid='" + proposal.enquiryid + "',productid='" + proposal.productid + "',companyid='" + proposal.companyid + "',relationid='" + item.relationid + "',relationname='" + item.relationname + "',title='" + item.title + "',firstname='" + item.firstname + "',lastname='" + item.lastname + "',dob='" + item.dob + "',height='" + item.height + "',weight='" + item.weight + "',isinsured=" + (item.isinsured ? "1" : "0") + ",occupationId='" + item.occupationid + "',engageManualLabour=" + (Convert.ToBoolean(item.isengagemanuallabour.ToLower()) ? "1" : "0") + ",engageWinterSports=" + (Convert.ToBoolean(item.isengagewintersports.ToLower()) ? "1" : "0") + ",illness=" + (Convert.ToBoolean(item.isillness.ToLower()) ? "1" : "0") + ",illnessdesc='" + item.illnessdesc + "'";

                            if (ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Insured", fieldsWithValues, "enquiryid='" + proposal.enquiryid + "' and id=" + item.id + "", "api"))
                            {
                                issuccess = true;
                            }
                            else
                            {
                                issuccess = false;
                                break;
                            }
                        }
                    }

                    string fieldsWithValue = "ntitle='" + proposal.nTitle + "',nfirstname='" + proposal.nFirstName + "',nlastname='" + proposal.nLastName + "',ndob='" + proposal.nDOB + "',nage='" + proposal.nAge + "',nrelation='" + proposal.nRelation + "'";
                    ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, "enquiryid='" + proposal.enquiryid + "'", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static List<Health_Insured> GetInsuredDetails(SearchUrlParaMeter para)
        {
            List<Health_Insured> insuredList = new List<Health_Insured>();
            try
            {
                string wherecon = "enquiryid='" + para.enquiry_id + "' and productid='" + para.productid + "' and companyid='" + para.companyid + "'";
                DataTable dtInsured = ConnectToDataBase.GetRecordFromTable("*", "T_Health_Insured", wherecon, "api");
                if (dtInsured != null && dtInsured.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsured.Rows.Count; i++)
                    {
                        Health_Insured insured = new Health_Insured();
                        insured.id = Convert.ToInt32(dtInsured.Rows[i]["id"].ToString());
                        insured.enquiryid = dtInsured.Rows[i]["enquiryid"].ToString();
                        insured.productid = dtInsured.Rows[i]["productid"].ToString();
                        insured.companyid = Convert.ToInt32(dtInsured.Rows[i]["companyid"].ToString());
                        insured.relationid = dtInsured.Rows[i]["relationid"].ToString();
                        insured.relationname = dtInsured.Rows[i]["relationname"].ToString();
                        insured.title = dtInsured.Rows[i]["title"].ToString();
                        insured.firstname = dtInsured.Rows[i]["firstname"].ToString();
                        insured.lastname = dtInsured.Rows[i]["lastname"].ToString();
                        insured.dob = dtInsured.Rows[i]["dob"].ToString();
                        insured.height = Convert.ToInt32(dtInsured.Rows[i]["height"].ToString());
                        insured.weight = Convert.ToInt32(dtInsured.Rows[i]["weight"].ToString());
                        insured.isinsured = Convert.ToBoolean(dtInsured.Rows[i]["isinsured"].ToString());
                        insured.issame = Convert.ToBoolean(dtInsured.Rows[i]["issame"].ToString());
                        insured.occupationId = dtInsured.Rows[i]["occupationId"].ToString();
                        insured.engageManualLabour = Convert.ToBoolean(dtInsured.Rows[i]["engageManualLabour"].ToString());
                        insured.engageWinterSports = Convert.ToBoolean(dtInsured.Rows[i]["engageWinterSports"].ToString());
                        insured.illness = Convert.ToBoolean(dtInsured.Rows[i]["illness"].ToString());
                        insured.illnessdesc = dtInsured.Rows[i]["illnessdesc"].ToString();
                        insured.engageManualLabourDesc = dtInsured.Rows[i]["engageManualLabourDesc"].ToString();
                        insured.engageWinterSportDesc = dtInsured.Rows[i]["engageWinterSportDesc"].ToString();
                        insuredList.Add(insured);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return insuredList;
        }
        public static List<Health_Insured> GetInsuredDetail(string enquiryid)
        {
            List<Health_Insured> insuredList = new List<Health_Insured>();
            try
            {
                string wherecon = "enquiryid='" + enquiryid + "' order by id";
                DataTable dtInsured = ConnectToDataBase.GetRecordFromTable("*", "T_Health_Insured", wherecon, "api");
                if (dtInsured != null && dtInsured.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsured.Rows.Count; i++)
                    {
                        Health_Insured insured = new Health_Insured();
                        insured.id = Convert.ToInt32(dtInsured.Rows[i]["id"].ToString());
                        insured.enquiryid = dtInsured.Rows[i]["enquiryid"].ToString();
                        insured.productid = dtInsured.Rows[i]["productid"].ToString();
                        insured.companyid = Convert.ToInt32(dtInsured.Rows[i]["companyid"].ToString());
                        insured.relationid = dtInsured.Rows[i]["relationid"].ToString();
                        insured.relationname = dtInsured.Rows[i]["relationname"].ToString();
                        insured.title = dtInsured.Rows[i]["title"].ToString();
                        insured.firstname = dtInsured.Rows[i]["firstname"].ToString();
                        insured.lastname = dtInsured.Rows[i]["lastname"].ToString();
                        insured.dob = dtInsured.Rows[i]["dob"].ToString();
                        insured.height = Convert.ToInt32(dtInsured.Rows[i]["height"].ToString());
                        insured.weight = Convert.ToInt32(dtInsured.Rows[i]["weight"].ToString());
                        insured.isinsured = Convert.ToBoolean(dtInsured.Rows[i]["isinsured"].ToString());
                        insured.issame = Convert.ToBoolean(dtInsured.Rows[i]["issame"].ToString());
                        insured.medicalhistory = dtInsured.Rows[i]["medicalhistory"].ToString();
                        insured.occupationId = dtInsured.Rows[i]["occupationId"].ToString();
                        insured.occupation = dtInsured.Rows[i]["occupation"].ToString();

                        insured.engageManualLabour = Convert.ToBoolean(dtInsured.Rows[i]["engageManualLabour"].ToString());
                        insured.engageWinterSports = Convert.ToBoolean(dtInsured.Rows[i]["engageWinterSports"].ToString());
                        insured.illness = Convert.ToBoolean(dtInsured.Rows[i]["illness"].ToString());
                        insured.illnessdesc = dtInsured.Rows[i]["illnessdesc"].ToString();
                        insured.engageManualLabourDesc = dtInsured.Rows[i]["engageManualLabourDesc"].ToString();
                        insured.engageWinterSportDesc = dtInsured.Rows[i]["engageWinterSportDesc"].ToString();
                        insuredList.Add(insured);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return insuredList;
        }
        public static bool UpdateQuestionerDetails(string enquiryid, List<PerposalQuestion> medhistory)
        {
            bool issuccess = false;

            try
            {
                if (medhistory != null && medhistory.Count > 0)
                {
                    foreach (var item in medhistory)
                    {
                        string fieldsWithValue = "medicalhistory='" + item.questionjson.Replace("'", "''") + "'";
                        string whereCondition = "enquiryid='" + enquiryid + "' and firstname='" + item.firstname + "' and lastname='" + item.lastnane + "'";

                        issuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Insured", fieldsWithValue, whereCondition, "api");
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static bool InsertPerposalDetails(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            bool issuccess = false;
            try
            {
                string wherecon = "enquiryid='" + enquiry_id + "' order by id";
                DataTable dtPerposal = ConnectToDataBase.GetRecordFromTable("*", "T_Health_Payment", wherecon, "api");

                if (dtPerposal != null && dtPerposal.Rows.Count > 0)
                {
                    string fieldsWithValue = "pp_referenceId='" + referenceId + "',pp_premium='" + premium + "',pp_totalPremium='" + totalPremium + "',pp_proposalNum='" + proposalNum + "',pp_productDetailId='" + productDetailId + "',pp_companyId='" + companyId + "',pp_rowpaymenturl='" + paymenturl + "',pp_serviceTax='" + servicetax + "'";//,pay_proposal_Number='" + proposalNum + "'";
                    string whereCondition = "enquiryid='" + enquiry_id + "'";

                    issuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, whereCondition, "api");
                }
                else
                {
                    string fileds = "enquiryId,pp_referenceId,company,pp_companyId,pp_premium,pp_totalPremium,pp_proposalNum,pp_productDetailId,pp_rowpaymenturl,logoUrl,pp_serviceTax";
                    string fieldValue = "'" + enquiry_id + "','" + referenceId + "',(select CompanyName from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + companyId + "','" + premium + "','" + totalPremium + "','" + proposalNum + "','" + productDetailId + "','" + paymenturl + "',(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + servicetax + "'";

                    issuccess = ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, "T_Health_Payment", "api");
                }


                string p_fieldsWithValue = "p_proposalNum='" + proposalNum + "',p_totalPremium='" + premium + "',logoUrl=(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),p_rowpaymenturl='" + paymenturl + "'";
                if (companyId == "2")
                {
                    p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId='" + proposalNum + "',pay_proposal_Number='" + proposalNum + "'";
                    //update pp_referenceId in payment table
                    ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Payment", "pp_referenceId='" + proposalNum + "'", "enquiryid='" + enquiry_id + "'", "api");
                }
                else
                {
                    p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId = '" + referenceId + "'";
                }
                string p_whereCondition = "enquiryid='" + enquiry_id + "'";

                bool isp_updated = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", p_fieldsWithValue, p_whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static Health_Payment GetHealthPaymentTableDetails(string enquiryid)
        {
            Health_Payment payment = new Health_Payment();
            try
            {
                string wherecon = "enquiryid='" + enquiryid + "' order by id";
                DataTable dtPayment = ConnectToDataBase.GetRecordFromTable("*", "T_Health_Payment", wherecon, "api");
                if (dtPayment != null && dtPayment.Rows.Count > 0)
                {
                    payment.id = Convert.ToInt32(dtPayment.Rows[0]["id"].ToString());
                    payment.enquiryId = dtPayment.Rows[0]["enquiryId"].ToString();
                    payment.company = dtPayment.Rows[0]["company"].ToString();
                    payment.pp_companyId = dtPayment.Rows[0]["pp_companyId"].ToString();
                    payment.pp_premium = dtPayment.Rows[0]["pp_premium"].ToString();
                    payment.pp_totalPremium = dtPayment.Rows[0]["pp_totalPremium"].ToString();
                    payment.pp_proposalNum = dtPayment.Rows[0]["pp_proposalNum"].ToString();
                    payment.pp_productDetailId = dtPayment.Rows[0]["pp_productDetailId"].ToString();
                    payment.pp_paymenturl = dtPayment.Rows[0]["pp_paymenturl"].ToString();
                    payment.pp_rowpaymenturl = dtPayment.Rows[0]["pp_rowpaymenturl"].ToString();
                    payment.cc_policyNumber = dtPayment.Rows[0]["cc_policyNumber"].ToString();
                    payment.cc_transactionRefNum = dtPayment.Rows[0]["cc_transactionRefNum"].ToString();
                    payment.cc_uwDecision = dtPayment.Rows[0]["cc_uwDecision"].ToString();
                    payment.cc_errorFlag = dtPayment.Rows[0]["cc_errorFlag"].ToString();
                    payment.cc_errorMsg = dtPayment.Rows[0]["cc_errorMsg"].ToString();
                    payment.ss_ORDERID = dtPayment.Rows[0]["ss_ORDERID"].ToString();
                    payment.ss_MID = dtPayment.Rows[0]["ss_MID"].ToString();
                    payment.ss_TXNID = dtPayment.Rows[0]["ss_TXNID"].ToString();
                    payment.ss_TXNAMOUNT = dtPayment.Rows[0]["ss_TXNAMOUNT"].ToString();
                    payment.ss_PAYMENTMODE = dtPayment.Rows[0]["ss_PAYMENTMODE"].ToString();
                    payment.ss_CURRENCY = dtPayment.Rows[0]["ss_CURRENCY"].ToString();
                    payment.ss_TXNDATE = dtPayment.Rows[0]["ss_TXNDATE"].ToString();
                    payment.ss_STATUS = dtPayment.Rows[0]["ss_STATUS"].ToString();
                    payment.ss_RESPCODE = dtPayment.Rows[0]["ss_RESPCODE"].ToString();
                    payment.ss_RESPMSG = dtPayment.Rows[0]["ss_RESPMSG"].ToString();
                    payment.ss_GATEWAYNAME = dtPayment.Rows[0]["ss_GATEWAYNAME"].ToString();
                    payment.ss_BANKTXNID = dtPayment.Rows[0]["ss_BANKTXNID"].ToString();
                    payment.ss_BANKNAME = dtPayment.Rows[0]["ss_BANKNAME"].ToString();
                    payment.ss_CHECKSUMHASH = dtPayment.Rows[0]["ss_CHECKSUMHASH"].ToString();
                    payment.Status = dtPayment.Rows[0]["Status"].ToString();
                    payment.logoUrl = dtPayment.Rows[0]["logoUrl"].ToString();
                    payment.pp_referenceId = dtPayment.Rows[0]["pp_referenceId"].ToString();
                    payment.pp_serviceTax = dtPayment.Rows[0]["pp_serviceTax"].ToString();
                    payment.payment_status = !string.IsNullOrEmpty(dtPayment.Rows[0]["payment_status"].ToString()) ? Convert.ToBoolean(dtPayment.Rows[0]["payment_status"].ToString()) : false;
                    payment.CreatedDate = dtPayment.Rows[0]["CreatedDate"].ToString();
                    payment.UpdatedDate = dtPayment.Rows[0]["UpdatedDate"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return payment;
        }

        public static DataTable GetOccupationDdl(string compId)
        {
            return ConnectToDataBase.GetRecordFromTable("*", "tbl_OccupationMaster", "CompanyID = '" + compId + "'", "api");
        }

        #region [From Api DataBase]
        public static DataTable GetNominee_CommonDetails(string companyid, string type)
        {
            string wherecon = "CompanyId=" + companyid + " and Type='" + type + "' order by Relationship";
            return ConnectToDataBase.GetRecordFromTable("*", "tbl_Nominee_Common", wherecon, "api");
        }

        public static DataTable GetAnnualIncomeDetails(string enquiryid, string companyid = "")
        {
            string wherecon = "";
            if (!string.IsNullOrEmpty(companyid))
            {
                wherecon = "CompanyId='" + companyid + "'";
            }
            else
            {
                wherecon = "CompanyId in (select companyid from T_Health_Proposal where enquiryid='" + enquiryid + "')";
            }
            return ConnectToDataBase.GetRecordFromTable("*", "Master_Annual_Income", wherecon, "api");
        }
        #endregion
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string policypdflink)
        {
            try
            {
                string fieldsWithValue = "policypdflink='" + (Config.WebsiteUrl + policypdflink) + "'";
                return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, ("enquiryid='" + enquiryid + "'"), "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string purchaseToken = "")
        {
            try
            {
                bool issuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_policy_Number='" + policyNumber + "'", ("enquiryid='" + enquiryid + "'"), "api");

                string fieldsWithValue = "cc_policyNumber='" + policyNumber + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Star_PurchaseToken='" + purchaseToken + "',Status='" + status + "',payment_status='1',UpdatedDate=getdate()";
                if (ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, ("enquiryid='" + enquiryid + "'"), "api"))
                {
                    return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_paymentdate=getdate()", ("enquiryid='" + enquiryid + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateAdityBirlaPaymentDetails(string amount, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string txnDate, string quoteId)
        {
            try
            {
                //bool issuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_policy_Number='" + policyNumber + "'", ("enquiryid='" + enquiryid + "'"), "api");

                string fieldsWithValue = "pp_totalPremium='" + amount + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Status='" + status + "',payment_status='1',ab_QuoteId='" + quoteId + "',UpdatedDate='" + txnDate + "'";
                if (ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, ("enquiryid='" + enquiryid + "'"), "api"))
                {
                    return ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_paymentdate=getdate()", ("enquiryid='" + enquiryid + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static string GetHealth_BrochureLink(string productid, string companyid, string linktype)
        {
            string brochureLink = string.Empty;
            try
            {
                DataTable dtDetails = ConnectToDataBase.GetRecordFromTable("BrochureLink,ClausesLink", "tbl_Product", "CompanyId=" + companyid + " and ProductId=" + productid + "", "api");
                if (dtDetails != null && dtDetails.Rows.Count > 0)
                {
                    if (linktype == "b")
                    {
                        brochureLink = dtDetails.Rows[0]["BrochureLink"].ToString();
                    }
                    else
                    {
                        brochureLink = dtDetails.Rows[0]["ClausesLink"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return brochureLink;
        }
        public static DataTable GetHealthTrackDetail(string enquiryid, ref DataTable dtAgencyDel, ref DataTable dtInsuredDel)
        {
            DataTable dtPerposerDetail = new DataTable();
            try
            {
                string p_query = "eh.Enquiry_Id,eh.AgencyId,eh.Policy_Type,eh.Sum_Insured,eh.Self,eh.SelfAge,eh.Spouse,eh.SpouseAge,eh.Son,eh.Son1Age,eh.Son2Age,eh.Son3Age,eh.Son4Age,eh.Daughter,eh.Daughter1Age, eh.Daughter2Age,eh.Daughter3Age,eh.Daughter4Age,eh.Father,eh.FatherAge,eh.Mother,eh.MotherAge,eh.Pin_Code, (UPPER(p.title)+' '+p.firstname+' '+p.lastname) as PerposerName,p.mobile,p.emailid,p.address1,p.address2,p.cityname,p.statename,p.pincode,p.annualincome,p.suminsured,p.pay_proposal_Number,p.pay_policy_Number,p.pay_paymentdate ,p.row_premium,p.row_basePremium,p.row_serviceTax,p.row_totalPremium,p.row_discountPercent,p.row_insuranceCompany,p.row_policyType,p.row_planname,p.row_policyTypeName,p.row_policyName ,p.ntitle,p.nfirstname,p.nlastname,p.ndob,p.nage,p.nrelation,p.policypdflink,p.p_rowpaymenturl as row_paymenturl,p.p_paymenturl as paymenturl,p.logoUrl, py.UpdatedDate as py_paymentdate,py.pp_premium,py.pp_totalPremium,py.cc_transactionRefNum,py.Status as Payment_Status,py.cc_uwDecision as Policy_Status";
                string p_tableName = "T_NewEnquiry_Health eh left join T_Health_Proposal p on eh.Enquiry_Id=p.EnquiryId left join T_Health_Payment py on eh.Enquiry_Id=py.EnquiryId";
                string p_wherecon = "eh.Enquiry_Id='" + enquiryid + "'";
                dtPerposerDetail = ConnectToDataBase.GetRecordFromTable(p_query, p_tableName, p_wherecon, "api");
                if (dtPerposerDetail != null && dtPerposerDetail.Rows.Count > 0)
                {
                    string agencyId = dtPerposerDetail.Rows[0]["AgencyId"].ToString();
                    if (!string.IsNullOrEmpty(agencyId))
                    {
                        string m_query = "(au.Title+' '+au.FirstName+' '+au.LastName) as OwnerName,au.AgencyName,au.BusinessPhone,au.EmailId,au.Address,(m.FirstName+' '+m.LastName) as RefName,m.EmailId as RefEmailId,m.MobileNo as RefMobileNo";
                        string m_tableName = "aspnet_user au left join [seeinsuredproduct_test].[dbo].[T_MemberLogin] m on au.ReferenceBy=m.LoginId";
                        dtAgencyDel = ConnectToDataBase.GetRecordFromTable(m_query, m_tableName, ("au.AgencyId=" + agencyId), "api");
                    }

                    dtInsuredDel = ConnectToDataBase.GetRecordFromTable("*", "T_Health_Insured", "EnquiryId='" + enquiryid + "'", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtPerposerDetail;
        }
        public static string SetPremiumBreakup(string basePremium, string premium, string discount, string serviceTax, string totalPremium, string period, string enquiryid)
        {
            return Health_Controller.SetPremiumBreakup(basePremium, premium, discount, serviceTax, totalPremium, period, enquiryid);
        }
        public static DataTable GetAddLessAddon(string enquiryid)
        {
            return Post_Health.GetAddLessAddon(enquiryid);
        }

        public static List<AddonChecked> GetSelectedAddLessAddon(string enquiryid)
        {
            List<AddonChecked> addonlist = new List<AddonChecked>();
            try
            {
                DataTable dtAddon = Post_Health.GetSelectedAddLessAddon(enquiryid);
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddon.Rows.Count; i++)
                    {
                        AddonChecked tempAddon = new AddonChecked();
                        tempAddon.addOnsId = dtAddon.Rows[i]["addOnsId"].ToString();
                        tempAddon.addOns = dtAddon.Rows[i]["addOns"].ToString();
                        tempAddon.code = dtAddon.Rows[i]["code"].ToString();
                        tempAddon.value = dtAddon.Rows[i]["value"].ToString();
                        tempAddon.calculation = dtAddon.Rows[i]["calculation"].ToString();
                        tempAddon.addOnValue = dtAddon.Rows[i]["addOnValue"].ToString();
                        tempAddon.BasePremium = dtAddon.Rows[i]["BasePremium"].ToString();
                        addonlist.Add(tempAddon);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return addonlist;
        }
        public static List<AddOne> GetAddOneList(string enquiryid)
        {
            List<AddOne> addonlist = new List<AddOne>();
            try
            {
                DataTable dtAddon = GetAddLessAddon(enquiryid);
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddon.Rows.Count; i++)
                    {
                        AddOne tempAddon = new AddOne();
                        tempAddon.addOnsId = Convert.ToInt32(dtAddon.Rows[i]["addOnsId"].ToString());
                        tempAddon.addOns = dtAddon.Rows[i]["addOns"].ToString();
                        tempAddon.code = dtAddon.Rows[i]["code"].ToString();
                        tempAddon.value = dtAddon.Rows[i]["value"].ToString();
                        tempAddon.calculation = dtAddon.Rows[i]["calculation"].ToString();
                        tempAddon.addOnValue = dtAddon.Rows[i]["addOnValue"].ToString();
                        tempAddon.productDetailID = dtAddon.Rows[i]["productDetailID"].ToString();
                        tempAddon.inquiry_id = dtAddon.Rows[i]["inquiryid"].ToString();
                        addonlist.Add(tempAddon);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return addonlist;
        }
        //public static DataTable GetAddLessAddon(string enquiryid, string addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID)
        //{
        //    return Post_Health.GetAddLessAddon(enquiryid, addOnsId, addOns, code, value, calculation, addOnValue, productDetailID);
        //}
        public static bool AddLessAddon(string[] addonlist)
        {
            return Post_Health.AddLessAddon(addonlist);
        }
    }
}