﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Helper.DataBase;
using insurance.Models;
using Post_Utility.Dashboard;
using Post_Utility.FrontSection;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Helper
{
    public static class AdministrationHelper
    {
        public static bool AgencyLogin(string userid, string password, ref string msg)
        {
            try
            {
                DataTable dtAgency = Administration_Helper.GetAgentDetailByUserId(userid);

                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    string agency_pass = dtAgency.Rows[0]["Password"].ToString();
                    bool agencystatus = dtAgency.Rows[0]["IsApproved"].ToString().ToLower().Trim() == "true" ? true : false;

                    if (agencystatus)
                    {
                        string tempPassword = Security.ApiBase64Decode(agency_pass);

                        if (password == tempPassword)
                        {
                            Administration_Helper.UpdateAgencyLastLogin(userid);
                            InitializeAgencySession(dtAgency);
                            return true;
                        }
                        else
                        {
                            msg = "Incorrect password !";
                        }
                    }
                    else
                    {
                        msg = "Your login suspended by administration !";
                    }
                }
                else
                {
                    msg = "Unsuccessful login !";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static void InitializeAgencySession(DataTable dtAgency)
        {
            if (dtAgency != null && dtAgency.Rows.Count > 0)
            {
                Agency_Detail olu = new Agency_Detail();

                olu.AgencyID = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyID"].ToString()) ? dtAgency.Rows[0]["AgencyID"].ToString() : string.Empty;
                olu.AgencyIdNew = !string.IsNullOrEmpty(dtAgency.Rows[0]["UserName"].ToString()) ? dtAgency.Rows[0]["UserName"].ToString() : string.Empty;
                olu.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyName"].ToString()) ? dtAgency.Rows[0]["AgencyName"].ToString() : string.Empty;
                olu.Address = !string.IsNullOrEmpty(dtAgency.Rows[0]["Address"].ToString()) ? dtAgency.Rows[0]["Address"].ToString() : string.Empty;
                olu.CountryID = !string.IsNullOrEmpty(dtAgency.Rows[0]["CountryID"].ToString()) ? dtAgency.Rows[0]["CountryID"].ToString() : string.Empty;
                //olu.CountryName = CommonClass.GetCountryName(olu.CountryID);
                olu.StateID = !string.IsNullOrEmpty(dtAgency.Rows[0]["StateID"].ToString()) ? dtAgency.Rows[0]["StateID"].ToString() : string.Empty;
                //olu.StateName = CommonClass.GetStateName(olu.StateID, olu.CountryID);
                olu.CityID = !string.IsNullOrEmpty(dtAgency.Rows[0]["CityID"].ToString()) ? dtAgency.Rows[0]["CityID"].ToString() : string.Empty;
                //olu.CityName = CommonClass.GetCityName(olu.CityID, olu.StateID);
                olu.Pincode = !string.IsNullOrEmpty(dtAgency.Rows[0]["Pincode"].ToString()) ? dtAgency.Rows[0]["Pincode"].ToString() : string.Empty;
                olu.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["GSTNo"].ToString()) ? dtAgency.Rows[0]["GSTNo"].ToString() : string.Empty;
                olu.BusinessEmailID = !string.IsNullOrEmpty(dtAgency.Rows[0]["BusinessEmailID"].ToString()) ? dtAgency.Rows[0]["BusinessEmailID"].ToString() : string.Empty;
                olu.BusinessPhone = !string.IsNullOrEmpty(dtAgency.Rows[0]["BusinessPhone"].ToString()) ? dtAgency.Rows[0]["BusinessPhone"].ToString() : string.Empty;
                olu.ReferenceBy = !string.IsNullOrEmpty(dtAgency.Rows[0]["ReferenceBy"].ToString()) ? Convert.ToInt32(dtAgency.Rows[0]["ReferenceBy"].ToString()) : 0;
                //olu.ReferenceByName = olu.ReferenceBy > 0 ? CommonClass.GetStaffName(olu.ReferenceBy.ToString()) : string.Empty;
                olu.BusinessAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[0]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[0]["BusinessAddressProofImg"].ToString() : string.Empty;
                olu.BusinessPanCardImg = !string.IsNullOrEmpty(dtAgency.Rows[0]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[0]["BusinessPanCardImg"].ToString() : string.Empty;
                olu.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[0]["Pan_No"].ToString()) ? dtAgency.Rows[0]["Pan_No"].ToString() : string.Empty;
                olu.Title = !string.IsNullOrEmpty(dtAgency.Rows[0]["Title"].ToString()) ? dtAgency.Rows[0]["Title"].ToString() : string.Empty;
                olu.FirstName = !string.IsNullOrEmpty(dtAgency.Rows[0]["FirstName"].ToString()) ? dtAgency.Rows[0]["FirstName"].ToString() : string.Empty;
                olu.LastName = !string.IsNullOrEmpty(dtAgency.Rows[0]["LastName"].ToString()) ? dtAgency.Rows[0]["LastName"].ToString() : string.Empty;
                olu.EmailID = !string.IsNullOrEmpty(dtAgency.Rows[0]["EmailId"].ToString()) ? dtAgency.Rows[0]["EmailId"].ToString() : string.Empty;
                olu.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[0]["Mobile"].ToString()) ? dtAgency.Rows[0]["Mobile"].ToString() : string.Empty;
                olu.PersonalAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[0]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[0]["PersonalAddressProofImg"].ToString() : string.Empty;
                olu.UserId = !string.IsNullOrEmpty(dtAgency.Rows[0]["UserName"].ToString()) ? dtAgency.Rows[0]["UserName"].ToString() : string.Empty;
                olu.CompanyLogoImg = !string.IsNullOrEmpty(dtAgency.Rows[0]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[0]["CompanyLogoImg"].ToString() : string.Empty;
                olu.Type = !string.IsNullOrEmpty(dtAgency.Rows[0]["Type"].ToString()) ? dtAgency.Rows[0]["Type"].ToString() : string.Empty;
                //olu.CreditLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["CreditLimit"].ToString()) ? dtAgency.Rows[0]["CreditLimit"].ToString() : string.Empty;
                //olu.TempCreditLimit = !string.IsNullOrEmpty(dtAgency.Rows[0]["TempCreditLimit"].ToString()) ? dtAgency.Rows[0]["TempCreditLimit"].ToString() : string.Empty;
                //olu.TempCreditDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["TempCreditDate"].ToString()) ? dtAgency.Rows[0]["TempCreditDate"].ToString() : string.Empty;
                olu.GroupType = !string.IsNullOrEmpty(dtAgency.Rows[0]["GroupType"].ToString()) ? dtAgency.Rows[0]["GroupType"].ToString() : string.Empty;
                olu.LastLoginDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["LastLoginDate"].ToString()) ? dtAgency.Rows[0]["LastLoginDate"].ToString() : string.Empty;
                olu.IsApproved = dtAgency.Rows[0]["IsApproved"].ToString().ToLower().Trim() == "true" ? true : false;
                //olu.BookingStatus = dtAgency.Rows[0]["BookingStatus"].ToString().ToLower().Trim() == "true" ? true : false;
                //olu.TempApiKey = !string.IsNullOrEmpty(dtAgency.Rows[0]["TempApiKey"].ToString()) ? dtAgency.Rows[0]["TempApiKey"].ToString() : string.Empty;
                // olu.EncryKey = !string.IsNullOrEmpty(dtAgency.Rows[0]["EncryKey"].ToString()) ? dtAgency.Rows[0]["EncryKey"].ToString() : string.Empty;
                olu.Password = !string.IsNullOrEmpty(dtAgency.Rows[0]["Password"].ToString()) ? Security.ApiBase64Decode(dtAgency.Rows[0]["Password"].ToString()) : string.Empty;
                // olu.IVKey = !string.IsNullOrEmpty(dtAgency.Rows[0]["IVKey"].ToString()) ? dtAgency.Rows[0]["IVKey"].ToString() : string.Empty;
                olu.Status = dtAgency.Rows[0]["IsApproved"].ToString().ToLower().Trim() == "true" ? true : false;
                olu.CreatedDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["Createdate"].ToString()) ? dtAgency.Rows[0]["Createdate"].ToString() : string.Empty;
                olu.UpdatedDate = !string.IsNullOrEmpty(dtAgency.Rows[0]["UpdatedDate"].ToString()) ? dtAgency.Rows[0]["UpdatedDate"].ToString() : string.Empty;


                List<Agency_Detail> lu = new List<Agency_Detail>();
                lu.Add(olu);
                HttpContext.Current.Session["agency"] = lu;
            }
        }
        public static bool IsAgencyLogin(ref Agency_Detail lu)
        {
            List<Agency_Detail> objLoginUser = new List<Agency_Detail>();
            bool idsuccess = IsAgencyLoginSuccess(ref objLoginUser);
            if (objLoginUser != null && objLoginUser.Count > 0)
            {
                lu = objLoginUser[0];
            }
            return idsuccess;
        }
        public static bool IsAgencyLoginSuccess(ref List<Agency_Detail> loginUserList)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["agency"] != null)
                {
                    loginUserList = (List<Agency_Detail>)HttpContext.Current.Session["agency"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }
        public static bool LogoutAgency(string agencyId)
        {
            bool isupdated = Administration_Helper.UpdateLastLoginDate(agencyId);
            HttpContext.Current.Session.Remove("agency");
            return true;
        }
        public static bool InsertLeadReq(LeadRequestModel model)
        {
            return Administration_Helper.Insert_LeadRequest(model.AgencyName, model.AgencyID, model.Name, model.Email, model.ContactNo, model.Age, model.Gender, model.Service, model.Remark);
        }
        public static bool UpdateBankDetails(BankDeatil agency, ref string msg)
        {
            bool value = false;
            try
            {
                string tablename = "T_AgencyBankDeatil";
                string filedswithvalue = "BankName='"+agency.BankName+ "',Branch='" + agency.Branch + "',AccountNo='" + agency.AccountNo + "',MobileNo='" + agency.MobileNo + "',IFSC='" + agency.IFSC + "',Status=1";
                string wherecondition = "AgencyID="+agency.AgencyID+"";
                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, filedswithvalue, wherecondition,"api"))
                {
                    msg = "bank details updated successfully.";
                    value = true;
                }
                else
                {
                    msg = "You are not authorized to update bank details.";
                    value = false;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return value;
        }

        public static bool UpdateIsverif(string mobileno)
        {
            bool value = false;
            try
            {
                string tablename = "T_AgencyBankDeatil";
                string filedswithvalue = "Isverified=1";
                string wherecondition = "MobileNo=" + mobileno + "";
                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, filedswithvalue, wherecondition, "api"))
                {                   
                    value = true;
                }
                else
                {                    
                    value = false;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return value;
        }
        public static bool AgencyResetPassword(CreateAgency agency, ref string msg)
        {
            bool value = false;
            try
            {
                DataTable dtAgency = Administration_Helper.GetAgentDetailByAgencyId(agency.AgencyID);
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    string agency_pass = dtAgency.Rows[0]["Password"].ToString();
                    string decrptpass = Security.ApiBase64Decode(agency_pass);
                    if (agency.CurrentPassword == decrptpass)
                    {
                        if (Administration_Helper.Update_AgencyPassword(Security.ApiBase64Encode(agency.Password), agency.AgencyID))
                        {
                            msg = "Your password changed successfully.";
                            value = true;
                        }
                    }
                    else
                    {
                        msg = "You have entered wrong current password.";
                        value = false;
                    }
                }
                else
                {
                    msg = "You are not authorized to change this user password.";
                    value = false;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return value;
        }
        public static DataTable GetEnquiryDetailsByAgencypro(string agencyid)
        {
            return Administration_Helper.GetEnquiryDetailsByAgencyPro(agencyid);
        }
        public static DataTable GetDasboarddetails(string agencyid)
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Merge(Dashboard_Helper.Get_HeatlhPolicy(agencyid));
                dt.Merge(Dashboard_Helper.Get_HeatlhProposal(agencyid));
                dt.Merge(Dashboard_Helper.Get_HeatlhLead(agencyid));
                dt.Merge(Dashboard_Helper.Get_MotorPolicy(agencyid));
                dt.Merge(Dashboard_Helper.Get_MotorProposal(agencyid));
                dt.Merge(Dashboard_Helper.Get_MotorLead(agencyid));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }
        public static decimal[] GetTotalPremium(string agencyid, string period = "day", string duration = "0")
        {
            decimal[] counts = new decimal[4];
            try
            {
                DataTable dt = Dashboard_Helper.Get_HealthTotalPremium(agencyid, period, duration);
                if (dt.Rows.Count > 0)
                {
                    counts[0] = Convert.ToDecimal(dt.Rows[0]["SUM"].ToString());
                }

                dt = Dashboard_Helper.Get_MotorTotalPremium(agencyid, period, duration);
                if (dt.Rows.Count > 0)
                {
                    counts[1] = Convert.ToDecimal(dt.Rows[0]["SUM"].ToString());
                }

                counts[2] = 0;
                counts[3] = 0;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return counts;
        }
    }
}