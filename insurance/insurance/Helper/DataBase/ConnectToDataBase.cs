﻿using insurance.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static insurance.Models.Custom.FrontAgencyModel;
using static insurance.Models.Health.HealthModel;

namespace insurance.Helper.DataBase
{
    public static class ConnectToDataBase
    {
        private static SqlConnection SeeInsuredConString = new SqlConnection(Config.SeeInsuredConString);
        private static SqlConnection SeeInsuredApiConString = new SqlConnection(Config.SeeInsuredApiConString);
        private static SqlConnection SeeInsuredMotorConString = new SqlConnection(Config.SeeInsuredMotorConString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }

        private static SqlConnection GetConnectionString(string dbType)
        {
            if (dbType == "api")
            {
                return SeeInsuredApiConString;
            }
            else if (dbType == "motor")
            {
                return SeeInsuredMotorConString;
            }
            return SeeInsuredConString;
        }
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("usp_GetRecordFromTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("tablename", tablename);
                Command.Parameters.AddWithValue("where", whereCondition);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }

        public static DataTable GetPolicygraph(string period, string duration, string database)
        {
            try
            {
                Command = new SqlCommand("Policygraph", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@period", period);
                Command.Parameters.AddWithValue("@duration", duration);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "PolicygraphT");
                ObjDataTable = ObjDataSet.Tables["Policygrapht"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }

        public static DataTable GetPolicygraphAgency(string Agencyid, string period, string durationValue, string database)
        {
            try
            {
                Command = new SqlCommand("PolicygraphbyAgency", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@AgencyId", Agencyid);
                Command.Parameters.AddWithValue("@period", period);
                Command.Parameters.AddWithValue("@duration", durationValue);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "PolicygraphbyAgencyT");
                ObjDataTable = ObjDataSet.Tables["PolicygraphbyAgencyT"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static bool InsertRecordToTable(string fileds, string fieldValue, string tableName, string database)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("values", fieldValue);
                Command.Parameters.AddWithValue("tablename", tableName);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static int InsertAgencyDetailReturnID(CreateAgency model, string database)
        {
            try
            {
                Command = new SqlCommand("usp_InsertAgencyRecord", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                //Command.Parameters.Add(new SqlParameter("@UserId", model.UserName));
                Command.Parameters.Add(new SqlParameter("@UserName", model.RegUserId));
                Command.Parameters.Add(new SqlParameter("@Password", model.Password));
                //Command.Parameters.Add(new SqlParameter("@IsApproved", false));
                //Command.Parameters.Add(new SqlParameter("@IsLockOut", false));
                //Command.Parameters.Add(new SqlParameter("@Createdate", DateTime.Now));
                //Command.Parameters.Add(new SqlParameter("@LastLoginDate", DateTime.Now));
                //Command.Parameters.Add(new SqlParameter("@FailedPasswordAttemptCount", 0));
                Command.Parameters.Add(new SqlParameter("@Pan_No", !string.IsNullOrEmpty(model.PanNo) ? model.PanNo : string.Empty));
                Command.Parameters.Add(new SqlParameter("@Address", model.Address));
                Command.Parameters.Add(new SqlParameter("@EmailId", model.EmailID));
                Command.Parameters.Add(new SqlParameter("@AgencyName", model.AgencyName));
                Command.Parameters.Add(new SqlParameter("@CountryID", model.CountryID));
                Command.Parameters.Add(new SqlParameter("@StateID", model.StateID));
                Command.Parameters.Add(new SqlParameter("@CityID", model.CityName));
                Command.Parameters.Add(new SqlParameter("@Pincode", model.Pincode));
                Command.Parameters.Add(new SqlParameter("@GSTNo", !string.IsNullOrEmpty(model.GSTNo) ? model.GSTNo : string.Empty));
                Command.Parameters.Add(new SqlParameter("@BusinessEmailID", model.BusinessEmailID));
                Command.Parameters.Add(new SqlParameter("@BusinessPhone", model.BusinessPhone));
                Command.Parameters.Add(new SqlParameter("@ReferenceBy", model.ReferenceBy));
                Command.Parameters.Add(new SqlParameter("@Title", model.Title));
                Command.Parameters.Add(new SqlParameter("@FirstName", model.FirstName));
                Command.Parameters.Add(new SqlParameter("@LastName", model.LastName));
                Command.Parameters.Add(new SqlParameter("@Mobile", model.Mobile));
                Command.Parameters.Add(new SqlParameter("@GroupType", !string.IsNullOrEmpty(model.GroupType) ? model.GroupType : string.Empty));
                //Command.Parameters.Add(new SqlParameter("@UpdatedDate", DateTime.Now));
                Command.Parameters.Add(new SqlParameter("@AgencyCode", model.AgencyCode));
                //Command.Parameters.Add(new SqlParameter("@AgencyID", model.AgencyID));
                //Command.Parameters.Add(new SqlParameter("@Id", 0));
                Command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                int id = (int)Command.Parameters["@Id"].Value;
                CloseConnection(GetConnectionString(database));


                if (isSuccess > 0)
                {
                    return id;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static int InsertRecordToTableReturnID(string fileds, string fieldValue, string tableName, string database)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTableReturnID", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@fileds", fileds));
                Command.Parameters.Add(new SqlParameter("@values", fieldValue));
                Command.Parameters.Add(new SqlParameter("@tablename", tableName));
                Command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                string id = Command.Parameters["@Id"].Value.ToString();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool UpdateRecordIntoAnyTable(string tableName, string fieldsWithValue, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("ups_UpdateRecordToTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("TableName", tableName);
                Command.Parameters.AddWithValue("FieldsWithValue", fieldsWithValue);
                Command.Parameters.AddWithValue("WhereCondition", whereCondition);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool DeleteRecordFromAnyTable(string tableName, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("usp_DeleteRecordFromTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("tablename", tableName);
                Command.Parameters.AddWithValue("where", whereCondition);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertSaveGenratedQuotes(NewEnquiry_Health model, string database, string userid = "", string password = "")
        {
            try
            {
                Command = new SqlCommand("usp_InsertGenratedNewQuotes", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@AgencyId", model.AgencyId));
                Command.Parameters.Add(new SqlParameter("@AgencyName", model.AgencyName));
                Command.Parameters.Add(new SqlParameter("@Policy_Type", model.Policy_Type));
                Command.Parameters.Add(new SqlParameter("@Sum_Insured", model.Sum_Insured));
                Command.Parameters.Add(new SqlParameter("@Self", model.Self));
                Command.Parameters.Add(new SqlParameter("@SelfAge", model.SelfAge));
                Command.Parameters.Add(new SqlParameter("@Spouse", model.Spouse));
                Command.Parameters.Add(new SqlParameter("@SpouseAge", model.SpouseAge));
                Command.Parameters.Add(new SqlParameter("@Son", model.Son));
                Command.Parameters.Add(new SqlParameter("@Son1Age", model.Son1Age));
                Command.Parameters.Add(new SqlParameter("@Son2Age", model.Son2Age));
                Command.Parameters.Add(new SqlParameter("@Son3Age", model.Son3Age));
                Command.Parameters.Add(new SqlParameter("@Son4Age", model.Son4Age));
                Command.Parameters.Add(new SqlParameter("@Daughter", model.Daughter));
                Command.Parameters.Add(new SqlParameter("@Daughter1Age", model.Daughter1Age));
                Command.Parameters.Add(new SqlParameter("@Daughter2Age", model.Daughter2Age));
                Command.Parameters.Add(new SqlParameter("@Daughter3Age", model.Daughter3Age));
                Command.Parameters.Add(new SqlParameter("@Daughter4Age", model.Daughter4Age));
                Command.Parameters.Add(new SqlParameter("@Father", model.Father));
                Command.Parameters.Add(new SqlParameter("@FatherAge", model.FatherAge));
                Command.Parameters.Add(new SqlParameter("@Mother", model.Mother));
                Command.Parameters.Add(new SqlParameter("@MotherAge", model.MotherAge));
                Command.Parameters.Add(new SqlParameter("@Gender", !string.IsNullOrEmpty(model.Gender) ? model.Gender : ""));
                Command.Parameters.Add(new SqlParameter("@First_Name", !string.IsNullOrEmpty(model.First_Name) ? model.First_Name : ""));
                Command.Parameters.Add(new SqlParameter("@Last_Name", !string.IsNullOrEmpty(model.Last_Name) ? model.Last_Name : ""));
                Command.Parameters.Add(new SqlParameter("@Email_Id", !string.IsNullOrEmpty(model.Email_Id) ? model.Email_Id : ""));
                Command.Parameters.Add(new SqlParameter("@Mobile_No", !string.IsNullOrEmpty(model.Mobile_No) ? model.Mobile_No : ""));
                Command.Parameters.Add(new SqlParameter("@Pin_Code", !string.IsNullOrEmpty(model.Pin_Code) ? model.Pin_Code : ""));
                Command.Parameters.Add(new SqlParameter("@Is_Term_Accepted", model.Is_Term_Accepted));
                Command.Parameters.Add(new SqlParameter("@Enquiry_Id", model.Enquiry_Id));
                Command.Parameters.Add(new SqlParameter("@Policy_Type_Text", model.Policy_Type_Text));
                Command.Parameters.Add(new SqlParameter("@EnquiryBookingType", model.EnquiryBookingType));
                Command.Parameters.Add(new SqlParameter("@login_userid", userid));
                Command.Parameters.Add(new SqlParameter("@login_password", password));

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetEnqDel_ApiIds(string enquiryid, string database)
        {
            try
            {
                Command = new SqlCommand("sp_GetEnqDel_ApiIds", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@Enquiry_id", enquiryid));

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static bool UpdatePaymentDetailsEnquiryResponse(string payreq, string payrespo, string enqid, string database)
        {
            try
            {
                OpenConnection(GetConnectionString(database));
                Command = new SqlCommand("UpdatePaymentDetails_Enquiry_Response", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("paymentrequest", payreq);
                Command.Parameters.AddWithValue("paymentresponse", payrespo);
                Command.Parameters.AddWithValue("enquiryid", enqid);
                int isupdate = Command.ExecuteNonQuery();
                if (isupdate > 0)
                {
                    return true;
                }
                CloseConnection(GetConnectionString(database));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        public static object InsertAndUpdateMotoResponse(string enquiryid, string searchrequest, string response, string reqType, string database, string chainid = "", string proposalno = "", string suminsured = "", string applicationid = "", string productcode = "")
        {
            try
            {
                int isSuccess = 0; string type = string.Empty;
                type = !string.IsNullOrEmpty(response) ? "" : (!string.IsNullOrEmpty(chainid) ? "" : "get");
                if (!string.IsNullOrEmpty(reqType)) { type = reqType; }
                Command = new SqlCommand("sp_InsertAndUpdateMotoResponse", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("enquiry_id", enquiryid);
                Command.Parameters.AddWithValue("searchrequest", searchrequest);
                Command.Parameters.AddWithValue("response", response);
                Command.Parameters.AddWithValue("chainid", chainid);
                Command.Parameters.AddWithValue("proposalno", proposalno);
                Command.Parameters.AddWithValue("suminsured", suminsured);
                Command.Parameters.AddWithValue("proposalresponse", response);
                Command.Parameters.AddWithValue("productcode", productcode);
                Command.Parameters.AddWithValue("applicationid", (!string.IsNullOrEmpty(applicationid) ? applicationid : ""));
                Command.Parameters.AddWithValue("type", type);

                OpenConnection(GetConnectionString(database));
                if (type == "get")
                {
                    Adapter = new SqlDataAdapter();
                    Adapter.SelectCommand = Command;
                    ObjDataSet = new DataSet();

                    ObjDataTable = new DataTable();
                    Adapter.Fill(ObjDataSet, "CommonTable");
                    ObjDataTable = ObjDataSet.Tables["CommonTable"];
                }
                else
                {
                    isSuccess = Command.ExecuteNonQuery();
                }

                CloseConnection(GetConnectionString(database));

                if (type != "get")
                {
                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return ObjDataTable;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static object InsertAndUpdateHealthResponse(string enquiryid, string searchrequest, string response, string reqType, string database)
        {
            try
            {
                int isSuccess = 0; string type = string.Empty;
                Command = new SqlCommand("sp_InsertAndUpdateResponse", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("enquiry_id", enquiryid);
                Command.Parameters.AddWithValue("searchrequest", searchrequest);
                Command.Parameters.AddWithValue("response", response);
                Command.Parameters.AddWithValue("type", reqType);

                OpenConnection(GetConnectionString(database));
                if (type == "get")
                {
                    Adapter = new SqlDataAdapter();
                    Adapter.SelectCommand = Command;
                    ObjDataSet = new DataSet();

                    ObjDataTable = new DataTable();
                    Adapter.Fill(ObjDataSet, "CommonTable");
                    ObjDataTable = ObjDataSet.Tables["CommonTable"];
                }
                else
                {
                    isSuccess = Command.ExecuteNonQuery();
                }

                CloseConnection(GetConnectionString(database));

                if (type != "get")
                {
                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
                else
                {
                    return ObjDataTable;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        #region [New Get Connection]
        //public static DataTable GetRecordFromTable(Dictionary<string, string> iDictionary)
        //{
        //    string fileds = iDictionary.FirstOrDefault(x => x.Key == "column").Value;
        //    string tablename = iDictionary.FirstOrDefault(x => x.Key == "table").Value;
        //    string where = iDictionary.FirstOrDefault(x => x.Key == "where").Value;
        //    string database = iDictionary.FirstOrDefault(x => x.Key == "database").Value;
        //    return GetRecordFromTable(fileds, tablename, where, database);
        //}
        //public static bool UpdateRecordIntoAnyTable(Dictionary<string, string> iDictionary)
        //{
        //    string tableName = iDictionary.FirstOrDefault(x => x.Key == "table").Value;
        //    string fieldsWithValue = iDictionary.FirstOrDefault(x => x.Key == "fieldvalue").Value;
        //    string whereCondition = iDictionary.FirstOrDefault(x => x.Key == "where").Value;
        //    string database = iDictionary.FirstOrDefault(x => x.Key == "database").Value;
        //    return UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, database);
        //}
        //public static bool InsertRecordToTable(Dictionary<string, string> iDictionary)
        //{
        //    string fileds = iDictionary.FirstOrDefault(x => x.Key == "column").Value;
        //    string fieldValue = iDictionary.FirstOrDefault(x => x.Key == "fieldvalue").Value;
        //    string tableName = iDictionary.FirstOrDefault(x => x.Key == "table").Value;
        //    string database = iDictionary.FirstOrDefault(x => x.Key == "database").Value;
        //    return InsertRecordToTable(fileds, fieldValue, tableName, database);
        //}
        #endregion
    }
}