﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Models.Common.CommonModel;
using insurance.Helper.DataBase;

namespace insurance.Helper.CommonHelper
{
    public static class CommonHelp
    {
        public static List<Gender> GetGender(string genderId = null, string status = null)
        {
			List<Gender> genderList = new List<Gender>();

			try
			{
                string fileds = "*";
                string tablename = "T_GENDER";
                string whereCondition = "Status=" + (status == "true" ? "1" : "0");

                DataTable dtGender = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
                if (dtGender.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGender.Rows.Count; i++)
                    {
                        Gender gender = new Gender();
                        gender.GenderId = Convert.ToInt32(dtGender.Rows[i]["GENDERID"].ToString());
                        gender.LookupKey = dtGender.Rows[i]["LOOKUPKEY"].ToString();
                        gender.LookupDataCD = dtGender.Rows[i]["LOOKUPDATACD"].ToString();
                        gender.LookupDataValueKey = dtGender.Rows[i]["LOOKUPDATAVALUEKEY"].ToString();
                        gender.Status = dtGender.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;                      
                        genderList.Add(gender);
                    }
                }
            }
			catch (Exception ex)
			{
				ex.ToString();
				throw;
			}

			return genderList;

		}
    }
}