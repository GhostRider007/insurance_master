﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Models.Custom.FrontAgencyModel;
using ecommerce.Models.Common;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Helper.DataBase;
using insurance.Models;
using Post_Utility.Common;
using Post_Utility.Utility;

namespace insurance.Helper
{
    public static class FrontAgencyHelper
    {
        public static List<CreateAgency> Get_AgencyDetail(string agencyName = "", string agencyId = "")
        {
            List<CreateAgency> agencyList = new List<CreateAgency>();
            try
            {
                DataTable dtAgency = AutoCompleteHelper.GetAgentDetail_Like(agencyName, agencyId, "", "");
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgency.Rows.Count; i++)
                    {
                        CreateAgency tempItem = new CreateAgency();
                        tempItem.IntAgencyID = Convert.ToInt32(dtAgency.Rows[i]["AgencyId"].ToString());
                        tempItem.AgencyID = dtAgency.Rows[i]["AgencyId"].ToString();
                        tempItem.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyName"].ToString()) ? dtAgency.Rows[i]["AgencyName"].ToString() : string.Empty;
                        tempItem.Address = !string.IsNullOrEmpty(dtAgency.Rows[i]["Address"].ToString()) ? dtAgency.Rows[i]["Address"].ToString() : string.Empty;

                        tempItem.CountryID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CountryID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["CountryID"].ToString()) : 0;
                        tempItem.CountryName = CountryList(tempItem.CountryID.ToString(), "1").FirstOrDefault().CountryName;

                        tempItem.StateID = !string.IsNullOrEmpty(dtAgency.Rows[i]["StateID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["StateID"].ToString()) : 0;
                        var tempState = StateList(tempItem.StateID.ToString(), tempItem.CountryID.ToString(), "1");
                        tempItem.StateName = tempState.Count() > 0 ? tempState.FirstOrDefault().StateName : string.Empty;

                        tempItem.CityID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CityID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["CityID"].ToString()) : 0;
                        tempItem.Pincode = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pincode"].ToString()) ? dtAgency.Rows[i]["Pincode"].ToString() : string.Empty;
                        var tempCity = CityList(tempItem.Pincode, null, null, null);
                        tempItem.CityName = tempCity.Count() > 0 ? tempCity.FirstOrDefault().CityName : string.Empty;
                        tempItem.GroupType = !string.IsNullOrEmpty(dtAgency.Rows[i]["GroupType"].ToString()) ? dtAgency.Rows[i]["GroupType"].ToString() : string.Empty;
                        tempItem.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["GSTNo"].ToString()) ? dtAgency.Rows[i]["GSTNo"].ToString() : string.Empty;
                        tempItem.BusinessEmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessEmailID"].ToString()) ? dtAgency.Rows[i]["BusinessEmailID"].ToString() : string.Empty;
                        tempItem.BusinessPhone = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPhone"].ToString()) ? dtAgency.Rows[i]["BusinessPhone"].ToString() : string.Empty;

                        tempItem.ReferenceBy = !string.IsNullOrEmpty(dtAgency.Rows[i]["ReferenceBy"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["ReferenceBy"].ToString()) : 0;
                        tempItem.BusinessAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                        tempItem.BusinessAddressProofImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                        tempItem.BusinessPanCardImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                        tempItem.BusinessPanCardImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                        tempItem.Title = !string.IsNullOrEmpty(dtAgency.Rows[i]["Title"].ToString()) ? dtAgency.Rows[i]["Title"].ToString() : string.Empty;

                        tempItem.FirstName = !string.IsNullOrEmpty(dtAgency.Rows[i]["FirstName"].ToString()) ? dtAgency.Rows[i]["FirstName"].ToString() : string.Empty;
                        tempItem.LastName = !string.IsNullOrEmpty(dtAgency.Rows[i]["LastName"].ToString()) ? dtAgency.Rows[i]["LastName"].ToString() : string.Empty;
                        tempItem.EmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["EmailId"].ToString()) ? dtAgency.Rows[i]["EmailId"].ToString() : string.Empty;
                        tempItem.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[i]["Mobile"].ToString()) ? dtAgency.Rows[i]["Mobile"].ToString() : string.Empty;

                        tempItem.PersonalAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                        tempItem.PersonalAddressProofImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                        tempItem.UserId = !string.IsNullOrEmpty(dtAgency.Rows[i]["UserName"].ToString()) ? dtAgency.Rows[i]["UserName"].ToString() : string.Empty;
                        tempItem.Password = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? dtAgency.Rows[i]["Password"].ToString() : string.Empty;
                        tempItem.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pan_No"].ToString()) ? dtAgency.Rows[i]["Pan_No"].ToString() : string.Empty;
                        tempItem.CompanyLogoImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                        tempItem.Branch = !string.IsNullOrEmpty(dtAgency.Rows[i]["BranchID"].ToString()) ? dtAgency.Rows[i]["BranchID"].ToString() : string.Empty;
                        tempItem.CompanyLogoImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                        tempItem.Status = dtAgency.Rows[i]["IsApproved"].ToString().ToLower() == "true" ? true : false;
                        //tempItem.CreatedDate = !string.IsNullOrEmpty(dtAgency.Rows[i]["Createdate"].ToString()) ? Convert.ToDateTime(dtAgency.Rows[i]["Createdate"].ToString()) : string.Empty;
                        //Convert.ToDateTime(dtAgency.Rows[i]["Createdate"].ToString());
                        //tempItem.UpdatedDate = Convert.ToDateTime(dtAgency.Rows[i]["UpdatedDate"].ToString());

                        agencyList.Add(tempItem);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return agencyList;
        }
        public static List<CreateAgency> GetAgencyList(string agencyId = null, string status = "1")
        {
            List<CreateAgency> result = new List<CreateAgency>();

            try
            {
                string fileds = "*";
                string tablename = "aspnet_user";
                string whereCondition = string.Empty;
                if (status == "(0,1)")
                {
                    whereCondition = "IsApproved in " + status + "";
                }
                else if (status == "0")
                {
                    whereCondition = "IsApproved = " + status + "";
                }
                else
                {
                    whereCondition = "IsApproved=" + status;
                }
                if (!string.IsNullOrEmpty(agencyId))
                {
                    whereCondition += " and AgencyID=" + agencyId;
                }

                whereCondition += " Order by AgencyID desc";

                DataTable dtAgency = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");

                if (dtAgency.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgency.Rows.Count; i++)
                    {
                        CreateAgency tempItem = new CreateAgency();
                        tempItem.IntAgencyID = Convert.ToInt32(dtAgency.Rows[i]["AgencyId"].ToString());
                        tempItem.AgencyID = dtAgency.Rows[i]["AgencyId"].ToString();
                        tempItem.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyName"].ToString()) ? dtAgency.Rows[i]["AgencyName"].ToString() : string.Empty;
                        tempItem.Address = !string.IsNullOrEmpty(dtAgency.Rows[i]["Address"].ToString()) ? dtAgency.Rows[i]["Address"].ToString() : string.Empty;

                        tempItem.CountryID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CountryID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["CountryID"].ToString()) : 0;
                        tempItem.CountryName = tempItem.CountryID > 0 ? CountryList(tempItem.CountryID.ToString(), "1").FirstOrDefault().CountryName : string.Empty;

                        tempItem.StateID = !string.IsNullOrEmpty(dtAgency.Rows[i]["StateID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["StateID"].ToString()) : 0;
                        var tempState = tempItem.StateID > 0 ? StateList(tempItem.StateID.ToString(), tempItem.CountryID.ToString(), "1") : null;
                        tempItem.StateName = tempState != null && tempState.Count() > 0 ? tempState.FirstOrDefault().StateName : string.Empty;

                        tempItem.CityID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CityID"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["CityID"].ToString()) : 0;
                        tempItem.Pincode = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pincode"].ToString()) ? dtAgency.Rows[i]["Pincode"].ToString() : string.Empty;
                        var tempCity = CityList(tempItem.Pincode, null, null, null);
                        tempItem.CityName = tempCity.Count() > 0 ? tempCity.FirstOrDefault().CityName : string.Empty;
                        tempItem.GroupType = !string.IsNullOrEmpty(dtAgency.Rows[i]["GroupType"].ToString()) ? dtAgency.Rows[i]["GroupType"].ToString() : string.Empty;
                        tempItem.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["GSTNo"].ToString()) ? dtAgency.Rows[i]["GSTNo"].ToString() : string.Empty;
                        tempItem.BusinessEmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessEmailID"].ToString()) ? dtAgency.Rows[i]["BusinessEmailID"].ToString() : string.Empty;
                        tempItem.BusinessPhone = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPhone"].ToString()) ? dtAgency.Rows[i]["BusinessPhone"].ToString() : string.Empty;

                        tempItem.ReferenceBy = !string.IsNullOrEmpty(dtAgency.Rows[i]["ReferenceBy"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["ReferenceBy"].ToString()) : 0;
                        tempItem.BusinessAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                        tempItem.BusinessAddressProofImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                        tempItem.BusinessPanCardImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                        tempItem.BusinessPanCardImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                        tempItem.Title = !string.IsNullOrEmpty(dtAgency.Rows[i]["Title"].ToString()) ? dtAgency.Rows[i]["Title"].ToString() : string.Empty;

                        tempItem.FirstName = !string.IsNullOrEmpty(dtAgency.Rows[i]["FirstName"].ToString()) ? dtAgency.Rows[i]["FirstName"].ToString() : string.Empty;
                        tempItem.LastName = !string.IsNullOrEmpty(dtAgency.Rows[i]["LastName"].ToString()) ? dtAgency.Rows[i]["LastName"].ToString() : string.Empty;
                        tempItem.EmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["EmailId"].ToString()) ? dtAgency.Rows[i]["EmailId"].ToString() : string.Empty;
                        tempItem.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[i]["Mobile"].ToString()) ? dtAgency.Rows[i]["Mobile"].ToString() : string.Empty;

                        tempItem.PersonalAddressProofImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                        tempItem.PersonalAddressProofImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                        tempItem.UserId = !string.IsNullOrEmpty(dtAgency.Rows[i]["UserName"].ToString()) ? dtAgency.Rows[i]["UserName"].ToString() : string.Empty;
                        tempItem.Password = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? Security.ApiBase64Decode(dtAgency.Rows[i]["Password"].ToString()) : string.Empty;
                        tempItem.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pan_No"].ToString()) ? dtAgency.Rows[i]["Pan_No"].ToString() : string.Empty;
                        tempItem.CompanyLogoImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                        tempItem.Branch = !string.IsNullOrEmpty(dtAgency.Rows[i]["BranchID"].ToString()) ? dtAgency.Rows[i]["BranchID"].ToString() : string.Empty;
                        tempItem.CompanyLogoImgShow = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                        tempItem.Status = dtAgency.Rows[i]["IsApproved"].ToString().ToLower() == "true" ? true : false;

                        tempItem.AgencyLastLoginDate = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyLastLoginDate"].ToString())? dtAgency.Rows[i]["AgencyLastLoginDate"].ToString():"N/A";
                        tempItem.LastLoginDate = !string.IsNullOrEmpty(dtAgency.Rows[i]["LastLoginDate"].ToString())? dtAgency.Rows[i]["LastLoginDate"].ToString() : "N/A";
                        //tempItem.CreatedDate = !string.IsNullOrEmpty(dtAgency.Rows[i]["Createdate"].ToString()) ? Convert.ToDateTime(dtAgency.Rows[i]["Createdate"].ToString()) : string.Empty;
                        //Convert.ToDateTime(dtAgency.Rows[i]["Createdate"].ToString());
                        //tempItem.UpdatedDate = Convert.ToDateTime(dtAgency.Rows[i]["UpdatedDate"].ToString());

                        result.Add(tempItem);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static int InserNewAgency(CreateAgency model)
        {
            try
            {
                //string UserID = "CONVERT(uniqueidentifier,'" + Guid.NewGuid().ToString()+"')";               
                //string fileds = "Createdate,LastLoginDate,IsApproved,IsLockOut,FailedPasswordAttemptCount,AgencyName,Address,CountryID,StateID,CityID,Pincode,GSTNo,BusinessEmailID,BusinessPhone,ReferenceBy,BusinessAddressProofImg,BusinessPanCardImg,Title,FirstName,LastName,EmailId,Mobile,PersonalAddressProofImg,UserName,CompanyLogoImg,Pan_No,GroupType,Password,UserId";
                //string fieldValue = "getdate(),getdate(),0,0,0,'" + model.AgencyName + "','" + model.Address + "'," + model.CountryID + "," + model.StateID + "," + model.CityID + ",'" + model.Pincode + "','" + model.GSTNo + "','" + model.BusinessEmailID + "','" + model.BusinessPhone + "'," + model.ReferenceBy + ",'" + model.BusinessAddressProofImg + "','" + model.BusinessPanCardImg + "','" + model.Title + "','" + model.FirstName + "','" + model.LastName + "','" + model.EmailID + "','" + model.Mobile + "','" + model.PersonalAddressProofImg + "','" + model.RegUserId + "','" + model.CompanyLogoImg + "','" + model.PanNo + "','" + model.GroupType + "','" + Security.ApiBase64Encode(model.RegPassword) + "'," + UserID.ToUpper() + "";
                //string tableName = "aspnet_user";
                model.UserName = Guid.NewGuid().ToString();
                model.AgencyCode = UtilityClass.GenrateRandomTransactionId("I", 4, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
                model.AgencyID = UtilityClass.GenrateRandomTransactionId("", 3, "1234567890");
                model.Password = Security.ApiBase64Encode(model.RegPassword);

                return ConnectToDataBase.InsertAgencyDetailReturnID(model, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool ActiveAgentDetails(int? agencyid, CreateAgency model)
        {
            try
            {
                string tempStatus = string.Empty;
                if (model.Status)
                {
                    tempStatus = "1";
                }
                else
                {
                    tempStatus = "0";
                }
                string tableName = "aspnet_user";
                string fieldsWithValue = "BranchID='" + model.Branch + "',ReferenceBy=" + model.ReferenceBy + ",GroupType='" + model.GroupType + "', UpdatedDate=getdate()";
                string whereCondition = "AgencyId=" + agencyid;
                bool IsSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api");
                if (IsSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateAgencyDetail(int? agencyid, CreateAgency model)
        {
            try
            {
                string tableName = "aspnet_user";
                string fieldsWithValue = "AgencyName='" + model.AgencyName + "',Address='" + model.Address + "',CountryID=" + model.CountryID + ",StateID=" + model.StateID + ",CityID=" + model.CityID + ",Pincode='" + model.Pincode + "',GSTNo='" + model.GSTNo + "',BusinessEmailID='" + model.BusinessEmailID + "',BusinessPhone='" + model.BusinessPhone + "',ReferenceBy=" + model.ReferenceBy + ",BusinessAddressProofImg='" + model.BusinessAddressProofImg + "',BusinessPanCardImg='" + model.BusinessPanCardImg + "',Title='" + model.Title + "',FirstName='" + model.FirstName + "',LastName='" + model.LastName + "',EmailId='" + model.EmailID + "',Pan_No='" + model.PanNo + "',Mobile='" + model.Mobile + "',PersonalAddressProofImg='" + model.PersonalAddressProofImg + "',CompanyLogoImg='" + model.CompanyLogoImg + "',UpdatedDate=getdate()";
                string whereCondition = "AgencyId=" + agencyid;
                bool IsSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api");
                if (IsSuccess)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static List<BankNameModel> BankList()
        {
            List<BankNameModel> BankList = new List<BankNameModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_bankName";
                string whereCondition = string.Empty;
                
                DataTable dtbank = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtbank.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbank.Rows.Count; i++)
                    {
                        BankNameModel model = new BankNameModel();                      
                        model.BankName = dtbank.Rows[i]["Bank_Name"].ToString();
                        model.id = Convert.ToInt32(dtbank.Rows[i]["id"].ToString());
                        BankList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return BankList;
        }
        public static List<Country> CountryList(string countryid = null, string status = null)
        {
            List<Country> countryList = new List<Country>();

            try
            {
                string fileds = "*";
                string tablename = "tbl_Country";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(countryid))
                {
                    whereCondition += "CountryID=" + countryid;
                }

                DataTable dtCountry = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");

                if (dtCountry.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCountry.Rows.Count; i++)
                    {
                        Country model = new Country();
                        model.CountryID = Convert.ToInt32(dtCountry.Rows[i]["CountryId"].ToString());
                        model.CountryName = dtCountry.Rows[i]["Country"].ToString();
                        //model.Status = dtCountry.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        countryList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return countryList;
        }
        public static List<State> StateList(string stateId = null, string countryID = null, string status = null)
        {
            List<State> stateList = new List<State>();

            try
            {
                string fileds = "*";
                string tablename = "tbl_State";
                string whereCondition = "CountryId=" + countryID;

                if (!string.IsNullOrEmpty(stateId))
                {
                    whereCondition += " and StateId=" + stateId;
                }

                DataTable dtState = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");

                if (dtState.Rows.Count > 0)
                {
                    for (int i = 0; i < dtState.Rows.Count; i++)
                    {
                        State model = new State();
                        model.StateID = Convert.ToInt32(dtState.Rows[i]["StateId"].ToString());
                        model.StateName = dtState.Rows[i]["State"].ToString();
                        model.CountryID = Convert.ToInt32(dtState.Rows[i]["CountryId"].ToString());
                        //model.Status = dtState.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        stateList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return stateList;
        }
        public static List<City> CityList(string cityPin = null, string cityId = null, string stateID = null, string status = null, string stateName = null)
        {
            List<City> cityList = new List<City>();

            try
            {
                string fileds = "*";
                string tablename = "T_City";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(cityId))
                {
                    whereCondition += " ID=" + cityId;
                }

                if (!string.IsNullOrEmpty(cityPin))
                {
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        whereCondition += " and ";
                    }
                    whereCondition += "PINCODE='" + cityPin + "'";
                }

                if (!string.IsNullOrEmpty(stateName))
                {
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        whereCondition += " and ";
                    }
                    whereCondition += " STATECD LIKE '%" + stateName + "%'";
                }

                DataTable dtCity = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
                if (dtCity.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCity.Rows.Count; i++)
                    {
                        City model = new City();
                        model.CityID = Convert.ToInt32(dtCity.Rows[i]["ID"].ToString());
                        model.CityName = dtCity.Rows[i]["CITYCD"].ToString();
                        model.StateName = dtCity.Rows[i]["STATECD"].ToString();
                        //model.Status = dtCity.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        cityList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return cityList;
        }
        public static string CityListWithPin(string cityPin = null)
        {
            string CityName = "";
            try
            {
                string fileds = "top 1  *";
                string tablename = "tblMaster_PinCode";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(cityPin))
                {
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        whereCondition += " and ";
                    }
                    whereCondition += "pincode='" + cityPin + "'";
                }

                DataTable dtCity = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
                if (dtCity.Rows.Count > 0)
                {


                    CityName = dtCity.Rows[0]["City"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return CityName;
        }
        public static List<StaffModel> RefByList(string roleID = null, string status = null, string staffId = null)
        {
            List<StaffModel> staffList = new List<StaffModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_MemberLogin";// "tblStaff";
                string whereCondition = "Status=" + status + "";

                if (!string.IsNullOrEmpty(roleID))
                {
                    whereCondition += " and RoleId=" + roleID;
                }

                if (!string.IsNullOrEmpty(staffId))
                {
                    whereCondition += " and LoginId=" + staffId;
                }

                DataTable dtStaff = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
                if (dtStaff.Rows.Count > 0)
                {
                    for (int i = 0; i < dtStaff.Rows.Count; i++)
                    {
                        StaffModel model = new StaffModel();
                        model.StaffID = Convert.ToInt32(dtStaff.Rows[i]["LoginId"].ToString());
                        model.StaffName = dtStaff.Rows[i]["FirstName"].ToString() + " " + dtStaff.Rows[i]["LastName"].ToString();
                        model.EmailID = dtStaff.Rows[i]["EmailId"].ToString();
                        model.Mobile = dtStaff.Rows[i]["MobileNo"].ToString();
                        model.Status = dtStaff.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        staffList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return staffList;
        }
        public static bool UpdateImagesAgency(int agencyID, CreateAgency model)
        {
            try
            {
                string tableName = "aspnet_user";
                string fieldsWithValue = "BusinessAddressProofImg='" + model.BusinessAddressProofImg + "',BusinessPanCardImg='" + model.BusinessPanCardImg + "',PersonalAddressProofImg='" + model.PersonalAddressProofImg + "',CompanyLogoImg='" + model.CompanyLogoImg + "'";
                string whereCondition = "AgencyID=" + agencyID;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool AgencyStatusUpdator(int id, bool status)
        {
            try
            {
                if (id > 0)
                {
                    string fieldswithvalue = "IsApproved = '" + status.ToString().ToLower() + "'";
                    string tablename = "aspnet_user";
                    string wherecondition = "AgencyID = " + id + " ";

                    return ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition, "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static EmulateLogin Insert_EmulateloginDetails(EmulateLogin model)
        {
            string tableName = "T_EmulateLogin";

            if (model.actionType == "insert")
            {
                string fileds = "Agencyid,AgencyName,MemberId,StaffName,Remarks,LoginUserId,OTP,OTPExpTime";
                string fieldValue = "'" + model.Agencyid + "','" + model.AgencyName + "','" + model.staffid + "','" + model.StaffName + "','" + model.Remarks.Replace("'", "''") + "','" + model.UserId + "','" + model.OTP + "','" + model.OTPExpTime + "'";
                int last_val = ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName, "seeinsured");
                model.EmulateId = last_val.ToString();
            }
            else
            {
                string fieldsWithValue = "OTP='" + model.OTP + "',OTPExpTime='" + model.OTPExpTime + "'";
                string whereCondition = "EmulateId=" + model.EmulateId;
                bool isupdated = ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api");
            }
            return model;
        }
        public static EmulateLogin Get_EmulateDetail(EmulateLogin model)
        {
            DataTable dtEmulate = ConnectToDataBase.GetRecordFromTable("*", "T_EmulateLogin", "EmulateId=" + model.EmulateId, "seeinsured");
            if (dtEmulate != null && dtEmulate.Rows.Count > 0)
            {
                model.OTP = dtEmulate.Rows[0]["OTP"].ToString();
                model.OTPExpTime = dtEmulate.Rows[0]["OTPExpTime"].ToString();
            }
            return model;
        }
    }
}