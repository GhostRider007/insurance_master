﻿
function AgencyLogoutProcess() {
    var agencyid = $("#btnAgencyLogout").data("agencyid");

    $("#btnAgencyLogout").html("<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Administration/AgencyLogout",
        data: '{agencyid: ' + JSON.stringify(agencyid) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    window.location.href = "/";
                }
            }
        },
        failure: function (data) {
            alert("failed");
        }
    });
}