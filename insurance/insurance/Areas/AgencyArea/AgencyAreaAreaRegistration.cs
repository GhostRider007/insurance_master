﻿using System.Web.Mvc;

namespace insurance.Areas.AgencyArea
{
    public class AgencyAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AgencyArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //context.MapRoute(name: "AgencyArea_Dashboard", url: "agency/dashboard", defaults: new { controller = "AgencyArea_Dashboard", action = "AgencyDashboard" });

            context.MapRoute("AgencyArea_default", "AgencyArea/{controller}/{action}/{id}", new { action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "insurance.Areas.AgencyArea.Controllers" });
        }
    }
}