﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Areas.AdminArea.Models.AdminArea_Commission
{
    public class Commission
    {
        public int id { get; set; }
        public int AgencyId { get; set; }
        public string AgencyName { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string InsuranceType { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CommissionType { get; set; }
        public decimal BaseCommssion { get; set; }
        public decimal TDS { get; set; }
        public bool Status { get; set; }
    }

    public class IncentiveCommission
    {
        public int id { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public List<SelectListItem> GroupList { get; set; }
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public List<SelectListItem> AgencyList { get; set; }
        public string InsurerId { get; set; }
        public string InsurerName { get; set; }
        public List<SelectListItem> InsurerList { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public List<SelectListItem> ProductList { get; set; }
        public string IRDAI { get; set; }
        public string AdditinalSI_1 { get; set; }
        public string AdditinalSI_1Val { get; set; }
        public string AdditinalSI_2 { get; set; }
        public string AdditinalSI_2Val { get; set; }
        public string AdditinalSI_3 { get; set; }
        public string AdditinalSI_3Val { get; set; }
        public string AdditinalSI_4 { get; set; }
        public string AdditinalSI_4Val { get; set; }
        public string AdditinalSI_5 { get; set; }
        public string AdditinalSI_5Val { get; set; }
        public string AdditinalSI_6 { get; set; }
        public string AdditinalSI_6Val { get; set; }
        public string AdditinalSI_7 { get; set; }
        public string AdditinalSI_7Val { get; set; }
        public string AdditinalSI_8 { get; set; }
        public string AdditinalSI_8Val { get; set; }
        public string AdditinalSI_9 { get; set; }
        public string AdditinalSI_9Val { get; set; }
        public string AdditinalSI_10 { get; set; }
        public string AdditinalSI_10Val { get; set; }
        public string AdditinalSI_11 { get; set; }
        public string AdditinalSI_11Val { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<IncentiveCommission> IncentiveList { get; set; }
    }

    public class CommissionReport
    {
        public string AgencyName { get; set; }
        public string Insurer { get; set; }
        public string ProductName { get; set; }
        public string TotalSale { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string IncentivePercentage { get; set; }
        public string IncAmount { get; set; }
    }
}