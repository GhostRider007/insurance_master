﻿using ecommerce.Models.Common;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.Models
{
    public static class Email
    {
        public static bool SendProposal(TrackEnquirydetail model)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("Proposal.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#ProposarName#", model.First_Name + model.Last_Name);
            message.Replace("#RegisteredPOSName#", model.AgencyName);
            message.Replace("#ProposalNo#", model.ProposalNo);
            message.Replace("#CreatedDate#", model.DateOfReg);
            message.Replace("#InquaryDate#", model.DateOfReg);
            message.Replace("#VehicleRegistration#", model.VechicleRegNo);
            message.Replace("#InsuranceType#", model.insurancetype);
            message.Replace("#RTO#", model.RtoName);
            message.Replace("#DateofRegistraion#", model.DateOfReg);
            message.Replace("#Brand#", model.BrandName);
            message.Replace("#Model#", model.ModelName);
            message.Replace("#Fuel#", model.Fuel);
            message.Replace("#Variant#", model.insurancetype);
            message.Replace("#IDV#", "---");
            message.Replace("#NCB#", "---");
            message.Replace("#NCBAmt#", "---");
            message.Replace("#AddonDetails#", "---");
            message.Replace("#OwnerName#", model.First_Name + model.Last_Name);
            message.Replace("#DOB#", model.dob);
            message.Replace("#Mobile#", model.Mobile_No);

            message.Replace("#Email#", model.Email_Id);
            message.Replace("#Address#", model.Address);
            message.Replace("#NomineeName#", model.ntitle + model.nfirstname + model.nlastname);
            message.Replace("#NomineeDOB#", model.ndob);
            message.Replace("#RelationshipwithNominee#", model.nrelation);
            message.Replace("#PolicyHolder#", model.policyholdertype);
            message.Replace("#ManufacturingYear#", model.DateOfMfg);
            message.Replace("#PreviousInsurer#", model.previousinsured);
            message.Replace("#PreviousPolicyNumber#", model.policynumber);
            message.Replace("#PreviousPolicyExpireDate#", model.policyexpirydate);
            message.Replace("#LastYaerClaim#", model.isclaiminlastyear);
            message.Replace("#PreviousyearNCB#", model.previousinsured);
            message.Replace("#EngineNumber#", model.engineno);
            message.Replace("#ChassisNumber#", model.chasisno);
            message.Replace("#Financedby#", "---");
            message.Replace("#PaymentUrl#", "" + Config.WebsiteUrl + "/moter/proposalsummary?enquiry_id='" + model.Enquiry_Id + "'");
            string subject = "Motor Insurence : Proposal Refrence No- " + model.ProposalNo;
            string ccEmail = string.Empty;
            ccEmail = model.AgencyBusinessEmailID;
            if (Email.SendEmail(Config.AdminEmail, model.Email_Id, subject, message.ToString(), ccEmail))
            {
                return true;
            }
            return false;
        }

        public static bool SendHealthProposal(TrackHealthEnquirydetail model)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("HealthProposal.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#AgencyName#", model.AgencyName);
            message.Replace("#Address#", model.Address);
            message.Replace("#City#", model.CityName);
            message.Replace("#Pin#", model.Pincode);
            message.Replace("#Phone#", model.BusinessPhone);
            message.Replace("#EmailAgency#", model.AgencyEmail);
            message.Replace("#Pin#", model.Pincode);
            message.Replace("#CreatedDate#", model.CreatedDate);
            message.Replace("#ProposarName#", model.firstname + model.lastname);
            message.Replace("#RegisteredPOSName#", model.AgencyName);
            message.Replace("#ProposalNo#", model.p_proposalNum);
            message.Replace("#CreatedDate#", model.CreatedDate);
            message.Replace("#OrderID#", model.EnquiryId);
            message.Replace("#Validtill#", model.pay_paymentdate);
            message.Replace("#Name#", model.firstname + model.lastname);
            message.Replace("#Gender#", model.gender);
            message.Replace("#Title#", model.title);
            message.Replace("#DOB#", model.dob);
            message.Replace("#Mobile#", model.mobile);
            message.Replace("#Email#", model.emailid);
            message.Replace("#AnnualIncome#", model.annualincome);
            message.Replace("#Address#", model.address1);
            message.Replace("#City#", model.cityname);
            message.Replace("#State#", model.statename);
            message.Replace("#PinCode#", model.pincode);

            DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.EmailHealth_InsuredDetails(model.EnquiryId);
            List<string> result = new List<string>();
            string Template = string.Empty;
            if (dtEnquiryDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                {
                            Template = "<tr style='border-collapse: initial; border: 1px solid rgb(204, 204, 224); font - size: 14px; border - spacing: 0px; padding: 0px; '>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["relationname"].ToString() + "</td>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["title"].ToString() + "</td>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["firstname"].ToString() + " " + dtEnquiryDetail.Rows[i]["lastname"].ToString() + "</td>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["dob"].ToString() + "</td>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["height"].ToString() + "</td>"
                                  + "<td style='text-align:center; border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;' colspan='1' >" + dtEnquiryDetail.Rows[i]["weight"].ToString() + "</td>"
                                  + "</tr>";
                    result.Add(Template);
                }
            }
            message.Replace("#T_Health_Insured#", Template);
            message.Replace("#NomineeName#", model.ntitle + model.nfirstname + model.nlastname);
            message.Replace("#DateOfBirth#", model.ndob);
            message.Replace("#Relationship#", model.nrelations);
            message.Replace("#InsurerName#", model.CompanyName);
            message.Replace("#ProductName#", model.ProductName);
            message.Replace("#Period#", model.periodfor);
            message.Replace("#PolicyType#", model.Policy_Type);
            message.Replace("#SumInsure#", model.suminsured);
            message.Replace("#Permimum#", model.p_totalPremium);
            message.Replace("#PaymentUrl#", model.p_rowpaymenturl);
            string subject = "Health Insurence : Proposal Refrence No- " + model.p_proposalNum;
            string ccEmail = string.Empty;
            ccEmail = model.AgencyEmail;
            if (Email.SendEmail(Config.AdminEmail, model.emailid, subject, message.ToString(), ccEmail))
            {
                return true;
            }
            return false;
        }

        public static bool SendCompareDetail(string Comparebody, string agencyemail, string agencyname)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("CompareEmail.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#CompareProduct#", Comparebody);
            message.Replace("#AgencyName#", agencyname);
            string subject = "Compare Insurance detail from " + Config.WebsiteName;
            string ccEmail = string.Empty;
            ccEmail = "Rajnish@seeingo.com";
            if (Email.SendEmail(Config.AdminEmail, agencyemail, subject, message.ToString(), ccEmail))
            {
                return true;
            }
            return false;
        }
        public static bool SendAgencyRegistrationDetails(CreateAgency model, string refStaffEmail = null)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("AgencyRegistration.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#AgencyName#", model.AgencyName);
            message.Replace("#Address#", model.Address);
            message.Replace("#BusinessEmailID#", model.BusinessEmailID);
            message.Replace("#UserID#", model.RegUserId);
            message.Replace("#BusinessPhone#", model.BusinessPhone);
            message.Replace("#Mobile#", model.Mobile);
            message.Replace("#ReferenceBy#", model.RefreceByName);
            string subject = "Agency Registration acknowledgement email from " + Config.WebsiteName;

            string ccEmail = string.Empty;
            if (!string.IsNullOrEmpty(refStaffEmail))
            {
                ccEmail = refStaffEmail + ",Rajnish@seeingo.com";
            }
            else
            {
                ccEmail = "Rajnish@seeingo.com";
            }

            if (Email.SendEmail(Config.AdminEmail, model.BusinessEmailID, subject, message.ToString(), ccEmail))
            {
                return true;
            }
            return false;
        }
        public static bool SendAgencyRegconfirmDetails(string UserID, string EmailID, string AgencyName, string AgencyPassword)
        {

            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("AgencyRegConfirmation.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#AgencyName#", AgencyName);
            message.Replace("#UserID#", UserID);
            message.Replace("#Password#", AgencyPassword);
            string subject = "Your " + Config.WebsiteName + "  Account is Now Activated ";
            if (Email.SendEmail(Config.AdminEmail, EmailID, subject, message.ToString()))
            {
                return true;
            }
            return false;

        }
        public static bool SendMemberDetails(MemberLogin memberl)
        {
            StringBuilder message = new StringBuilder();
            message.Append(Utility.ReadHTMLTemplate("StaffCredentials.html"));
            message.Replace("#Logo#", Config.GetLogoLink);
            message.Replace("#Url#", Config.WebsiteUrl);
            message.Replace("#StaffName#", memberl.FirstName);
            message.Replace("#UserID#", memberl.UserId);
            message.Replace("#Password#", memberl.Password);
            message.Replace("#StaffRole#", memberl.RoleName);
            string subject = "Executive login credentials for " + Config.WebsiteName;
            if (Email.SendEmail(Config.AdminEmail, memberl.EmailId, subject, message.ToString()))
            {
                return true;
            }
            return false;
        }
        private static bool SendEmail(string emailFrom, string emailTo, string subject, string body, string cc = "Rajnish@seeingo.com")
        {
            bool issent = false;
            try
            {
                using (MailMessage message = new MailMessage(emailFrom, emailTo, subject, body))
                {
                    if (cc != null) { message.CC.Add(cc); }
                    message.ReplyToList.Add(new MailAddress(emailFrom));
                    message.IsBodyHtml = true;
                    using (SmtpClient emailClient = new SmtpClient("smtp.gmail.com"))
                    {
                        emailClient.Port = 587;
                        emailClient.UseDefaultCredentials = false;
                        emailClient.EnableSsl = true;
                        emailClient.Credentials = new System.Net.NetworkCredential("support@seeinsured.com", "Insured@see");
                        emailClient.Send(message);
                    }
                }

                issent = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issent;
        }
    }
}