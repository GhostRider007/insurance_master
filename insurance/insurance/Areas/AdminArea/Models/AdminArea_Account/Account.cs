﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Areas.AdminArea.Models.AdminArea_Account
{
    public class Account
    {
        public class BackEndLogin
        {
            public string UserId { get; set; }
            public string Password { get; set; }
            public bool Remember { get; set; }
        }
        public class Branch
        {
            public int Id { get; set; }
            public string BranchName { get; set; }
            public string Branchid { get; set; }

        }
        public class MemberLogin
        {
            public int LoginId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailId { get; set; }
            public string MobileNo { get; set; }
            public string UserId { get; set; }            
            public string Password { get; set; }
            public string MemberType { get; set; }
            public string Image { get; set; }
            public int RoleId { get; set; }
            public string RoleName { get; set; }
            public bool Status { get; set; }
            public string CreatedDate { get; set; }
            public string CurrentPassword { get; set; }
            public string ConfPassword { get; set; }
            public List<SelectListItem> RoleList { get; set; }
            public List<MemberLogin> MemberList { get; set; }
        }

    }
}