﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Motor
{
    public class NCBPercentage
    {
        public int ID { get; set; }
        public string ncbpercentage { get; set; }
        public string ncbgodigit { get; set; }
        public string ncbshriram { get; set; }

    }
}
