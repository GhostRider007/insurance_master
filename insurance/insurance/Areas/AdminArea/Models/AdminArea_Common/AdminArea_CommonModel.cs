﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;

namespace insurance.Areas.AdminArea.Models.AdminArea_Common
{
    public class AdminArea_CommonModel
    {
        public static void InitializeUserSession(DataTable item, bool isWelcome = false)
        {
            MemberLogin olu = new MemberLogin();
            olu.LoginId = Convert.ToInt32(item.Rows[0]["LoginId"].ToString());
            olu.FirstName = item.Rows[0]["FirstName"].ToString();
            olu.LastName = item.Rows[0]["LastName"].ToString();
            olu.EmailId = item.Rows[0]["EmailId"].ToString();
            olu.MobileNo = item.Rows[0]["MobileNo"].ToString();
            olu.UserId = item.Rows[0]["UserId"].ToString();
            olu.Password = item.Rows[0]["Password"].ToString();
            olu.MemberType = item.Rows[0]["MemberType"].ToString();
            olu.Image = item.Rows[0]["Image"].ToString();
            olu.RoleId = Convert.ToInt32(item.Rows[0]["RoleId"].ToString());
            olu.Status = item.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;

            List<MemberLogin> lu = new List<MemberLogin>();
            lu.Add(olu);

            HttpContext.Current.Session["backendloginsession"] = lu;

            //if (olu.MemberType.ToLower().Trim() == "admin")
            //{
            //    HttpContext.Current.Session["backendlogincookie"] = lu;
            //}
            //else
            //{
            //    HttpContext.Current.Session["staff"] = lu;
            //}
        }
    }
}