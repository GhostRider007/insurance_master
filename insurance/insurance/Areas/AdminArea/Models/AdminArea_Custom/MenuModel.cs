﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class MenuModel
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string MenuUrl { get; set; }
        public string Icon { get; set; }
        public string Position { get; set; }
        public bool Status { get; set; }
        public string ActiveClass { get; set; }
        public string CreatedDate { get; set; }
        public List<MenuModel> MenuList { get; set; }
    }

    public class SubMenuModel
    {
        public int SubMenuId { get; set; }
        public int MenuId { get; set; }
        public string SubMenuName { get; set; }
        public string SubMenuUrl { get; set; }
        public string Icon { get; set; }
        public string Position { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public List<SelectListItem> MenuList { get; set; }
        public List<SubMenuModel> SubMenuList { get; set; }
    }

    #region[StaffMenuModal]
    public class StaffMenuModel
    {
        public int MenuId { get; set; }
        public string MemberType { get; set; }
        public string ActiveClass { get; set; }
        public string MenuUrl { get; set; }
        public string MenuName { get; set; }
        public string Position { get; set; }
        public bool Status { get; set; }
        public string Icon { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public List<SelectListItem> RoleList { get; set; }
        public List<StaffMenuModel> StaffMenuList { get; set; }
        public List<SubMenuModel> SubMenuList { get; set; }
        public List<SubMenuIdListModel> SubMenuIdList { get; set; }
    }

    public class SubMenuIdListModel
    {
        public int MenuId { get; set; }
        public int SubMenuId { get; set; }
    }
    #endregion
}