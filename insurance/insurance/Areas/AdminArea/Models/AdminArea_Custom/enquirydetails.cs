﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class EnquiryDetails
    {
        public int EnquiryId { get; set; }
        public string Policy_Type { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Sum_Insured { get; set; }
        public string Enquiry_Id { get; set; }
        public string Enqdate { get; set; }
        public string AgencyName { get; set; }
        public string AgencyId { get; set; }
        public string Adult { get; set; }
        public string Child { get; set; }
        public string BlockID { get; set; }
        public string Status { get; set; }
        public string Email_Id { get; set; }
        public string Mobile_No { get; set; }
        public string Pin_Code { get; set; }

        public List<EnquiryDetails> EnquiryDetailList { get; set; }

    }

    public class TrackEnquirydetail
    {
        public string engineno { get; set; }
        public string dob { get; set; }
        public string chasisno { get; set; }
        public string vehicletype { get; set; }

        public string ChainId { get; set; }
        public string AgencyId { get; set; }
        public string previousinsured { get; set; }
        public string nrelation { get; set; }
        public string ntitle { get; set; }
        public string ispreviousinsurer { get; set; }
        public string isclaiminlastyear { get; set; }
        public string ppolicynumber { get; set; }
        public string policyexpirydate { get; set; }

        public string DateOfMfg { get; set; }
        public string nfirstname { get; set; }
        public string nlastname { get; set; }
        public string ndob { get; set; }

        public string policyholdertype { get; set; }
        public string CreatedDate { get; set; }
        public string ProposalNo { get; set; }
        public string insurancetype { get; set; }
        public string RtoName { get; set; }
        public string BrandName { get; set; }
        public string DateOfReg { get; set; }
        public string Fuel { get; set; }
        public string BusinessPhone { get; set; }
        public string BusinessEmailID { get; set; }
        public string Address { get; set; }
        public string policynumber { get; set; }
        public string VarientName { get; set; }
        public string ModelName { get; set; }      
        public string VechicleRegNo { get; set; }
        public string Policy_Type { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Sum_Insured { get; set; }
        public string Enquiry_Id { get; set; }
        public string Enqdate { get; set; }
        public string AgencyName { get; set; }
        public string Adult { get; set; }
        public string Child { get; set; }
        public string BlockID { get; set; }
        public string Status { get; set; }
        public string Email_Id { get; set; }
        public string Mobile_No { get; set; }
        public string Pin_Code { get; set; }

        public string AgencyAddress { get; set; }
        public string AgencyCityName { get; set; }
        public string AgencyCountry { get; set; }
        public string AgencyBusinessPhone { get; set; }
        public string AgencyBusinessEmailID { get; set; }
        public string AgencyGSTNo { get; set; }
        public string InvoiceNo { get; set; }
        public string OrderID { get; set; }
        public string Agencyownername { get; set; }
        public string AgencyPersonalEmail { get; set; }
        public string AgencyMobile { get; set; }


        public List<TrackEnquirydetail> TrackEnquirydetailList { get; set; }

    }

    public class TrackHealthEnquirydetail
    {
        public string EnquiryId { get; set; }
        public string p_rowpaymenturl { get; set; }
        public string CityName { get; set; }
        public string Pincode { get; set; }
        public string CompanyName { get; set; }
        public string title { get; set; }
        public string BusinessPhone { get; set; }
        public string Address { get; set; }
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Policy_Type { get; set; }
        public string Sum_Insured { get; set; }
        public string Pin_Code { get; set; }
        public string UpdatedDate { get; set; }
        public string productid { get; set; }
        public string companyid { get; set; }
        public string suminsured { get; set; }
        public string premium { get; set; }
        public string dob { get; set; }
        public string address1 { get; set; }
        public string statename { get; set; }
        public string cityname { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mobile { get; set; }
        public string emailid { get; set; }
        public string pincode { get; set; }
        public string gender { get; set; }
        public string policyfor { get; set; }
        public string plantype { get; set; }
        public string ntitle { get; set; }
        public string nfirstname { get; set; }
        public string nlastname { get; set; }
        public string ndob { get; set; }
        public string nage { get; set; }
        public string nrelations { get; set; }
        public string annualincome { get; set; }
        public string p_proposalNum { get; set; }
        public string p_totalPremium { get; set; }
        public string p_paymenturl { get; set; }
        public string pay_proposal_Number { get; set; }
        public string CreatedDate { get; set; }
        public string pay_paymentdate { get; set; }
        public string ProductName { get; set; }
        public string periodfor { get; set; }
        public string AgencyEmail { get; set; }
        public List<TrackHealthEnquirydetail> TrackEnquirydetailhealthList { get; set; }

    }

    #region[Motor]
    public class EnquiryDetails_Motors
    {
        public int Id { get; set; }
        public string Enquiry_Id { get; set; }
        public string AgencyId { get; set; }
        public string BlockID { get; set; }
        public string NewStatus { get; set; }
        public string AgencyName { get; set; }
        public string VechileRegNo { get; set; }
        public string MobileNo { get; set; }
        public bool Is_Term_Accepted { get; set; }
        public string RtoId { get; set; }
        public string RtoName { get; set; }
        public string DateOfReg { get; set; }
        public string YearOfMake { get; set; }
        public string BrandId { get; set; }
        public string BrandName { get; set; }
        public string ModelId { get; set; }
        public string ModelName { get; set; }
        public string Fuel { get; set; }
        public string VarientId { get; set; }
        public string VarientName { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string PinCode { get; set; }
        public string ispreviousinsurer { get; set; }
        public string insurerid { get; set; }
        public string insurername { get; set; }
        public string insurancetype { get; set; }
        public string policynumber { get; set; }
        public string policyexpirydate { get; set; }
        public string isclaiminlastyear { get; set; }
        public string previousyearnoclaim { get; set; }

        public string vehicletype { get; set; }
        public List<EnquiryDetails_Motors> EnquiryDetailList { get; set; }


    }
    #endregion

    #region[Common]
    public class BookingDetails
    {
        public string AgencyID { get; set; }
        public string AgencyName { get; set; }
        public string Enquiry_Id { get; set; }
        public string Insurance { get; set; }
        public string Insurer { get; set; }
        public string PolicyNo { get; set; }
        public string SumInsured { get; set; }
        public string ProposerName { get; set; }
        public string CustomerName { get; set; }
        public string Insured { get; set; }
        public string Basic { get; set; }
        public string GST { get; set; }
        public string Premium { get; set; }
        public string Discount { get; set; }
        public string TDS { get; set; }
        public string ProductName { get; set; }
        public string PolicyType { get; set; }
        public string Tenure { get; set; }
        public string PolicyDate { get; set; }
    }
    #endregion
}