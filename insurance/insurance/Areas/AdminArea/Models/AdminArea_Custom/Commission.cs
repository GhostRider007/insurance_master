﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class Commission
    {
        public int CommisionId { get; set; }
        public string BasicPer { get; set; }
        public string BasicTDS { get; set; }
        public string IncentivePer { get; set; }
        public string IncentiveTDS { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public List<Commission> CommissionList { get; set; }
    }
}