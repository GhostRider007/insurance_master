﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class AgencyModel
    {
        public class Agency
        {
            public int AgencyID { get; set; }
            public string AgencyIdNew { get; set; }
            public string AgencyName { get; set; }
            public string Address { get; set; }
            public string CountryID { get; set; }
            public string CountryName { get; set; }
            public string StateID { get; set; }
            public string StateName { get; set; }
            public string CityID { get; set; }
            public string CityName { get; set; }
            public string Pincode { get; set; }
            public string GSTNo { get; set; }
            public string BusinessEmailID { get; set; }
            public string BusinessPhone { get; set; }
            public int ReferenceBy { get; set; }
            public string BusinessAddressProofImg { get; set; }
            public string BusinessPanCardImg { get; set; }
            public string PanNo { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailID { get; set; }
            public string Mobile { get; set; }
            public string PersonalAddressProofImg { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }
            public string CompanyLogoImg { get; set; }
            public string Type { get; set; }
            public string CreditLimit { get; set; }
            public string TempCreditLimit { get; set; }
            public string TempCreditDate { get; set; }
            public string GroupType { get; set; }
            public string LastLoginDate { get; set; }
            public bool IsApproved { get; set; }
            public string IsApprovedStr { get; set; }
            public string TempApiKey { get; set; }
            public string EncryKey { get; set; }
            public string IVKey { get; set; }
            public bool Status { get; set; }
            public string CreatedDate { get; set; }
            public string UpdatedDate { get; set; }

            public string AvalibleAmount { get; set; }
            public string DueAmount { get; set; }
            public List<Agency> AgencyList { get; set; }

            public int DsrID { get; set; }
            public int StaffId { get; set; }
            public string StaffName { get; set; }
            public string DSRStatus { get; set; }            
            public string ReadStatus { get; set; }
            public string NewdrsReport { get; set; }
            public string AdminRemark { get; set; }
            public string UserFeedBack { get; set; }
            public string AgencyFeedBack { get; set; }
            public string CreatedByUserID { get; set; }
            public string CreatedByUserName { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
        }
    }
}