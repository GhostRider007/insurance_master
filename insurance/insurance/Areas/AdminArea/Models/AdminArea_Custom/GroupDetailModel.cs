﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class GroupDetailModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public List<GroupDetailModel> GroupList { get; set; }
    }
}