﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace insurance.Areas.AdminArea.Models.AdminArea_Custom
{
    public class RoleModel
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public bool Status { get; set; }
        public string CreatedDate { get; set; }
        public List<RoleModel> RoleList { get; set; }
    }

    public class RolePermission
    {
        public string RoleName { get; set; }
        public List<MenuModel> MenuList { get; set; }
        public List<SubMenuModel> SubMenuList { get; set; }
        public List<SetRolePermission> RolePermissionList { get; set; }
    }

    //public class SubMenuIdListModel
    //{
    //    public int MenuId { get; set; }
    //    public int SubMenuId { get; set; }
    //}

    public class SetRolePermission
    {
        public int PermissionID { get; set; }
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int SubMenuID { get; set; }
        public string SubMenuName { get; set; }
        public int RoleID { get; set; }
        public string IsDelete { get; set; }
        public string IsSectionCheck { get; set; }
        //public DateTime CreatedDate { get; set; }
    }
    public class CompanyModel
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        public string logoUrl { get; set; }
        public string CreateDate { get; set; }
        public List<CompanyModel> CompanyList { get; set; }
    }

    public class MotorCompanyModel
    {
        public int id { get; set; }
        public string Supplier { get; set; }
        public string ApiUsername { get; set; }
        public string ApiPassword { get; set; }
        public string ApiAgencyID { get; set; }
        public string TwoWQuoteUrl { get; set; }
        public string TwoWProposalUrl { get; set; }
        public string PCQuoteUrl { get; set; }
        public string PCProposalUrl { get; set; }
        public string CMVQuoteUrl { get; set; }
        public string CMVProposalUrl { get; set; }
        public string Clientid { get; set; }
        public string TripType { get; set; }
        public string URLTYPE { get; set; }

        public string OtherData { get; set; }
        public string CreatedDate { get; set; }
        public string TotalHit { get; set; }
        public string Paymentkey { get; set; }
        public string Image { get; set; }
        public bool Active { get; set; }
        public List<MotorCompanyModel> CompanyList { get; set; }
    }

    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public bool Active { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ProductCode { get; set; }
        public string PolicyCode { get; set; }
        public string ProductType { get; set; }
        public string Product_Description { get; set; }
        public string Rider { get; set; }
        public string ModifyDate { get; set; }
        public string CreateDate { get; set; }
        public string BrochureLink { get; set; }
        public string ClausesLinkUrl { get; set; }
    }
}