﻿
$(function () {
    var totalrecord = $("#totalrecord").val();
    if (totalrecord == "0") {
        $(".searchpanel").addClass("hidden");
    }
    else {
        $(".searchpanel").removeClass("hidden");
    }
});

function MenuSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("MenuName")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var menudetail = {};
    menudetail.MenuId = GetParameterValues("menuid");
    menudetail.MenuName = $("#MenuName").val();
    menudetail.MenuUrl = $("#MenuUrl").val();
    menudetail.Icon = $("#Icon").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Menu/ProcessInsertMenu",
        data: '{menu: ' + JSON.stringify(menudetail) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                    if (data[1] == "insert") {
                        ResetMenuDetail();
                    }
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
            setTimeout(function () {
                window.location = 'menu-list';
            }, 500);
        }
    });
}

function ResetMenuDetail() {
    $("#MenuName").val("");
    $("#MenuUrl").val("");
    $("#Icon").val("");
}

$('#txtMenuSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var MenuList = $(".menulistcss");
    if (_this.val() != "") {
        $.each(MenuList, function (i, item) {
            var Menuname = $(this).data('menuname').toLowerCase();
            if (Menuname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(MenuList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".btnSetPosition").click(function () {
    var menuid = $(this).data("menuid");
    var thismenuposval = $("#txtPosition_" + menuid).val();

    var thisbutton = $("#btnSetPosition_" + menuid);

    $(thisbutton).html("<i class='fa fa-pulse fa-spinner processspin' style='color: #fff;'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/Backend_Menu/UpdateMenuPosition",
        data: '{menuid:' + JSON.stringify(menuid) + ',posval:' + JSON.stringify(thismenuposval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data = "true") {
                    setTimeout(function () {
                        $(thisbutton).html("Set").prop("disabled", false);
                    }, 1000);
                }
                else {
                    alert("Error Occured!");
                }
            }
        }
    });
});

$(".deletemenu").click(function () {
    if (confirm('Are you sure, you want to delete this menu ?')) {
        var menuid = $(this).data("menuid");
        $("#delspan_" + menuid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Menu/UpdateMenuStatus",
            data: '{menuid:' + JSON.stringify(menuid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });        
    }
});

$(".restoremenu").click(function () {
    if (confirm('Are you sure, you want to restore this menu ?')) {
        var menuid = $(this).data("menuid");
        $("#restorespan_" + menuid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Menu/UpdateMenuStatus",
            data: '{menuid:' + JSON.stringify(menuid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});


function SubMenuSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckDropDownBlankValidation('MenuId')) return !1;
    if (CheckBlankValidation("SubMenuName")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var submenudetail = {};
    submenudetail.SubMenuId = GetParameterValues("submenuid");
    submenudetail.MenuId = $("#MenuId").val();
    submenudetail.SubMenuName = $("#SubMenuName").val();
    submenudetail.SubMenuUrl = $("#SubMenuUrl").val();
    submenudetail.Icon = $("#Icon").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Menu/ProcessInsertSubMenu",
        data: '{submenu: ' + JSON.stringify(submenudetail) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                    if (data[1] == "insert") {
                        ResetMenuDetail();
                    }
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
            setTimeout(function () {
                window.location = 'submenu-list';
            }, 1000);
        }
    });
}

function ResetSubMenuDetail() {
    $("#SubMenuName").val("");
    $("#SubMenuUrl").val("");
    $("#Icon").val("");
}

$('#txtSubMenuSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var SubMenuList = $(".submenulistcss");
    if (_this.val() != "") {
        $.each(SubMenuList, function (i, item) {
            var SubMenuname = $(this).data('submenuname').toLowerCase();
            if (SubMenuname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(SubMenuList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deletesubmenu").click(function () {
    if (confirm('Are you sure, you want to delete this sub menu ?')) {
        var submenuid = $(this).data("submenuid");
        $("#delsubmenuspan_" + submenuid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Menu/UpdateSubMenuStatus",
            data: '{submenuid:' + JSON.stringify(submenuid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restoresubmenu").click(function () {
    if (confirm('Are you sure, you want to restore this sub menu ?')) {
        var submenuid = $(this).data("submenuid");
        $("#restoresubmenuspan_" + submenuid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Menu/UpdateSubMenuStatus",
            data: '{submenuid:' + JSON.stringify(submenuid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".btnSubMenuSetPosition").click(function () {
    var submenuid = $(this).data("submenuid");
    var thissubmenuposval = $("#txtSubMenuPosition_" + submenuid).val();

    var thisbutton = $("#btnSubMenuSetPosition_" + submenuid);

    $(thisbutton).html("<i class='fa fa-pulse fa-spinner processspin' style='color: #fff;'></i>").prop("disabled", true);

    $.ajax({
        type: "Post",
        url: "/Backend_Menu/UpdateSubMenuPosition",
        data: '{submenuid:' + JSON.stringify(submenuid) + ',posval:' + JSON.stringify(thissubmenuposval) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data = "true") {
                    setTimeout(function () {
                        $(thisbutton).html("Set").prop("disabled", false);
                    }, 1000);
                }
                else {
                    alert("Error Occured!");
                }
            }
        }
    });
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}