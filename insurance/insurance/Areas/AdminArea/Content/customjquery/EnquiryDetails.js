﻿$(document).ready(function () {
    $("#searchProduct").prop("disabled", true);
    BindAgencies();
    BindInsurers();
});

$(".ActiveEnquiry").click(function () {
    if (confirm('Are you sure, you want to Accept this Enquiry ?')) {
        var Enquiry_ID = $(this).data("memberid");

        $.ajax({
            type: "Post",
            url: "/Backend_Enquiry/AcceptRejectBlockedEnquiry",
            data: '{Enquiry_ID:' + JSON.stringify(Enquiry_ID) + ',status:' + JSON.stringify("UnderProcess") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});
$(".InActiveEnquiry").click(function () {
    if (confirm('Are you sure, you want to In Reject this Enquiry ?')) {
        
        var Enquiry_ID = $(this).data("memberid");

        $.ajax({
            type: "Post",
            url: "/Backend_Enquiry/AcceptRejectBlockedEnquiry",
            data: '{Enquiry_ID:' + JSON.stringify(Enquiry_ID) + ',status:' + JSON.stringify("Rejected") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".DetailEnquiry").click(function () {
          
        var Enquiry_ID = $(this).data("memberid");
        $.ajax({
            type: "Post",
            url: "/Backend_Enquiry/BlockedEnquiryDetails",
            data: '{Enquiry_ID:' + JSON.stringify(Enquiry_ID) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                   
                    //if (data = "true") {
                        $('#ModalPopUp').modal('show');
                    document.getElementById("AgencyName").value = data[0].AgencyName;
                    document.getElementById("Name").value = data[0].First_Name + " " + data[0].Last_Name;
                        document.getElementById("Sum_Insured").value = data[0].Sum_Insured;
                    document.getElementById("Policy_Type").value = data[0].Policy_Type;
                    document.getElementById("Enquiry_Id").value = data[0].Enquiry_Id;
                    document.getElementById("Enqdate").value = data[0].Enqdate;
                    document.getElementById("Adult").value = data[0].Adult;
                    document.getElementById("Child").value = data[0].Child;
                    document.getElementById("BlockID").value = data[0].BlockID;
                    document.getElementById("Email_Id").value = data[0].Email_Id;
                    document.getElementById("Mobile_No").value = data[0].Mobile_No;
                    document.getElementById("Pin_Code").value = data[0].Pin_Code;
                                      

                    }
                    else {

                        alert("Error Occured!");
                    }
               // }
            }
        });
   
});

function ClickedCurrEnquiry(id) {        
    if (id != null) {
        if (confirm('Are you sure, you want to Block this Enquiry ?')) {
            //var Enquiry_ID = $(this).data("memberid");
            $("#BackendEnquiry_" + id).html("<i class='fa fa-spinner fa-pulse'></i>").css({ "background-color": "red", "border-color": "red", "color": "#fff" });
            $.ajax({
                type: "Post",
                url: "/Backend_Enquiry/BlockEnquiry",
                data: '{Enquiry_ID:' + JSON.stringify(id) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data = "true") {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                        else {
                            alert("Error Occured!");
                        }
                    }
                }
            });
        }
    }
}

function BindAgencies() {

    $.ajax({
        type: "Post",
        url: "/Backend_Enquiry/BindAgencies",
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
           // $("#searchInsurer").html("<option value=''>---Select Agency---</option>");
            $("#searchAgencyId").append(data);
        }
    });
}
$("#searchInsurer").change(function () {
    let Insurer = $("#searchInsurer option:selected").val();
    if (Insurer != '') {
        BindProducts();
    }
    else {
        $("#searchProduct").prop("disabled", true);
        $("#searchProduct").val("");
    }
});
function BindInsurers() {

    $.ajax({
        type: "Post",
        url: "/Backend_Enquiry/BindInsurersMotor",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#searchInsurer").html("<option value=''>--Select--</option>");
            $("#searchInsurer").append(data);
            $("#searchInsurer").prop("disabled", false);
        }
    });
}
function BindProducts() {

    $.ajax({
        type: "Post",
        url: "/Backend_Enquiry/BindProductsMotor",
        data: '{insuranceId:' + JSON.stringify($("#searchInsurer option:selected").data('compid')) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#searchProduct").html("<option value=''>--Select--</option>");
            $("#searchProduct").append(data);
            $("#searchProduct").prop("disabled", false);
        }
    });
}

//$('#txtAgencySearch').on('keyup', function () {
//    var issearch = false;
//    var _this = $(this); // copy of this object for further usage
//    var MemberList = $(".memberlistcss");
//    if (_this.val() != "") {
//        $.each(MemberList, function (i, item) {
//            var membername = $(this).data('membername').toLowerCase();
//            if (membername.search(_this.val()) >= 0) {
//                issearch = true;
//                $(this).removeClass("hidden");
//            }
//            else {
//                $(this).addClass("hidden");
//            }
//        });

//        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
//        else { $(".norecordfound").addClass("hidden"); }
//    }
//    else {
//        $.each(MemberList, function (i, item) {
//            $(this).removeClass("hidden");
//        });

//        $(".norecordfound").addClass("hidden");
//    }
//});
//function filterData(dataCount) {
//    let searchStr = $("#txtAgencySearch").val();
//    let FromDate = $("#txtFromDateSearch").val();
//    let ToDate = $("#txtToDateSearch").val();

//  let searchStrLen = searchStr.replace(' ','');


//    if (searchStrLen.length > 0 || FromDate.trim().length > 0 || ToDate.trim().length > 0) {

//        for (let i = 0; i < dataCount; i++) {
//            $("#row_" + i).addClass("hidden");

//        }

//        $.ajax({
//            type: "Post",
//            url: "/Backend_Enquiry/EnquiryFileration",
//            data: '{searchStr: ' + JSON.stringify(searchStr) + ', FromDate: ' + JSON.stringify(FromDate) + ', ToDate: ' + JSON.stringify(ToDate) + '}',
//            contentType: "application/json; charset=utf-8",
//            datatype: "json",
//            success: function (data) {

//                if (data.length > 0) {
//                    for (let i = 0; i < dataCount; i++) {
//                        let currID = $("#row_" + i).attr("data-id");

//                        if (data.includes(Number(currID))) {
//                            $("#row_" + i).removeClass("hidden");
//                        }

//                        //for (let j = 0; j < data.length; j++) {
//                        //    if (currID === data[j]) {
                                
//                        //    }
//                        //}
//                    }
//                }
//            }
//        });
//    }

//    //else {
//    //    for (let i = 0; i < dataCount; i++) {
//    //        $("#row_" + i).removeClass("hidden");
//    //    }
//    //}
//};
