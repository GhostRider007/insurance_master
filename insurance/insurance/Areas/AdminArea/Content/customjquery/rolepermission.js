﻿$("#btnSetSubmit").click(function () {
    var thisbutton = $("#btnSetSubmit");
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var perModelList = [];

    var isonechecked = false;

    $(".innersubmenu").each(function () {
        var mainMenu = $(this).attr("id");
        var roleid = GetParameterValues("roleid");
        var menuid = $(this).data("menuid");

        $("." + mainMenu).each(function () {
            var subMenuCheck = $(this).prop("checked");
            var submenuid = $(this).data("submenuid");
            if (subMenuCheck) {
                isonechecked = true;
                isAllBlanck = false;
                var subMenu = $(this).attr("id");
                var perModel = {};
                perModel.MenuID = menuid;
                perModel.MenuName = mainMenu;
                perModel.SubMenuID = submenuid;
                perModel.SubMenuName = subMenu;
                perModel.RoleID = roleid;
                perModel.IsDelete = "delete";
                perModel.IsSectionCheck = subMenuCheck;
                perModelList.push(perModel);
            }
        });
    });

    if (isonechecked) {
        $.ajax({
            type: "POST",
            url: "/Backend_RolePermission/InserRolePermissionList",
            data: '{modelList: ' + JSON.stringify(perModelList) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != "") {
                    if (data == "true") {
                        ShowMessagePopup("Success", "#00a300", "Role permission set successfully.", "#00a300");
                    }
                }

                $(thisbutton).html("Submit");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Warning", "#e10808", "Please select at least one section !", "#e10808");
        $(thisbutton).html("Submit");
    }
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}

function menuChecker(menuid) {

    //$("input:checkbox").each(function (index, element) {

    //    //alert($(element).attr("data-menuname"));
    //    let menuTemp = $(element).attr("data-menuname");
    //    if (menuTemp == menuName) {
    //        $(this).prop('checked', $("#checkbox_" + menuName).prop('checked'));
    //    }
    //});


    $(".submenuclass").each(function (index, element) {
        let menuidsub = $(this).data("menuidsub");

        if (menuidsub === menuid) {
            //$(this).prop('checked', true);
            $(this).prop('checked', $("#checkbox_" + menuid).prop('checked'));
        }


        //alert($(element).attr("data-menuname"));
        //let menuTemp = $(element).attr("data-menuname");
        //if (menuTemp == menuName) {
        //    $(this).prop('checked', $("#checkbox_" + menuName).prop('checked'));
        //}
    });

};
