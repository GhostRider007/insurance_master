﻿function CommisionSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("BasicPer")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var comm = {};
    comm.CommisionId = GetParameterValues("commissionid");
    comm.BasicPer = $("#BasicPer").val();
    comm.BasicTDS = $("#BasicTDS").val();
    comm.IncentivePer = $("#IncentivePer").val();
    comm.IncentiveTDS = $("#IncentiveTDS").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Role/ProcessInsertCommission",
        data: '{comm: ' + JSON.stringify(comm) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                    if (data[1] == "insert") {
                        $("#BasicPer").val("");
                        $("#BasicTDS").val("");
                        $("#IncentivePer").val("");
                        $("#IncentiveTDS").val("");
                    }
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
        }
    });
}

$(".deletecommision").click(function () {
    if (confirm('Are you sure, you want to delete this commission ?')) {
        var commissionid = $(this).data("commisionid");
        $("#delcommisionspan_" + commissionid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCommissionStatus",
            data: '{commissionid:' + JSON.stringify(commissionid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restorecommision").click(function () {
    if (confirm('Are you sure, you want to restore this commission ?')) {
        var commissionid = $(this).data("commisionid");
        $("#restorecommisionspan_" + commissionid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCommissionStatus",
            data: '{commissionid:' + JSON.stringify(commissionid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}