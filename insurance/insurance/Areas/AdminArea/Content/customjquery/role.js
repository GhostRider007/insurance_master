﻿$(function () {
    var totalrecord = $("#totalrecord").val();
    if (totalrecord == "0") {
        $(".searchpanel").addClass("hidden");
    }
    else {
        $(".searchpanel").removeClass("hidden");
    }
});

function RoleSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("RoleName")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var role = {};
    role.RoleID = GetParameterValues("roleid");
    role.RoleName = $("#RoleName").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Role/ProcessInsertRole",
        data: '{role: ' + JSON.stringify(role) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                    if (data[1] == "insert") {
                        $("#RoleName").val("");
                    }
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
        }
    });
}

$('#txtRoleSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var RoleList = $(".rolelistcss");
    if (_this.val() != "") {
        $.each(RoleList, function (i, item) {
            var rolename = $(this).data('rolename').toLowerCase();
            if (rolename.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(RoleList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deleterole").click(function () {
    if (confirm('Are you sure, you want to delete this role ?')) {
        var roleid = $(this).data("roleid");
        $("#delrolespan_" + roleid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateRoleStatus",
            data: '{roleid:' + JSON.stringify(roleid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restorerole").click(function () {
    if (confirm('Are you sure, you want to restore this role ?')) {
        var roleid = $(this).data("roleid");
        $("#restorerolespan_" + roleid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateRoleStatus",
            data: '{roleid:' + JSON.stringify(roleid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

function CompanySubmitEvt() {
    var thisbutton = $("#btnSubmit");
    //if (CheckBlankValidation("RoleName")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    
}

$('#txtCompanySearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var CompanyList = $(".companylistcss");
    if (_this.val() != "") {
        $.each(CompanyList, function (i, item) {
            var companyname = $(this).data('companyname').toLowerCase();
            if (companyname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.CompanyList(RoleList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$('#txtCompanymotorSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var CompanyList = $(".companylistcss");
    if (_this.val() != "") {
        $.each(CompanyList, function (i, item) {
            var companyname = $(this).data('supplier').toLowerCase();
            if (companyname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.CompanyList(RoleList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deletecompany").click(function () {
    if (confirm('Are you sure, you want to delete this company ?')) {
        var companyid = $(this).data("companyid");
        $("#delcompanyspan_" + companyid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCompanyStatus",
            data: '{companyid:' + JSON.stringify(companyid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".deletecompanymotor").click(function () {
    if (confirm('Are you sure, you want to delete this company ?')) {
        var companyid = $(this).data("companyid");
        $("#delcompanyspan_" + companyid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCompanyStatushealth",
            data: '{companyid:' + JSON.stringify(companyid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restorecompany").click(function () {
    if (confirm('Are you sure, you want to restore this role ?')) {
        var companyid = $(this).data("companyid");
        $("#restorecompanyspan_" + companyid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCompanyStatus",
            data: '{companyid:' + JSON.stringify(companyid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restorecompanymotor").click(function () {
    if (confirm('Are you sure, you want to restore this role ?')) {
        var companyid = $(this).data("companyid");
        $("#restorecompanyspan_" + companyid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateCompanyStatushealth",
            data: '{companyid:' + JSON.stringify(companyid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}