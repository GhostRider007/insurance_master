﻿$(function () {
    var totalrecord = $("#totalrecord").val();
    if (totalrecord == "0") {
        $(".searchpanel").addClass("hidden");
    }
    else {
        $(".searchpanel").removeClass("hidden");
    }
});

function GroupSubmitEvt() {
    var thisbutton = $("#btnSubmit");
    if (CheckBlankValidation("GroupName")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var groupid = GetParameterValues("groupid");

    groupid = groupid != undefined ? groupid : null;
    var groupname = $("#GroupName").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Role/ProcessGroupDetail",
        data: '{groupid:' + JSON.stringify(groupid) + ',groupname: ' + JSON.stringify(groupname) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {                  
                    ShowMessagePopup("Success", "#00a300", data[2], "#00a300");  
                    if (data[1] == "ins") {
                        $("#GroupName").val('');
                    }
                }
                else {
                    ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                }
            }

            $(thisbutton).html("Submit");
        }
    });
}

$('#txtGroupSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var GroupList = $(".grouplistcss");
    if (_this.val() != "") {
        $.each(GroupList, function (i, item) {
            var groupname = $(this).data('groupname').toLowerCase();
            if (groupname.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(GroupList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

function deletethisrecord(groupid) {
    if (confirm('Are you sure, you want to delete this group ?')) {
        $("#delgroupnspan_" + groupid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateGroupStatus",
            data: '{groupid:' + JSON.stringify(groupid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
}

function restorethisrecord(groupid) {
    if (confirm('Are you sure, you want to restore this group ?')) {       
        $("#restoregroupspan_" + groupid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Role/UpdateGroupStatus",
            data: '{groupid:' + JSON.stringify(groupid) + ',status:' + JSON.stringify("restore") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
} 

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}