﻿
$(function () {
    var totalrecord = $("#totalrecord").val();
    if (totalrecord == "0") {
        $(".searchpanel").addClass("hidden");
    }
    else {
        $(".searchpanel").removeClass("hidden");
    }
});

function MemberSubmitEvt() {
    var thisbutton = $("#btnMemberSubmit");
    if (CheckBlankValidation("FirstName")) return !1;
    if (CheckBlankValidation("LastName")) return !1;
    if (CheckBlankValidation("EmailId")) return !1;
    if (CheckEmailValidatoin("EmailId")) return !1;
    if (CheckBlankValidation("MobileNo")) return !1;
    if (CheckDropDownBlankValidation('RoleId')) return !1;
    if (CheckBlankValidation("UserId")) return !1;
    if (CheckBlankValidation("Password")) return !1;

    var member = {};
    member.LoginId = GetParameterValues("memberid");
    member.FirstName = $("#FirstName").val();
    member.LastName = $("#LastName").val();
    member.EmailId = $("#EmailId").val();
    member.MobileNo = $("#MobileNo").val();
    member.UserId = $("#UserId").val();
    member.Password = $("#Password").val();
    member.Image = $("#fluStaffPhoto").val().replace(/C:\\fakepath\\/i, '');
    member.RoleId = $("#RoleId").val();
    member.MemberType = "staff";

    if (!$(".useridexist").hasClass("available")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            url: "/Backend_Registration/ProcessInsertMember",
            data: '{member: ' + JSON.stringify(member) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data[0] == "true") {
                        if (member.Image != null && member.Image != "") {
                            var fileUpload = $("#fluStaffPhoto").get(0);
                            var files = fileUpload.files;
                            //Create FormData object
                            var fileData = new FormData();
                            //Looping over all files and add it to FormData object
                            for (var i = 0; i < files.length; i++) {
                                fileData.append(files[i].name, files[i]);
                            }

                            $.ajax({
                                url: '/Backend_Registration/UpdateMemberImageName',
                                type: "POST",
                                contentType: false, // Not to set any content header  
                                processData: false, // Not to process data  
                                data: fileData,
                                //data: { fileData, imageId: imgId, hotcode: hotelco },
                                success: function (result) {
                                    if (result != null) {
                                        if (result == "true") {
                                            ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                                            if (data[1] == "insert") {
                                                ResetMemberDetail();
                                            }
                                        }
                                        else {
                                            ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            ShowMessagePopup("Success", "#00a300", data[2], "#00a300");
                            if (data[1] == "insert") {
                                ResetMemberDetail();
                            }
                        }
                    }
                    else {
                        ShowMessagePopup("Failed", "#d91717", "error occured.", "#d91717");
                    }
                    setTimeout(function () {
                        window.location = 'member-list';
                    }, 3000);
                }

                $(thisbutton).html("Submit");
            }
        });
    }
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}

function ResetMemberDetail() {
    $("#FirstName").val("");
    $("#LastName").val("");
    $("#EmailId").val("");
    $("#MobileNo").val("");
    $("#UserId").val("");
    $("#Password").val("");
    $("#fluStaffPhoto").val("");
    $("#RoleId").val("");
}

function CheckUserId() {
    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Backend_Registration/IsUserAlreadyExist",
            data: '{userid: ' + JSON.stringify($("#UserId").val()) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data == "true") {
                        $(".useridexist").html("User Id already already exist !!!");
                        $(".useridexist").addClass(" available").removeClass("hidden");
                    }
                    else {
                        $(".useridexist").html("");
                        $(".useridexist").removeClass(" available").addClass("hidden");
                    }
                }
            }
        });
    }, 100);
}

$('#txtMemberSearch').on('keyup', function () {
    var issearch = false;
    var _this = $(this); // copy of this object for further usage
    var MemberList = $(".memberlistcss");
    if (_this.val() != "") {
        $.each(MemberList, function (i, item) {
            var membername = $(this).data('membername').toLowerCase();
            if (membername.search(_this.val()) >= 0) {
                issearch = true;
                $(this).removeClass("hidden");
            }
            else {
                $(this).addClass("hidden");
            }
        });

        if (!issearch) { $(".norecordfound").removeClass("hidden"); }
        else { $(".norecordfound").addClass("hidden"); }
    }
    else {
        $.each(MemberList, function (i, item) {
            $(this).removeClass("hidden");
        });

        $(".norecordfound").addClass("hidden");
    }
});

$(".deletemember").click(function () {
    if (confirm('Are you sure, you want to delete this member ?')) {
        var memberid = $(this).data("memberid");
        $("#delmemberspan_" + memberid).removeClass("fa-trash").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Registration/UpdateMemberStatus",
            data: '{memberid:' + JSON.stringify(memberid) + ',status:' + JSON.stringify("delete") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});

$(".restoremember").click(function () {
    if (confirm('Are you sure, you want to restore this member ?')) {
        var memberid = $(this).data("memberid");
        $("#restorememberspan_" + memberid).removeClass("fa-undo").addClass(" fa-pulse fa-spinner");
        $.ajax({
            type: "Post",
            url: "/Backend_Registration/UpdateMemberStatus",
            data: '{memberid:' + JSON.stringify(memberid) + ',status:' + JSON.stringify("active") + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data = "true") {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    else {
                        alert("Error Occured!");
                    }
                }
            }
        });
    }
});
