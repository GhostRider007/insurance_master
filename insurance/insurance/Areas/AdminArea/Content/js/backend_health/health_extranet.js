﻿//----------------------------------VARIABLE SECTION-------------------------------------
let InsuredPersonsCount = 0;
//-------------------------REGISTRATION SECTION------------------------------------------
$(function () {
    var enquiry_id = GetParameterValues('enquiry_id');
    if (enquiry_id == null || enquiry_id == '') { GenrateEnquiry_Id(); }
    BindInsurers();

});

function GenrateEnquiry_Id() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetGenrateEnquiryId",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                window.location.href = window.location.href + response;
            }
        }
    });
}

ResetMembers = () => {
    $(".chkmember").each(function () {
        let objmember = $(this).data("member");
        if ($(".chkbox_" + objmember).prop("checked") == true) {
            $(".chkbox_" + objmember).click();
        }
    });
}

function OnOffMember(membertype) {
    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") {
        $(".chkmember").each(function () {
            let objmember = $(this).data("member");

            if (objmember == membertype) {
                if ($(".chkbox_" + membertype).prop("checked") == true) {
                    $(".chkbox_" + membertype).click();
                }
            }
            else {
                if ($(".chkbox_" + objmember).prop("checked") == true) {
                    $(".chkbox_" + objmember).click();
                }
            }
        });
    }
    else {
        if ($(".FamilySection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "father" || objmember == "mother") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
        if ($("#ParentsSection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "self" || objmember == "spouse" || objmember == "son" || objmember == "daughter") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
    }
}
$("#chkSon").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divSonAgeSection"), "Son"); });

$("#chkDaughter").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divDaughterAgeSection"), "Daughter"); });

$("#CheSelf").click(function () {
    $("#txtSelfAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SelfAge").removeClass("hidden");
    }
    else {
        $(".SelfAge").addClass("hidden");
    }
})

$("#CheSpouse").click(function () {
    $("#txtSpouseAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SpouseAge").removeClass("hidden");
    }
    else {
        $(".SpouseAge").addClass("hidden");
    }
})

$("#CheFather").click(function () {
    $("#txtFatherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".FatherAge").removeClass("hidden");
    }
    else {
        $(".FatherAge").addClass("hidden");
    }
})

$("#CheMother").click(function () {
    $("#txtMotherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".MotherAge").removeClass("hidden");
    }
    else {
        $(".MotherAge").addClass("hidden");
    }
})

$(".clickcheckbox").click(function () {
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $("#" + selectedId + "Input").val("1");
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").removeClass("hidden");
        SonDautAgeSectionPlusMinus("plus", 1, "#div" + thisname + "AgeSection", thisname);
    }
    else {
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").addClass("hidden");
        SonDautAgeSectionPlusMinus("minus", 1, "#div" + thisname + "AgeSection", thisname);
    }
});

function SonDautAgeSectionPlusMinus(type, count, appentdiv, name) {
    if (type == "plus") {
        $(appentdiv).html("");
        var i; var bindagedel = "";
        for (i = 1; i <= parseInt(count); i++) {
            bindagedel += "<div class='col-sm-12'><div class='row'>"
                + "<div class='col-sm-6'> <label class='pull-right' style='color: #e71820;'>" + name + "</label></div>"
                + "<div class='col-sm-6'><h6>"
                + "<span class='ageleft'>Age</span>"
                + "<input type='text' class='form-control agetextbox' id='txt" + name + i + "Age' name='txt" + name + i + "Age' maxlength='2' onkeypress='return isNumberOnlyKeyNoDot(event)' onchange='return CalculateChildAge(\"txt" + name + i + "Age\",\"childageerror_" + name + i + "\")' /> <span class='yrposion'>yr</span>"
                + "</h6><span id='childageerror_" + name + i + "' style='float: right;margin: 0px 0px 0px 42px;color: #e71820;font-size: 8px;'></span></div>"
                + "</div></div>";
        }
        $(appentdiv).html(bindagedel);
    }
    else {
        $(appentdiv).html("");
    }
}

CalculateChildAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) <= 25) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age must below 25 Y&nbsp;</span>");
        }
    }
}

CalculateAdultAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");
        }
    }
}

function increment(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count < 4) {
        $(obj).parent().parent().find('input').val(count + 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count + 1, ("#" + appdiv), name);
    }
}
function decrement(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count != 1) {
        $(obj).parent().parent().find('input').val(count - 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count - 1, ("#" + appdiv), name);
    }
}


function CheckPincode2() {
    $(".notpincodevalid").addClass("hidden").html("");
    var txtPinCodeVal = $("#txtPinCode").val();

    if (txtPinCodeVal.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/CheckPincode",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response != "") {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response);
                    $("#txtPinCode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

function BindInsurers() {

    $.ajax({
        type: "Post",
        url: "/Backend_Enquiry/BindInsurers",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#ddlInsurer").html("<option value=''>--Select--</option>");
            $("#ddlInsurer").append(data);
        }
    });
}

$("#ddlInsurer").change(function () {
    let Insurer = $("#ddlInsurer option:selected").val();
    if (Insurer != '') {
        BindProducts();
    }
    else {
        $("#ddlProduct").prop("disabled", true);
        $("#ddlProduct").val("");
    }
});
$("#ddlInsurer").change(BindProducts);
function BindProducts() {

    $.ajax({
        type: "Post",
        url: "/Backend_Enquiry/BindProducts",
        data: '{insuranceId:' + JSON.stringify($("#ddlInsurer option:selected").data('compid')) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlProduct").html("<option value=''>--Select--</option>");
            $("#ddlProduct").append(data);
            $("#ddlProduct").prop("disabled", false);
        }
    });
}

function BindPlans() {

    //$.ajax({
    //    type: "Post",
    //    url: "/Backend_Enquiry/BindPlanType",
    //    async: false,
    //    datatype: "json",
    //    success: function (data) {
    //        $("#ddlPlanType").html("<option value=''>--Select--</option>");
    //        $("#ddlPlanType").append(data);
    //    }
    //});
}

$("#txtBasic").change(function () {
    let basic = $("#txtBasic").val();
    let tax = parseFloat(0.18 * basic);
    let premium = parseFloat(basic) + parseFloat(tax);
    $("#txtGST").val(tax);
    $("#txtTotalPremium").val(premium);
});



$("#section1Btn").click(function () {

    let isfieldapplied = false;
    let membercount = 0;
    let adultcount = 0;
    let childcount = 0;
    if (CheckFocusDropDownBlankValidation("ddlPolicyType")) return !1;
    if (CheckFocusBlankValidation("ddlSumInsured")) return !1;
    if (CheckFocusBlankValidation("txtPinCode")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlInsurer")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlProduct")) return !1;
    //if (CheckFocusDropDownBlankValidation("ddlPlanType")) return !1;

    if ($("#CheSelf").is(":checked")) { if (CheckFocusBlankValidation("txtSelfAge")) return !1; isfieldapplied = true; membercount = membercount + 1; adultcount = adultcount + 1; }
    if ($("#CheSpouse").is(":checked")) { if (CheckFocusBlankValidation("txtSpouseAge")) return !1; isfieldapplied = true; membercount = membercount + 1; adultcount = adultcount + 1; }
    if ($("#chkSon").is(":checked")) { var noofson = $("#chkSonInput").val(); var i; for (i = 1; i <= noofson; i++) { if (CheckFocusBlankValidation("txtSon" + i + "Age")) return !1; } isfieldapplied = true; membercount = membercount + 1; childcount = childcount + 1; }
    if ($("#chkDaughter").is(":checked")) { var noofdaughter = $("#chkDaughterInput").val(); var i; for (i = 1; i <= noofdaughter; i++) { if (CheckFocusBlankValidation("txtDaughter" + i + "Age")) return !1; } isfieldapplied = true; membercount = membercount + 1; childcount = childcount + 1; }
    if ($("#CheFather").is(":checked")) { if (CheckFocusBlankValidation("txtFatherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; adultcount = adultcount + 1; }
    if ($("#CheMother").is(":checked")) { if (CheckFocusBlankValidation("txtMotherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; adultcount = adultcount + 1; }

    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") { if (membercount < 1) { alert("Please select at least one member for genrate quotes"); return false; } }
    else { if (membercount < 2) { alert("Please select minimum two members for genrate quotes"); return false; } }
    if (CheckFocusBlankValidation("txtBasic")) return !1;

    InsuredPersonsCount = membercount;

    $("#companyImg").attr("src", $("#ddlInsurer option:selected").data('imgurl'));
    $("#productName_DetailSection").html($("#ddlProduct option:selected").val());
    $("#planType_DetailSection").html($("#ddlPlanType option:selected").val());
    $("#policyType_DetailSection").html($("#ddlPolicyType option:selected").val());
    $("#member_DetailSection").html("Adult-" + adultcount + ", Child-" + childcount);
    $("#sumInsured_DetailSection").html($("#ddlSumInsured").val());
    $("#basic_DetailSection").html($("#txtBasic").val());
    $("#discount_DetailSection").html("₹ 0.00");
    $("#gst_DetailSection").html($("#txtGST").val());
    $("#premium_DetailSection").html($("#txtTotalPremium").val());

    $("#fdselectedplansection").addClass("hidden");
    $("#fdproposerdetails").removeClass("hidden");

    proposalStarterEvent();
});
//---------------------------------------------------------------------------------------------
//--------------------------------------Proposal Details---------------------------------------
$("#section2PreviousBtn").click(function () {
    $("#fdproposerdetails").addClass("hidden");
    $("#fdselectedplansection").removeClass("hidden");
    $("#ddlAreaList").val("0");
});

function proposalStarterEvent() {
    $("#txtPPincode").val($("#txtPinCode").val());
    GetCityListFromStarHealth($("#txtPPincode").val(), '', '');
}

GetCityListFromStarHealth = (pincode, cityid, areaid) => {
    if (pincode != "") {
        let intpincode = parseInt(pincode);
        if (pincode.length == 6) {
            $("#ddlCityList").html("").html("<option value='0'>processing... <i class='fa fa-spinner fa-pulse'></i></option>");
            $.ajax({
                type: "Post",
                url: "/Backend_Administration/ExtranetGetCityListFromStarHealth",
                data: '{pincode:' + JSON.stringify(pincode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        $("#ddlCityList").html(data);
                        if (cityid != "") {
                            $("#ddlCityList").val(cityid);
                            if (areaid != "") {
                                $.ajax({
                                    type: "Post",
                                    url: "/Backend_Administration/ExtranetGetAreaListFromStarHealth",
                                    data: '{pincode:' + JSON.stringify(pincode) + ',cityid:' + JSON.stringify(cityid) + '}',
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (data) {
                                        if (data !== null) {
                                            $("#ddlAreaList").html(data);
                                            $("#ddlAreaList").val(areaid);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }
}
$("#ddlCityList").change(function () {
    let cityid = $("#ddlCityList option:selected").val();
    if (cityid != "" && cityid != "0") {
        $("#ddlAreaList").html("").html("<option value='0'>processing... <i class='fa fa-spinner fa-pulse'></i></option>");
        let pincode = $("#txtPPincode").val();
        $.ajax({
            type: "Post",
            url: "/Backend_Administration/ExtranetGetAreaListFromStarHealth",
            data: '{pincode:' + JSON.stringify(pincode) + ',cityid:' + JSON.stringify(cityid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlAreaList").html(data);
                }
            }
        });
    }
    else {
        $("#ddlAreaList").html("").html("<option value='0'>Select Area</option>");
    }
});

CalculateAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notpdobvalid").html("<span style='position: absolute;top: 10px;right: 15px;'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notpdobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

$("#section2NextBtn").click(function () {
    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if (CheckFocusBlankValidation("txtPEmail")) return !1;
    if (CheckFocusBlankValidation("txtPAddress1")) return !1;
    if (CheckFocusBlankValidation("txtPAddress2")) return !1;
    if (CheckFocusBlankValidation("txtPPincode")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlCityList")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlAreaList")) return !1;
    if (CheckFocusBlankValidation("txtPAnnualIncome")) return !1;
    let selectedamount = $("#txtTotalPremium").val();
    if (parseFloat(selectedamount) > 49000) {
        if (CheckFocusBlankValidation("txtPPan")) return !1;
    }

    let isproposerinsured = ($("#chkIsProposerInsured").prop("checked") === true ? 1 : 0);
    let ispropnominee = ($("#chkIsPropNominee").prop("checked") === true ? 1 : 0);
    BindInsuredDetails(isproposerinsured, ispropnominee);



    $("#fdproposerdetails").addClass("hidden");
    $("#fdInsuredDetails").removeClass("hidden");
});
//----------------------------------------------------------------------------------------------
//-------------------INTERMEDIATE SECTION (Between Proposer and Insured/Nominee)----------------
//----------------------------------------------------------------------------------------------

let totalperson = 0;
function BindInsuredDetails(isproposerinsured, ispropnominee) {

    totalperson = InsuredPersonsCount;
    let insuredHtml = "";
    for (let i = 1; i <= totalperson; i++) {
        insuredHtml = insuredHtml + "<div class='row form-group' style='padding:10px; border: 1px dotted #ccc;border-radius: 5px;'>" +
            "<input type='hidden' id='hdnInsuredId_" + i + "' value='0'>" +

            " <div class='col-sm-7'>  " +
            " <div class='row'>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <select class='form-control formcontlbord' name='ddlRelation_" + i + "' id='ddlRelation_" + i + "' onchange='InsuredTitleChange(" + i + ");'>  " +
            " <option value=''>Relation</option>  " +
            " </select>  " +
            " </div>  " +
            " <div class='col-sm-3 form-validation'>  " +
            " <select name='ddlTitle_" + i + "' id='ddlTitle_" + i + "' class='form-control formcontlbord'>  " +
            " <option value='' selected >Title</option>  " +
            " <option value='Mr.'>Mr.</option>  " +
            " <option value='Mrs.'>Mrs.</option>  " +
            " <option value='Ms.'>Ms.</option>  " +
            " </select>  " +
            " </div>  " +
            " <div class='col-sm-5 form-validation'>  " +
            " <input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='First Name*' value=''>  " +
            " </div>  " +
            " </div>  " +
            " </div>  " +

            " <div class='col-sm-5'>  " +
            " <div class='row'>  " +
            " <div class='col-sm-6 form-validation'>  " +
            " <input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='Last Name*' value=''>  " +
            " </div>  " +
            " <div class='col-sm-6 form-validation'>  " +
            " <input type='text' class='form-control formcontlbord DynamicCommanDateWithYr hasDatepicker' id='txtInDOB_" + i + "' autocomplete='off' placeholder='Date Of Birth*' value='' onchange='return CalculateNomineeAge(&quot;txtInDOB_" + i + "&quot;,&quot;notndobvalid" + i + "&quot;,&quot;position: absolute;top: 40px;right: 15px;font-size: 10px;&quot;)' size='10'><span class='notndobvalid" + i + "'></span>  " +
            " </div>  " +
            " </div>  " +
            " </div>  " +



            " <div class='col-sm-12'>  " +
            " <div class='row' style='margin-top: 12px;'>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <input onkeypress='return isNumberValidationPrevent(event);' type='text' class='form-control formcontlbord' id='txtInHeight_" + i + "' placeholder='height (cm)*' maxlength='3' minlength='1'><span class='cmposion'>CM</span>  " +
            " </div>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <input onkeypress='return isNumberValidationPrevent(event);' name='txtInWeight_" + i + "' id='txtInWeight_" + i + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)*' maxlength='3'><span class='cmposion'>KG</span>  " +
            " </div>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <select class='form-control formcontlbord' name='ddlOccupation_" + i + "' id='ddlOccupation_" + i + "'>  " +
            " <option value=''>Select Occupation</option>  " +
            " </select>  " +
            " </div>  " +
            " </div>  " +
            " </div  " +

            " <div class='col-sm-12'>  " +
            " <div class='row' style='margin-top: 12px;'>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <select class='form-control formcontlbord' name='ddlIllness_" + i + "' id='ddlIllness_" + i + "' onchange='ExistingIllnessFn(" + i + ")'>  " +
            " <option value=''>Existing Illness</option>  " +
            " <option value='true'>Yes</option>  " +
            " <option value='false'>No</option>  " +
            " </select>  " +
            " </div>  " +
            " <div class='col-sm-8 form-validation ExistingIllness_" + i + "''>  " +
            " <input type='text' readonly class='form-control formcontlbord' id='txtExistingIllness_" + i + "' placeholder='Describe Existing Illness'>  " +
            " </div>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <select class='form-control formcontlbord' disabled name='ddlEngageManualLabour_" + i + "' id='ddlEngageManualLabour_" + i + "' onchange='EngageManualLabourFn(" + i + ")'>  " +
            " <option value=''>Is Engage Manual Labour</option>  " +
            " <option value='true'>Yes</option>  " +
            " <option value='false'>No</option>  " +
            " </select>  " +
            " </div>  " +
            " <div class='col-sm-8 form-validation EngageManualLabour_" + i + "'>  " +
            " <input type='text' readonly class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour'>  " +
            " </div>  " +
            " <div class='col-sm-4 form-validation'>  " +
            " <select class='form-control formcontlbord' disabled name='ddlEngageWinterSports_" + i + "' id='ddlEngageWinterSports_" + i + "' onchange='EngageWinterSportsFn(" + i + ")'>  " +
            " <option value=''>Is Engage Winter Sports</option>  " +
            " <option value='true'>Yes</option>  " +
            " <option value='false'>No</option>  " +
            " </select>  " +
            " </div>  " +
            " <div class='col-sm-8 form-validation EngageWinterSports_" + i + "'>  " +
            " <input type='text' readonly class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports'>  " +
            " </div>  " +
            " </div>  " +
            " </div>  ";
    }

    $("#insuredDetailsDIV").html(insuredHtml);

    BindRelationships();
    BindOccupations();
    if (isproposerinsured) {

        $("#ddlRelation_1 option").each(function (i, e) {
            if (e.text.toLowerCase().indexOf("self") !== -1) {
                return $('#ddlRelation_1 :nth-child(' + (i + 1) + ')').prop('selected', true);
            }
        });
        $("#ddlRelation_1").prop("disabled", true);

        $("#ddlTitle_1").val($("#ddlPTitle").val());
        $("#ddlTitle_1").prop("disabled", true);

        $("#txtInFirstName_1").val($("#txtPFirstName").val());
        $("#txtInFirstName_1").prop("readonly", true);

        $("#txtInLastName_1").val($("#txtPLastName").val());
        $("#txtInLastName_1").prop("readonly", true);

        $("#txtInDOB_1").val($("#txtPDOB").val());
        $("#txtInDOB_1").prop("readonly", true);

    } else {
        $("#ddlRelation_1").val("");
        $("#ddlRelation_1").prop("disabled", false);

        $("#ddlTitle_1").val("");
        $("#ddlTitle_1").prop("disabled", false);

        $("#txtInFirstName_1").val("");
        $("#txtInFirstName_1").prop("readonly", false);

        $("#txtInLastName_1").val("");
        $("#txtInLastName_1").prop("readonly", false);

        $("#txtInDOB_1").val("");
        $("#txtInDOB_1").prop("readonly", false);
    }
    if (ispropnominee) {

        $("#ddlnRelation option").each(function (i, e) {
            if (e.text.toLowerCase().indexOf("self") !== -1) {
                return $('#ddlnRelation :nth-child(' + (i + 1) + ')').prop('selected', true);
            }
        });
        $("#ddlnRelation").prop("disabled", true);

        $("#ddlnTitle").val($("#ddlPTitle").val());
        $("#ddlnTitle").prop("disabled", true);

        $("#nFirstName").val($("#txtPFirstName").val());
        $("#nFirstName").prop("readonly", true);

        $("#nLastName").val($("#txtPLastName").val());
        $("#nLastName").prop("readonly", true);

        $("#nDOB").val($("#txtPDOB").val());
        $("#nDOB").prop("readonly", true);


    } else {
        $("#ddlnRelation").val("");
        $("#ddlnRelation").prop("disabled", false);

        $("#ddlnTitle").val("");
        $("#ddlnTitle").prop("disabled", false);

        $("#nFirstName").val("");
        $("#nFirstName").prop("readonly", false);

        $("#nLastName").val("");
        $("#nLastName").prop("readonly", false);

        $("#nDOB").val("");
        $("#nDOB").prop("readonly", false);
    }

    //$('.DynamicCommanDateWithYr').datepicker({
    //    dateFormat: "dd/mm/yy",
    //    showStatus: true,
    //    showWeeks: true,
    //    currentText: 'Now',
    //    changeMonth: true,
    //    changeYear: true,
    //    autoSize: true,
    //    maxDate: -0,
    //    gotoCurrent: true,
    //    showAnim: 'blind',
    //    highlightWeek: true,
    //    yearRange: '1950:' + (new Date).getFullYear()
    //});

    //CalculateNomineeAge($("#nDOB"), 'ndobvalid');
}

//--------------------------------------Insured/Nominee  Details---------------------------------------
$("#section3PreviousBtn").click(function () {
    $("#fdproposerdetails").removeClass("hidden");
    $("#fdInsuredDetails").addClass("hidden");
});

function BindRelationships() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetGetRelation",
        data: '{compid:' + JSON.stringify($("#ddlInsurer option:selected").data('compid')) + ',prodId:' + JSON.stringify($("#ddlProduct option:selected").data('productid')) + ',policyType:' + JSON.stringify($("#ddlPolicyType option:selected").val()) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            for (let i = 1; i <= totalperson; i++) {
                $("#ddlRelation_" + i).html(data);
            }
            $("#ddlnRelation").html(data);
        }
    });
}
function BindOccupations() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetGetOccupation",
        data: '{compid:' + JSON.stringify($("#ddlInsurer option:selected").data('compid')) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            for (let i = 1; i <= totalperson; i++) {
                $("#ddlOccupation_" + i).html(data);
            }
        }
    });
}

function ExistingIllnessFn(index) {
    if ($("#ddlIllness_" + index + " option:selected").val() === 'true') {
        $("#txtExistingIllness_" + index).removeAttr("readonly");
        $("#ddlEngageManualLabour_" + index).prop("disabled", false);
    }
    else {
        $("#txtExistingIllness_" + index).prop("readonly", true);
        $("#txtExistingIllness_" + index).val("");

        $("#ddlEngageManualLabour_" + index).val("");
        $("#ddlEngageManualLabour_" + index).prop("disabled", true);

        disableManualLabour(index);
    }
}
function EngageManualLabourFn(index) {
    if ($("#ddlEngageManualLabour_" + index + " option:selected").val() === 'true') {
        $("#txtEngageManualLabour_" + index).removeAttr("readonly");
        $("#ddlEngageWinterSports_" + index).prop("disabled", false);
    }
    else {
        disableManualLabour(index);
    }
}
function EngageWinterSportsFn(index) {
    if ($("#ddlEngageWinterSports_" + index + " option:selected").val() === 'true') {
        $("#txtEngageWinterSports_" + index).removeAttr("readonly");
    }
    else {
        disableWinterSport(index);
    }
}

function disableManualLabour(index) {
    $("#txtEngageManualLabour_" + index).prop("readonly", true);
    $("#txtEngageManualLabour_" + index).val("");
    $("#ddlEngageWinterSports_" + index).val("");
    $("#ddlEngageWinterSports_" + index).prop("disabled", true);
    disableWinterSport(index);
}
function disableWinterSport(index) {
    $("#txtEngageWinterSports_" + index).val("");
    $("#txtEngageWinterSports_" + index).prop("readonly", true);
}

InsuredTitleChange = (currIndex) => {
    let currTitle = $("#ddlRelation_" + currIndex + " option:selected").data("title");
    $("#ddlTitle_" + currIndex).val(currTitle);
}
CalculateNomineeAge = (fid, errorclass, errorstyle) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

$("#section3NextBtn").click(function () {

    for (let i = 1; i <= totalperson; i++) {
        if (CheckFocusDropDownBlankValidation("ddlRelation_" + i)) return !1;
        if (CheckFocusDropDownBlankValidation("ddlTitle_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInFirstName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInLastName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInDOB_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInHeight_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInWeight_" + i)) return !1;
        if (CheckFocusDropDownBlankValidation("ddlOccupation_" + i)) return !1;
        if (CheckFocusDropDownBlankValidation("ddlIllness_" + i)) return !1;

        if ($("#ddlIllness_" + i + " option:selected").val() === "true") {
            if (CheckFocusBlankValidation("txtExistingIllness_" + i)) return !1;            
        }
        if ($("#ddlEngageManualLabour_" + i + " option:selected").val() === "true") {
            if (CheckFocusBlankValidation("txtEngageManualLabour_" + i)) return !1;            
        }
        if ($("#ddlEngageWinterSports_" + i + " option:selected").val() === "true") {
            if (CheckFocusBlankValidation("txtEngageWinterSports_" + i)) return !1;
        }        
    }

    if (CheckFocusDropDownBlankValidation("ddlnRelation")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlnTitle")) return !1;
    if (CheckFocusBlankValidation("nFirstName")) return !1;
    if (CheckFocusBlankValidation("nLastName")) return !1;
    if (CheckFocusBlankValidation("nDOB")) return !1;    

    $("#fdInsuredDetails").addClass("hidden");
    $("#fdMedicalHistory").removeClass("hidden");
});


//--------------------------------------Medical Details------------------------------------------------
$("#section4PreviousBtn").click(function () {
    $("#fdInsuredDetails").removeClass("hidden");
    $("#fdMedicalHistory").addClass("hidden");
});

$("#section4NextBtn").click(function () {
    SaveDetails();
    alert("End of Form")
});
//-----------------------------------------------------------------------------------------------------


function SaveDetails() {

    var enquiryDetails = {};
    enquiryDetails.Policy_Type = $("#ddlPolicyType option:selected").val();
    enquiryDetails.Policy_Type_Text = $("#ddlPolicyType option:selected").text();
    enquiryDetails.Sum_Insured = $("#txtSumInsured").val();
    enquiryDetails.Self = $('#CheSelf').prop('checked');
    enquiryDetails.SelfAge = $("#txtSelfAge").val();
    enquiryDetails.Spouse = $('#CheSpouse').prop('checked');
    enquiryDetails.SpouseAge = $("#txtSpouseAge").val();
    enquiryDetails.Son = $('#chkSon').prop('checked');
    enquiryDetails.Son1Age = $("#txtSon1Age").val();
    enquiryDetails.Son2Age = $("#txtSon2Age").val();
    enquiryDetails.Son3Age = $("#txtSon3Age").val();
    enquiryDetails.Son4Age = $("#txtSon4Age").val();
    enquiryDetails.Daughter = $('#chkDaughter').prop('checked');
    enquiryDetails.Daughter1Age = $("#txtDaughter1Age").val();
    enquiryDetails.Daughter2Age = $("#txtDaughter2Age").val();
    enquiryDetails.Daughter3Age = $("#txtDaughter3Age").val();
    enquiryDetails.Daughter4Age = $("#txtDaughter4Age").val();
    enquiryDetails.Father = $('#CheFather').prop('checked');
    enquiryDetails.FatherAge = $("#txtFatherAge").val();
    enquiryDetails.Mother = $('#CheMother').prop('checked');
    enquiryDetails.MotherAge = $("#txtMotherAge").val();
    enquiryDetails.Gender = ""; //$("#ddlGender option:selected").val();
    enquiryDetails.First_Name = ""; //$("#txtFirstName").val();
    enquiryDetails.Last_Name = ""; //$("#txtLastName").val();
    enquiryDetails.Email_Id = ""; //$("#txtEmailId").val();
    enquiryDetails.Mobile_No = ""; // $("#txtMobileNo").val();
    enquiryDetails.Pin_Code = $("#txtPinCode").val();
    enquiryDetails.Is_Term_Accepted = true; //$('#chkHearBy').prop('checked');
    enquiryDetails.Enquiry_Id = GetParameterValues('enquiry_id');
    enquiryDetails.EnquiryBookingType = "offline";


    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    proposal.title = $("#ddlPTitle option:selected").val();
    proposal.gender = proposal.title === "mr." ? "Male" : "FeMale";
    proposal.firstname = $("#txtPFirstName").val();
    proposal.lastname = $("#txtPLastName").val();
    proposal.mobile = $("#txtPMobile").val();
    proposal.emailid = $("#txtPEmail").val();
    proposal.pincode = $("#txtPPincode").val();

    proposal.dob = $("#txtPDOB").val();
    proposal.address1 = $("#txtPAddress1").val();
    proposal.address2 = $("#txtPAddress2").val();
    proposal.landmark = $("#txtPLandmark").val();
    proposal.statename = "";
    proposal.cityid = $("#ddlCityList option:selected").val();
    proposal.cityname = $("#ddlCityList option:selected").text();
    proposal.areaid = $("#ddlAreaList option:selected").val();
    proposal.areaname = $("#ddlAreaList option:selected").text();
    proposal.isproposerinsured = ($("#chkIsProposerInsured").prop("checked") === true ? 1 : 0);
    proposal.ispropnominee = ($("#chkIsPropNominee").prop("checked") === true ? 1 : 0);
    proposal.criticalillness = 0;
    proposal.panno = $("#txtPPan").val();
    proposal.aadharno = $("#txtPAadhar").val();
    proposal.annualincome = $("#txtPAnnualIncome").val();

    proposal.productid = $("#ddlProduct option:selected").data("productid");
    proposal.companyid = $("#ddlInsurer option:selected").data("compid");
    proposal.suminsured = $("#txtSumInsured").val();

    proposal.row_premium = $("#txtBasic").val();
    proposal.row_basePremium = $("#txtBasic").val();
    proposal.row_serviceTax = $("#txtGST").val();
    proposal.row_totalPremium = $("#txtTotalPremium").val();
    proposal.row_discountPercent = 0;
    proposal.row_insuranceCompany = $("#ddlInsurer option:selected").val();
    proposal.row_policyType = $("#ddlPolicyType option:selected").val();
    proposal.row_planname = $("#ddlPlanType option:selected").val();
    proposal.row_policyTypeName = $("#ddlProduct option:selected").val();
    proposal.row_policyName = $("#ddlProduct option:selected").val();


    proposal.nRelation = $("#ddlnRelation option:selected").val();
    proposal.nTitle = $("#ddlnTitle option:selected").val();
    proposal.nFirstName = $("#nFirstName").val();
    proposal.nLastName = $("#nLastName").val();
    proposal.nDOB = $("#nDOB").val();

    let nBdate = proposal.nDOB.split('/');
    nBdate = new Date(nBdate[2] + "-" + nBdate[1] + "-" + nBdate[0])
    proposal.nAge = Math.floor((new Date() - nBdate) / 31557600000);

    var insuredlist = [];

    for (var i = 1; i <= parseInt(totalperson); i++) {
        let insured = {};

        insured.id = $("#hdnInsuredId_" + i).val();
        insured.relationid = $("#ddlRelation_" + i + " option:selected").val();
        insured.relationname = $("#ddlRelation_" + i + " option:selected").text();
        insured.title = $("#ddlTitle_" + i + " option:selected").val();
        insured.firstname = $("#txtInFirstName_" + i).val();
        insured.lastname = $("#txtInLastName_" + i).val();
        insured.dob = $("#txtInDOB_" + i).val();
        insured.height = $("#txtInHeight_" + i).val();
        insured.weight = $("#txtInWeight_" + i).val();
        insured.occupationid = $("#ddlOccupation_" + i + " option:selected").val();
        insured.occupation = $("#ddlOccupation_" + i + " option:selected").html();

        insured.isillness = ($("#ddlIllness_" + i + " option:selected").val() === true ? true : false);
        insured.illnessdesc = $("#txtExistingIllness_" + i).val();

        insured.isengagemanuallabour = ($("#ddlEngageManualLabour_" + i + " option:selected").val() === true ? true : false);
        insured.engageManualLabourDesc = $("#txtEngageManualLabour_" + i).val();

        insured.isengagewintersports = ($("#ddlEngageWinterSports_" + i + " option:selected").val() === true ? true : false);
        insured.engageWinterSportDesc = $("#txtEngageWinterSports_" + i).val();

        insured.isinsured = ((i === 1 && $("#chkIsPropNominee").prop("checked") === true) ? true : false);

        insuredlist.push(insured);
    }

    proposal.InsuredDetails = insuredlist;
    


    if ($("#txtPinCode").hasClass("successpincode")) {
        $.ajax({
            type: "Post",
            url: "/Backend_Administration/ExtranetSaveDetails",
            data: '{enquiryDetails: ' + JSON.stringify(enquiryDetails) + ',proposal: ' + JSON.stringify(proposal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data === "success") {
                    alert("Success\nAll Details are Saved");
                }
                else {
                    alert("Failure");
                }
            }
        });
    }


}