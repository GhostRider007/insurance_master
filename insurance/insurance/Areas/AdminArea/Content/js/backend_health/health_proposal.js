﻿function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(function () {
    let enquiryid = GetParameterValues('enquiryid');
    GetproposalAllDetails(enquiryid);
});

GetproposalAllDetails = (enquiryid) => {
    if (enquiryid != "" && enquiryid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Health/Admin_GetproposalAllDetails",
            //data: '{enquiry_id:' + JSON.stringify(enquiryid) + ',productid: ' + JSON.stringify(productid) + ',companyid: ' + JSON.stringify(companyid) + ',suminsured: ' + JSON.stringify(suminsured) + '}',
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response[0] != "logout") {
                    setTimeout(function () {
                        //$("#Additional").html(response[0]);
                        $("#ProposerDetail").html(response[1]);
                        $("#Insured").html(response[2]);
                        $("#Medical").html(response[3]);
                        $("#Other").html(response[4]);
                        $("#divProductSummDetails").html(response[5]);
                        //$("#divPaymentSection").html(response[6]);
                        $("#lblProposalId").html("Enquiry Id : " + enquiryid.toUpperCase() + " &nbsp;/&nbsp; Proposal Id : " + response[6]);
                    }, 500);
                }
                else {
                    $("#divPerposalModal").html("").html("<div class='modal-header'><h5 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information - Unauthorized access</h5></div><div class='modal-body text-center' style='padding: 40px;'><h5 class='text-danger'>You not authorized to access this page or details.</h5></div><div class='modal-footer'><button type='button' class='btn btn-danger' id='btnUnauthorizedClose' onclick='UnauthorizedAccess();'>OK</button></div>");
                    $("#PStatusModal").modal('show');
                }
            }
        });
    }
}