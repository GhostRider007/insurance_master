﻿$(function () {
    //StarterEvent();
    BindData();
});

//const StarterEvent = () => {
//    BindGroupList();
//    BindAgentList();
//    BindCompanyList();
//}

const BindData = () => {
    let id = $("#comm_id").val();

    if (id > 0) {
        $.ajax({
            type: "Post",
            url: "/Backend_Commision/AddEditHealthComission_Get",
            data: '{id: ' + JSON.stringify(id) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data.length > 0) {

                    $("#comm_ddlProductName").html(data[1]);

                    $("#comm_id").val(data[0].id);
                    $("#comm_AgencyId").val(data[0].AgencyId);
                    $("#comm_ddlAgencyName").val(data[0].AgencyId);
                    $("#comm_GroupId").val(data[0].GroupId);
                    $("#comm_ddlGroupName").val(data[0].GroupId);
                    $("#comm_CompanyId").val(data[0].CompanyId);
                    $("#comm_ddlCompanyName").val(data[0].CompanyId);
                    $("#comm_ProductCode").val(data[0].ProductCode);
                    $("#comm_ddlProductName").val(data[0].ProductCode);
                    $("#comm_ddlCommissionType").val(data[0].CommissionType);
                    $("#comm_baseCommission").val(data[0].BaseCommssion);
                    $("#comm_TDS").val(data[0].TDS);
                }
            }
        });

    }
};

//const BindGroupList = () => {
//    $("#comm_ddlGroupName").html("<option value=\"-1\">Please Wait...</option>");
//    $.ajax({
//        type: "Post",
//        url: "/Backend_Commision/BindGroups",
//        async:false,
//        datatype: "json",
//        success: function (data) {
//            if (data != null) {
//                $("#comm_ddlGroupName").html(data);
//            }
//        }
//    });
//};
//const BindAgentList = () => {
//    $("#comm_ddlAgencyName").html("<option value=\"-1\">Please Wait...</option>");
//    $.ajax({
//        type: "Post",
//        url: "/Backend_Commision/BindAgency",
//        async: false,
//        datatype: "json",
//        success: function (data) {
//            if (data != null) {
//                $("#comm_ddlAgencyName").html(data);
//            }
//        }
//    });
//};
//const BindCompanyList = () => {
//    $("#comm_ddlCompanyName").html("<option value=\"-1\">Please Wait...</option>");
//    $.ajax({
//        type: "Post",
//        url: "/Backend_Commision/BindCompany",
//        async: false,
//        data: '{type: ' + JSON.stringify("health") + '}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (data) {
//            if (data != null) {
//                $("#comm_ddlCompanyName").html(data);
//            }
//        }
//    });
//};

$("#comm_ddlAgencyName").change(function () {
    $("#comm_AgencyId").val($("#comm_ddlAgencyName option:selected").val());
    $("#comm_GroupId").val(0);
    $("#comm_ddlGroupName").val(0);

});
$("#comm_ddlGroupName").change(function () {
    $("#comm_GroupId").val($("#comm_ddlGroupName option:selected").val());
    $("#comm_AgencyId").val(0);
    $("#comm_ddlAgencyName").val(0);
});
$("#comm_ddlCompanyName").change(function () {

    let id = $("#comm_ddlCompanyName option:selected").val();
    $("#comm_CompanyId").val(id);

    $("#comm_ddlProductName").html("<option value=\"-1\">Please Wait...</option>");
    $.ajax({
        type: "Post",
        url: "/Backend_Commision/BindProduct",
        data: '{type: ' + JSON.stringify("health") + ',CompanyId: ' + JSON.stringify(id) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#comm_ddlProductName").html(data);
            }
        }
    });
});
$("#comm_ddlProductName").change(function () {
    $("#comm_ProductCode").val($("#comm_ddlProductName option:selected").val());
});

$("#comm_btnSubmit").click(function () {

    
    if ($("#comm_AgencyId").val() == 0 && $("#comm_GroupId").val() == 0) {
        alert("Agency and Group cannot be empty!!!\nPlease select either one of them.");
        return !1;
    } 
    if (CheckFocusDropDownBlankValidation("comm_ddlCompanyName")) return !1;
    if (CheckFocusDropDownBlankValidation("comm_ddlProductName")) return !1;
    if (CheckFocusDropDownBlankValidation("comm_ddlCommissionType")) return !1;
    if (CheckFocusBlankValidation("comm_baseCommission")) return !1;
    if (CheckFocusBlankValidation("comm_TDS")) return !1;

    model = {};
    model.id = $("#comm_id").val();
    model.AgencyId = $("#comm_AgencyId").val();
    model.AgencyName = (model.AgencyId == 0) ? null : $("#comm_ddlAgencyName option:selected").html();
    model.GroupId = $("#comm_GroupId").val();
    model.GroupName = (model.GroupId == 0) ? null : $("#comm_ddlGroupName option:selected").html();
    model.InsuranceType = 'health';
    model.CompanyId = $("#comm_CompanyId").val();
    model.CompanyName = $("#comm_ddlCompanyName option:selected").html();
    model.ProductCode = $("#comm_ProductCode").val();
    model.ProductName = $("#comm_ddlProductName option:selected").html();
    model.CommissionType = $("#comm_ddlCommissionType option:selected").val();
    model.BaseCommssion = $("#comm_baseCommission").val();
    model.TDS = $("#comm_TDS").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Commision/AddEditHealthComission_Post",
        data: '{model: ' + JSON.stringify(model) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data === true) {
                window.location.href = '/AdminArea/Backend_Commision/HealthComissionList';
            }
            else {
                alert("Error Occured. \nPlease try again or contact administrator");
            }
        }
    });


});