﻿
totalCount();
totalPremium();
policyGraph();
$("#txt_daysPolicy").change(function () {
    policyGraph();
    totalCount();
    totalPremium();
});
function policyGraph() {
    let timeDuration = $("#txt_daysPolicy option:selected").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Dashboard/policyGraph",
        data: '{duration:' + JSON.stringify(timeDuration) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {            
            $("#motorpolicycount").html(response[0]);
            $("#healthpolicycount").html(response[3]);

            let pmotor = parseInt(response[0]);
            let phealth = parseInt(response[3]);

            let policy_totalmotor = pmotor + phealth + 0 + 0;
            if (policy_totalmotor == 0) {
                PolicyChart(25, 25, 25, 25);
            }
            else {
                PolicyChart(((pmotor / policy_totalmotor) * 100).toFixed(2), ((phealth / policy_totalmotor) * 100).toFixed(2), 0, 0);
            }

            $("#moterproposalcount").html(response[2]);
            $("#helthproposalcount").html(response[17]);
            let pomotor = parseInt(response[2]);
            let pohealth = parseInt(response[17]);
            let proposal_totalmotor = pomotor + pohealth + 0 + 0;
            if (proposal_totalmotor == 0) {
                ProposalChart(25, 25, 25, 25);
            }
            else {
                ProposalChart(((pomotor / proposal_totalmotor) * 100).toFixed(2), ((pohealth / proposal_totalmotor) * 100).toFixed(2), 0, 0);
            }

            $("#healthleadcount").html(response[4]);
            $("#moterleadcount").html(response[5]);

            let polmotor = parseInt(response[4]);
            let polhealth = parseInt(response[5]);
            let Lead_totalmotor = polmotor + polhealth + 0 + 0;
            if (Lead_totalmotor == 0) {
                LeadChart(25, 25, 25, 25);
            }
            else {
                LeadChart(((polmotor / Lead_totalmotor) * 100).toFixed(2), ((polhealth / Lead_totalmotor) * 100).toFixed(2), 0, 0);
            }

            $("#Godigitpolicycount").html(response[6]);
            $("#Shrirampolicycount").html(response[7]);
            $("#Iffcotokiopolicycount").html(response[8]);

            $("#Godigitproposalcount").html(response[9]);
            $("#Shriramproposalcount").html(response[10]);
            $("#Iffcotokioproposalcount").html(response[11]);

            //$("#totalPolicyCount").html(response[12]);
            //$("#totalPerposalCount").html(response[13]);
            //$("#totalleadcount").html(response[14]);

            $("#MotorDelhibranchcount").html(response[16]);
            $("#MotorLudhianabranchcount").html(response[15]);

            let delhibranch = parseInt(response[16]);
            let Ludhianabranch = parseInt(response[15]);
            let totalbranch = delhibranch + Ludhianabranch;
            if (totalbranch == 0) {
                MoterpolicybranchChart(50, 50)
            }
            else {
                MoterpolicybranchChart(((delhibranch / totalbranch) * 100).toFixed(2), ((Ludhianabranch / totalbranch) * 100).toFixed(2));
            }

            let godigit = parseInt(response[6]);
            let shriram = parseInt(response[7]);
            let iffcotokio = parseInt(response[8]);
            let totalmotor = godigit + shriram + iffcotokio;

            if (totalmotor == 0) {
                init_chart_doughnut2(33.3, 33.3, 33.3);
            }
            else {
                init_chart_doughnut2(((godigit / totalmotor) * 100).toFixed(2), ((shriram / totalmotor) * 100).toFixed(2), ((iffcotokio / totalmotor) * 100).toFixed(2));
            }
            let pgodigit = parseInt(response[9]);
            let pshriram = parseInt(response[10]);
            let piffcotokio = parseInt(response[11]);
            let ptotalmotor = pgodigit + pshriram + piffcotokio;
            if (ptotalmotor == 0) {
                init_chart_doughnut3(33.3, 33.3, 33.3);
            }
            else {
                init_chart_doughnut3(((pgodigit / ptotalmotor) * 100).toFixed(2), ((pshriram / ptotalmotor) * 100).toFixed(2), ((piffcotokio / ptotalmotor) * 100).toFixed(2));
            }

            $("#Starpolicycount").html(response[18]);
            $("#Carepolicycount").html(response[19]);
            $("#ManipalCignapolicycount").html(response[20]);
            $("#Adityapolicycount").html(response[21]);

            let pStarpolicy = parseInt(response[18]);
            let pCarepolicy = parseInt(response[19]);
            let pManipalCignapolicy = parseInt(response[20]);
            let pAdityapolicy = parseInt(response[21]);
            let ptotalHealth = pStarpolicy + pCarepolicy + pManipalCignapolicy + pAdityapolicy;            
            if (ptotalHealth == 0) {
                HealthPolicyCompnyChart(25, 25, 25, 25);
            }
            else {
                HealthPolicyCompnyChart(((pStarpolicy / ptotalHealth) * 100).toFixed(2), ((pCarepolicy / ptotalHealth) * 100).toFixed(2), ((pManipalCignapolicy / ptotalHealth) * 100).toFixed(2),((pAdityapolicy / ptotalHealth) * 100).toFixed(2));
            }

            $("#Starpropcount").html(response[22]);
            $("#Carepropcount").html(response[23]);
            $("#ManipalCignapropcount").html(response[24]);
            $("#Adityapropcount").html(response[25]);
            let poStarpolicy = parseInt(response[22]);
            let poCarepolicy = parseInt(response[23]);
            let poManipalCignapolicy = parseInt(response[24]);
            let poAdityapolicy = parseInt(response[25]);
            let pototalHealth = poStarpolicy + poCarepolicy + poManipalCignapolicy + poAdityapolicy;
            if (pototalHealth == 0) {
                HealthPropsolCompnyChart(25, 25, 25, 25);
            }
            else {
                HealthPropsolCompnyChart(((poStarpolicy / pototalHealth) * 100).toFixed(2), ((poCarepolicy / pototalHealth) * 100).toFixed(2), ((poManipalCignapolicy / pototalHealth) * 100).toFixed(2), ((poAdityapolicy / pototalHealth) * 100).toFixed(2));
            }

            $("#DelhihBranchcount").html(response[26]);
            $("#LudhianahBranchcount").html(response[27]);

            let hdelhibranch = parseInt(response[26]);
            let hLudhianabranch = parseInt(response[27]);
            let htotalbranch = hdelhibranch + hLudhianabranch;
            if (htotalbranch == 0) {
                HealthpolicybranchChart(50, 50)
            }
            else {
                HealthpolicybranchChart(((hdelhibranch / htotalbranch) * 100).toFixed(2), ((hLudhianabranch / htotalbranch) * 100).toFixed(2));
            }
            totalCount();
        }
        
    });

    //$.ajax({
    //    type: "Post",
    //    url: "/Backend_Dashboard/policyGraphHealth",
    //    data: '{duration:' + JSON.stringify(timeDuration) + '}',
    //    async: false,
    //    contentType: "application/json; charset=utf-8",
    //    datatype: "json",
    //    success: function (data) {

    //        $("#healthleadcount").html(data[0]);
    //        $("#helthproposalcount").html(data[1]);
    //        $("#healthpolicycount").html(data[2]);
    //    }
    //});

}


function totalCount() {

    $("#totalPolicyCount").html(parseInt($("#healthpolicycount").html()) + parseInt($("#motorpolicycount").html()) + parseInt($("#lifepolicycount").html()) + parseInt($("#travelpolicycount").html()));
    $("#totalPerposalCount").html(parseInt($("#helthproposalcount").html()) + parseInt($("#moterproposalcount").html()) + parseInt($("#lifeproposalcount").html()) + parseInt($("#travelproposalcount").html()));
    $("#totalLeadcount").html(parseInt($("#healthleadcount").html()) + parseInt($("#moterleadcount").html()) + parseInt($("#lifeleadcount").html()) + parseInt($("#travelleadcount").html()));
    
}

function totalPremium() {

    let timeDuration = $("#txt_daysPolicy option:selected").val();
    $.ajax({
        type: "Post",
        url: "/Backend_Dashboard/GetTotalPremium",
        data: '{duration:' + JSON.stringify(timeDuration) + '}',
        async: false,
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#totalHealthPremium").html("₹ " + parseFloat(data[0]).toFixed(2));
            $("#totalMotorPremium").html("₹ " + parseFloat(data[1]).toFixed(2));
        }
    });
}


function PolicyChart(pmoter, phealth, plife, ptravel) {
    $(".canvasDoughnut").html("");
    if ("undefined" != typeof Chart && (console.log("PolicyChart"),
        $(".canvasDoughnut").length)) {
        var a = {
            type: "doughnut",
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: ["Health", "Motor", "Life", "Travel"],
                datasets: [{
                    data: [phealth, pmoter, plife, ptravel],
                    backgroundColor: ["#E74C3C", "#1ABB9C", "#9B59B6", "#9CC2CB"]
                }]
            }, options: { legend: !1, responsive: !1 }
        };
        $(".canvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function ProposalChart(pomoter, pohealth, polife, potravel) {
    $(".PropcanvasDoughnut").html("");
    if ("undefined" != typeof Chart && (console.log("ProposalChart"),
        $(".PropcanvasDoughnut").length)) {
        var a = {
            type: "doughnut",
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: ["Health", "Motor", "Life", "Travel"],
                datasets: [{
                    data: [pohealth, pomoter, polife, potravel],
                    backgroundColor: ["#E74C3C", "#1ABB9C", "#9B59B6", "#9CC2CB"]
                }]
            }, options: { legend: !1, responsive: !1 }
        };
        $(".PropcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function LeadChart(polmoter, polhealth, pollife, poltravel) {
    $(".LeadcanvasDoughnut").html("");
    if ("undefined" != typeof Chart && (console.log("LeadChart"),
        $(".LeadcanvasDoughnut").length)) {
        var a = {
            type: "doughnut",
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: ["Health", "Motor", "Life", "Travel"],
                datasets: [{
                    data: [polmoter, polhealth, pollife, poltravel],
                    backgroundColor: ["#E74C3C", "#1ABB9C", "#9B59B6", "#9CC2CB"]
                }]
            }, options: { legend: !1, responsive: !1 }
        };
        $(".LeadcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function init_chart_doughnut2(godigit, shriram, iffcotokio) {
    if ("undefined" != typeof Chart && (console.log("init_chart_doughnut2"),
        $(".comcanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["GODIGIT", "SHRIRAM", "IFFCOTOKIO"], datasets: [{ data: [godigit, shriram, iffcotokio], backgroundColor: ["#fdd619", "#000000", "#367b12"] }] }, options: { legend: !1, responsive: !1 } };
        $(".comcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function init_chart_doughnut3(pgodigit, pshriram, piffcotokio) {
    if ("undefined" != typeof Chart && (console.log("init_chart_doughnut3"),
        $(".pcomcanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["PGODIGIT", "PSHRIRAM", "PIFFCOTOKIO"], datasets: [{ data: [pgodigit, pshriram, piffcotokio], backgroundColor: ["#fdd619", "#000000", "#367b12"] }] }, options: { legend: !1, responsive: !1 } };
        $(".pcomcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function HealthPolicyCompnyChart(starHealth, careHealth, manipalCigna, adityaBirla) {
    if ("undefined" != typeof Chart && (console.log("HealthCompnyChart"),
        $(".healthcanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["Star Health", "Care Health", "ManipalCigna", "Aditya Birla Health Insurance"], datasets: [{ data: [starHealth, careHealth, manipalCigna, adityaBirla], backgroundColor: ["#5755cc", "#fdd619", "#ea7b09", "#ea0928"] }] }, options: { legend: !1, responsive: !1 } };
        $(".healthcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function HealthPropsolCompnyChart(starHealth, careHealth, manipalCigna, adityaBirla) {
    if ("undefined" != typeof Chart && (console.log("HealthpoCompnyChart"),
        $(".healthpocanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["Star Health", "Care Health", "ManipalCigna", "Aditya Birla Health Insurance"], datasets: [{ data: [starHealth, careHealth, manipalCigna, adityaBirla], backgroundColor: ["#5755cc", "#fdd619", "#ea7b09", "#ea0928"] }] }, options: { legend: !1, responsive: !1 } };
        $(".healthpocanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function MoterpolicybranchChart(delhi, Ludhiana) {
    if ("undefined" != typeof Chart && (console.log("MoterpolicybranchChart"),
        $(".motorbranchcanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["Delhi", "Ludhiana"], datasets: [{ data: [Ludhiana, delhi], backgroundColor: ["#5755cc", "#ea0928"] }] }, options: { legend: !1, responsive: !1 } };
        $(".motorbranchcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}

function HealthpolicybranchChart(delhi, Ludhiana) {
    if ("undefined" != typeof Chart && (console.log("healthbranchcanvasDoughnut"),
        $(".healthbranchcanvasDoughnut").length)) {
        var a = { type: "doughnut", tooltipFillColor: "rgba(51, 51, 51, 0.55)", data: { labels: ["Delhi", "Ludhiana"], datasets: [{ data: [Ludhiana, delhi], backgroundColor: ["#5755cc", "#ea0928"] }] }, options: { legend: !1, responsive: !1 } };
        $(".healthbranchcanvasDoughnut").each(function () { var e = $(this); new Chart(e, a) })
    }
}