﻿//$(function () {
//    if (localStorage.getItem("backloginid") == null) {
//        window.location.href = "/backend/login";
//    }
//});

function Logout() {
    $.ajax({
        type: "Post",
        url: "/Account/BackendLogOut",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data == "true") {
                    localStorage.removeItem("backloginid");
                    window.location.href = "/backend/login";
                }
            }
        }
    });
}