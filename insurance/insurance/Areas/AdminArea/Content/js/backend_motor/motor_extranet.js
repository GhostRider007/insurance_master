﻿//----------------------------------VARIABLE SECTION-------------------------------------
let enquiry_id = '';
let sec4agevalidation = 0;
//-------------------------SECTION 1------------------------------------------
$(function () {
    enquiry_id = GetParameterValues('enquiry_id');
    if (enquiry_id == null || enquiry_id == '') { GenrateEnquiry_Id(); }
    BindInsurers();
    BindRTOs();

});

function GenrateEnquiry_Id() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetGenrateMotorEnquiryId",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                window.location.href = window.location.href + response;
            }
        }
    });
}

PincodeCheckAndGetDetails = () => {
    $(".notpincodevalid").addClass("hidden").html("");
    let txtPinCodeVal = $("#txtPincode").val();

    if (txtPinCodeVal.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPincode").removeClass("successpincode").addClass("errorpincode");
        //$("#txtPState").val("");
        //$("#txtPCity").val("");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/CheckAndGetDetails",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 0) {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response[1]);
                    $("#txtPincode").addClass("successpincode").removeClass("errorpincode");

                    $("#txtPState").val(response[0]);
                    $("#txtPCity").val(response[1]);
                    $("#txtPPincode").val(txtPinCodeVal);
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $("#txtPincode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

$("#ddlVehicleType").change(BindInsurers);

function BindInsurers() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/GetExtranetBrandDetails_Motor",
        data: '{vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlbrand").html('');
            $("#ddlbrand").html(data);
        }
    });
}

$("#ddlbrand").change(BindModels);

function BindModels() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/GetExtranetModelDetails_Motor",
        data: '{vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + ',brand:' + JSON.stringify($("#ddlbrand option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#ddlModel").html(data);
        }
    });
}

$("#ddlModel").change(BindFuels);
function BindFuels() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/GetExtranetFuelDetails_Motor",
        data: '{vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + ',brand:' + JSON.stringify($("#ddlbrand option:selected").val()) + ',model:' + JSON.stringify($("#ddlModel option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#ddlFuel").html(data);
        }
    });
}

$("#ddlFuel").change(BindVarients);
function BindVarients() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/GetExtranetVarientDetails_Motor",
        data: '{vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + ',brand:' + JSON.stringify($("#ddlbrand option:selected").val()) + ',model:' + JSON.stringify($("#ddlModel option:selected").val()) + ',fuel:' + JSON.stringify($("#ddlFuel option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#ddlVarient").html(data);
        }
    });
}

function BindRTOs() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/GetExtranetRTOList_Motor",
        async: false,
        datatype: "json",
        success: function (data) {
            $("#ddlRTO").html(data);
        }
    });
}
$("#ddlRTO").change(function () {
    $("#txtRegNo1").val($(this).val());
});

$("#section1Btn").click(function () {

    if (CheckFocusBlankValidation("txtPincode")) return !1;
    if (CheckFocusBlankValidation("txtMobile")) return !1;
    if (CheckFocusBlankValidation("txtRegDate")) return !1;
    if (CheckFocusBlankValidation("txtMfgDate")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlRTO")) return !1;
    if (CheckFocusBlankValidation("txtRegNo2")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlbrand")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlModel")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlFuel")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlVarient")) return !1;

    $("#regNo_DetailSection").html($("#txtRegNo1").val() + "-" + $("#txtRegNo2").val().toLocaleUpperCase());
    $("#vehicle_DetailSection").html($("#ddlbrand option:selected").val() + " " + $("#ddlModel option:selected").val() + " " + $("#ddlVarient option:selected").val());
    $("#fuel_DetailSection").html($("#ddlFuel option:selected").val());
    $("#plantype_DetailSection").html($("#ddlInsuranceType option:selected").val());
    if ($("#ddlVehicleType option:selected").val() == 'bike') {
        $("#logo_img").attr("src", "https://seeinsured.com/Content/images/bike-final.gif");
    } else {
        $("#logo_img").attr("src", "https://seeinsured.com/Content/images/car-new.gif");
    }
    $("#section1").addClass("hidden");
    $("#section2").removeClass("hidden");

    section2StarterEvent();

});

//-------------------------SECTION-2--------------------------------

$("#section2PreviousBtn").click(function () {
    $("#section2").addClass("hidden");
    $("#section1").removeClass("hidden");

    $(".linext2").removeClass("active");
});

function section2StarterEvent() {
    $(".linext2").addClass("active")

    BindPreviousPolicyType();
    BindInsurer();
}

function BindPreviousPolicyType() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindPreviousPolicyType_Motor",
        data: '{insurancetype:' + JSON.stringify($("#ddlInsuranceType option:selected").val()) + ',vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlPrevPolicyType").html(data);
        }
    });
}

$("#ddlIsPrevInsurerKnown").change(function () {
    if ($(this).val() == 'yes') {
        $("#previousInsuranceDetails").removeClass("hidden");

    }
    else {
        $("#previousInsuranceDetails").addClass("hidden");
    }
});

$("#ddlClaimInLastYear").change(function () {

    if ($(this).val() === 'no') {

        $.ajax({
            type: "Post",
            url: "/Backend_Administration/GetExtranetNCBPercentage_Motor",
            datatype: "json",
            success: function (data) {
                $("#ddlClaimedNCB").html(data);
            }
        });

        $("#ddlClaimedNCB").prop("disabled", false);
    }
    else {
        $("#ddlClaimedNCB").html("<option value=\"\">----</option>");
        $("#ddlClaimedNCB").prop("disabled", true);
    }


});


function BindInsurer() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindInsurers_Motor",
        datatype: "json",
        success: function (data) {
            $("#ddlInsurers").html(data);
        }
    });
}

$("#ddlInsurers").change(BindProduct);

function BindProduct() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindProducts_Motor",
        data: '{insuranceId:' + JSON.stringify($("#ddlInsurers option:selected").data('compid')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlInsuranceProducts").html(data);
        }
    });
}

$("#txtSumInsured").change(function () {
    let amt = $(this).val();
    $(this).val(parseFloat(amt).toFixed(2));
});

$("#txtBasicAmt").change(function () {
    let amt = $(this).val();
    $(this).val(parseFloat(amt).toFixed(2));
    $("#txtGSTAmt").val((amt * 0.18).toFixed(2));
    $("#txtTotalAmt").val((parseFloat(amt) + parseFloat(amt * 0.18)).toFixed(2));
});

$("#section2NextBtn").click(function () {

    if (CheckFocusDropDownBlankValidation("ddlPrevPolicyType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlIsPrevInsurerKnown")) return !1;

    if ($("#ddlIsPrevInsurerKnown option:selected").val() == 'yes') {
        if (CheckFocusBlankValidation("txtpolicyNo")) return !1;
        if (CheckFocusBlankValidation("txtPolicyExpDate")) return !1;
        if (CheckFocusDropDownBlankValidation("ddlClaimInLastYear")) return !1;

        if ($("#ddlClaimInLastYear option:selected").val() == 'no') {
            if (CheckFocusDropDownBlankValidation("ddlClaimedNCB")) return !1;
        }
    }

    if (CheckFocusDropDownBlankValidation("ddlInsurers")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlInsuranceProducts")) return !1;
    if (CheckFocusBlankValidation("txtSumInsured")) return !1;
    if (CheckFocusBlankValidation("txtBasicAmt")) return !1;
    if (CheckFocusBlankValidation("txtGSTAmt")) return !1;
    if (CheckFocusBlankValidation("txtTotalAmt")) return !1;

    $("#insurer_DetailSection").html($("#ddlInsurers option:selected").val());
    $("#product_DetailSection").html($("#ddlInsuranceProducts option:selected").val());
    $("#sumInsured_DetailSection").html("₹ " + $("#txtSumInsured").val());

    $("#basic_DetailSection").html("₹ " + $("#txtBasicAmt").val());
    $("#gst_DetailSection").html("₹ " + $("#txtGSTAmt").val());
    $("#premium_DetailSection").html("₹ " + $("#txtTotalAmt").val());

    $("#companyLogo_img").attr("src", $("#ddlInsurers option:selected").data('imgurl'));
    $("#companyLogo_img").css("display", "block");

    $("#section2").addClass("hidden");
    $("#section3").removeClass("hidden");

    section3StarterEvent();
});


//-------------------------SECTION-3--------------------------------
$("#section3PreviousBtn").click(function () {
    $("#section3").addClass("hidden");
    $("#section2").removeClass("hidden");
    $(".linext3").removeClass("active");
});

function section3StarterEvent() {
    $(".linext3").addClass("active");
}

$("#section3NextBtn").click(function () {

    $("#section3").addClass("hidden");
    $("#section4").removeClass("hidden");

    section4StarterEvent();
});

//-------------------------SECTION-4--------------------------------
$("#section4PreviousBtn").click(function () {
    $("#section4").addClass("hidden");
    $("#section3").removeClass("hidden");
    $(".linext4").removeClass("active");
});

function section4StarterEvent() {
    $(".linext4").addClass("active");

    $("#txtPMobile").val($("#txtMobile").val());
    BindRelation();
}

function BindRelation() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindRelation_Motor",
        data: '{chainid:' + JSON.stringify($("#ddlInsurers option:selected").data('compid')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlnRelation").html(data);
        }
    });
}

CalculateAge = (fid, el) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("#" + el).html("<span style='position: absolute;top: 10px;right: 15px;'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("#" + el).html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");
        }
    }
}

$("#section4NextBtn").click(function () {
    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if ($("#txtPDOB").hasClass("errorpage")) return !1;
    if (CheckFocusBlankValidation("txtPEmail")) return !1;
    if (CheckFocusBlankValidation("txtPAddress1")) return !1;
    if (CheckFocusBlankValidation("txtPAddress2")) return !1;
    if (CheckFocusBlankValidation("txtPPincode")) return !1;
    if (CheckFocusBlankValidation("txtPCity")) return !1;
    if (CheckFocusBlankValidation("txtPState")) return !1;

    if (CheckFocusDropDownBlankValidation("ddlnRelation")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlnTitle")) return !1;
    if (CheckFocusBlankValidation("txtnFirstName")) return !1;
    if (CheckFocusBlankValidation("txtnLastName")) return !1;
    if (CheckFocusBlankValidation("txtnDOB")) return !1;
    if ($("#txtnDOB").hasClass("errorpage")) return !1;

    $("#section4").addClass("hidden");
    $("#section5").removeClass("hidden");

    section5StarterEvent();
});

//-------------------------SECTION-5--------------------------------
$("#section5PreviousBtn").click(function () {
    $("#section5").addClass("hidden");
    $("#section4").removeClass("hidden");
    $(".linext5").removeClass("active");
});

function section5StarterEvent() {
    $(".linext5").addClass("active");
    BindVoluntaryDeductible();
    BindPreviousInsured();
}

function BindVoluntaryDeductible() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindVolunatryDeductible_Motor",
        data: '{chainid:' + JSON.stringify($("#ddlInsurers option:selected").data('compid')) + ',vehicleType:' + JSON.stringify($("#ddlVehicleType option:selected").val()) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlVoluntaryDeductible").html(data);
        }
    });
}
function BindPreviousInsured() {
    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetBindPreviousInsured_Motor",
        data: '{chainid:' + JSON.stringify($("#ddlInsurers option:selected").data('compid')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            $("#ddlPreviousInsured").html(data);
        }
    });
}

var enquiryDetails = {}; let proposal = {};

$("#finalSubmit").click(function () {

    if (CheckFocusBlankValidation("txtEngineNo")) return !1;
    if (CheckFocusBlankValidation("txtChasisNo")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlPolicyHolderType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlVoluntaryDeductible")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlPreviousInsured")) return !1;
    if (parseFloat($("#txtSumInsured").val()) > 49000) {
        if (CheckFocusBlankValidation("txtGSTNo")) return !1;
        if (CheckFocusBlankValidation("txtPanNo")) return !1;
    }

    enquiryDetails.Enquiry_Id = enquiry_id;
    enquiryDetails.VechileRegNo = $("#txtRegNo1").val().toLocaleUpperCase() + $("#txtRegNo2").val().toLocaleUpperCase();
    enquiryDetails.VechileType = $("#ddlVehicleType option:selected").val();
    enquiryDetails.MobileNo = $("#txtMobile").val();
    enquiryDetails.Is_Term_Accepted = true;
    enquiryDetails.RtoId = $("#ddlRTO option:selected").val();
    enquiryDetails.RtoName = $("#ddlRTO option:selected").html();
    enquiryDetails.DateOfReg = $("#txtRegDate").val();
    enquiryDetails.DateOfMfg = $("#txtMfgDate").val();
    enquiryDetails.BrandId = $("#ddlbrand option:selected").val();
    enquiryDetails.BrandName = $("#ddlbrand option:selected").val();
    enquiryDetails.ModelId = $("#ddlModel option:selected").val();
    enquiryDetails.ModelName = $("#ddlModel option:selected").val();
    enquiryDetails.Fuel = $("#ddlFuel option:selected").val();
    enquiryDetails.VarientId = $("#ddlVarient option:selected").val();
    enquiryDetails.VarientName = $("#ddlVarient option:selected").val();
    enquiryDetails.PinCode = parseInt($("#txtPincode").val());
    enquiryDetails.ispreviousinsurer = $("#ddlIsPrevInsurerKnown option:selected").val();
    enquiryDetails.insurerid = $("#ddlInsurers option:selected").data('compid').toString();
    enquiryDetails.insurername = $("#ddlInsurers option:selected").val();
    enquiryDetails.policynumber = $("#txtpolicyNo").val();
    enquiryDetails.policyexpirydate = $("#txtPolicyExpDate").val();
    enquiryDetails.isclaiminlastyear = $("#ddlClaimInLastYear option:selected").val();
    enquiryDetails.previousyearnoclaim = $("#ddlClaimedNCB option:selected").val();
    enquiryDetails.originalpreviouspolicytype = $("#ddlPrevPolicyType option:selected").val();
    enquiryDetails.insurancetype = $("#ddlInsuranceType option:selected").val();
    enquiryDetails.EnquiryBookingType = 'offline';


    proposal.enquiry_id = enquiry_id;
    proposal.title = $("#ddlPTitle option:selected").val();
    proposal.gender = $("#ddlPTitle option:selected").val() == 'Mr.' ? 'MALE' : 'FEMALE';
    proposal.firstname = $("#txtPFirstName").val();
    proposal.lastname = $("#txtPLastName").val();
    proposal.mobileno = $("#txtPMobile").val();
    proposal.dob = $("#txtPDOB").val();
    proposal.emailid = $("#txtPEmail").val();
    proposal.address1 = $("#txtPAddress1").val();
    proposal.address2 = $("#txtPAddress2").val();
    proposal.landmark = $("#txtPLandmark").val();
    proposal.pincode = $("#txtPPincode").val();
    proposal.statename = $("#txtPState").val();
    proposal.cityname = $("#txtPCity").val();
    proposal.nrelation = $("#ddlnRelation option:selected").val();
    proposal.ntitle = $("#ddlnTitle option:selected").val();
    proposal.nfirstname = $("#txtnFirstName").val();
    proposal.nlastname = $("#txtnLastName").val();
    proposal.ndob = $("#txtnDOB").val();
    proposal.vehicleregno = $("#txtRegNo1").val().toLocaleUpperCase() + $("#txtRegNo2").val().toLocaleUpperCase();
    proposal.engineno = $("#txtEngineNo").val();
    proposal.chasisno = $("#txtChasisNo").val();
    proposal.valuntarydeductible = $("#ddlVoluntaryDeductible option:selected").val();
    proposal.previousinsured = $("#ddlPreviousInsured option:selected").val();
    proposal.policyholdertype = $("#ddlPolicyHolderType option:selected").val();
    proposal.gstno = $("#txtGSTNo").val() == '' ? 'N/A' : $("#txtGSTNo").val();
    proposal.panno = $("#txtPanNo").val() == '' ? 'N/A' : $("#txtPanNo").val();

    proposal.insurance_product = $("#ddlInsuranceProducts option:selected").val();
    proposal.chainid = $("#ddlInsurers option:selected").data('compid').toString();
    proposal.suminsured = $("#txtSumInsured").val();
    proposal.basic = $("#txtBasicAmt").val();
    proposal.gst = $("#txtGSTAmt").val();
    proposal.totalpremium = $("#txtTotalAmt").val();

    $.ajax({
        type: "Post",
        url: "/Backend_Administration/ExtranetSaveDetails_Motor",
        data: '{enquirydetails: ' + JSON.stringify(enquiryDetails) + ',proposal: ' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data === "success") {
                alert("Success\nAll Details are Saved");
            }
            else {
                alert("Failure");
            }
        }
    });
});
