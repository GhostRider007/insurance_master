﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using insurance.Helper;
using insurance.Helper.DataBase;
using insurance.Models;
using Post_Utility.Admin_Backend;
using Post_Utility.Utility;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_AutoCompleteHelper
    {
        public static Agency GetAgencyDetail(string agencyName = null, string agencyId = null, string status = null)
        {
            Agency agencyDel = new Agency();

            try
            {
                if (!string.IsNullOrEmpty(agencyName) || !string.IsNullOrEmpty(agencyId))
                {
                    DataTable dtAgency = AdminBackend_AgencyHelper.GetAgencyList(agencyId, status, agencyName);

                    if (dtAgency.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtAgency.Rows.Count; i++)
                        {
                            agencyDel.AgencyID = Convert.ToInt32(dtAgency.Rows[i]["AgencyID"].ToString());
                            agencyDel.AgencyIdNew = "SST" + agencyDel.AgencyID;
                            agencyDel.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyName"].ToString()) ? dtAgency.Rows[i]["AgencyName"].ToString() : string.Empty;
                            agencyDel.Address = !string.IsNullOrEmpty(dtAgency.Rows[i]["Address"].ToString()) ? dtAgency.Rows[i]["Address"].ToString() : string.Empty;
                            agencyDel.CountryID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CountryID"].ToString()) ? dtAgency.Rows[i]["CountryID"].ToString() : string.Empty;
                            agencyDel.CountryName = FrontAgencyHelper.CountryList(agencyDel.CountryID, "1").FirstOrDefault().CountryName;
                            agencyDel.StateID = !string.IsNullOrEmpty(dtAgency.Rows[i]["StateID"].ToString()) ? dtAgency.Rows[i]["StateID"].ToString() : string.Empty;
                            agencyDel.StateName = FrontAgencyHelper.StateList(agencyDel.StateID, agencyDel.CountryID, "1").FirstOrDefault().StateName;
                            agencyDel.CityID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CityID"].ToString()) ? dtAgency.Rows[i]["CityID"].ToString() : string.Empty;                            
                            agencyDel.Pincode = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pincode"].ToString()) ? dtAgency.Rows[i]["Pincode"].ToString() : string.Empty;
                            agencyDel.CityName = FrontAgencyHelper.CityList(agencyDel.Pincode).FirstOrDefault().CityName;
                            agencyDel.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["GSTNo"].ToString()) ? dtAgency.Rows[i]["GSTNo"].ToString() : string.Empty;
                            agencyDel.BusinessEmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessEmailID"].ToString()) ? dtAgency.Rows[i]["BusinessEmailID"].ToString() : string.Empty;
                            agencyDel.BusinessPhone = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPhone"].ToString()) ? dtAgency.Rows[i]["BusinessPhone"].ToString() : string.Empty;
                            agencyDel.ReferenceBy = !string.IsNullOrEmpty(dtAgency.Rows[i]["ReferenceBy"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["ReferenceBy"].ToString()) : 0;

                            string addressImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                            agencyDel.BusinessAddressProofImg = UtilityClass.GetImagePath(addressImg, "Agency", agencyDel.AgencyID);

                            string panImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                            agencyDel.BusinessPanCardImg = UtilityClass.GetImagePath(panImg, "Agency", agencyDel.AgencyID);
                            agencyDel.Title = !string.IsNullOrEmpty(dtAgency.Rows[i]["Title"].ToString()) ? dtAgency.Rows[i]["Title"].ToString() : string.Empty;
                            agencyDel.FirstName = !string.IsNullOrEmpty(dtAgency.Rows[i]["FirstName"].ToString()) ? dtAgency.Rows[i]["FirstName"].ToString() : string.Empty;
                            agencyDel.LastName = !string.IsNullOrEmpty(dtAgency.Rows[i]["LastName"].ToString()) ? dtAgency.Rows[i]["LastName"].ToString() : string.Empty;
                            agencyDel.EmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["EmailID"].ToString()) ? dtAgency.Rows[i]["EmailID"].ToString() : string.Empty;
                            agencyDel.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[i]["Mobile"].ToString()) ? dtAgency.Rows[i]["Mobile"].ToString() : string.Empty;

                            string peraddressImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                            agencyDel.PersonalAddressProofImg = UtilityClass.GetImagePath(peraddressImg, "Agency", agencyDel.AgencyID);
                            agencyDel.UserId = !string.IsNullOrEmpty(dtAgency.Rows[i]["UserId"].ToString()) ? dtAgency.Rows[i]["UserId"].ToString() : string.Empty;

                            string password = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? dtAgency.Rows[i]["Password"].ToString() : string.Empty;
                            string encryKey = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? dtAgency.Rows[i]["Password"].ToString() : string.Empty;

                            agencyDel.Password = !string.IsNullOrEmpty(password) ? Security.Decrypt(encryKey, password) : string.Empty; //Utility.Decrypt(item.Password) : string.Empty;

                            string companyImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                            agencyDel.CompanyLogoImg = UtilityClass.GetImagePath(companyImg, "Agency", agencyDel.AgencyID);
                            agencyDel.Status = dtAgency.Rows[i]["Status"].ToString() == "true" ? true : false;
                            agencyDel.GroupType = !string.IsNullOrEmpty(dtAgency.Rows[i]["GroupType"].ToString()) ? dtAgency.Rows[i]["GroupType"].ToString() : string.Empty;
                            agencyDel.CreatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                            agencyDel.UpdatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["UpdatedDate"].ToString())).ToString("dd/MM/yyyy");
                            agencyDel.IsApproved = dtAgency.Rows[i]["IsApproved"].ToString() == "true" ? true : false;
                            agencyDel.LastLoginDate = (Convert.ToDateTime(dtAgency.Rows[i]["LastLoginDate"].ToString())).ToString("dd/MM/yyyy");

                            agencyDel.AvalibleAmount = "00.00";
                            agencyDel.CreditLimit = "00.00";
                            agencyDel.DueAmount = "00.00";

                            agencyDel.Status = dtAgency.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                            agencyDel.CreatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return agencyDel;

        }
    }
}