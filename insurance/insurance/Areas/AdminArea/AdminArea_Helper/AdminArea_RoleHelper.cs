﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Helper.DataBase;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_RoleHelper
    {
        #region [Menu Section]

        public static List<CompanyModel> GetCompanyList(string companyId = null, string status = null)
        {
            List<CompanyModel> result = new List<CompanyModel>();

            try
            {
                string fileds = "*";
                string tablename = "tbl_Company";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status=" + status;
                }

                if (!string.IsNullOrEmpty(companyId))
                {
                    whereCondition += " and CompanyId=" + companyId;
                }

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        CompanyModel model = new CompanyModel();
                        model.CompanyId = Convert.ToInt32(dtRole.Rows[i]["CompanyId"].ToString());
                        model.CompanyName = dtRole.Rows[i]["CompanyName"].ToString();
                        model.Description = dtRole.Rows[i]["Description"].ToString();
                        model.logoUrl = dtRole.Rows[i]["logoUrl"].ToString();
                        model.Status = dtRole.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreateDate = (Convert.ToDateTime(dtRole.Rows[i]["CreateDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<MotorCompanyModel> GetMotorCompanyList(string companyId = null, string status = null)
        {
            List<MotorCompanyModel> result = new List<MotorCompanyModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_motorcredential";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Active=" + status;
                }

                if (!string.IsNullOrEmpty(companyId))
                {
                    whereCondition += " and id=" + companyId;
                }

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        MotorCompanyModel model = new MotorCompanyModel();
                        model.id = Convert.ToInt32(dtRole.Rows[i]["Id"].ToString());
                        model.Supplier = dtRole.Rows[i]["Supplier"].ToString();
                        model.ApiUsername = dtRole.Rows[i]["ApiUsername"].ToString();
                        model.ApiPassword = dtRole.Rows[i]["ApiPassword"].ToString();
                        model.ApiAgencyID = dtRole.Rows[i]["ApiAgencyID"].ToString();
                        model.TwoWQuoteUrl = dtRole.Rows[i]["2WQuoteUrl"].ToString();
                        model.TwoWProposalUrl = dtRole.Rows[i]["2WProposalUrl"].ToString();
                        model.PCQuoteUrl = dtRole.Rows[i]["PCQuoteUrl"].ToString();
                        model.PCProposalUrl = dtRole.Rows[i]["PCProposalUrl"].ToString();
                        model.CMVQuoteUrl = dtRole.Rows[i]["CMVQuoteUrl"].ToString();
                        model.CMVProposalUrl = dtRole.Rows[i]["CMVProposalUrl"].ToString();
                        model.Clientid = dtRole.Rows[i]["Clientid"].ToString();
                        model.TripType = dtRole.Rows[i]["TripType"].ToString();
                        model.URLTYPE = dtRole.Rows[i]["URLTYPE"].ToString();
                        model.OtherData = dtRole.Rows[i]["OtherData"].ToString();
                        model.TotalHit = dtRole.Rows[i]["TotalHit"].ToString();
                        model.Paymentkey = dtRole.Rows[i]["Paymentkey"].ToString();
                        model.Image = dtRole.Rows[i]["Image"].ToString();
                        model.Active = dtRole.Rows[i]["Active"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtRole.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<RoleModel> GetRoleList(string roleId = null, string status = null)
        {
            List<RoleModel> result = new List<RoleModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Role";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status=" + status;
                }

                if (!string.IsNullOrEmpty(roleId))
                {
                    whereCondition += " and RoleID=" + roleId;
                }

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "SeeInsuredApi");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        RoleModel model = new RoleModel();
                        model.RoleID = Convert.ToInt32(dtRole.Rows[i]["RoleID"].ToString());
                        model.RoleName = dtRole.Rows[i]["RoleName"].ToString();
                        model.Status = dtRole.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtRole.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertRoleDetail(RoleModel role)
        {
            try
            {
                string fileds = "RoleName";
                string fieldValue = "'" + role.RoleName + "'";
                string tableName = "T_Role";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateRoleStatus(string menuid, string status)
        {
            try
            {
                string tableName = "T_Role";
                string fieldsWithValue = "Status=" + status;
                string whereCondition = "RoleID=" + menuid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateRoleDetail(RoleModel role)
        {
            try
            {
                string tableName = "T_Role";
                string fieldsWithValue = "RoleName='" + role.RoleName + "'";
                string whereCondition = "RoleID=" + role.RoleID;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static List<SetRolePermission> GetRolePermissionList(string roleid)
        {
            List<SetRolePermission> rolePermissionList = new List<SetRolePermission>();

            try
            {
                string fileds = "*";
                string tablename = "T_SetRolePermission";
                string whereCondition = "RoleID=" + roleid;

                DataTable dtRolePer = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtRolePer.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRolePer.Rows.Count; i++)
                    {
                        SetRolePermission model = new SetRolePermission();
                        model.PermissionID = Convert.ToInt32(dtRolePer.Rows[i]["PermissionID"].ToString());
                        model.MenuID = Convert.ToInt32(dtRolePer.Rows[i]["MenuID"].ToString());
                        model.MenuName = dtRolePer.Rows[i]["MenuName"].ToString();
                        model.SubMenuID = Convert.ToInt32(dtRolePer.Rows[i]["SubMenuID"].ToString());
                        model.SubMenuName = dtRolePer.Rows[i]["SubMenuName"].ToString();
                        model.RoleID = Convert.ToInt32(dtRolePer.Rows[i]["RoleID"].ToString());
                        rolePermissionList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return rolePermissionList;
        }
        public static bool DeleteAssignedRolePermission(string roleid)
        {
            try
            {
                string tableName = "T_SetRolePermission";
                string whereCondition = "RoleID=" + roleid;

                if (ConnectToDataBase.DeleteRecordFromAnyTable(tableName, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool InsertAssignedRolePermission(SetRolePermission rolePer)
        {
            try
            {
                string fileds = "MenuID,MenuName,SubMenuID,SubMenuName,RoleID";
                string fieldValue = "" + rolePer.MenuID + ",'" + rolePer.MenuName + "'," + rolePer.SubMenuID + ",'" + rolePer.SubMenuName + "'," + rolePer.RoleID + "";
                string tableName = "T_SetRolePermission";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool InsertCompanyDetail(CompanyModel model)
        {
            try
            {
                string fileds = "CompanyName,Description,logoUrl,Status,ModifyDate,CreateDate";
                string fieldValue = "'" + model.CompanyName + "','" + model.Description + "','" + model.logoUrl + "',1,getdate(),getdate()";
                string tableName = "tbl_Company";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool InsertCompanyDetailmotor(MotorCompanyModel model)
        {
            try
            {
                string fileds = "Supplier,ApiUsername,ApiPassword,ApiAgencyID,Active,PCQuoteUrl,PCProposalUrl,CMVQuoteUrl,CMVProposalUrl,Clientid,TripType,OtherData,CreatedDate,Paymentkey,Image";
                string fieldValue = "'" + model.Supplier + "','" + model.ApiUsername + "','" + model.ApiPassword + "','" + model.ApiAgencyID + "',1,'" + model.PCQuoteUrl + "','" + model.PCProposalUrl + "','" + model.CMVQuoteUrl + "','" + model.CMVProposalUrl + "','" + model.Supplier + "','" + model.TripType + "','" + model.OtherData + "',getdate(),'" + model.Paymentkey + "','" + model.Image + "'";
                string tableName = "T_motorcredential";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateCompanyDetail(CompanyModel model)
        {
            try
            {
                string tableName = "tbl_Company";
                string fieldsWithValue = "CompanyName='" + model.CompanyName + "',Description='" + model.Description + "',logoUrl='" + model.logoUrl + "',ModifyDate=getdate()";
                string whereCondition = "CompanyId=" + model.CompanyId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdatebrochureDetail(Product model)
        {
            try
            {
                string tableName = "tbl_Product";
                string fieldsWithValue = "BrochureLink='" + model.BrochureLink + "',ModifyDate=getdate()";
                string whereCondition = "ProductId=" + model.ProductId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateClausesLinkDetail(Product model)
        {
            try
            {
                string tableName = "tbl_Product";
                string fieldsWithValue = "ClausesLink='" + model.ClausesLinkUrl + "',ModifyDate=getdate()";
                string whereCondition = "ProductId=" + model.ProductId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdatebrochureDetailmotor(Product model)
        {
            try
            {
                string tableName = "Tblmaster_productdetails";
                string fieldsWithValue = "BrochureLink='" + model.BrochureLink + "'";
                string whereCondition = "id=" + model.ProductId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateClausesLinkUrlDetailmotor(Product model)
        {
            try
            {
                string tableName = "Tblmaster_productdetails";
                string fieldsWithValue = "ClausesLink='" + model.ClausesLinkUrl + "'";
                string whereCondition = "id=" + model.ProductId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateCompanyDetailMotor(MotorCompanyModel model)
        {
            try
            {
                string tableName = "T_motorcredential";
                string fieldsWithValue = "Supplier='" + model.Supplier + "',ApiUsername='" + model.ApiUsername + "',ApiPassword='" + model.ApiPassword + "',ApiAgencyID='" + model.ApiAgencyID + "',PCQuoteUrl='" + model.PCQuoteUrl + "',PCProposalUrl='" + model.PCProposalUrl + "',CMVQuoteUrl='" + model.CMVQuoteUrl + "',CMVProposalUrl='" + model.CMVProposalUrl + "',Clientid='" + model.Supplier + "',TripType='" + model.TripType + "',OtherData='" + model.OtherData + "',Paymentkey='" + model.Paymentkey + "',Image='" + model.Image + "'";
                string whereCondition = "id=" + model.id;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateCompanyStatus(string companyid, string status)
        {
            try
            {
                string tableName = "tbl_Company";
                string fieldsWithValue = "Status=" + status;
                string whereCondition = "CompanyId=" + companyid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool UpdateCompanyStatusmotor(string companyid, string status)
        {
            try
            {
                string tableName = "T_motorcredential";
                string fieldsWithValue = "Active=" + status;
                string whereCondition = "Id=" + companyid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        #endregion

        #region[Product]
        public static List<Product> GetProductList(string productId = null, string status = null, string companyId = null)
        {
            List<Product> result = new List<Product>();

            try
            {
                string fileds = "*";
                string tablename = "tbl_Product a inner join tbl_Company b on a.companyId = b.CompanyId";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "a.Active = " + status;
                }

                if (!string.IsNullOrEmpty(productId) && companyId == null)
                {
                    whereCondition += " and a.ProductId=" + productId;
                }

                if (!string.IsNullOrEmpty(companyId))
                {
                    whereCondition += " and a.CompanyId=" + companyId;
                }

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        Product model = new Product();
                        model.ProductId = Convert.ToInt32(dtRole.Rows[i]["ProductId"].ToString());
                        model.ProductName = dtRole.Rows[i]["ProductName"].ToString();
                        model.Active = dtRole.Rows[i]["Active"].ToString().ToLower() == "true" ? true : false;
                        model.CompanyId = Convert.ToInt32(dtRole.Rows[i]["CompanyId"].ToString());
                        model.CompanyName = dtRole.Rows[i]["CompanyName"].ToString();
                        model.ProductCode = dtRole.Rows[i]["ProductCode"].ToString();
                        model.Product_Description = dtRole.Rows[i]["Product_Description"].ToString();
                        model.ProductName = dtRole.Rows[i]["ProductName"].ToString();
                        model.BrochureLink = dtRole.Rows[i]["BrochureLink"].ToString();
                        model.ClausesLinkUrl = dtRole.Rows[i]["ClausesLink"].ToString();
                        model.Rider = dtRole.Rows[i]["Rider"].ToString();
                        model.CreateDate = (Convert.ToDateTime(dtRole.Rows[i]["CreateDate"].ToString())).ToString("dd/MM/yyyy");
                        model.ModifyDate = (Convert.ToDateTime(dtRole.Rows[i]["ModifyDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        public static List<Product> GetMotorProductList(string productId = null, string status = null)
        {
            List<Product> result = new List<Product>();

            try
            {
                string fileds = "*";
                string tablename = "Tblmaster_productdetails a inner join T_motorcredential b on a.companyId = b.CompanyId";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status = " + status;
                }

                if (!string.IsNullOrEmpty(productId))
                {
                    whereCondition += " and a.id=" + productId;
                }

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        Product model = new Product();
                        model.ProductId = Convert.ToInt32(dtRole.Rows[i]["Id"].ToString());
                        model.ProductName = dtRole.Rows[i]["ProductName"].ToString();
                        model.Active = dtRole.Rows[i]["Status"].ToString().ToLower() == "1" ? true : false;
                        model.CompanyId = Convert.ToInt32(dtRole.Rows[i]["CompanyId"].ToString());
                        model.CompanyName = dtRole.Rows[i]["CompanyName"].ToString();
                        model.ProductName = dtRole.Rows[i]["ProductName"].ToString();
                        model.ProductCode = dtRole.Rows[i]["ProductCode"].ToString();
                        model.ProductType = dtRole.Rows[i]["ProductType"].ToString();
                        model.Product_Description = dtRole.Rows[i]["PolicyType"].ToString();
                        model.PolicyCode = dtRole.Rows[i]["PolicyCode"].ToString();
                        model.BrochureLink = dtRole.Rows[i]["BrochureLink"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        #endregion

        #region[Common Status Updator]
        public static bool StatusUpdator(int id, bool status, string table, string database, string whereCondId, string statusField)
        {
            try
            {
                if (id > 0)
                {
                    string fieldswithvalue = statusField + " = '" + status.ToString().ToLower() + "'";
                    string tablename = table;
                    string wherecondition = whereCondId + " = " + id + " ";

                    return ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition, database);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool MotorStatusUpdator(int id, int status, string table, string database, string whereCondId, string statusField)
        {
            try
            {
                if (id > 0)
                {
                    string fieldswithvalue = statusField + " = " + status.ToString().ToLower() + "";
                    string tablename = table;
                    string wherecondition = whereCondId + " = " + id + " ";

                    return ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition, database);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion
    }
}