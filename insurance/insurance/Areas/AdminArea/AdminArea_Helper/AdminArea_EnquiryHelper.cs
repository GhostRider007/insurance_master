﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Helper.DataBase;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using static insurance.Models.Custom.FrontAgencyModel;
using insurance.Models.Common;
using Post_Utility.Admin_Backend;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public class AdminArea_EnquiryHelper
    {

        public static int[] GetPolicyCount(string period = "day", string duration = "0")
        {
            int[] counts = new int[28];
            try
            {

                DataTable dt = ConnectToDataBase.GetPolicygraph(period, duration, "motor");
                if (dt.Rows.Count > 0)
                {
                    counts[0] = Convert.ToInt32(dt.Rows[0]["motorcount"].ToString());
                    counts[1] = Convert.ToInt32(dt.Rows[0]["healthCount"].ToString());
                    counts[2] = Convert.ToInt32(dt.Rows[0]["motorpolicycount"].ToString());

                    counts[3] = Convert.ToInt32(dt.Rows[0]["healthPolicyCount"].ToString());
                    counts[4] = Convert.ToInt32(dt.Rows[0]["healthEnqCount"].ToString());
                    counts[5] = Convert.ToInt32(dt.Rows[0]["motorEnqcount"].ToString());

                    counts[6] = Convert.ToInt32(dt.Rows[0]["GODIGITPolicy"].ToString());
                    counts[7] = Convert.ToInt32(dt.Rows[0]["SHRIRAMPolicy"].ToString());
                    counts[8] = Convert.ToInt32(dt.Rows[0]["IFFCOTOKIOPolicy"].ToString());

                    counts[9] = Convert.ToInt32(dt.Rows[0]["GODIGITProposal"].ToString());
                    counts[10] = Convert.ToInt32(dt.Rows[0]["SHRIRAMProposal"].ToString());
                    counts[11] = Convert.ToInt32(dt.Rows[0]["IFFCOTOKIOProposal"].ToString());

                    counts[12] = Convert.ToInt32(dt.Rows[0]["totalPolicyCount"].ToString());
                    counts[13] = Convert.ToInt32(dt.Rows[0]["totalPerposalCount"].ToString());
                    counts[14] = Convert.ToInt32(dt.Rows[0]["totalleadcount"].ToString());
                    counts[15] = Convert.ToInt32(dt.Rows[0]["Ludhiana"].ToString());
                    counts[16] = Convert.ToInt32(dt.Rows[0]["Delhi"].ToString());
                    counts[17] = Convert.ToInt32(dt.Rows[0]["healthPropsolCount"].ToString());

                    counts[18] = Convert.ToInt32(dt.Rows[0]["StarHealth"].ToString());
                    counts[19] = Convert.ToInt32(dt.Rows[0]["CareHealth"].ToString());
                    counts[20] = Convert.ToInt32(dt.Rows[0]["ManipalCigna"].ToString());
                    counts[21] = Convert.ToInt32(dt.Rows[0]["AdityaBirla"].ToString());
                    counts[22] = Convert.ToInt32(dt.Rows[0]["PoStarHealth"].ToString());
                    counts[23] = Convert.ToInt32(dt.Rows[0]["PoCareHealth"].ToString());
                    counts[24] = Convert.ToInt32(dt.Rows[0]["PoManipalCigna"].ToString());
                    counts[25] = Convert.ToInt32(dt.Rows[0]["PoAdityaBirla"].ToString());

                    counts[26] = Convert.ToInt32(dt.Rows[0]["HealthDelhi"].ToString());
                    counts[27] = Convert.ToInt32(dt.Rows[0]["HealthLudhiana"].ToString());

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return counts;
        }

        public static int[] GetPolicyCountHealth(string period = "day", string duration = "0")
        {
            int[] counts = new int[17];
            try
            {
                string tableStr = "T_NewEnquiry_Health hEnq inner join T_Health_Proposal hProp  on hEnq.enquiry_id = hProp.enquiryid";

                string whereCond_NewEnq = "createddate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";

                string whereCond_Prop = "(hProp.p_proposalNum is NOT NULL or hProp.p_proposalNum <> '') and (hProp.pay_policy_Number is NULL or hProp.pay_policy_Number='' ) and hEnq.createddate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";
                string whereCond_Policy = "(hProp.pay_policy_Number <> '' and hProp.pay_policy_Number is NOT NULL) and hEnq.createddate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";



                DataTable dt = ConnectToDataBase.GetRecordFromTable("Count(*) as Count", "T_NewEnquiry_Health", whereCond_NewEnq, "api");
                if (dt.Rows.Count > 0)
                {
                    counts[0] = Convert.ToInt32(dt.Rows[0]["Count"].ToString());
                }

                dt = ConnectToDataBase.GetRecordFromTable("Count(*) as Count", tableStr, whereCond_Prop, "api");
                if (dt.Rows.Count > 0)
                {
                    counts[1] = Convert.ToInt32(dt.Rows[0]["Count"].ToString());
                }

                dt = ConnectToDataBase.GetRecordFromTable("Count(*) as Count", tableStr, whereCond_Policy, "api");
                if (dt.Rows.Count > 0)
                {
                    counts[2] = Convert.ToInt32(dt.Rows[0]["Count"].ToString());
                }
                counts[0] = counts[0] - counts[1] - counts[2];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return counts;
        }
        public static decimal[] GetTotalPremium(string period = "day", string duration = "0")
        {
            decimal[] counts = new decimal[4];
            try
            {
                string tableStr_Health = "T_NewEnquiry_Health a inner Join T_Health_Proposal b on a.enquiry_id = b.enquiryid  inner join T_Health_Payment c on a.enquiry_id = c.enquiryid";
                string whereCond_Health = "b.pay_proposal_Number is not null and c.CreatedDate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";

                string tableStr_Motor = "T_Motor_Enquiry_Response a inner join T_Motor_Proposal b on a.enquiry_id = b.Enquiry_Id";
                string whereCond_Motor = "a.policynumber is not null and a.policynumber <> '' and b.createddate>(SELECT DATEADD(" + period + "," + duration + ",CAST(CAST(GETDATE() AS DATE) AS DATETIME)))";


                DataTable dt = ConnectToDataBase.GetRecordFromTable("SUM(CAST(b.premium AS DECIMAL(18,2))) as SUM", tableStr_Health, whereCond_Health, "api");
                if (dt.Rows.Count > 0)
                {
                    counts[0] = Convert.ToDecimal(dt.Rows[0]["SUM"].ToString());
                }
                dt = ConnectToDataBase.GetRecordFromTable("SUM(CAST(a.suminsured AS DECIMAL(18,2))) as SUM", tableStr_Motor, whereCond_Motor, "motor");
                if (dt.Rows.Count > 0)
                {
                    counts[1] = Convert.ToDecimal(dt.Rows[0]["SUM"].ToString());
                }

                counts[2] = 0;
                counts[3] = 0;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return counts;
        }
        public static DataTable GetEnquiryDetailsMotor(string listType, string AgencyId = null, string FromDate = null, string ToDate = null, string area = "admin", bool isFilter = false)
        {
            return AdminBackend_EnquiryHelper.GetEnquiryDetailsMotor(listType, AgencyId, FromDate, ToDate, area, isFilter);
        }
        public static List<TwowheelermapModel> GetMake()
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();

            try
            {
                string fileds = "distinct Make ";
                string tablename = "tblMaster_Car";
                string whereCondition = string.Empty;
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel model = new TwowheelermapModel();
                        model.Make = !string.IsNullOrEmpty(map.Rows[i]["Make"].ToString()) ? map.Rows[i]["Make"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<TwowheelermapModel> GetBikeMake()
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();

            try
            {
                string fileds = "distinct Make ";
                string tablename = "tblMaster_TwoWheeler";
                string whereCondition = string.Empty;
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel model = new TwowheelermapModel();
                        model.Make = !string.IsNullOrEmpty(map.Rows[i]["Make"].ToString()) ? map.Rows[i]["Make"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<TwowheelermapModel> GetTwoWheelerMaplist(string make, string model, string fuelType)
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();

            try
            {
                string fileds = "*";
                string tablename = "tblMaster_Car";
                string whereCondition = "";
                if (!string.IsNullOrEmpty(fuelType))
                {
                    whereCondition = " make='" + make + "' and model='" + model + "' and FuelType='" + fuelType + "'";
                }
                else
                {
                    whereCondition = " make='" + make + "' and model='" + model + "'";
                }
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel models = new TwowheelermapModel();
                        models.Make = !string.IsNullOrEmpty(map.Rows[i]["Make"].ToString()) ? map.Rows[i]["Make"].ToString() : string.Empty;
                        models.vehicleMaincode = !string.IsNullOrEmpty(map.Rows[i]["vehicleMaincode"].ToString()) ? map.Rows[i]["vehicleMaincode"].ToString() : string.Empty;
                        models.vehicleModel = !string.IsNullOrEmpty(map.Rows[i]["Model"].ToString()) ? map.Rows[i]["Model"].ToString() : string.Empty;
                        models.FuelType = !string.IsNullOrEmpty(map.Rows[i]["FuelType"].ToString()) ? map.Rows[i]["FuelType"].ToString() : string.Empty;
                        models.Variant = !string.IsNullOrEmpty(map.Rows[i]["Variant"].ToString()) ? map.Rows[i]["Variant"].ToString() : string.Empty;
                        models.BodyType = !string.IsNullOrEmpty(map.Rows[i]["BodyType"].ToString()) ? map.Rows[i]["BodyType"].ToString() : string.Empty;
                        models.SeatingCapacity = !string.IsNullOrEmpty(map.Rows[i]["SeatingCapacity"].ToString()) ? map.Rows[i]["SeatingCapacity"].ToString() : string.Empty;
                        models.GoDigit = !string.IsNullOrEmpty(map.Rows[i]["GoDigit"].ToString()) ? map.Rows[i]["GoDigit"].ToString() : string.Empty;
                        models.ShriRam = !string.IsNullOrEmpty(map.Rows[i]["ShriRam"].ToString()) ? map.Rows[i]["ShriRam"].ToString() : string.Empty;
                        models.IFFCOTokio = !string.IsNullOrEmpty(map.Rows[i]["IFFCOTokio"].ToString()) ? map.Rows[i]["IFFCOTokio"].ToString() : string.Empty;
                        models.Power = !string.IsNullOrEmpty(map.Rows[i]["Power"].ToString()) ? map.Rows[i]["Power"].ToString() : string.Empty;
                        models.CubicCapacity = !string.IsNullOrEmpty(map.Rows[i]["CubicCapacity"].ToString()) ? map.Rows[i]["CubicCapacity"].ToString() : string.Empty;
                        models.GrossVehicleWeight = !string.IsNullOrEmpty(map.Rows[i]["GrossVehicleWeight"].ToString()) ? map.Rows[i]["GrossVehicleWeight"].ToString() : string.Empty;
                        models.NoOfWheels = !string.IsNullOrEmpty(map.Rows[i]["NoOfWheels"].ToString()) ? map.Rows[i]["NoOfWheels"].ToString() : string.Empty;
                        models.Abs = !string.IsNullOrEmpty(map.Rows[i]["Abs"].ToString()) ? map.Rows[i]["Abs"].ToString() : string.Empty;
                        models.AirBags = !string.IsNullOrEmpty(map.Rows[i]["AirBags"].ToString()) ? map.Rows[i]["AirBags"].ToString() : string.Empty;
                        models.Length = !string.IsNullOrEmpty(map.Rows[i]["Length"].ToString()) ? map.Rows[i]["Length"].ToString() : string.Empty;
                        models.ExShowroomPrice = !string.IsNullOrEmpty(map.Rows[i]["ExShowroomPrice"].ToString()) ? map.Rows[i]["ExShowroomPrice"].ToString() : string.Empty;
                        models.PriceYear = !string.IsNullOrEmpty(map.Rows[i]["PriceYear"].ToString()) ? map.Rows[i]["PriceYear"].ToString() : string.Empty;
                        models.Production = !string.IsNullOrEmpty(map.Rows[i]["Production"].ToString()) ? map.Rows[i]["Production"].ToString() : string.Empty;
                        models.Manufacturing = !string.IsNullOrEmpty(map.Rows[i]["Manufacturing"].ToString()) ? map.Rows[i]["Manufacturing"].ToString() : string.Empty;
                        models.VehicleType = !string.IsNullOrEmpty(map.Rows[i]["VehicleType"].ToString()) ? map.Rows[i]["VehicleType"].ToString() : string.Empty;
                        result.Add(models);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<TwowheelermapModel> GetTBikeMaplist(string make, string model, string fuelType)
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();

            try
            {
                string fileds = "*";
                string tablename = "tblMaster_TwoWheeler";
                string whereCondition = "";
                if (!string.IsNullOrEmpty(fuelType))
                {
                    whereCondition = " make='" + make + "' and model='" + model + "' and FuelType='" + fuelType + "'";
                }
                else
                {
                    whereCondition = " make='" + make + "' and model='" + model + "'";
                }
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel models = new TwowheelermapModel();
                        models.Make = !string.IsNullOrEmpty(map.Rows[i]["Make"].ToString()) ? map.Rows[i]["Make"].ToString() : string.Empty;
                        models.vehicleMaincode = !string.IsNullOrEmpty(map.Rows[i]["vehicleMaincode"].ToString()) ? map.Rows[i]["vehicleMaincode"].ToString() : string.Empty;
                        models.vehicleModel = !string.IsNullOrEmpty(map.Rows[i]["Model"].ToString()) ? map.Rows[i]["Model"].ToString() : string.Empty;
                        models.FuelType = !string.IsNullOrEmpty(map.Rows[i]["FuelType"].ToString()) ? map.Rows[i]["FuelType"].ToString() : string.Empty;
                        models.Variant = !string.IsNullOrEmpty(map.Rows[i]["Variant"].ToString()) ? map.Rows[i]["Variant"].ToString() : string.Empty;
                        models.BodyType = !string.IsNullOrEmpty(map.Rows[i]["BodyType"].ToString()) ? map.Rows[i]["BodyType"].ToString() : string.Empty;
                        models.SeatingCapacity = !string.IsNullOrEmpty(map.Rows[i]["SeatingCapacity"].ToString()) ? map.Rows[i]["SeatingCapacity"].ToString() : string.Empty;
                        models.GoDigit = !string.IsNullOrEmpty(map.Rows[i]["GoDigit"].ToString()) ? map.Rows[i]["GoDigit"].ToString() : string.Empty;
                        models.ShriRam = !string.IsNullOrEmpty(map.Rows[i]["ShriRam"].ToString()) ? map.Rows[i]["ShriRam"].ToString() : string.Empty;
                        models.IFFCOTokio = !string.IsNullOrEmpty(map.Rows[i]["IFFCOTokio"].ToString()) ? map.Rows[i]["IFFCOTokio"].ToString() : string.Empty;
                        models.Power = !string.IsNullOrEmpty(map.Rows[i]["Power"].ToString()) ? map.Rows[i]["Power"].ToString() : string.Empty;
                        models.CubicCapacity = !string.IsNullOrEmpty(map.Rows[i]["CubicCapacity"].ToString()) ? map.Rows[i]["CubicCapacity"].ToString() : string.Empty;
                        models.GrossVehicleWeight = !string.IsNullOrEmpty(map.Rows[i]["GrossVehicleWeight"].ToString()) ? map.Rows[i]["GrossVehicleWeight"].ToString() : string.Empty;
                        models.NoOfWheels = !string.IsNullOrEmpty(map.Rows[i]["NoOfWheels"].ToString()) ? map.Rows[i]["NoOfWheels"].ToString() : string.Empty;
                        models.Abs = !string.IsNullOrEmpty(map.Rows[i]["Abs"].ToString()) ? map.Rows[i]["Abs"].ToString() : string.Empty;
                        models.AirBags = !string.IsNullOrEmpty(map.Rows[i]["AirBags"].ToString()) ? map.Rows[i]["AirBags"].ToString() : string.Empty;
                        models.Length = !string.IsNullOrEmpty(map.Rows[i]["Length"].ToString()) ? map.Rows[i]["Length"].ToString() : string.Empty;
                        models.ExShowroomPrice = !string.IsNullOrEmpty(map.Rows[i]["ExShowroomPrice"].ToString()) ? map.Rows[i]["ExShowroomPrice"].ToString() : string.Empty;
                        models.PriceYear = !string.IsNullOrEmpty(map.Rows[i]["PriceYear"].ToString()) ? map.Rows[i]["PriceYear"].ToString() : string.Empty;
                        models.Production = !string.IsNullOrEmpty(map.Rows[i]["Production"].ToString()) ? map.Rows[i]["Production"].ToString() : string.Empty;
                        models.Manufacturing = !string.IsNullOrEmpty(map.Rows[i]["Manufacturing"].ToString()) ? map.Rows[i]["Manufacturing"].ToString() : string.Empty;
                        models.VehicleType = !string.IsNullOrEmpty(map.Rows[i]["VehicleType"].ToString()) ? map.Rows[i]["VehicleType"].ToString() : string.Empty;
                        result.Add(models);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<TwowheelermapModel> GetModel(string make)
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();
            try
            {
                string fileds = "distinct Model ";
                string tablename = "tblMaster_Car";
                string whereCondition = "make='" + make + "'";
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel model = new TwowheelermapModel();
                        model.vehicleModel = !string.IsNullOrEmpty(map.Rows[i]["Model"].ToString()) ? map.Rows[i]["Model"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<TwowheelermapModel> GetBikeModel(string make)
        {
            List<TwowheelermapModel> result = new List<TwowheelermapModel>();
            try
            {
                string fileds = "distinct Model ";
                string tablename = "tblMaster_TwoWheeler";
                string whereCondition = "make='" + make + "'";
                DataTable map = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (map.Rows.Count > 0)
                {
                    for (int i = 0; i < map.Rows.Count; i++)
                    {
                        TwowheelermapModel model = new TwowheelermapModel();
                        model.vehicleModel = !string.IsNullOrEmpty(map.Rows[i]["Model"].ToString()) ? map.Rows[i]["Model"].ToString() : string.Empty;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        #region[For Proposal and Policy List]

        internal static DataTable GetRenewalList_Health(string agencyid, string area = "admin")
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string fileds = "a.AgencyId, a.Enquiry_Id, a.AgencyName, a.Policy_Type, a.BlockID, b.p_proposalNum as ProposalNumber, b.pay_proposal_Number, b.pay_policy_Number as PolicyNumber,b.premium,b.companyid, (select CompanyName from tbl_Company where CompanyId = b.companyid) as CompanyName, (select JSON_VALUE(perposalrequest, '$.products.policyName') from T_Enquiry_Response where ISJSON(perposalrequest) = 1 and enquiry_id = a.enquiry_id) as ProductName, b.createddate as ProposalDate,b.suminsured, (i.title + ' ' + i.firstname + ' ' + i.lastname) as InsuredName,p.UpdatedDate as PolicyDate,(DATEDIFF(day, getdate(), CONVERT(datetime,b.endon_date,103))) as DaysLeft";

                string tablename = "T_NewEnquiry_Health a inner join T_Health_Insured i on a.enquiry_id=i.enquiryid left Join T_Health_Proposal b on a.enquiry_id = b.enquiryid left join T_Health_Payment p on a.enquiry_id = p.enquiryid";

                string whereCondition = "(b.pay_policy_Number is not null and b.pay_policy_Number<>'' and DATEDIFF(day, getdate(), CONVERT(datetime,b.endon_date,103)) between 0 and 1000)";

                if (!string.IsNullOrEmpty(agencyid))
                {
                    whereCondition = whereCondition + " and a.AgencyId='" + agencyid + "'";
                }

                whereCondition = whereCondition + " order by DaysLeft desc";

                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }
        public static DataTable GetEnquiryDetailsHealth(string agencyid, string listType, string FromDate = null, string ToDate = null, string area = "admin", bool isFilter = false)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string fileds = "a.AgencyId, a.Enquiry_Id, a.AgencyName, a.Policy_Type, a.BlockID, b.p_proposalNum as ProposalNumber, b.pay_proposal_Number, b.pay_policy_Number as PolicyNumber,b.premium,b.companyid, (select CompanyName from tbl_Company where CompanyId = b.companyid) as CompanyName, (select JSON_VALUE(perposalrequest, '$.products.policyName') from T_Enquiry_Response where ISJSON(perposalrequest) = 1 and enquiry_id = a.enquiry_id) as ProductName, b.createddate as ProposalDate,b.suminsured, (i.title + ' ' + i.firstname + ' ' + i.lastname) as InsuredName,p.UpdatedDate as PolicyDate";

                string tablename = "T_NewEnquiry_Health a inner join T_Health_Insured i on a.enquiry_id=i.enquiryid left Join T_Health_Proposal b on a.enquiry_id = b.enquiryid left join T_Health_Payment p on a.enquiry_id = p.enquiryid";

                string whereCondition = (listType == "policy" ? "(b.pay_policy_Number is not null and b.pay_policy_Number<>'')" : "(b.pay_policy_Number is null or b.pay_policy_Number='') and (b.p_proposalNum is not NULL or b.p_proposalNum <> '')") + " and i.isinsured=1 ";

                if (!string.IsNullOrEmpty(agencyid))
                {
                    whereCondition = whereCondition + " and a.AgencyId='" + agencyid + "'";
                }

                if (listType == "proposal")
                {
                    if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && area == "admin" && isFilter == false)
                    {
                        whereCondition = whereCondition + " and  b.createddate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(FromDate))
                        {
                            whereCondition = whereCondition + " and b.createddate >= '" + FromDate + "'";
                        }

                        if (!string.IsNullOrEmpty(ToDate))
                        {
                            whereCondition = whereCondition + " and b.createddate <= DATEADD(Day, 1, '" + ToDate + "')";
                        }
                    }

                    whereCondition = whereCondition + " order by b.createddate desc";
                }
                else if (listType == "policy")
                {
                    if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && area == "admin" && isFilter == false)
                    {
                        whereCondition = whereCondition + " and  p.UpdatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(FromDate))
                        {
                            whereCondition = whereCondition + " and p.UpdatedDate >= '" + FromDate + "'";
                        }

                        if (!string.IsNullOrEmpty(ToDate))
                        {
                            whereCondition = whereCondition + " and p.UpdatedDate <= DATEADD(Day, 1, '" + ToDate + "')";
                        }
                    }

                    whereCondition = whereCondition + " order by p.UpdatedDate desc";
                }
                if (area == "agency" && string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate))
                {
                    fileds = "top 20 " + fileds;
                }
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        #endregion
        public static DataTable GetEnquiryDetails()
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string fileds = " top 5 *";
                string tablename = "T_NewEnquiry_Health order by createddate desc";
                string whereCondition = string.Empty;
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable GetBankDetails(string agencyID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string fileds = "*";
                string tablename = "T_AgencyBankDeatil";
                string whereCondition = "AgencyID='" + agencyID + "'";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }
        public static DataTable GetEnquiryDetailList(string userid, string status, string EnquiryId = null, string AgencyId = null, string PolicyType = null, string FromDate = null, string ToDate = null)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "";
                string fileds = " * ";
                string tableNameStr = "(select * from T_NewEnquiry_Health where enquiry_id not in (select enquiryid from T_Health_Proposal) union select * from T_NewEnquiry_Health where enquiry_id in (select enquiryid from T_Health_Proposal where p_referenceId is NULL and p_proposalNum is NuLL)) as list";


                if (userid != "admin")
                {

                    if (string.IsNullOrEmpty(status) || status == "all")
                    {

                        tablename = tableNameStr + " where (BlockID='" + userid + "' or BlockID IS NULL)";
                    }
                    else if (status == "Block")
                    {
                        tablename = tableNameStr + " where (BlockID='" + userid + "')";

                    }
                    else if (status == "UnBlock")
                    {
                        tablename = tableNameStr + " where ( BlockID IS NULL)";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(status) || status == "all")
                    {
                        //tablename = "T_NewEnquiry_Health where Status is NULL and BlockID is NULL order by createddate desc";
                        tablename = tableNameStr + " where (Status is NULL and BlockID is NULL)";
                    }
                    else if (status == "Block")
                    {
                        tablename = tableNameStr + " where ( BlockID IS NOT NULL)";
                    }
                    else if (status == "UnBlock")
                    {
                        tablename = tableNameStr + " where ( BlockID IS NULL)";
                    }
                }

                #region[Filteraton]

                bool isFilter = false;
                if (!string.IsNullOrEmpty(EnquiryId) || !string.IsNullOrEmpty(AgencyId) || !string.IsNullOrEmpty(PolicyType))
                {
                    isFilter = true;
                }

                if (!string.IsNullOrEmpty(EnquiryId))
                {
                    tablename = tablename + " and ( Enquiry_Id = '" + EnquiryId.Trim().ToLower() + "')";
                }
                if (!string.IsNullOrEmpty(AgencyId))
                {
                    tablename = tablename + " and ( AgencyId = '" + AgencyId + "')";
                }
                if (!string.IsNullOrEmpty(PolicyType))
                {
                    tablename = tablename + " and ( Policy_Type = '" + PolicyType + "')";
                }

                if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && isFilter == false)
                {
                    tablename = tablename + " and  CreatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                }
                else
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        tablename = tablename + " and CreatedDate >= '" + FromDate + "'";
                    }

                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        tablename = tablename + " and CreatedDate <= DATEADD(Day, 1, '" + ToDate + "')";
                    }
                }

                #endregion

                tablename = tablename + " order by CreatedDate desc";
                string whereCondition = string.Empty;
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable TrackDetails(string Enquiry_ID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "";
                string fileds = " * ";

                tablename = "T_NewEnquiry_Health a join " + Config.SeeInsuredConString + "..T_Agency b  on a.AgencyName=b.AgencyName ";



                string whereCondition = "Enquiry_ID = '" + Enquiry_ID + "'";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable TrackDetailsMotor(string Enquiry_ID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "";
                string fileds = " * ";

                tablename = "T_NewEnquiry_Motor a join seeinsuredproduct_test..T_Agency b  on a.AgencyName=b.AgencyName ";



                string whereCondition = "Enquiry_ID = '" + Enquiry_ID + "'";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable EmailProposalDetailsMotor(string Enquiry_ID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = " T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id";
                string fileds = "a.Fuel,a.BrandName,a.DateOfReg,a.RtoName,a.insurancetype,a.Id,a.AgencyName,a.AgencyId, a.CreatedDate,a.enquiry_id,a.ModelName,a.VarientName,c.dob, c.mobileno,emailid,c.ntitle,c.nfirstname,c.nlastname,ndob,c.policyholdertype,a.DateOfMfg,a.ispreviousinsurer,a.policyexpirydate,a.isclaiminlastyear, c.engineno,c.nrelation,c.chasisno,c.previousinsured,(Select BusinessEmailID from seeinsuredapi_test..aspnet_user  where AgencyId = a.AgencyId) AgencyEmail, a.vehicletype,b.proposalnumber as ProposalNo,b.chainid as ChainId,b.suminsured as PremimumAmount,c.firstname as FirstName, c.lastname as lastName ,b.policynumber,b.policypdflink";
                string whereCondition = "a.enquiry_id='" + Enquiry_ID + "' and b.transactionnumber is  null and b.proposalnumber is not Null and b.paymentstatus is null  order by createddate desc";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable EmailProposalDetailsHealth(string Enquiry_ID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "T_NewEnquiry_Health hEnq inner join T_Health_Proposal hProp  on hEnq.enquiry_id = hProp.enquiryid  inner join aspnet_user agen on hEnq.AgencyId = agen.AgencyId ";
                string fileds = "*,(Select ProductName from tbl_Product where ProductId=hProp.ProductId) as ProductName,(Select CompanyName  from tbl_Company  where companyid=hProp.companyid) as CompanyName,(Select emailid from aspnet_user where AgencyId=hEnq.AgencyId) as AgencyEmail";
                string whereCondition = "(hProp.p_proposalNum is NOT NULL or hProp.p_proposalNum <> '') and (hProp.pay_policy_Number is NULL or hProp.pay_policy_Number='') and   hEnq.enquiry_id='" + Enquiry_ID + "'";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable EmailHealth_InsuredDetails(string Enquiry_ID)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "T_Health_Insured";
                string fileds = "*";
                string whereCondition = "enquiryid='" + Enquiry_ID + "'";
                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }
        public static DataTable GetBlockedEnquiryList(string userid, string status)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "";
                string fileds = " * ";
                string whereCondition = string.Empty;
                if (userid != "admin")
                {

                    if (status == "all")
                    {
                        tablename = "T_NewEnquiry_Health";
                        whereCondition = " BlockID='" + userid + "' and (status='Block' or status='underProcess' or status='Rejected')  order by createddate desc";
                    }
                    else
                    {

                        tablename = "T_NewEnquiry_Health";
                        whereCondition = "BlockID = '" + userid + "'  and status = '" + status + "' order by createddate desc";
                    }
                }
                else
                {
                    if (status == "all")
                    {
                        tablename = "T_NewEnquiry_Health";
                        whereCondition = " (status='Block' or status='underProcess' or status='Rejected')  order by createddate desc";
                    }
                    else
                    {
                        tablename = "T_NewEnquiry_Health";
                        whereCondition = "status = '" + status + "' order by createddate desc";
                    }
                }

                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "api");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }

        public static DataTable GetMoterBlockedEnquiryList(string userid, string status)
        {
            DataTable dtEnq = new DataTable();
            try
            {
                string tablename = "";
                string fileds = " * ";
                string whereCondition = string.Empty;
                if (userid != "admin")
                {

                    if (status == "all")
                    {
                        tablename = "T_NewEnquiry_Motor";
                        whereCondition = " BlockID='" + userid + "' and (NewStatus='Block' or NewStatus='underProcess' or NewStatus='Rejected')  order by createddate desc";
                    }
                    else
                    {

                        tablename = "T_NewEnquiry_Motor";
                        whereCondition = "BlockID = '" + userid + "'  and NewStatus = '" + status + "' order by createddate desc";
                    }
                }
                else
                {
                    if (status == "all")
                    {
                        tablename = "T_NewEnquiry_Motor";
                        whereCondition = " (Newstatus='Block' or NewStatus='underProcess' or NewStatus='Rejected')  order by createddate desc";
                    }
                    else
                    {
                        tablename = "T_NewEnquiry_Motor";
                        whereCondition = "NewStatus = '" + status + "' order by createddate desc";
                    }
                }

                dtEnq = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtEnq;
        }
        public static bool BlockEnquiry(string Enquiry_Id, string BlockID)
        {
            try
            {
                string tableName = "T_NewEnquiry_Health";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(BlockID))
                {
                    fieldsWithValue = "BlockID='" + BlockID + "',Status='Block'";
                }

                string whereCondition = "Enquiry_Id='" + Enquiry_Id + "'";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool MoterBlockEnquiry(string Enquiry_Id, string BlockID)
        {
            try
            {
                string tableName = "T_NewEnquiry_Motor";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(BlockID))
                {
                    fieldsWithValue = "BlockID='" + BlockID + "',NewStatus='Block'";
                }

                string whereCondition = "Enquiry_Id='" + Enquiry_Id + "'";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool ActiveBlockEnquiry(string Enquiry_Id, string BlockID, string Status)
        {
            try
            {
                string tableName = "T_NewEnquiry_Health";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(BlockID))
                {
                    fieldsWithValue = "BlockID='" + BlockID + "',Status='" + Status + "'";
                }

                string whereCondition = "Enquiry_Id='" + Enquiry_Id + "'";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool ActiveBlockEnquiryMotor(string Enquiry_Id, string BlockID, string Status)
        {
            try
            {
                string tableName = "T_NewEnquiry_Motor";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(BlockID))
                {
                    fieldsWithValue = "BlockID='" + BlockID + "',NewStatus='" + Status + "'";
                }

                string whereCondition = "Enquiry_Id='" + Enquiry_Id + "'";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        #region[Motor]

        public static List<EnquiryDetails_Motors> GetEnquiryDetailList_Motor(string userid, string status, string FromDate = null, string ToDate = null, string AgencyId = null, string PolicyType = null, string EnquiryId = null)
        {

            List<EnquiryDetails_Motors> result = new List<EnquiryDetails_Motors>();
            try
            {
                string tablename = "";
                string fileds = " * ";
                if (userid != "admin")
                {
                    if (string.IsNullOrEmpty(status) || status == "all")
                    {
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id  where ( a.BlockID IS NULL and b.proposalnumber is null) ";
                    }
                    else if (status == "Block")
                    {
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id  where ( a.BlockID='" + userid + "' and b.proposalnumber is null) ";

                    }
                    else if (status == "UnBlock")
                    {
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id  where ( a.BlockID IS NULL and b.proposalnumber is null) ";
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(status) || status == "all")
                    {
                        //tablename = "T_NewEnquiry_Health where Status is NULL and BlockID is NULL order by createddate desc";
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id where  b.proposalnumber is null ";
                    }
                    else if (status == "Block")
                    {
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id  where ( a.BlockID IS NOT NULL and b.proposalnumber is null) ";
                    }
                    else if (status == "UnBlock")
                    {
                        tablename = "T_NewEnquiry_Motor a inner join  T_Motor_Enquiry_Response b  on a.enquiry_id = b.enquiry_id  where ( a.BlockID IS NULL and b.proposalnumber is null ";
                    }
                }

                #region[Filtration]

                bool isFilter = false;
                if (!string.IsNullOrEmpty(EnquiryId) || !string.IsNullOrEmpty(AgencyId) || !string.IsNullOrEmpty(PolicyType))
                {
                    isFilter = true;
                }

                if (string.IsNullOrEmpty(FromDate) && string.IsNullOrEmpty(ToDate) && isFilter == false)
                {
                    tablename = tablename + " and  a.createddate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
                }
                else
                {
                    if (!string.IsNullOrEmpty(FromDate))
                    {
                        tablename = tablename + " and a.createddate > '" + FromDate + "'";
                    }
                    if (!string.IsNullOrEmpty(ToDate))
                    {
                        tablename = tablename + " and a.createddate < DATEADD(Day, 1, '" + ToDate + "')";
                    }
                }

                if (!string.IsNullOrEmpty(AgencyId))
                {
                    tablename = tablename + " and a.AgencyId = '" + AgencyId + "'";
                }
                if (!string.IsNullOrEmpty(PolicyType))
                {
                    tablename = tablename + " and a.insurancetype = '" + PolicyType + "'";
                }
                if (!string.IsNullOrEmpty(EnquiryId))
                {
                    tablename = tablename + " and a.enquiry_id = '" + EnquiryId.Trim().ToLower() + "'";
                }

                #endregion

                tablename = tablename + " order by a.createddate desc";

                string whereCondition = string.Empty;
                DataTable dtEnquiryDetail = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (dtEnquiryDetail.Rows.Count > 0)
                {

                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        EnquiryDetails_Motors model = new EnquiryDetails_Motors();
                        model.Id = Convert.ToInt32(dtEnquiryDetail.Rows[i]["Id"].ToString());
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.AgencyId = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.VechileRegNo = dtEnquiryDetail.Rows[i]["VechileRegNo"].ToString();
                        model.MobileNo = dtEnquiryDetail.Rows[i]["MobileNo"].ToString();
                        model.Is_Term_Accepted = Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Is_Term_Accepted"].ToString());
                        model.RtoId = dtEnquiryDetail.Rows[i]["RtoId"].ToString();
                        model.RtoName = dtEnquiryDetail.Rows[i]["RtoName"].ToString();
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        model.NewStatus = dtEnquiryDetail.Rows[i]["NewStatus"].ToString();
                        model.DateOfReg = dtEnquiryDetail.Rows[i]["DateOfReg"].ToString();
                        //model.YearOfMake = dtEnquiryDetail.Rows[i]["YearOfMake"].ToString();
                        model.BrandId = dtEnquiryDetail.Rows[i]["BrandId"].ToString();
                        model.BrandName = dtEnquiryDetail.Rows[i]["BrandName"].ToString();
                        model.ModelId = dtEnquiryDetail.Rows[i]["ModelId"].ToString();
                        model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.Fuel = dtEnquiryDetail.Rows[i]["Fuel"].ToString();
                        model.VarientId = dtEnquiryDetail.Rows[i]["VarientId"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.vehicletype = dtEnquiryDetail.Rows[i]["vehicletype"].ToString();
                        model.Status = Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Status"].ToString());
                        model.CreatedDate = Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString());
                        model.UpdatedDate = Convert.ToDateTime(dtEnquiryDetail.Rows[i]["UpdatedDate"].ToString());
                        model.PinCode = !string.IsNullOrEmpty(dtEnquiryDetail.Rows[i]["PinCode"].ToString()) ? dtEnquiryDetail.Rows[i]["PinCode"].ToString() : "Not Available";
                        model.ispreviousinsurer = dtEnquiryDetail.Rows[i]["ispreviousinsurer"].ToString();
                        model.insurancetype = dtEnquiryDetail.Rows[i]["insurancetype"].ToString();
                        model.insurerid = dtEnquiryDetail.Rows[i]["insurerid"].ToString();
                        model.insurername = dtEnquiryDetail.Rows[i]["insurername"].ToString();
                        model.policynumber = dtEnquiryDetail.Rows[i]["policynumber"].ToString();
                        model.policyexpirydate = dtEnquiryDetail.Rows[i]["policyexpirydate"].ToString();
                        model.isclaiminlastyear = dtEnquiryDetail.Rows[i]["isclaiminlastyear"].ToString();
                        model.previousyearnoclaim = dtEnquiryDetail.Rows[i]["previousyearnoclaim"].ToString();
                        result.Add(model);
                    }

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static bool MotorEnquiryStatusUpdator(string Enquiry_Id, string BlockID)
        {
            try
            {
                string tableName = "T_NewEnquiry_Motor";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(BlockID))
                {
                    fieldsWithValue = "BlockID='" + BlockID + "',NewStatus='Block'";
                }

                string whereCondition = "Enquiry_Id='" + Enquiry_Id + "'";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "motor"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;

        }

        #endregion
    }
}