﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using static insurance.Models.Custom.FrontAgencyModel;
using ecommerce.Models.Common;
using insurance.Helper.DataBase;
using Post_Utility.Utility;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_RegistrationHelper
    {
        public static List<Branch> GetBranch()
        {
            List<Branch> result = new List<Branch>();

            try
            {
                string fileds = "ID,BranchID,BranchName";
                string tablename = "T_Branch";
                string whereCondition = string.Empty;            
                DataTable dtbranch = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtbranch.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbranch.Rows.Count; i++)
                    {
                        Branch model = new Branch();
                        model.Id = Convert.ToInt32(dtbranch.Rows[i]["ID"].ToString());
                        model.BranchName = !string.IsNullOrEmpty(dtbranch.Rows[i]["BranchName"].ToString()) ? dtbranch.Rows[i]["BranchName"].ToString() : string.Empty;                       
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }

        public static List<MemberLogin> GetMemberList(string memberId = null, string Roleid = null, string status = null)
        {
            List<MemberLogin> result = new List<MemberLogin>();

            try
            {
                string fileds = "m.*, r.RoleName";
                string tablename = "T_MemberLogin m INNER JOIN T_Role r ON m.RoleId = r.RoleID";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "m.Status=" + status;
                }

                if (!string.IsNullOrEmpty(Roleid))
                {
                    whereCondition = "m.RoleId=" + Roleid;
                }
                if (!string.IsNullOrEmpty(memberId))
                {
                    whereCondition += " and m.LoginId=" + memberId;
                }

                DataTable dtMember = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMember.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMember.Rows.Count; i++)
                    {
                        MemberLogin model = new MemberLogin();
                        model.LoginId = Convert.ToInt32(dtMember.Rows[i]["LoginId"].ToString());
                        model.FirstName = !string.IsNullOrEmpty(dtMember.Rows[i]["FirstName"].ToString()) ? dtMember.Rows[i]["FirstName"].ToString() : string.Empty;
                        model.LastName = !string.IsNullOrEmpty(dtMember.Rows[i]["LastName"].ToString()) ? dtMember.Rows[i]["LastName"].ToString() : string.Empty;
                        model.EmailId = !string.IsNullOrEmpty(dtMember.Rows[i]["EmailId"].ToString()) ? dtMember.Rows[i]["EmailId"].ToString() : string.Empty;
                        model.MobileNo = !string.IsNullOrEmpty(dtMember.Rows[i]["MobileNo"].ToString()) ? dtMember.Rows[i]["MobileNo"].ToString() : string.Empty;
                        model.UserId = !string.IsNullOrEmpty(dtMember.Rows[i]["UserId"].ToString()) ? dtMember.Rows[i]["UserId"].ToString() : string.Empty;
                        model.Password = !string.IsNullOrEmpty(dtMember.Rows[i]["Password"].ToString()) ? dtMember.Rows[i]["Password"].ToString() : string.Empty;
                        model.MemberType = !string.IsNullOrEmpty(dtMember.Rows[i]["MemberType"].ToString()) ? dtMember.Rows[i]["MemberType"].ToString() : string.Empty;
                        model.Image = !string.IsNullOrEmpty(dtMember.Rows[i]["Image"].ToString()) ? UtilityClass.GetImagePath(dtMember.Rows[i]["Image"].ToString(), "MemberImages", ("Member_" + model.LoginId)) : "/Content/images/user.png";
                        model.RoleId = !string.IsNullOrEmpty(dtMember.Rows[i]["RoleId"].ToString()) ? Convert.ToInt32(dtMember.Rows[i]["RoleId"].ToString()) : 0;
                        model.RoleName = !string.IsNullOrEmpty(dtMember.Rows[i]["RoleName"].ToString()) ? dtMember.Rows[i]["RoleName"].ToString() : string.Empty;
                        model.Status = dtMember.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtMember.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool UpdateMemberStatus(string memberId, string status)
        {
            try
            {
                string tableName = "T_MemberLogin";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    fieldsWithValue = "Status=" + status;
                }

                string whereCondition = "LoginId=" + memberId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static int InsertMemberDetail(MemberLogin member)
        {
            try
            {
                string fileds = "FirstName,LastName,EmailId,MobileNo,UserId,Password,MemberType,Image,RoleId";
                string fieldValue = "'" + member.FirstName + "','" + member.LastName + "','" + member.EmailId + "','" + member.MobileNo + "','" + member.UserId + "','" + member.Password + "','" + member.MemberType + "','" + member.Image + "'," + member.RoleId + "";
                string tableName = "T_MemberLogin";

                int memberId = ConnectToDataBase.InsertRecordToTableReturnID(fileds, fieldValue, tableName, "seeinsured");
                if (memberId > 0)
                {
                    return memberId;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool UpdateMemberDetail(MemberLogin member)
        {
            try
            {
                string tableName = "T_MemberLogin";
                string fieldsWithValue = "FirstName='" + member.FirstName + "',LastName='" + member.LastName + "',EmailId='" + member.EmailId + "',MobileNo='" + member.MobileNo + "',UserId='" + member.UserId + "',Password='" + member.Password + "',RoleId='" + member.RoleId + "'";
                if (!string.IsNullOrEmpty(member.Image))
                {
                    fieldsWithValue += ",Image='" + member.Image + "'";
                }
                string whereCondition = "LoginId=" + member.LoginId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }

        public static bool SaveDsrReport(Agency model)
        {
            try
            {
                string tableName = "T_DailySalesReport";
                string fileds = "AgencyUserId,AgencyName,AgencyId,BusinessEmailId,AvailabeBalance,CreditLimit,DueBalance,AgencyAddress,AgencyBusinessPhoneNo,AgencyFeedback,UserFeedback,CreatedByName,ReadStatus,CreatedByUserID,CreatedDate";
                string fieldValue = "'" + (!string.IsNullOrEmpty(model.UserId) ? model.UserId : model.AgencyIdNew) + "','" + model.AgencyName + "','" + (!string.IsNullOrEmpty(model.AgencyIdNew) ? model.AgencyIdNew : "SST" + model.AgencyID.ToString()) + "','" + model.BusinessEmailID + "','" + model.AvalibleAmount + "','" + model.CreditLimit + "','" + model.DueAmount + "','" + model.Address + "','" + model.BusinessPhone + "','" + model.AgencyFeedBack + "','" + model.UserFeedBack + "','" + model.CreatedByUserName + "','" + model.ReadStatus + "','" + model.CreatedByUserID + "',GETDATE()";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return false;
        }

        public static List<Agency> GetDsrreportList(Agency model, string type)
        {
            List<Agency> objDsrList = new List<Agency>();

            try
            {
                string fileds = "DSRId,AgencyName,AdminRemark,AgencyId,BusinessEmailId,AvailabeBalance,CreditLimit,DueBalance,AgencyAddress,AgencyBusinessPhoneNo,AgencyFeedback,UserFeedback,ReadStatus,CreatedByName,CreatedByUserID,CreatedDate";
                string tablename = "T_DailySalesReport";
                string whereCondition = string.Empty;

                if (model.Type != "admin")
                {
                    if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate) && !string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "' and CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate))
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "' and CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "' and CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate))
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "' and CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else
                    {
                        whereCondition = "CreatedByName='" + model.CreatedByUserName + "'and CreatedDate>='" + model.TempCreditDate + "'";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        whereCondition = whereCondition + " order by DSRId desc";
                    }
                    if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(model.StaffName))
                    {
                        whereCondition = "ReadStatus='" + type + "' and CreatedByName='" + model.StaffName + "'  order by DSRId desc";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate) && !string.IsNullOrEmpty(model.StaffName) && !string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "' and CreatedByName='" + model.StaffName + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate) && !string.IsNullOrEmpty(model.StaffName))
                    {
                        whereCondition = "CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "' and CreatedByName='" + model.StaffName + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate) && !string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate) && !string.IsNullOrEmpty(model.ToDate))
                    {
                        whereCondition = "CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "' ";
                    }
                    else if (!string.IsNullOrEmpty(model.FromDate))
                    {
                        whereCondition = "CreatedDate>='" + Reverse(model.FromDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.StaffName) && !string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "CreatedByName ='" + model.StaffName + "' and ReadStatus='" + model.DSRStatus + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.StaffName))
                    {
                        whereCondition = "CreatedByName ='" + model.StaffName + "'";
                    }
                    else if (!string.IsNullOrEmpty(model.DSRStatus))
                    {
                        whereCondition = "ReadStatus ='" + model.DSRStatus + "'";
                    }
                    else
                    {
                        whereCondition = "CreatedDate>='" + model.TempCreditDate + "' order by DSRId desc";
                    }
                }

                DataTable dt = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Agency tempmodel = new Agency();
                        tempmodel.DsrID = !string.IsNullOrEmpty(dt.Rows[i]["DsrID"].ToString()) ? Convert.ToInt32(dt.Rows[i]["DsrID"].ToString()) : 0;
                        tempmodel.AgencyName = !string.IsNullOrEmpty(dt.Rows[i]["AgencyName"].ToString()) ? dt.Rows[i]["AgencyName"].ToString() : string.Empty;
                        tempmodel.AdminRemark = !string.IsNullOrEmpty(dt.Rows[i]["AdminRemark"].ToString()) ? dt.Rows[i]["AdminRemark"].ToString() : string.Empty;
                        tempmodel.AgencyIdNew = !string.IsNullOrEmpty(dt.Rows[i]["AgencyId"].ToString()) ? dt.Rows[i]["AgencyId"].ToString() : string.Empty;
                        tempmodel.BusinessEmailID = !string.IsNullOrEmpty(dt.Rows[i]["BusinessEmailId"].ToString()) ? dt.Rows[i]["BusinessEmailId"].ToString() : string.Empty;
                        tempmodel.BusinessPhone = !string.IsNullOrEmpty(dt.Rows[i]["AgencyBusinessPhoneNo"].ToString()) ? dt.Rows[i]["AgencyBusinessPhoneNo"].ToString() : string.Empty;
                        tempmodel.AvalibleAmount = !string.IsNullOrEmpty(dt.Rows[i]["AvailabeBalance"].ToString()) ? dt.Rows[i]["AvailabeBalance"].ToString() : string.Empty;
                        tempmodel.CreditLimit = !string.IsNullOrEmpty(dt.Rows[i]["CreditLimit"].ToString()) ? dt.Rows[i]["CreditLimit"].ToString() : string.Empty;
                        tempmodel.DueAmount = !string.IsNullOrEmpty(dt.Rows[i]["DueBalance"].ToString()) ? dt.Rows[i]["DueBalance"].ToString() : string.Empty;
                        tempmodel.AgencyFeedBack = !string.IsNullOrEmpty(dt.Rows[i]["AgencyFeedBack"].ToString()) ? dt.Rows[i]["AgencyFeedBack"].ToString() : string.Empty;
                        tempmodel.UserFeedBack = !string.IsNullOrEmpty(dt.Rows[i]["UserFeedBack"].ToString()) ? dt.Rows[i]["UserFeedBack"].ToString() : string.Empty;
                        tempmodel.CreatedByUserName = !string.IsNullOrEmpty(dt.Rows[i]["CreatedByName"].ToString()) ? dt.Rows[i]["CreatedByName"].ToString() : string.Empty;
                        tempmodel.ReadStatus = !string.IsNullOrEmpty(dt.Rows[i]["ReadStatus"].ToString()) ? dt.Rows[i]["ReadStatus"].ToString() : string.Empty;
                        tempmodel.TempCreditDate = !string.IsNullOrEmpty(dt.Rows[i]["CreatedDate"].ToString()) ? dt.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        tempmodel.CreatedByUserID = !string.IsNullOrEmpty(dt.Rows[i]["CreatedByUserID"].ToString()) ? dt.Rows[i]["CreatedByUserID"].ToString() : string.Empty;
                        objDsrList.Add(tempmodel);
                    }


                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return objDsrList;
        }

        public static List<Agency> GetDsrreportbyID(Agency model)
        {
            List<Agency> objDsrList = new List<Agency>();

            try
            {
                string TableName = "T_DailySalesReport";
                string ColumnName = "DSRId,AgencyName,AdminRemark,AgencyId,BusinessEmailId,AvailabeBalance,CreditLimit,DueBalance,AgencyAddress,AgencyBusinessPhoneNo,AgencyFeedback,UserFeedback,ReadStatus,CreatedByName,CreatedByUserID";
                string WhereCondition = "";
                if (model.CreatedByUserName != "AdminAdmin" && model.Type != "adminAdmin")
                {
                    WhereCondition = "CreatedByName='" + model.CreatedByUserName + "'";
                }

                if (model.DsrID > 0)
                {
                    WhereCondition = WhereCondition + "DSRId=" + model.DsrID;
                }
                else
                {
                    if (!string.IsNullOrEmpty(WhereCondition))
                    {
                        WhereCondition = WhereCondition + " order by DSRId desc";
                    }
                    else
                    {
                        WhereCondition = "DSRId>0 order by DSRId desc";
                    }
                }

                DataTable dt = ConnectToDataBase.GetRecordFromTable(ColumnName, TableName, WhereCondition, "seeinsured");

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Agency tempmodel = new Agency();
                        model.DsrID = !string.IsNullOrEmpty(dt.Rows[0]["DsrID"].ToString()) ? Convert.ToInt32(dt.Rows[0]["DsrID"].ToString()) : 0;
                        model.AgencyName = !string.IsNullOrEmpty(dt.Rows[0]["AgencyName"].ToString()) ? dt.Rows[0]["AgencyName"].ToString() : string.Empty;
                        model.Address = !string.IsNullOrEmpty(dt.Rows[0]["AgencyAddress"].ToString()) ? dt.Rows[0]["AgencyAddress"].ToString() : string.Empty;
                        model.BusinessEmailID = !string.IsNullOrEmpty(dt.Rows[0]["BusinessEmailId"].ToString()) ? dt.Rows[0]["BusinessEmailId"].ToString() : string.Empty;
                        model.BusinessPhone = !string.IsNullOrEmpty(dt.Rows[0]["AgencyBusinessPhoneNo"].ToString()) ? dt.Rows[0]["AgencyBusinessPhoneNo"].ToString() : string.Empty;
                        model.AvalibleAmount = !string.IsNullOrEmpty(dt.Rows[0]["AvailabeBalance"].ToString()) ? dt.Rows[0]["AvailabeBalance"].ToString() : string.Empty;
                        model.CreditLimit = !string.IsNullOrEmpty(dt.Rows[0]["CreditLimit"].ToString()) ? dt.Rows[0]["CreditLimit"].ToString() : string.Empty;
                        model.DueAmount = !string.IsNullOrEmpty(dt.Rows[0]["DueBalance"].ToString()) ? dt.Rows[0]["DueBalance"].ToString() : string.Empty;
                        model.AgencyFeedBack = !string.IsNullOrEmpty(dt.Rows[0]["AgencyFeedBack"].ToString()) ? dt.Rows[0]["AgencyFeedBack"].ToString() : string.Empty;
                        model.UserFeedBack = !string.IsNullOrEmpty(dt.Rows[0]["UserFeedBack"].ToString()) ? dt.Rows[0]["UserFeedBack"].ToString() : string.Empty;
                        model.CreatedByUserName = !string.IsNullOrEmpty(dt.Rows[0]["CreatedByName"].ToString()) ? dt.Rows[0]["CreatedByName"].ToString() : string.Empty;
                        model.ReadStatus = !string.IsNullOrEmpty(dt.Rows[0]["ReadStatus"].ToString()) ? dt.Rows[0]["ReadStatus"].ToString() : string.Empty;
                        objDsrList.Add(tempmodel);
                    }


                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return objDsrList;
        }
        public static bool ReadbyAdminUpdate(Agency model)
        {
            try
            {
                string TableName = "T_DailySalesReport";
                string FieldsWithValue = "ReadStatus='" + model.ReadStatus + "',UpdatedDate=getdate()";
                string WhereCondition = "DSRId=" + model.DsrID + "";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(TableName, FieldsWithValue, WhereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool ReplybyAdminUpdate(Agency model)
        {
            try
            {
                string TableName = "T_DailySalesReport";
                string FieldsWithValue = "AdminRemark='" + model.AdminRemark + "',UpdatedDate=getdate()";
                string WhereCondition = "DSRId=" + model.DsrID + "";

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(TableName, FieldsWithValue, WhereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static string Reverse(string str, string isAdd)
        {
            string date = string.Empty;
            string month = string.Empty;
            string year = string.Empty;
            string result = string.Empty;

            if (!string.IsNullOrEmpty(str))
            {
                string[] objStr = str.Split('-');

                for (int i = 0; i < objStr.Count(); i++)
                {
                    if (i == 0)
                    {
                        //if (!string.IsNullOrEmpty(isAdd))
                        //{
                        //    int addedDate = Utility.ConvertToInteger(objStr[i]) + 1;
                        //    if (addedDate < 32)
                        //    {
                        //        date = addedDate.ToString();
                        //    }
                        //    else
                        //    {
                        //        date = objStr[i];
                        //    }
                        //}
                        //else
                        //{
                        date = objStr[i];
                        //}
                    }
                    else if (i == 1)
                    {
                        month = objStr[i];
                    }
                    else if (i == 2)
                    {
                        year = objStr[i];
                    }
                }

                if (!string.IsNullOrEmpty(date) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    if (!string.IsNullOrEmpty(isAdd) && isAdd == "removehifan")
                    {
                        result = year + "" + month + "" + date;
                    }
                    else
                    {
                        result = year + "-" + month + "-" + date;
                    }
                }
            }

            return result;
        }

        public static List<LeadRequestModel> GetleadRequestlist(LeadRequestModel model)
        {
            List<LeadRequestModel> result = new List<LeadRequestModel>();

            try
            {
                string fileds = "LeadId,AgencyName,AgencyID,Name,Email,ContactNo,Age,Gender,Service,Remark,CreatedDate";
                string tablename = "T_LeadRequest";
                string whereCondition = string.Empty;
                if (!string.IsNullOrEmpty(model.FormDate) && !string.IsNullOrEmpty(model.ToDate))
                {
                    whereCondition = "CreatedDate>='" + Reverse(model.FormDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'and CreatedDate<='" + Reverse(model.ToDate.Trim().Replace('/', '-'), string.Empty) + " 23:59:59" + "''";
                }
                else if (!string.IsNullOrEmpty(model.FormDate))
                {
                    whereCondition = "CreatedDate>='" + Reverse(model.FormDate.Trim().Replace('/', '-'), string.Empty) + " 00:00:00" + "'";
                }
                else
                {
                    whereCondition = "CreatedDate>='" + model.TempDate + "' order by LeadId desc";
                }
                DataTable dtMember = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMember.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMember.Rows.Count; i++)
                    {
                        LeadRequestModel tempmodel = new LeadRequestModel();
                        tempmodel.RequestId = Convert.ToInt32(dtMember.Rows[i]["LeadId"].ToString());
                        tempmodel.AgencyName = !string.IsNullOrEmpty(dtMember.Rows[i]["AgencyName"].ToString()) ? dtMember.Rows[i]["AgencyName"].ToString() : string.Empty;
                        tempmodel.AgencyID = !string.IsNullOrEmpty(dtMember.Rows[i]["AgencyID"].ToString()) ? dtMember.Rows[i]["AgencyID"].ToString() : string.Empty;
                        tempmodel.Name = !string.IsNullOrEmpty(dtMember.Rows[i]["Name"].ToString()) ? dtMember.Rows[i]["Name"].ToString() : string.Empty;
                        tempmodel.Email = !string.IsNullOrEmpty(dtMember.Rows[i]["Email"].ToString()) ? dtMember.Rows[i]["Email"].ToString() : string.Empty;
                        tempmodel.ContactNo = !string.IsNullOrEmpty(dtMember.Rows[i]["ContactNo"].ToString()) ? dtMember.Rows[i]["ContactNo"].ToString() : string.Empty;
                        tempmodel.Age = !string.IsNullOrEmpty(dtMember.Rows[i]["Age"].ToString()) ? dtMember.Rows[i]["Age"].ToString() : string.Empty;
                        tempmodel.Gender = !string.IsNullOrEmpty(dtMember.Rows[i]["Gender"].ToString()) ? dtMember.Rows[i]["Gender"].ToString() : string.Empty;
                        tempmodel.Service = !string.IsNullOrEmpty(dtMember.Rows[i]["Service"].ToString()) ? dtMember.Rows[i]["Service"].ToString() : string.Empty;
                        tempmodel.Remark = !string.IsNullOrEmpty(dtMember.Rows[i]["Remark"].ToString()) ? dtMember.Rows[i]["Remark"].ToString() : string.Empty;
                        tempmodel.CreatedDate = !string.IsNullOrEmpty(dtMember.Rows[i]["CreatedDate"].ToString()) ? dtMember.Rows[i]["CreatedDate"].ToString() : string.Empty;
                        result.Add(tempmodel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
    }
}