﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.Models.AdminArea_Motor;
using insurance.Helper.DataBase;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public class AdminArea_MotorHelper
    {
        public static List<NCBPercentage> GetNCBList()
        {
            List<NCBPercentage> ncbList = new List<NCBPercentage>();
            try
            {
                string fileds = "*";
                string tablename = "tblMaster_NCBPercentage";
                string whereCondition = string.Empty;

                DataTable dtRole = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");

                if (dtRole.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRole.Rows.Count; i++)
                    {
                        NCBPercentage model = new NCBPercentage();
                        model.ID = Convert.ToInt32(dtRole.Rows[i]["ID"].ToString());
                        model.ncbpercentage = dtRole.Rows[i]["ncbpercentage"].ToString();
                        model.ncbgodigit = dtRole.Rows[i]["ncbgodigit"].ToString();
                        model.ncbshriram = dtRole.Rows[i]["ncbshriram"].ToString();
                        ncbList.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ncbList;
        }
    }
}