﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Helper.DataBase;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_GroupHelper
    {
        public static List<GroupDetailModel> GetGroupList(string groupId = null, string status = null)
        {
            List<GroupDetailModel> result = new List<GroupDetailModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_GroupDetail";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status=" + status;
                }

                if (!string.IsNullOrEmpty(groupId))
                {
                    whereCondition += " and GroupId=" + groupId;
                }

                DataTable dtCommission = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtCommission.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCommission.Rows.Count; i++)
                    {
                        GroupDetailModel model = new GroupDetailModel();
                        model.GroupId = Convert.ToInt32(dtCommission.Rows[i]["GroupId"].ToString());
                        model.GroupName = !string.IsNullOrEmpty(dtCommission.Rows[i]["GroupName"].ToString()) ? dtCommission.Rows[i]["GroupName"].ToString() : string.Empty;
                        model.Status = dtCommission.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtCommission.Rows[i]["CreateDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool UpdateGroupStatus(string groupid, string status)
        {
            try
            {
                string tableName = "T_GroupDetail";

                string fieldsWithValue = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    fieldsWithValue = "Status=" + status;
                }

                string whereCondition = "GroupId=" + groupid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool InsertUpdateGroupDetail(string groupid, string groupname)
        {
            try
            {
                if (!string.IsNullOrEmpty(groupid) && Convert.ToInt32(groupid) > 0)
                {
                    string tableName = "T_GroupDetail";
                    string fieldsWithValue = "GroupName='" + groupname + "'";
                    string whereCondition = "GroupId=" + groupid;

                    if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(groupname))
                    {
                        string fileds = "GroupName";
                        string fieldValue = "'" + groupname + "'";
                        string tableName = "T_GroupDetail";

                        if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
    }
}