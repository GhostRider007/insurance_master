﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
//using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Areas.AdminArea.Models.AdminArea_Commission;

using insurance.Helper.DataBase;
using Post_Utility.Admin_Backend;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_CommissionHelper
    {
        public static List<Models.AdminArea_Custom.Commission> GetCommissionList(string commisionId = null, string status = null)
        {
            List<Models.AdminArea_Custom.Commission> result = new List<Models.AdminArea_Custom.Commission>();

            try
            {
                DataTable dtCommission = AdminBackend_CommissionHelper.GetCommissionList(commisionId, status);

                if (dtCommission.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCommission.Rows.Count; i++)
                    {
                        Models.AdminArea_Custom.Commission model = new Models.AdminArea_Custom.Commission();
                        model.CommisionId = Convert.ToInt32(dtCommission.Rows[i]["CommisionId"].ToString());
                        model.BasicPer = !string.IsNullOrEmpty(dtCommission.Rows[i]["BasicPer"].ToString()) ? dtCommission.Rows[i]["BasicPer"].ToString() : string.Empty;
                        model.BasicTDS = !string.IsNullOrEmpty(dtCommission.Rows[i]["BasicTDS"].ToString()) ? dtCommission.Rows[i]["BasicTDS"].ToString() : string.Empty;
                        model.IncentivePer = !string.IsNullOrEmpty(dtCommission.Rows[i]["IncentivePer"].ToString()) ? dtCommission.Rows[i]["IncentivePer"].ToString() : string.Empty;
                        model.IncentiveTDS = !string.IsNullOrEmpty(dtCommission.Rows[i]["IncentiveTDS"].ToString()) ? dtCommission.Rows[i]["IncentiveTDS"].ToString() : string.Empty;
                        model.Status = dtCommission.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtCommission.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertCommissionDetail(Models.AdminArea_Custom.Commission comm)
        {
            if (AdminBackend_CommissionHelper.InsertCommissionDetail(comm.BasicPer, comm.BasicTDS, comm.IncentivePer, comm.IncentiveTDS))
            {
                return true;
            }

            return false;
        }
        public static bool UpdateCommissionStatus(string commid, string status)
        {
            if (AdminBackend_CommissionHelper.UpdateCommissionStatus(commid, status))
            {
                return true;
            }

            return false;
        }
        public static bool UpdateCommissionDetail(Models.AdminArea_Custom.Commission comm)
        {
            if (AdminBackend_CommissionHelper.UpdateCommissionDetail(comm.BasicPer, comm.BasicTDS, comm.IncentivePer, comm.IncentiveTDS, comm.CommisionId))
            {
                return true;
            }

            return false;
        }

        //=========================================NEW ONES============================================================
        public static List<Commission> ComissionList(int? id, string insuramceType, string status = null)
        {
            List<Commission> result = new List<Commission>();

            try
            {
                DataTable dtCommission = AdminBackend_CommissionHelper.CommissionList(id, insuramceType, status);

                if (dtCommission.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCommission.Rows.Count; i++)
                    {
                        Commission model = new Commission();
                        model.id = Convert.ToInt32(dtCommission.Rows[i]["id"].ToString());
                        model.AgencyId = Convert.ToInt32(dtCommission.Rows[i]["AgencyId"].ToString());
                        model.AgencyName = !string.IsNullOrEmpty(dtCommission.Rows[i]["AgencyName"].ToString()) ? dtCommission.Rows[i]["AgencyName"].ToString() : string.Empty;
                        model.GroupId = Convert.ToInt32(dtCommission.Rows[i]["GroupId"].ToString());
                        model.GroupName = !string.IsNullOrEmpty(dtCommission.Rows[i]["GroupName"].ToString()) ? dtCommission.Rows[i]["GroupName"].ToString() : string.Empty;
                        model.CompanyId = Convert.ToInt32(dtCommission.Rows[i]["CompanyId"].ToString());
                        model.CompanyName = !string.IsNullOrEmpty(dtCommission.Rows[i]["CompanyName"].ToString()) ? dtCommission.Rows[i]["CompanyName"].ToString() : string.Empty;
                        model.ProductCode = !string.IsNullOrEmpty(dtCommission.Rows[i]["ProductCode"].ToString()) ? dtCommission.Rows[i]["ProductCode"].ToString() : string.Empty;
                        model.ProductName = !string.IsNullOrEmpty(dtCommission.Rows[i]["ProductName"].ToString()) ? dtCommission.Rows[i]["ProductName"].ToString() : string.Empty;
                        model.CommissionType = !string.IsNullOrEmpty(dtCommission.Rows[i]["CommissionType"].ToString()) ? dtCommission.Rows[i]["CommissionType"].ToString() : string.Empty;
                        model.BaseCommssion = Convert.ToDecimal(dtCommission.Rows[i]["BaseCommssion"].ToString());
                        model.TDS = Convert.ToDecimal(dtCommission.Rows[i]["TDS"].ToString());
                        model.Status = Convert.ToBoolean(dtCommission.Rows[i]["Status"].ToString());
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool AddEditCommission(Commission model)
        {
            bool isSuccess = false;
            try
            {
                Dictionary<string, string> dObj_Str = new Dictionary<string, string>();
                Dictionary<string, object> dObj = new Dictionary<string, object>();

                dObj_Str.Add("AgencyName", model.AgencyName);
                dObj_Str.Add("GroupName", model.GroupName);
                dObj_Str.Add("CompanyName", model.CompanyName);
                dObj_Str.Add("ProductCode", model.ProductCode);
                dObj_Str.Add("ProductName", model.ProductName);
                dObj_Str.Add("CommissionType", model.CommissionType);
                dObj_Str.Add("InsuranceType", model.InsuranceType);

                dObj.Add("AgencyId", model.AgencyId);
                dObj.Add("GroupId", model.GroupId);
                dObj.Add("CompanyId", model.CompanyId);
                dObj.Add("BaseCommssion", model.BaseCommssion);
                dObj.Add("TDS", model.TDS);
                dObj.Add("Status", 1);

                if (model.id > 0)
                {
                    dObj.Add("id", model.id);
                    isSuccess = AdminBackend_CommissionHelper.UpdateCommission(dObj, dObj_Str);
                }
                else
                {
                    bool ifExist = AdminBackend_CommissionHelper.CheckIfRecordExistCommission(dObj, dObj_Str);
                    if (!ifExist)
                    {
                        isSuccess = AdminBackend_CommissionHelper.InsertCommission(dObj, dObj_Str);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }
        public static bool AddEditInsentiveComission(IncentiveCommission model)
        {
            Dictionary<string, string> incentive = new Dictionary<string, string>();

            incentive.Add("GroupId", model.GroupId);
            incentive.Add("GroupName", model.GroupName);
            incentive.Add("AgencyId", model.AgencyId);
            incentive.Add("AgencyName", model.AgencyName);
            incentive.Add("InsurerId", model.InsurerId);
            incentive.Add("InsurerName", model.InsurerName);
            incentive.Add("ProductCode", model.ProductCode);
            incentive.Add("ProductName", model.ProductName);
            incentive.Add("IRDAI", model.IRDAI);
            incentive.Add("AdditinalSI_1", model.AdditinalSI_1);
            incentive.Add("AdditinalSI_1Val", model.AdditinalSI_1Val);
            incentive.Add("AdditinalSI_2", model.AdditinalSI_2);
            incentive.Add("AdditinalSI_2Val", model.AdditinalSI_2Val);
            incentive.Add("AdditinalSI_3", model.AdditinalSI_3);
            incentive.Add("AdditinalSI_3Val", model.AdditinalSI_3Val);
            incentive.Add("AdditinalSI_4", model.AdditinalSI_4);
            incentive.Add("AdditinalSI_4Val", model.AdditinalSI_4Val);
            incentive.Add("AdditinalSI_5", model.AdditinalSI_5);
            incentive.Add("AdditinalSI_5Val", model.AdditinalSI_5Val);
            incentive.Add("AdditinalSI_6", model.AdditinalSI_6);
            incentive.Add("AdditinalSI_6Val", model.AdditinalSI_6Val);
            incentive.Add("AdditinalSI_7", model.AdditinalSI_7);
            incentive.Add("AdditinalSI_7Val", model.AdditinalSI_7Val);
            incentive.Add("AdditinalSI_8", model.AdditinalSI_8);
            incentive.Add("AdditinalSI_8Val", model.AdditinalSI_8Val);
            incentive.Add("AdditinalSI_9", model.AdditinalSI_9);
            incentive.Add("AdditinalSI_9Val", model.AdditinalSI_9Val);
            incentive.Add("AdditinalSI_10", model.AdditinalSI_10);
            incentive.Add("AdditinalSI_10Val", model.AdditinalSI_10Val);
            incentive.Add("AdditinalSI_11", model.AdditinalSI_11);
            incentive.Add("AdditinalSI_11Val", model.AdditinalSI_11Val);

            bool ifExist = AdminBackend_CommissionHelper.CheckIfRecordExistInsentiveCommission(incentive);
            if (!ifExist)
            {
                return AdminBackend_CommissionHelper.AddEditInsentiveComission(model.id, incentive);
            }
            return false;
        }
        public static bool DeleteCommission(string id)
        {
            return AdminBackend_CommissionHelper.DeleteCommission(id);
        }
        public static List<IncentiveCommission> GetHealthInsentiveComissionList(string status, string id = null)
        {
            List<IncentiveCommission> incentiveList = new List<IncentiveCommission>();
            try
            {
                DataTable dtIncentive = AdminBackend_CommissionHelper.GetHealthInsentiveComissionList(status, id);
                if (dtIncentive != null && dtIncentive.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtIncentive.Rows.Count; i++)
                    {
                        IncentiveCommission incentive = new IncentiveCommission();
                        incentive.id = Convert.ToInt32(dtIncentive.Rows[i]["id"].ToString());
                        incentive.GroupId = dtIncentive.Rows[i]["GroupId"].ToString();
                        incentive.GroupName = dtIncentive.Rows[i]["GroupName"].ToString();
                        incentive.AgencyId = dtIncentive.Rows[i]["AgencyId"].ToString();
                        incentive.AgencyName = dtIncentive.Rows[i]["AgencyName"].ToString();
                        incentive.InsurerId = dtIncentive.Rows[i]["InsurerId"].ToString();
                        incentive.InsurerName = dtIncentive.Rows[i]["InsurerName"].ToString();
                        incentive.ProductCode = dtIncentive.Rows[i]["ProductCode"].ToString();
                        incentive.ProductName = dtIncentive.Rows[i]["ProductName"].ToString();
                        incentive.IRDAI = dtIncentive.Rows[i]["IRDAI"].ToString();
                        incentive.AdditinalSI_1 = dtIncentive.Rows[i]["AdditinalSI_1"].ToString();
                        incentive.AdditinalSI_1Val = dtIncentive.Rows[i]["AdditinalSI_1Val"].ToString();
                        incentive.AdditinalSI_2 = dtIncentive.Rows[i]["AdditinalSI_2"].ToString();
                        incentive.AdditinalSI_2Val = dtIncentive.Rows[i]["AdditinalSI_2Val"].ToString();
                        incentive.AdditinalSI_3 = dtIncentive.Rows[i]["AdditinalSI_3"].ToString();
                        incentive.AdditinalSI_3Val = dtIncentive.Rows[i]["AdditinalSI_3Val"].ToString();
                        incentive.AdditinalSI_4 = dtIncentive.Rows[i]["AdditinalSI_4"].ToString();
                        incentive.AdditinalSI_4Val = dtIncentive.Rows[i]["AdditinalSI_4Val"].ToString();
                        incentive.AdditinalSI_5 = dtIncentive.Rows[i]["AdditinalSI_5"].ToString();
                        incentive.AdditinalSI_5Val = dtIncentive.Rows[i]["AdditinalSI_5Val"].ToString();
                        incentive.AdditinalSI_6 = dtIncentive.Rows[i]["AdditinalSI_6"].ToString();
                        incentive.AdditinalSI_6Val = dtIncentive.Rows[i]["AdditinalSI_6Val"].ToString();
                        incentive.AdditinalSI_7 = dtIncentive.Rows[i]["AdditinalSI_7"].ToString();
                        incentive.AdditinalSI_7Val = dtIncentive.Rows[i]["AdditinalSI_7Val"].ToString();
                        incentive.AdditinalSI_8 = dtIncentive.Rows[i]["AdditinalSI_8"].ToString();
                        incentive.AdditinalSI_8Val = dtIncentive.Rows[i]["AdditinalSI_8Val"].ToString();
                        incentive.AdditinalSI_9 = dtIncentive.Rows[i]["AdditinalSI_9"].ToString();
                        incentive.AdditinalSI_9Val = dtIncentive.Rows[i]["AdditinalSI_9Val"].ToString();
                        incentive.AdditinalSI_10 = dtIncentive.Rows[i]["AdditinalSI_10"].ToString();
                        incentive.AdditinalSI_10Val = dtIncentive.Rows[i]["AdditinalSI_10Val"].ToString();
                        incentive.AdditinalSI_11 = dtIncentive.Rows[i]["AdditinalSI_11"].ToString();
                        incentive.AdditinalSI_11Val = dtIncentive.Rows[i]["AdditinalSI_11Val"].ToString();
                        incentive.Status = Convert.ToBoolean(dtIncentive.Rows[i]["Status"].ToString());

                        incentiveList.Add(incentive);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return incentiveList;
        }
        public static bool UpdateInsentiveComission(string id, string status)
        {
            return AdminBackend_CommissionHelper.UpdateInsentiveComission(id, status);
        }
        public static Dictionary<string, string> GetCompanyList(string type)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            DataTable dt = AdminBackend_CommissionHelper.GetCompanyList(type);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i]["CompanyId"].ToString(), dt.Rows[i]["CompanyName"].ToString());
                }
            }
            return list;
        }
        public static Dictionary<string, string> GetProductList(string type, string CompanyId, string vehicleType = null)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            DataTable dt = AdminBackend_CommissionHelper.GetProductList(type, CompanyId, vehicleType);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i]["ProductId"].ToString(), dt.Rows[i]["ProductName"].ToString());
                }
            }
            return list;
        }
        public static List<CommissionReport> GenerateCommissionReport(string agencyId, string time)
        {
            List<CommissionReport> result = new List<CommissionReport>();
            try
            {
                DataTable dtComm = AdminBackend_CommissionHelper.GetCommisionLedgerListForReport(agencyId, time, "health");
                if (dtComm != null && dtComm.Rows.Count > 0)
                {
                    for (int i = 0; i < dtComm.Rows.Count; i++)
                    {
                        CommissionReport item = new CommissionReport();
                        item.AgencyName = dtComm.Rows[i]["AgencyName"].ToString();
                        item.Insurer = dtComm.Rows[i]["Insurer"].ToString();
                        item.ProductName = dtComm.Rows[i]["ProductName"].ToString();
                        item.TotalSale = dtComm.Rows[i]["TotalSale"].ToString();
                        item.GroupId = dtComm.Rows[i]["GroupId"].ToString();
                        item.GroupName = dtComm.Rows[i]["GroupName"].ToString();
                        item.IncentivePercentage = dtComm.Rows[i]["IncentivePercentage"].ToString();
                        if (!string.IsNullOrEmpty(item.TotalSale) && !string.IsNullOrEmpty(item.IncentivePercentage))
                        {
                            item.IncAmount = (Math.Round(((Convert.ToDecimal(item.TotalSale) * Convert.ToDecimal(item.IncentivePercentage)) / 10), 2)).ToString();
                        }
                        result.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}