﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.Models.AdminArea_Common;
using insurance.Helper.DataBase;
using insurance.Models;
using Post_Utility.Admin_Backend;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Models.Common.CommonModel;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_AccountHelper
    {
        public static bool LoginMember(BackEndLogin login, ref string msg, ref string loginId)
        {
            bool isLogin = false;

            try
            {
                if (!string.IsNullOrEmpty(login.UserId) && !string.IsNullOrEmpty(login.Password))
                {
                    DataTable dtMember = AdminBackend_AccountHelper.GetLoginMemberByUserId(login.UserId, login.Password);

                    if (dtMember.Rows.Count > 0)
                    {
                        bool status = dtMember.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;

                        if (status)
                        {
                            loginId = dtMember.Rows[0]["LoginId"].ToString();
                            isLogin = true;
                            AdminArea_CommonModel.InitializeUserSession(dtMember, true);
                            if (login.Remember)
                            {
                                HttpCookie cook = new HttpCookie("backendlogincookie");
                                cook.Value = dtMember.Rows[0]["LoginId"].ToString();
                                cook.Expires = DateTime.Now.AddYears(1);
                                HttpContext.Current.Response.Cookies.Add(cook);
                            }
                        }
                        else
                        {
                            isLogin = false;
                            msg = "Your login suspended by admin !";
                        }
                    }
                    else
                    {
                        isLogin = false;
                        msg = "Incorrect userid and password !";
                    }
                }
                else
                {
                    isLogin = false;
                    msg = "Userid and password required !";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                msg = "Error: Network not available!";
            }

            return isLogin;
        }
        public static bool LogoutBackendLogin()
        {
            HttpContext.Current.Session.Remove("backendloginsession");
            if (HttpContext.Current.Request.Cookies["backendlogincookie"] != null)
            {
                HttpContext.Current.Response.Cookies["backendlogincookie"].Expires = DateTime.Now.AddYears(-1);
            }

            return true;
        }
        public static bool IsBackUserLogin(ref MemberLogin lu)
        {
            List<MemberLogin> objLoginUser = new List<MemberLogin>();
            bool isLogin = IsUserLoginSuccess(ref objLoginUser);

            if (isLogin)
            {
                lu = objLoginUser[0];
            }

            return isLogin;
        }
        public static bool IsUserLoginSuccess(ref List<MemberLogin> loginUserList)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["backendloginsession"] == null && HttpContext.Current.Request.Cookies["backendlogincookie"] != null)
                {
                    int? loginId = Convert.ToInt32(HttpContext.Current.Request.Cookies["backendlogincookie"].Value);
                    DataTable dtMember = AdminBackend_AccountHelper.IsUserLoginSuccess(loginId.Value);
                    if (dtMember.Rows.Count > 0)
                    {
                        bool status = dtMember.Rows[0]["Status"].ToString().ToLower() == "true" ? true : false;

                        if (status)
                        {
                            AdminArea_CommonModel.InitializeUserSession(dtMember, status);
                        }
                    }
                }

                if (HttpContext.Current.Session["backendloginsession"] != null)
                {
                    loginUserList = (List<MemberLogin>)HttpContext.Current.Session["backendloginsession"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }
        public static bool IsUserAlreadyExist(string userId)
        {
            DataTable dtMember = AdminBackend_AccountHelper.IsUserAlreadyExist(userId);
            if (dtMember.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        public static bool UpdateMemberImage(string memberId, string imageurlname)
        {
            if (AdminBackend_AccountHelper.UpdateMemberImage(memberId, imageurlname))
            {
                return true;
            }

            return false;
        }
        public static bool AdminMemberResetPassword(MemberLogin model, ref string msg)
        {
            bool value = false;
            try
            {
                DataTable dtMember = AdminBackend_AccountHelper.GetLoginMemberByLoginId(model.LoginId.ToString());
                if (dtMember != null && dtMember.Rows.Count > 0)
                {
                    string member_pass = dtMember.Rows[0]["Password"].ToString();
                    if (model.CurrentPassword == member_pass)
                    {
                        if (AdminBackend_AccountHelper.Update_MemberPassword(model.Password, model.LoginId.ToString()))
                        {
                            msg = "Your password changed successfully.";
                            value = true;
                        }
                    }
                    else
                    {
                        msg = "You have entered wrong current password.";
                        value = false;
                    }
                }
                else
                {
                    msg = "You are not authorized to change this user password.";
                    value = false;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return value;
        }
        public static bool Admin_ChangeAgencyPassword(CreateAgency model)
        {
            string password = Security.ApiBase64Encode(model.Password);
            return AdminBackend_AccountHelper.Admin_ChangeAgencyPassword(password, model.AgencyID);
        }

        #region[Booking Details]
        public static DataTable GetBookingDetails(string fromDate = null, string toDate = null, string AgencyId = null, string InsuranceType = null, string Insurer = null, string Product = null, string CustomerName = null)
        {
            DataTable dtHealth = new DataTable();
            DataTable dtMotor = new DataTable();
            DataTable dt = new DataTable();
            try
            {
                if (string.IsNullOrEmpty(InsuranceType) || InsuranceType.ToLower() == "health")
                {
                    dtHealth = AdminBackend_AccountHelper.GetHealth_BookingDetail(fromDate, toDate, AgencyId, InsuranceType, Insurer, Product, CustomerName);
                }
                if (string.IsNullOrEmpty(InsuranceType) || InsuranceType.ToLower() == "motor")
                {
                    dtMotor = AdminBackend_AccountHelper.GetMotor_BookingDetail(fromDate, toDate, AgencyId, InsuranceType, Insurer, Product, CustomerName);
                }

                dt.Merge(dtHealth);
                dt.Merge(dtMotor);

                dt.DefaultView.Sort = "PolicyDate desc";
                dt = dt.DefaultView.ToTable();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }
        public static List<ddlBindClass> BindAgencies()
        {

            List<ddlBindClass> result = new List<ddlBindClass>();

            try
            {
                DataTable dt = AdminBackend_AccountHelper.GetAgentDetail();

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlBindClass model = new ddlBindClass();

                        model.Key = dt.Rows[i]["AgencyId"].ToString();
                        model.Value = dt.Rows[i]["AgencyName"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<ddlBindClass> BindInsurers(string insuranceType)
        {

            List<ddlBindClass> result = new List<ddlBindClass>();

            try
            {
                DataTable dt = AdminBackend_AccountHelper.GetCompanyDetail(insuranceType);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlBindClass model = new ddlBindClass();

                        model.Key = dt.Rows[i]["CompanyId"].ToString();
                        model.Value = dt.Rows[i]["CompanyName"].ToString();
                        model.Data1 = dt.Rows[i]["ImgUrl"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<ddlBindClass> BindProducts(string insuranceType, string insuranceId)
        {

            List<ddlBindClass> result = new List<ddlBindClass>();

            try
            {
                DataTable dt = AdminBackend_AccountHelper.GetCompanyProductDetail(insuranceType, insuranceId);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ddlBindClass model = new ddlBindClass();

                        model.Key = dt.Rows[i]["ProductName"].ToString();
                        model.Value = dt.Rows[i]["ProductName"].ToString();
                        model.Data1 = dt.Columns.Contains("ProductId") ? dt.Rows[i]["ProductId"].ToString() : "N/A";
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        #endregion
    }
}