﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using insurance.Helper;
using insurance.Helper.DataBase;
using insurance.Models;
using Post_Utility.Admin_Backend;
using Post_Utility.Utility;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_AgencyHelper
    {
        public static List<Agency> GetAgencyList(string agencyId = null, string status = null)
        {
            List<Agency> result = new List<Agency>();

            try
            {
                DataTable dtAgency = AdminBackend_AgencyHelper.GetAgencyList(agencyId, status);

                if (dtAgency.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgency.Rows.Count; i++)
                    {
                        Agency tempItem = new Agency();
                        tempItem.AgencyID = Convert.ToInt32(dtAgency.Rows[i]["AgencyID"].ToString());
                        tempItem.AgencyIdNew = "SST" + tempItem.AgencyID;
                        tempItem.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyName"].ToString()) ? dtAgency.Rows[i]["AgencyName"].ToString() : string.Empty;
                        tempItem.Address = !string.IsNullOrEmpty(dtAgency.Rows[i]["Address"].ToString()) ? dtAgency.Rows[i]["Address"].ToString() : string.Empty;
                        
                        tempItem.CountryID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CountryID"].ToString()) ? dtAgency.Rows[i]["CountryID"].ToString() : string.Empty;
                        tempItem.CountryName = FrontAgencyHelper.CountryList(tempItem.CountryID, "1").FirstOrDefault().CountryName;
                        
                        tempItem.StateID = !string.IsNullOrEmpty(dtAgency.Rows[i]["StateID"].ToString()) ? dtAgency.Rows[i]["StateID"].ToString() : string.Empty;
                        var tempState = FrontAgencyHelper.StateList(tempItem.StateID, tempItem.CountryID, "1") ;
                        tempItem.StateName = tempState.Count() > 0 ? tempState.FirstOrDefault().StateName : string.Empty;

                        tempItem.CityID = !string.IsNullOrEmpty(dtAgency.Rows[i]["CityID"].ToString()) ? dtAgency.Rows[i]["CityID"].ToString() : string.Empty;                        
                        tempItem.Pincode = !string.IsNullOrEmpty(dtAgency.Rows[i]["Pincode"].ToString()) ? dtAgency.Rows[i]["Pincode"].ToString() : string.Empty;
                        var tempCity = FrontAgencyHelper.CityList(tempItem.Pincode,null, null,null);
                        tempItem.CityName = tempCity.Count() > 0 ? tempCity.FirstOrDefault().CityName : string.Empty;

                        tempItem.GSTNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["GSTNo"].ToString()) ? dtAgency.Rows[i]["GSTNo"].ToString() : string.Empty;
                        tempItem.BusinessEmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessEmailID"].ToString()) ? dtAgency.Rows[i]["BusinessEmailID"].ToString() : string.Empty;
                        tempItem.BusinessPhone = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPhone"].ToString()) ? dtAgency.Rows[i]["BusinessPhone"].ToString() : string.Empty;
                        tempItem.ReferenceBy = !string.IsNullOrEmpty(dtAgency.Rows[i]["ReferenceBy"].ToString()) ? Convert.ToInt32(dtAgency.Rows[i]["ReferenceBy"].ToString()) : 0;

                        string addressImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessAddressProofImg"].ToString()) ? dtAgency.Rows[i]["BusinessAddressProofImg"].ToString() : string.Empty;
                        tempItem.BusinessAddressProofImg = UtilityClass.GetImagePath(addressImg, "Agency", tempItem.AgencyID);

                        string panImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["BusinessPanCardImg"].ToString()) ? dtAgency.Rows[i]["BusinessPanCardImg"].ToString() : string.Empty;
                        tempItem.BusinessPanCardImg = UtilityClass.GetImagePath(panImg, "Agency", tempItem.AgencyID);
                        tempItem.Title = !string.IsNullOrEmpty(dtAgency.Rows[i]["Title"].ToString()) ? dtAgency.Rows[i]["Title"].ToString() : string.Empty;
                        tempItem.FirstName = !string.IsNullOrEmpty(dtAgency.Rows[i]["FirstName"].ToString()) ? dtAgency.Rows[i]["FirstName"].ToString() : string.Empty;
                        tempItem.LastName = !string.IsNullOrEmpty(dtAgency.Rows[i]["LastName"].ToString()) ? dtAgency.Rows[i]["LastName"].ToString() : string.Empty;
                        tempItem.EmailID = !string.IsNullOrEmpty(dtAgency.Rows[i]["EmailID"].ToString()) ? dtAgency.Rows[i]["EmailID"].ToString() : string.Empty;
                        tempItem.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[i]["Mobile"].ToString()) ? dtAgency.Rows[i]["Mobile"].ToString() : string.Empty;

                        string peraddressImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["PersonalAddressProofImg"].ToString()) ? dtAgency.Rows[i]["PersonalAddressProofImg"].ToString() : string.Empty;
                        tempItem.PersonalAddressProofImg = UtilityClass.GetImagePath(peraddressImg, "Agency", tempItem.AgencyID);
                        tempItem.UserId = !string.IsNullOrEmpty(dtAgency.Rows[i]["UserId"].ToString()) ? dtAgency.Rows[i]["UserId"].ToString() : string.Empty;

                        string password = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? dtAgency.Rows[i]["Password"].ToString() : string.Empty;
                        string encryKey = !string.IsNullOrEmpty(dtAgency.Rows[i]["Password"].ToString()) ? dtAgency.Rows[i]["Password"].ToString() : string.Empty;

                        tempItem.Password = !string.IsNullOrEmpty(password) ? Security.Decrypt(encryKey, password) : string.Empty; //Utility.Decrypt(item.Password) : string.Empty;

                        string companyImg = !string.IsNullOrEmpty(dtAgency.Rows[i]["CompanyLogoImg"].ToString()) ? dtAgency.Rows[i]["CompanyLogoImg"].ToString() : string.Empty;
                        tempItem.CompanyLogoImg = UtilityClass.GetImagePath(companyImg, "Agency", tempItem.AgencyID);
                        tempItem.Status = dtAgency.Rows[i]["Status"].ToString().ToLower().Trim() == "true" ? true : false;
                        tempItem.GroupType = !string.IsNullOrEmpty(dtAgency.Rows[i]["GroupType"].ToString()) ? dtAgency.Rows[i]["GroupType"].ToString() : string.Empty;
                        tempItem.CreatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        tempItem.UpdatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["UpdatedDate"].ToString())).ToString("dd/MM/yyyy");
                        tempItem.IsApproved = dtAgency.Rows[i]["IsApproved"].ToString().ToLower().Trim() == "true" ? true : false;
                        tempItem.IsApprovedStr = dtAgency.Rows[i]["IsApproved"].ToString();
                        tempItem.LastLoginDate = (Convert.ToDateTime(dtAgency.Rows[i]["LastLoginDate"].ToString())).ToString("dd/MM/yyyy");

                        tempItem.AvalibleAmount = "00.00";
                        tempItem.CreditLimit = "00.00";
                        tempItem.DueAmount = "00.00";                        

                        tempItem.Status = dtAgency.Rows[i]["Status"].ToString().ToLower().Trim() == "true" ? true : false;
                        tempItem.CreatedDate = (Convert.ToDateTime(dtAgency.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(tempItem);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
    }
}