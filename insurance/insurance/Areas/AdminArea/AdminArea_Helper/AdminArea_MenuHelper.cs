﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Helper.DataBase;

namespace insurance.Areas.AdminArea.AdminArea_Helper
{
    public static class AdminArea_MenuHelper
    {
        #region [Menu Section]
        public static List<MenuModel> GetMenuList(string menuId = null, string status = null)
        {
            List<MenuModel> result = new List<MenuModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Menu";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status=" + status;
                }

                if (!string.IsNullOrEmpty(menuId))
                {
                    whereCondition += " and MenuId=" + menuId;
                }

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        MenuModel model = new MenuModel();
                        model.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());
                        model.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                        model.ActiveClass = dtMenu.Rows[i]["ActiveClass"].ToString();
                        model.MenuUrl = !string.IsNullOrEmpty(dtMenu.Rows[i]["MenuUrl"].ToString()) ? dtMenu.Rows[i]["MenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtMenu.Rows[i]["Icon"].ToString()) ? dtMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtMenu.Rows[i]["Position"].ToString()) ? dtMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtMenu.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertMenuDetail(MenuModel menu)
        {
            try
            {
                string fileds = "MenuName,MenuUrl,Icon";
                string fieldValue = "'" + menu.MenuName + "','" + menu.MenuUrl + "','" + menu.Icon + "'";
                string tableName = "T_Menu";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateMenuPosition(string menuid, string posval = null, string status = null)
        {
            try
            {
                string tableName = "T_Menu";

                string fieldsWithValue = string.Empty;
                if (!string.IsNullOrEmpty(posval))
                {
                    fieldsWithValue = "Position=" + posval;
                }

                if (!string.IsNullOrEmpty(status))
                {
                    fieldsWithValue = "Status=" + status;
                }

                string whereCondition = "MenuId=" + menuid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateMenuDetail(MenuModel menu)
        {
            try
            {
                string tableName = "T_Menu";
                string fieldsWithValue = "MenuName='" + menu.MenuName + "',MenuUrl='" + menu.MenuUrl + "',Icon='" + menu.Icon + "'";
                string whereCondition = "MenuId=" + menu.MenuId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        #endregion

        #region [Sub Menu Section]
        public static List<SubMenuModel> GetSubMenuList(string submenuId = null, string status = null)
        {
            List<SubMenuModel> result = new List<SubMenuModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_SubMenu";
                string whereCondition = string.Empty;

                if (!string.IsNullOrEmpty(status))
                {
                    whereCondition = "Status=" + status;
                }

                if (!string.IsNullOrEmpty(submenuId))
                {
                    whereCondition += " and SubMenuId=" + submenuId;
                }

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        SubMenuModel model = new SubMenuModel();
                        model.SubMenuId = Convert.ToInt32(dtMenu.Rows[i]["SubMenuId"].ToString());
                        model.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());
                        model.SubMenuName = dtMenu.Rows[i]["SubMenuName"].ToString();
                        model.SubMenuUrl = !string.IsNullOrEmpty(dtMenu.Rows[i]["SubMenuUrl"].ToString()) ? dtMenu.Rows[i]["SubMenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtMenu.Rows[i]["Icon"].ToString()) ? dtMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtMenu.Rows[i]["Position"].ToString()) ? dtMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        model.CreatedDate = (Convert.ToDateTime(dtMenu.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static bool InsertSubMenuDetail(SubMenuModel subMenu)
        {
            try
            {
                string fileds = "MenuId,SubMenuName,SubMenuUrl,Icon";
                string fieldValue = "" + subMenu.MenuId + ",'" + subMenu.SubMenuName + "','" + subMenu.SubMenuUrl + "','" + subMenu.Icon + "'";
                string tableName = "T_SubMenu";

                if (ConnectToDataBase.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateSubMenuPosition(string subMenuid, string posval = null, string status = null)
        {
            try
            {
                string tableName = "T_SubMenu";

                string fieldsWithValue = string.Empty;
                if (!string.IsNullOrEmpty(posval))
                {
                    fieldsWithValue = "Position=" + posval;
                }

                if (!string.IsNullOrEmpty(status))
                {
                    fieldsWithValue = "Status=" + status;
                }

                string whereCondition = "SubMenuId=" + subMenuid;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        public static bool UpdateSubMenuDetail(SubMenuModel subMenu)
        {
            try
            {
                string tableName = "T_SubMenu";
                string fieldsWithValue = "MenuId=" + subMenu.MenuId + ",SubMenuName='" + subMenu.SubMenuName + "',SubMenuUrl='" + subMenu.SubMenuUrl + "',Icon='" + subMenu.Icon + "'";
                string whereCondition = "SubMenuId=" + subMenu.SubMenuId;

                if (ConnectToDataBase.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return false;
        }
        #endregion

        #region[SetMenuForStaff]
        public static List<StaffMenuModel> GetMenuByRoleList(int roleId)
        {
            List<StaffMenuModel> result = new List<StaffMenuModel>();

            try
            {
                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable("*", "T_Menu", "status=1 and menuid in (select menuid from T_SetRolePermission where roleid=" + roleId + ") order by Position", "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        StaffMenuModel model = new StaffMenuModel();
                        model.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());
                        model.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                        model.ActiveClass = dtMenu.Rows[i]["ActiveClass"].ToString();
                        model.MenuUrl = !string.IsNullOrEmpty(dtMenu.Rows[i]["MenuUrl"].ToString()) ? dtMenu.Rows[i]["MenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtMenu.Rows[i]["Icon"].ToString()) ? dtMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtMenu.Rows[i]["Position"].ToString()) ? dtMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static List<SubMenuModel> GetSubMenuByRoleList(int roleId)
        {
            List<SubMenuModel> result = new List<SubMenuModel>();

            try
            {
                DataTable dtSubMenu = ConnectToDataBase.GetRecordFromTable("*", "T_SubMenu", "status=1 and submenuid in (select submenuid from T_SetRolePermission where roleid=" + roleId + ")", "seeinsured");

                if (dtSubMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSubMenu.Rows.Count; i++)
                    {
                        SubMenuModel model = new SubMenuModel();
                        model.SubMenuId = Convert.ToInt32(dtSubMenu.Rows[i]["SubMenuId"].ToString());
                        model.MenuId = Convert.ToInt32(dtSubMenu.Rows[i]["MenuId"].ToString());
                        model.SubMenuName = dtSubMenu.Rows[i]["SubMenuName"].ToString();
                        model.SubMenuUrl = !string.IsNullOrEmpty(dtSubMenu.Rows[i]["SubMenuUrl"].ToString()) ? dtSubMenu.Rows[i]["SubMenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtSubMenu.Rows[i]["Icon"].ToString()) ? dtSubMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtSubMenu.Rows[i]["Position"].ToString()) ? dtSubMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtSubMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<StaffMenuModel> StaffMenuList(int? menuID)
        {
            List<StaffMenuModel> result = new List<StaffMenuModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_Menu";
                string whereCondition = "Status=1";

                if (menuID > 0)
                {
                    whereCondition += " and MenuId=" + menuID;
                }

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        StaffMenuModel model = new StaffMenuModel();
                        model.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());
                        model.MenuName = dtMenu.Rows[i]["MenuName"].ToString();
                        model.ActiveClass = dtMenu.Rows[i]["ActiveClass"].ToString();
                        model.MenuUrl = !string.IsNullOrEmpty(dtMenu.Rows[i]["MenuUrl"].ToString()) ? dtMenu.Rows[i]["MenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtMenu.Rows[i]["Icon"].ToString()) ? dtMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtMenu.Rows[i]["Position"].ToString()) ? dtMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<SubMenuModel> StaffSubMenuList(int? submenuID)
        {
            List<SubMenuModel> result = new List<SubMenuModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_SubMenu";
                string whereCondition = "Status=1";

                if (submenuID > 0)
                {
                    whereCondition += " and SubMenuId=" + submenuID;
                }

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        SubMenuModel model = new SubMenuModel();
                        model.SubMenuId = Convert.ToInt32(dtMenu.Rows[i]["SubMenuId"].ToString());
                        model.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());
                        model.SubMenuName = dtMenu.Rows[i]["SubMenuName"].ToString();
                        model.SubMenuUrl = !string.IsNullOrEmpty(dtMenu.Rows[i]["SubMenuUrl"].ToString()) ? dtMenu.Rows[i]["SubMenuUrl"].ToString() : string.Empty;
                        model.Icon = !string.IsNullOrEmpty(dtMenu.Rows[i]["Icon"].ToString()) ? dtMenu.Rows[i]["Icon"].ToString() : string.Empty;
                        model.Position = !string.IsNullOrEmpty(dtMenu.Rows[i]["Position"].ToString()) ? dtMenu.Rows[i]["Position"].ToString() : string.Empty;
                        model.Status = dtMenu.Rows[i]["Status"].ToString().ToLower() == "true" ? true : false;

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return result;
        }
        public static List<SubMenuIdListModel> StafftSubMenuIdList(int? roleid)
        {
            List<SubMenuIdListModel> result = new List<SubMenuIdListModel>();

            try
            {
                string fileds = "*";
                string tablename = "T_SetRolePermission";
                string whereCondition = "RoleID == roleid ORDER BY MenuName";

                DataTable dtMenu = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");

                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        SubMenuIdListModel subMenu = new SubMenuIdListModel();
                        subMenu.SubMenuId = Convert.ToInt32(dtMenu.Rows[i]["SubMenuId"].ToString());
                        subMenu.MenuId = Convert.ToInt32(dtMenu.Rows[i]["MenuId"].ToString());

                        result.Add(subMenu);
                    }
                }
            }

            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion
    }
}