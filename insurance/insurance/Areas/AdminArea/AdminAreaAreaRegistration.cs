﻿using System.Web.Mvc;

namespace insurance.Areas.AdminArea
{
    public class AdminAreaAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AdminArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(name: "AdminArea_AdminLogin", url: "backend/login", defaults: new { controller = "Account", action = "AdminLogin" });
            
            context.MapRoute(name: "Backend_Dashboard", url: "backend-dashboard", defaults: new { controller = "Backend_Dashboard", action = "BackEndDashboard" });
            context.MapRoute(name: "AdminArea_Booking-Report", url: "account/booking-report", defaults: new { controller = "Account", action = "BookingReport" });
            //=================================Menu Section=====================================
            context.MapRoute(name: "Backend_MenuList", url: "menu/menu-list", defaults: new { controller = "Backend_Menu", action = "MenuList" });
            context.MapRoute(name: "Backend_MenuAddEdit", url: "menu/add-edit-menu", defaults: new { controller = "Backend_Menu", action = "AddEditMenu" });
            //=================================Sub Menu Section=====================================
            context.MapRoute(name: "Backend_SubMenuList", url: "menu/submenu-list", defaults: new { controller = "Backend_Menu", action = "SubMenuList" });
            context.MapRoute(name: "Backend_SubMenuAddEdit", url: "menu/add-edit-submenu", defaults: new { controller = "Backend_Menu", action = "AddEditSubMenu" });
            //=================================Role Section=====================================
            context.MapRoute(name: "Backend_RoleList", url: "role-management/role-list", defaults: new { controller = "Backend_Role", action = "RoleList" });
            context.MapRoute(name: "Backend_RoleAddEdit", url: "role-management/add-edit-role", defaults: new { controller = "Backend_Role", action = "AddEditRole" });            
            //=================================Role Permission=====================================
            context.MapRoute(name: "Backend_RolePermission", url: "role-management/role-permission", defaults: new { controller = "Backend_RolePermission", action = "RolePermissionList" });
            context.MapRoute(name: "Backend_AssignRolePermission", url: "role-management/assign-role-permission", defaults: new { controller = "Backend_RolePermission", action = "AssignRolePermission" });
            //context.MapRoute(name: "Backend_RoleAddEdit", url: "role-management/add-edit-role", defaults: new { controller = "Backend_Role", action = "AddEditRole" });
            //=================================Registration Section=====================================
            context.MapRoute(name: "Backend_RegistrationMemberList", url: "registration/member-list", defaults: new { controller = "Backend_Registration", action = "MemberRegistrationList" });
            context.MapRoute(name: "Backend_RegistrationMemberAddEdit", url: "registration/add-edit-member", defaults: new { controller = "Backend_Registration", action = "AddEditMemberRegistration" });

            context.MapRoute(name: "Backend_RegistrationAgencyDeatils", url: "registration/agency-deatils", defaults: new { controller = "Backend_Registration", action = "AgentDetails" });
            context.MapRoute(name: "Backend_RegistrationAgencyList", url: "registration/agency-list", defaults: new { controller = "Backend_Registration", action = "AgencyRegistrationList" }); 
            context.MapRoute(name: "Backend_RegistrationAgencyRegistration", url: "registration/add-edit-agency", defaults: new { controller = "Backend_Registration", action = "AgencyRegistration" });
            //=================================Commission Section=====================================
            context.MapRoute(name: "Backend_CommissionList", url: "management/commission-list", defaults: new { controller = "Backend_Role", action = "CommissionList" });
            context.MapRoute(name: "Backend_CommissionAddEdit", url: "management/add-edit-commission", defaults: new { controller = "Backend_Role", action = "AddEditCommission" });
            //=================================Group Section=====================================
            context.MapRoute(name: "Backend_GroupList", url: "management/group-list", defaults: new { controller = "Backend_Role", action = "GroupList" });
            context.MapRoute(name: "Backend_GroupAddEdit", url: "management/add-edit-group", defaults: new { controller = "Backend_Role", action = "GroupAddEdit" });

            context.MapRoute(name: "Backend_AddDsrReportlist", url: "profile/dsrlist", defaults: new { controller = "DailySalesReport", action = "DsrReportlist" });
            context.MapRoute(name: "Backend_AddDsrReport", url: "profile/add-new-dsr-report", defaults: new { controller = "DailySalesReport", action = "DsrReport" });

            context.MapRoute(name: "Backend_LeadReqlist", url: "profile/lead-requst-list", defaults: new { controller = "DailySalesReport", action = "LeadRequestList" });
            context.MapRoute(name: "Backend_CompanyList", url: "profile/health/company-list", defaults: new { controller = "Backend_Role", action = "CompanyList" });
            context.MapRoute(name: "Backend_CompanyAddEdit", url: "profile/add-edit-company", defaults: new { controller = "Backend_Role", action = "AddEditCompany" });
            context.MapRoute(name: "Backend_ProductList", url: "profile/health/product-list", defaults: new { controller = "Backend_Role", action = "ProductList" });
            context.MapRoute(name: "Backend_ProductListbrochure", url: "profile/health/update-brochure", defaults: new { controller = "Backend_Role", action = "Updatebrochure" });
            context.MapRoute(name: "Backend_ProductListbrochuremotor", url: "profile/motor/update-brochure", defaults: new { controller = "Backend_Role", action = "UpdateMotorbrochure" });

            context.MapRoute(name: "Backend_ProductListClausesLink", url: "profile/health/update-Clauses", defaults: new { controller = "Backend_Role", action = "ClausesLink" });
            context.MapRoute(name: "Backend_ProductListClausesLinkmotor", url: "profile/motor/update-Clauses", defaults: new { controller = "Backend_Role", action = "MotorClausesLink" });

            context.MapRoute(name: "Backend_ProductmotorList", url: "profile/motor/product-list", defaults: new { controller = "Backend_Role", action = "MotorProductList" });
            context.MapRoute(name: "Backend_CompanyAddEditMotor", url: "profile/motor/add-edit-company", defaults: new { controller = "Backend_Role", action = "AddEditCompanymotor" });
            context.MapRoute(name: "Backend_CompanymotorList", url: "profile/motor/company-list", defaults: new { controller = "Backend_Role", action = "MotorCompanyList" });

            context.MapRoute(name: "AdminArea_AdminResetPass", url: "profile/change-password", defaults: new { controller = "Backend_Administration", action = "ChangeAdminPassword" });
            context.MapRoute(name: "Admin_ChangeAgencyPassword", url: "profile/agency-change-password", defaults: new { controller = "Backend_Administration", action = "Admin_ChangeAgencyPassword" });
            context.MapRoute(name: "AdminArea_AdminEmulateLogin", url: "profile/emulate-login", defaults: new { controller = "Backend_Administration", action = "AdminEmulateLogin" });
            context.MapRoute(name: "AdminArea_AdminEmulatorLogin", url: "profile/emulator-login", defaults: new { controller = "Backend_Administration", action = "AdminEmulatorLogin" });
            context.MapRoute(name: "AdminArea_ExtranetBooking", url: "profile/health/extranet-booking", defaults: new { controller = "Backend_Administration", action = "AdminExtranetBooking" });
            context.MapRoute(name: "AdminArea_ExtranetCreateProposal", url: "profile/health/extranet-create-proposal", defaults: new { controller = "Backend_Administration", action = "AdminExtranetCreateProposal" });

            context.MapRoute(name: "AdminArea_ExtranetBooking_Motor", url: "profile/motor/extranet-booking", defaults: new { controller = "Backend_Administration", action = "AdminExtranetBooking_Motor" });
            context.MapRoute(name: "AdminArea_ExtranetCreateProposal_Motor", url: "profile/motor/extranet-create-proposal", defaults: new { controller = "Backend_Administration", action = "AdminExtranetCreateProposal_Motor" });

            context.MapRoute(name: "MotorPolicyList", url: "motor/PolicyList", defaults: new { controller = "Backend_Enquiry", action = "MotorPolicyList" });
            context.MapRoute(name: "MotorProposalList", url: "motor/proposalList", defaults: new { controller = "Backend_Enquiry", action = "MotorProposalList" });

            context.MapRoute(name: "HealthRenewalList", url: "health/RenewalList", defaults: new { controller = "Backend_Enquiry", action = "HealthRenewalList" });
            context.MapRoute(name: "HealthPolicyList", url: "health/PolicyList", defaults: new { controller = "Backend_Enquiry", action = "HealthPolicyList" });
            context.MapRoute(name: "HealthProposalList", url: "health/proposalList", defaults: new { controller = "Backend_Enquiry", action = "HealthProposalList" });
            context.MapRoute(name: "HealthProposalSummaryDetail", url: "health/proposal-summary", defaults: new { controller = "Backend_Enquiry", action = "HealthProposalSummaryDetail" });
            context.MapRoute(name: "NameNewEnquiryList", url: "health/quotation/enquirylist", defaults: new { controller = "Backend_Enquiry", action = "NewEnquiryList" });
            context.MapRoute(name: "BlockedEnquiryList", url: "health/quotation/enquirylist/BlockedEnquiry", defaults: new { controller = "Backend_Enquiry", action = "BlockedEnquiryList" });
            //context.MapRoute(name: "EnquiryTrackDetails", url: "health/quotation/enquirylist/TrackDetails", defaults: new { controller = "Backend_Enquiry", action = "EnquiryTrackDetails" });
            context.MapRoute(name: "EnquiryTrackDetails_Motor", url: "motor/quotation/enquirylist", defaults: new { controller = "Backend_Enquiry", action = "NewEnquiryList_Motor" });
            context.MapRoute(name: "BlockedEnquiryListMoter", url: "motor/quotation/enquirylist/BlockedEnquiry", defaults: new { controller = "Backend_Enquiry", action = "MoterBlockedEnquiryList" });
            context.MapRoute(name: "EnquiryTrackDetailsMotor", url: "motor/quotation/enquirylist/TrackDetails", defaults: new { controller = "Backend_Enquiry", action = "MotorEnquiryTrackDetails" });
            context.MapRoute(name: "EnquiryTrackDetailsHealth", url: "health/quotation/enquirylist/TrackDetails", defaults: new { controller = "Backend_Enquiry", action = "HealthEnquiryTrackDetails" });

            //=================================Commission Section=====================================
            context.MapRoute(name: "NCB_Percentage_List", url: "backend_motor/ncb-percentage-list", defaults: new { controller = "Backend_Motor", action = "NCBList" });
            context.MapRoute(name: "TwowheelerMapList", url: "motor-master/four-wheeler-mapping", defaults: new { controller = "Backend_Enquiry", action = "TwowheelerMap" });
            context.MapRoute(name: "BikeMapList", url: "motor-master/two-wheeler-mapping", defaults: new { controller = "Backend_Enquiry", action = "BikeMap" });


            context.MapRoute(
                "AdminArea_default",
                "AdminArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "insurance.Areas.AdminArea.Controllers" }
            );
        }
    }
}