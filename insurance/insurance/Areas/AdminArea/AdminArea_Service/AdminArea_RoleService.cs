﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_RoleService
    {
        #region [Role Section]
        public static List<CompanyModel> GetCompanyList(string companyId = null, string status = null)
        {
            return AdminArea_RoleHelper.GetCompanyList(companyId, status);
        }
        public static List<MotorCompanyModel> GetMotorCompanyList(string companyId = null, string status = null)
        {
            return AdminArea_RoleHelper.GetMotorCompanyList(companyId, status);
        }
        public static List<RoleModel> GetRoleList(string roleId = null, string status = null)
        {
            return AdminArea_RoleHelper.GetRoleList(roleId, status);
        }
        public static bool InsertRoleDetail(RoleModel role)
        {
            return AdminArea_RoleHelper.InsertRoleDetail(role);
        }
        public static bool UpdateRoleDetail(RoleModel role)
        {
            return AdminArea_RoleHelper.UpdateRoleDetail(role);
        }
        public static bool UpdateRoleStatus(string roleid, string status)
        {
            return AdminArea_RoleHelper.UpdateRoleStatus(roleid, status);
        }
        public static List<SetRolePermission> GetRolePermissionList(string roleid)
        {
            return AdminArea_RoleHelper.GetRolePermissionList(roleid);
        }
        public static bool DeleteAssignedRolePermission(string roleid)
        {
            return AdminArea_RoleHelper.DeleteAssignedRolePermission(roleid);
        }
        public static bool InsertAssignedRolePermission(SetRolePermission rolePer)
        {
            return AdminArea_RoleHelper.InsertAssignedRolePermission(rolePer);
        }
        public static bool InsertCompanyDetail(CompanyModel model)
        {
            return AdminArea_RoleHelper.InsertCompanyDetail(model);
        }
        public static bool InsertCompanyDetailmotor(MotorCompanyModel model)
        {
            return AdminArea_RoleHelper.InsertCompanyDetailmotor(model);
        }
        public static bool UpdateCompanyDetail(CompanyModel model)
        {
            return AdminArea_RoleHelper.UpdateCompanyDetail(model);
        }
        public static bool UpdatebrochureDetail(Product model)
        {
            return AdminArea_RoleHelper.UpdatebrochureDetail(model);
        }
        public static bool UpdateClausesLinkDetail(Product model)
        {
            return AdminArea_RoleHelper.UpdateClausesLinkDetail(model);
        }
        public static bool UpdatebrochureDetailmotor(Product model)
        {
            return AdminArea_RoleHelper.UpdatebrochureDetailmotor(model);
        }
        public static bool UpdateClausesLinkUrlDetailmotor(Product model)
        {
            return AdminArea_RoleHelper.UpdateClausesLinkUrlDetailmotor(model);
        }
        public static bool UpdateCompanyDetailMotor(MotorCompanyModel model)
        {
            return AdminArea_RoleHelper.UpdateCompanyDetailMotor(model);
        }
        public static bool UpdateCompanyStatus(string companyid, string status)
        {
            return AdminArea_RoleHelper.UpdateCompanyStatus(companyid, status);
        }
        public static bool UpdateCompanyStatusmotor(string id, string status)
        {
            return AdminArea_RoleHelper.UpdateCompanyStatusmotor(id, status);
        }
        #endregion

        #region[Product]
        public static List<Product> GetProductList(string productId = null, string status = null, string companyId = null)
        {
            return AdminArea_RoleHelper.GetProductList(productId, status, companyId);
        }
        public static List<Product> GetMotorProductList(string productId = null, string status = null)
        {
            return AdminArea_RoleHelper.GetMotorProductList(productId, status);
        }

        public static bool StatusUpdator(int id, bool status, string table, string database, string whereCondId, string statusField)
        {
            return AdminArea_RoleHelper.StatusUpdator(id, status, table, database, whereCondId, statusField);
        }
        public static bool MotorStatusUpdator(int id, int status, string table, string database, string whereCondId, string statusField)
        {
            return AdminArea_RoleHelper.MotorStatusUpdator(id, status, table, database, whereCondId, statusField);
        }

        #endregion
    }
}