﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_GroupService
    {
        public static List<GroupDetailModel> GetGroupList(string groupId = null, string status = null)
        {
            return AdminArea_GroupHelper.GetGroupList(groupId, status);
        }
        public static bool UpdateGroupStatus(string groupid, string status)
        {
            return AdminArea_GroupHelper.UpdateGroupStatus(groupid, status);
        }
        public static bool InsertUpdateGroupDetail(string groupid, string groupname)
        {
            return AdminArea_GroupHelper.InsertUpdateGroupDetail(groupid, groupname);
        }
    }
}