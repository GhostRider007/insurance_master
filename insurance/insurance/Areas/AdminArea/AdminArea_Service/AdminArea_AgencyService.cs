﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using insurance.Areas.AdminArea.AdminArea_Helper;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_AgencyService
    {
        public static List<Agency> GetAgencyList(string agencyId = null, string status = null)
        {
            return AdminArea_AgencyHelper.GetAgencyList(agencyId, status);
        }
    }
}