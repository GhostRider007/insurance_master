﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_MenuService
    {
        #region[SetMenuForStaff]

        public static List<StaffMenuModel> StaffMenuList(int? menuID)
        {
            return AdminArea_MenuHelper.StaffMenuList(menuID);
        }
        public static List<SubMenuModel> StaffSubMenuList(int? submenuID)
        {
            return AdminArea_MenuHelper.StaffSubMenuList(submenuID);
        }
        public static List<SubMenuIdListModel> StafftSubMenuIdList(int? roleid)
        {
            return AdminArea_MenuHelper.StafftSubMenuIdList(roleid);
        }

        #endregion

        #region [Menu Section]
        public static List<StaffMenuModel> GetMenuByRoleList(int roleId)
        {
            return AdminArea_MenuHelper.GetMenuByRoleList(roleId);
        }
        public static List<SubMenuModel> GetSubMenuByRoleList(int roleId)
        {
            return AdminArea_MenuHelper.GetSubMenuByRoleList(roleId);
        }
        public static List<MenuModel> GetMenuList(string menuId = null, string status = null)
        {
            return AdminArea_MenuHelper.GetMenuList(menuId, status);
        }
        public static bool InsertMenuDetail(MenuModel menu)
        {
            return AdminArea_MenuHelper.InsertMenuDetail(menu);
        }
        public static bool UpdateMenuDetail(MenuModel menu)
        {
            return AdminArea_MenuHelper.UpdateMenuDetail(menu);
        }
        public static bool UpdateMenuPosition(string menuid, string posval = null, string status = null)
        {
            return AdminArea_MenuHelper.UpdateMenuPosition(menuid, posval, status);
        }
        #endregion

        #region [Sub Menu Section]
        public static List<SubMenuModel> GetSubMenuList(string submenuId = null, string status = null)
        {
            return AdminArea_MenuHelper.GetSubMenuList(submenuId, status);
        }
        public static bool InsertSubMenuDetail(SubMenuModel submenu)
        {
            return AdminArea_MenuHelper.InsertSubMenuDetail(submenu);
        }
        public static bool UpdateSubMenuDetail(SubMenuModel submenu)
        {
            return AdminArea_MenuHelper.UpdateSubMenuDetail(submenu);
        }
        public static bool UpdateSubMenuPosition(string submenuId, string posval = null, string status = null)
        {
            return AdminArea_MenuHelper.UpdateSubMenuPosition(submenuId, posval, status);
        }
        #endregion
    }
}