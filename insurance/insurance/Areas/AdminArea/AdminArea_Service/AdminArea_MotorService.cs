﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Motor;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public class AdminArea_MotorService
    {
        public static List<NCBPercentage> GetNCBList() 
        {
           return AdminArea_MotorHelper.GetNCBList();
        }
    }
}