﻿using insurance.Areas.AdminArea.AdminArea_Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_AutoCompleteService
    {
        public static Agency GetAgencyDetail(string agencyName = null, string agencyId = null, string status = null)
        {
            return AdminArea_AutoCompleteHelper.GetAgencyDetail(agencyName, agencyId, status);
        }
    }
}