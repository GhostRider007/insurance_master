﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Helper;
//using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Areas.AdminArea.Models.AdminArea_Commission;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_CommissionService
    {
        public static List<insurance.Areas.AdminArea.Models.AdminArea_Custom.Commission> GetCommissionList(string commisionId = null, string status = null)
        {
            return AdminArea_CommissionHelper.GetCommissionList(commisionId, status);
        }
        public static bool InsertCommissionDetail(insurance.Areas.AdminArea.Models.AdminArea_Custom.Commission comm)
        {
            return AdminArea_CommissionHelper.InsertCommissionDetail(comm);
        }
        public static bool UpdateCommissionDetail(insurance.Areas.AdminArea.Models.AdminArea_Custom.Commission comm)
        {
            return AdminArea_CommissionHelper.UpdateCommissionDetail(comm);
        }
        public static bool UpdateCommissionStatus(string commisionId, string status)
        {
            return AdminArea_CommissionHelper.UpdateCommissionStatus(commisionId, status);
        }

        //================================NEW ONES===============================================
        public static List<Commission> ComissionList(int? id, string insuramceType, string status)
        {
            return AdminArea_CommissionHelper.ComissionList(id, insuramceType, status);
        }
        public static bool AddEditCommission(Commission model)
        {
            return AdminArea_CommissionHelper.AddEditCommission(model);
        }

        //public static bool UpdateCommissionStatus(string commisionId, string status)
        //{
        //    return AdminArea_CommissionHelper.UpdateCommissionStatus(commisionId, status);
        //}

        public static bool AddEditInsentiveComission(IncentiveCommission model)
        {
            return AdminArea_CommissionHelper.AddEditInsentiveComission(model);
        }
        public static bool DeleteCommission(string id)
        {
            return AdminArea_CommissionHelper.DeleteCommission(id);
        }

        public static List<IncentiveCommission> GetHealthInsentiveComissionList(string status, string id = null)
        {
            return AdminArea_CommissionHelper.GetHealthInsentiveComissionList(status, id);
        }
        public static bool UpdateInsentiveComission(string id, string status)
        {
            return AdminArea_CommissionHelper.UpdateInsentiveComission(id, status);
        }
        public static string GetCompanyList(string type)
        {
            string result = string.Empty;
            Dictionary<string, string> list = AdminArea_CommissionHelper.GetCompanyList(type);
            if (list.Count > 0)
            {
                result = "<option value=\"\">Select Insurer</option>";
            }
            foreach (var item in list)
            {
                result = result + "<option value=\"" + item.Key + "\">" + item.Value + "</option>";
            }
            return result;
        }
        public static string GetProductList(string type, string CompanyId, string vehicleType = null)
        {
            string result = string.Empty;
            Dictionary<string, string> list = AdminArea_CommissionHelper.GetProductList(type, CompanyId, vehicleType);
            if (list.Count > 0)
            {
                result = "<option value=\"\">Select Product</option>";
            }
            foreach (var item in list)
            {
                result = result + "<option value=\"" + item.Key + "\">" + item.Value + "</option>";
            }
            return result;
        }

        public static string GenerateCommissionReport(string agencyId, string time)
        {
            string result = string.Empty;
            List<CommissionReport> comm = AdminArea_CommissionHelper.GenerateCommissionReport(agencyId, time);

            if (comm.Count > 0)
            {
                decimal totalAMount = 0;
                foreach (var item in comm)
                {
                    totalAMount = totalAMount + Convert.ToDecimal(item.IncAmount);
                    result += "<tr><td>" + item.AgencyName + "</td><td>" + item.Insurer + "</td><td>" + item.ProductName + "</td><td> " + (!string.IsNullOrEmpty(item.TotalSale) ? "₹ " + item.TotalSale : "- - -") + "</td><td>" + (!string.IsNullOrEmpty(item.IncentivePercentage) ? item.IncentivePercentage + " %" : "- - -") + "</td><td> " + (!string.IsNullOrEmpty(item.IncAmount) ? "₹ " + item.IncAmount : "- - -") + "</td><td>" + (!string.IsNullOrEmpty(item.GroupName) ? item.GroupName : "- - -") + "</td></tr>";
                }
                result += "<tr><td colspan='7' style='border-top: none!important;'></td></tr><tr><td colspan='5'><h4 style='color: green;text-align: center;'>TOTAL INCENTIVE AMOUNT </h4></td><td><h4 style='color: green;'>₹ " + totalAMount + "</h4></td><td></td></tr>";
            }
            else
            {
                result += "<tr style='background:#f6f6f6;'><td colspan='7' class='text-center text-danger'><span>No record found!</span></td></tr>";
            }
            return result;
        }
    }
}