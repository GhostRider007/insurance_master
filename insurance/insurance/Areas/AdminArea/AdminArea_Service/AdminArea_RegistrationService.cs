﻿using insurance.Areas.AdminArea.AdminArea_Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_RegistrationService
    {
        public static List<MemberLogin> GetMemberList(string memberId = null, string Rollid = null ,string status=null)
        {
            return AdminArea_RegistrationHelper.GetMemberList(memberId, Rollid, status);
        }

        public static bool UpdateMemberStatus(string memberId, string status)
        {
            return AdminArea_RegistrationHelper.UpdateMemberStatus(memberId, status);
        }

        public static int InsertMemberDetail(MemberLogin member)
        {
            return AdminArea_RegistrationHelper.InsertMemberDetail(member);
        }
        public static bool UpdateMemberDetail(MemberLogin member)
        {
            return AdminArea_RegistrationHelper.UpdateMemberDetail(member);
        }

        public static bool IsUserAlreadyExist(string userId)
        {
            return AdminArea_AccountHelper.IsUserAlreadyExist(userId);
        }

        public static bool UpdateMemberImage(string memberId, string imageurlname)
        {
            return AdminArea_AccountHelper.UpdateMemberImage(memberId, imageurlname);
        }

        public static bool SaveDsrReport(Agency model)
        {
            return AdminArea_RegistrationHelper.SaveDsrReport(model);
        }

        public static List<Agency> GetDsrreportList(Agency model, string type )
        {
            return AdminArea_RegistrationHelper.GetDsrreportList(model, type);
        }
        public static bool ReadbyAdminUpdate(Agency model)
        {
            return AdminArea_RegistrationHelper.ReadbyAdminUpdate(model);
        }
        public static bool ReplybyAdminUpdate(Agency model)
        {
            return AdminArea_RegistrationHelper.ReplybyAdminUpdate(model);
        }
        public static List<Agency> GetDsrreportbyID(Agency model)
        {
            return AdminArea_RegistrationHelper.GetDsrreportbyID(model);
        }

        public static List<LeadRequestModel> GetleadRequestlist(LeadRequestModel model)
        {
            return AdminArea_RegistrationHelper.GetleadRequestlist(model);
        }        

    }
}