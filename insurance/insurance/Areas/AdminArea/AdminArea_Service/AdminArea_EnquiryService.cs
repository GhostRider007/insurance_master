﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public class AdminArea_EnquiryService
    {
        public static List<BankDeatil> GetBankDetailsList(string AgencyID)
        {
            List<BankDeatil> result = new List<BankDeatil>();

            try
            {
                DataTable dtbankDetail = AdminArea_EnquiryHelper.GetBankDetails(AgencyID);

                if (dtbankDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtbankDetail.Rows.Count; i++)
                    {
                        BankDeatil model = new BankDeatil();                      
                        model.id = Convert.ToInt32(dtbankDetail.Rows[i]["id"].ToString());
                        model.Isverified = Convert.ToBoolean(dtbankDetail.Rows[i]["Isverified"].ToString());
                        model.Status = Convert.ToBoolean(dtbankDetail.Rows[i]["Status"].ToString());
                        model.AccountNo = dtbankDetail.Rows[i]["AccountNo"].ToString();
                        model.AgencyID = dtbankDetail.Rows[i]["AgencyID"].ToString();
                        model.BankName = dtbankDetail.Rows[i]["BankName"].ToString();
                        model.Branch = dtbankDetail.Rows[i]["Branch"].ToString();
                        model.IFSC = dtbankDetail.Rows[i]["IFSC"].ToString();
                        model.MobileNo = dtbankDetail.Rows[i]["MobileNo"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        public static List<ProposalDetailsMotor> GetEnquiryDetailsMotor(string listType, string FromDate = null, string ToDate = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string EnquiryId = null, string ProposalNo = null, string PolicyNo = null, string CustomerName = null, string area="admin")
        {
            List<ProposalDetailsMotor> result = new List<ProposalDetailsMotor>();

            try
            {
                bool isFilter = false;
                if (!string.IsNullOrEmpty(EnquiryId) || !string.IsNullOrEmpty(ProposalNo) || !string.IsNullOrEmpty(PolicyNo) || !string.IsNullOrEmpty(Insurer) || !string.IsNullOrEmpty(Product) || !string.IsNullOrEmpty(PolicyType) || !string.IsNullOrEmpty(CustomerName))
                {
                    isFilter = true;
                }
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetEnquiryDetailsMotor(listType, AgencyId,FromDate, ToDate, area, isFilter);

                #region[Filteration]
                if (!string.IsNullOrEmpty(EnquiryId))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Enquiry_Id") == EnquiryId.Trim().ToLower())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(ProposalNo))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("ProposalNumber") == ProposalNo.Trim())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(PolicyNo))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("PolicyNumber") == PolicyNo.Trim())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(CustomerName))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("CustomerName").ToLower().Contains(CustomerName.ToLower()))
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Insurer))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("CompanyName") == Insurer)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Product))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("ProductName") == Product)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(PolicyType))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Policy_Type") == PolicyType)
                                            .CopyToDataTable();
                }
                #endregion

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        ProposalDetailsMotor model = new ProposalDetailsMotor();

                        model.AgencyID = dtEnquiryDetail.Rows[i]["AgencyID"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.BlockId = dtEnquiryDetail.Rows[i]["BlockId"].ToString();

                        model.CustomerName = dtEnquiryDetail.Rows[i]["CustomerName"].ToString();
                        model.VehicleClass = dtEnquiryDetail.Rows[i]["VehicleClass"].ToString();
                        model.VehicleName = dtEnquiryDetail.Rows[i]["VehicleName"].ToString();

                        //model.CompanyId = dtEnquiryDetail.Rows[i]["CompanyId"].ToString();
                        model.CompanyName = dtEnquiryDetail.Rows[i]["CompanyName"].ToString();
                        model.ProductName = dtEnquiryDetail.Rows[i]["ProductName"].ToString();
                        //model.SumInsured = dtEnquiryDetail.Rows[i]["SumInsured"].ToString();

                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.PremiumAmount = dtEnquiryDetail.Rows[i]["PremiumAmount"].ToString();

                        model.ProposalNumber = dtEnquiryDetail.Rows[i]["ProposalNumber"].ToString();
                        model.CreatedDate_Proposal = dtEnquiryDetail.Rows[i]["CreatedDate_Proposal"].ToString();                        
                        
                        model.PolicyNumber = dtEnquiryDetail.Rows[i]["PolicyNumber"].ToString();
                        model.CreatedDate_Policy = dtEnquiryDetail.Rows[i]["CreatedDate_Policy"].ToString();

                        model.PolicyPDFLink = dtEnquiryDetail.Rows[i]["PolicyPDFLink"].ToString();

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        internal static List<ProposalDetailsHealth> GetRenewalList_Health(string EnquiryId = null, string PolicyNo = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string CustomerName = null, string area = "admin")
        {
            List<ProposalDetailsHealth> result = new List<ProposalDetailsHealth>();

            try
            {
                bool isFilter = false;
                if (!string.IsNullOrEmpty(EnquiryId) || !string.IsNullOrEmpty(PolicyNo) || !string.IsNullOrEmpty(Insurer) || !string.IsNullOrEmpty(Product) || !string.IsNullOrEmpty(PolicyType) || !string.IsNullOrEmpty(CustomerName))
                {
                    isFilter = true;
                }

                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetRenewalList_Health(AgencyId, area);

                #region[Filteration]
                if (!string.IsNullOrEmpty(EnquiryId))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Enquiry_Id") == EnquiryId.Trim().ToLower())
                                            .CopyToDataTable();
                }                
                if (!string.IsNullOrEmpty(PolicyNo))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("PolicyNumber") == PolicyNo.Trim())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(CustomerName))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("InsuredName").ToLower().Contains(CustomerName.ToLower()))
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Insurer))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("CompanyName") == Insurer)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Product))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("ProductName") == Product)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(PolicyType))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Policy_Type") == PolicyType)
                                            .CopyToDataTable();
                }
                #endregion

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        ProposalDetailsHealth model = new ProposalDetailsHealth();

                        model.AgencyID = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.BlockId = dtEnquiryDetail.Rows[i]["BlockID"].ToString();

                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.PremiumAmount = dtEnquiryDetail.Rows[i]["premium"].ToString();
                        model.SumInsured = dtEnquiryDetail.Rows[i]["suminsured"].ToString();
                        model.companyid = dtEnquiryDetail.Rows[i]["companyid"].ToString();
                        model.CompanyName = dtEnquiryDetail.Rows[i]["CompanyName"].ToString();

                        model.CustomerName = dtEnquiryDetail.Rows[i]["InsuredName"].ToString();
                        model.ProductName = dtEnquiryDetail.Rows[i]["ProductName"].ToString(); //AdminArea_EnquiryHelper.GetProductNameHealth(model.Enquiry_Id);

                        model.p_proposalNum = dtEnquiryDetail.Rows[i]["ProposalNumber"].ToString();
                        model.pay_proposal_Number = dtEnquiryDetail.Rows[i]["pay_proposal_Number"].ToString();
                        model.CreatedDate_Proposal = dtEnquiryDetail.Rows[i]["ProposalDate"].ToString();

                        model.pay_policy_Number = dtEnquiryDetail.Rows[i]["PolicyNumber"].ToString();
                        model.CreatedDate_Policy = dtEnquiryDetail.Rows[i]["PolicyDate"].ToString();
                        model.policyStartDate = dtEnquiryDetail.Rows[i]["starton_date"].ToString();
                        model.policyExpiryDate = dtEnquiryDetail.Rows[i]["endon_date"].ToString();
                        model.daysLeft_expiry = Convert.ToInt32(dtEnquiryDetail.Rows[i]["DaysLeft"].ToString());
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static List<ProposalDetailsHealth> GetEnquiryDetailsByAgencyHealth(string listType, string EnquiryId = null, string ProposalNo = null, string PolicyNo = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string FromDate = null, string ToDate = null, string CustomerName = null, string area = "admin")
        {
            List<ProposalDetailsHealth> result = new List<ProposalDetailsHealth>();

            try
            {
                bool isFilter = false;
                if (!string.IsNullOrEmpty(EnquiryId) || !string.IsNullOrEmpty(ProposalNo) || !string.IsNullOrEmpty(PolicyNo) || !string.IsNullOrEmpty(Insurer) || !string.IsNullOrEmpty(Product) || !string.IsNullOrEmpty(PolicyType) || !string.IsNullOrEmpty(CustomerName)) 
                {
                    isFilter = true;
                }

                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetEnquiryDetailsHealth(AgencyId, listType, FromDate, ToDate, area, isFilter);

                #region[Filteration]
                if (!string.IsNullOrEmpty(EnquiryId))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Enquiry_Id") == EnquiryId.Trim().ToLower())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(ProposalNo))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("ProposalNumber") == ProposalNo.Trim())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(PolicyNo))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("PolicyNumber") == PolicyNo.Trim())
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(CustomerName))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("InsuredName").ToLower().Contains(CustomerName.ToLower()))
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Insurer))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("CompanyName") == Insurer)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(Product))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("ProductName") == Product)
                                            .CopyToDataTable();
                }
                if (!string.IsNullOrEmpty(PolicyType))
                {
                    dtEnquiryDetail = dtEnquiryDetail.AsEnumerable().Where(row => row.Field<String>("Policy_Type") == PolicyType)
                                            .CopyToDataTable();
                }
                #endregion

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        ProposalDetailsHealth model = new ProposalDetailsHealth();

                        model.AgencyID = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.BlockId = dtEnquiryDetail.Rows[i]["BlockID"].ToString();

                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.PremiumAmount = dtEnquiryDetail.Rows[i]["premium"].ToString();
                        model.SumInsured = dtEnquiryDetail.Rows[i]["suminsured"].ToString();
                        model.companyid = dtEnquiryDetail.Rows[i]["companyid"].ToString();
                        model.CompanyName = dtEnquiryDetail.Rows[i]["CompanyName"].ToString();

                        model.CustomerName = dtEnquiryDetail.Rows[i]["InsuredName"].ToString();
                        model.ProductName = dtEnquiryDetail.Rows[i]["ProductName"].ToString(); //AdminArea_EnquiryHelper.GetProductNameHealth(model.Enquiry_Id);

                        model.p_proposalNum = dtEnquiryDetail.Rows[i]["ProposalNumber"].ToString();
                        model.pay_proposal_Number = dtEnquiryDetail.Rows[i]["pay_proposal_Number"].ToString();
                        model.CreatedDate_Proposal = dtEnquiryDetail.Rows[i]["ProposalDate"].ToString();

                        model.pay_policy_Number = dtEnquiryDetail.Rows[i]["PolicyNumber"].ToString();
                        model.CreatedDate_Policy = dtEnquiryDetail.Rows[i]["PolicyDate"].ToString();

                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<EnquiryDetails> GetEnquiryDetails()
        {
            List<EnquiryDetails> result = new List<EnquiryDetails>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetEnquiryDetails();

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        EnquiryDetails model = new EnquiryDetails();
                        int adultCount = 0; int childCount = 0;

                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Self"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Spouse"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Father"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Mother"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Son"].ToString()))
                        {
                            for (int s = 1; s <= 4; s++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Son" + s + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Daughter"].ToString()))
                        {
                            for (int d = 1; d <= 4; d++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Daughter" + d + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }

                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.Adult = adultCount.ToString();
                        model.Child = childCount.ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["First_Name"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["Last_Name"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enqdate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }
        public static List<EnquiryDetails> GetEnquiryDetailList(string userid, string status, string EnquiryId = null, string AgencyId = null, string PolicyType = null, string FromDate = null, string ToDate = null)
        {
            List<EnquiryDetails> result = new List<EnquiryDetails>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetEnquiryDetailList(userid, status, EnquiryId, AgencyId, PolicyType, FromDate, ToDate);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        EnquiryDetails model = new EnquiryDetails();
                        int adultCount = 0; int childCount = 0;

                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Self"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Spouse"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Father"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Mother"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Son"].ToString()))
                        {
                            for (int s = 1; s <= 4; s++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Son" + s + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Daughter"].ToString()))
                        {
                            for (int d = 1; d <= 4; d++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Daughter" + d + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }
                        model.EnquiryId = Convert.ToInt32(dtEnquiryDetail.Rows[i]["EnquiryId"].ToString());
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.Adult = adultCount.ToString();
                        model.Child = childCount.ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["First_Name"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["Last_Name"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.AgencyId = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        //model.Enqdate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        model.Pin_Code = dtEnquiryDetail.Rows[i]["Pin_Code"].ToString();
                        model.Enqdate = dtEnquiryDetail.Rows[i]["CreatedDate"].ToString();
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<EnquiryDetails_Motors> GetMotorBlockedEnquiryList(string userid, string status)
        {
            List<EnquiryDetails_Motors> result = new List<EnquiryDetails_Motors>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetMoterBlockedEnquiryList(userid, status);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        EnquiryDetails_Motors model = new EnquiryDetails_Motors();
                        model.Id = Convert.ToInt32(dtEnquiryDetail.Rows[i]["Id"].ToString());
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.AgencyId = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.VechileRegNo = dtEnquiryDetail.Rows[i]["VechileRegNo"].ToString();
                        model.MobileNo = dtEnquiryDetail.Rows[i]["MobileNo"].ToString();
                        model.Is_Term_Accepted = Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Is_Term_Accepted"].ToString());
                        model.RtoId = dtEnquiryDetail.Rows[i]["RtoId"].ToString();
                        model.RtoName = dtEnquiryDetail.Rows[i]["RtoName"].ToString();
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        model.NewStatus = dtEnquiryDetail.Rows[i]["NewStatus"].ToString();
                        model.DateOfReg = dtEnquiryDetail.Rows[i]["DateOfReg"].ToString();
                        //model.YearOfMake = dtEnquiryDetail.Rows[i]["YearOfMake"].ToString();
                        model.BrandId = dtEnquiryDetail.Rows[i]["BrandId"].ToString();
                        model.BrandName = dtEnquiryDetail.Rows[i]["BrandName"].ToString();
                        model.ModelId = dtEnquiryDetail.Rows[i]["ModelId"].ToString();
                        model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.Fuel = dtEnquiryDetail.Rows[i]["Fuel"].ToString();
                        model.VarientId = dtEnquiryDetail.Rows[i]["VarientId"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.Status = Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Status"].ToString());
                        model.CreatedDate = Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString());
                        model.UpdatedDate = Convert.ToDateTime(dtEnquiryDetail.Rows[i]["UpdatedDate"].ToString());
                        model.PinCode = !string.IsNullOrEmpty(dtEnquiryDetail.Rows[i]["PinCode"].ToString()) ? dtEnquiryDetail.Rows[i]["PinCode"].ToString() : "Not Available";
                        model.ispreviousinsurer = dtEnquiryDetail.Rows[i]["ispreviousinsurer"].ToString();
                        model.insurerid = dtEnquiryDetail.Rows[i]["insurerid"].ToString();
                        model.insurername = dtEnquiryDetail.Rows[i]["insurername"].ToString();
                        model.policynumber = dtEnquiryDetail.Rows[i]["policynumber"].ToString();
                        model.policyexpirydate = dtEnquiryDetail.Rows[i]["policyexpirydate"].ToString();
                        model.isclaiminlastyear = dtEnquiryDetail.Rows[i]["isclaiminlastyear"].ToString();
                        model.previousyearnoclaim = dtEnquiryDetail.Rows[i]["previousyearnoclaim"].ToString();
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<EnquiryDetails> GetBlockedEnquiryList(string userid, string status)
        {
            List<EnquiryDetails> result = new List<EnquiryDetails>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.GetBlockedEnquiryList(userid, status);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        EnquiryDetails model = new EnquiryDetails();
                        int adultCount = 0; int childCount = 0;

                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Self"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Spouse"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Father"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Mother"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Son"].ToString()))
                        {
                            for (int s = 1; s <= 4; s++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Son" + s + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Daughter"].ToString()))
                        {
                            for (int d = 1; d <= 4; d++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Daughter" + d + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }

                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.Adult = adultCount.ToString();
                        model.Child = childCount.ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["First_Name"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["Last_Name"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enqdate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        model.Status = dtEnquiryDetail.Rows[i]["Status"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<TrackEnquirydetail> TrackDetails(string Enquiry_ID)
        {
            List<TrackEnquirydetail> result = new List<TrackEnquirydetail>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.TrackDetails(Enquiry_ID);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        TrackEnquirydetail model = new TrackEnquirydetail();
                        int adultCount = 0; int childCount = 0;

                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Self"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Spouse"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Father"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Mother"].ToString()))
                        {
                            adultCount = adultCount + 1;
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Son"].ToString()))
                        {
                            for (int s = 1; s <= 4; s++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Son" + s + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }
                        if (Convert.ToBoolean(dtEnquiryDetail.Rows[i]["Daughter"].ToString()))
                        {
                            for (int d = 1; d <= 4; d++)
                            {
                                if (Convert.ToInt32(dtEnquiryDetail.Rows[i]["Daughter" + d + "Age"].ToString()) > 0)
                                {
                                    childCount = childCount + 1;
                                }
                            }
                        }

                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.Adult = adultCount.ToString();
                        model.Child = childCount.ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["First_Name"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["Last_Name"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enqdate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        model.Status = dtEnquiryDetail.Rows[i]["Status"].ToString();
                        model.Email_Id = dtEnquiryDetail.Rows[i]["Email_Id"].ToString();
                        model.Mobile_No = dtEnquiryDetail.Rows[i]["Mobile_No"].ToString();
                        model.Pin_Code = dtEnquiryDetail.Rows[i]["Pin_Code"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<TrackEnquirydetail> TrackDetailsMoter(string Enquiry_ID)
        {
            List<TrackEnquirydetail> result = new List<TrackEnquirydetail>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.TrackDetailsMotor(Enquiry_ID);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        TrackEnquirydetail model = new TrackEnquirydetail();

                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.VechicleRegNo = dtEnquiryDetail.Rows[i]["VechileRegNo"].ToString();
                        model.RtoName = dtEnquiryDetail.Rows[i]["RtoName"].ToString();
                        model.DateOfReg = dtEnquiryDetail.Rows[i]["DateOfReg"].ToString();
                        model.BrandName = dtEnquiryDetail.Rows[i]["BrandName"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Enqdate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
                        model.BlockID = dtEnquiryDetail.Rows[i]["BlockID"].ToString();
                        model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.Status = dtEnquiryDetail.Rows[i]["NewStatus"].ToString();
                        model.Mobile_No = dtEnquiryDetail.Rows[i]["MobileNo"].ToString();
                        model.Pin_Code = dtEnquiryDetail.Rows[i]["PinCode"].ToString();
                        model.policynumber = dtEnquiryDetail.Rows[i]["policynumber"].ToString();
                        model.Address = dtEnquiryDetail.Rows[i]["Address"].ToString();
                        model.BusinessEmailID = dtEnquiryDetail.Rows[i]["BusinessEmailID"].ToString();
                        model.BusinessPhone = dtEnquiryDetail.Rows[i]["BusinessPhone"].ToString();
                        model.insurancetype = dtEnquiryDetail.Rows[i]["insurancetype"].ToString();
                        model.Fuel = dtEnquiryDetail.Rows[i]["Fuel"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["FirstName"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["LastName"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<TrackEnquirydetail> EmailProposalDetailsMotor(string Enquiry_ID)
        {
            List<TrackEnquirydetail> result = new List<TrackEnquirydetail>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.EmailProposalDetailsMotor(Enquiry_ID);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        TrackEnquirydetail model = new TrackEnquirydetail();

                        model.Fuel = dtEnquiryDetail.Rows[i]["Fuel"].ToString();
                        model.BrandName = dtEnquiryDetail.Rows[i]["BrandName"].ToString();
                        model.DateOfReg = dtEnquiryDetail.Rows[i]["DateOfReg"].ToString();
                        model.RtoName = dtEnquiryDetail.Rows[i]["RtoName"].ToString();
                        model.insurancetype = dtEnquiryDetail.Rows[i]["insurancetype"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.AgencyId = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["enquiry_id"].ToString();
                        model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.dob = dtEnquiryDetail.Rows[i]["dob"].ToString();
                        model.Mobile_No = dtEnquiryDetail.Rows[i]["mobileno"].ToString();
                        model.Email_Id = dtEnquiryDetail.Rows[i]["emailid"].ToString();
                        model.ntitle = dtEnquiryDetail.Rows[i]["ntitle"].ToString();
                        model.nfirstname = dtEnquiryDetail.Rows[i]["nfirstname"].ToString();
                        model.nlastname = dtEnquiryDetail.Rows[i]["nlastname"].ToString();
                        model.ndob = dtEnquiryDetail.Rows[i]["ndob"].ToString();
                        model.policyholdertype = dtEnquiryDetail.Rows[i]["policyholdertype"].ToString();
                        model.DateOfMfg = dtEnquiryDetail.Rows[i]["DateOfMfg"].ToString();
                        model.ispreviousinsurer = dtEnquiryDetail.Rows[i]["ispreviousinsurer"].ToString();
                        model.policyexpirydate = dtEnquiryDetail.Rows[i]["policyexpirydate"].ToString();
                        model.isclaiminlastyear = dtEnquiryDetail.Rows[i]["isclaiminlastyear"].ToString();
                        model.engineno = dtEnquiryDetail.Rows[i]["engineno"].ToString();
                        model.nrelation = dtEnquiryDetail.Rows[i]["nrelation"].ToString();
                        model.chasisno = dtEnquiryDetail.Rows[i]["chasisno"].ToString();
                        model.previousinsured = dtEnquiryDetail.Rows[i]["previousinsured"].ToString();
                        model.AgencyBusinessEmailID = dtEnquiryDetail.Rows[i]["AgencyEmail"].ToString();
                        model.vehicletype = dtEnquiryDetail.Rows[i]["vehicletype"].ToString();
                        model.ProposalNo = dtEnquiryDetail.Rows[i]["ProposalNo"].ToString();
                        model.ChainId = dtEnquiryDetail.Rows[i]["ChainId"].ToString();
                        model.First_Name = dtEnquiryDetail.Rows[i]["FirstName"].ToString();
                        model.Last_Name = dtEnquiryDetail.Rows[i]["lastName"].ToString();
                        model.policynumber = dtEnquiryDetail.Rows[i]["policynumber"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static List<TrackHealthEnquirydetail> EmailProposalDetailsHealth(string Enquiry_ID)
        {
            List<TrackHealthEnquirydetail> result = new List<TrackHealthEnquirydetail>();

            try
            {
                DataTable dtEnquiryDetail = AdminArea_EnquiryHelper.EmailProposalDetailsHealth(Enquiry_ID);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        TrackHealthEnquirydetail model = new TrackHealthEnquirydetail();
                        model.EnquiryId = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.AgencyId = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.Pin_Code = dtEnquiryDetail.Rows[i]["Pin_Code"].ToString();
                        model.UpdatedDate = dtEnquiryDetail.Rows[i]["UpdatedDate"].ToString();
                        model.productid = dtEnquiryDetail.Rows[i]["productid"].ToString();
                        model.companyid = dtEnquiryDetail.Rows[i]["companyid"].ToString();
                        model.suminsured = dtEnquiryDetail.Rows[i]["suminsured"].ToString();
                        model.premium = dtEnquiryDetail.Rows[i]["premium"].ToString();
                        model.dob = dtEnquiryDetail.Rows[i]["dob"].ToString();
                        model.address1 = dtEnquiryDetail.Rows[i]["address1"].ToString();
                        model.statename = dtEnquiryDetail.Rows[i]["statename"].ToString();
                        model.cityname = dtEnquiryDetail.Rows[i]["cityname"].ToString();
                        model.firstname = dtEnquiryDetail.Rows[i]["firstname"].ToString();
                        model.lastname = dtEnquiryDetail.Rows[i]["lastname"].ToString();
                        model.mobile = dtEnquiryDetail.Rows[i]["mobile"].ToString();
                        model.emailid = dtEnquiryDetail.Rows[i]["emailid"].ToString();
                        model.pincode = dtEnquiryDetail.Rows[i]["pincode"].ToString();
                        model.gender = dtEnquiryDetail.Rows[i]["gender"].ToString();
                        model.policyfor = dtEnquiryDetail.Rows[i]["policyfor"].ToString();
                        model.plantype = dtEnquiryDetail.Rows[i]["plantype"].ToString();
                        model.ntitle = dtEnquiryDetail.Rows[i]["ntitle"].ToString();
                        model.nfirstname = dtEnquiryDetail.Rows[i]["nfirstname"].ToString();
                        model.nlastname = dtEnquiryDetail.Rows[i]["nlastname"].ToString();
                        model.ndob = dtEnquiryDetail.Rows[i]["ndob"].ToString();
                        model.nage = dtEnquiryDetail.Rows[i]["nage"].ToString();
                        model.nrelations = dtEnquiryDetail.Rows[i]["nrelation"].ToString();
                        model.annualincome = dtEnquiryDetail.Rows[i]["annualincome"].ToString();
                        model.p_proposalNum = dtEnquiryDetail.Rows[i]["p_proposalNum"].ToString();
                        model.p_totalPremium = dtEnquiryDetail.Rows[i]["p_totalPremium"].ToString();
                        model.p_paymenturl = dtEnquiryDetail.Rows[i]["p_paymenturl"].ToString();
                        model.pay_proposal_Number = dtEnquiryDetail.Rows[i]["pay_proposal_Number"].ToString();
                        model.pay_paymentdate = dtEnquiryDetail.Rows[i]["pay_paymentdate"].ToString();
                        model.periodfor = dtEnquiryDetail.Rows[i]["periodfor"].ToString();
                        model.ProductName = dtEnquiryDetail.Rows[i]["ProductName"].ToString();
                        model.AgencyEmail = dtEnquiryDetail.Rows[i]["AgencyEmail"].ToString();
                        model.CreatedDate = dtEnquiryDetail.Rows[i]["CreatedDate"].ToString();
                        model.CityName = dtEnquiryDetail.Rows[i]["CityName"].ToString();
                        model.BusinessPhone = dtEnquiryDetail.Rows[i]["BusinessPhone"].ToString();
                        model.Address = dtEnquiryDetail.Rows[i]["Address"].ToString();
                        model.Pincode = dtEnquiryDetail.Rows[i]["Pincode"].ToString();
                        model.title = dtEnquiryDetail.Rows[i]["title"].ToString();
                        model.CompanyName = dtEnquiryDetail.Rows[i]["CompanyName"].ToString();
                        model.p_rowpaymenturl = dtEnquiryDetail.Rows[i]["p_rowpaymenturl"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static bool BlockEnquiry(string Enquiry_Id, string BlockID)
        {
            return AdminArea_EnquiryHelper.BlockEnquiry(Enquiry_Id, BlockID);
        }
        public static bool MoterBlockEnquiry(string Enquiry_Id, string BlockID)
        {
            return AdminArea_EnquiryHelper.MoterBlockEnquiry(Enquiry_Id, BlockID);
        }
        public static bool ActiveBlockEnquiry(string Enquiry_Id, string BlockID, string status)
        {
            return AdminArea_EnquiryHelper.ActiveBlockEnquiry(Enquiry_Id, BlockID, status);
        }
        public static bool ActiveBlockEnquiryMotor(string Enquiry_Id, string BlockID, string status)
        {
            return AdminArea_EnquiryHelper.ActiveBlockEnquiryMotor(Enquiry_Id, BlockID, status);
        }


        public static List<EnquiryDetails_Motors> GetEnquiryDetailList_Motor(string userid, string status, string FromDate = null, string ToDate = null, string AgencyId = null, string PolicyType = null, string EnquiryId = null)
        {
            return AdminArea_EnquiryHelper.GetEnquiryDetailList_Motor(userid, status, FromDate, ToDate, AgencyId, PolicyType, EnquiryId);
        }
        public static bool MotorEnquiryStatusUpdator(string Enquiry_Id, string BlockID)
        {
            return AdminArea_EnquiryHelper.MotorEnquiryStatusUpdator(Enquiry_Id, BlockID);
        }
    }


}