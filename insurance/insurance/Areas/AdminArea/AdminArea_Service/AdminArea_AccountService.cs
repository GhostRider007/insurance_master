﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using System.Data;
using static insurance.Models.Common.CommonModel;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.AdminArea_Service
{
    public static class AdminArea_AccountService
    {
        public static bool LoginMember(BackEndLogin login, ref string msg, ref string loginId)
        {
            return AdminArea_AccountHelper.LoginMember(login, ref msg, ref loginId);
        }
        public static bool LogoutBackendLogin()
        {
            return AdminArea_AccountHelper.LogoutBackendLogin();
        }
        public static bool IsBackUserLogin(ref MemberLogin lu)
        {
            return AdminArea_AccountHelper.IsBackUserLogin(ref lu);
        }
        public static bool AdminMemberResetPassword(MemberLogin model, ref string msg)
        {
            return AdminArea_AccountHelper.AdminMemberResetPassword(model, ref msg);
        }
        public static bool Admin_ChangeAgencyPassword(CreateAgency model)
        {
            return AdminArea_AccountHelper.Admin_ChangeAgencyPassword(model);
        }

        #region[Booking Report]
        public static List<BookingDetails> GetBookingDetails(string fromDate = null, string toDate = null, string AgencyId = null, string InsuranceType = null, string Insurer = null, string Product = null, string CustomerName = null)
        {
            List<BookingDetails> list = new List<BookingDetails>();

            try
            {
                DataTable dt = AdminArea_AccountHelper.GetBookingDetails(fromDate, toDate, AgencyId, InsuranceType, Insurer, Product, CustomerName);

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        BookingDetails model = new BookingDetails();

                        model.AgencyID = dt.Rows[i]["AgencyID"].ToString();
                        model.AgencyName = dt.Rows[i]["AgencyName"].ToString();
                        model.Enquiry_Id = dt.Rows[i]["Enquiry_Id"].ToString();
                        model.Insurance = dt.Rows[i]["Insurance"].ToString();
                        model.Insurer = dt.Rows[i]["Insurer"].ToString();
                        model.PolicyNo = dt.Rows[i]["PolicyNo"].ToString();
                        model.SumInsured = dt.Rows[i]["SumInsured"].ToString();
                        model.ProposerName = dt.Rows[i]["Proposer_Name"].ToString();
                        model.Insured = dt.Rows[i]["Insured_Name"].ToString();
                        model.Basic = dt.Rows[i]["Basic"].ToString();
                        model.GST = dt.Rows[i]["GST"].ToString();
                        model.Premium = dt.Rows[i]["Premium"].ToString();
                        model.Discount = dt.Rows[i]["Discount"].ToString();
                        model.TDS = dt.Rows[i]["TDS"].ToString();
                        model.ProductName = dt.Rows[i]["ProductName"].ToString();
                        model.PolicyType = dt.Rows[i]["PolicyType"].ToString();
                        model.Tenure = dt.Rows[i]["Tenure"].ToString();
                        model.PolicyDate = dt.Rows[i]["PolicyDate"].ToString();

                        list.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return list;
        }
        public static string BindAgencies()
        {

            string result = string.Empty;

            try
            {
                List<ddlBindClass> list = AdminArea_AccountHelper.BindAgencies();
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        result = result + "<option value=\"" + item.Key + "\">" + item.Value + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static string BindInsurers(string insuranceType)
        {

            string result = string.Empty;

            try
            {
                List<ddlBindClass> list = AdminArea_AccountHelper.BindInsurers(insuranceType);

                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        result = result + "<option data-imgurl=\"http://seeinsured.com" + item.Data1 + "\" data-compid=\"" + item.Key + "\" value=\"" + item.Value + "\">" + item.Value + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static string BindProducts(string insuranceType, string insuranceId)
        {

            string result = string.Empty;

            try
            {
                List<ddlBindClass> list = AdminArea_AccountHelper.BindProducts(insuranceType, insuranceId);
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        result = result + "<option data-productid=\"" + item.Data1 + "\" value=\"" + item.Key + "\">" + item.Value + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        #endregion
    }
}