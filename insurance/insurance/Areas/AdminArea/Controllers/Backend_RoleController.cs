﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecommerce.Models.Common;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using Post_Utility.Utility;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_RoleController : Controller
    {
        #region [Role]
        public ActionResult RoleList(string status)
        {
            RoleModel model = new RoleModel();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.RoleList = AdminArea_RoleService.GetRoleList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult CompanyList(string status)
        {
            CompanyModel model = new CompanyModel();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.CompanyList = AdminArea_RoleService.GetCompanyList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }

        public ActionResult MotorCompanyList(string status)
        {
            MotorCompanyModel model = new MotorCompanyModel();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.CompanyList = AdminArea_RoleService.GetMotorCompanyList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }


        public ActionResult ClausesLink(string productId)
        {
            Product model = new Product();

            try
            {
                if (!string.IsNullOrEmpty(productId))
                {
                    model = AdminArea_RoleService.GetProductList(productId, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClausesLink(Product model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                Product tempModel = new Product();
                if (model != null)
                {
                    tempModel.ProductId = model.ProductId;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("ClausesLink", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/HealthClauses/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.ClausesLinkUrl = localPath;
                }

                if (model.ProductId > 0)
                {
                    tempModel.ClausesLinkUrl = !string.IsNullOrEmpty(tempModel.ClausesLinkUrl) ? tempModel.ClausesLinkUrl : model.ClausesLinkUrl;
                    if (AdminArea_RoleService.UpdateClausesLinkDetail(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("ProductList");
            }
            return View(model);
        }

        public ActionResult MotorClausesLink(string productId)
        {
            Product model = new Product();

            try
            {
                if (!string.IsNullOrEmpty(productId))
                {
                    model = AdminArea_RoleService.GetProductList(productId, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MotorClausesLink(Product model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                Product tempModel = new Product();
                if (model != null)
                {
                    tempModel.ProductId = model.ProductId;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("ClausesLink", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/MotorClauses/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.ClausesLinkUrl = localPath;
                }

                if (model.ProductId > 0)
                {
                    tempModel.ClausesLinkUrl = !string.IsNullOrEmpty(tempModel.ClausesLinkUrl) ? tempModel.ClausesLinkUrl : model.ClausesLinkUrl;
                    if (AdminArea_RoleService.UpdateClausesLinkUrlDetailmotor(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("MotorProductList");
            }
            return View(model);
        }
        public ActionResult Updatebrochure(string productId)
        {
            Product model = new Product();

            try
            {
                if (!string.IsNullOrEmpty(productId))
                {
                    model = AdminArea_RoleService.GetProductList(productId, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Updatebrochure(Product model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                Product tempModel = new Product();
                if (model != null)
                {
                    tempModel.ProductId = model.ProductId;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("Brochure", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/HealthBrochure/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.BrochureLink = localPath;
                }

                if (model.ProductId > 0)
                {
                    tempModel.BrochureLink = !string.IsNullOrEmpty(tempModel.BrochureLink) ? tempModel.BrochureLink : model.BrochureLink;
                    if (AdminArea_RoleService.UpdatebrochureDetail(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("ProductList");
            }
            return View(model);
        }

        public ActionResult UpdateMotorbrochure(string productId)
        {
            Product model = new Product();

            try
            {
                if (!string.IsNullOrEmpty(productId))
                {
                    model = AdminArea_RoleService.GetMotorProductList(productId, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateMotorbrochure(Product model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                Product tempModel = new Product();
                if (model != null)
                {
                    tempModel.ProductId = model.ProductId;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("Brochure", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/MotorBrochure/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.BrochureLink = localPath;
                }

                if (model.ProductId > 0)
                {
                    tempModel.BrochureLink = !string.IsNullOrEmpty(tempModel.BrochureLink) ? tempModel.BrochureLink : model.BrochureLink;
                    if (AdminArea_RoleService.UpdatebrochureDetailmotor(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("MotorProductList");
            }
            return View(model);
        }

        public ActionResult AddEditCompany(string companyid)
        {
            CompanyModel model = new CompanyModel();

            try
            {
                if (!string.IsNullOrEmpty(companyid))
                {
                    model = AdminArea_RoleService.GetCompanyList(companyid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        public ActionResult AddEditCompanymotor(string companyid)
        {
            MotorCompanyModel model = new MotorCompanyModel();

            try
            {
                if (!string.IsNullOrEmpty(companyid))
                {
                    model = AdminArea_RoleService.GetMotorCompanyList(companyid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditCompany(CompanyModel model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                CompanyModel tempModel = new CompanyModel();
                if (model != null)
                {
                    tempModel.CompanyId = model.CompanyId;
                    tempModel.CompanyName = model.CompanyName;
                    tempModel.Description = model.Description;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("Logo", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/images/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.logoUrl = localPath;
                }

                if (model.CompanyId > 0)
                {
                    tempModel.logoUrl = !string.IsNullOrEmpty(tempModel.logoUrl) ? tempModel.logoUrl : model.logoUrl;
                    if (AdminArea_RoleService.UpdateCompanyDetail(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }
                else
                {
                    if (AdminArea_RoleService.InsertCompanyDetail(tempModel))
                    {
                        isSuccess = true;
                        msg = "inserted";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("CompanyList");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditCompanymotor(MotorCompanyModel model, HttpPostedFileBase logo)
        {
            string msg = string.Empty;
            bool isSuccess = false;

            if (ModelState.IsValid)
            {
                MotorCompanyModel tempModel = new MotorCompanyModel();
                if (model != null)
                {
                    tempModel.id = model.id;
                    tempModel.Supplier = model.Supplier;
                    tempModel.ApiUsername = model.ApiUsername;
                    tempModel.ApiPassword = model.ApiPassword;
                    tempModel.ApiAgencyID = model.ApiAgencyID;
                    tempModel.TwoWQuoteUrl = model.TwoWQuoteUrl;
                    tempModel.TwoWProposalUrl = model.TwoWProposalUrl;
                    tempModel.PCQuoteUrl = model.PCQuoteUrl;
                    tempModel.PCProposalUrl = model.PCProposalUrl;
                    tempModel.CMVQuoteUrl = model.CMVQuoteUrl;
                    tempModel.CMVProposalUrl = model.CMVProposalUrl;
                    tempModel.Clientid = model.Supplier;
                    tempModel.TripType = model.TripType;
                    tempModel.URLTYPE = model.URLTYPE;
                    tempModel.OtherData = model.OtherData;
                    tempModel.TotalHit = model.TotalHit;
                    tempModel.Paymentkey = model.Paymentkey;
                }

                if (logo != null)
                {
                    string ranNumber = UtilityClass.GenrateRandomTransactionId("MotorLogo", 6, "1234567890");
                    string ext = Path.GetExtension(logo.FileName);
                    string localPath = "/images/" + ranNumber + ext;
                    string filePath = Server.MapPath(localPath);

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    logo.SaveAs(filePath);

                    tempModel.Image = localPath;
                }

                if (model.id > 0)
                {
                    tempModel.Image = !string.IsNullOrEmpty(tempModel.Image) ? tempModel.Image : model.Image;
                    if (AdminArea_RoleService.UpdateCompanyDetailMotor(tempModel))
                    {
                        isSuccess = true;
                        msg = "updated";
                    }
                }
                else
                {
                    if (AdminArea_RoleService.InsertCompanyDetailmotor(tempModel))
                    {
                        isSuccess = true;
                        msg = "inserted";
                    }
                }

                if (isSuccess)
                {
                    TempData["Msg"] = "Success ! Record " + msg + " successfully!";
                }

                return RedirectToAction("MotorCompanyList");
            }
            return View(model);
        }
        public ActionResult AddEditRole(string roleid)
        {
            RoleModel model = new RoleModel();

            try
            {
                if (!string.IsNullOrEmpty(roleid))
                {
                    model = AdminArea_RoleService.GetRoleList(roleid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public JsonResult ProcessInsertRole(RoleModel role)
        {
            List<string> result = new List<string>();

            try
            {
                if (role.RoleID > 0)
                {
                    if (AdminArea_RoleService.UpdateRoleDetail(role))
                    {
                        result.Add("true");
                        result.Add("update");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
                else
                {
                    if (AdminArea_RoleService.InsertRoleDetail(role))
                    {
                        result.Add("true");
                        result.Add("insert");
                        result.Add("Record inserted successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }

        public JsonResult UpdateCompanyStatus(string companyid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_RoleService.UpdateCompanyStatus(companyid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        public JsonResult UpdateCompanyStatushealth(string companyid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_RoleService.UpdateCompanyStatusmotor(companyid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult UpdateRoleStatus(string roleid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_RoleService.UpdateRoleStatus(roleid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Commission]
        public ActionResult CommissionList(string status)
        {

            Commission model = new Commission();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.CommissionList = AdminArea_CommissionService.GetCommissionList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult AddEditCommission(string commissionid)
        {
            Commission model = new Commission();

            try
            {
                if (!string.IsNullOrEmpty(commissionid))
                {
                    model = AdminArea_CommissionService.GetCommissionList(commissionid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public JsonResult ProcessInsertCommission(Commission comm)
        {
            List<string> result = new List<string>();

            try
            {
                if (comm.CommisionId > 0)
                {
                    if (AdminArea_CommissionService.UpdateCommissionDetail(comm))
                    {
                        result.Add("true");
                        result.Add("update");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
                else
                {
                    if (AdminArea_CommissionService.InsertCommissionDetail(comm))
                    {
                        result.Add("true");
                        result.Add("insert");
                        result.Add("Record inserted successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateCommissionStatus(string commissionid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_CommissionService.UpdateCommissionStatus(commissionid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Group]
        public ActionResult GroupList(string status)
        {
            GroupDetailModel model = new GroupDetailModel();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.GroupList = AdminArea_GroupService.GetGroupList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult GroupAddEdit(string groupid)
        {
            GroupDetailModel model = new GroupDetailModel();

            try
            {
                if (!string.IsNullOrEmpty(groupid))
                {
                    model = AdminArea_GroupService.GetGroupList(groupid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public JsonResult UpdateGroupStatus(string groupid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_GroupService.UpdateGroupStatus(groupid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult ProcessGroupDetail(string groupid, string groupname)
        {
            List<string> result = new List<string>();

            try
            {
                if (AdminArea_GroupService.InsertUpdateGroupDetail(groupid, groupname))
                {
                    result.Add("true");
                    if (!string.IsNullOrEmpty(groupid))
                    {
                        result.Add("up");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("ins");
                        result.Add("Record inserted successfully.");
                    }
                }
                else
                {
                    result.Add("false");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        #endregion

        #region[Product]

        public ActionResult ProductList(string status)
        {
            List<Product> model = new List<Product>();

            try
            {
                if (string.IsNullOrEmpty(status))
                {
                    status = "1";
                }
                else
                {

                    if (status == "deactive")
                        status = "0";
                    if (status == "active")
                        status = "1";
                    if (status == "all")
                        status = string.Empty;
                }

                model = AdminArea_RoleService.GetProductList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        public ActionResult MotorProductList(string status)
        {
            List<Product> model = new List<Product>();

            try
            {
                if (string.IsNullOrEmpty(status))
                {
                    status = "1";
                }
                else
                {

                    if (status == "deactive")
                        status = "0";
                    if (status == "active")
                        status = "1";
                    if (status == "all")
                        status = string.Empty;
                }

                model = AdminArea_RoleService.GetMotorProductList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        #endregion

        #region[Common Status Updator]
        public JsonResult StatusUpdator(int id, bool status, string table, string database, string whereCondId, string statusField)
        {
            List<object> result = new List<object>();
            bool isActivated = AdminArea_RoleService.StatusUpdator(id, status, table, database, whereCondId, statusField);
            if (isActivated)
            {
                result.Add("Success");
            }
            return Json(result);
        }
        public JsonResult MotorStatusUpdator(int id, bool status, string table, string database, string whereCondId, string statusField)
        {
            List<object> result = new List<object>();
            int newstatus;
            if (status)
            {
                newstatus = 1;
            }
            else
            {
                newstatus = 0;
            }
            bool isActivated = AdminArea_RoleService.MotorStatusUpdator(id, newstatus, table, database, whereCondId, statusField);
            if (isActivated)
            {
                result.Add("Success");
            }
            return Json(result);
        }
        #endregion
    }
}