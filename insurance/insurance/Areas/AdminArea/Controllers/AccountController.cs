﻿using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;

namespace insurance.Areas.AdminArea.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult AdminLogin()
        {
            return View();
        }
        public JsonResult ProcessBackendLogin(string username, string password, string isrember)
        {
            List<string> result = new List<string>();

            try
            {
                string msg = string.Empty;
                string loginId = string.Empty;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    BackEndLogin login = new BackEndLogin();
                    login.UserId = username;
                    login.Password = password;
                    login.Remember = isrember.ToLower().Trim() == "true" ? true : false;

                    bool isSuccessLogin = AdminArea_AccountService.LoginMember(login, ref msg, ref loginId);
                    if (isSuccessLogin)
                    {
                        result.Add("true");
                        result.Add(loginId);
                    }
                    else
                    {
                        result.Add("false");
                        result.Add(msg);
                    }
                }
                else
                {
                    result.Add("false");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult BackendLogOut()
        {
            string result = "false";

            try
            {
                if (AdminArea_AccountService.LogoutBackendLogin())
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }

        #region[Booking Report]
        public ActionResult BookingReport(string fromDate = null, string toDate = null, string AgencyId = null, string InsuranceType = null, string Insurer = null, string Product = null,string CustomerName=null) 
        {
            List<BookingDetails> list = new List<BookingDetails>();

            list = AdminArea_AccountService.GetBookingDetails(fromDate, toDate, AgencyId, InsuranceType, Insurer, Product, CustomerName);
            return View(list);
        }
        public JsonResult BindAgencies() 
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindAgencies();
            return Json(result);
        }
        public JsonResult BindInsurers(string insuranceType)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindInsurers(insuranceType);
            return Json(result);
        }
        public JsonResult BindProducts(string insuranceType, string insuranceId)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindProducts(insuranceType,insuranceId);
            return Json(result);
        }
        
        #endregion
    }
}