﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Commission;
using insurance.Models.Common;
using insurance.Service;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_CommisionController : Controller
    {
        #region [Health White]
        public ActionResult HealthComissionList()
        {
            List<Commission> commisssionList = new List<Commission>();
            try
            {
                commisssionList = AdminArea_CommissionService.ComissionList(0, "health", "1");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(commisssionList);
        }
        public ActionResult AddEditHealthComission(int? id)
        {
            List<object> list = new List<object>();
            id = (id > 0) ? id : 0;
            string groupList = BindGroups();
            string agencyList = BindAgency();
            string conpanyList = BindCompany("health");
            //conpanyList = conpanyList.Replace("<option value=\"\">Select Insurer</option>", "<option value=\"all\">All</option>");
            list.Add(id);
            list.Add(groupList);
            list.Add(agencyList);
            list.Add(conpanyList);
            return View(list);
        }
        public JsonResult AddEditHealthComission_Get(int? id)
        {
            List<object> result = new List<object>();
            Commission model = new Commission();
            string productList = string.Empty;
            try
            {
                if (id > 0)
                {
                    model = AdminArea_CommissionService.ComissionList(id, "health", "1")[0];
                    productList = AdminArea_CommissionService.GetProductList("health", model.CompanyId.ToString(), null);
                    productList = productList.Replace("<option value=\"\">Select Product</option>", "<option value=\"all\">All</option>");
                    result.Add(model);
                    result.Add(productList);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result);
        }
        public JsonResult AddEditHealthComission_Post(Commission model)
        {
            bool IsSuccess = false;
            try
            {
                IsSuccess = AdminArea_CommissionService.AddEditCommission(model);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(IsSuccess);
        }
        public JsonResult DeleteCommission(string id)
        {
            bool IsSuccess = false;
            IsSuccess = AdminArea_CommissionService.DeleteCommission(id);
            return Json(IsSuccess);
        }
        #endregion

        #region [Health Black]
        public ActionResult HealthInsentiveComissionList()
        {
            IncentiveCommission model = new IncentiveCommission();
            model.IncentiveList = AdminArea_CommissionService.GetHealthInsentiveComissionList("1", null);
            return View(model);
        }
        public ActionResult UpdateInsentiveComission(string id, string status)
        {
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(status))
            {
                bool isdeleted = AdminArea_CommissionService.UpdateInsentiveComission(id, status);
            }
            return Redirect("/adminarea/backend_commision/healthinsentivecomissionlist");
        }
        public ActionResult AddEditInsentiveComission(string id)
        {
            IncentiveCommission model = new IncentiveCommission();

            if (!string.IsNullOrEmpty(id))
            {
                model = AdminArea_CommissionService.GetHealthInsentiveComissionList("1", id).FirstOrDefault();
            }
            model.GroupList = CommonClass.PopulateGroup(AdminArea_GroupService.GetGroupList(null, "1"));
            model.AgencyList = CommonClass.PopulateAgency(FrontAgencyService.GetAgencyList(null, "1"));
            model.InsurerList = CommonClass.PopulateInsurer(AdminArea_RoleService.GetCompanyList(null, "1"));
            model.ProductList = CommonClass.PopulateProduct(AdminArea_RoleService.GetProductList("0", "1", model.InsurerId));

            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditInsentiveComission(string id, IncentiveCommission model)
        {
            string redirecturl = string.Empty;
            model.id = !string.IsNullOrEmpty(id) ? Convert.ToInt32(id) : 0;
            if (AdminArea_CommissionService.AddEditInsentiveComission(model))
            {
                redirecturl = "/adminarea/backend_commision/healthinsentivecomissionlist";
            }
            return Redirect(redirecturl);
        }
        public JsonResult GetProductList(string insurerid)
        {
            return Json(AdminArea_RoleService.GetProductList(null, "1", insurerid));
        }
        #endregion

        #region[Common Area]
        public ActionResult CommissionReport()
        {
            List<object> list = new List<object>();
            string agencyList = BindAgency();
            list.Add(agencyList);
            return View(list);
        }
        public JsonResult GenerateCommissionReport(string agencyId, string time)
        {
            string result = string.Empty;
            try
            {
                result = AdminArea_CommissionService.GenerateCommissionReport(agencyId, time);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        #endregion

        #region[Common Lists]
        public string BindGroups()
        {
            string result = string.Empty;
            try
            {
                List<SelectListItem> list = CommonClass.PopulateGroup(AdminArea_GroupService.GetGroupList(null, "1"));

                if (list.Count > 0)
                {
                    result = "<option value=\"0\">Select Group</option>";
                }
                foreach (var item in list)
                {
                    result = result + "<option value=\"" + item.Value + "\">" + item.Text + "</option>";
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
            //return Json(result);
        }
        public string BindAgency()
        {
            string result = string.Empty;
            try
            {
                var agencyList = FrontAgencyService.GetAgencyList(null, "1");
                List<SelectListItem> list = CommonClass.PopulateAgency(agencyList);
                if (list.Count > 0)
                {
                    result = "<option value='0'>Select Agency</option>";
                }
                for (int i = 0; i < list.Count; i++)
                {
                    result = result + "<option value=\"" + list[i].Value + "\" data-text=\"" + list[i].Text + "\" data-city=\"" + agencyList[i].CityName + "\">" + list[i].Text + "  (" + list[i].Value + ")  - " + agencyList[i].CityName + "</option>";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            //return Json(result);
            return result;
        }
        public string BindCompany(string type)
        {
            string result = string.Empty;
            try
            {
                result = AdminArea_CommissionService.GetCompanyList(type);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            //return Json(result);
            return result;
        }
        public JsonResult BindProduct(string type, string CompanyId, string vehicleType = null)
        {
            string result = string.Empty;
            try
            {
                result = AdminArea_CommissionService.GetProductList(type, CompanyId, vehicleType);
                result = result.Replace("<option value=\"\">Select Product</option>", "<option value=\"all\">All</option>");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result);
        }
        #endregion
    }
}