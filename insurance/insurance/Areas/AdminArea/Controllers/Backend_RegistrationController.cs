﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Models.Common;
using ecommerce.Models.Common;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using static insurance.Models.Custom.FrontAgencyModel;
using insurance.Service;
using insurance.Helper.DataBase;
using System.Data;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.Models;
using insurance.Models;
using Post_Utility.Utility;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_RegistrationController : Controller
    {
        #region [Member]
        public ActionResult MemberRegistrationList(string status)
        {
            MemberLogin model = new MemberLogin();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.MemberList = AdminArea_RegistrationService.GetMemberList(null,null,status) ;
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }
        public ActionResult AddEditMemberRegistration(string memberid)
        {
            MemberLogin model = new MemberLogin();
            try
            {
                if (!string.IsNullOrEmpty(memberid))
                {
                    model = AdminArea_RegistrationService.GetMemberList(memberid, "1").FirstOrDefault();
                }

                if (model != null)
                {
                    model.RoleList = CommonClass.PopulateRole(AdminArea_RoleService.GetRoleList(null, "1"));
                }
                else {
                    model = new MemberLogin();
                    model.RoleList = new List<SelectListItem>();
                    model.RoleList.Add(new SelectListItem
                    {
                        Text = "No Data Found",
                        Value = ""
                    });
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public JsonResult ProcessInsertMember(MemberLogin member)
        {
            List<string> result = new List<string>();

            try
            {
                if (member.LoginId > 0)
                {
                    if (AdminArea_RegistrationService.UpdateMemberDetail(member))
                    {
                        TempData["memberid"] = member.LoginId;
                        result.Add("true");
                        result.Add("update");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
                else
                {
                    int memberId = AdminArea_RegistrationService.InsertMemberDetail(member);
                    if (memberId > 0)
                    {
                        TempData["memberid"] = memberId;
                        result.Add("true");
                        result.Add("insert");
                        result.Add("Record inserted successfully.");
                        Email.SendMemberDetails(member);
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }

        public JsonResult UpdateMemberImageName()
        {
            string returnMsg = "false";
            try
            {
                if (TempData["memberid"] != null)
                {
                    string memberid = TempData["memberid"].ToString();

                    if (Request.Files.Count > 0)
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            string fname = file.FileName;

                            if (!string.IsNullOrEmpty(fname))
                            {
                                Directory.CreateDirectory(Server.MapPath(UtilityClass.GetFullPathStringWithAppData("MemberImages", ("Member_" + memberid))));

                                string ext = Path.GetExtension(file.FileName);
                                string imageurlname = UtilityClass.GenrateRandomTransactionId(memberid, 3) + ext;
                                string filePath = Server.MapPath(UtilityClass.GetFullPathStringWithAppData("MemberImages", ("Member_" + memberid)) + imageurlname);

                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                if (AdminArea_RegistrationService.UpdateMemberImage(memberid, imageurlname))
                                {
                                    returnMsg = "true";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(returnMsg);
        }
        public JsonResult UpdateMemberStatus(string memberid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_RegistrationService.UpdateMemberStatus(memberid, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult IsUserAlreadyExist(string userid)
        {
            string result = "false";

            try
            {
                if (AdminArea_RegistrationService.IsUserAlreadyExist(userid))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Agency]
        public ActionResult AgentDetails(string agencyid)
        {
            CreateAgency model = new CreateAgency();
            try
            {
                model = FrontAgencyService.GetAgencyList(agencyid, "1").FirstOrDefault();
                model.RefByList = CommonClass.PopulateRefBy(AdminArea_RegistrationService.GetMemberList(null, "4", "1"));
                model.branchList = CommonClass.Populatebranch(AdminArea_RegistrationHelper.GetBranch());
                model.GroupTypeList = CommonClass.PopulateGroupType(AdminArea_GroupHelper.GetGroupList(null, null));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult AgentDetails(CreateAgency model)
        {
            bool isSuccess = false;
            try
            {
                if (!string.IsNullOrEmpty(model.AgencyID))
                {                    
                    isSuccess = FrontAgencyService.ActiveAgentDetails(Convert.ToInt32(model.AgencyID), model);                   
                    TempData["Message"] = "success";
                }
            }
            catch(Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult AgencyRegistrationList(string status)
        {
            CreateAgency model = new CreateAgency();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "(0,1)";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }
                model.AgencyList = FrontAgencyService.GetAgencyList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult AgencyRegistration(string agencyid = null)
        {
            CreateAgency model = new CreateAgency();

            try
            {
                if (agencyid != null && !string.IsNullOrEmpty(agencyid.Trim()))
                {
                    model = FrontAgencyService.GetAgencyList(agencyid).FirstOrDefault();
                    model.RegUserId = model.UserId;
                    model.RegPassword = model.Password;
                    model.ConfPassword = model.Password;
                    model.CountryList = CommonClass.PopulateCountry(FrontAgencyService.CountryList(null, "1"));
                    model.StateList = CommonClass.PopulateState(FrontAgencyService.StateList(null, model.CountryID.ToString(), "1"));
                    foreach (var state in model.StateList)
                    {
                        if (Convert.ToInt32(state.Value) == model.StateID)
                        {
                            state.Selected = true;
                            break;
                        }
                    }
                    model.CityList = CommonClass.PopulateCity(FrontAgencyService.CityList(model.Pincode, null, null, null, null));
                    model.CityList[0].Selected = true;
                    model.RefByList = CommonClass.PopulateRefBy(FrontAgencyService.RefByList("1", "1"));
                    model.ConfPassword = model.Password;
                }
                else
                {
                    model.CountryList = CommonClass.PopulateCountry(FrontAgencyService.CountryList(null, "1"));
                    model.StateList = new List<SelectListItem>();
                    model.CityList = new List<SelectListItem>();
                    model.RefByList = CommonClass.PopulateRefBy(FrontAgencyService.RefByList("1", "1"));
                    model.Status = true;
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateAgency(int? agencyid, CreateAgency model, IEnumerable<HttpPostedFileBase> files)
        {
            bool isSuccess = false;
            int agencyID = 0;

            try
            {
                if (agencyid > 0)
                {
                    agencyID = agencyid.Value;
                    isSuccess = FrontAgencyService.UpdateAgencyDetail(agencyid, model);
                }
                else
                {
                    agencyID = FrontAgencyService.InserNewAgency(model);
                    Email.SendAgencyRegistrationDetails(model, null);
                }

                //For image insert in folder
                if (agencyID > 0)
                {
                    CreateAgency tempModel = new CreateAgency();

                    Directory.CreateDirectory(Server.MapPath(UtilityClass.GetFullPathWithAppData("Agency", agencyID)));

                    int index = 0;

                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            string ext = Path.GetExtension(file.FileName);
                            string filePath = Server.MapPath(UtilityClass.GetFullPathWithAppData("Agency", agencyID) + CommonClass.SetDocumentName(index, ext));

                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            file.SaveAs(filePath);

                            if (index == 0)
                            {
                                tempModel.BusinessAddressProofImg = CommonClass.SetDocumentName(index, ext);
                            }
                            else if (index == 1)
                            {
                                tempModel.BusinessPanCardImg = CommonClass.SetDocumentName(index, ext);
                            }
                            else if (index == 2)
                            {
                                tempModel.PersonalAddressProofImg = CommonClass.SetDocumentName(index, ext);
                            }
                            //else if (index == 3)
                            //{
                            //    tempModel.PersonalAddressProofImg2 = Common.SetDocumentName(index, ext);
                            //}
                            else if (index == 3)
                            {
                                tempModel.CompanyLogoImg = CommonClass.SetDocumentName(index, ext);
                            }
                        }

                        index++;
                    }

                    if (!string.IsNullOrEmpty(tempModel.BusinessAddressProofImg) || !string.IsNullOrEmpty(tempModel.BusinessPanCardImg) || !string.IsNullOrEmpty(tempModel.PersonalAddressProofImg) || !string.IsNullOrEmpty(tempModel.CompanyLogoImg))
                    {
                        isSuccess = FrontAgencyService.UpdateImagesAgency(agencyID, tempModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            TempData["Message"] = "success";
            return RedirectToAction("AgencyRegistrationList", "Backend_Registration");
        }

        #region[Agency Json]
        public JsonResult AgencyStatusUpdator(int id, bool status)
        {
            CreateAgency model = new CreateAgency();
            List<object> result = new List<object>();
            bool isActivated = FrontAgencyService.AgencyStatusUpdator(id, status);            
            
            if (isActivated)
            {
                if (status)
                {
                    model = FrontAgencyService.GetAgencyList(id.ToString(), "1").FirstOrDefault();
                    Email.SendAgencyRegconfirmDetails(model.UserId, model.BusinessEmailID, model.AgencyName, Security.ApiBase64Decode(model.Password));
                }
                
                result.Add("Success");
            }
            return Json(result);
        }
        public JsonResult ValidateUserID(string userID)
        {
            string result = "success";
            int count = ConnectToDataBase.GetRecordFromTable("*", "aspnet_user", " UserName='" + userID + "'", "api").Rows.Count;

            if (count == 1)
            {
                result = "failed";
            }
            return Json(result);

        }
        #endregion

        #endregion

        public ActionResult BindState(string countryid)
        {
            CreateAgency model = new CreateAgency();
            model.StateList = CommonClass.PopulateState(FrontAgencyService.StateList(null, countryid, "1"));
            return Json(model.StateList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BindCity(string pinCode, string stateName)
        {
            CreateAgency model = new CreateAgency();
            model.CityName = FrontAgencyService.CityListWithPin(pinCode);
            return Json(model.CityName, JsonRequestBehavior.AllowGet);
        }
    }
}