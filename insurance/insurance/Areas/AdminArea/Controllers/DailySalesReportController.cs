﻿using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Helper.DataBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Areas.AdminArea.Controllers
{
    public class DailySalesReportController : Controller
    {
        // GET: AdminArea/DailySalesReport
        public ActionResult DsrReport(Agency model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.AgencyName))
                {

                    MemberLogin lu = new MemberLogin();
                    if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                    {
                        if (lu.MemberType.ToLower() == "staff")
                        {
                            model.StaffId = lu.LoginId;
                        }
                    }
                    model = AdminArea_AutoCompleteService.GetAgencyDetail(model.AgencyName, null, "1");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult SaveDsrReport(Agency model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.AgencyFeedBack) && !string.IsNullOrEmpty(model.UserFeedBack))
                {
                    MemberLogin lu = new MemberLogin();
                    if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                    {
                        model.CreatedByUserID = lu.LoginId.ToString();
                        model.CreatedByUserName = lu.FirstName + lu.LastName;
                        model.ReadStatus = "Inprocess";

                        if (AdminArea_RegistrationService.SaveDsrReport(model))
                        {
                            //string moblieNo = model.BusinessPhone;
                            //string datetime = DateTime.Now.ToString("MM/dd/yyyy h:mm tt");
                            //string message = "I " + lu.StaffName + " from SST visited your office at " + datetime + ". Please contact me on 0" + lu.Mobile + " for any further Travel related Queries.";
                            //if (!string.IsNullOrEmpty(moblieNo) && !string.IsNullOrEmpty(message))
                            //{
                            //    if (model.AgencyID > 0)
                            //    {
                            //        ConfigureMailService.GenrateSMSRequestXMLString(moblieNo, message, model.AgencyID.ToString());
                            //    }
                            //    else
                            //    {
                            //        ConfigureMailService.GenrateSMSRequestXMLString(moblieNo, message, model.AgencyIdNew.ToString());
                            //    }
                            //}
                            TempData["Message"] = "Your feedback has been saved successfully.";
                            return Redirect("/profile/dsrlist");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("/profile/DsrList");
        }
        public ActionResult DsrReportlist(Agency Dsrmodel, string type)
        {
            MemberLogin lu = new MemberLogin();
            if (AdminArea_AccountService.IsBackUserLogin(ref lu))
            {
                Dsrmodel.CreatedByUserName = lu.FirstName + lu.LastName;
                Dsrmodel.Type = lu.MemberType;
                string todayDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
                Dsrmodel.TempCreditDate = Reverse(todayDate.Trim().Replace('/', '-'), string.Empty);
            }
            string TableName = "T_DailySalesReport";
            string ColumnName = "count(*) as DsrReporCount";
            string WhereCondition = "ReadStatus='inprocess'";
            DataTable dtnewDsr = ConnectToDataBase.GetRecordFromTable(ColumnName, TableName, WhereCondition, "seeinsured");
            if (dtnewDsr.Rows.Count > 0)
            {
                Dsrmodel.NewdrsReport = !string.IsNullOrEmpty(dtnewDsr.Rows[0]["DsrReporCount"].ToString()) ? dtnewDsr.Rows[0]["DsrReporCount"].ToString() : "0";
            }
            Dsrmodel.AgencyList = AdminArea_RegistrationService.GetDsrreportList(Dsrmodel, type);

            return View(Dsrmodel);
        }

        public ActionResult ReadReplyDsrReport(Agency model, int? dsrid, string readstatus)
        {
            if (dsrid > 0)
            {
                MemberLogin lu = new MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    model.CreatedByUserName = lu.FirstName + lu.LastName;
                    model.Type = lu.MemberType;
                }
                if (model.ReadStatus == "Inprocess")
                {
                    model.ReadStatus = "Read";
                    if (AdminArea_RegistrationService.ReadbyAdminUpdate(model))
                    {
                        model.AgencyList = AdminArea_RegistrationService.GetDsrreportbyID(model);
                    }
                }
                else
                {
                    model.AgencyList = AdminArea_RegistrationService.GetDsrreportbyID(model);
                }
            }
            return View(model);
        }
        public ActionResult AdminReply(Agency model)
        {
            try
            {
                if (model.DsrID > 0)
                {
                    if (!string.IsNullOrEmpty(model.AdminRemark))
                    {
                        if (AdminArea_RegistrationService.ReplybyAdminUpdate(model))
                        {
                            TempData["Message"] = "Your Reply has been  Send successfully.";
                            return Redirect("/profile/DsrList");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("/profile/DsrList");
        }

        public ActionResult LeadRequestList(LeadRequestModel model)
        {
            string todayDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            model.TempDate = Reverse(todayDate.Trim().Replace('/', '-'), string.Empty);
            model.LeadRequestList = AdminArea_RegistrationService.GetleadRequestlist(model);
            return View(model);
        }
        public static string Reverse(string str, string isAdd)
        {
            string date = string.Empty;
            string month = string.Empty;
            string year = string.Empty;
            string result = string.Empty;

            if (!string.IsNullOrEmpty(str))
            {
                string[] objStr = str.Split('-');

                for (int i = 0; i < objStr.Count(); i++)
                {
                    if (i == 0)
                    {
                        //if (!string.IsNullOrEmpty(isAdd))
                        //{
                        //    int addedDate = Utility.ConvertToInteger(objStr[i]) + 1;
                        //    if (addedDate < 32)
                        //    {
                        //        date = addedDate.ToString();
                        //    }
                        //    else
                        //    {
                        //        date = objStr[i];
                        //    }
                        //}
                        //else
                        //{
                        date = objStr[i];
                        //}
                    }
                    else if (i == 1)
                    {
                        month = objStr[i];
                    }
                    else if (i == 2)
                    {
                        year = objStr[i];
                    }
                }

                if (!string.IsNullOrEmpty(date) && !string.IsNullOrEmpty(month) && !string.IsNullOrEmpty(year))
                {
                    if (!string.IsNullOrEmpty(isAdd) && isAdd == "removehifan")
                    {
                        result = year + "" + month + "" + date;
                    }
                    else
                    {
                        result = year + "-" + month + "-" + date;
                    }
                }
            }

            return result;
        }

    }
}