﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Account;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Helper.DataBase;
using insurance.Helper.HealthHelper;
using insurance.Models.Common;
using insurance.Models.Motor;
using insurance.Service;
using insurance.Service.MotorService;
using static insurance.Models.Custom.FrontAgencyModel;
using static insurance.Models.Health.HealthModel;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_EnquiryController : Controller
    {
        public ActionResult TwowheelerMap(string make = null, string vehicleModel = null, string fuelType = null)
        {
            TwowheelermapModel models = new TwowheelermapModel();
            try
            {
                if (!string.IsNullOrEmpty(make) && !string.IsNullOrEmpty(vehicleModel))
                {
                    models.Make = make.Trim();
                    models.vehicleModel = vehicleModel.Trim();
                    models.FuelType = fuelType;
                }
                models.Makelist = CommonClass.PopulateMake(AdminArea_EnquiryHelper.GetMake());
                models.ModelList = CommonClass.PopulateModel(AdminArea_EnquiryHelper.GetModel(models.Make));
                models.MapingList = AdminArea_EnquiryHelper.GetTwoWheelerMaplist(models.Make, models.vehicleModel, models.FuelType);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(models);
        }

        public ActionResult BikeMap(string make = null, string vehicleModel = null, string fuelType = null)
        {
            TwowheelermapModel models = new TwowheelermapModel();
            try
            {
                if (!string.IsNullOrEmpty(make) && !string.IsNullOrEmpty(vehicleModel))
                {
                    models.Make = make.Trim();
                    models.vehicleModel = vehicleModel.Trim();
                    models.FuelType = fuelType;
                }
                models.Makelist = CommonClass.PopulateMake(AdminArea_EnquiryHelper.GetBikeMake());
                models.ModelList = CommonClass.PopulateModel(AdminArea_EnquiryHelper.GetBikeModel(models.Make));
                models.MapingList = AdminArea_EnquiryHelper.GetTBikeMaplist(models.Make, models.vehicleModel, models.FuelType);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(models);
        }

        public ActionResult BindModel(string make)
        {
            TwowheelermapModel models = new TwowheelermapModel();
            models.ModelList = CommonClass.PopulateModel(AdminArea_EnquiryHelper.GetModel(make));
            return Json(models.ModelList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BindBikeModel(string make)
        {
            TwowheelermapModel models = new TwowheelermapModel();
            models.ModelList = CommonClass.PopulateModel(AdminArea_EnquiryHelper.GetBikeModel(make));
            return Json(models.ModelList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateMapList(List<TwowheelermapModel> MapList)
        {
            string result = "";
            for (int i = 0; i < MapList.Count(); i++)
            {

                string fieldswithvalue = "GoDigit = '" + MapList[i].GoDigit + "' ,ShriRam = '" + MapList[i].ShriRam + "',IFFCOTokio='" + MapList[i].IFFCOTokio + "'";
                string tablename = "tblMaster_Car";
                string wherecondition = "vehicleMaincode = '" + MapList[i].MapID + "'";
                bool IsSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition, "motor");
                if (IsSuccess) { result = "Success"; }
            }
            return Json(result);
        }

        public JsonResult UpdateBikeMapList(List<TwowheelermapModel> MapList)
        {
            string result = "";
            for (int i = 0; i < MapList.Count(); i++)
            {

                string fieldswithvalue = "GoDigit = '" + MapList[i].GoDigit + "' ,ShriRam = '" + MapList[i].ShriRam + "',IFFCOTokio='" + MapList[i].IFFCOTokio + "'";
                string tablename = "tblMaster_TwoWheeler";
                string wherecondition = "vehicleMaincode = '" + MapList[i].MapID + "'";
                bool IsSuccess = ConnectToDataBase.UpdateRecordIntoAnyTable(tablename, fieldswithvalue, wherecondition, "motor");
                if (IsSuccess) { result = "Success"; }
            }
            return Json(result);
        }


        #region[Health]

        public ActionResult HealthRenewalList(string EnquiryId = null, string PolicyNo = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string CustomerName = null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                list = AdminArea_EnquiryService.GetRenewalList_Health(EnquiryId, PolicyNo, AgencyId, Insurer, Product, PolicyType, CustomerName);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }

        public ActionResult HealthPolicyList(string EnquiryId = null, string PolicyNo = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string FromDate = null, string ToDate = null, string CustomerName = null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                list = AdminArea_EnquiryService.GetEnquiryDetailsByAgencyHealth("policy", EnquiryId, null, PolicyNo, AgencyId, Insurer, Product, PolicyType, FromDate, ToDate, CustomerName);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult HealthProposalList(string EnquiryId = null, string ProposalNo = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string FromDate = null, string ToDate = null, string CustomerName = null)
        {
            List<ProposalDetailsHealth> list = new List<ProposalDetailsHealth>();
            try
            {
                list = AdminArea_EnquiryService.GetEnquiryDetailsByAgencyHealth("proposal", EnquiryId, ProposalNo, null, AgencyId, Insurer, Product, PolicyType, FromDate, ToDate, CustomerName);

            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(list);
        }
        public ActionResult HealthProposalSummaryDetail(string enquiryid)
        {
            return View();
        }
        public ActionResult NewEnquiryList(string status, string EnquiryId = null, string AgencyId = null, string PolicyType = null, string FromDate = null, string ToDate = null)
        {
            EnquiryDetails enq = new EnquiryDetails();
            string userid = "";
            try
            {

                Account.MemberLogin lu = new Account.MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    userid = lu.UserId;
                }

                enq.EnquiryDetailList = AdminArea_EnquiryService.GetEnquiryDetailList(userid, status, EnquiryId, AgencyId, PolicyType, FromDate, ToDate);
            }
            catch (Exception)
            {

                throw;
            }

            return View(enq);
        }
        public JsonResult BlockEnquiry(string Enquiry_ID)
        {
            string result = string.Empty;

            string BlockID = ""; string EnquiryID = "";
            Account.MemberLogin lu = new Account.MemberLogin();
            if (!AdminArea_AccountService.IsBackUserLogin(ref lu))
            {
                BlockID = "";
            }
            else
            {
                BlockID = lu.UserId;
            }

            EnquiryID = Enquiry_ID;
            try
            {

                if (AdminArea_EnquiryService.BlockEnquiry(Enquiry_ID, BlockID))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return Json(result);
        }

        public ActionResult BlockedEnquiryList(string status)
        {
            EnquiryDetails enq = new EnquiryDetails();
            string userid = "";
            try
            {

                Account.MemberLogin lu = new Account.MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    userid = lu.UserId;
                }
                if (string.IsNullOrEmpty(status))
                {
                    status = "Block";
                }


                enq.EnquiryDetailList = AdminArea_EnquiryService.GetBlockedEnquiryList(userid, status);
            }
            catch (Exception)
            {

                throw;
            }

            return View(enq);
        }

        public JsonResult AcceptRejectBlockedEnquiry(string Enquiry_ID, string status)
        {
            string result = string.Empty;

            string BlockID = ""; string EnquiryID = "";
            Account.MemberLogin lu = new Account.MemberLogin();
            if (!AdminArea_AccountService.IsBackUserLogin(ref lu))
            {
                BlockID = "";
            }
            else
            {
                BlockID = lu.UserId;
            }

            EnquiryID = Enquiry_ID;
            try
            {

                if (AdminArea_EnquiryService.ActiveBlockEnquiry(Enquiry_ID, BlockID, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return Json(result);
        }

        public JsonResult BlockedEnquiryDetails(string Enquiry_ID)
        {

            var result = AdminArea_EnquiryService.TrackDetails(Enquiry_ID);


            return Json(result);
        }

        public ActionResult EnquiryTrackDetails(string Enquiry_ID)
        {
            TrackEnquirydetail enq = new TrackEnquirydetail();
            string userid = "";
            try
            {

                Account.MemberLogin lu = new Account.MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    userid = lu.UserId;
                }

                enq.TrackEnquirydetailList = AdminArea_EnquiryService.TrackDetails(Enquiry_ID);
            }
            catch (Exception)
            {

                throw;
            }

            return View(enq);
        }
        public JsonResult BindAgencies()
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindAgencies();
            return Json(result);
        }
        public JsonResult BindInsurers()
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindInsurers("Health");
            return Json(result);
        }
        public JsonResult BindProducts(string insuranceId)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindProducts("Health", insuranceId);
            return Json(result);
        }
        public ActionResult HealthEnquiryTrackDetails(string Enquiry_ID)
        {
            return View();
        }
        public JsonResult BindHealthTrackDetails(string enquiry_id)
        {
            string result = string.Empty;
            try
            {
                DataTable dtAgencyDel = new DataTable();
                DataTable dtInsuredDel = new DataTable();
                DataTable dtPerposerDetail = Health_Helper.GetHealthTrackDetail(enquiry_id, ref dtAgencyDel, ref dtInsuredDel);

                StringBuilder str = new StringBuilder();

                str.Append("<tr><th class='text-center bg fontsize' style='border: 3px solid black;' colspan='12'><span><b>OrderID / Insurance Details Page</b></span></th></tr>");
                str.Append("<tr>");
                str.Append("<th colspan = '4' class='text-center' width='400'> <b></b></th>");
                str.Append("<th colspan = '4' class='text-center' width='400'> <b>Agency Information</b></th>");
                str.Append("<th colspan = '4' class='text-center' width='400'> <b>Invoice Information</b></th>");
                str.Append("</tr>");
                str.Append("<tr>");

                str.Append("<td colspan = '4' width='400'>");
                str.Append("<div><img src='" + (!string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["logoUrl"].ToString()) ? dtPerposerDetail.Rows[0]["logoUrl"].ToString() : "/Content/images/imgnotavl.PNG") + "' alt='company_logo' width='200' height='auto'></div>");
                str.Append("</td>");

                str.Append("<td colspan = '4' width='400'>");
                str.Append("<p>Agency : <span>" + dtAgencyDel.Rows[0]["AgencyName"].ToString() + "</span></p>");
                str.Append("<p>Contact No : <span>" + dtAgencyDel.Rows[0]["BusinessPhone"].ToString() + "</span></p>");
                str.Append("<p>Email : <span>" + dtAgencyDel.Rows[0]["EmailId"].ToString() + "</span></p>");
                str.Append("<p>Address: <span>" + dtAgencyDel.Rows[0]["Address"].ToString() + "</span></p>");
                str.Append("</td>");

                str.Append("<td colspan = '4' width='400'>");
                str.Append("<p>&nbsp;</p>");
                str.Append("<p>Order ID : <span>" + enquiry_id.ToUpper() + "</span></p>");
                str.Append("<p>Proposal No : <span>" + dtPerposerDetail.Rows[0]["pay_proposal_Number"].ToString() + "</span></p>");
                str.Append("<p>Policy No : <span>" + dtPerposerDetail.Rows[0]["pay_policy_Number"].ToString() + "</span></p>");
                str.Append("<p>Payment Date : <span>" + dtPerposerDetail.Rows[0]["pay_paymentdate"].ToString() + " </span></p>");
                str.Append("</td>");

                str.Append("</tr>");

                str.Append("<tr><th colspan = '12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Agency Owner / Manager Information</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='4'><b>Owner/Manager&nbsp;Name</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>Email&nbsp;ID</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>Mobile</b></th>");
                str.Append("</tr>");

                str.Append("<tr style = 'text-align:center;' >");
                str.Append("<td class='text-center' colspan='4'>" + dtAgencyDel.Rows[0]["OwnerName"].ToString() + "</td>");
                str.Append("<td class='text-center' colspan='4'>" + dtAgencyDel.Rows[0]["EmailId"].ToString() + "</td>");
                str.Append("<td class='text-center' colspan='4'>" + dtAgencyDel.Rows[0]["BusinessPhone"].ToString() + "</td>");
                str.Append("</tr>");


                str.Append("<tr><th colspan = '12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Agency Sales Person Informed</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th colspan = '4' class='text-center'><b>Name</b></th>");
                str.Append("<th colspan = '4' class='text-center'><b>Email&nbsp;ID</b></th>");
                str.Append("<th colspan = '4' class='text-center'><b>Mobile</b></th>");
                str.Append("</tr>");

                str.Append("<tr style = 'text-align:center;' >");
                str.Append("<td colspan='4' class='text-center'>" + dtAgencyDel.Rows[0]["RefName"].ToString().ToUpper() + "</td>");
                str.Append("<td colspan = '4' class='text-center'>" + dtAgencyDel.Rows[0]["RefEmailId"].ToString() + "</td>");
                str.Append("<td colspan = '4' class='text-center'>" + dtAgencyDel.Rows[0]["RefMobileNo"].ToString() + "</td>");
                str.Append("</tr>");

                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Enquiry Details</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='1'><b>Policy Type</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Sum Insured</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Self Age</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Spouse Age</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Sons' Age</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Daughters' Age</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Father Age</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Mother Age</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Pin Code</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["Policy_Type"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + dtPerposerDetail.Rows[0]["Sum_Insured"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["SelfAge"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["SpouseAge"].ToString() + "</b></th>");
                string sonage = dtPerposerDetail.Rows[0]["Son1Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Son2Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Son3Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Son4Age"].ToString();
                str.Append("<th class='text-center' colspan='2'><b>" + sonage + "</b></th>");
                string betiage = dtPerposerDetail.Rows[0]["Daughter1Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Daughter2Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Daughter3Age"].ToString() + "/" + dtPerposerDetail.Rows[0]["Daughter4Age"].ToString();
                str.Append("<th class='text-center' colspan='2'><b>" + betiage + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["FatherAge"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["MotherAge"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>" + dtPerposerDetail.Rows[0]["Pin_Code"].ToString() + "</b></th>");
                str.Append("</tr>");

                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Proposer Details</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='2'><b>Proposer Name</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Contact No</b></th>");
                str.Append("<th class='text-center' colspan='3'><b>Email ID</b></th>");
                str.Append("<th class='text-center' colspan='3'><b>Address</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Annual Income</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");

                str.Append("<th class='text-center' colspan='2'><b>" + dtPerposerDetail.Rows[0]["PerposerName"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + dtPerposerDetail.Rows[0]["mobile"].ToString() + "</b></th>");
                str.Append("<th class='text-center' colspan='3'><b>" + dtPerposerDetail.Rows[0]["emailid"].ToString() + "</b></th>");
                string p_address = dtPerposerDetail.Rows[0]["address1"].ToString() + ", " + dtPerposerDetail.Rows[0]["address2"].ToString() + ", " + dtPerposerDetail.Rows[0]["cityname"].ToString() + ", " + dtPerposerDetail.Rows[0]["statename"].ToString() + ", " + dtPerposerDetail.Rows[0]["pincode"].ToString();
                str.Append("<th class='text-center' colspan='3'><b>" + p_address + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + dtPerposerDetail.Rows[0]["annualincome"].ToString() + "</b></th>");
                str.Append("</tr>");

                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Product Details</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='2'><b>CompanyName</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Product Name</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Plan Name</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Sum Insured</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Basic</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Service Tax</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Total Premium</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_insuranceCompany"].ToString()) ? "- - -" : dtPerposerDetail.Rows[0]["row_insuranceCompany"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_policyName"].ToString()) ? "- - -" : dtPerposerDetail.Rows[0]["row_policyName"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_planname"].ToString()) ? "- - -" : dtPerposerDetail.Rows[0]["row_planname"].ToString()) + "</b></th>");

                str.Append("<th class='text-center' colspan='2'><b> " + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["suminsured"].ToString()) ? "- - -" : "₹ " + dtPerposerDetail.Rows[0]["suminsured"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='1'><b> " + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_basePremium"].ToString()) ? "- - -" : "₹ " + dtPerposerDetail.Rows[0]["row_basePremium"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b> " + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_serviceTax"].ToString()) ? "- - -" : "₹ " + dtPerposerDetail.Rows[0]["row_serviceTax"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b> " + (string.IsNullOrEmpty(dtPerposerDetail.Rows[0]["row_totalPremium"].ToString()) ? "- - -" : "₹ " + dtPerposerDetail.Rows[0]["row_totalPremium"].ToString()) + "</b></th>");

                str.Append("</tr>");


                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Insured Details</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='1'><b>Relation</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Name</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>D.O.B</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Height/Weight</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Occupation</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Illness</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Manual Labour</b></th>");
                str.Append("<th class='text-center' colspan='1'><b>Winter Labour</b></th>");
                str.Append("</tr>");

                if (dtInsuredDel != null && dtInsuredDel.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsuredDel.Rows.Count; i++)
                    {
                        str.Append("<tr>");
                        str.Append("<th class='text-center' colspan='1'><span><b>" + dtInsuredDel.Rows[i]["relationname"].ToString() + "</b></span></th>");
                        str.Append("<th class='text-center' colspan='2'><b>" + dtInsuredDel.Rows[i]["title"].ToString().ToUpper() + " " + dtInsuredDel.Rows[i]["firstname"].ToString().ToUpper() + " " + dtInsuredDel.Rows[i]["lastname"].ToString().ToUpper() + "</b></th>");
                        str.Append("<th class='text-center' colspan='1'><b>" + dtInsuredDel.Rows[i]["dob"].ToString() + "</b></th>");
                        str.Append("<th class='text-center' colspan='2'><b>" + dtInsuredDel.Rows[i]["height"].ToString() + " cm/" + dtInsuredDel.Rows[i]["weight"].ToString() + " Kg</b></th>");
                        str.Append("<th class='text-center' colspan='2'><b>" + dtInsuredDel.Rows[i]["occupation"].ToString() + "</b></th>");
                        bool illness = Convert.ToBoolean(dtInsuredDel.Rows[i]["illness"].ToString());
                        str.Append("<th class='text-center' colspan='2'><b>" + (illness == true ? dtInsuredDel.Rows[i]["illnessdesc"].ToString() : "No") + "</b></th>");
                        bool engageManualLabour = Convert.ToBoolean(dtInsuredDel.Rows[i]["engageManualLabour"].ToString());
                        str.Append("<th class='text-center' colspan='1'><b>" + (engageManualLabour == true ? dtInsuredDel.Rows[i]["engageManualLabourDesc"].ToString() : "No") + "</b></th>");
                        bool engageWinterSports = Convert.ToBoolean(dtInsuredDel.Rows[i]["engageWinterSports"].ToString());
                        str.Append("<th class='text-center' colspan='1'><b>" + (engageWinterSports == true ? dtInsuredDel.Rows[i]["engageWinterSportDesc"].ToString() : "No") + "</b></th>");
                        str.Append("</tr>");
                    }
                }
                else
                {
                    str.Append("<tr>");
                    str.Append("<th class='text-center' colspan='1'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='1'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='1'><b>-----</b></th>");
                    str.Append("<th class='text-center' colspan='1'><b>-----</b></th>");
                    str.Append("</tr>");
                }

                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Nominee Details</b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='4'><b>Relation</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>Name</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>D.O.B</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='4'><b>" + (dtPerposerDetail.Rows[0]["nRelation"] != null ? dtPerposerDetail.Rows[0]["nRelation"].ToString() : "- - -") + "</b></th>");
                string nomineetitle = dtPerposerDetail.Rows[0]["nTitle"] != null ? dtPerposerDetail.Rows[0]["nTitle"].ToString() : string.Empty;
                string nomineefname = dtPerposerDetail.Rows[0]["nTitle"] != null ? dtPerposerDetail.Rows[0]["nTitle"].ToString() : string.Empty;
                string nomineelname = dtPerposerDetail.Rows[0]["nTitle"] != null ? dtPerposerDetail.Rows[0]["nTitle"].ToString() : string.Empty;
                string nomieename = !string.IsNullOrEmpty(nomineetitle) ? (nomineetitle.ToUpper() + " " + nomineefname.ToUpper() + " " + nomineelname.ToUpper()) : "- - -";
                str.Append("<th class='text-center' colspan='4'><b>" + nomieename + "</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>" + (dtPerposerDetail.Rows[0]["nDOB"] != null ? dtPerposerDetail.Rows[0]["nDOB"].ToString() : "- - -") + "</b></th>");
                str.Append("</tr>");



                str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Payment Details </b></th></tr>");
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='2'><b>Payment Date</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Premium Paid</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>Transaction Reference</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Payment Status</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>Policy Status</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='2'><b>" + (dtPerposerDetail.Rows[0]["py_paymentdate"] == null ? "---" : dtPerposerDetail.Rows[0]["py_paymentdate"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + (dtPerposerDetail.Rows[0]["pp_totalPremium"] == null ? "---" : dtPerposerDetail.Rows[0]["pp_totalPremium"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='4'><b>" + (dtPerposerDetail.Rows[0]["cc_transactionRefNum"] == null ? "---" : dtPerposerDetail.Rows[0]["cc_transactionRefNum"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + (dtPerposerDetail.Rows[0]["Payment_Status"] == null ? "---" : dtPerposerDetail.Rows[0]["Payment_Status"].ToString()) + "</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>" + (dtPerposerDetail.Rows[0]["Policy_Status"] == null ? "---" : dtPerposerDetail.Rows[0]["Policy_Status"].ToString()) + "</b></th>");
                str.Append("</tr>");

                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='4'><b>Payment Url</b></th>");
                str.Append("<th class='text-center' colspan='8'><b>" + (dtPerposerDetail.Rows[0]["paymenturl"] == null ? "---" : dtPerposerDetail.Rows[0]["paymenturl"].ToString()) + "</b></th>");
                str.Append("</tr>");


                result = str.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result);
        }
        #endregion

        #region[Motor]
        public ActionResult MotorPolicyList(string FromDate = null, string ToDate = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string EnquiryId = null, string PolicyNo = null, string CustomerName = null)
        {
            List<ProposalDetailsMotor> enq = new List<ProposalDetailsMotor>();
            try
            {
                enq = AdminArea_EnquiryService.GetEnquiryDetailsMotor("Policy", FromDate, ToDate, AgencyId, Insurer, Product, PolicyType, EnquiryId, null, PolicyNo, CustomerName);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(enq);
        }
        public ActionResult MotorProposalList(string FromDate = null, string ToDate = null, string AgencyId = null, string Insurer = null, string Product = null, string PolicyType = null, string EnquiryId = null, string ProposalNo = null, string CustomerName = null)
        {
            List<ProposalDetailsMotor> enq = new List<ProposalDetailsMotor>();
            try
            {

                enq = AdminArea_EnquiryService.GetEnquiryDetailsMotor("Proposal", FromDate, ToDate, AgencyId, Insurer, Product, PolicyType, EnquiryId, ProposalNo, null, CustomerName);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(enq);
        }
        public ActionResult NewEnquiryList_Motor(string status = null, string FromDate = null, string ToDate = null, string AgencyId = null, string PolicyType = null, string EnquiryId = null)
        {
            List<EnquiryDetails_Motors> enq = new List<EnquiryDetails_Motors>();
            string userid = "";
            try
            {

                Account.MemberLogin lu = new Account.MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    userid = lu.UserId;
                }

                enq = AdminArea_EnquiryService.GetEnquiryDetailList_Motor(userid, status, FromDate, ToDate, AgencyId, PolicyType, EnquiryId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(enq);
        }

        public JsonResult BindInsurersMotor()
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindInsurers("Motor");
            return Json(result);
        }
        public JsonResult BindProductsMotor(string insuranceId)
        {
            string result = string.Empty;
            result = AdminArea_AccountService.BindProducts("Motor", insuranceId);
            return Json(result);
        }

        //public JsonResult MotorEnquiryStatusUpdator(string Enquiry_ID)
        //{

        //    string result = string.Empty;

        //    string BlockID = ""; string EnquiryID = "";
        //    Account.MemberLogin lu = new Account.MemberLogin();
        //    if (!AdminArea_AccountService.IsBackUserLogin(ref lu))
        //    {
        //        BlockID = "";
        //    }
        //    else
        //    {
        //        BlockID = lu.UserId;
        //    }

        //    EnquiryID = Enquiry_ID;
        //    try
        //    {

        //        if (AdminArea_EnquiryService.MotorEnquiryStatusUpdator(Enquiry_ID, BlockID))
        //        {
        //            result = "true";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        throw;
        //    }


        //    return Json(result);           
        //}

        public JsonResult MoterBlockEnquiry(string Enquiry_ID)
        {
            string result = string.Empty;

            string BlockID = ""; string EnquiryID = "";
            Account.MemberLogin lu = new Account.MemberLogin();
            if (!AdminArea_AccountService.IsBackUserLogin(ref lu))
            {
                BlockID = "";
            }
            else
            {
                BlockID = lu.UserId;
            }

            EnquiryID = Enquiry_ID;
            try
            {

                if (AdminArea_EnquiryService.MoterBlockEnquiry(Enquiry_ID, BlockID))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return Json(result);
        }
        //public JsonResult MotorEnquiryFileration(string searchStr, string FromDate, string ToDate)
        //{
        //    string whereCondition = "";
        //    if (!string.IsNullOrEmpty(FromDate))
        //    {
        //        whereCondition = "CreatedDate >= '" + FromDate + "'";
        //    }

        //    if (!string.IsNullOrEmpty(ToDate))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate))
        //        {
        //            whereCondition += " and";
        //        }
        //        whereCondition += " CreatedDate <= '" + Convert.ToDateTime(ToDate).AddDays(1).ToString("yyyy-MM-dd") + "'";
        //    }

        //    if (!string.IsNullOrEmpty(searchStr))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate) || !string.IsNullOrEmpty(ToDate))
        //        {
        //            whereCondition += " and";
        //        }

        //        whereCondition += " ((Enquiry_Id LIKE '%" + searchStr + "%') OR (AgencyName LIKE '%" + searchStr + "%') OR (BrandName LIKE '%" + searchStr + "%') OR (RtoName LIKE '%" + searchStr + "%') OR (MobileNo LIKE '%" + searchStr + "%') OR (PinCode LIKE '%" + searchStr + "%'))";
        //    }

        //    var dt = ConnectToDataBase.GetRecordFromTable("*", "T_NewEnquiry_Motor", whereCondition, "motor");

        //    int[] Ids = new int[dt.Rows.Count];
        //    int i = 0;
        //    foreach (var item in dt.Rows)
        //    {
        //        int currID = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
        //        Ids[i] = currID;
        //        i++;
        //    }

        //    return Json(Ids);
        //}

        //public JsonResult MotorPolicyListFileration(string searchStr, string FromDate, string ToDate)
        //{
        //    string whereCondition = "";
        //    if (!string.IsNullOrEmpty(FromDate))
        //    {
        //        whereCondition = "a.CreatedDate >= '" + FromDate + "'";
        //    }

        //    if (!string.IsNullOrEmpty(ToDate))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate))
        //        {
        //            whereCondition += " and";
        //        }
        //        whereCondition += " a.CreatedDate <= '" + Convert.ToDateTime(ToDate).AddDays(1).ToString("yyyy-MM-dd") + "'";
        //    }

        //    if (!string.IsNullOrEmpty(searchStr))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate) || !string.IsNullOrEmpty(ToDate))
        //        {
        //            whereCondition += " and";
        //        }

        //        whereCondition += " ((a.enquiry_id LIKE '%" + searchStr + "%') OR (a.AgencyName LIKE '%" + searchStr + "%') OR (b.proposalnumber LIKE '%" + searchStr + "%') OR (c.firstname LIKE '%" + searchStr + "%') OR (a.vehicletype LIKE '%" + searchStr + "%') and b.transactionnumber is  null and b.proposalnumber is not Null and b.paymentstatus is null )";
        //    }

        //    var dt = ConnectToDataBase.GetRecordFromTable("a.Id,a.AgencyName, a.CreatedDate,a.BlockID,a.enquiry_id,a.ModelName,a.VarientName,a.vehicletype,b.proposalnumber,b.chainid as ChainId,b.suminsured as PremimumAmount,c.firstname as FirstName, c.lastname as lastName ,b.policynumber,b.policypdflink", "T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id  inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id", whereCondition, "motor");

        //    int[] Ids = new int[dt.Rows.Count];
        //    int i = 0;
        //    foreach (var item in dt.Rows)
        //    {
        //        int currID = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
        //        Ids[i] = currID;
        //        i++;
        //    }

        //    return Json(Ids);
        //}

        //public JsonResult MotorPolicyListFilerationpolicy(string searchStr, string FromDate, string ToDate)
        //{
        //    string whereCondition = "";
        //    if (!string.IsNullOrEmpty(FromDate))
        //    {
        //        whereCondition = "a.CreatedDate >= '" + FromDate + "'";
        //    }

        //    if (!string.IsNullOrEmpty(ToDate))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate))
        //        {
        //            whereCondition += " and";
        //        }
        //        whereCondition += " a.CreatedDate <= '" + Convert.ToDateTime(ToDate).AddDays(1).ToString("yyyy-MM-dd") + "'";
        //    }

        //    if (!string.IsNullOrEmpty(searchStr))
        //    {
        //        if (!string.IsNullOrEmpty(FromDate) || !string.IsNullOrEmpty(ToDate))
        //        {
        //            whereCondition += " and";
        //        }

        //        whereCondition += " ((a.enquiry_id LIKE '%" + searchStr + "%') OR (a.AgencyName LIKE '%" + searchStr + "%') OR (b.proposalnumber LIKE '%" + searchStr + "%') OR (c.firstname LIKE '%" + searchStr + "%') OR (a.vehicletype LIKE '%" + searchStr + "%') and b.transactionnumber is not null and b.policynumber is not Null and b.paymentstatus='success')";
        //    }

        //    var dt = ConnectToDataBase.GetRecordFromTable("a.Id,a.AgencyName, a.CreatedDate,a.BlockID,a.enquiry_id,a.ModelName,a.VarientName,a.vehicletype,b.proposalnumber,b.chainid as ChainId,b.suminsured as PremimumAmount,c.firstname as FirstName, c.lastname as lastName ,b.policynumber,b.policypdflink", "T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id  inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id", whereCondition, "motor");

        //    int[] Ids = new int[dt.Rows.Count];
        //    int i = 0;
        //    foreach (var item in dt.Rows)
        //    {
        //        int currID = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
        //        Ids[i] = currID;
        //        i++;
        //    }

        //    return Json(Ids);
        //}

        public ActionResult MoterBlockedEnquiryList(string status)
        {
            EnquiryDetails_Motors enq = new EnquiryDetails_Motors();
            string userid = "";
            try
            {

                Account.MemberLogin lu = new Account.MemberLogin();
                if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                {
                    userid = lu.UserId;
                }
                if (string.IsNullOrEmpty(status))
                {
                    status = "Block";
                }
                enq.EnquiryDetailList = AdminArea_EnquiryService.GetMotorBlockedEnquiryList(userid, status);
            }
            catch (Exception)
            {

                throw;
            }

            return View(enq);
        }

        public JsonResult AcceptRejectBlockedEnquiryMotor(string Enquiry_ID, string status)
        {
            string result = string.Empty;

            string BlockID = ""; string EnquiryID = "";
            Account.MemberLogin lu = new Account.MemberLogin();
            if (!AdminArea_AccountService.IsBackUserLogin(ref lu))
            {
                BlockID = "";
            }
            else
            {
                BlockID = lu.UserId;
            }

            EnquiryID = Enquiry_ID;
            try
            {

                if (AdminArea_EnquiryService.ActiveBlockEnquiryMotor(Enquiry_ID, BlockID, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }


            return Json(result);
        }

        public JsonResult BlockedEnquiryDetailsMotor(string Enquiry_ID)
        {

            var result = AdminArea_EnquiryService.TrackDetails(Enquiry_ID);


            return Json(result);
        }

        public ActionResult MotorEnquiryTrackDetails(string Enquiry_ID)
        {
            //TrackEnquirydetail enq = new TrackEnquirydetail();
            //string userid = "";
            //try
            //{

            //    Account.MemberLogin lu = new Account.MemberLogin();
            //    if (AdminArea_AccountService.IsBackUserLogin(ref lu))
            //    {
            //        userid = lu.UserId;
            //    }

            //    enq.TrackEnquirydetailList = AdminArea_EnquiryService.TrackDetailsMoter(Enquiry_ID);
            //}
            //catch (Exception)
            //{

            //    throw;
            //}

            //return View(enq);
            return View();
        }
        public JsonResult BindMotorTrackDetails(string enquiry_id)
        {
            string result = string.Empty;

            MotorModel enqDetail = Motor_Service.GetEnquiryDetails(enquiry_id);
            CreateAgency agencyDetail = FrontAgencyService.GetAgencyList(enqDetail.AgencyId, "1").FirstOrDefault();
            StaffModel staffDetail = FrontAgencyService.RefByList(null, "1", agencyDetail.ReferenceBy.ToString()).FirstOrDefault();
            MotorProposal propDetail = Motor_Service.GetMotorProposal(enquiry_id);
            List<AddUpdateSelectedAddon> addonList = Motor_Service.GetAddonList(enquiry_id);

            #region[Extra needed Data]
            string fileds = " (select ProductName from tblMaster_PreviousPolicyType where ProductID = a.originalpreviouspolicytype) as PreviousPolicyType, ";
            fileds = fileds + " (select Image from T_MotorCredential where id = b.chainid ) as logourl,(select Supplier from T_MotorCredential where id = b.chainid ) as CompanyName, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.ProductName') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as ProductName, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.vehicle.vehicleIDV.idv') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as IDV, ";
            fileds = fileds + " ( ";
            fileds = fileds + " select discountList.DiscountPercent ";
            fileds = fileds + " from  ";
            fileds = fileds + " T_Motor_Enquiry_Response CROSS APPLY OPENJSON(proposalresponse,'$.discounts.otherDiscounts') ";
            fileds = fileds + " with ( ";
            fileds = fileds + " DiscountType NVARCHAR(50) '$.discountType', DiscountPercent NVARCHAR(50) '$.discountPercent',DiscountAmount NVARCHAR(50) '$.discountAmount' ";
            fileds = fileds + " ) as discountList ";
            fileds = fileds + " where DiscountType = 'NCB_DISCOUNT' and enquiry_id = a.enquiry_id ) as NCB, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.ProductName') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as ProductName, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.ProductDetails.netPremium') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as Basic, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.ProductDetails.totalTax') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as GST, ";
            fileds = fileds + " (select JSON_VALUE(proposalresponse, '$.ProductDetails.grossPremium') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and enquiry_id = a.enquiry_id) as TotalPremium, ";
            fileds = fileds + " b.proposalnumber as ProposalNumber,b.policynumber as PolicyNumber,b.createddate as PaymentDate,b.paymentstatus as PaymentStatus,b.transactionnumber as TransactionNumber, ";
            fileds = fileds + " (select JSON_VALUE(paymentcreationresponse, '$.Paymenturl') from T_Motor_Enquiry_Response where ISJSON(paymentcreationresponse) = 1 and enquiry_id = a.enquiry_id) as PaymentUrl ";

            string tablename = "T_NewEnquiry_Motor a inner join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id";
            string whereCondition = "a.enquiry_id = '" + enquiry_id + "'";

            DataTable productDetailTable = ConnectToDataBase.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
            #endregion

            StringBuilder str = new StringBuilder();

            str.Append("<tr><th class='text-center bg fontsize' style='border: 3px solid black;' colspan='12'><span><b>OrderID / Insurance Details Page</b></span></th></tr>");
            str.Append("<tr>");
            str.Append("<th colspan = '4' class='text-center' width='400'> <b></b></th>");
            str.Append("<th colspan = '4' class='text-center' width='400'> <b>Agency Information</b></th>");
            str.Append("<th colspan = '4' class='text-center' width='400'> <b>Invoice Information</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");

            str.Append("<td colspan = '4' width='400'>");
            str.Append("<div><img src='http://seeinsured.com/" + productDetailTable.Rows[0]["logourl"].ToString() + "' alt='" + productDetailTable.Rows[0]["CompanyName"].ToString() + "' width='200' height='auto'></div>");
            str.Append("</td>");

            str.Append("<td colspan = '4' width='400'>");
            str.Append("<p>Agency : <span>" + enqDetail.AgencyName + "</span></p>");
            str.Append("<p>Contact No : <span>" + agencyDetail.BusinessPhone + "</span></p>");
            str.Append("<p>Email : <span>" + agencyDetail.BusinessEmailID + "</span></p>");
            str.Append("<p>Address: <span>" + agencyDetail.FullAddress + "</span></p>");
            str.Append("</td>");

            str.Append("<td colspan = '4' width='400'>");
            str.Append("<p>&nbsp;</p>");
            str.Append("<p>Order ID : <span>" + enquiry_id.ToUpper() + "</span></p>");
            str.Append("<p>Proposal No : <span>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["ProposalNumber"].ToString()) ? "---" : productDetailTable.Rows[0]["ProposalNumber"].ToString()) + "</span></p>");
            str.Append("<p>Policy No : <span>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PolicyNumber"].ToString()) ? "---" : productDetailTable.Rows[0]["PolicyNumber"].ToString()) + "</span></p>");
            str.Append("<p>Payment Date : <span>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PolicyNumber"].ToString()) ? "---" : productDetailTable.Rows[0]["PaymentDate"].ToString()) + "</span></p>");
            str.Append("</td>");

            str.Append("</tr>");

            str.Append("<tr><th colspan = '12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Agency Owner / Manager Information</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='4'><b>Owner/Manager&nbsp;Name</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>Email&nbsp;ID</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>Mobile</b></th>");
            str.Append("</tr>");

            str.Append("<tr style = 'text-align:center;'>");
            str.Append("<td class='text-center' colspan='4'>" + agencyDetail.Title.ToUpper() + " " + agencyDetail.FirstName.ToUpper() + " " + agencyDetail.LastName.ToUpper() + "</td>");
            str.Append("<td class='text-center' colspan='4'>" + agencyDetail.EmailID + "</td>");
            str.Append("<td class='text-center' colspan='4'>" + agencyDetail.Mobile + "</td>");
            str.Append("</tr>");


            str.Append("<tr><th colspan = '12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Agency Sales Person Informed</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th colspan = '4' class='text-center'><b>Name</b></th>");
            str.Append("<th colspan = '4' class='text-center'><b>Email&nbsp;ID</b></th>");
            str.Append("<th colspan = '4' class='text-center'><b>Mobile</b></th>");
            str.Append("</tr>");

            str.Append("<tr style = 'text-align:center;' >");
            str.Append("<td colspan='4' class='text-center'>" + staffDetail.StaffName.ToUpper() + "</td>");
            str.Append("<td colspan = '4' class='text-center'>" + staffDetail.EmailID + "</td>");
            str.Append("<td colspan = '4' class='text-center'>" + staffDetail.Mobile + "</td>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Enquiry Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Vehicle Type</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>Vehicle Registration</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>Mobile No.</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Pincode</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Insurance Type</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.VechileType + "</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>" + enqDetail.VechileRegNo + "</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>" + enqDetail.MobileNo + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.PinCode + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.insurancetype + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Vehicle Info</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='1'><b>RTO</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>RTO Name</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Reg Date</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Manf Date</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Brand</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Model</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Fuel</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>Varient</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='1'><b>" + enqDetail.RtoId + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.RtoName + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + enqDetail.DateOfReg + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + enqDetail.DateOfMfg + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + enqDetail.BrandName + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.ModelName + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + enqDetail.Fuel + "</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>" + enqDetail.VarientName + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Previous Policy Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Prev Policy Type</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Insured Before</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Prev Policy No.</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Exp Date</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Last Claim</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Prev NCB</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["PreviousPolicyType"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.ispreviousinsurer.ToUpper() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.policynumber + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.policyexpirydate + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.isclaiminlastyear + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + enqDetail.previousyearnoclaim + "%</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Product Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Insurer</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Product Name</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>IDV</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>NCB</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Basic</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>GST(18%)</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Premium</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["CompanyName"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["ProductName"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["IDV"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["NCB"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + productDetailTable.Rows[0]["Basic"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + productDetailTable.Rows[0]["GST"].ToString() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + productDetailTable.Rows[0]["TotalPremium"].ToString() + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>ADDONS Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='3'><b>Addon Type</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>Addon Name</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Min Amountt</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Max Amount</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Insured Amount</b></th>");
            str.Append("</tr>");

            if (addonList.Count > 0)
            {
                foreach (var item in addonList)
                {
                    str.Append("<tr>");
                    str.Append("<th class='text-center' colspan='3'><b>" + item.AddonName + "</b></th>");
                    str.Append("<th class='text-center' colspan='3'><b>" + item.InnerName + "</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>" + item.MinAmt + "</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>" + item.MaxAmt + "</b></th>");
                    str.Append("<th class='text-center' colspan='2'><b>" + item.InsuredAmt + "</b></th>");
                    str.Append("</tr>");
                }
            }
            else
            {
                str.Append("<tr>");
                str.Append("<th class='text-center' colspan='3'><b>---</b></th>");
                str.Append("<th class='text-center' colspan='3'><b>---</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>---</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>---</b></th>");
                str.Append("<th class='text-center' colspan='2'><b>---</b></th>");
                str.Append("</tr>");
            }


            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Owner Details</b></th></tr>"); //proposal
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Owner Name</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Contact No</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>D.O.B</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Marital</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Email ID</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>Address</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>Pincode</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.title.ToUpper() + " " + propDetail.firstname.ToUpper() + " " + propDetail.lastname.ToUpper() + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.mobileno + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + propDetail.dob + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>---</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.emailid + "</b></th>");
            str.Append("<th class='text-center' colspan='3'><b>" + propDetail.address1 + " " + propDetail.address2 + " " + propDetail.cityname + " " + propDetail.statename + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + propDetail.pincode + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Nominee Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='4'><b>Relation</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>Name</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>D.O.B</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='4'><b>" + propDetail.nrelation + "</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>" + propDetail.ntitle.ToUpper() + " " + propDetail.nfirstname.ToUpper() + " " + propDetail.nlastname.ToUpper() + "</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>" + propDetail.ndob + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Vehicle Details</b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Engine Number</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Chasis Number</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Voluntary Deductible</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Previous Insured</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Policy Holder Type</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>GST No</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>PAN No</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.engineno + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.chasisno + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.valuntarydeductible + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.previousinsured + "</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + propDetail.policyholdertype + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + propDetail.gstno + "</b></th>");
            str.Append("<th class='text-center' colspan='1'><b>" + propDetail.panno + "</b></th>");
            str.Append("</tr>");

            str.Append("<tr><th colspan ='12' style='border: 3px solid black;' class='text-center bg fontsize'><b>Payment Details </b></th></tr>");
            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>Payment Date</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Premium Paid</b></th>");
            str.Append("<th class='text-center' colspan='4'><b>Transaction Reference</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Payment Status</b></th>");
            str.Append("<th class='text-center' colspan='2'><b>Policy Status</b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PaymentStatus"].ToString()) ? "---" : productDetailTable.Rows[0]["PaymentDate"].ToString()) + "<b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PaymentStatus"].ToString()) ? "---" : productDetailTable.Rows[0]["TotalPremium"].ToString()) + "<b></th>");
            str.Append("<th class='text-center' colspan='4'><b>" + productDetailTable.Rows[0]["TransactionNumber"].ToString() + "<b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PaymentStatus"].ToString()) ? "INCOMPLETE" : productDetailTable.Rows[0]["PaymentStatus"].ToString()) + "<b></th>");
            str.Append("<th class='text-center' colspan='2'><b>" + (string.IsNullOrEmpty(productDetailTable.Rows[0]["PaymentStatus"].ToString()) ? "---" : "CREATED") + "<b></th>");
            str.Append("</tr>");

            str.Append("<tr>");
            str.Append("<th class='text-center' colspan='4'><b>Payment Url</b></th>");
            str.Append("<th class='text-center' colspan='8'><b>" + productDetailTable.Rows[0]["PaymentUrl"].ToString() + "<b></th>");
            str.Append("</tr>");


            result = str.ToString();
            return Json(result);
        }
        #endregion

    }
}