﻿using insurance.Areas.AdminArea.AdminArea_Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static insurance.Areas.AdminArea.Models.AdminArea_Custom.AgencyModel;

namespace insurance.Areas.AdminArea.Controllers
{
    public class AutoCompleteController : Controller
    {
        public ActionResult GetAgencyDetail(string agencyname)
        {
            Agency AgencyDel = AdminArea_AutoCompleteService.GetAgencyDetail(agencyname, null, "1");

            return View();
        }
    }
}