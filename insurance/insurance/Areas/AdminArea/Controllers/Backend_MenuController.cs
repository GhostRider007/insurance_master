﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using insurance.Models.Common;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_MenuController : Controller
    {
        #region [SetMenuForStaff]

        public ActionResult SetMenuForStaff(MemberLogin lu)
        {
            StaffMenuModel model = new StaffMenuModel();

            try
            {
                model.MemberType = lu.MemberType;
                model.StaffMenuList = AdminArea_MenuService.GetMenuByRoleList(lu.RoleId);
                model.SubMenuList = AdminArea_MenuService.GetSubMenuByRoleList(lu.RoleId);
                //if (lu.MemberType == "staff")
                //{                    
                //    model.SubMenuIdList = AdminArea_MenuService.StafftSubMenuIdList(lu.RoleId);
                //}
                
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        #endregion

        #region [Menu Section]
        public ActionResult MenuList(string status)
        {
            MenuModel model = new MenuModel();

            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.MenuList = AdminArea_MenuService.GetMenuList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }
        public ActionResult AddEditMenu(string menuid)
        {
            MenuModel model = new MenuModel();

            try
            {
                if (!string.IsNullOrEmpty(menuid))
                {
                    model = AdminArea_MenuService.GetMenuList(menuid, "1").FirstOrDefault();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(model);
        }
        public JsonResult ProcessInsertMenu(MenuModel menu)
        {
            List<string> result = new List<string>();

            try
            {
                if (menu.MenuId > 0)
                {
                    if (AdminArea_MenuService.UpdateMenuDetail(menu))
                    {
                        result.Add("true");
                        result.Add("update");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
                else
                {
                    if (AdminArea_MenuService.InsertMenuDetail(menu))
                    {
                        result.Add("true");
                        result.Add("insert");
                        result.Add("Record inserted successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateMenuPosition(string menuid, string posval)
        {
            string result = string.Empty;

            try
            {
                if (AdminArea_MenuService.UpdateMenuPosition(menuid, posval))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult UpdateMenuStatus(string menuid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_MenuService.UpdateMenuPosition(menuid, null, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion

        #region [Sub Menu Section]
        public ActionResult SubMenuList(string status)
        {
            SubMenuModel model = new SubMenuModel();
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (status.ToLower().Trim() == "active")
                    {
                        status = "1";
                    }
                    else if (status.ToLower().Trim() == "all")
                    {
                        status = "";
                    }
                    else
                    {
                        status = "0";
                    }
                }
                else
                {
                    status = "1";
                }

                model.SubMenuList = AdminArea_MenuService.GetSubMenuList(null, status);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return View(model);
        }
        public ActionResult AddEditSubMenu(string submenuid)
        {
            SubMenuModel model = new SubMenuModel();
            try
            {
                if (!string.IsNullOrEmpty(submenuid))
                {
                    model = AdminArea_MenuService.GetSubMenuList(submenuid, "1").FirstOrDefault();
                }

                model.MenuList = CommonClass.PopulateMenu(AdminArea_MenuService.GetMenuList(null, "1"));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }            
            return View(model);
        }
        public JsonResult ProcessInsertSubMenu(SubMenuModel submenu)
        {
            List<string> result = new List<string>();

            try
            {
                if (submenu.SubMenuId > 0)
                {
                    if (AdminArea_MenuService.UpdateSubMenuDetail(submenu))
                    {
                        result.Add("true");
                        result.Add("update");
                        result.Add("Record updated successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
                else
                {
                    if (AdminArea_MenuService.InsertSubMenuDetail(submenu))
                    {
                        result.Add("true");
                        result.Add("insert");
                        result.Add("Record inserted successfully.");
                    }
                    else
                    {
                        result.Add("false");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult UpdateSubMenuStatus(string submenuid, string status)
        {
            string result = string.Empty;

            try
            {
                if (status.ToLower().Trim() == "delete")
                {
                    status = "0";
                }
                else
                {
                    status = "1";
                }

                if (AdminArea_MenuService.UpdateSubMenuPosition(submenuid, null, status))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        public JsonResult UpdateSubMenuPosition(string submenuid, string posval)
        {
            string result = string.Empty;

            try
            {
                if (AdminArea_MenuService.UpdateSubMenuPosition(submenuid, posval))
                {
                    result = "true";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return Json(result);
        }
        #endregion
    }
}