﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static insurance.Areas.AdminArea.Models.AdminArea_Account.Account;
using insurance.Areas.AdminArea.AdminArea_Service;
using static insurance.Models.Custom.FrontAgencyModel;
using insurance.Service;
using insurance.Models;
using insurance.Service.HealthService;
using insurance.HealthApi;
using insurance.Models.Common;
using static insurance.Models.Health.HealthModel;
using insurance.Service.MotorService;
using Post_Utility.Utility;
using insurance.Models.Motor;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_AdministrationController : Controller
    {
        public ActionResult ChangeAdminPassword()
        {
            MemberLogin model = new MemberLogin();
            if (!AdminArea_AccountService.IsBackUserLogin(ref model))
            {
                if (AdminArea_AccountService.LogoutBackendLogin())
                {
                    Response.Redirect("/backend/login");
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeAdminPassword(MemberLogin model)
        {
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(model.CurrentPassword) && !string.IsNullOrEmpty(model.Password))
            {
                if (AdminArea_AccountService.AdminMemberResetPassword(model, ref msg))
                {
                    TempData["Message"] = msg;
                }
                else
                {
                    ViewBag.Msg = "<div class='col-md-12 text-danger' style='margin-top:10px;'>" + msg + "</div>";
                }
            }
            return View(model);
        }

        public ActionResult Admin_ChangeAgencyPassword()
        {
            CreateAgency model = new CreateAgency();
            return View(model);
        }

        [HttpPost]
        public ActionResult Admin_ChangeAgencyPassword(CreateAgency model)
        {
            string msg = string.Empty;
            if (!string.IsNullOrEmpty(model.Password) && !string.IsNullOrEmpty(model.ConfPassword) && !string.IsNullOrEmpty(model.AgencyID))
            {
                if (AdminArea_AccountService.Admin_ChangeAgencyPassword(model))
                {
                    //TempData["Message"] = "Agency password changed successfully.";
                    TempData["Message"] = "Agency password changed successfully.";
                }
                else
                {
                    ViewBag.Msg = "<div class='col-md-12 text-danger' style='margin-top:10px;'>Some error occured, please try again later!</div>";
                }
            }
            else
            {
                ViewBag.Msg = "<div class='col-md-12 text-danger' style='margin-top:10px;'>Some error occured, please try again later!</div>";
            }
            return View(model);
        }
        public ActionResult AdminEmulateLogin(CreateAgency model)
        {
            if (model.IntAgencyID > 0)
            {
                model = FrontAgencyService.Get_AgencyDetail("", model.IntAgencyID.ToString()).FirstOrDefault();
            }
            return View(model);
        }
        public ActionResult Backend_EmulateLogin(EmulateLogin emodel)
        {
            CreateAgency mode = new CreateAgency();
            try
            {
                if (!string.IsNullOrEmpty(emodel.Remarks.Trim()))
                {
                    MemberLogin lu = new MemberLogin();
                    if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                    {
                        emodel.staffid = lu.LoginId.ToString();
                        emodel.StaffName = lu.FirstName + " " + lu.LastName;
                        emodel.actionType = "insert";

                        emodel = FrontAgencyService.Insert_EmulateloginDetails(emodel);
                        if (!string.IsNullOrEmpty(emodel.EmulateId))
                        {
                            string msg = string.Empty;

                            bool isLogin = AdministrationService.AgencyLogin(emodel.UserId, Security.ApiBase64Decode(emodel.Password), ref msg);
                            if (isLogin)
                            {
                                return Redirect("/agency/dashboard");
                            }
                            else
                            {
                                TempData["Message"] = msg;
                            }
                        }
                    }
                }
                else
                {
                    TempData["Message"] = "login unsuccessful";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Redirect("/profile/emulate-login");
        }
        public ActionResult AdminEmulatorLogin(EmulateLogin emodel)
        {
            if (emodel.Agencyid > 0)
            {
                CreateAgency model = FrontAgencyService.Get_AgencyDetail("", emodel.Agencyid.ToString()).FirstOrDefault();
                emodel.Agencyid = model.IntAgencyID;
                emodel.AgencyName = model.AgencyName;
                emodel.BusinessPhone = model.BusinessPhone;
                emodel.UserId = model.UserId;
                emodel.BusinessEmailID = model.BusinessEmailID;
                emodel.AgencyAddress = model.Address;
                emodel.Password = model.Password;
            }
            return View(emodel);
        }
        public JsonResult EmulateWithOTP(EmulateLogin emodel)
        {
            List<string> rutStr = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(emodel.Remarks))
                {
                    MemberLogin lu = new MemberLogin();
                    if (AdminArea_AccountService.IsBackUserLogin(ref lu))
                    {
                        emodel.staffid = lu.LoginId.ToString();
                        emodel.StaffName = lu.FirstName + " " + lu.LastName;
                        emodel.actionType = "insert";
                        emodel.OTP = UtilityClass.GenrateRandomTransactionId(string.Empty, 4, "1234567890");
                        DateTime nowDateTime = DateTime.Now.AddMinutes(5);
                        emodel.OTPExpTime = nowDateTime.ToString("yyyy-MM-dd HH:mm");

                        emodel = FrontAgencyService.Insert_EmulateloginDetails(emodel);
                        if (!string.IsNullOrEmpty(emodel.EmulateId))
                        {
                            string mobmessage = "Your one time password (OTP) is " + emodel.OTP + " for  Agency ID login  : " + emodel.Agencyid + ", valid for next 5 minutes";

                            if (SmsUtility.Send_SeeinsuredSMS(emodel.BusinessPhone, mobmessage))
                            {
                                rutStr.Add("success");
                                rutStr.Add(emodel.EmulateId);
                            }
                            else
                            {
                                rutStr.Add("failed");
                            }
                        }
                    }
                }
                else
                {
                    TempData["Message"] = "login unsuccessful";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(rutStr);
        }
        public JsonResult ResendEmulateLoginWithOTP(EmulateLogin emodel)
        {
            try
            {
                if (!string.IsNullOrEmpty(emodel.EmulateId.Trim()))
                {
                    string newOTP = UtilityClass.GenrateRandomTransactionId(string.Empty, 4, "1234567890");
                    DateTime nowDateTime = DateTime.Now.AddMinutes(15);
                    string newOTPExpTime = nowDateTime.ToString("yyyy-MM-dd HH:mm");
                    emodel.actionType = "update";
                    emodel = FrontAgencyService.Insert_EmulateloginDetails(emodel);
                    if (!string.IsNullOrEmpty(emodel.EmulateId))
                    {
                        string mobmessage = "Your one time password (OTP) is " + emodel.OTP + " for  Agency ID login  : " + emodel.Agencyid + ", valid for next 5 minutes";

                        if (SmsUtility.Send_SeeinsuredSMS(emodel.BusinessPhone, mobmessage))
                        {
                            return Json(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(false);
        }
        public JsonResult EmulateLoginWithOTP(EmulateLogin emodel)
        {
            List<string> retVal = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(emodel.TempOTP))
                {
                    if (!string.IsNullOrEmpty(emodel.EmulateId.Trim()))
                    {
                        emodel = FrontAgencyService.Get_EmulateDetail(emodel);

                        DateTime expTime = Convert.ToDateTime(emodel.OTPExpTime);
                        if (DateTime.Now < expTime)
                        {
                            if (emodel.TempOTP == emodel.OTP)
                            {
                                string msg = string.Empty;
                                bool isLogin = AdministrationService.AgencyLogin(emodel.UserId, Security.ApiBase64Decode(emodel.Password), ref msg);
                                if (isLogin)
                                {
                                    retVal.Add("success");
                                    retVal.Add("/agency/dashboard");
                                }
                                else
                                {
                                    TempData["Message"] = msg;
                                }
                            }
                            else
                            {
                                retVal.Add("wrongotp");
                                retVal.Add("You have entered wrong otp.");
                            }
                        }
                        else
                        {
                            retVal.Add("expired");
                            retVal.Add("Otp has been expired, Please resend otp.");
                        }
                    }
                }
                else
                {
                    retVal.Add("wrongotp");
                    retVal.Add("You have entered wrong otp.");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(retVal);
        }

        #region[EXTRANET]

        #region[HEALTH EXTRANET]
        public ActionResult AdminExtranetBooking(CreateAgency model)
        {
            if (model.IntAgencyID > 0)
            {
                model = FrontAgencyService.Get_AgencyDetail("", model.IntAgencyID.ToString()).FirstOrDefault();
            }
            return View(model);
        }  //  /profile/health/extranet-booking
        public ActionResult AdminExtranetCreateProposal(string agencyId)
        {
            //CreateAgency model = FrontAgencyService.Get_AgencyDetail("", agencyId.ToString()).FirstOrDefault();
            //return View(model);
            return View();
        }

        public JsonResult ExtranetGenrateEnquiryId()
        {
            string result = "&enquiry_id=" + ("SIH" + Health_Service.GenrateEnquiryId());
            return Json(result.ToLower());
        }
        public JsonResult ExtranetGetCityListFromStarHealth(string pincode)
        {
            string cityList = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(pincode))
                {
                    string userid = Config.AdminAPIUserId;
                    string password = Config.AdminAPIPassword;
                    string agencyId = Config.AdminAPIAgencyId;
                    cityList = HealthAPIHelper.GetCityListFromStarHealth(userid, password, agencyId, pincode);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(cityList);
        }

        public JsonResult ExtranetGetAreaListFromStarHealth(string pincode, string cityid)
        {
            string areaList = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(pincode))
                {
                    string userid = Config.AdminAPIUserId;
                    string password = Config.AdminAPIPassword;
                    string agencyId = Config.AdminAPIAgencyId;
                    areaList = HealthAPIHelper.GetAreaListFromStarHealth(userid, password, agencyId, pincode, cityid);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(areaList);
        }

        public JsonResult ExtranetGetRelation(string compid, string prodId, string policyType)
        {
            string result = "<option value=''>Relation</option>";
            List<insurance.Models.Health.HealthModel.RelationMaster> relationList = Health_Service.GetRelation(compid, prodId, policyType);
            if (relationList.Count > 0)
            {
                foreach (var item in relationList)
                {
                    result = result + "<option value='" + item.RelationshipId + "' data-title='" + item.Title + "'>" + item.RelationshipName + "</option>";
                }
            }
            return Json(result);
        }
        public JsonResult ExtranetGetOccupation(string compid)
        {
            string result = "<option value=''>Select Occupation</option>";
            result = result + Health_Service.GetOccupationDdl(compid, string.Empty);
            return Json(result);
        }

        public JsonResult ExtranetSaveDetails(NewEnquiry_Health enquiryDetails, CreateHealth_Proposal proposal)
        {
            string result = string.Empty;
            try
            {
                enquiryDetails.AgencyId = "";
                enquiryDetails.AgencyName = "";
                bool IsEnquirySaved = Health_Service.SaveGenratedQuotes(enquiryDetails);
                bool IsProposal_and_NomineeSaved = Health_Service.ExtranetInsertProposalDetails(proposal);
                bool IsInsuredSaved = Health_Service.InsertInsuredDetails(proposal);

                result = "success";
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result);
        }



        #endregion

        #region[MOTOR EXTRANET]
        public ActionResult AdminExtranetBooking_Motor(CreateAgency model)
        {
            if (model.IntAgencyID > 0)
            {
                model = FrontAgencyService.Get_AgencyDetail("", model.IntAgencyID.ToString()).FirstOrDefault();
            }
            return View(model);
        }  //  /profile/motor/extranet-booking
        public ActionResult AdminExtranetCreateProposal_Motor(string agencyId)
        {
            //CreateAgency model = FrontAgencyService.Get_AgencyDetail("", agencyId.ToString()).FirstOrDefault();
            //return View(model);
            return View();
        }   //      /profile/motor/extranet-create-proposal?agencyId=


        //public ActionResult AdminMotorExtranetCreateProposal(string agencyId)
        //{
        //    return View();
        //}


        public JsonResult ExtranetGenrateMotorEnquiryId()
        {
            string result = "&enquiry_id=" + ("SIM" + Motor_Service.GenrateMotorEnquiryId());
            return Json(result.ToLower());
        }
        public JsonResult GetExtranetBrandDetails_Motor(string vehicleType)
        {
            string result = "<option value=''>Select Brands</option>";

            try
            {
                bool isBike = false;
                if (vehicleType == "bike")
                {
                    isBike = true;
                }
                List<string> brandList = Motor_Service.GetBrandList(isBike);
                if (brandList != null && brandList.Count > 0)
                {
                    foreach (var item in brandList)
                    {
                        result = result + "<option value='" + item.Trim() + "'>" + item.Trim() + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetExtranetModelDetails_Motor(string brand, string vehicleType)
        {
            string result = "<option value=''>Select Models</option>";

            try
            {
                bool isBike = false;
                if (vehicleType == "bike")
                {
                    isBike = true;
                }

                List<string> modelList = Motor_Service.GetModelList(brand, null, isBike);
                if (modelList != null && modelList.Count > 0)
                {
                    foreach (var item in modelList)
                    {
                        result = result + " <option value = '" + item.Trim() + "'> " + item.Trim() + " </option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetExtranetFuelDetails_Motor(string brand, string model, string vehicleType)
        {
            string result = "<option value=''>Select Fuel</option>";

            try
            {
                bool isBike = false;
                if (vehicleType == "bike")
                {
                    isBike = true;
                }

                List<string> fuelList = Motor_Service.GetFuelList(brand, model, null, isBike);
                if (fuelList != null && fuelList.Count > 0)
                {
                    foreach (var item in fuelList)
                    {
                        result = result + " <option value = '" + item.Trim() + "'> " + item.Trim() + " </option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetExtranetVarientDetails_Motor(string brand, string model, string fuel, string vehicleType)
        {
            string result = "<option value=''>Varients</option>";

            try
            {
                bool isBike = false;
                if (vehicleType == "bike")
                {
                    isBike = true;
                }

                List<string> varientList = Motor_Service.GetVarientList(brand, model, fuel, null, isBike);
                if (varientList != null && varientList.Count > 0)
                {
                    foreach (var item in varientList)
                    {
                        result = result + " <option value = '" + item.Trim() + "'> " + item.Trim() + " </option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult GetExtranetNCBPercentage_Motor()
        {
            string result = "<option value=''>NCB</option>";
            var ncbList = CommonClass.PopulateGetNCBPercentage(Motor_Service.GetNCBPercentage());
            if (ncbList != null && ncbList.Count > 0)
            {
                foreach (var item in ncbList)
                {
                    result = result + " <option value = '" + item.Value + "'> " + item.Value + "% </option>";
                }
            }
            return Json(result);
        }
        public JsonResult GetExtranetRTOList_Motor()
        {
            string result = "<option value=''>Select RTO</option>";
            var RTOList = CommonClass.PopulateRTO(Motor_Service.GetRTODetails());
            if (RTOList != null && RTOList.Count > 0)
            {
                foreach (var item in RTOList)
                {
                    result = result + " <option value = '" + item.Value + "'> " + item.Text + "</option>";
                }
            }
            return Json(result);

        }
        public JsonResult ExtranetBindPreviousPolicyType_Motor(string insurancetype, string vehicleType)
        {
            string ptype = "PC";
            if (vehicleType == "bike")
            {
                ptype = "2W";
            }
            string result = Motor_Service.GetPreviousPolicyType(insurancetype, ptype);
            result = result.Replace("<option value=''>SELECT PREVIOUS POLICY TYPE</option>", "<option value=''>Select</option>");
            return Json(result);
        }
        public JsonResult ExtranetBindInsurers_Motor()
        {
            string result = AdminArea_AccountService.BindInsurers("Motor");
            result = "<option value=''>Select</option>" + result;
            return Json(result);
        }
        public JsonResult ExtranetBindProducts_Motor(string insuranceId)
        {
            string result = AdminArea_AccountService.BindProducts("Motor", insuranceId);
            return Json(result);
        }
        public JsonResult ExtranetBindRelation_Motor(string chainid)
        {
            string result = "<option value=''>Relation</option>";
            List<string> list = Motor_Service.MotorNomineeRelation(chainid);
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    result = result + " <option value = '" + item + "'> " + item + " </option>";
                }
            }
            return Json(result);
        }
        public JsonResult ExtranetBindVolunatryDeductible_Motor(string chainid, string vehicleType)
        {
            string result = Motor_Service.GetVolunatryDeductible(chainid, vehicleType);
            return Json(result);
        }
        public JsonResult ExtranetBindPreviousInsured_Motor(string chainid)
        {
            string result = Motor_Service.GetPreviousInsured(chainid);
            return Json(result);
        }
        public JsonResult ExtranetSaveDetails_Motor(MotorModel enquirydetails, MotorProposal proposal)
        {
            string result = string.Empty;
            try
            {
                MotorModel m1 = new MotorModel();
                MotorProposal m2 = new MotorProposal();

                //m1 = Newtonsoft.Json.JsonConvert.DeserializeObject<MotorModel>(enquirydetails);
                //m2 = Newtonsoft.Json.JsonConvert.DeserializeObject<MotorProposal>(proposal);

                //enquirydetails.AgencyId = "N/A";
                //enquirydetails.AgencyName = "N/A";
                bool IsEnquirySaved = false ;  
                //IsEnquirySaved = Motor_Service.ExtranetSaveDetails_Motor(enquirydetails, proposal);
                //bool IsInsuredSaved = Health_Service.InsertInsuredDetails(proposal);
                if (IsEnquirySaved)
                {
                    result = "success";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        public JsonResult AutoComplete_AgencyDetail(string agencyname)
        {
            var agencyList = FrontAgencyService.Get_AgencyDetail(agencyname);
            return Json(agencyList);
        }
    }
}