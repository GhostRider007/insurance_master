﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Helper;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_DashboardController : Controller
    {
        public ActionResult BackEndDashboard()
        {

            EnquiryDetails enq = new EnquiryDetails();

            try
            {
                enq.EnquiryDetailList = AdminArea_EnquiryService.GetEnquiryDetails();
            }
            catch (Exception)
            {

                throw;
            }


            return View(enq);
        }

        public JsonResult policyGraph(string duration)
        {
            string durationValue = "-"+duration.Substring(0, 2);
            string period = duration.Substring(3);
            int[] result = AdminArea_EnquiryHelper.GetPolicyCount(period, durationValue);
            return Json(result);
        }
        public JsonResult policyGraphHealth(string duration)
        {
            string durationValue = "-" + duration.Substring(0, 2);
            string period = duration.Substring(3);

            int[] result = AdminArea_EnquiryHelper.GetPolicyCountHealth(period, durationValue);
            return Json(result);
        }
        public JsonResult GetTotalPremium(string duration)
        {
            string durationValue = "-" + duration.Substring(0, 2);
            string period = duration.Substring(3);

            decimal[] result = AdminArea_EnquiryHelper.GetTotalPremium(period, durationValue);
            return Json(result);
        }
    }
}