﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Motor;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_MotorController : Controller
    {
        // GET: AdminArea/Backend_Motor
        public ActionResult NCBList()
        {
            List<NCBPercentage> modelList = new List<NCBPercentage>();
            modelList = AdminArea_MotorService.GetNCBList();
            return View(modelList);
        }
    }
}