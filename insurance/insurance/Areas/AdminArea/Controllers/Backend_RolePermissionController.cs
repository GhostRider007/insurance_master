﻿using insurance.Areas.AdminArea.AdminArea_Service;
using insurance.Areas.AdminArea.Models.AdminArea_Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace insurance.Areas.AdminArea.Controllers
{
    public class Backend_RolePermissionController : Controller
    {
        public ActionResult RolePermissionList()
        {
            RoleModel model = new RoleModel();

            try
            {
                model.RoleList = AdminArea_RoleService.GetRoleList(null, "1");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        public ActionResult AssignRolePermission(string roleid)
        {
            RolePermission model = new RolePermission();

            try
            {
                model.MenuList = AdminArea_MenuService.GetMenuList(null, "1");
                model.SubMenuList = AdminArea_MenuService.GetSubMenuList(null, "1");
                model.RoleName = AdminArea_RoleService.GetRoleList(roleid, "1").FirstOrDefault().RoleName;
                model.RolePermissionList = AdminArea_RoleService.GetRolePermissionList(roleid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(model);
        }

        public JsonResult InserRolePermissionList(SetRolePermission[] modelList)
        {
            string issuccess = string.Empty;

            try
            {
                if (modelList != null && modelList.Count() > 0)
                {
                    int count = 0; int roleid = 0;
                    foreach (var item in modelList)
                    {
                        if (roleid != item.RoleID)
                        {
                            count = 0;
                        }

                        if (count == 0)
                        {
                            roleid = item.RoleID;
                            count = 1;
                            bool isDeleted = AdminArea_RoleService.DeleteAssignedRolePermission(item.RoleID.ToString());
                        }

                        bool isInserted = AdminArea_RoleService.InsertAssignedRolePermission(item);
                        if (isInserted)
                        {
                            issuccess = "true";
                        }
                        else
                        {
                            issuccess = "false";
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(issuccess);
        }
    }
}