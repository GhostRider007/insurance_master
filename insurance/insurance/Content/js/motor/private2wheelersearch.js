﻿GetBikeGetQuotes = () => {
    if (CheckFocusBlankValidation("txtBikeVechileReg")) return !1;
    if (CheckFocusBlankValidation("txtBikeMobileNo")) return !1;
    if (CheckFocusBlankValidation("txtBikePinCode")) return !1;

    if (!($("#chkBikeHearBy").prop("checked"))) { alert("You must agree terms and condition."); return false; }

    let vechtype = "Bike";
    if ($(".motericon").hasClass("caractive")) { vechtype = "Car"; } else { vechtype = "Bike"; }
    let vechReg = $("#txtBikeVechileReg").val();
    let mobile = $("#txtBikeMobileNo").val();
    let pincode = $("#txtBikePinCode").val();
    let insurancetype = $("#ddlInsuranceType_2Wheeler option:selected").val();

    $("#btnBikeGetQuotes").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        url: "/Moter/SaveMotorGenratedQuotes",
        data: '{vehreg: ' + JSON.stringify(vechReg) + ',mobile: ' + JSON.stringify(mobile) + ',enqid:' + JSON.stringify(GetParameterValues('enquiry_id')) + ',pincode:' + JSON.stringify(pincode) + ',vechtype:' + JSON.stringify(vechtype) + ',insurancetype:' + JSON.stringify(insurancetype) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data !== "") {
                window.location.href = data;
            }
            else {
                $("#btnBikeGetQuotes").html("GET QUOTES");
            }
        }
    });
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$("#reg").click(function () {
    $(".regs").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$("#brandtab").click(function () {
    $(".selctcar").removeClass("hidden");
    $(".regs").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
});
$("#models").click(function () {
    $(".selectmodel").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#ful").click(function () {
    $(".fuelss").removeClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#vari").click(function () {
    $(".Variant").removeClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".regs").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$(".Variantbtn").click(function () {
    $(".choseplan").removeClass("hidden");
    $(".registrationform").addClass("hidden");
});

$("#RTO_Code_A").select2();
$("#ddlYearOfMake").select2();

$("#btnRegContinue").click(function () {
    $('#brandtab').removeAttr("disabled");
    $("#brandtab").click();
});
$(document.body).on('click', ".brandselectsec", function (e) {
    $('#models').removeAttr("disabled");
    $("#models").click();
})
$(document.body).on('click', ".modelselectsec", function (e) {
    $('#ful').removeAttr("disabled");
    $("#ful").click();
});
$(document.body).on('click', ".fueldyn", function (e) {
    $('#vari').removeAttr("disabled");
    $("#vari").click();
});
$(document.body).on('click', ".varientselectsec", function (e) {
    $('#otherdel').removeAttr("disabled");
    $("#otherdel").click();
});

function SaveMotorOtherDetails() {
    window.location.href = "/bike/bikeSearchList";
}