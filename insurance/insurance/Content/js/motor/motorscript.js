﻿$(function () {
    var enquiry_id = GetParameterValues('enquiry_id');
    if (enquiry_id == null || enquiry_id == '') { GenrateEnquiry_Id(); }
    //StarterEvent();
});

GenrateEnquiry_Id = () => {
    $.ajax({
        type: "Post",
        url: "/Moter/GenrateMotorEnquiryId",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                window.location.href = response;
            }
        }
    });
}

GetCarGetQuotes = () => {
    if (CheckFocusBlankValidation("txtVechileReg")) return !1;
    if (CheckFocusBlankValidation("txtMobileNo")) return !1;
    if (!($("#chkCarHearBy").prop("checked"))) { alert("You must agree terms and condition."); return false; }

    var vechtype = "Private_Car";
    //if ($(".carimg").hasClass("caractive")) { vechtype = "Car"; } else { vechtype = "Bike"; }
    if ($(".motericon").hasClass("caractive")) { vechtype = "Private_Car"; } else { vechtype = "Bike"; }
    var vechReg = $("#txtVechileReg").val();
    var mobile = $("#txtMobileNo").val();
    var pincode = $("#txtPinCode").val();
    var statename = $("#hdnState").val();
    var insurancetype = $("#ddlInsuranceType option:selected").val();

    if (mobile.length == 10) {
        if (!$("#txtPinCode").hasClass("errorpincode")) {
            $("#btnCarGetQuotes").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/Moter/SaveMotorGenratedQuotes",
                data: '{vehreg: ' + JSON.stringify(vechReg) + ',mobile: ' + JSON.stringify(mobile) + ',enqid:' + JSON.stringify(GetParameterValues('enquiry_id')) + ',pincode:' + JSON.stringify(pincode) + ',state:' + JSON.stringify(statename) + ',vechtype:' + JSON.stringify(vechtype) + ',insurancetype:' + JSON.stringify(insurancetype) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    if (response != "") {
                        window.location.href = response;
                    }
                    else {
                        $("#btnCarGetQuotes").html("GET QUOTES");
                    }
                }
            });
        }
    }
    else {
        if (NotExistFocusBlankValidation("txtMobileNo")) return !1;
    }
}

function CheckPincode2(element, vtype) {
    $(".notpincodevalid_" + vtype).addClass("hidden").html("");
    var txtPinCodeVal = $(element).val();

    if (vtype === 'car') {
        $("#txtBikePinCode").val("");
        $(".notpincodevalid_bike").addClass("hidden").html("");
    } else {
        $("#txtPinCode").val("");
        $(".notpincodevalid_car").addClass("hidden").html("");
    }

    if (txtPinCodeVal.length != 6) {
        $(".notpincodevalid_" + vtype).removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $(element).removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid_" + vtype).removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/CheckAndGetDetails",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 1) {
                    $("#hdnState").val(response[0]);
                    $(".notpincodevalid_" + vtype).removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response[1]);
                    $(element).addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid_" + vtype).removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $(element).removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

$(".motericon").click(function () {
    $(".carhideshow").removeClass("hidden");
    $(".motericon").addClass("caractive");
    $(".bikehideshow").addClass("hidden");
    $(".bikecon").removeClass("caractive");
    ResetFields();
});
$(".bikecon").click(function () {
    $(".bikehideshow").removeClass("hidden");
    $(".carhideshow").addClass("hidden");
    $(".motericon").removeClass("caractive");
    $(".bikecon").addClass("caractive");
    ResetFields();
});

function ResetFields() {
    $("#txtBikeVechileReg").val("");
    $("#txtBikeMobileNo").val("");
    $("#txtBikePinCode").val("");
    $("#txtVechileReg").val("");
    $("#txtMobileNo").val("");
    $("#txtPinCode").val("");
    $(".notpincodevalid_car").html("");
    $(".notpincodevalid_bike").html("");
}