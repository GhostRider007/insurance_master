﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    let enqid = GetParameterValues('enquiry_id');
    //BindAddonsDetails(enqid);
    BindVehicleDetails(enqid);
});

BindVehicleDetails = (enqid) => {
    if (enqid != "" && enqid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Moter/BindVehicleDetails",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#divVechleDetails").html(response[0]);
                $("#divPlanSummarySection").html(response[1]);
                $("#AddOnCovers").html(response[2]);
                $("#LegalLiabilitys").html(response[3]);
                $("#AccessoriesCovers").html(response[4]);
            }
        });
    }
}

BindAddonsDetails = (enqid) => {
    if (enqid != "" && enqid != undefined) {
        $("#AddonsSection").removeClass("row");
        $.ajax({
            type: "Post",
            url: "/Moter/GetAddonsDetails",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#AddonsSection").addClass("row");
                $("#AddonsSection").html(response);
            }
        });
    }
}
$(document.body).on("click", ".acccovers", function (e) {
    let heading = $(this).data("coverheading");
    let name = $(this).data("covername");
    let rangcovername = $(this).data("rangcovername");

    $("#txtamtrang_" + rangcovername).html("");
    if (this.checked) {
        $("#txtamtrang_" + rangcovername).css("display", "block");
    }
    else {
        $("#txtamtrang_" + rangcovername).css("display", "none");
    }
});
NextSelectedPlan = () => {
    let isanyselected = false;
    let addoncover = [];

    $(".addoncovers").each(function () {
        if ($(this).prop("checked") == true) {
            isanyselected = true;
            let addoncoverlist = {};
            addoncoverlist.Enquiry_Id = GetParameterValues('enquiry_id');
            addoncoverlist.AddonName = $(this).data("coverheading");
            addoncoverlist.InnerName = $(this).data("covername");
            addoncoverlist.InsuredAmt = $(this).data("amount");
            addoncoverlist.MinAmt = 0;
            addoncoverlist.MaxAmt = 0;
            addoncover.push(addoncoverlist);
        }
    });

    $(".acccovers").each(function () {
        if ($(this).prop("checked") == true) {
            isanyselected = true;
            let addoncoverlist = {};
            addoncoverlist.Enquiry_Id = GetParameterValues('enquiry_id');
            addoncoverlist.AddonName = $(this).data("coverheading");
            addoncoverlist.InnerName = $(this).data("covername");
            let rangcovername = $(this).data("rangcovername");
            let minamt = $("#txtamtrang_" + rangcovername).data("minamount");
            let maxamt = $("#txtamtrang_" + rangcovername).data("maxamount");
            addoncoverlist.MinAmt = minamt;
            addoncoverlist.MaxAmt = maxamt;
            addoncoverlist.InsuredAmt = $("#txtamtrang_" + rangcovername).val();
            addoncover.push(addoncoverlist);
        }
    });

    $(".acccovers").each(function () {
        if ($(this).prop("checked") == true) {
            let rangcovername = $(this).data("rangcovername");
            let minamt = $("#txtamtrang_" + rangcovername).data("minamount");
            if ($("#txtamtrang_" + rangcovername).val() == "") {
                $("#txtamtrang_" + rangcovername).val(minamt);
            }
        }
    });

    //if (isanyselected) {
    $("#btnNextSelectedPlan").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Moter/AddUpdateSelectedAddon",
        data: '{addon: ' + JSON.stringify(addoncover) + ',enqId: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //if (response) {
            $("#txtPPincode").val(response[1]);
            CheckAndGetDetails();
            $("#tabOwnerDel").addClass("active");
            $("#fsSelectedAddonSection").css("display", "none");
            $("#fsOwnerDetailsSection").css("display", "block");
            $("html, body").animate({ scrollTop: 0 }, "fast");
            // }
            $("#btnNextSelectedPlan").html("NEXT");
        }
    });
    //}
    //else {
    //    $("#tabOwnerDel").addClass("active");
    //    $("#fsSelectedAddonSection").css("display", "none");
    //    $("#fsOwnerDetailsSection").css("display", "block");
    //    $("html, body").animate({ scrollTop: 0 }, "fast");
    //}
}

PrevOwnerDetails = () => {
    $("#fsSelectedAddonSection").css("display", "block");
    $("#fsOwnerDetailsSection").css("display", "none");
    $("html, body").animate({ scrollTop: 0 }, "fast");
}

//NextOwnerDetails = () => {
//    $("#tabNomineeDel").addClass("active");
//    $("#fsOwnerDetailsSection").css("display", "none");
//    $("#fsNomineeDetailsSection").css("display", "block");
//    $("html, body").animate({ scrollTop: 0 }, "fast");
//}

PrevNomineeDetails = () => {
    $("#fsNomineeDetailsSection").css("display", "none");
    $("#fsOwnerDetailsSection").css("display", "block");
    $("html, body").animate({ scrollTop: 0 }, "fast");
}

//NextNomineeDetails = () => {
//    $("#tabVehicleDel").addClass("active");
//    $("#fsVehicleHistorySection").css("display", "block");
//    $("#fsNomineeDetailsSection").css("display", "none");
//    $("html, body").animate({ scrollTop: 0 }, "fast");
//}

PrevVehicleHistory = () => {
    $("#fsVehicleHistorySection").css("display", "none");
    $("#fsNomineeDetailsSection").css("display", "block");
    $("html, body").animate({ scrollTop: 0 }, "fast");
}

//Arrow function
CheckAndGetDetails = () => {
    $(".notpincodevalid").addClass("hidden").html("");
    var txtPinCodeVal = $("#txtPPincode").val();

    if (txtPinCodeVal.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
        $("#txtPState").val("");
        $("#txtPCity").val("");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/CheckAndGetDetails",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 0) {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response[1]);
                    $("#txtPState").val(response[0]);
                    $("#txtPCity").val(response[1]);
                    $("#txtPPincode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

$("#btnNextOwnerDetails").click(() => {
    //if (CheckFocusDropDownBlankValidation("ddlPGender")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if (!$("#txtPDOB").hasClass("errorpage")) {
        if (CheckFocusBlankValidation("txtPEmail")) return !1;
        if (CheckEmailValidatoin("txtPEmail")) return !1;
        if (CheckFocusBlankValidation("txtPAddress1")) return !1;
        if (CheckFocusBlankValidation("txtPAddress2")) return !1;
        if (CheckFocusBlankValidation("txtPPincode")) return !1;
        if ($("#txtPPincode").hasClass("errorpincode")) { return false; }
        if (CheckFocusBlankValidation("txtPState")) return !1;
        if (CheckFocusBlankValidation("txtPCity")) return !1;

        motorProposalDetail = {};
        motorProposalDetail.enquiry_id = GetParameterValues("enquiry_id");
        motorProposalDetail.chainid = "";
        motorProposalDetail.proposalno = "";
        motorProposalDetail.suminsured = "";
        motorProposalDetail.title = $("#ddlPTitle option:selected").val();
        //motorProposalDetail.gender = $("#ddlPGender option:selected").val();
        motorProposalDetail.gender = (motorProposalDetail.title == "mr" ? "MALE" : "FEMALE");
        motorProposalDetail.firstname = $("#txtPFirstName").val();
        motorProposalDetail.lastname = $("#txtPLastName").val();
        motorProposalDetail.mobileno = $("#txtPMobile").val();
        motorProposalDetail.dob = $("#txtPDOB").val();
        motorProposalDetail.emailid = $("#txtPEmail").val();
        motorProposalDetail.address1 = $("#txtPAddress1").val();
        motorProposalDetail.address2 = $("#txtPAddress2").val();
        motorProposalDetail.landmark = $("#txtPLandmark").val();
        motorProposalDetail.pincode = $("#txtPPincode").val();

        //motorProposalDetail.stateid = $("#ddlPStateId").val();
        motorProposalDetail.statename = $("#txtPState").val();

        // motorProposalDetail.cityid = $("#ddlPCityId").val();
        motorProposalDetail.cityname = $("#txtPCity").val();

        //if ($("#chkIsProposerInsured").attr("checked") == 'checked') {
        //    motorProposalDetail.isproposer = true;
        //}
        //else motorProposalDetail.isproposer = false;
        motorProposalDetail.isproposer = false;

        $("#btnNextOwnerDetails").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/SaveMotorProposalData",
            data: '{motorProposalDetail: ' + JSON.stringify(motorProposalDetail) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.issuccess == "true") {
                        $("#tabNomineeDel").addClass("active");
                        $("#fsOwnerDetailsSection").css("display", "none");
                        $("#fsNomineeDetailsSection").css("display", "block");
                        $("html, body").animate({ scrollTop: 0 }, "fast");

                        if (data.relation != null) {
                            //$("#ddlRelation_1").append("<option value=''>Select</option>");
                            $("#ddlRelation_1").html("").append("<option value=''>SELECT</option>");
                            $(data.relation).each(function (i, item) {
                                $("#ddlRelation_1").append("<option value='" + item + "'>" + item + "</option>");
                            });
                        }
                    }
                }
                $("#btnNextOwnerDetails").html("NEXT");
            }
        });
    }
});

$("#btnNextProposerDetails").click(() => {
    if (CheckFocusDropDownBlankValidation("ddlRelation_1")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlTitle_1")) return !1;
    if (CheckFocusBlankValidation("txtInFirstName_1")) return !1;
    if (CheckFocusBlankValidation("txtInLastName_1")) return !1;
    if (CheckFocusBlankValidation("txtInDOB_1")) return !1;

    if (!$("#txtInDOB_1").hasClass("errorpage")) {
        let enquiry_id = GetParameterValues("enquiry_id");
        let nrelation = $("#ddlRelation_1 option:selected").val();
        let ntitle = $("#ddlTitle_1 option:selected").val();
        let nfirstname = $("#txtInFirstName_1").val();
        let nlastname = $("#txtInLastName_1").val();
        let ndob = $("#txtInDOB_1").val();

        $("#btnNextProposerDetails").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/SaveMotorNomineeData",
            data: '{enquiry_id: ' + JSON.stringify(enquiry_id) + ', relation: ' + JSON.stringify(nrelation) + ', title: ' + JSON.stringify(ntitle) + ', nfirstname: ' + JSON.stringify(nfirstname) + ',nlastname: ' + JSON.stringify(nlastname) + ',ndob: ' + JSON.stringify(ndob) + '}',

            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data == 'Success') {
                    $("#tabVehicleDel").addClass("active");
                    $("#fsVehicleHistorySection").css("display", "block");
                    $("#fsNomineeDetailsSection").css("display", "none");
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                }
                $("#btnNextProposerDetails").html("NEXT");

                $.ajax({
                    type: "Post",
                    url: "/Moter/GetValuntaryAndPreviousInsured",
                    data: '{enqid: ' + JSON.stringify(enquiry_id) + '}',

                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (data) {
                        if (data != null) {
                            $("#txtvihicle").val(data[0]);
                            $("#ddlValuntary1").html(data[1]);
                            $("#ddlPreviousInsured").html(data[2]);
                            //$("#ddlPreviousPolicyType").html(data[3]);
                            if (data[4] != "" && data[4] == "Fresh") {
                                $("#ddlPolicyHolderType").val(data[5]);
                                $("#ddlPolicyHolderType").prop("disabled", true);
                                $("#divPreviousInsured").css("display", "none").addClass("fresh");
                            }
                            else {
                                $("#ddlPolicyHolderType").prop("disabled", false);
                                $("#divPreviousInsured").css("display", "block").removeClass("fresh");
                            }
                        }
                    }
                });
            }
        });
    }
});

let PerposalLastSubmit = () => {
    let enchasno = false;
    let hdnGrossPrice = $("#hdnGrossPrice").val();

    if (CheckFocusBlankValidation("txtvihicle")) return !1;
    //if (CheckFocusBlankValidation("txtvihicleYr")) return !1;
    if (CheckFocusBlankValidation("txtengin")) return !1;
    if (CheckFocusBlankValidation("txtchassisno")) return !1;
    //if (CheckFocusBlankValidation("txtValuntary")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlValuntary1")) return !1;
    if (!$("#divPreviousInsured").hasClass("fresh")) {
        if (CheckFocusDropDownBlankValidation("ddlPreviousInsured")) return !1;
    }
    if (CheckFocusDropDownBlankValidation("ddlPolicyHolderType")) return !1;
    //if (CheckDropDownBlankValidation("ddlIsPreviousInsurerKnown")) return !1;

    if (hdnGrossPrice != "") {
        if (parseFloat(hdnGrossPrice) >= 49000) {
            if (CheckFocusBlankValidation("txtPanCard")) return !1;
        }
    }

    let enquiry_id = GetParameterValues("enquiry_id");
    let vehicleregno = $("#txtvihicle").val();
    let vehiclemfdyear = ""; //$("#txtvihicleYr").val();
    let engineno = $("#txtengin").val();
    if (engineno.length >= 6 && engineno.length <= 17) {
        $("#spengineno").css("color", "#ccc").css("font-weight", "normal");
        let chasisno = $("#txtchassisno").val();
        if (chasisno.length == 17) {
            $("#spchassisno").css("color", "#ccc").css("font-weight", "normal");

            let goforpeocess = true;
            let gstno = $("#txtGst").val();
            let panno = $("#txtPanCard").val();

            if (gstno != "") {
                if (gstno.length == 15) { $("#gstlenvalidation").removeClass("gstrederror").css("color", "#ccc").css("font-weight", "normal"); goforpeocess = true; }
                else { $("#gstlenvalidation").addClass("gstrederror").css("color", "red").css("font-weight", "bold"); goforpeocess = false; return false; }
            }
            else {
                if ($("#gstlenvalidation").hasClass("gstrederror")) {
                    $("#gstlenvalidation").removeClass("gstrederror").css("color", "#ccc").css("font-weight", "normal"); goforpeocess = true;
                }
            }
            if (panno != "") {
                if (panno.length == 10) { $("#panlenvalidation").removeClass("panrederror").css("color", "#ccc").css("font-weight", "normal"); goforpeocess = true; }
                else { $("#panlenvalidation").addClass("panrederror").css("color", "red").css("font-weight", "bold"); goforpeocess = false; return false; }
            }
            else {
                if ($("#panlenvalidation").hasClass("panrederror")) {
                    $("#panlenvalidation").removeClass("panrederror").css("color", "#ccc").css("font-weight", "normal"); goforpeocess = true;
                }
            }

            if (goforpeocess) {
                let valuntarydeductible = $("#ddlValuntary1 option:selected").val();

                let previousInsured = "";
                if ($("#divPreviousInsured").hasClass("fresh")) {
                    previousInsured = "";
                }
                else {
                    previousInsured = $("#ddlPreviousInsured option:selected").val();
                }

                let policyholder = $("#ddlPolicyHolderType option:selected").val();
                let islonelease = "no"; //$("#ddlIsPreviousInsurerKnown option:selected").val();
                $("#btnLastSubmit").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
                $.ajax({
                    type: "Post",
                    url: "/Moter/SaveVehicleHistoryData",
                    data: '{enquiry_id: ' + JSON.stringify(enquiry_id) + ', vehicleregno: ' + JSON.stringify(vehicleregno) + ', vehiclemfdyear: ' + JSON.stringify(vehiclemfdyear) + ', engineno: ' + JSON.stringify(engineno) + ',chasisno: ' + JSON.stringify(chasisno) + ',valuntarydeductible: ' + JSON.stringify(valuntarydeductible) + ',previousinsured: ' + JSON.stringify(previousInsured) + ',policyholder: ' + JSON.stringify(policyholder) + ',islonelease: ' + JSON.stringify(islonelease) + ',gstno:' + JSON.stringify(gstno) + ',panno:' + JSON.stringify(panno) + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (data) {
                        if (data != "") {

                            //$("#tabVehicleDel").addClass("active");
                            //$("#fsNomineeDetailsSection").css("display", "none");
                            //$("#fsVehicleHistorySection").css("display", "block");
                            //$("html, body").animate({ scrollTop: 0 }, "fast");
                            $("#divPerposalModal").html(data);
                            $("#btnLastSubmit").html("Submit");
                            $("#PMButton").click();
                        }
                        else {
                            $("#divPerposalModal").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger'>There is some error occurred, Please try again later.</p></div>");
                            $("#btnLastSubmit").html("Submit");
                            $("#PMButton").click();
                        }
                    }
                });
            }
        }
        else {
            $("#spchassisno").css("color", "red").css("font-weight", "bold");
        }
    }
    else {
        $("#spengineno").css("color", "red").css("font-weight", "bold");
        //setInterval(blink_text, 500);
    }
};
//function blink_text() {
//    $("#spengineno").fadeOut(500);
//    $("#spengineno").fadeIn(500);
//}
//function removeblink_text() {
//    $("#spengineno").fadeOut("normal", function () {
//        $(this).remove();
//    });
//    $("#txtengin").after("<span class='text-success' style='font-size: 10px;float: right;' id='spengineno'>must be min 6 and max 17 digit</span>");
//}
CheckPerposalDetailSummary = () => {
    window.location.href = "/moter/proposalsummary?enquiry_id=" + GetParameterValues("enquiry_id");
}

CalculateAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]),
            end = new Date(),
            nowYear = end.getFullYear(),
            pastYear = start.getFullYear(),
            diff = new Date(end - start),
            days = diff / 1000 / 60 / 60 / 24,
            age = nowYear - pastYear;
        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notpdobvalid").html("<i class='fa fa-check-circle'></i> " + age + " Years").css("color", "rgb(0, 163, 0)");
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notpdobvalid").html("<span class='text-danger pull-right' style='font-size: 12px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>").css("color", "red");
        }
    }
}
CalculateNomineeAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]),
            end = new Date(),
            nowYear = end.getFullYear(),
            pastYear = start.getFullYear(),
            diff = new Date(end - start),
            days = diff / 1000 / 60 / 60 / 24,
            age = nowYear - pastYear;
        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notndobvalid").html("<i class='fa fa-check-circle'></i> " + age + " Years").css("color", "rgb(0, 163, 0)");
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notndobvalid").html("<span class='text-danger pull-right' style='font-size: 12px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>").css("color", "red");
        }
    }
}

//function NextSelectedPlan() {
//    $(".section1").addClass("hidden");
//    $(".section3").addClass("hidden");
//    $(".section4").addClass("hidden");
//    $(".section2").removeClass("hidden");
//    $(".linext2").addClass("active");
//}
//function NextProposerDetails() {
//    $(".section1").addClass("hidden");
//    $(".section3").removeClass("hidden");
//    $(".section4").addClass("hidden");
//    $(".section2").addClass("hidden");
//    $(".linext3").addClass("active");
//}
//function PerposalSubmit() {
//    $(".section1").addClass("hidden");
//    $(".section3").addClass("hidden");
//    $(".section4").removeClass("hidden");
//    $(".section2").addClass("hidden");
//    $(".linext4").addClass("active");
//}
//$(".previous3").click(function () {
//    $(".section3").removeClass("hidden");
//    $(".section1").addClass("hidden");
//    $(".section4").addClass("hidden");
//    $(".section2").addClass("hidden");
//    $(".linext2").addClass("active");
//    $(".linext3").addClass("active");
//    $(".linext4").removeClass("active");
//});
//$(".previous2").click(function () {
//    $(".section3").addClass("hidden");
//    $(".section1").addClass("hidden");
//    $(".section4").addClass("hidden");
//    $(".section2").removeClass("hidden");
//    $(".linext2").addClass("active");
//    $(".linext3").removeClass("active");
//    $(".linext4").removeClass("active");
//});
//$(".previous1").click(function () {
//    $(".section3").addClass("hidden");
//    $(".section1").removeClass("hidden");
//    $(".section4").addClass("hidden");
//    $(".section2").addClass("hidden");
//    $(".linext2").removeClass("active");
//    $(".linext3").removeClass("active");
//    $(".linext4").removeClass("active");
//});
//$("#ddlIsPreviousInsurerKnown").change(function () {
//    if ($("option:selected", this).text() == "Yes") {
//        $(".prtext").removeClass("hidden");
//    } else {
//        $(".prtext").addClass("hidden");
//    }
//});

CheckMinMaxAccessory = (element) => {
    let minamt = $(element).data("minamount");
    let maxamt = $(element).data("maxamount");
    let thisamouval = $(element).val();
    let covername = $(element).data("covername");
    $("#perrormsg_" + covername).html("");

    if (thisamouval >= minamt && thisamouval <= maxamt) {

    }
    else {
        $("#perrormsg_" + covername).html("amount b/w ₹ " + minamt + "-" + maxamt + "");
    }
}

$("#ddlValuntary1").select2();
$("#ddlPreviousInsured").select2();
