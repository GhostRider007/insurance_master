﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function GetCustomParameterValues(url, param) {
    var url = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    BindMotorProductList(GetParameterValues('enquiry_id'));
});


BindMotorProductList = (enqid) => {
    $.ajax({
        type: "Post",
        url: "/Moter/GetMotorRegDetails",
        data: '{enqid: ' + JSON.stringify(enqid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response.length > 1) {
                $("#divProductLeftSection").html(response[0]);
                if (response[1] != "") { $("#divProductList").html(response[1]); }
                else { $("#divProductList").html("<div class='text-center'><img src='/Content/images/responsenotfound.png' style='width: 50%;'/></div>"); }
                $("#lblProductCount").html(response[2] + " Records");
            }
            else {
                $("#divProductLeftSection").html(response[0]);
                $("#divProductList").html("<div class='text-center'><img src='/Content/images/responsenotfound.png' style='width: 40%;padding-bottom: 50px;'/></div>");
                //$("#lblProductCount").html("0 Records");
            }
        }
    });
}

ClickMotorPerposal = (loopcount) => {
    let proposalno = $("#btnMotorPerposal_" + loopcount).data("proposalno");
    let suminsured = $("#btnMotorPerposal_" + loopcount).data("suminsured");
    let price = $("#btnMotorPerposal_" + loopcount).data("price");
    let chainid = $("#btnMotorPerposal_" + loopcount).data("chainid");
    let productcode = $("#btnMotorPerposal_" + loopcount).data("productcode");
    let chainimage = $("#btnMotorPerposal_" + loopcount).data("imageurl");

    $("#btnMotorPerposal_" + loopcount).html("<i class='fa fa-pulse fa-spinner'></i>");
    $(".bookbtnpopup_" + loopcount).html("<i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Moter/InsertAndUpdateMotoResponse",
        data: '{enquiryid:' + JSON.stringify(GetParameterValues('enquiry_id')) + ', chianid:' + JSON.stringify(chainid) + ', proposalno:' + JSON.stringify(proposalno) + ', suminsured:' + JSON.stringify(suminsured) + ',productcode:' + JSON.stringify(productcode) + ',chainimage:' + JSON.stringify(chainimage) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response == true) {
                window.location.href = "/moter/moterpremiumsummary?enquiry_id=" + GetParameterValues('enquiry_id');
            }
            else {
                $("#btnMotorPerposal_" + loopcount).html(price);
                $(".bookbtnpopup_" + loopcount).html(price);
            }
        }
    });
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(document.body).on('click', '.tablinks', function (e) {
    let evt = e;
    let cityName = $(this).data("tabname");
    openCity(evt, cityName);
});

$(document.body).on('click', '.seedetails', function (e) {
    let tabclickid = $(this).data("tabclickid");
    //$("#highlights_" + tabclickid).click();
    $(".tablinks").each(function () { $(this).removeClass("active"); });
    document.getElementById("Policy_Cover_" + tabclickid).style.display = "block";
    document.getElementById("Policy_does_not_Cover_" + tabclickid).style.display = "none";
    document.getElementById("Add-on_" + tabclickid).style.display = "none";
    document.getElementById("Key_Features_of_Insurer_" + tabclickid).style.display = "none";
    $(".Policy_Cover_" + tabclickid).addClass("active");
});

//$(document.body).on("click", ".clickmotorperposal", function (e) {
//    alert();

//});

//function BindMotorProductList(enqid) {
//    $.ajax({
//        type: "Post",
//        url: "/Moter/GetMotorRegDetails",
//        data: '{enqid: ' + JSON.stringify(enqid) + '}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            if (response.length > 0) {
//                $("#divCarProductList").html(response[0]);
//                $("#lblProductCount").html(response[1] + " Records");
//            }
//            else {
//                //$("#divCarProductList").html(" <p class='text-center' style='font-size: 25px;margin-top: 60px;'>We are unable to fetching quotes based on the information you provided.</p>");
//                //$("#divCarProductList").html("<div class='text-center'><img src='/Content/images/responsenotfound.png' style='width: 50%;'/></div>");
//                $("#lblProductCount").html("0 Records");
//            }
//        }
//    });
//}

//function BindMotorProductList(vehiclecode, licplnumber, regdate, pincode) {
//    //var motorreq = '{"enquiryId": "' + GetParameterValues('enquiry_id') + '","policyHolderType":"INDIVIDUAL","vehicleMaincode":"1111810803","licensePlateNumber":"' + licplnumber + '","registrationDate":"' + regdate + '","pincode":"' + pincode + '"}';

//    var motorreq = '{"enquiryId":"' + GetParameterValues('enquiry_id') + '","licensePlateNumber":"' + licplnumber + '","pincode":"' + pincode + '","policyHolderType":"INDIVIDUAL","registrationDate":"2018-11-01","vehicleMaincode":"1111810803"}';

//    $.ajax({
//        type: "Post",
//        url: "/Moter/GetMotorApiRespoDetails",
//        data: '{motorreq: ' + JSON.stringify(motorreq) + '}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            $("#divCarProductList").html(response[0]);
//            $("#lblProductCount").html(response[1] + " Records");
//        }
//    });
//}