﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(function () {
    let enqid = GetParameterValues('enquiry_id');
    BindVehicleDetails(enqid);
    BindAllProposalDetails(enqid);
});

BindVehicleDetails = (enqid) => {
    if (enqid != "" && enqid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Moter/GetProposalPlanSummary",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#divPlanSummarySection").html(response);
            }
        });
    }
}

BindAllProposalDetails = (enqid) => {
    if (enqid != "" && enqid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Moter/BindAllProposalDetails",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#divProposalPageDetails").html(response[0]);
                $("#divPaymentSection").html(response[1]);
                $("#lblProposalId").html("Enquiry Id : " + enqid.toUpperCase() + " &nbsp;/&nbsp; Proposal Id : " + response[2]);
            }
        });
    }
}

GetPdfDocument = () => {
    let enqid = GetParameterValues('enquiry_id');
    $.ajax({
        type: "Post",
        url: "/Moter/GetPdfDocument",
        data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            var a = $("<a/>");
            a.attr("download", (enqid + ".pdf"));
            a.attr("href", data);
            $("body").append(a);
            a[0].click();
            $("body").remove(a);
        }
    });
}

CheckPaymentDetails = () => {
    $("#divPerposalModal").html("<p class='text-center' style='font-size: 20px;padding-top: 10%;'>Please wait while we are fetching information.</p><p class='text-center' style='font-size: 20px;padding-bottom: 10%;'>Please wait... <i class='fa fa-spinner fa-pulse'></i></p>");
    $("#PStatusModal").modal('show');
    let enqid = GetParameterValues('enquiry_id');

    setTimeout(function () {
        $.ajax({
            type: "Post",
            url: "/Moter/CheckPaymentStatusDetails",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != "" && data != undefined) {
                    $("#divPerposalModal").html(data);
                }
                else {
                    $("#divPerposalModal").html("<p class='text-center' style='font-size: 20px;padding-top: 10%;'>Payment pending. Please complete payment</p>");
                }
            }
        });
    }, 2000);
}