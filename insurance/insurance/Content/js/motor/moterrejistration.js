﻿function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$("#reg").click(function () {
    $(".regs").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$("#brandtab").click(function () {
    $(".selctcar").removeClass("hidden");
    $(".regs").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
});
$("#models").click(function () {
    $(".selectmodel").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#ful").click(function () {
    $(".fuelss").removeClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#vari").click(function () {
    $(".Variant").removeClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".regs").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$(".Variantbtn").click(function () {
    $(".choseplan").removeClass("hidden");
    $(".registrationform").addClass("hidden");
});

$("#RTO_Code_A").select2();
$("#ddlYearOfMake").select2();
//===============================================================
$(function () {
    GetVehicleDetail();
    //GetYearOfMake();
    $("#reg").click();
});

GetVehicleDetail = () => {
    $.ajax({
        type: "Post",
        url: "/Moter/GetVehicleDetail",
        data: '{enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                summary.vregno = response.VechileRegNo;
                summary.vtype = response.VechileType;
                summary.instype = response.insurancetype;
                BindSelectedSummary(summary);
            }
        }
    });
}

function GetYearOfMake() {

    let currdate = parseInt(new Date().getFullYear());

    let makeofyearhtml = "<option value=''>Choose Year</option>";

    for (let i = 0; i <= 20; i++) {
        makeofyearhtml += "<option value='" + (currdate - i) + "'>" + (currdate - i) + "</option>";
    }


    $("#ddlYearOfMake").html("");
    $("#ddlYearOfMake").html(makeofyearhtml);
}

var summary = {};
$("#RTO_Code_A").change(function () {
    var rtoid = $("#RTO_Code_A option:selected").val();
    var rtoval = $("#RTO_Code_A option:selected").text();
    summary.rtoid = rtoid;
    summary.rto = rtoval;
    BindSelectedSummary(summary);
});
$("#txtDateOfReg").change(function () {
    var thisval = $(this).val();
    summary.datereg = thisval;
    BindSelectedSummary(summary);
});
$("#ddlYearOfMake").change(function () {
    var thisval = $(this).val();
    summary.yearofmake = thisval;
    BindSelectedSummary(summary);
});
$("#btnRegContinue").click(function () {
    if (CheckFocusDropDownBlankValidation("RTO_Code_A")) return !1;
    if (CheckFocusBlankValidation("txtDateOfReg")) return !1;
    //if (CheckFocusDropDownBlankValidation("ddlYearOfMake")) return !1;
    if (CheckFocusBlankValidation("txtDateOfManufacture")) return !1;

    var motormodel = {};
    motormodel.Enquiry_Id = GetParameterValues('enquiry_id');
    motormodel.RtoId = $("#RTO_Code_A option:selected").val();
    motormodel.RtoName = $("#RTO_Code_A option:selected").text();
    motormodel.DateOfReg = $("#txtDateOfReg").val();
    motormodel.DateOfMfg = $("#txtDateOfManufacture").val(); //$("#ddlYearOfMake option:selected").val();

    $("#btnRegContinue").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Moter/UpdateMotorRegistration",
        //data: '{model: ' + JSON.stringify(motormodel) + '}',
        data: '{enqid: ' + JSON.stringify(motormodel.Enquiry_Id) + ',rtoid: ' + JSON.stringify(motormodel.RtoId) + ',rtoname: ' + JSON.stringify(motormodel.RtoName) + ',dateofreg: ' + JSON.stringify(motormodel.DateOfReg) + ',dateofmfg: ' + JSON.stringify(motormodel.DateOfMfg) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                //BindBrandDetails();
                $("#divBrandItemSection").html(response);
                $('#brandtab').removeAttr("disabled");
                $("#brandtab").click();
                $("#btnRegContinue").html("UPDATE");
            }
            else {
                $("#btnRegContinue").html("CONTINUE");
            }
        }
    });
});

//function BindBrandDetails() {
//    $.ajax({
//        type: "Post",
//        url: "/Moter/GetBrandDetails",
//        data: '{model: ' + JSON.stringify(model) + '}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            if (response != "") {
//                $("#divBrandItemSection").html(response);
//                $('#brandtab').removeAttr("disabled");
//                $("#brandtab").click();
//                $("#btnRegContinue").html("UPDATE");
//            }
//        }
//    });
//}

$("#txtBrandSearch").keyup(function () {
    var brandsearchval = $(this).val().toLowerCase();
    $(".branddyn").each(function (i, item) {
        var currbrand = $(this).data('brand').toLowerCase();
        if (currbrand.includes(brandsearchval) == true) {
            $(this).attr("style", "display:block");
        }
        else {
            $(this).attr("style", "display:none");
        }
    });
});

$("#txtModelSearch").keyup(function () {
    var modelsearchval = $(this).val().toLowerCase();
    $(".modeldyn").each(function (i, item) {
        var currmodel = $(this).data('model').toLowerCase();
        if (currmodel.includes(modelsearchval) == true) {
            $(this).attr("style", "display:block");
        }
        else {
            $(this).attr("style", "display:none");
        }
    });
});

$(document.body).on('click', ".brandselectsec", function (e) {
    var currbrand = $(this).closest('div.branddyn').data('brand');
    summary.brand = currbrand;
    BindSelectedSummary(summary);
    BindModelDetails(currbrand);
});

$(document.body).on('click', ".modelselectsec", function (e) {
    var currmodel = $(this).closest('div.modeldyn').data('model');
    summary.model = currmodel;
    BindSelectedSummary(summary);
    BindFuelDetails(currmodel);
});

$(document.body).on('click', ".fueldyn", function (e) {
    var currfuel = $(this).data('fuel');
    summary.fuel = currfuel;
    BindSelectedSummary(summary);
    BindVarientDetails(currfuel);
});

$(document.body).on('click', ".varientselectsec", function (e) {
    var currvarient = $(this).closest('div.varientdyn').data('varient');
    summary.varient = currvarient;
    BindSelectedSummary(summary);
    GoToOtherDetailSection(currvarient);
});

function BindModelDetails(selectedbrand) {
    var tempselectedmodel = selectedbrand.replace(/ /g, "_");
    $.ajax({
        type: "Post",
        url: "/Moter/GetModelDetails",
        data: '{brand: ' + JSON.stringify(selectedbrand) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $(".brandselectsec").removeAttr("style");
                $("#BrandSected_" + tempselectedmodel).css("color", "#fff !important").css("background", "#1375b9").css("border", "1px dotted #1375b9");

                $("#divModelItemSection").html(response);
                $('#models').removeAttr("disabled");
                $("#models").click();
            }
        }
    });
}

function BindFuelDetails(selectedmodel) {
    var tempselectedmodel = selectedmodel.replace(/ /g, "_");
    $.ajax({
        type: "Post",
        url: "/Moter/GetFuelDetails",
        data: '{model: ' + JSON.stringify(selectedmodel) + ',brand: ' + JSON.stringify(summary.brand) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $(".modelselectsec").removeAttr("style");
                $("#ModelSected_" + tempselectedmodel).css("color", "#fff !important").css("background", "#1375b9").css("border", "1px dotted #1375b9");

                $("#divFuelItemSection").html(response);
                $('#ful').removeAttr("disabled");
                $("#ful").click();
            }
        }
    });
}

function BindVarientDetails(selectedfuel) {
    var tempselectedfuel = selectedfuel.replace(/ /g, "_");
    $.ajax({
        type: "Post",
        url: "/Moter/GetVarientDetails",
        data: '{model: ' + JSON.stringify(summary.model) + ',brand: ' + JSON.stringify(summary.brand) + ',fuel: ' + JSON.stringify(selectedfuel) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $(".fueldyn").removeAttr("style");
                $("#Fuel_" + tempselectedfuel).css("color", "#fff !important").css("background", "#1375b9").css("border", "1px dotted #1375b9");

                $("#divVarientItemSection").html(response);
                $('#vari').removeAttr("disabled");
                $("#vari").click();
            }
        }
    });
}

function GoToOtherDetailSection(selectedvarient) {
    $("#ddlIsPreviousInsurerKnown").val("").change();
    var tempselectedvarient = selectedvarient.replace(/ /g, "_").replace(/[_\W]+/g, "_");
    $.ajax({
        type: "Post",
        url: "/Moter/UpdateVarientDetails",
        data: '{varient: ' + JSON.stringify(selectedvarient) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response == true) {
                $('#otherdel').removeAttr("disabled");
                $("#otherdel").click();
            }
            //$(".varientselectsec").removeAttr("style");
            //$("#VarientSected_" + tempselectedvarient).css("color", "#fff !important").css("background", "#1375b9").css("border", "1px dotted #1375b9");
            //if (response == true) {
            //    window.location.href = "/moter/motersearchlist?enquiry_id=" + GetParameterValues('enquiry_id');
            //}

            $.ajax({
                type: "Post",
                url: "/Moter/GetValuntaryAndPreviousInsured",
                data: '{enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',

                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        //$("#txtvihicle").val(data[0]);
                        //$("#ddlValuntary1").html(data[1]);
                        //$("#ddlPreviousInsured").html(data[2]);
                        if (data[4] == "Renewal") {
                            $("#divPreviousInsurerKnown").css("display", "flex");
                            $("#ddlPreviousPolicyType").html(data[3]);
                            $("#divPreviousPolicyType").css("display", "flex");
                            $("#divPreviousPolicyType").addClass("renewal");
                            $("#divPolicyHolderType").css("display", "none");
                        }
                        else {
                            $("#divPreviousInsurerKnown").css("display", "none");
                            $("#ddlPreviousPolicyType").html("");
                            $("#divPreviousPolicyType").removeClass("renewal");
                            $("#divPreviousPolicyType").css("display", "none");
                            $("#divPolicyHolderType").css("display", "flex");
                        }
                    }
                }
            });
        }
    });
}

$("#otherdel").click(function () {
    $("#ddlPreviousNoClaimBonus").html("");
    $.ajax({
        type: "Post",
        url: "/Moter/GetNCBPercentage",
        //data: '{isprevious: ' + JSON.stringify(ispreviousinsurer) + ',insurerid: ' + JSON.stringify(insurerid) + ',insurername: ' + JSON.stringify(insurername) + ',policynumber: ' + JSON.stringify(policynumber) + ',expdate: ' + JSON.stringify(policyexpirydate) + ',lastyr: ' + JSON.stringify(isclaiminlastyear) + ',noclaim: ' + JSON.stringify(previousyearnoclaim) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                $("#ddlPreviousNoClaimBonus").append("<option value=''>Select</option>");
                $(response).each(function (i, item) {
                    $("#ddlPreviousNoClaimBonus").append("<option value='" + item.Value + "'>" + item.Value + " %</option>");
                });
            }
        }
    });
});

//$("#otherdel").click(function () {
//    $.ajax({
//        type: "Post",
//        url: "/Moter/GetPreviousInsurerCodes_GoDigit",
//        //data: '{isprevious: ' + JSON.stringify(ispreviousinsurer) + ',insurerid: ' + JSON.stringify(insurerid) + ',insurername: ' + JSON.stringify(insurername) + ',policynumber: ' + JSON.stringify(policynumber) + ',expdate: ' + JSON.stringify(policyexpirydate) + ',lastyr: ' + JSON.stringify(isclaiminlastyear) + ',noclaim: ' + JSON.stringify(previousyearnoclaim) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
//        contentType: "application/json; charset=utf-8",
//        datatype: "json",
//        success: function (response) {
//            if (response != null) {
//                $(response).each(function (i, item) {
//                    $("#ddlPreviousInsurerCode").append("<option value='" + item.Value + "'>" + item.Text + "</option>");
//                });
//            }
//        }
//    });

//});

function SaveMotorOtherDetails() {
    if ($("#divPreviousPolicyType").hasClass("renewal")) {
        //$(".pidyes").css("display", "block");
        if (CheckFocusDropDownBlankValidation("ddlPreviousPolicyType")) return !1;

        if (CheckFocusDropDownBlankValidation("ddlIsPreviousInsurerKnown")) return !1;
        let isPreviousInsurer = $("#ddlIsPreviousInsurerKnown option:selected").val();
        let ppType = ($("#ddlPreviousPolicyType option:selected").val() != undefined ? $("#ddlPreviousPolicyType option:selected").val() : "");
        if (isPreviousInsurer == "yes") {
            //if (CheckFocusDropDownBlankValidation("ddlPreviousInsurerCode")) return !1;
            //if (CheckFocusBlankValidation("txtPolicyNumber")) return !1;
            if (CheckFocusBlankValidation("txtPolicyExpDate")) return !1;
            if (CheckFocusDropDownBlankValidation("ddlClaimInLastYear")) return !1;
            let isclaiminlastyear = $("#ddlClaimInLastYear option:selected").val();
            if (isclaiminlastyear == "no") {
                if (CheckFocusDropDownBlankValidation("ddlPreviousNoClaimBonus")) return !1;
            }

            let ispreviousinsurer = isPreviousInsurer;
            //let insurerid = $("#ddlPreviousInsurerCode option:selected").val();
            //let insurername = $("#ddlPreviousInsurerCode option:selected").text();
            let policynumber = $("#txtPolicyNumber").val();
            let policyexpirydate = $("#txtPolicyExpDate").val();
            let previousyearnoclaim = "0";
            if (isclaiminlastyear == "no") {
                previousyearnoclaim = $("#ddlPreviousNoClaimBonus option:selected").val();
            }
            $("#btnMotorGetSearch").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/Moter/UpdateOtherDetails",
                data: '{isprevious: ' + JSON.stringify(ispreviousinsurer) + ',policynumber: ' + JSON.stringify(policynumber) + ',expdate: ' + JSON.stringify(policyexpirydate) + ',lastyr: ' + JSON.stringify(isclaiminlastyear) + ',noclaim: ' + JSON.stringify(previousyearnoclaim) + ',enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + ',pptype: ' + JSON.stringify(ppType) + ',phtype: ""}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    try {
                        if (response == true) {
                            window.location.href = "/moter/motersearchlist?enquiry_id=" + GetParameterValues('enquiry_id');
                        }
                        else {
                            $("#btnMotorGetSearch").html("GET SEARCH");
                        }
                    } catch (e) {
                        $("#btnMotorGetSearch").html("GET SEARCH");
                    }
                }
            });
        }
    }
    else {
        if (CheckFocusDropDownBlankValidation("ddlPolicyHolderType")) return !1;
        //if (CheckFocusDropDownBlankValidation("ddlIsPreviousInsurerKnown")) return !1;
        let isPreviousInsurer = "no"; //$("#ddlIsPreviousInsurerKnown option:selected").val();
        let phtype = $("#ddlPolicyHolderType option:selected").val();
        $("#btnMotorGetSearch").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/UpdateOtherDetails",
            data: '{isprevious: ' + JSON.stringify(isPreviousInsurer) + ',policynumber: "",expdate: "",lastyr: "",noclaim: "",enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + ',pptype: "",phtype: ' + JSON.stringify(phtype) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                try {
                    if (response == true) {
                        window.location.href = "/moter/motersearchlist?enquiry_id=" + GetParameterValues('enquiry_id');
                    }
                    else {
                        $("#btnMotorGetSearch").html("GET SEARCH");
                    }
                } catch (e) {
                    $("#btnMotorGetSearch").html("GET SEARCH");
                }
            }
        });
    }
}

function BindSelectedSummary(summary) {
    if (summary != null) {
        var summarthtml = "";

        if (summary.vtype != "" && summary.vtype != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Vehicle Type</label></div><div class='col-sm-6'><label>" + summary.vtype.replace("_"," ") + "</label></div></div>";
        }
        if (summary.vregno != "" && summary.vregno != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Registration No.</label></div><div class='col-sm-6'><label>" + summary.vregno + "</label></div></div>";
        }       
        if (summary.instype != "" && summary.instype != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Insurance Type</label></div><div class='col-sm-6'><label>" + summary.instype + "</label></div></div>";
        }
        if (summary.rtoid != "" && summary.rtoid != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>RTO</label></div><div class='col-sm-6'><label>" + summary.rto + "</label></div></div>";
        }
        if (summary.datereg != "" && summary.datereg != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Date of Registration</label></div><div class='col-sm-6'><label>" + summary.datereg + "</label></div></div>";
        }
        if (summary.yearofmake != "" && summary.yearofmake != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Year of Manufactur</label></div><div class='col-sm-6'><label>" + summary.yearofmake + "</label></div></div>";
        }
        if (summary.brand != "" && summary.brand != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Brand</label></div><div class='col-sm-6'><label>" + summary.brand + "</label></div></div>";
        }
        if (summary.model != "" && summary.model != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Model</label></div><div class='col-sm-6'><label>" + summary.model + "</label></div></div>";
        }
        if (summary.fuel != "" && summary.fuel != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Fuel Type</label></div><div class='col-sm-6'><label>" + summary.fuel + "</label></div></div>";
        }
        if (summary.varient != "" && summary.varient != undefined) {
            summarthtml += "<div class='row borderobx'><div class='col-sm-6' style='border-right: 1px solid #ccc;'><label>Variant</label></div><div class='col-sm-6'><label>" + summary.varient + "</label></div></div>";
        }

        $("#divSelctedSummary").html("");

        if (summarthtml != "") {
            $("#divSelctedSummary").html("<div class='row'><div class='col-sm-12 imgborderobx'><img src='/Content/images/car-new.gif' class='carimg'/></div></div>" + summarthtml);
        }
        else {
            $("#divSelctedSummary").html("<div class='row'><div class='col-sm-12 imgborderobx'><img src='/Content/images/car-new.gif' class='carimg'/></div></div><div class='row borderobx'><div class='col-sm-12' style='border-right: 1px solid #ccc;text-align:center;color:#dc3545'><label>Details Not Available !</label></div></div>");
        }
    }
    else {
        alert("empty");
    }
}

$("#ddlIsPreviousInsurerKnown").change(function () {
    let thisval = $(this).val();
    if (thisval == "yes") {
        $("#divHavePreviousInsurerKnown").css("display", "block");
    }
    else {
        $("#ddlPreviousInsurerCode").val("").change();
        $("#txtPolicyNumber").val("");
        $("#txtPolicyExpDate").val("");
        $("#ddlClaimInLastYear").val("").change();
        $("#ddlPreviousNoClaimBonus").val("").change();
        $("#divHavePreviousInsurerKnown").css("display", "none");
    }
});

$("#ddlClaimInLastYear").change(function () {
    let thisval = $(this).val();
    if (thisval == "no") {
        $("#NoClaimBonus").css("display", "block");
    }
    else {
        $("#ddlPreviousNoClaimBonus").val("").change();
        $("#NoClaimBonus").css("display", "none");
    }
});

$("#ddlPreviousInsurerCode").select2();