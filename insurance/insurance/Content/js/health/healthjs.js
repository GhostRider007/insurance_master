﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    var enquiry_id = GetParameterValues('enquiry_id');
    if (enquiry_id == null || enquiry_id == '') { GenrateEnquiry_Id(); }
    StarterEvent();
});

function GenrateEnquiry_Id() {
    $.ajax({
        type: "Post",
        url: "/Health/GenrateEnquiryId",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                window.location.href = response;
            }
        }
    });
}
function StarterEvent() {
    BindSumInsured("ddlSumInsured");
}
function GetProductList() {
    if (CheckFocusBlankValidation("txtFirstName")) return !1;
    if (CheckFocusBlankValidation("txtLastName")) return !1;
    if (CheckFocusBlankValidation("txtEmailId")) return !1;
    if (CheckEmailValidatoin("txtEmailId")) return !1;
    if (CheckFocusBlankValidation("txtMobileNo")) return !1;
    if (CheckFocusBlankValidation("txtPinCode")) return !1;
    if (CheckFocusBlankValidation("txtFirstName")) return !1;
    if (!($("#chkHearBy").prop("checked"))) { alert("You must agree terms and condition."); return false; }

    var quotesVal = {};
    quotesVal.Policy_Type = $("#ddlPolicyType option:selected").val();
    quotesVal.Policy_Type_Text = $("#ddlPolicyType option:selected").text();
    quotesVal.Sum_Insured = $("#ddlSumInsured option:selected").val();
    quotesVal.Self = $('#CheSelf').prop('checked');
    quotesVal.SelfAge = $("#txtSelfAge").val();
    quotesVal.Spouse = $('#CheSpouse').prop('checked');
    quotesVal.SpouseAge = $("#txtSpouseAge").val();
    quotesVal.Son = $('#chkSon').prop('checked');
    quotesVal.Son1Age = $("#txtSon1Age").val();
    quotesVal.Son2Age = $("#txtSon2Age").val();
    quotesVal.Son3Age = $("#txtSon3Age").val();
    quotesVal.Son4Age = $("#txtSon4Age").val();
    quotesVal.Daughter = $('#chkDaughter').prop('checked');
    quotesVal.Daughter1Age = $("#txtDaughter1Age").val();
    quotesVal.Daughter2Age = $("#txtDaughter2Age").val();
    quotesVal.Daughter3Age = $("#txtDaughter3Age").val();
    quotesVal.Daughter4Age = $("#txtDaughter4Age").val();
    quotesVal.Father = $('#CheFather').prop('checked');
    quotesVal.FatherAge = $("#txtFatherAge").val();
    quotesVal.Mother = $('#CheMother').prop('checked');
    quotesVal.MotherAge = $("#txtMotherAge").val();
    quotesVal.Gender = $("#ddlGender option:selected").val();
    quotesVal.First_Name = $("#txtFirstName").val();
    quotesVal.Last_Name = $("#txtLastName").val();
    quotesVal.Email_Id = $("#txtEmailId").val();
    quotesVal.Mobile_No = $("#txtMobileNo").val();
    quotesVal.Pin_Code = $("#txtPinCode").val();
    quotesVal.Is_Term_Accepted = $('#chkHearBy').prop('checked');
    quotesVal.Enquiry_Id = GetParameterValues('enquiry_id');

    if (quotesVal.Pin_Code.length == 6) {
        if ($("#txtPinCode").hasClass("successpincode")) {
            $("#btnGetQuotes").html("Please Wait... <i class='fa fa-spin fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/Health/SaveGenratedQuotes",
                data: '{quotes: ' + JSON.stringify(quotesVal) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    if (response != "") {
                        if (response == "notredirect") {
                            window.location.href = "/health/helthsearch";
                        }
                        else {
                            window.location.href = response;
                        }
                    }
                    else {
                        $("#btnGetQuotes").html("GET QUOTES");
                    }
                }
            });
        }
        else {
            if (CheckFocusBlankValidation("txtPinCode")) return !1;
        }
    }
    else {
        if (NotExistFocusBlankValidation("txtPinCode")) return !1;
        $(".notpincodevalid").removeClass("hidden").html("provide valid pincode");
    }
}
function GetNewProductList() {
    var isfieldapplied = false;
    let membercount = 0;
    if (CheckFocusDropDownBlankValidation("ddlPolicyType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlSumInsured")) return !1;
    if ($("#CheSelf").is(":checked")) { if (CheckFocusBlankValidation("txtSelfAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }
    if ($("#CheSpouse").is(":checked")) { if (CheckFocusBlankValidation("txtSpouseAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }
    if ($("#chkSon").is(":checked")) { var noofson = $("#chkSonInput").val(); var i; for (i = 1; i <= noofson; i++) { if (CheckFocusBlankValidation("txtSon" + i + "Age")) return !1; } isfieldapplied = true; membercount = membercount + 1; }
    if ($("#chkDaughter").is(":checked")) { var noofdaughter = $("#chkDaughterInput").val(); var i; for (i = 1; i <= noofdaughter; i++) { if (CheckFocusBlankValidation("txtDaughter" + i + "Age")) return !1; } isfieldapplied = true; membercount = membercount + 1; }
    if ($("#CheFather").is(":checked")) { if (CheckFocusBlankValidation("txtFatherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }
    if ($("#CheMother").is(":checked")) { if (CheckFocusBlankValidation("txtMotherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }

    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") { if (membercount < 1) { alert("Please select at least one member for genrate quotes"); return false; } }
    else { if (membercount < 2) { alert("Please select minimum two members for genrate quotes"); return false; } }
    if (CheckFocusBlankValidation("txtPinCode")) return !1;

    var quotesVal = {};
    quotesVal.Policy_Type = $("#ddlPolicyType option:selected").val();
    quotesVal.Policy_Type_Text = $("#ddlPolicyType option:selected").text();
    quotesVal.Sum_Insured = $("#ddlSumInsured option:selected").val();
    quotesVal.Self = $('#CheSelf').prop('checked');
    quotesVal.SelfAge = $("#txtSelfAge").val();
    quotesVal.Spouse = $('#CheSpouse').prop('checked');
    quotesVal.SpouseAge = $("#txtSpouseAge").val();
    quotesVal.Son = $('#chkSon').prop('checked');
    quotesVal.Son1Age = $("#txtSon1Age").val();
    quotesVal.Son2Age = $("#txtSon2Age").val();
    quotesVal.Son3Age = $("#txtSon3Age").val();
    quotesVal.Son4Age = $("#txtSon4Age").val();
    quotesVal.Daughter = $('#chkDaughter').prop('checked');
    quotesVal.Daughter1Age = $("#txtDaughter1Age").val();
    quotesVal.Daughter2Age = $("#txtDaughter2Age").val();
    quotesVal.Daughter3Age = $("#txtDaughter3Age").val();
    quotesVal.Daughter4Age = $("#txtDaughter4Age").val();
    quotesVal.Father = $('#CheFather').prop('checked');
    quotesVal.FatherAge = $("#txtFatherAge").val();
    quotesVal.Mother = $('#CheMother').prop('checked');
    quotesVal.MotherAge = $("#txtMotherAge").val();
    quotesVal.Gender = ""; //$("#ddlGender option:selected").val();
    quotesVal.First_Name = ""; //$("#txtFirstName").val();
    quotesVal.Last_Name = ""; //$("#txtLastName").val();
    quotesVal.Email_Id = ""; //$("#txtEmailId").val();
    quotesVal.Mobile_No = ""; // $("#txtMobileNo").val();
    quotesVal.Pin_Code = $("#txtPinCode").val();
    quotesVal.Is_Term_Accepted = true; //$('#chkHearBy').prop('checked');
    quotesVal.Enquiry_Id = GetParameterValues('enquiry_id');
    quotesVal.EnquiryBookingType = "online";

    if (quotesVal.Pin_Code.length == 6) {
        if ($("#txtPinCode").hasClass("successpincode")) {
            $("#btnGetQuotes").html("Please Wait... <i class='fa fa-spin fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/Health/SaveGenratedQuotes",
                data: '{quotes: ' + JSON.stringify(quotesVal) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    if (response != "") {
                        if (response == "notredirect") {
                            window.location.href = "/health/helthsearch";
                        }
                        else {
                            window.location.href = response;
                        }
                    }
                    else {
                        $("#btnGetQuotes").html("GET QUOTES");
                    }
                }
            });
        }
        else {
            if (CheckFocusBlankValidation("txtPinCode")) return !1;
        }
    }
    else {
        if (NotExistFocusBlankValidation("txtPinCode")) return !1;
        $(".notpincodevalid").removeClass("hidden").html("provide valid pincode");
    }
}
function ValidateFirstQuotesSection() {
    var isfieldapplied = false;
    if (CheckFocusDropDownBlankValidation("ddlPolicyType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlSumInsured")) return !1;
    if ($("#CheSelf").is(":checked")) { if (CheckFocusBlankValidation("txtSelfAge")) return !1; isfieldapplied = true; }
    if ($("#CheSpouse").is(":checked")) { if (CheckFocusBlankValidation("txtSpouseAge")) return !1; isfieldapplied = true; }
    if ($("#chkSon").is(":checked")) { var noofson = $("#chkSonInput").val(); var i; for (i = 1; i <= noofson; i++) { if (CheckFocusBlankValidation("txtSon" + i + "Age")) return !1; } isfieldapplied = true; }
    if ($("#chkDaughter").is(":checked")) { var noofdaughter = $("#chkDaughterInput").val(); var i; for (i = 1; i <= noofdaughter; i++) { if (CheckFocusBlankValidation("txtDaughter" + i + "Age")) return !1; } isfieldapplied = true; }
    if ($("#CheFather").is(":checked")) { if (CheckFocusBlankValidation("txtFatherAge")) return !1; isfieldapplied = true; }
    if ($("#CheMother").is(":checked")) { if (CheckFocusBlankValidation("txtMotherAge")) return !1; isfieldapplied = true; }

    if (isfieldapplied) {
        $(".section1").addClass("hidden");
        $(".section2").removeClass("hidden");
        $(".linext2").addClass("active");
    }
    else {
        alert("Please select at least one member for genrate quotes");
    }
}
function BindSumInsured(dropdownid) {
    $("#" + dropdownid).append("<option value=''>Please Wait...</option>");
    $.ajax({
        type: "Post",
        url: "/Health/GetSumInsuredPriceList",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#" + dropdownid).html("");
                $("#" + dropdownid).append("<option value='0'>Select</option>");
                $.each(response, function (i, item) { $("#" + dropdownid).append("<option value='" + item.SumInsuredValue + "' data-minprice='" + item.MinSumInsured + "' data-maxprice='" + item.MaxSumInsured + "'> " + item.SumInsuredText + "</option>"); });
            }
        }
    });
}
//$('#ddlPolicyType').change(function (event) {
//    var slectedOpt = $("#ddlPolicyType option:selected").text();
//    if (slectedOpt == "Individual - Parents" || slectedOpt == "Floater - Parents") {
//        $("#ParentsSection").removeClass("hidden");
//        $(".OtherSection").addClass("hidden");
//        if ($("#CheSelf").prop("checked") == true) { $("#CheSelf").click(); }
//        if ($("#CheSpouse").prop("checked") == true) { $("#CheSpouse").click(); }
//        if ($("#chkSon").prop("checked") == true) { $("#chkSon").click(); }
//        if ($("#chkDaughter").prop("checked") == true) { $("#chkDaughter").click(); }
//    }
//    else {
//        $("#ParentsSection").addClass("hidden");
//        $(".OtherSection").removeClass("hidden");
//        if ($("#CheFather").prop("checked") == true) { $("#CheFather").click(); }
//        if ($("#CheMother").prop("checked") == true) { $("#CheMother").click(); }
//    }
//});

ResetMembers = () => {
    $(".chkmember").each(function () {
        let objmember = $(this).data("member");
        if ($(".chkbox_" + objmember).prop("checked") == true) {
            $(".chkbox_" + objmember).click();
        }
    });
}

function OnOffMember(membertype) {
    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") {
        $(".chkmember").each(function () {
            let objmember = $(this).data("member");

            if (objmember == membertype) {
                if ($(".chkbox_" + membertype).prop("checked") == true) {
                    $(".chkbox_" + membertype).click();
                }
            }
            else {
                if ($(".chkbox_" + objmember).prop("checked") == true) {
                    $(".chkbox_" + objmember).click();
                }
            }
        });
    }
    else {
        if ($(".FamilySection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "father" || objmember == "mother") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
        if ($("#ParentsSection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "self" || objmember == "spouse" || objmember == "son" || objmember == "daughter") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
    }
}

//=======================================================
$("#chkSon").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divSonAgeSection"), "Son"); });

$("#chkDaughter").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divDaughterAgeSection"), "Daughter"); });

$("#CheSelf").click(function () {
    $("#txtSelfAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SelfAge").removeClass("hidden");
    }
    else {
        $(".SelfAge").addClass("hidden");
    }
})

$("#CheSpouse").click(function () {
    $("#txtSpouseAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SpouseAge").removeClass("hidden");
    }
    else {
        $(".SpouseAge").addClass("hidden");
    }
})

$("#CheFather").click(function () {
    $("#txtFatherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".FatherAge").removeClass("hidden");
    }
    else {
        $(".FatherAge").addClass("hidden");
    }
})

$("#CheMother").click(function () {
    $("#txtMotherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".MotherAge").removeClass("hidden");
    }
    else {
        $(".MotherAge").addClass("hidden");
    }
})

$(".clickcheckbox").click(function () {
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $("#" + selectedId + "Input").val("1");
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").removeClass("hidden");
        SonDautAgeSectionPlusMinus("plus", 1, "#div" + thisname + "AgeSection", thisname);
    }
    else {
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").addClass("hidden");
        SonDautAgeSectionPlusMinus("minus", 1, "#div" + thisname + "AgeSection", thisname);
    }
});

function SonDautAgeSectionPlusMinus(type, count, appentdiv, name) {
    if (type == "plus") {
        $(appentdiv).html("");
        var i; var bindagedel = "";
        for (i = 1; i <= parseInt(count); i++) {
            bindagedel += "<div class='col-sm-12'><div class='row'>"
                + "<div class='col-sm-6'> <label class='pull-right' style='color: #e71820;'>" + name + "</label></div>"
                + "<div class='col-sm-6'><h6>"
                + "<span class='ageleft'>Age</span>"
                + "<input type='text' class='form-control agetextbox' id='txt" + name + i + "Age' name='txt" + name + i + "Age' maxlength='2' onkeypress='return isNumberOnlyKeyNoDot(event)' onchange='return CalculateChildAge(\"txt" + name + i + "Age\",\"childageerror_" + name + i + "\")' /> <span class='yrposion'>yr</span>"
                + "</h6><span id='childageerror_" + name + i + "' style='float: right;margin: 0px 0px 0px 42px;color: #e71820;font-size: 8px;'></span></div>"
                + "</div></div>";
        }
        $(appentdiv).html(bindagedel);
    }
    else {
        $(appentdiv).html("");
    }
}

CalculateChildAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) <= 25) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age must below 25 Y&nbsp;</span>");
        }
    }
}

CalculateAdultAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");
        }
    }
}

function increment(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count < 4) {
        $(obj).parent().parent().find('input').val(count + 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count + 1, ("#" + appdiv), name);
    }
}
function decrement(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count != 1) {
        $(obj).parent().parent().find('input').val(count - 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count - 1, ("#" + appdiv), name);
    }
}
function checkSection1RegForm() {
    ValidateFirstQuotesSection();
    //$(".section1").addClass("hidden");
    //$(".section2").removeClass("hidden");
    //$(".linext2").addClass("active");
}
function checkSection2RegForm() {
    $(".section1").addClass("hidden");
    $(".section2").addClass("hidden");

}
function checkSection3RegForm() {
    $('.mainregcss').addClass("hidden");
    $('.tempregcss').removeClass("hidden");
}
$(".previous1").click(function () {
    $(".section2").addClass("hidden");
    $(".linext2").removeClass("active");
    $(".section1").removeClass("hidden");
});
$('.tablinks2').on('click', function () {
    $('.tablinks2').removeClass('selected');
    $(this).addClass('selected');
});
$('.tablinks3').on('click', function () {
    $('.tablinks3').removeClass('selected');
    $(this).addClass('selected');
});
$('.tablinks4').on('click', function () {
    $('.tablinks4').removeClass('selected');
    $(this).addClass('selected');
});
$('.tablinks5').on('click', function () {
    $('.tablinks5').removeClass('selected');
    $(this).addClass('selected');
});

function CheckPincode2() {
    $(".notpincodevalid").addClass("hidden").html("");
    var txtPinCodeVal = $("#txtPinCode").val();

    if (txtPinCodeVal.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/CheckPincode",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response != "") {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response);
                    $("#txtPinCode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}