﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    //GetDataByProductId(GetParameterValues('enquiry_id'), GetParameterValues('product'), GetParameterValues('companyid'), GetParameterValues('suminsured'));
    GetDataByProductId(GetParameterValues('enquiry_id'));
});

//function GetDataByProductId(id, productid, companyid, suminsured) {    
function GetDataByProductId(enquiryid) {
    $.ajax({
        type: "Post",
        url: "/Health/GetDataByProductId",
        //data: '{enquiry_id:' + JSON.stringify(enquiryid) + ',productid: ' + JSON.stringify(productid) + ',companyid: ' + JSON.stringify(companyid) + ',suminsured: ' + JSON.stringify(suminsured) + '}',
        data: '{enquiry_id:' + JSON.stringify(enquiryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#spTotalPremium").html("Permium is " + response[0]);
            $("#divSelectedPlanSection").html("").html(response[1]);
            $("#divProductDetails").html("").html(response[2]);
        }
    });
}

$(document.body).on('click', ".chkpremiumamt", function (e) {
    $("#spTotalPremium").html("Permium is " + $(this).val());
    let dataprem = $(this).data("premium");
    $("#divPremiumBreakup").html('<p class="text-center" style="margin-left: 23%;">Please wait,<br>We are fetching details... <i class="fa fa-spinner fa-pulse"></i></p>');
    $.ajax({
        type: "Post",
        url: "/Health/SetChangedPremiumBreakup",
        //data: '{enquiry_id:' + JSON.stringify(enquiryid) + ',productid: ' + JSON.stringify(productid) + ',companyid: ' + JSON.stringify(companyid) + ',suminsured: ' + JSON.stringify(suminsured) + '}',
        data: '{totalpremium:' + JSON.stringify(dataprem) + ',enquiryid:' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            setTimeout(function () {
                $("#divPremiumBreakup").html(response[0]);
                $("#divAddonSection").html(response[1]);
            }, 500);
        }
    });
});

function NextSelectedPlan() {
    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    //proposal.productid = GetParameterValues('product');
    //proposal.companyid = GetParameterValues('companyid');
    //proposal.suminsured = GetParameterValues('suminsured');
    proposal.policyfor = $("#policyfor").data("policyfor");
    proposal.plantype = $("#plantype").data("plantype");

    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            proposal.periodfor = $(this).data("periodfor");
            proposal.premium = $(this).data("premium");
            proposal.discount = $(this).data("discount");
        }
    });

    let addonlist = [];
    $(".chkAddon_Year" + proposal.periodfor).each(function (index, element) {
        if ($(this).prop("checked") == true) {
            let addon = {};
            addon.inquiryid = $(this).data("inquiryid");
            addon.addOnsId = $(this).data("addonsid");
            addon.code = $(this).data("code");
            addon.PlanAmount = $(this).data("totalpremium");
            addon.PlanPeriod = $(this).data("period");
            addonlist.push(addon);
        }
    });

    $("#btnNextSelectedPlan").html("processing... <i class='fa fa-spinner fa-pulse'></i>");

    $.ajax({
        type: "Post",
        url: "/Health/GetNSaveSelectedPlanData",
        data: '{proposal:' + JSON.stringify(proposal) + ',addonlist:' + JSON.stringify(addonlist) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            setTimeout(function () {
                GetAnnualIncomeDetails(response);
            }, 200);

            //$("#ddlPTitle").val(response.title);
            //$("#txtPFirstName").val(response.firstname);
            //$("#txtPLastName").val(response.lastname);
            //$("#txtPMobile").val(response.mobile);
            //$("#txtPEmail").val(response.emailid);
            //$("#txtPPincode").val(response.pincode);
            //$("#txtPDOB").val(response.dob);
            //$("#txtPAddress1").val(response.address1);
            //$("#txtPAddress2").val(response.address2);
            //$("#txtPLandmark").val(response.landmark);
            //$("#txtPState").val(response.statename);
            //$("#txtPCity").val(response.cityname);

            //$("#txtPPan").val(response.panno);
            //$("#txtPAadhar").val(response.aadharno);
            ////$("#ddlAnualIncome option[value=" + response.annualincome + "]").attr("selected", "selected");  //response.annualincome
            //$("#ddlAnualIncome").val(response.annualincome);

            //if (response.isproposerinsured) { $("#chkIsProposerInsured").prop("checked", true); } else { $("#chkIsProposerInsured").prop("checked", false); }

            //$("#fdselectedplansection").css("display", "none");
            //$("#fdproposerdetails").css("display", "block");
            //$(".linext2").addClass("active");
            ////GoToProposerDetail();
            //$("#btnNextSelectedPlan").html("NEXT");

            //CalculateAge('txtPDOB');
        }
    });

}

GetAnnualIncomeDetails = (response) => {
    if (response.enquiryid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetAnnualIncomeDetails",
            data: '{enquiryid:' + JSON.stringify(response.enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlAnualIncome").html(data);
                }
                $("#ddlPMStatus").val(response.mstatus);
                $("#ddlPTitle").val(response.title);
                $("#txtPFirstName").val(response.firstname);
                $("#txtPLastName").val(response.lastname);
                $("#txtPMobile").val(response.mobile);
                $("#txtPEmail").val(response.emailid);
                $("#txtPPincode").val(response.pincode);
                $("#txtPDOB").val(response.dob);
                $("#txtPAddress1").val(response.address1);
                $("#txtPAddress2").val(response.address2);
                $("#txtPLandmark").val(response.landmark);
                GetCityListFromStarHealth(response.pincode, response.cityid, response.areaid);
                CheckAndGetDetails(response.pincode);
                //$("#txtPState").val(response.statename);
                //$("#txtPCity").val(response.cityname);

                $("#txtPPan").val(response.panno);
                $("#txtPAadhar").val(response.aadharno);
                //$("#ddlAnualIncome option[value=" + response.annualincome + "]").attr("selected", "selected");  //response.annualincome
                $("#ddlAnualIncome").val(response.annualincome);

                if (response.isproposerinsured) { $("#chkIsProposerInsured").prop("checked", true); } else { $("#chkIsProposerInsured").prop("checked", false); }
                if (response.criticalillness) { $("#chkHavingCriticalIllness").prop("checked", true); } else { $("#chkHavingCriticalIllness").prop("checked", false); }
                if (response.ispropnominee) { $("#chkIsPropNominee").prop("checked", true); } else { $("#chkIsPropNominee").prop("checked", false); }

                $("#fdselectedplansection").css("display", "none");
                $("#fdproposerdetails").css("display", "block");
                $(".linext2").addClass("active");
                //GoToProposerDetail();
                $("#btnNextSelectedPlan").html("NEXT");

                CalculateAge('txtPDOB');
            }
        });
    }
}

GetCityListFromStarHealth = (pincode, cityid, areaid) => {
    if (pincode != "") {
        let intpincode = parseInt(pincode);
        if (pincode.length == 6) {
            $("#ddlCityList").html("").html("<option value='0'>processing... <i class='fa fa-spinner fa-pulse'></i></option>");
            $.ajax({
                type: "Post",
                url: "/Health/GetCityListFromStarHealth",
                data: '{pincode:' + JSON.stringify(pincode) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        $("#ddlCityList").html(data);
                        if (cityid != "") {
                            $("#ddlCityList").val(cityid);
                            if (areaid != "") {
                                $.ajax({
                                    type: "Post",
                                    url: "/Health/GetAreaListFromStarHealth",
                                    data: '{pincode:' + JSON.stringify(pincode) + ',cityid:' + JSON.stringify(cityid) + '}',
                                    contentType: "application/json; charset=utf-8",
                                    datatype: "json",
                                    success: function (data) {
                                        if (data != null) {
                                            $("#ddlAreaList").html(data);
                                            $("#ddlAreaList").val(areaid);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }
}

$("#ddlCityList").change(function () {
    let cityid = $("#ddlCityList option:selected").val();
    if (cityid != "" && cityid != "0") {
        $("#ddlAreaList").html("").html("<option value='0'>processing... <i class='fa fa-spinner fa-pulse'></i></option>");
        let pincode = $("#txtPPincode").val();
        $.ajax({
            type: "Post",
            url: "/Health/GetAreaListFromStarHealth",
            data: '{pincode:' + JSON.stringify(pincode) + ',cityid:' + JSON.stringify(cityid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlAreaList").html(data);
                }
            }
        });
    }
    else {
        $("#ddlAreaList").html("").html("<option value='0'>Select Area</option>");
    }
});

$(".previous1").click(function () {
    $("#fdproposerdetails").css("display", "none");
    $("#fdselectedplansection").css("display", "block");
    $(".linext2").removeClass("active");
});

function NextProposerDetails() {

    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlPMStatus")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if ($("#txtPMobile").hasClass("pmoberror")) { return false; }
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if (CheckFocusBlankValidation("txtPEmail")) return !1;
    if (CheckEmailValidatoin("txtPEmail")) return !1;
    if (CheckFocusBlankValidation("txtPAddress1")) return !1;
    if (CheckFocusBlankValidation("txtPAddress2")) return !1;
    if (CheckFocusBlankValidation("txtPPincode")) return !1;
    //if (CheckFocusBlankValidation("txtPState")) return !1;
    //if (CheckFocusBlankValidation("txtPCity")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlCityList")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlAreaList")) return !1;

    let selectedamount = 0;
    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            //proposal.periodfor = $(this).data("periodfor");
            selectedamount = $(this).data("premium");
            //proposal.discount = $(this).data("discount");
        }
    });

    if (parseFloat(selectedamount) > 49000) {
        if (CheckFocusBlankValidation("txtPPan")) return !1;
    }
    if ($("#txtPPan").hasClass("panerror")) { return false; }

    let hdnProductId = $("#hdnProductId").val();
    if (hdnProductId == 3) { if (CheckFocusBlankValidation("txtPPan")) return !1; }
    if (CheckFocusDropDownBlankValidation("ddlAnualIncome")) return !1;

    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    //proposal.productid = GetParameterValues('product');
    //proposal.companyid = GetParameterValues('companyid');
    //proposal.suminsured = GetParameterValues('suminsured');
    proposal.mstatus = $("#ddlPMStatus option:selected").val();
    proposal.title = $("#ddlPTitle option:selected").val();
    proposal.gender = proposal.title == "mr." ? "Male" : "FeMale";
    proposal.firstname = $("#txtPFirstName").val();
    proposal.lastname = $("#txtPLastName").val();
    proposal.mobile = $("#txtPMobile").val();
    proposal.emailid = $("#txtPEmail").val();
    proposal.pincode = $("#txtPPincode").val();

    proposal.dob = $("#txtPDOB").val();
    proposal.address1 = $("#txtPAddress1").val();
    proposal.address2 = $("#txtPAddress2").val();
    proposal.landmark = $("#txtPLandmark").val();
    //proposal.stateid = $("#ddlPState option:selected").val();
    proposal.statename = $("#hdnState").val();
    proposal.cityid = $("#ddlCityList option:selected").val();
    proposal.cityname = $("#ddlCityList option:selected").text();
    proposal.areaid = $("#ddlAreaList option:selected").val();
    proposal.areaname = $("#ddlAreaList option:selected").text();
    proposal.isproposerinsured = ($("#chkIsProposerInsured").prop("checked") == true ? 1 : 0);
    proposal.ispropnominee = ($("#chkIsPropNominee").prop("checked") == true ? 1 : 0);
    proposal.criticalillness = ($("#chkHavingCriticalIllness").prop("checked") == true ? 1 : 0);
    proposal.panno = $("#txtPPan").val();
    proposal.aadharno = $("#txtPAadhar").val();
    proposal.annualincome = $("#ddlAnualIncome option:selected").val();

    if (!proposal.criticalillness) {
        $("#btnNextProposerDetails").html("processing... <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/GetNSaveProposerDetailsData",
            data: '{proposal:' + JSON.stringify(proposal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null) {
                    $("#fdselectedplansection").css("display", "none");
                    $("#fdproposerdetails").css("display", "none");
                    $("#fdInsuredDetails").css("display", "block");
                    $(".linext3").addClass("active");
                    BindInsuredDetails(proposal.isproposerinsured);
                }
            }
        });
    }
    else {
        alert("If any insured have critical illness then you can't processed.");
    }
}

FillExistingIllness = (illnesspos) => {
    let existingillnes = $("#ddlIllness_" + illnesspos + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".ExistingIllness_" + illnesspos).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".ExistingIllness_" + illnesspos).addClass("notshow").css("display", "none");
        $("#txtExistingIllness_" + illnesspos).val("");
    }
}

FillEngageManualLabour = (mlabour) => {
    let existingillnes = $("#ddlEngageManualLabour_" + mlabour + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageManualLabour_" + mlabour).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageManualLabour_" + mlabour).addClass("notshow").css("display", "none");
        $("#txtEngageManualLabour_" + mlabour).val("");
    }
}

FillEngageWinterSports = (wsport) => {
    let existingillnes = $("#ddlEngageWinterSports_" + wsport + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageWinterSports_" + wsport).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageWinterSports_" + wsport).addClass("notshow").css("display", "none");
        $("#txtEngageWinterSports_" + wsport).val("");
    }
}

function NextInsuredDetails() {

    for (var i = 1; i <= parseInt(totalperson); i++) {
        if (CheckFocusBlankValidation("ddlRelation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlTitle_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInFirstName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInLastName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInDOB_" + i)) return !1;
        if ($("#txtInDOB_" + i).hasClass("errorpage")) { return false; }
        if (CheckFocusBlankValidation("txtInHeight_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInWeight_" + i)) return !1;

        if (CheckFocusBlankValidation("ddlOccupation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlIllness_" + i)) return !1;

        let existingillnes = $("#ddlIllness_" + i + " option:selected").val();
        if (existingillnes == "true") { if (CheckFocusBlankValidation("txtExistingIllness_" + i)) return !1; }

        let ismlabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        if (ismlabour == "true") { if (CheckFocusBlankValidation("txtEngageManualLabour_" + i)) return !1; }

        let iswsport = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        if (iswsport == "true") { if (CheckFocusBlankValidation("txtEngageWinterSports_" + i)) return !1; }

        if (CheckFocusBlankValidation("ddlEngageManualLabour_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlEngageWinterSports_" + i)) return !1;
    }

    if (CheckFocusDropDownBlankValidation("ddlnRelation")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlnTitle")) return !1;
    if (CheckFocusBlankValidation("nFirstName")) return !1;
    if (CheckFocusBlankValidation("nLastName")) return !1;
    if (CheckFocusBlankValidation("nDOB")) return !1;
    if ($("#nDOB").hasClass("errorpage")) { return false; }

    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');

    var insuredlist = [];

    for (var i = 1; i <= parseInt(totalperson); i++) {
        let insured = {};

        insured.id = $("#hdnInsuredId_" + i).val();
        insured.relationid = $("#ddlRelation_" + i + " option:selected").val();
        insured.relationname = $("#ddlRelation_" + i + " option:selected").text();
        insured.title = $("#ddlTitle_" + i + " option:selected").val();
        insured.firstname = $("#txtInFirstName_" + i).val();
        insured.lastname = $("#txtInLastName_" + i).val();
        insured.dob = $("#txtInDOB_" + i).val();
        insured.height = $("#txtInHeight_" + i).val();
        insured.weight = $("#txtInWeight_" + i).val();

        insured.occupationid = $("#ddlOccupation_" + i + " option:selected").val();
        insured.occupation = $("#ddlOccupation_" + i + " option:selected").text();
        insured.isillness = $("#ddlIllness_" + i + " option:selected").val();
        insured.illnessdesc = $("#txtExistingIllness_" + i).val();

        insured.isengagemanuallabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        insured.engageManualLabourDesc = $("#txtEngageManualLabour_" + i).val();

        insured.isengagewintersports = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        insured.engageWinterSportDesc = $("#txtEngageWinterSports_" + i).val();

        insured.isinsured = (i == 1 ? true : false);

        insuredlist.push(insured);
    }

    proposal.InsuredDetails = insuredlist;
    proposal.nRelation = $("#ddlnRelation option:selected").val();
    proposal.nTitle = $("#ddlnTitle option:selected").val();
    proposal.nFirstName = $("#nFirstName").val();
    proposal.nLastName = $("#nLastName").val();
    proposal.nDOB = $("#nDOB").val();

    let nBdate = proposal.nDOB.split('/');
    nBdate = new Date(nBdate[2] + "-" + nBdate[1] + "-" + nBdate[0])

    proposal.nAge = Math.floor((new Date() - nBdate) / 31557600000);


    $("#btnInsuredNext").html("processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/GetNSaveInsuredDetail",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#divMedicalHistoryDetail").html(response);
            }
            else {
                $("#divMedicalHistoryDetail").html("No medical history found!");
            }
            $("#fdselectedplansection").css("display", "none");
            $("#fdproposerdetails").css("display", "none");
            $("#fdInsuredDetails").css("display", "none");
            $("#fdMedicalHistory").css("display", "block");
            $(".linext4").addClass("active");
            $("#btnInsuredNext").html("NEXT");
        }
    });
}

$(".previous3").click(function () {
    $("#fdMedicalHistory").css("display", "none");
    $(".linext4").removeClass("active");
    $("#fdInsuredDetails").css("display", "block");
});

var totalperson = 1;
function BindInsuredDetails(isproposerinsured) {
    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    //proposal.productid = GetParameterValues('product');
    //proposal.companyid = GetParameterValues('companyid');
    //proposal.suminsured = GetParameterValues('suminsured');
    //proposal.companyid = companyid;
    //proposal.policyfor = policyfor;
    proposal.isproposerinsured = isproposerinsured;

    $.ajax({
        type: "Post",
        url: "/Health/BindInsuredDetails",
        async: false,
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#fdInsuredDetails").html(response[0]);
            totalperson = response[1];

            $('.DynamicCommanDateWithYr').datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                changeMonth: true,
                changeYear: true,
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true,
                //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
                yearRange: '1922:' + (new Date).getFullYear()
            });

            $("#btnNextProposerDetails").html("NEXT");

            CalculateNomineeAge($("#nDOB"), 'ndobvalid');
        }
    });



}

function GoToSelectedPlan() {
    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    //proposal.productid = GetParameterValues('product');
    //proposal.companyid = GetParameterValues('companyid');
    //proposal.suminsured = GetParameterValues('suminsured');
    GetT_Health_Proposal(proposal, "selectedplan");
}

function GoToProposerDetail() {
    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');
    //proposal.productid = GetParameterValues('product');
    //proposal.companyid = GetParameterValues('companyid');
    //proposal.suminsured = GetParameterValues('suminsured');
    GetT_Health_Proposal(proposal, "proposer");
    $("#fdInsuredDetails").css("display", "none");
    $(".linext3").removeClass("active");
    $("#fdproposerdetails").css("display", "block");
}

function GoToInsured() {
    let insured = {};
    //insured.enquiryid = GetParameterValues('enquiry_id');
    //insured.productid = GetParameterValues('product');
    //insured.companyid = GetParameterValues('companyid');
    //insured.relationid = GetParameterValues('companyid');
    //insured.relationname = GetParameterValues('companyid');
    //insured.title = GetParameterValues('companyid');
    //insured.firstname = GetParameterValues('companyid');
    //insured.lastname = GetParameterValues('companyid');
    //insured.dob = GetParameterValues('companyid');
    //insured.height = GetParameterValues('companyid');
    //insured.weight = GetParameterValues('companyid');
    //insured.isinsured = GetParameterValues('companyid');
}

function GetT_Health_Proposal(proposal, type) {
    $.ajax({
        type: "Post",
        url: "/Health/GetT_Health_Proposal",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                AssignTypeWiseDetail(response, type);
            }
        }
    });
}

function AssignTypeWiseDetail(proposal, type) {
    if (type == "selectedplan") {
        //GetDataByProductId(proposal.enquiryid, proposal.productid, proposal.companyid, proposal.suminsured);
    }
    else if (type == "proposer") {
        $("#ddlPGender").val(proposal.gender);;
        $("#txtPFirstName").val(proposal.firstname);
        $("#txtPLastName").val(proposal.lastname);
        $("#txtPMobile").val(proposal.mobile);
        $("#txtPEmail").val(proposal.emailid);
        $("#txtPPincode").val(proposal.pincode);
        $("#txtPDOB").val(proposal.dob);
        $("#txtPAddress1").val(proposal.address1);
        $("#txtPAddress2").val(proposal.address2);
        $("#txtPLandmark").val(proposal.landmark);
        $("#ddlPState").val(proposal.stateid);
        $("#ddlPCity").val(proposal.cityid);
    }
}

RemoveHtmlTags = (strhtml) => {
    if (strhtml != "" && strhtml != undefined) {
        var strsyntax = /<(\w+)[^>]*>.*<\/\1>/gi;
        var somtag = /\r?\n|\r/g;
        strhtml = strhtml.toString().replace(strsyntax, "").replace(somtag, "");
    }
    return strhtml;
}

function PerposalLastSubmit() {
    //window.location.href = "/health/proposalsummary";

    let QuestionCount = $("#hdnQuestionCount").val();
    let QuestionList = [];
    if (QuestionCount != "0" && QuestionCount != undefined) {
        let startquestionstr = true; let tempinsuredcount = "0";
        let questionstr = "";
        let insuredname = [];
        $(".mainquestionar").each(function () {
            let insuredcount = $(this).data("insuredcount");
            let quescode = RemoveHtmlTags($(this).data("quescode"));
            let repotype = $(this).data("repotype");
            let isrequired = $(this).data("isrequired");
            let quessetcode = $(this).data("quessetcode").replace(" ", "_");
            let questiondesc = $("#divMainQuestion_" + insuredcount + "_" + quescode).html();
            let subquestionexist = $("#divMainQuestion_" + insuredcount + "_" + quescode).data("subquestionexist");

            if (tempinsuredcount == insuredcount) {
                if (startquestionstr) {
                    questionstr = "[";
                    startquestionstr = false;
                }
            }

            if (tempinsuredcount != insuredcount) {
                insuredname = $("#hdnInsuredName_" + tempinsuredcount).val().split(',');
                let question = {};
                question.enquiryid = GetParameterValues('enquiry_id');
                question.firstname = insuredname[0];
                question.lastnane = insuredname[1];

                questionstr = questionstr.slice(0, -1) + "]";
                //console.log(questionstr);
                //questionstrList.push(questionstr);
                question.questionjson = questionstr;
                QuestionList.push(question);

                tempinsuredcount = insuredcount;
                questionstr = "";
                questionstr = "[";
            }
            else {
                insuredname = $("#hdnInsuredName_" + insuredcount).val().split(',');
            }

            if (repotype == "boolean") {
                if ($(this).prop("checked") == true) {
                    questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"true\",";
                }
                else {
                    questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"false\",";
                }

                //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount);
            }
            else if (repotype == "date") {
                let thisdateval = $("#dateQues_" + quescode).val();
                questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"" + thisdateval + "\",";
                //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) +"},";
            }
            else if (repotype == "integer") {
                let thisintval = $("#intQues_" + quescode).val();
                questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"" + thisintval + "\",";
                //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
            }
            else if (repotype == "free text") {
                let thisstrval = $("#strQues_" + quescode).val();
                questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"" + questionstr + "\",";
                //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
            }

            questionstr = questionstr + "\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[" + MakeSubQuestion2(quessetcode, quescode, insuredcount) + "]},";
        });

        let question = {};
        question.enquiryid = GetParameterValues('enquiry_id');
        question.firstname = insuredname[0];
        question.lastnane = insuredname[1];

        question.questionjson = questionstr.slice(0, -1) + "]";
        QuestionList.push(question);

        //questionstrList.push(questionstr.slice(0, -1) + "]");

        //questionstr = questionstr.slice(0, -1) + "]";
        //console.log(questionstr);
        //questionstrList.push(questionstr);
    }
    $("#btnPerposalLastSubmit").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/CreateHealthPerposal",
        data: '{enquiry_id:' + JSON.stringify(GetParameterValues('enquiry_id')) + ',medhistory:' + JSON.stringify(QuestionList) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#divPerposalModal").html(response);
                $("#btnPerposalLastSubmit").html("Submit");
                $("#PMButton").click();
            }
            else {
                $("#divPerposalModal").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger' style='word-break: break-word;'>There is some error occurred, Please try again later.</p></div>");
                $("#btnPerposalLastSubmit").html("Submit");
                $("#PMButton").click();
            }
        }
    });
}

CheckPerposalDetailSummary = () => {
    window.location.href = "/health/proposalsummary?enquiry_id=" + GetParameterValues("enquiry_id");
}

function MakeSubQuestion2(quessetcode, quescode, insuredcount) {
    let subquestionstr = "";
    $(".subquestionfield_" + insuredcount + "_" + quescode).each(function () {
        let subquestioncode = $(this).data("subquestioncode");
        let repotype = $(this).data("repotype").toLowerCase();
        let sub2exist = $(this).data("sub2exist");
        let isrequired = $(this).data("isrequired");
        let subdesc = $(this).data("subdesc");
        //if (subdesc == "Since how long does the applicant smoke") {
        //    alert("tghus");
        //    let kk = "";
        //}
        let subquestionsetcode = $(this).data("subquestionsetcode");

        if (repotype == "boolean") {
            if ($(this).prop("checked") == true) {
                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"true\",\"required\": \"Yes\",\"dropDownData\": [],\"subQuestion\":[]},";
            }
            else {
                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"false\",\"required\": \"Yes\",\"dropDownData\": [],\"subQuestion\":[]},";
            }
        }
        else if (repotype == "dropdown") {
            //let thisdateval = $(this + " option:selected").val();
            let thisdateval = ""; let subquecode = "";
            if (subquestioncode == null || subquestioncode == "") {
                subquecode = subquestionsetcode.replace(" ", "_");
            }
            else {
                subquecode = subquestioncode;
            }
            thisdateval = $("#" + insuredcount + "_" + quessetcode + "_" + $.trim(subquecode) + " option:selected").val();
            if (thisdateval == "Select") { thisdateval = ""; }
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisdateval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [" + MakeDropdownQuestion(quessetcode, insuredcount, subquestioncode, subquestionsetcode) + "],\"subQuestion\":[]},";
        }
        else if (repotype == "year") {
            let thisdateval = $(this).val();
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisdateval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[]},";
        }
        else if (repotype == "datetime") {
            let thisdateval = $(this).val();
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisdateval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[]},";
        }
        else if (repotype == "date") {
            let thisdateval = $(this).val();
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisdateval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[]},";
            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) +"},";
        }
        else if (repotype == "integer") {
            let thisintval = $(this).val();
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisintval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[]},";
            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
        }
        else if (repotype == "free text") {
            let thisstrval = $(this).val();
            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisstrval + "\",\"required\": \"" + isrequired + "\",\"dropDownData\": [],\"subQuestion\":[]},";
            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
        }
    });

    return subquestionstr.slice(0, -1);
}

function MakeDropdownQuestion(quessetcode, insuredcount, subquecode, subquesetcode) {
    let subdropquestionstr = "";
    if (subquecode == null || subquecode == "") {
        subquecode = subquesetcode.replace(" ", "_");
    }

    let currid = (insuredcount + "_" + quessetcode + "_" + $.trim(subquecode) + " option");
    $("#" + currid).each(function () {
        var thisOptionValue = $(this).val();
        var thisOptionText = $(this).text();
        if (thisOptionText != "Select") { subdropquestionstr = subdropquestionstr + "{ \"text\": \"" + thisOptionText + "\", \"value\": \"" + thisOptionValue + "\" },"; }
    });
    return subdropquestionstr.slice(0, -1);
}

//function MakeSubQuestion(quessetcode, insuredcount) {
//    let subquestionstr = "";

//    $(".subquestionfield_" + insuredcount + "_" + quessetcode).each(function () {
//        let subquestioncode = $(this).data("subquestioncode");
//        let repotype = $(this).data("repotype");
//        let sub2exist = $(this).data("sub2exist");
//        let subdesc = $(this).data("subdesc");

//        if (repotype == "boolean") {
//            if ($(this).prop("checked") == true) {
//                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"true\",\"subQuestion\":[]},";
//            }
//            else {
//                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"false\",\"subQuestion\":[]},";
//            }
//        }
//        else if (repotype == "date") {
//            let thisdateval = $(this).val();
//            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisdateval + "\",\"subQuestion\":[]},";
//            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) +"},";
//        }
//        else if (repotype == "integer") {
//            let thisintval = $(this).val();
//            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisintval + "\",\"subQuestion\":[]},";
//            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
//        }
//        else if (repotype == "free text") {
//            let thisstrval = $(this).val();
//            subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"" + thisstrval + "\",\"subQuestion\":[]},";
//            //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount) + "},";
//        }
//    });

//    return subquestionstr.slice(0, -1);
//}



CalculateAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        //var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]),
        //    end = new Date(),
        //    nowYear = end.getFullYear(),
        //    pastYear = start.getFullYear(),
        //    diff = new Date(end - start),
        //    days = diff / 1000 / 60 / 60 / 24,
        //    age = nowYear - pastYear;
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notpdobvalid").html("<span style='position: absolute;top: 10px;right: 15px;'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notpdobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

//CheckAndGetDetails = () => {
//    $(".notpincodevalid").addClass("hidden").html("");
//    var txtPinCodeVal = $("#txtPPincode").val();

//    if (txtPinCodeVal.length != 6) {
//        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
//        $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
//        $("#txtPState").val("");
//        $("#txtPCity").val("");
//    }
//    else {
//        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
//        $.ajax({
//            type: "Post",
//            url: "/Moter/CheckAndGetDetails",
//            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
//            contentType: "application/json; charset=utf-8",
//            datatype: "json",
//            success: function (response) {
//                if (response != null && response.length > 0) {
//                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response[1]);
//                    $("#txtPState").val(response[0]);
//                    $("#txtPCity").val(response[1]);
//                    $("#txtPPincode").addClass("successpincode").removeClass("errorpincode");
//                }
//                else {
//                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
//                    $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
//                }
//            }
//        });
//    }
//}

CheckAndGetDetails = (pincode) => {
    $(".notpincodevalid").addClass("hidden").html("");

    if (pincode.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Moter/CheckAndGetDetails",
            data: '{pincode: ' + JSON.stringify(pincode) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 0) {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> verified - " + response[1]);
                    $("#hdnState").val(response[0]);
                    $("#txtPPincode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
                    $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

CalculateChildNomineeAge = (fid, errorclass, errorstyle) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age <= 25) {
            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must below 25 Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

CalculateNomineeAge = (fid, errorclass, errorstyle) => {
    //let startdate = $(fid).val();
    //if (startdate != "") {
    //    let splitstartdate = startdate.split('/');
    //    var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]),
    //        end = new Date(),
    //        nowYear = end.getFullYear(),
    //        pastYear = start.getFullYear(),
    //        diff = new Date(end - start),
    //        days = diff / 1000 / 60 / 60 / 24,
    //        age = nowYear - pastYear;
    //    if (age >= 18) {
    //        $(fid).removeClass("errorpage");
    //        $("." + errorclass).html("<i class='fa fa-check-circle'></i> " + age + " Years").css("color", "rgb(0, 163, 0)");

    //        if (errorclass === 'ndobvalid') {
    //            $("#btnInsuredNext").removeAttr('disabled');
    //            $("#btnInsuredNext").css('opacity', 1);
    //        }
    //    }
    //    else {
    //        $(fid).addClass("errorpage");
    //        $("." + errorclass).html("<i class='fa fa-question-circle text-danger' data-toggle='tooltip' title='Age should be grater then 18 years!'></i>").css("color", "red");

    //        if (errorclass === 'ndobvalid') {
    //            $("#btnInsuredNext").attr('disabled');
    //            $("#btnInsuredNext").css('opacity', 0.3);
    //        }
    //    }
    //}

    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        //var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]),
        //    end = new Date(),
        //    nowYear = end.getFullYear(),
        //    pastYear = start.getFullYear(),
        //    diff = new Date(end - start),
        //    days = diff / 1000 / 60 / 60 / 24,
        //    age = nowYear - pastYear;
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

$(document.body).on('click', ".mainquestionar", function (e) {
    let qscode = $(this).data("quescode");
    let insuredcount = $(this).data("insuredcount");

    if ($(this).prop("checked") == true) {
        $(".DivSubQuestionSection_" + insuredcount + "_" + qscode).css("display", "block");
    }
    else {
        $(".DivSubQuestionSection_" + insuredcount + "_" + qscode).css("display", "none");
    }
});

InsuredTitleChange = (currIndex) => {
    let currTitle = $("#ddlRelation_" + currIndex + " option:selected").data("title");
    $("#ddlTitle_" + currIndex).val(currTitle);
}

//$(document.body).on('click', ".questiondate", function (e) {
//    $(".ui-datepicker-calendar").css("display", "none");
//});

CheckHeight = (thidid) => {
    let heightval = $("#txtInHeight_" + thidid).val();

    $(".notheight_" + thidid).html("").css("display", "none");
    if (heightval != "" && heightval != undefined) {
        let heightintval = parseInt(heightval);
        if (heightintval > 9 && heightintval <= 999) {
            removeErrorClass("txtInHeight_" + thidid);
        }
        else {
            addFocusErrorClass("txtInHeight_" + thidid, '');
            $(".notheight_" + thidid).html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 180px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Height must be 10+</span>").css("display", "block");
            $("#txtInHeight_" + thidid).val("");
            return true;
        }
    }
    return true;
}

CheckWeight = (thidid) => {
    let heightval = $("#txtInWeight_" + thidid).val();

    $(".notweight_" + thidid).html("").css("display", "none");
    if (heightval != "" && heightval != undefined) {
        let heightintval = parseInt(heightval);
        if (heightintval > 0 && heightintval <= 999) {
            removeErrorClass("txtInWeight_" + thidid);
        }
        else {
            addFocusErrorClass("txtInWeight_" + thidid, '');
            $(".notweight_" + thidid).html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Weight must be bove 0 kg</span>").css("display", "block");
            $("#txtInWeight_" + thidid).val("");
            return true;
        }
    }
    return true;
}

CheckPanNumber = () => {
    $(".notp_pan").html("").css("display", "none");
    $("#txtPPan").removeClass("panerror");

    let panVal = $("#txtPPan").val();
    if (panVal != "" && panVal != undefined) {
        //var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        var regpan = /[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
        if (regpan.test(panVal)) { return true; }
        else {
            $("#txtPPan").addClass("panerror");
            $(".notp_pan").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Permanent Account Number is not yet valid.</span>").css("display", "block");
            return false;
        }
    }
}

CheckEmailAddress = () => {
    $(".notp_email").html("").css("display", "none");
    $("#txtPEmail").removeClass("emailerror");

    let panVal = $("#txtPEmail").val();
    if (panVal != "" && panVal != undefined) {
        //var regpan = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        var regpan = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regpan.test(panVal)) { return true; }
        else {
            $("#txtPEmail").addClass("emailerror");
            $(".notp_email").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Email address is not yet valid.</span>").css("display", "block");
            return false;
        }
    }
}

CheckMobileNumber = (mid, errorclas) => {
    let mobno = $("#" + mid).val();
    $("#" + mid).removeClass("pmoberror");
    if (mobno.length == 10) {
        let regx = /^[6-9]{1}[0-9]{9}$/;
        var valid = regx.test(mobno);
        if (valid) {
            $("." + errorclas).html("<span style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-check-circle'></i> Valid</span>").css("color", "rgb(0, 163, 0)");
        }
        else {
            $("#" + mid).addClass("pmoberror");
            $("." + errorclas).html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Mobile number not valid.</span>");
        }
    }
    else {
        $("#" + mid).addClass("pmoberror");
        $("." + errorclas).html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Mobile number not valid.</span>");
    }
}

AddLessAddon = (addOnsId) => {

    $(".AddonClick").click();
    let addOns = $("#chkAddon_" + addOnsId).data("addons");
    let code = $("#chkAddon_" + addOnsId).data("code");
    let value = $("#chkAddon_" + addOnsId).data("value");
    let calculation = $("#chkAddon_" + addOnsId).data("calculation");
    let addOnValue = $("#chkAddon_" + addOnsId).data("addonvalue");
    let productDetailID = $("#chkAddon_" + addOnsId).data("productdetailid");
    let inquiryid = $("#chkAddon_" + addOnsId).data("inquiryid");
    let status = $("#chkAddon_" + addOnsId).prop("checked") == true ? 1 : 0;
    let period = $("#chkAddon_" + addOnsId).data("period");

    let basePremium = "";// $("#chkAddon_" + addOnsId).data("basepremium");
    let premium = "";// $("#chkAddon_" + addOnsId).data("premium");
    let discountPercent = "";// $("#chkAddon_" + addOnsId).data("discountpercent");
    let serviceTax = "";// $("#chkAddon_" + addOnsId).data("servicetax");
    let totalPremium = "";// $("#chkAddon_" + addOnsId).data("totalpremium");

    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            //proposal.periodfor = $(this).data("periodfor");
            basePremium = premium = $(this).data("basepremium");
            discountPercent = $(this).data("discount");
            serviceTax = $(this).data("servicetax");
            totalPremium = $(this).data("premium");
        }
    });

    let addonlist = [addOnsId, addOns, code, value, calculation, addOnValue, productDetailID, inquiryid, status, basePremium, premium, discountPercent, serviceTax, totalPremium, period];
    $("#divPremiumBreakup").html('<p class="text-center" style="margin-left: 23%;">Please wait,<br>We are fetching details... <i class="fa fa-spinner fa-pulse"></i></p>');
    $.ajax({
        type: "Post",
        url: "/Health/AddLessAddon",
        data: '{addonlist:' + JSON.stringify(addonlist) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            setTimeout(function () { $("#divPremiumBreakup").html(response); $("#AddonClose").click(); }, 500);
        }
    });
}