﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(function () {
    let enquiryid = GetParameterValues('enquiry_id');
    GetproposalAllDetails(enquiryid);
});

GetproposalAllDetails = (enquiryid) => {
    if (enquiryid != "" && enquiryid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Health/GetproposalAllDetails",
            //data: '{enquiry_id:' + JSON.stringify(enquiryid) + ',productid: ' + JSON.stringify(productid) + ',companyid: ' + JSON.stringify(companyid) + ',suminsured: ' + JSON.stringify(suminsured) + '}',
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != "") {
                    if (response[0] != "logout") {
                        setTimeout(function () {
                            //$("#Additional").html(response[0]);
                            $("#ProposerDetail").html(response[1]);
                            $("#AddonsDetail").html(response[2]);
                            $("#Insured").html(response[3]);
                            $("#Medical").html(response[4]);
                            $("#Other").html(response[5]);
                            $("#divProductSummDetails").html(response[6]);
                            //$("#divPaymentSection").html(response[6]);
                            $("#lblProposalId").html("Enquiry Id : " + enquiryid.toUpperCase() + " &nbsp;/&nbsp; Proposal Id : " + response[7]);
                        }, 500);
                    }
                    else {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information - Unauthorized access</h6></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You not authorized to access this page or details.</h6></div><div class='modal-footer'><button type='button' class='btn btn-danger' id='btnUnauthorizedClose' onclick='UnauthorizedAccess();'>OK</button></div>");
                        $("#PStatusModal").modal('show');
                    }
                }
                else {
                    $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information - Unauthorized access</h6></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You not authorized to access this page or details.</h6></div><div class='modal-footer'><button type='button' class='btn btn-danger' id='btnUnauthorizedClose' onclick='UnauthorizedAccess();'>OK</button></div>");
                    $("#PStatusModal").modal('show');
                }
            }
        });
    }
}

UnauthorizedAccess = () => { $("#btnUnauthorizedClose").html("<i class='fa fa-pulse fa-spinner'></i>"); window.location.href = "https://seeinsured.com/"; }

RedirectPaymentUrl = () => {
    let enquiryid = GetParameterValues('enquiry_id');
    if (enquiryid != "" && enquiryid != undefined) {
        $(".fapayment").removeClass("fa-credit-card").addClass("fa-pulse fa-spinner");
        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');

        $.ajax({
            type: "Post",
            url: "/Health/GenratePaymentUrl",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response == "paid") {
                    setTimeout(function () {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-success'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-success'>This payment has already been paid.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }, 1000);
                }
                else if (response != "" && response != "paid" && response != undefined) {
                    setTimeout(function () {
                        window.open(response);
                        $(".close").click();
                    }, 1500);
                }
                else {
                    alert("Unable to generate payment url, please contect to administrator.");
                }
                $(".fapayment").addClass("fa-credit-card").removeClass("fa-pulse fa-spinner");
            }
        });
    }
}

CheckPaymentStatusDetails = () => {
    let enquiryid = GetParameterValues('enquiry_id');
    if (enquiryid != "" && enquiryid != undefined) {
        $(".fapaymentstatus").removeClass("fa-rupee").addClass("fa-pulse fa-spinner");

        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');

        $.ajax({
            type: "Post",
            url: "/Health/CheckHealthPaymentDetails",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                setTimeout(function () {
                    if (response == "notpay") {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You haven't payment yet, Please payment first.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                    else if (response != "failed" && response != "notpay") {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-success'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Payment Success</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'>" + response + "</div>");
                        //$("#PStatusModal").modal('show');
                    }
                    else {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                }, 1000);
                $(".fapaymentstatus").addClass("fa-rupee").removeClass("fa-pulse fa-spinner");
            }
        });
    }
}

GetHealthPdfDocument = () => {
    let enquiryid = GetParameterValues('enquiry_id');
    if (enquiryid != "" && enquiryid != undefined) {
        $(".fadownloadpdf").removeClass("fa-download").addClass("fa-pulse fa-spinner");
        //&times;
        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');

        $.ajax({
            type: "Post",
            url: "/Health/GetHealthPdfDocument",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $(".fadownloadpdf").addClass("fa-download").removeClass("fa-pulse fa-spinner");
                setTimeout(function () {
                    if (data != null) {
                        if (data == "notpay") {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You haven't payment yet, Please payment first.</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                        else if (data == "failed") {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Failed to generate Policy pdf, Please contact to administrator</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                        else if (data != "" && data != "notpay" && data != "failed") {
                            $(".close").click();

                            var a = $("<a/>");
                            a.attr("download", (enquiryid + ".pdf"));
                            a.attr("href", data);
                            $("body").append(a);
                            a[0].click();
                            $("body").remove(a);
                        }
                        else {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                    }
                    else {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                }, 1000);
            }
        });
    }
}