﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

var policy_type = null; var sum_insured = null; var self = null; var selfage = null; var spouse = null; var spouseage = null; var son = null; var son1age = null; var son2age = null; var son3age = null;
var son4age = null; var daughter = null; var daughter1age = null; var daughter2age = null; var daughter3age = null; var daughter4age = null; var father = null; var fatherage = null; var mother = null;
var motherage = null; var gender = null; var first_name = null; var last_name = null; var email_id = null; var mobile_no = null; var pin_code = null; var policy_type_text = null;
var total_Adults = 0; var total_Childs = 0;

function FirstLetterCap(para) { return para.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); }); }

$(function () {
    GetUrlDetails(GetParameterValues('enquiry_id'));
});



function GetUrlDetails(enqid) {
    if (enqid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetUrlDetails",
            data: '{enqid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != "") {
                    if (response == "redirectenq") {
                        window.location.href = "/health/helthsearch";
                    }
                    else {
                        var urlDetails = response;

                        policy_type = GetCustomParameterValues(urlDetails, 'policy_type'); sum_insured = GetCustomParameterValues(urlDetails, 'sum_insured'); self = (GetCustomParameterValues(urlDetails, 'self') == 'true' ? true : false);
                        selfage = GetCustomParameterValues(urlDetails, 'selfage'); spouse = (GetCustomParameterValues(urlDetails, 'spouse') == 'true' ? true : false); spouseage = GetCustomParameterValues(urlDetails, 'spouseage');
                        son = (GetCustomParameterValues(urlDetails, 'son') == 'true' ? true : false); son1age = GetCustomParameterValues(urlDetails, 'son1age'); son2age = GetCustomParameterValues(urlDetails, 'son2age');
                        son3age = GetCustomParameterValues(urlDetails, 'son3age'); son4age = GetCustomParameterValues(urlDetails, 'son4age'); daughter = (GetCustomParameterValues(urlDetails, 'daughter') == 'true' ? true : false);
                        daughter1age = GetCustomParameterValues(urlDetails, 'daughter1age'); daughter2age = GetCustomParameterValues(urlDetails, 'daughter2age'); daughter3age = GetCustomParameterValues(urlDetails, 'daughter3age');
                        daughter4age = GetCustomParameterValues(urlDetails, 'daughter4age'); father = (GetCustomParameterValues(urlDetails, 'father') == 'true' ? true : false); fatherage = GetCustomParameterValues(urlDetails, 'fatherage');
                        mother = (GetCustomParameterValues(urlDetails, 'mother') == 'true' ? true : false); motherage = GetCustomParameterValues(urlDetails, 'motherage'); gender = GetCustomParameterValues(urlDetails, 'gender');
                        first_name = GetCustomParameterValues(urlDetails, 'first_name'); last_name = GetCustomParameterValues(urlDetails, 'last_name'); email_id = GetCustomParameterValues(urlDetails, 'email_id');
                        mobile_no = GetCustomParameterValues(urlDetails, 'mobile_no'); pin_code = GetCustomParameterValues(urlDetails, 'pin_code'); policy_type_text = FirstLetterCap(decodeURI(GetCustomParameterValues(urlDetails, 'policy_type_text')));

                        if (self) { total_Adults = total_Adults + 1; } if (spouse) { total_Adults = total_Adults + 1; } if (father) { total_Adults = total_Adults + 1; } if (mother) { total_Adults = total_Adults + 1; }
                        if (son) { if (parseInt(son1age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(son2age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(son3age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(son4age) > 0) { total_Childs = total_Childs + 1; } }
                        if (daughter) { if (parseInt(daughter1age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(daughter2age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(daughter3age) > 0) { total_Childs = total_Childs + 1; } if (parseInt(daughter4age) > 0) { total_Childs = total_Childs + 1; } }
                       
                        $("#lblPolicyFor").html(policy_type_text);
                        //$("#lblAdults").html(total_Adults);
                        //$("#lblChilds").html(total_Childs);
                        $("#lblPinCode").html(pin_code);
                        $("#lblSumInsured").html("₹ " + sum_insured.replace('-', ' - ₹ '));

                        $("#tdBuyPolicyFor").html(policy_type_text);
                        $("#tdPolicyPlan").html(FirstLetterCap(policy_type));
                        $("#tdMember").html(total_Adults + ' Adults, ' + total_Childs + ' Childs');
                        $("#tdSumInsured").html(sum_insured.replace('-', ' - '));
                        $("#tdPrimaryInsuredGender").html(FirstLetterCap(gender));
                        $("#tdPrimaryInsuredPincode").html(pin_code);

                        //BindSumInsured("ddlSumInsured", sum_insured);
                        FetchHealthApiListing(enqid, total_Adults, total_Childs);
                    }
                }
                else {
                    $("#btnGetQuotes").html("GET QUOTES");
                }
            }
        });
    }
}

function BindSumInsured(dropdownid, suminsured) {
    $("#" + dropdownid).html("");
    var splitsuminsured = suminsured.split(',');
    $("#" + dropdownid).append("<option value=''>Select</option>");
    for (var i = 0; i < splitsuminsured.length; i++) {
        $("#" + dropdownid).append("<option value='" + splitsuminsured[i] + "'>₹ " + splitsuminsured[i] + " </option>");
    }
}

function BindProvider(dropdownid, suminsured) {
    var splitsuminsured = suminsured.split(',');
    var providerHtml = "";
    for (var i = 0; i < splitsuminsured.length; i++) {
        var dest = splitsuminsured[i].split(" ").join("");
        providerHtml = providerHtml + "<label class='seeingo' style='color: #777777!important;'>" + splitsuminsured[i] + "<input type='checkbox' value='" + splitsuminsured[i] + "' class='submenuclass chkfilterprovider' id='chkProvider_" + dest + "' name='chkProvider_" + dest + "'><span class='checkmark'></span></label>";
    }
    $("#" + dropdownid).append(providerHtml);
    $("#divFilterProvider").removeClass("hidden");
    /*https://localhost:44338/health/helthsearchlist?enquiry_id=2020111126701*/
}

function GetCustomParameterValues(url, param) {
    var url = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function FetchHealthApiListing(enqid, adult, child) {
    if (enqid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/FetchHealthApiListing",
            data: '{enqid: ' + JSON.stringify(enqid) + ',adult: ' + JSON.stringify(adult) + ',child: ' + JSON.stringify(child) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#divHealthProductList").html("");
                if (response != null && response.length > 0) {
                    $("#divHealthProductList").html(response[0]);
                    BindPriceRangeSection(parseInt(response[1]), parseInt(response[2]));
                    BindTenureRangeSection("ddlTenure", response[3]);
                    $("#lblProductCount").html(response[6] + " Records");
                    BindSumInsured("ddlSumInsured", response[4]);
                    //BindProvider("ddlProvider", response[5]);

                    BindProvider("divInnerProvider", response[5]);
                    //$("#divFilterProvider").removeClass("hidden");
                    $("#divFilterSumInsured").removeClass("hidden");
                    $("#divFilterPremium").removeClass("hidden");
                    $("#divFilterTenure").removeClass("hidden");
                }
                else {
                    $("#lblProductCount").html("0 Records");
                }
            }
        });
    }
}

function BindTenureRangeSection(dropdownid, tenure) {
    if (tenure != null && tenure != "") {
        var tenureSplt = tenure.split('-');
        for (var i = 1; i <= parseInt(tenureSplt[1]); i++) {
            $("#" + dropdownid).append("<option value=" + i + ">" + i + " Years</option>");
        }
        $("#divFilterTenure").removeClass("hidden");
    }
}

function BindPriceRangeSection(minval, maxval) {
    $("#minRangeSlide").html("₹ " + minval);
    $("#maxRangeSlide").html("₹ " + maxval);

    var minRangeSlide = $("#minRangeSlide");
    var maxRangeSlide = $("#maxRangeSlide");

    $("#sliderprice").slider({
        range: true,
        min: minval,
        max: maxval,
        values: [minval, maxval],
        step: 100,
        slide: function (event, ui) {
            minRangeSlide.html('₹ ' + ui.values[0]);
            maxRangeSlide.html('₹ ' + ui.values[1]);
        },
        change: function (event, ui) {
            PriceFilter(ui.values[0], ui.values[1]);
        }
    });

    $("#divFilterPremium").removeClass("hidden");
}

$(document.body).on('click', ".chech", function (e) {
    $(".comp").removeClass("hidden");
});

$(document.body).on('click', ".bookbtn", function (e) {
    let productid = $(this).data("productid");
    let suminsured = $(this).data("suminsured");
    let suminsuredid = $(this).data("suminsuredid");
    let companyid = $(this).data("companyid");
    let adult = $(this).data("adult");
    let child = $(this).data("child");
    let planname = $(this).data("planname");
    let totalamt = $(this).data("totalamt");
    let thiscount = $(this).data("thiscount");
    let tenure = $(this).data("tenure");

    if (thiscount != "") {
        $(".bookbtn").each(function () { $(this).attr("disabled", true); });
        $("#premiumCount" + thiscount).html("Processing..<i class='fa fa-spinner fa-pulse'></i>").css({ "background": "#262566" });
        let urlpara = "enquiry_id=" + GetParameterValues('enquiry_id') + "&product=" + productid + "&companyid=" + companyid + "&suminsuredid=" + suminsuredid + "&suminsured=" + suminsured + "&adult=" + adult + "&child=" + child + "&planname=" + planname + "&totalamt=" + totalamt + "&tenure=" + tenure;
        $.ajax({
            type: "Post",
            url: "/Health/InsertSearchUrlParaDetail",
            data: '{enquiryid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + ',urlpara:' + JSON.stringify(urlpara) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                //window.open("/health/proposaldetails?enquiry_id=" + GetParameterValues('enquiry_id'));
                window.location.href = "/health/proposaldetails?enquiry_id=" + GetParameterValues('enquiry_id');
            }
        });
    }
    //window.open("/health/proposaldetails?enquiry_id=" + GetParameterValues('enquiry_id') + "&product=" + productid + "&companyid=" + companyid + "&suminsured=" + suminsured + "&adult=" + adult + "&child=" + child);
});

var model = {}
model.EnquiryId = GetParameterValues('enquiry_id');
function PriceFilter(min, max) {
    model.MinPrice = min;
    model.MaxPrice = max;
    //model.Type = "price";
    FinalFilterAllCategory(model);
}

$(document.body).on('change', "#ddlTenure", function (e) {
    model.Tenure = $(this).val();
    FinalFilterAllCategory(model);
});

$(document.body).on('change', "#ddlSumInsured", function (e) {
    model.SumInsured = $(this).val();
    FinalFilterAllCategory(model);
});

$(document.body).on('click', ".chkfilterprovider", function (e) {
    var providerlist = [];
    $(".chkfilterprovider").each(function (index, element) { if ($(this).prop("checked") == true) { providerlist.push($(this).val()); } });
    model.Provider = providerlist;
    FinalFilterAllCategory(model);
});

function FinalFilterAllCategory(paramodel) {
    $(".FilterProcessModalClick").click();
    $.ajax({
        type: "Post",
        url: "/Health/FilterSection",
        data: '{filter: ' + JSON.stringify(paramodel) + ',adult: ' + JSON.stringify(total_Adults) + ',child: ' + JSON.stringify(total_Childs) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#divHealthProductList").html("");
            if (response != "") {
                if (response.length == 2) {
                    if (response[0] != "") {
                        $("#divHealthProductList").html(response[0]);
                    }
                    else {
                        $("#divHealthProductList").html("<h2 style='text-align: center;margin-top: 10%;'>No matching results found !</h2>");
                    }
                    $("#lblProductCount").html(response[1] + " Records");
                }
                else {
                    if (response[0] != "") {
                        $("#divHealthProductList").html(response[0]);
                    }
                    else {
                        $("#divHealthProductList").html("<h2 style='text-align: center;margin-top: 10%;'>No matching results found !</h2>");
                    }
                    $("#lblProductCount").html(response[6] + " Records");
                }
            }
            else {
                $("#divHealthProductList").html("<h2 style='text-align: center;margin-top: 10%;'>No matching results found !</h2>");
                $("#lblProductCount").html("0 Records");
            }
            $(window).scrollTop(0);
            setTimeout(function () {
                $("#ProcessIdClose").click();
            }, 3000);
        }
    });
}

var checked_HIP = 0;
var compDetailCount = [];

function CompareCheck(element, dataCount) {

    if ($(element).prop("checked") === true) {
        checked_HIP += 1;
        if (checked_HIP > 3) {
            checked_HIP = 3;
            $(element).prop("checked", false);
            return alert("You cannot select more than 3 products.\nPlease clear all or deselect one of them");
        }
        compDetailCount.push(dataCount);
    }
    else {
        checked_HIP -= 1;
        if (checked_HIP === 0) {
            $("#compare_div").addClass("hidden");
        }
        compDetailCount = compDetailCount.filter(item => item !== dataCount)
    }
    $("#compCount").html(checked_HIP);
}

let clearCompSelection = () => {
    $(".healthInsuranceProducts").each(function () {
        $(this).prop("checked", false);
    });
    checked_HIP = 0;
    $("#compCount").html(checked_HIP);
    $("#compare_div").addClass("hidden");
    compDetailCount = [];
}

$("#btn_compareProducts").click(function () {

    if (checked_HIP === 1) {
        alert("Please select atleast two products.");
        return false;
    }
    else {
        $("#comp-modalContent").html("<p class='text-center' style='font-size: 25px;margin: 60px;'>We are comparing products based on the information you have selected. <i class='fa fa-spinner fa-pulse'></i></p>");
        let compDetailArray = [];
        for (let i = 0; i < compDetailCount.length; i++) {
            var compDetail = {};
            compDetail.sumInsured = $("#compData" + compDetailCount[i]).data("sum_insured");
            compDetail.plan = ($("#compData" + compDetailCount[i]).data("plan") == undefined || $("#compData" + compDetailCount[i]).data("plan") == "") ? "" : $("#compData" + compDetailCount[i]).data("plan");
            compDetail.companyId = $("#compData" + compDetailCount[i]).data("companyid");
            compDetail.productid = $("#compData" + compDetailCount[i]).data("productid");
            compDetail.cover = $("#coverCount" + compDetailCount[i]).html();
            compDetail.premium = $("#premiumCount" + compDetailCount[i]).html();
            compDetailArray.push(compDetail);
        }

        $.ajax({
            type: "Post",
            url: "/Health/CompareInsuranceProducts",
            data: '{compReqArray: ' + JSON.stringify(compDetailArray) + ',enqId: ' + JSON.stringify(GetParameterValues("enquiry_id")) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#comp-modalContent").html(data);
            }
        });
    }



});


$(document.body).on('click', "#btn_Email", function (e) {
    $("#img_quote").addClass('hidden');
    $(".mailform").removeClass('hidden');
});
$(document.body).on('click', "#compareemai", function (e) {

    $(".sendbtn").addClass('hidden');
    $(".temsend").removeClass('hidden');
    var EmailAddress = $("#mailaddress").val();
    if (checked_HIP === 1) {
        alert("Please select atleast two products.");
        return false;
    }
    else {

        let compDetailArray = [];
        for (let i = 0; i < compDetailCount.length; i++) {
            var compDetail = {};
            compDetail.sumInsured = $("#compData" + compDetailCount[i]).data("sum_insured");
            compDetail.plan = ($("#compData" + compDetailCount[i]).data("plan") == undefined || $("#compData" + compDetailCount[i]).data("plan") == "") ? "" : $("#compData" + compDetailCount[i]).data("plan");
            compDetail.companyId = $("#compData" + compDetailCount[i]).data("companyid");
            compDetail.productid = $("#compData" + compDetailCount[i]).data("productid");
            compDetail.cover = $("#coverCount" + compDetailCount[i]).html();
            compDetail.premium = $("#premiumCount" + compDetailCount[i]).html();
            compDetailArray.push(compDetail);
        }

        $.ajax({
            type: "Post",
            url: "/Health/CompareInsuranceProductsEmail",
            data: '{compReqArray: ' + JSON.stringify(compDetailArray) + ',enqId: ' + JSON.stringify(GetParameterValues("enquiry_id")) + ',toemail: ' + JSON.stringify(EmailAddress) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#img_quote").removeClass('hidden');
                $(".sendmail").addClass('hidden');
                $(".mailform").addClass('hidden');
                $(".temsend").addClass('hidden');
                $(".sendbtn").removeClass('hidden');
                EmailAddress.val("");
            }
        });
    }



});

//$(document.body).on('click', ".tablinks", function (e) {
//    $(".tablinks").each(function () { $(this).removeClass("active"); });    
//    $(this).addClass("active");
//    var tabname = $(this).data("tabname");
//    var catname = $(this).data("catname");
//    $(".tabcontent").each(function () { $(this).attr("display", "none;"); });
//    $("#" + catname).attr("display", "block;");
//});

var clickedseedetailid = "";
$(document.body).on('click', ".btnseedelt", function (e) {
    var thiscount = $(this).data("thiscount");
    //$(".tabcontent").css("display", "none;");   
    //$(".tablinks").removeClass("active");

    //$("#USP_" + thiscount).css("display", "block;");
    $("#TabLinkId_USP" + thiscount).addClass("active");
    $("#USP_" + thiscount).css("display", "block;");
    $("#TabLinkId_USP" + thiscount).click();

    //$(".tablinks").each(function () {
    //    var tabidname = $(this).data("tabidname");
    //    if (tabidname == ("USP_" + thiscount)) {
    //        $(this).addClass("active");
    //    }
    //    else {
    //        $(this).removeClass("active");
    //    }
    //});
});

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}