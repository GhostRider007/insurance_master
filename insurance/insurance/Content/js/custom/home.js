﻿

function AgencyLoginProcess() {
    $("#LoginErrorMsg").html("");
    var thisbutton = $("#btnAgencyLogin");

    if (CheckFocusBlankValidation("txtAgencyUserId")) return !1;
    if (CheckFocusBlankValidation("txtAgencyPassword")) return !1;

    var userid = $("#txtAgencyUserId").val();
    var password = $("#txtAgencyPassword").val();

    $(thisbutton).html("<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Administration/AgencyLogin",
        data: '{userid: ' + JSON.stringify(userid) + ',password: ' + JSON.stringify(password) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "true") {
                    window.location.href = data[1];
                }
                else {
                    $("#LoginErrorMsg").html(data[1]);
                    $(thisbutton).html("Login").prop('disabled', false);
                }
            }            
        },
        failure: function (data) {
            alert("failed");
        }
    });
}


function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor, modaldialogmarginname, modaldialogmargin) {
    $(".popupheading").css("color", headcolor).html(headerHeading);
    $(".popupcontent").css("color", bodycolor).html(bodyMsg);
    $("#modaldialogid").attr(modaldialogmarginname, modaldialogmargin);
    $(".popupmessage").click();
}