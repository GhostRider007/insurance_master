﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}

function ValidateEmail(email) {
    //var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //var emailReg = new RegExp(/^[^<>()[\]\\,;:\%#^\s@\"$&!@]+@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z0-9]+\.)+[a-zA-Z]{2,}))$/gm);
    //var emailReg=/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var valid = emailReg.test(email);
    if (!valid) { return false; }
    else { return true; }
}

function addErrorClass(i, msg) {
    removeErrorClass(i);
    $("#" + i).closest('div.form-validation').addClass('has-error');
    $("#" + i).closest('div.form-validation').append("<span class='help-error-block pull-right' title='This field is required.'><i class='fa fa-star'></i>&nbsp;" + msg + "</span>");
    $("#" + i).focus();
}

function addFocusErrorClass(i, msg) {
    removeErrorClass(i);
    $("#" + i).closest('div.form-validation').addClass('has-error');
    $("#" + i).focus();
}

function removeErrorClass(i) {
    $("#" + i).closest('div.form-validation').removeClass('has-error');
    $("#" + i).closest('div.form-validation').find('span.help-error-block').remove();
}

function CheckBlankValidation(i) {
    if ($("#" + i).val().trim() === "") {
        addErrorClass(i, 'This field is required.');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckNumberBlankValidation(i) {
    if ($("#" + i).val().trim() == "0") {
        addErrorClass(i, 'This field is required.');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckDropDownBlankValidation(i) {
    if ($("#" + i + " option:selected").val() == "" || $("#" + i + " option:selected").val() == "0") {
        addErrorClass(i, 'This field is required.');
        return true;
    }
    else { removeErrorClass(i); }
}

function BlockSpecialChar(e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}
//----------------Start------------Validation without message-----------------------------------------------
function NotExistFocusBlankValidation(i) {
    addFocusErrorClass(i, '');
    return true;
}

function CheckFocusBlankValidation(i) {
    if ($("#" + i).val() == "") {
        addFocusErrorClass(i, '');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckFocusDropDownBlankValidation(i) {
    if ($("#" + i + " option:selected").val() == "" /*|| $("#" + i + " option:selected").val().trim() == "0"*/) {
        addFocusErrorClass(i, '');
        return true;
    }
    else { removeErrorClass(i); }
}
//----------------Start------------Validation without message Cancellation Page-----------------------------------------------

function CheckFocusCancellationBlankValidation(i) {
    if ($("#" + i).val().trim() == "") {
        $("#" + i).css("border-bottom", "");
        $("#" + i).css("border", "1px solid red");
        $("#" + i).focus();
        return true;
    }
    else { $("#" + i).css("border", ""); }
}

//----------------EndCanellation------------Validation without message-------------------------------------------------

//----------------Start------------Validation without message Checkout Page-----------------------------------------------

function CheckFocusChekoutBlankValidation(i) {
    if ($("#" + i).val().trim() == "") {
        $("#" + i).css("border-bottom", "");
        $("#" + i).css("border-bottom", "1px solid red");
        $("#" + i).focus();
        return true;
    }
    else { $("#" + i).css("border-bottom", ""); }
}

//----------------End------------Validation without message-------------------------------------------------

function CheckEmailValidatoin(i) {
    if (!ValidateEmail($("#" + i).val().trim())) {
        addErrorClass(i, 'Email is not valid.');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckBoxCheckedValidation(i) {
    if (!$("#" + i).is(':checked')) {
        addErrorClass(i, 'You must agree to it.');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckRadioButtonListCheckedValidation(i) {
    var radiolistchecked = '0';
    $('#' + i + ' input[type="radio"]').each(function () {
        if ($(this).is(':checked')) {
            radiolistchecked = '1';
        }
    });

    if (radiolistchecked == '0') {
        addErrorClass(i, 'You must select one of it.');
        return true;
    }
    else { removeErrorClass(i); }
}

function CheckSamePasswordValidation(i, t) {
    if ($("#" + i).val() != $("#" + t).val()) {
        addErrorClass(t, 'Not same as password you have entered.');
        return true;
    }
    else { removeErrorClass(t); }
}

function noSpaceValidation(evt, t) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 32) {
        addErrorClass($(t).attr('id'), 'This field can\'t contain space');
        return false;
    }
    removeErrorClass($(t).attr('id'));
    return true;
}

function isNumberValidation(evt, t) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57))) {
        addErrorClass($(t).attr('id'), 'Enter numeric values only');
        return false;
    }
    removeErrorClass($(t).attr('id'));
    return true;
}

function isNumberValidationPrevent(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57))) {
        return false;
    }
    return true;
}

function isStringValidationPrevent(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (!((charCode >= 97 && charCode <= 122) || (charCode >= 65 && charCode <= 90))) {
        return false;
    }
    return true;
}

function IsAlphabet(e) {
    var regex = new RegExp(/^[a-zA-Z\s]+$/);
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    else {
        e.preventDefault();
        return false;
    }
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57))) {
        alert('Accept only numeric values');
        return false;
    }
    return true;
}

function isNumberKeyWithSpace(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && charCode != 32 && (charCode < 48 || charCode > 57)) {
        alert('Accept only numeric values');
        return false;
    }
    return true;
}

function isDecimalOnlyKey(event, e) {
    if ((event.which != 46 || $(e).val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
}

function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
}

$(function () {
    if ($(".edit").length > 0) {
        $(".edit").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "768px", helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } }, afterClose: function () { parent.location.reload(!0) } });
    }
    if ($(".edit2").length > 0) {
        $(".edit2").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "768px", helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } }, afterClose: function () { parent.location.reload(!1) } });
    }
    if ($(".openclose").length > 0) {
        $(".openclose").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "768px", helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } } });
    }
    if ($(".login_popup").length > 0) {
        $(".login_popup").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "400px", helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } } });
    }
    if ($(".open400").length > 0) {
        $(".open400").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "400px", helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } } });
    }
    if ($(".reviewopenclose").length > 0) {
        $(".reviewopenclose").fancybox({ openEffect: "elastic", closeEffect: "elastic", prevEffect: "fade", nextEffect: "fade", fitToView: !0, maxWidth: "1000px", "autoSize": false, helpers: { overlay: { css: { background: "transparent", filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#F22a2a2a,endColorstr=#F22a2a2a)", zoom: "1", background: "rgba(0, 0, 0, 0.5)" } } } });
    }
});

function openPopUp(theURL, winName, features) {
    mapWin = window.open(theURL, winName, features);
    mapWin.focus();
}

function validateNumbersOnly(input, kbEvent) {
    var keyCode, keyChar;
    keyCode = kbEvent.keyCode;

    if (window.event) {
        keyCode = kbEvent.keyCode; // IE
    }
    else {
        keyCode = kbEvent.which; //firefox
    }

    if (keyCode == null) { return true };
    // get character

    keyChar = String.fromCharCode(keyCode);
    var charSet = "0123456789. ";
    // check valid chars
    if (charSet.indexOf(keyChar) != -1) { return true };
    // control keys

    if (keyCode > 31 && keyCode != 32 && (keyCode < 48 || keyCode > 57)) {
        alert('Accept only numeric values');
        return false;
    }

    return true;
}

function isNumberOnlyKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && charCode != 32 && (charCode < 48 || charCode > 57)) {
        alert('Accept only numeric values');
        return false;
    }
    return true;
}

function isNumberOnlyKeyNoDot(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && charCode != 32 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isNumberOnlyKeyNoDotNoSpace(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(".intval").keypress(function (event) {
    if (((event.which != 46 || (event.which == 46 && $(this).val() == '')) ||
        $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
}).on('paste', function (event) {
    event.preventDefault();
});

function isAlphaNumeric(event) {
    var regex = new RegExp("^[a-zA-Z0-9\b]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    return true;
}

function isAlphaNumericWithSpace(event) {
    var regex = new RegExp(/^[a-z\d\-_\s]+$/i);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    return true;
}

function onBeginRequest(sender, args) {
    var popuploading = $(".loadingPopUp");
    if (popuploading != null) {
        var modalloading = $('<div/>');
        modalloading.addClass("loadingmodal");
        $('body').append(modalloading);
        popuploading.show();
        var toploading = Math.max($(window).height() / 2 - popuploading[0].offsetHeight / 2, 0);
        var leftloading = Math.max($(window).width() / 2 - popuploading[0].offsetWidth / 2, 0);
        popuploading.css({ top: toploading, left: leftloading });
    }
}

function onEndRequest(sender, args) {

    var popuploading = $('.loadingPopUp');
    if (popuploading != null) {
        $('.loadingmodal').remove();
        popuploading.hide();
    }

    $(document).ready(function () {

        bindAllDropDown();
        bindListing();
        bindmyTab();
    });
}

//Start: For Checking form input for black value and setting style accordinglly.
$(function () {
    function CheckFormLineText() {
        $('.formLine input,.formLine textarea').each(function () {
            tmpval = $(this).val();
            if (tmpval != '') {
                $(this).addClass('noempty');
            }
            else {
                $(this).removeClass('noempty');
            }
        });
    }
    $('.formLine input,.formLine textarea').blur(function () {
        CheckFormLineText();
    });
    CheckFormLineText();
});
//End: For Checking form input for black value and setting style accordinglly.


function AlphaNumericOnly(e) {
    var keyCode = e.keyCode || e.which;
    var regex = /^[A-Za-z0-9]+$/;
    var isValid = regex.test(String.fromCharCode(keyCode));
    if (!isValid) {
        return isValid;
    }
    return isValid;
}

function CheckPasswordWithCombination(textid) {
    let password = $("#" + textid).val();
    if (password != "") {
        var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
        var isValid = regex.test(password);
        if (!isValid) {
            return isValid;
        }
        return isValid;
    }
}