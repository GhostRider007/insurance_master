﻿//$('.CommanDateYEAR').monthpicker({
//    pattern: 'yyyy-mm',
//    selectedYear: (new Date).getFullYear(),
//    startYear: 2005,
//    finalYear: (new Date).getFullYear()
//});
//var options = {
//    selectedYear: (new Date).getFullYear(),
//    startYear: 2005,
//    finalYear: (new Date).getFullYear(),
//    openOnFocus: false // Let's now use a button to show the widget
//};

//﻿$('.CommanDateYEAR').monthpicker({
//    pattern: 'yyyy-mm',
//    selectedYear: 2015,
//    startYear: 1900,
//    finalYear: 2212    
//});
//var options = {
//    selectedYear: 2015,
//    startYear: 2008,
//    finalYear: 2018,    
//    openOnFocus: false // Let's now use a button to show the widget
//};


$('.CommanDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});

$('.CommanDateMMYY').datepicker({
    dateFormat: "MM YYYY",
    showStatus: true,
    showWeeks: false,
    currentText: 'Now',
    autoSize: true,
    changeMonth: true,
    changeYear: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: false

});
$('.offlineDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    minDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});

$('.blockbackdates').datepicker({
    dateFormat: "dd-mm-yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    minDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true
});


$('.CommmanAllDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});

$('.CommanDateWithYr').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true,
    //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
    yearRange: '1922:' + (new Date).getFullYear()
});

//$('.MonthYearOnly').datepicker({
//    dateFormat: 'mm/yy',
//    changeMonth: true,
//    changeYear: true,
//    showButtonPanel: true,

//    onClose: function (dateText, inst) {
//        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
//        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
//        $(this).datepicker('setDate', new Date(year, month, 1));
//    }
//});

//$(".MonthYearOnly").focus(function () {
//    $(".ui-datepicker-calendar").hide();
//    $("#ui-datepicker-div").position({
//        my: "center top",
//        at: "center bottom",
//        of: $(this)
//    });

//});

$('.datewithnoresf').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true,
    //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
    //yearRange: '1950:' + (new Date).getFullYear()
});

$('.notshowbeforedate').datepicker({
    dateFormat: "dd/mm/yy",
    minDate: 0,
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true
    //yearRange: '2021:' + (new Date).getFullYear()
});


$(function () {
    $(".FromDate").attr("placeholder", "Select Starting Date");
    $(".ToDate").attr("placeholder", "Select Starting Date First");
    $(".ToDate").attr("disabled", true);
});

$('.FromDate').datepicker({
    dateFormat: "dd/mm/yy",
    minDate: null,
    maxDate: null,
    showAnim: "slideDown",
    numberOfMonths: 1,
    changeYear: true,
    changeMonth: true,
    showOtherMonths: true,
    showButtonPanel: true,
    currentText: "Today",
    onClose: function () {
        if ($(".FromDate").val() !== "") {
            $(".ToDate").removeAttr("disabled");
            $(".ToDate").attr("placeholder", "Select End Date");
            var minDate = $(".FromDate").datepicker("getDate");
            $('.ToDate').datepicker("option", "minDate", new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate() + 1));

            //var toDate = $(".FromDate").datepicker("getDate");
            //$('.ToDate').datepicker("setDate", new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate() + 1 ));
        }
        else {
            $(".ToDate").attr("placeholder", "Select Starting Date First");
            $(".ToDate").attr("disabled", true);
        }
    }

});

$('.ToDate').datepicker({
    dateFormat: "dd/mm/yy",
    maxDate: null,
    showAnim: "slideDown",
    numberOfMonths: 1,
    changeYear: true,
    changeMonth: true,
    showOtherMonths: true,
    showButtonPanel: true,
});


$('.CommanDateYEAR').monthpicker({ pattern: 'yyyy-mm', selectedYear: (new Date).getFullYear(), startYear: 2005, finalYear: (new Date).getFullYear() });

$('.DateMMYYYY').monthpicker({ pattern: 'MM-YYYY', selectedYear: (new Date).getFullYear(), startYear: 2005, finalYear: (new Date).getFullYear() });