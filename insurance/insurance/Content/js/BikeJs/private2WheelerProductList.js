﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

function GetCustomParameterValues(url, param) {
    var url = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    BindMotorProductList(GetParameterValues('enquiry_id'));
});


BindMotorProductList = (enqid) => {
    $.ajax({
        type: "Post",
        url: "/Bike/GetBikeRegDetails",
        data: '{enqid: ' + JSON.stringify(enqid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response.length > 0) {
                $("#divProductLeftSection").html(response[0]);
                if (response[1] != "") { $("#divProductList").html(response[1]); }
                else { $("#divProductList").html("<div class='text-center'><img src='/Content/images/responsenotfound.png' style='width: 50%;'/></div>"); }
                $("#lblProductCount").html(response[2] + " Records");
            }
            else {
                //$("#divCarProductList").html(" <p class='text-center' style='font-size: 25px;margin-top: 60px;'>We are unable to fetching quotes based on the information you provided.</p>");
                $("#divProductList").html("<div class='text-center'><img src='/Content/images/responsenotfound.png' style='width: 50%;'/></div>");
                $("#lblProductCount").html("0 Records");
            }
        }
    });
}

ClickMotorPerposal = (loopcount) => {
    let proposalno = $("#btnMotorPerposal_" + loopcount).data("proposalno");
    let suminsured = $("#btnMotorPerposal_" + loopcount).data("suminsured");
    let price = $("#btnMotorPerposal_" + loopcount).data("price");
    let chainid = $("#btnMotorPerposal_" + loopcount).data("chainid");
    let productcode = $("#btnMotorPerposal_" + loopcount).data("productcode");
    let productname = $("#btnMotorPerposal_" + loopcount).data("product");
    let chainimage = "";
    $("#btnMotorPerposal_" + loopcount).html("<i class='fa fa-pulse fa-spinner'></i>");
    $(".bookbtnpopup_" + loopcount).html("<i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Moter/InsertAndUpdateMotoResponse",
        data: '{enquiryid:' + JSON.stringify(GetParameterValues('enquiry_id')) + ', chianid:' + JSON.stringify(chainid) + ', proposalno:' + JSON.stringify(proposalno) + ', suminsured:' + JSON.stringify(suminsured) + ',productcode:' + JSON.stringify(productcode) + ',chainimage:' + JSON.stringify(chainimage) + ',product:' + JSON.stringify(productname) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response == true) {
                window.location.href = "/bike/bikeproposaldetails?enquiry_id=" + GetParameterValues('enquiry_id');
            }
            else {
                $("#btnMotorPerposal_" + loopcount).html(price);
                $(".bookbtnpopup_" + loopcount).html(price);
            }
        }
    });
}

$(document.body).on('click', '.tablinks', function (e) {
    let evt = e;
    let cityName = $(this).data("tabname");
    openCity(evt, cityName);
});

$(document.body).on('click', '.seedetails', function (e) {
    let tabclickid = $(this).data("tabclickid");
    //$("#highlights_" + tabclickid).click();
    document.getElementById("highlights_" + tabclickid).style.display = "block";
    $(".highlights_" + tabclickid).addClass("active");
});