﻿// onload
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(function () {
    GetVehicleDetail();

    $("#reg").click();
});

$("#reg").click(function () {
    $(".regs").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$("#brandtab").click(function () {
    $(".selctcar").removeClass("hidden");
    $(".regs").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
});
$("#models").click(function () {
    $(".selectmodel").removeClass("hidden");
    $(".Variant").addClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#fueltab").click(function () {
    $(".fuel").removeClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".Variant").addClass("hidden");
    $(".selctcar").addClass("hidden");
    $(".regs").addClass("hidden");
});
$("#vari").click(function () {
    $(".Variant").removeClass("hidden");
    $(".fuelss").addClass("hidden");
    $(".selectmodel").addClass("hidden");
    $(".regs").addClass("hidden");
    $(".selctcar").addClass("hidden");
});
$(".Variantbtn").click(function () {
    $(".choseplan").removeClass("hidden");
    $(".registrationform").addClass("hidden");
});

$("#RTO_Code_A").select2();
$("#ddlYearOfMake_2Wheeler").select2();

GetVehicleDetail = () => {


    $.post("/Bike/GetBikeDetails", {
        enqid: GetParameterValues('enquiry_id')
    },
        function (data, status) {
            if (data != "") {
                $('#divSelctedSummary').append('<div class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Vehicle Type</label></div><div class="col-sm-6"><label>' + data.VechileType + '</label></div></div>');
                $('#divSelctedSummary').append('<div class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Registration No.</label></div><div class="col-sm-6"><label>' + data.VechileRegNo + '</label></div></div>');
                $('#divSelctedSummary').append('<div class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Insurance Type</label></div><div class="col-sm-6"><label>' + data.insurancetype + '</label></div></div>');
            }
            else {
                $('#divSelctedSummary').append('<div class="row borderobx">< div class= "col-sm-12" style = "border-right: 1px solid #ccc;text-align:center;color:#dc3545"><label>Details Not Available !<label></div></div >');
            }
        });
}

var bike_summary = {
    Enquiry_Id: '',
    RtoId: '',
    RtoName: '',
    DateOfReg: '',
    YearOfMake: '',
    BrandName: '',
    ModelName: '',
    Fuel: '',
    VarientName: '',
    isclaiminlastyear: '',
    policynumber: '',
    policyexpirydate: '',
    previousyearnoclaim: '',
    ispreviousinsurer: '',
    policyholder: ''
};
bike_summary.Enquiry_Id = GetParameterValues('enquiry_id');
var insuranceType = '';
//--------------------------------------------

//(function GetYearOfMake() {
//    let currdate = parseInt(new Date().getFullYear());

//    let makeofyearhtml = "<option value=''>Choose Year</option>";

//    for (let i = 0; i <= 20; i++) {
//        makeofyearhtml += "<option value='" + (currdate - i) + "'>" + (currdate - i) + "</option>";
//    }
//    $("#ddlYearOfMake_2Wheeler").html("");
//    $("#ddlYearOfMake_2Wheeler").html(makeofyearhtml);
//})();

$("#btnRegContinue_2Wheeler").click(() => {

    if (CheckFocusDropDownBlankValidation("RTO_Code_A")) return !1;
    if (CheckFocusBlankValidation("txtDateOfReg_2Wheeler")) return !1;
    if (CheckFocusBlankValidation("txtYearOfMake_2Wheeler")) return !1;

    //bike_summary.Enquiry_Id = GetParameterValues('enquiry_id');
    bike_summary.RtoId = $("#RTO_Code_A option:selected").val();
    bike_summary.RtoName = $("#RTO_Code_A option:selected").text();
    bike_summary.DateOfReg = $("#txtDateOfReg_2Wheeler").val();
    bike_summary.DateofMfg = $("#txtYearOfMake_2Wheeler").val();

    let Enquiry_Id = GetParameterValues('enquiry_id');
    let RtoId = $("#RTO_Code_A option:selected").val();
    let RtoName = $("#RTO_Code_A option:selected").text();
    let DateOfReg = $("#txtDateOfReg_2Wheeler").val();
    let DateofMfg = $("#txtYearOfMake_2Wheeler").val();

    $("#btnRegContinue_2Wheeler").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        url: "/Bike/UpdateMotorRegistration",
        data: '{enqid: ' + JSON.stringify(Enquiry_Id) + ',rtoid: ' + JSON.stringify(RtoId) + ',rtoname: ' + JSON.stringify(RtoName) + ',dateofreg: ' + JSON.stringify(DateOfReg) + ',dateofmfg: ' + JSON.stringify(DateofMfg) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != "") {
                //BindBrandDetails();
                $("#divBrandItemSection").html(data);
                $('#brandtab').removeAttr("disabled");
                $("#brandtab").click();
                $("#btnRegContinue").html("UPDATE");
            }
            else {
                alert("Something went Wrong");
            }
            $("#btnRegContinue_2Wheeler").html("CONTINUE");
        }
    });
});

function selectBrand(element) {

    $(".brandselectsec").each(function () {
        $(this).removeClass("selectedBtn");
    });
    $(element).toggleClass("selectedBtn");

    bike_summary.BrandName = $(element).html().toString(); // element.id

    //var obj = {};
    //obj.BrandName = $(element).html().toString();enquiryId

    $.ajax({
        type: "Post",
        url: "/Bike/GetBikeModelDetails",
        data: '{brandName: ' + JSON.stringify(bike_summary.BrandName) + ',enquiryId: ' + JSON.stringify(bike_summary.Enquiry_Id) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            if (data != "") {
                $("#divModelItemSection").html("");
                $("#divModelItemSection").html(data);
                // $('#brandtab').prop("disabled", true);
                $("#models").click();
                $('#models').removeAttr("disabled");
                $("#txtBrandSearch").val("");

                //for summary
                $("#brandName_summary").remove();
                $("#modelName_summary").remove();
                $("#fuel_summary").remove();
                $("#varientName_summary").remove();

                let brandName_summary = bike_summary.BrandName;
                $('#divSelctedSummary').append('<div id="brandName_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Brand</label></div><div class="col-sm-6"><label>' + brandName_summary + '</label></div></div>');
            }
            else {
                alert("Something went Wrong");
            }
        }
    });

};

function selectModel(element) {

    $(".modelselectsec").each(function () {
        $(this).removeClass("selectedBtn");
    });
    $(element).toggleClass("selectedBtn");

    bike_summary.ModelName = $(element).html().toString();

    $.ajax({
        type: "Post",
        url: "/Bike/GetBikeFuelDetails",
        data: '{brandName: ' + JSON.stringify(bike_summary.BrandName) + ',modelName: ' + JSON.stringify(bike_summary.ModelName) + ',enquiryId: ' + JSON.stringify(bike_summary.Enquiry_Id) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            if (data != "") {
                $("#divFuelItemSection").html("");
                $("#divFuelItemSection").html(data);
                $('#fueltab').removeAttr("disabled");
                $("#txtModelSearch").val("");

                //for summary
                $("#modelName_summary").remove();
                $("#fuel_summary").remove();
                $("#varientName_summary").remove();

                let modelName_summary = bike_summary.ModelName;
                $('#divSelctedSummary').append('<div id="modelName_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Model</label></div><div class="col-sm-6"><label>' + modelName_summary + '</label></div></div>');

                $("#fueltab").click();

            }
            else {
                alert("Something went Wrong");
            }
        }
    });
};

function selectFuel(element) {

    $(".fuelselectsec").each(function () {
        $(this).removeClass("selectedBtn");
    });
    $(element).toggleClass("selectedBtn");

    bike_summary.Fuel = $(element).html().toString();

    $.ajax({
        type: "Post",
        url: "/Bike/GetBikeVariantDetails",
        data: '{brandName: ' + JSON.stringify(bike_summary.BrandName) + ',modelName: ' + JSON.stringify(bike_summary.ModelName) + ',fuelType: ' + JSON.stringify(bike_summary.Fuel) + ',enquiryId: ' + JSON.stringify(bike_summary.Enquiry_Id) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            if (data != "") {
                $("#divVarientItemSection").html("");
                $("#divVarientItemSection").html(data);
                $('#vari').removeAttr("disabled");
                $("#vari").click();

                //for summary
                $("#fuel_summary").remove();
                $("#varientName_summary").remove();

                let fuel_summary = bike_summary.Fuel;
                $('#divSelctedSummary').append('<div id="fuel_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Fuel</label></div><div class="col-sm-6"><label>' + fuel_summary + '</label></div></div>');
            }
            else {
                alert("Something went Wrong");
            }
        }
    });
};

//$("#ddlIsPreviousInsurerKnown").change(function(){
//    if ($(this.selectedOptions[0]).val() === "" || $(this.selectedOptions[0]).val() === "no") {
//        $("#divHavePreviousInsurerKnown").css("display", "none");
//        $("#btnBikeGetSearch").css("opacity", "0.3").attr("disabled",true);
//    }
//    else {
//        $("#btnBikeGetSearch").css("opacity", "1").removeAttr("disabled");
//        $("#divHavePreviousInsurerKnown").css("display", "block");
//    }

//});

function selectVarient(element) {
    $(".varientselectsec").each(function () {
        $(this).removeClass("selectedBtn");
    });
    $(element).toggleClass("selectedBtn");

    bike_summary.VarientName = $(element).html().toString();
    $("#ddlIsPreviousInsurerKnown").val("").change();

    $.ajax({
        type: "Post",
        url: "/Bike/UpdateBikeVarientDetails",
        data: '{varientName: ' + JSON.stringify(bike_summary.VarientName) + ',enquiryId: ' + JSON.stringify(bike_summary.Enquiry_Id) + '}',
        contentType: "application/json; charset=utf-8",
        async: false,
        datatype: "json",
        success: function (data) {
            if (data != "") {

                $("#otherdel").removeAttr("disabled");
                $("#txtVariantSearch").val("");


                //for summary
                $("#varientName_summary").remove();
                let varientName_summary = bike_summary.VarientName;
                $('#divSelctedSummary').append('<div id="varientName_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Varient</label></div><div class="col-sm-6"><label>' + varientName_summary + '</label></div></div>');

                $("#otherdel").click();

                $.ajax({
                    type: "Post",
                    url: "/Bike/GetValuntaryAndPreviousInsured",
                    data: '{enqid: ' + JSON.stringify(GetParameterValues('enquiry_id')) + '}',

                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (data) {
                        if (data != null) {

                            //insuranceType = data[4];
                            //if (insuranceType.toLowerCase() === 'fresh') {
                            //    $("#divPreviousInsurerKnown").css("display", "flex");
                            //    $("#ddlIsPreviousInsurerKnown").attr("disabled", "disabled");
                            //    $("#ddlIsPreviousInsurerKnown").html("<option value='no' selected >No</option>");
                            //    $("#btnBikeGetSearch").css("opacity", "1").removeAttr("disabled");
                            //}
                            //else {
                            //    $("#divPreviousInsurerKnown").css("display", "none");
                            //}

                            //if (data[3] !== "" && data[3] !== undefined) {
                            //    $("#ddlPreviousPolicyType").html(data[3]);
                            //}
                            //else {
                            //    $("#ddlPreviousPolicyType").html("<option value='-1' selected >No record Found</option>");
                            //}
                            if (data[4] == "Renewal") {
                                $("#divPreviousInsurerKnown").css("display", "flex");
                                $("#ddlPreviousPolicyType").html(data[3]);
                                $("#divPreviousPolicyType").css("display", "flex");
                                $("#divPreviousPolicyType").addClass("renewal");
                                $("#divPolicyHolderType").css("display", "none");
                            }
                            else {
                                $("#divPreviousInsurerKnown").css("display", "none");
                                $("#ddlPreviousPolicyType").html("");
                                $("#divPreviousPolicyType").removeClass("renewal");
                                $("#divPreviousPolicyType").css("display", "none");
                                $("#divPolicyHolderType").css("display", "flex");
                            }
                        }
                    }
                });

            }
            else {
                alert("Something went Wrong");
            }
        }
    });
};

function UpdateBikeOtherDetails() {
    if ($("#divPreviousPolicyType").hasClass("renewal")) {
        if (CheckFocusDropDownBlankValidation("ddlPreviousPolicyType")) return !1;
        if (CheckFocusDropDownBlankValidation("ddlIsPreviousInsurerKnown")) return !1;
        if (CheckFocusBlankValidation("txtPolicyExpDate")) return !1;

        bike_summary.ispreviousinsurer = $("#ddlIsPreviousInsurerKnown option:selected").val();
        bike_summary.policyholder = $("#ddlPreviousPolicyType option:selected").val();

        bike_summary.isclaiminlastyear = "";
        bike_summary.policynumber = "";
        bike_summary.policyexpirydate = "";
        bike_summary.previousyearnoclaim = "";

        if (bike_summary.ispreviousinsurer == "yes") {
            if (CheckFocusBlankValidation("txtPolicyExpDate")) return !1;
            if (CheckFocusDropDownBlankValidation("ddlClaimInLastYear")) return !1;

            bike_summary.isclaiminlastyear = $("#ddlClaimInLastYear option:selected").val();
            if (bike_summary.isclaiminlastyear == "no") {
                if (CheckFocusDropDownBlankValidation("ddlPreviousNoClaimBonus")) return !1;
            }

            bike_summary.policynumber = $("#txtPolicyNumber").val();
            bike_summary.policyexpirydate = $("#txtPolicyExpDate").val();

            bike_summary.previousyearnoclaim = "0";

            if (bike_summary.isclaiminlastyear == "no") {
                bike_summary.previousyearnoclaim = $("#ddlPreviousNoClaimBonus option:selected").val();
            }
        }


        $("#btnBikeGetSearch").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            url: "/Bike/UpdateBikeOtherDetails",
            data: '{isprevious: ' + JSON.stringify(bike_summary.ispreviousinsurer) + ',policynumber: ' + JSON.stringify(bike_summary.policynumber) + ',expdate: ' + JSON.stringify(bike_summary.policyexpirydate) + ',lastyr: ' + JSON.stringify(bike_summary.isclaiminlastyear) + ',noclaim: ' + JSON.stringify(bike_summary.previousyearnoclaim) + ',enqid: ' + JSON.stringify(bike_summary.Enquiry_Id) + ',policyholder: ' + JSON.stringify(bike_summary.policyholder) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            success: function (response) {
                if (response == true) {
                    window.location.href = "/bike/bikeSearchList?enquiry_id=" + GetParameterValues('enquiry_id');
                    //$.post("/Bike/bikeSearchList", bike_summary);
                }
                else {
                    $("#btnBikeGetSearch").html("GET SEARCH");
                    alert("Something went Wrong");
                }                
            }
        });
    }
    else {
        if (CheckFocusDropDownBlankValidation("ddlPolicyHolderType")) return !1;
        //if (CheckFocusDropDownBlankValidation("ddlIsPreviousInsurerKnown")) return !1;
        bike_summary.ispreviousinsurer = "no"; //$("#ddlIsPreviousInsurerKnown option:selected").val();
        let phtype = $("#ddlPolicyHolderType option:selected").val();
        $("#btnBikeGetSearch").html("Processing... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            url: "/Bike/UpdateBikeOtherDetails",
            data: '{isprevious: ' + JSON.stringify(bike_summary.ispreviousinsurer) + ',policynumber: "",expdate: "",lastyr: "",noclaim: "",enqid: ' + JSON.stringify(bike_summary.Enquiry_Id) + ',policyholder: "",phtype: ' + JSON.stringify(phtype) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            async: false,
            success: function (response) {
                if (response == true) {
                    window.location.href = "/bike/bikeSearchList?enquiry_id=" + GetParameterValues('enquiry_id');
                    //$.post("/Bike/bikeSearchList", bike_summary);
                }
                else {
                    alert("Something went Wrong");
                }
                $("#btnBikeGetSearch").html("GET SEARCH");
            }
        });
    }
};

$("#ddlIsPreviousInsurerKnown").change(function () {
    let thisval = $(this).val();
    if (thisval == "yes") {
        $("#divHavePreviousInsurerKnown").css("display", "block");
    }
    else {
        $("#ddlPreviousInsurerCode").val("").change();
        $("#txtPolicyNumber").val("");
        $("#txtPolicyExpDate").val("");
        $("#ddlClaimInLastYear").val("").change();
        $("#ddlPreviousNoClaimBonus").val("").change();
        $("#divHavePreviousInsurerKnown").css("display", "none");
    }
});

$("#otherdel").click(function () {
    $.ajax({
        type: "Post",
        url: "/Moter/GetNCBPercentage",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                $("#ddlPreviousNoClaimBonus").html("");
                $("#ddlPreviousNoClaimBonus").append("<option value=''>Select</option>");
                $(response).each(function (i, item) {
                    $("#ddlPreviousNoClaimBonus").append("<option value='" + item.Value + "'>" + item.Value + " %</option>");
                });
            }
        }
    });
});


$("#ddlClaimInLastYear").change(function () {

    let thisval = $(this).val().toLowerCase();
    if (thisval == "no") {
        $("#NoClaimBonus").css("display", "block");
    }
    else {
        $("#ddlPreviousNoClaimBonus").val("").change();
        $("#NoClaimBonus").css("display", "none");
    }

});

//summary detail section
$("#RTO_Code_A").change(() => {

    $("#rto_summary").remove();
    let rto_summary = $("#RTO_Code_A option:selected").text();
    $('#divSelctedSummary').append('<div id="rto_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>RTO</label></div><div class="col-sm-6"><label>' + rto_summary + '</label></div></div>');
});

$("#txtDateOfReg_2Wheeler").change(() => {

    $("#regDate_summary").remove();
    let regDate_summary = $("#txtDateOfReg_2Wheeler").val();
    $('#divSelctedSummary').append('<div id="regDate_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Date of Registration</label></div><div class="col-sm-6"><label>' + regDate_summary + '</label></div></div>');
});


$("#ddlYearOfMake_2Wheeler").change(() => {

    $("#yearOfMake_summary").remove();
    let yearOfMake_summary = $("#ddlYearOfMake_2Wheeler option:selected").text();
    $('#divSelctedSummary').append('<div id="yearOfMake_summary" class="row borderobx"><div class="col-sm-6" style="border-right: 1px solid #ccc;"><label>Year of Manufacture</label></div><div class="col-sm-6"><label>' + yearOfMake_summary + '</label></div></div>');
});

//Filter Section

$("#txtBrandSearch").keyup(function () {
    var brandsearchval = $(this).val().toLowerCase();
    $(".branddyn").each(function (i, item) {
        var currbrand = $(this).data('brand').toLowerCase();
        if (currbrand.includes(brandsearchval) == true) {
            $(this).attr("style", "display:block");
        }
        else {
            $(this).attr("style", "display:none");
        }
    });
});

$("#txtModelSearch").keyup(function () {
    var modelsearchval = $(this).val().toLowerCase();
    $(".modeldyn").each(function (i, item) {
        var currmodel = $(this).data('model').toLowerCase();
        if (currmodel.includes(modelsearchval) == true) {
            $(this).attr("style", "display:block");
        }
        else {
            $(this).attr("style", "display:none");
        }
    });
});