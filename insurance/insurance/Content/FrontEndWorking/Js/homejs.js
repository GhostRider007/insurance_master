﻿
$(function () {
    GetGenderDetails();
    BindHealthAges();
});

function GetGenderDetails() {
    $.ajax({
        type: "Post",
        url: "/Home/GetGender",
        //data: '{stateid: ' + JSON.stringify(null) + ',status: ' + JSON.stringify(true) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                $("#ddlGenderList").html("");
                $("#ddlGenderList").html(data);
            }
        }
    });
}
function BindHealthAges() {
    //$('#InsureAge').click();
    var option1 = '';
    option1 += '<option value="' + 0 + '">Select Age</option>';
    for (var i = 18; i <= 100; i++) {
        option1 += '<option value="' + i + '">' + i + ' Years </option>';
        if (i == 100) {
            $('#InsureAge').empty().append(option1);

        }
    }

    $('#InsureAge').empty().append(option1);

}