﻿using insurance.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Service
{
    public static class FrontAgencyService
    {
        public static List<CreateAgency> Get_AgencyDetail(string agencyName = "", string agencyId = "")
        {
            return FrontAgencyHelper.Get_AgencyDetail(agencyName, agencyId);
        }
        public static List<CreateAgency> GetAgencyList(string agencyId = null, string status = "1")
        {
            return FrontAgencyHelper.GetAgencyList(agencyId, status);
        }
        public static EmulateLogin Insert_EmulateloginDetails(EmulateLogin model)
        {
            return FrontAgencyHelper.Insert_EmulateloginDetails(model);
        }
        public static EmulateLogin Get_EmulateDetail(EmulateLogin model)
        {
            return FrontAgencyHelper.Get_EmulateDetail(model);
        }
        public static List<Country> CountryList(string countryid = null, string status = null)
        {
            return FrontAgencyHelper.CountryList(countryid, status);
        }
        public static List<BankNameModel> BankList()
        {
            return FrontAgencyHelper.BankList();
        }
        public static List<State> StateList(string stateId = null, string countryID = null, string status = null)
        {
            return FrontAgencyHelper.StateList(stateId, countryID, status);
        }
        public static List<City> CityList(string cityPin, string cityId = null, string stateID = null, string status = null, string stateName = null)
        {
            return FrontAgencyHelper.CityList(cityPin, cityId, stateID, status, stateName);
        }
        public static string CityListWithPin(string cityPin)
        {
            return FrontAgencyHelper.CityListWithPin(cityPin);
        }
        public static List<StaffModel> RefByList(string roleID = null, string status = null, string staffId = null)
        {
            return FrontAgencyHelper.RefByList(roleID, status, staffId);
        }
        public static bool ActiveAgentDetails(int? agencyid, CreateAgency model)
        {
            return FrontAgencyHelper.ActiveAgentDetails(agencyid, model);
        }

        public static bool UpdateAgencyDetail(int? agencyid, CreateAgency model)
        {
            return FrontAgencyHelper.UpdateAgencyDetail(agencyid, model);
        }
        public static int InserNewAgency(CreateAgency model)
        {
            return FrontAgencyHelper.InserNewAgency(model);
        }

        public static bool UpdateImagesAgency(int agencyID, CreateAgency model)
        {
            return FrontAgencyHelper.UpdateImagesAgency(agencyID, model);
        }

        public static bool AgencyStatusUpdator(int id, bool status)
        {
            return FrontAgencyHelper.AgencyStatusUpdator(id, status);
        }
    }
}