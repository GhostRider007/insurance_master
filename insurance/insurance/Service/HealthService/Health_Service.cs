﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.HealthApi;
using insurance.Helper.HealthHelper;
using Post_Utility.Utility;
using static insurance.Models.Health.HealthModel;

namespace insurance.Service.HealthService
{
    public static class Health_Service
    {
        public static string GenrateEnquiryId()
        {
            return UtilityClass.EnryptString("sih" + UtilityClass.GenrateEnquiryId(5, "1234567890"));
        }
        public static string DecryptGenrateEnquiryId(string enqid)
        {
            return UtilityClass.DecryptString(enqid);
        }
        public static List<SumInsuredMaster> GetSumInsuredPriceList()
        {
            return Health_Helper.GetSumInsuredPriceList();
        }
        public static bool SaveGenratedQuotes(NewEnquiry_Health quotes, string userid = "", string password = "")
        {
            return Health_Helper.SaveGenratedQuotes(quotes, userid, password);
        }
        public static NewEnquiry_Health GetNewEnquiryHealthDetails(string enqid)
        {
            return Health_Helper.GetNewEnquiryHealthDetails(enqid);
        }
        public static string CheckPinCodeExist(string pincode)
        {
            return Health_Helper.CheckPinCodeExist(pincode);
        }

        public static bool InsertSelectedPlanProposal(CreateHealth_Proposal proposal)
        {
            return Health_Helper.InsertSelectedPlanProposal(proposal);
        }
        public static bool UpdateSelectedAddons(List<AddonChecked> addonlist)
        {
            return Health_Helper.UpdateSelectedAddons(addonlist);
        }
        public static bool InsertProposerDetailsProposal(CreateHealth_Proposal proposal)
        {
            return Health_Helper.InsertProposerDetailsProposal(proposal);
        }
        public static bool ExtranetInsertProposalDetails(CreateHealth_Proposal proposal)
        {
            return Health_Helper.ExtranetInsertProposalDetails(proposal);
        }
        public static CreateHealth_Proposal GetProposalDetail(CreateHealth_Proposal proposal)
        {
            return Health_Helper.GetProposalDetail(proposal);
        }
        public static CreateHealth_Proposal GetProposalDetail(string enquiryid)
        {
            return Health_Helper.GetProposalDetail(enquiryid);
        }
        //public static bool UpdateProposalPaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status)
        //{
        //    return Health_Helper.UpdateProposalPaymentDetails(policyNumber, transactionRefNum, uwDecision, errorFlag, errorMsg, status);
        //}
        public static List<RelationMaster> GetRelation(string companyid, string productId, string policyfor)
        {
            return Health_Helper.GetRelation(companyid, productId, policyfor);
        }

        public static bool InsertSearchUrlParaDetail(string enquiryid, string urlpara, string selectedproductjson)
        {
            return Health_Helper.InsertSearchUrlParaDetail(enquiryid, urlpara, selectedproductjson);
        }

        public static SearchUrlParaMeter GetSearchUrlParaMeter(string enquiry_id)
        {
            return Health_Helper.GetSearchUrlParaMeter(enquiry_id);
        }

        public static T_Enquiry_Response GetEnquiry_Response(string enquiry_id)
        {
            return Health_Helper.GetEnquiry_Response(enquiry_id);
        }

        public static CreateHealth_Proposal GetProposalDetailsBySearchParaMeter(SearchUrlParaMeter para)
        {
            CreateHealth_Proposal proposal = new CreateHealth_Proposal();
            proposal.enquiryid = para.enquiry_id;
            proposal.productid = para.productid;
            proposal.companyid = para.companyid;
            proposal.suminsured = para.suminsured;

            return Health_Helper.GetProposalDetail(proposal);
        }
        public static void RemoveHealth_UrlParaFromCache()
        {
            Health_Helper.RemoveHealth_UrlParaFromCache();
        }

        public static bool InsertInsuredDetails(CreateHealth_Proposal proposal)
        {
            return Health_Helper.InsertInsuredDetails(proposal);
        }

        public static List<Health_Insured> GetInsuredDetails(SearchUrlParaMeter para)
        {
            return Health_Helper.GetInsuredDetails(para);
        }

        public static List<Health_Insured> GetInsuredDetail(string enquiryid)
        {
            return Health_Helper.GetInsuredDetail(enquiryid);
        }

        public static bool UpdateQuestionerDetails(string enquiryid, List<PerposalQuestion> medhistory)
        {
            return Health_Helper.UpdateQuestionerDetails(enquiryid,medhistory);
        }

        public static bool InsertPerposalDetails(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            return Health_Helper.InsertPerposalDetails(enquiry_id, referenceId, premium, totalPremium, proposalNum, productDetailId, companyId, paymenturl, servicetax);
        }
        public static Health_Payment GetHealthPaymentTableDetails(string enquiryid)
        {
            return Health_Helper.GetHealthPaymentTableDetails(enquiryid);
        }
        public static bool Create_PaymentLinkByProposal(string userId, string password, string agencyID, string enquiryid)
        {
            string paymenturl = string.Empty;
            try
            {
                Health_Payment payment = GetHealthPaymentTableDetails(enquiryid);
                if (!payment.payment_status)
                {
                    if (!string.IsNullOrEmpty(payment.pp_paymenturl))
                    {
                        paymenturl = payment.pp_paymenturl;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(payment.pp_rowpaymenturl))
                        {
                            paymenturl = HealthAPIHelper.GenrateHealthPaymentUrl(userId, password, agencyID, payment.pp_rowpaymenturl, enquiryid, payment.pp_companyId);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(paymenturl))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        public static string GetOccupationDdl(string compId, string occupationId)
        {
            DataTable occTable = Health_Helper.GetOccupationDdl(compId);

            string occDdlList = string.Empty;

            if (occTable != null && occTable.Rows.Count > 0)
            {
                for (int i = 0; i < occTable.Rows.Count; i++)
                {
                    string occId = occTable.Rows[i]["OccupationId"].ToString();
                    string occupation = occTable.Rows[i]["Occupation"].ToString();
                    occDdlList = occDdlList + "<option value='" + occId + "' " + (occId == occupationId ? "selected" : string.Empty) + ">" + occupation + "</option>";
                }
            }
            return occDdlList;
        }

        #region [From Api DataBase]
        public static List<ApiNominee_Common> GetNominee_CommonDetails(string companyid, string type)
        {
            List<ApiNominee_Common> nomineeList = new List<ApiNominee_Common>();
            try
            {
                DataTable dtNominee = Health_Helper.GetNominee_CommonDetails(companyid, type);
                if (dtNominee != null && dtNominee.Rows.Count > 0)
                {
                    for (int i = 0; i < dtNominee.Rows.Count; i++)
                    {
                        ApiNominee_Common tempNominee = new ApiNominee_Common();

                        tempNominee.NomineeId = dtNominee.Rows[i]["NomineeId"].ToString();
                        tempNominee.RelationshipCode = dtNominee.Rows[i]["RelationshipCode"].ToString();
                        tempNominee.Relationship = dtNominee.Rows[i]["Relationship"].ToString();
                        tempNominee.Title = dtNominee.Rows[i]["Title"].ToString();
                        tempNominee.CompanyId = dtNominee.Rows[i]["CompanyId"].ToString();
                        tempNominee.CompanyName = dtNominee.Rows[i]["CompanyName"].ToString();
                        tempNominee.Status = dtNominee.Rows[i]["Status"].ToString();
                        tempNominee.Type = dtNominee.Rows[i]["Type"].ToString();
                        tempNominee.Gender = dtNominee.Rows[i]["Gender"].ToString();

                        nomineeList.Add(tempNominee);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return nomineeList;
        }
        public static string GetAnnualIncomeDetails(string enquiryid)
        {
            string strIncome = string.Empty;

            DataTable dtAnnualIncome = Health_Helper.GetAnnualIncomeDetails(enquiryid);
            if (dtAnnualIncome != null && dtAnnualIncome.Rows.Count > 0)
            {
                strIncome = "<option value=''>Select Annual Income</option>";
                for (int i = 0; i < dtAnnualIncome.Rows.Count; i++)
                {
                    strIncome = strIncome + "<option value='" + dtAnnualIncome.Rows[i]["ValueCode"].ToString() + "'>" + dtAnnualIncome.Rows[i]["ValueAmount"].ToString() + "</option>";
                }
            }

            return strIncome;
        }
        #endregion        
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string transactionNumber)
        {
            return Health_Helper.UpdateHealthPolicyPdfLink(enquiryid, transactionNumber);
        }

        public static bool UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string Status, string enquiryid, string purchaseToken = "")
        {
            return Health_Helper.UpdateCarePaymentDetails(policyNumber, transactionRefNum, uwDecision, errorFlag, errorMsg, Status, enquiryid, purchaseToken);
        }
        public static bool UpdateAdityBirlaPaymentDetails(string amount, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string txnDate, string quoteId)
        {
            return Health_Helper.UpdateAdityBirlaPaymentDetails(amount, transactionRefNum, uwDecision, errorFlag, errorMsg, status, enquiryid, txnDate, quoteId);
        }
        public static string GetHealth_BrochureLink(string productid, string companyid, string linktype)
        {
            return Health_Helper.GetHealth_BrochureLink(productid, companyid, linktype);
        }

        public static string SetPremiumBreakup(string basePremium, string premium, string discount, string serviceTax, string totalPremium, string period, string enquiryid)
        {
            return Health_Helper.SetPremiumBreakup(basePremium, premium, discount, serviceTax, totalPremium, period, enquiryid);
        }
        public static DataTable GetAddLessAddon(string enquiryid)
        {
            return Health_Helper.GetAddLessAddon(enquiryid);
        }
        //public static bool GetAddLessAddon(string enquiryid, string addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID)
        //{
        //    DataTable dtAddon = Health_Helper.GetAddLessAddon(enquiryid, addOnsId, addOns, code, value, calculation, addOnValue, productDetailID);
        //    if (dtAddon != null && dtAddon.Rows.Count > 0)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
        public static bool AddLessAddon(string[] addonlist)
        {
            return Health_Helper.AddLessAddon(addonlist);
        }

        public static List<AddonChecked> GetSelectedAddLessAddon(string enquiryid)
        {
            return Health_Helper.GetSelectedAddLessAddon(enquiryid);
        }
    }
}