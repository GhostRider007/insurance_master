﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using insurance.Helper;
using insurance.Helper.DataBase;
using static insurance.Models.Custom.FrontAgencyModel;

namespace insurance.Service
{
    public static class AdministrationService
    {
        public static bool AgencyLogin(string userid, string password, ref string msg)
        {
            return AdministrationHelper.AgencyLogin(userid, password, ref msg);
        }

        public static bool IsAgencyLogin(ref Agency_Detail lu)
        {
            return AdministrationHelper.IsAgencyLogin(ref lu);
        }

        public static bool LogoutAgency(string agencyId)
        {
            return AdministrationHelper.LogoutAgency(agencyId);
        }

        public static bool InsertLeadReq(LeadRequestModel model)
        {
            return AdministrationHelper.InsertLeadReq(model);
        }

        public static bool AgencyResetPassword(CreateAgency agency, ref string msg)
        {
            return AdministrationHelper.AgencyResetPassword(agency, ref msg);
        }
        public static bool UpdateBankDetails(BankDeatil agency, ref string msg)
        {
            return AdministrationHelper.UpdateBankDetails(agency, ref msg);
        }
        public static bool UpdateIsverif(string mobileno)
        {
            return AdministrationHelper.UpdateIsverif(mobileno);
        }
        public static List<ProposalDetails> GetEnquiryDetailsByAgencypro(string agencyid)
        {
            List<ProposalDetails> result = new List<ProposalDetails>();

            try
            {
                DataTable dtEnquiryDetail = AdministrationHelper.GetEnquiryDetailsByAgencypro(agencyid);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        ProposalDetails model = new ProposalDetails();
                        model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
                        model.Id = Convert.ToInt32(dtEnquiryDetail.Rows[i]["Id"].ToString());
                        model.policynumber= dtEnquiryDetail.Rows[i]["policynumber"].ToString();
                        model.policypdflink = dtEnquiryDetail.Rows[i]["policypdflink"].ToString();
                        model.ProposalNo= dtEnquiryDetail.Rows[i]["ProposalNo"].ToString();
                        model.enquiry_id= dtEnquiryDetail.Rows[i]["enquiry_id"].ToString();
                        model.ModelName=dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.vehicletype= dtEnquiryDetail.Rows[i]["vehicletype"].ToString();
                        model.FirstName= dtEnquiryDetail.Rows[i]["FirstName"].ToString();
                        model.lastName=dtEnquiryDetail.Rows[i]["lastName"].ToString();
                        model.ChainId= dtEnquiryDetail.Rows[i]["ChainId"].ToString();
                        model.PremimumAmount= dtEnquiryDetail.Rows[i]["PremimumAmount"].ToString();                      
                        model.CreatedDate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");                       
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        //public static List<ProposalDetails> GetEnquiryDetailsbyAgency(string agencyid)
        //{
        //    List<ProposalDetails> result = new List<ProposalDetails>();

        //    try
        //    {
        //        DataTable dtEnquiryDetail = AdministrationHelper.GetEnquiryDetailsByAgency(agencyid);

        //        if (dtEnquiryDetail.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
        //            {
        //                ProposalDetails model = new ProposalDetails();
        //                model.policynumber = dtEnquiryDetail.Rows[i]["policynumber"].ToString();
        //                model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
        //                model.Id = Convert.ToInt32(dtEnquiryDetail.Rows[i]["Id"].ToString());
        //                model.policypdflink = dtEnquiryDetail.Rows[i]["policypdflink"].ToString();
        //                model.ProposalNo = dtEnquiryDetail.Rows[i]["ProposalNo"].ToString();
        //                model.enquiry_id = dtEnquiryDetail.Rows[i]["enquiry_id"].ToString();
        //                model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
        //                model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
        //                model.vehicletype = dtEnquiryDetail.Rows[i]["vehicletype"].ToString();
        //                model.FirstName = dtEnquiryDetail.Rows[i]["FirstName"].ToString();
        //                model.lastName = dtEnquiryDetail.Rows[i]["lastName"].ToString();
        //                model.ChainId = dtEnquiryDetail.Rows[i]["ChainId"].ToString();
        //                model.PremimumAmount = dtEnquiryDetail.Rows[i]["PremimumAmount"].ToString();
        //                model.CreatedDate = (Convert.ToDateTime(dtEnquiryDetail.Rows[i]["CreatedDate"].ToString())).ToString("dd/MM/yyyy");
        //                result.Add(model);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        throw;
        //    }
        //    return result;
        //}

        //public static List<ProposalDetailsHealth> GetEnquiryDetailsByAgencyHealth(string agencyid, string listType)
        //{
        //    List<ProposalDetailsHealth> result = new List<ProposalDetailsHealth>();

        //    try
        //    {
        //        DataTable dtEnquiryDetail = AdministrationHelper.GetEnquiryDetailsByAgencyHealth(agencyid, listType);

        //        if (dtEnquiryDetail.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
        //            {
        //                ProposalDetailsHealth model = new ProposalDetailsHealth();

        //                model.AgencyID = dtEnquiryDetail.Rows[i]["AgencyId"].ToString();
        //                model.AgencyName = dtEnquiryDetail.Rows[i]["AgencyName"].ToString();
        //                model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();

        //                model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
        //                model.PremiumAmount = dtEnquiryDetail.Rows[i]["premium"].ToString();
        //                model.SumInsured = dtEnquiryDetail.Rows[i]["suminsured"].ToString();
        //                model.companyid = dtEnquiryDetail.Rows[i]["companyid"].ToString();

        //                model.CustomerName = AdministrationHelper.GetInsuredCustomerNameHealth(model.Enquiry_Id);
        //                model.ProductName = AdministrationHelper.GetProductNameHealth(model.Enquiry_Id);

        //                model.p_proposalNum = dtEnquiryDetail.Rows[i]["p_proposalNum"].ToString();
        //                model.pay_proposal_Number = dtEnquiryDetail.Rows[i]["pay_proposal_Number"].ToString();
        //                model.CreatedDate_Proposal = dtEnquiryDetail.Rows[i]["CreatedDateProposal"].ToString();

        //                model.pay_policy_Number = dtEnquiryDetail.Rows[i]["pay_policy_Number"].ToString();
        //                model.CreatedDate_Policy = dtEnquiryDetail.Rows[i]["CreatedDatePolicy"].ToString();

        //                result.Add(model);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        throw;
        //    }
        //    return result;
        //}

        public static List<DashboardDetails> GetDasboardHelathdetails(string agencyid)
        {
            List<DashboardDetails> result = new List<DashboardDetails>();

            try
            {
                DataTable dtEnquiryDetail = AdministrationHelper.GetDasboarddetails(agencyid);

                if (dtEnquiryDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEnquiryDetail.Rows.Count; i++)
                    {
                        DashboardDetails model = new DashboardDetails();
                        model.Enquiry_Id = dtEnquiryDetail.Rows[i]["Enquiry_Id"].ToString();
                        model.ModelName = dtEnquiryDetail.Rows[i]["ModelName"].ToString();
                        model.VechileRegNo = dtEnquiryDetail.Rows[i]["VechileRegNo"].ToString();
                        model.vehicletype = dtEnquiryDetail.Rows[i]["vehicletype"].ToString();
                        model.firstname = dtEnquiryDetail.Rows[i]["firstname"].ToString();                       
                        model.lastname = dtEnquiryDetail.Rows[i]["lastname"].ToString();
                        model.totalPremium = dtEnquiryDetail.Rows[i]["p_totalPremium"].ToString();
                        model.Policy_Type = dtEnquiryDetail.Rows[i]["Policy_Type"].ToString();
                        model.service = dtEnquiryDetail.Rows[i]["Service"].ToString();
                        model.servicetype = dtEnquiryDetail.Rows[i]["ServiceType"].ToString();
                        model.createddate = dtEnquiryDetail.Rows[i]["createddate"].ToString();
                        model.suminsured = dtEnquiryDetail.Rows[i]["suminsured"].ToString();
                        model.Sum_Insured = dtEnquiryDetail.Rows[i]["Sum_Insured"].ToString();
                        model.CompanyName = dtEnquiryDetail.Rows[i]["CompanyName"].ToString();
                        model.Prodcutname = dtEnquiryDetail.Rows[i]["Prodcutname"].ToString();

                        model.Fuel = dtEnquiryDetail.Rows[i]["Fuel"].ToString();
                        model.VarientName = dtEnquiryDetail.Rows[i]["VarientName"].ToString();
                        model.BrandName = dtEnquiryDetail.Rows[i]["BrandName"].ToString();
                        result.Add(model);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return result;
        }

        public static int[] GetPolicyCountAgency( string Agencyid, string period, string durationValue)
        {
            int[] counts = new int[4];
            try
            {

                DataTable dt = ConnectToDataBase.GetPolicygraphAgency(Agencyid, period, durationValue, "motor");
                if (dt.Rows.Count > 0)
                {
                    counts[0] = Convert.ToInt32(dt.Rows[0]["PolicyCount"].ToString());
                    counts[1] = Convert.ToInt32(dt.Rows[0]["Perposalcount"].ToString());
                    counts[2] = Convert.ToInt32(dt.Rows[0]["Leadcount"].ToString());                   

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return counts;
        }
    }
}