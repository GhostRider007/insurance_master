﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static insurance.Models.Common.CommonModel;
using insurance.Helper.CommonHelper;
using System.Net;
using System.IO;

namespace insurance.Service.CommonService
{
    public static class CommonServe
    {
        public static List<Gender> GetGender(string genderId = null, string status = null)
        {
            return CommonHelp.GetGender(genderId, status);
        }

        public static string SmsMessage(string mobileno ,string msg)
        {       
            string url = "http://mobicomm.dove-sms.com//submitsms.jsp?user=SSTrvlPL&key=70a9327a28XX&mobile=+91"+mobileno+"&message="+msg+"&senderid=SEEINS&accusage=1";           
            HttpWebRequest _createRequest = (HttpWebRequest)WebRequest.Create(url);        
            HttpWebResponse myResp = (HttpWebResponse)_createRequest.GetResponse();
            System.IO.StreamReader _responseStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = _responseStreamReader.ReadToEnd();
            _responseStreamReader.Close();
            myResp.Close();
            return responseString;
        }
        public static int GenerateRandomNum()
        {
            // Number of digits for random number to generate
            int randomDigits = 4;
            int _max = (int)Math.Pow(10, randomDigits);
            Random _rdm = new Random();
            int _out = _rdm.Next(0, _max);

            while (randomDigits != _out.ToString().ToArray().Distinct().Count())
            {
                _out = _rdm.Next(0, _max);
            }
            return _out;
        }
    }
}