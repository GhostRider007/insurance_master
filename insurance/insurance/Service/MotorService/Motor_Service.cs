﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using insurance.Helper.MoterHelper;
using insurance.Models.Motor;
using Post_Utility.Utility;

namespace insurance.Service.MotorService
{
    public static class Motor_Service
    {        
        public static string GenrateMotorEnquiryId()
        {
            return UtilityClass.EnryptString("sim" + UtilityClass.GenrateEnquiryId(5, "1234567890"));
        }
        public static string DecryptGenrateEnquiryId(string enqid)
        {
            return UtilityClass.DecryptString(enqid);
        }
        public static bool SaveMoterGenratedQuotes(MotorModel model)
        {
            return Motor_Helper.SaveMoterGenratedQuotes(model);
        }
        public static List<MotorRTOModel> GetRTODetails()
        {
            return Motor_Helper.GetRTODetails();
        }
        public static bool UpdateMoterRegDetail(MotorModel model)
        {
            return Motor_Helper.UpdateMoterRegDetail(model);
        }
        public static List<string> GetBrandList(bool IsBike)
        {
            return Motor_Helper.GetBrandList(IsBike);
        }
        public static List<string> GetModelList(string brand, string enqId, bool IsBike)
        {
            return Motor_Helper.GetModelList(brand, enqId, IsBike);
        }
        public static List<string> GetFuelList(string brand, string model, string enqId, bool IsBike)
        {
            return Motor_Helper.GetFuelList(brand, model, enqId, IsBike);
        }
        public static List<string> GetVarientList(string brand, string model, string fuel, string enqId, bool IsBike)
        {
            return Motor_Helper.GetVarientList(brand, model, fuel, enqId, IsBike);
        }
        public static string GetVehicleMainCode(string brand, string model, string fuel, string variant, bool IsBike)
        {
            return Motor_Helper.GetVehicleMainCode(brand, model, fuel, variant, IsBike);
        }
        public static bool UpdateVarientDetails(string varient, string enqId)
        {
            return Motor_Helper.UpdateVarientDetails(varient, enqId);
        }
        public static MotorModel GetEnquiryDetails(string enqId)
        {
            MotorModel model = new MotorModel();

            try
            {
                DataTable dtDet = Motor_Helper.GetEnquiryDetails(enqId);
                if (dtDet != null && dtDet.Rows.Count > 0)
                {
                    model.Id = Convert.ToInt32(dtDet.Rows[0]["Id"].ToString());
                    model.Enquiry_Id = dtDet.Rows[0]["Enquiry_Id"].ToString();
                    model.AgencyId = dtDet.Rows[0]["AgencyId"].ToString();
                    model.AgencyName = dtDet.Rows[0]["AgencyName"].ToString();
                    model.BrandName = dtDet.Rows[0]["BrandName"].ToString();
                    model.ModelName = dtDet.Rows[0]["ModelName"].ToString();
                    model.VechileRegNo = dtDet.Rows[0]["VechileRegNo"].ToString();
                    model.MobileNo = dtDet.Rows[0]["MobileNo"].ToString();
                    model.RtoId = dtDet.Rows[0]["RtoId"].ToString();
                    model.RtoName = dtDet.Rows[0]["RtoName"].ToString();
                    model.DateOfMfg = dtDet.Rows[0]["DateOfMfg"].ToString();
                    model.BrandId = dtDet.Rows[0]["BrandId"].ToString();
                    model.BrandName = dtDet.Rows[0]["BrandName"].ToString();
                    model.ModelId = dtDet.Rows[0]["ModelId"].ToString();
                    model.VarientId = dtDet.Rows[0]["VarientId"].ToString();
                    model.ispreviousinsurer = dtDet.Rows[0]["ispreviousinsurer"].ToString();
                    model.policyexpirydate = dtDet.Rows[0]["policyexpirydate"].ToString();
                    model.isclaiminlastyear = dtDet.Rows[0]["isclaiminlastyear"].ToString();
                    model.previousyearnoclaim = dtDet.Rows[0]["previousyearnoclaim"].ToString();
                    model.Fuel = dtDet.Rows[0]["Fuel"].ToString();
                    model.VarientName = dtDet.Rows[0]["VarientName"].ToString();
                    model.DateOfReg = dtDet.Rows[0]["DateOfReg"].ToString();
                    model.GoDigitId = dtDet.Rows[0]["GoDigit"].ToString();
                    model.ShriRamId = dtDet.Rows[0]["ShriRam"].ToString();
                    model.policynumber = dtDet.Rows[0]["policynumber"].ToString();
                    model.PinCode = Convert.ToInt32(dtDet.Rows[0]["PinCode"].ToString());
                    model.State = dtDet.Rows[0]["statename"].ToString();
                    model.VechileType = dtDet.Rows[0]["vehicletype"].ToString();
                    model.originalpreviouspolicytype = dtDet.Rows[0]["originalpreviouspolicytype"].ToString();
                    model.insurancetype = dtDet.Rows[0]["insurancetype"].ToString();
                    model.policyholdertype = dtDet.Rows[0]["policyholdertype"].ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }

            return model;
        }
        public static MotorProposal GetMotorProposal(string enqId)
        {
            MotorProposal model = new MotorProposal();

            try
            {
                DataTable dtDet = Motor_Helper.GetMotorProposal(enqId);
                if (dtDet != null && dtDet.Rows.Count > 0)
                {
                    model.enquiry_id = dtDet.Rows[0]["enquiry_id"].ToString();
                    //model.suminsured = dtDet.Rows[0]["suminsured"].ToString();
                    model.title = dtDet.Rows[0]["title"].ToString();
                    model.gender = dtDet.Rows[0]["gender"].ToString();
                    model.firstname = dtDet.Rows[0]["firstname"].ToString();
                    model.lastname = dtDet.Rows[0]["lastname"].ToString();
                    model.mobileno = dtDet.Rows[0]["mobileno"].ToString();
                    model.dob = dtDet.Rows[0]["dob"].ToString();
                    model.emailid = dtDet.Rows[0]["emailid"].ToString();
                    model.address1 = dtDet.Rows[0]["address1"].ToString();
                    model.address2 = dtDet.Rows[0]["address2"].ToString();
                    model.landmark = dtDet.Rows[0]["landmark"].ToString();
                    model.pincode = dtDet.Rows[0]["pincode"].ToString();
                    model.statename = dtDet.Rows[0]["statename"].ToString();
                    model.cityname = dtDet.Rows[0]["cityname"].ToString();
                    model.isproposer = Convert.ToBoolean(dtDet.Rows[0]["isproposer"].ToString());
                    model.nrelation = dtDet.Rows[0]["nrelation"].ToString();
                    model.ntitle = dtDet.Rows[0]["ntitle"].ToString();
                    model.nfirstname = dtDet.Rows[0]["nfirstname"].ToString();
                    model.nlastname = dtDet.Rows[0]["nlastname"].ToString();
                    model.ndob = dtDet.Rows[0]["ndob"].ToString();
                    model.vehicleregno = dtDet.Rows[0]["vehicleregno"].ToString();
                    model.vehiclemfdyear = dtDet.Rows[0]["vehiclemfdyear"].ToString();
                    model.engineno = dtDet.Rows[0]["engineno"].ToString();
                    model.chasisno = dtDet.Rows[0]["chasisno"].ToString();
                    model.valuntarydeductible = dtDet.Rows[0]["valuntarydeductible"].ToString();
                    model.previousinsured = dtDet.Rows[0]["previousinsured"].ToString();
                    model.policyholdertype = dtDet.Rows[0]["policyholdertype"].ToString();
                    model.islonelease = !string.IsNullOrEmpty(dtDet.Rows[0]["islonelease"].ToString()) ? Convert.ToBoolean(dtDet.Rows[0]["islonelease"].ToString()) : false;
                    model.gstno = dtDet.Rows[0]["gstno"].ToString();
                    model.panno = dtDet.Rows[0]["panno"].ToString();
                    //Convert.ToBoolean(dtDet.Rows[0]["islonelease"].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return model;
        }
        public static MotorEnquiryResponse GetMotorEnquiryResponse(string enqId)
        {
            MotorEnquiryResponse model = new MotorEnquiryResponse();

            try
            {
                DataTable dtDet = Motor_Helper.GetMotorEnquiryResponse(enqId);
                if (dtDet != null && dtDet.Rows.Count > 0)
                {
                    model.id = Convert.ToInt32(dtDet.Rows[0]["id"].ToString());
                    model.enquiry_id = dtDet.Rows[0]["enquiry_id"].ToString();
                    model.searchrequest = dtDet.Rows[0]["searchrequest"].ToString();
                    model.response = dtDet.Rows[0]["response"].ToString();
                    model.chainid = dtDet.Rows[0]["chainid"].ToString();
                    model.preproposalno = dtDet.Rows[0]["preproposalno"].ToString();
                    model.suminsured = dtDet.Rows[0]["suminsured"].ToString();
                    model.proposalrequest = dtDet.Rows[0]["proposalrequest"].ToString();
                    model.proposalresponse = dtDet.Rows[0]["proposalresponse"].ToString();
                    model.paymentrequest = dtDet.Rows[0]["paymentrequest"].ToString();
                    model.paymentcreationresponse = dtDet.Rows[0]["paymentcreationresponse"].ToString();
                    model.applicationid = dtDet.Rows[0]["applicationid"].ToString();
                    model.transactionnumber = dtDet.Rows[0]["transactionnumber"].ToString();
                    model.policypdflink = dtDet.Rows[0]["policypdflink"].ToString();
                    model.productcode = dtDet.Rows[0]["productcode"].ToString();
                    model.policynumber = dtDet.Rows[0]["policynumber"].ToString();
                    model.proposalnumber = dtDet.Rows[0]["proposalnumber"].ToString();
                    model.paymentstatus = dtDet.Rows[0]["paymentstatus"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        public static bool UpdateOtherDetails(string isprevious, string insurerid, string insurername, string policynumber, string expdate, string lastyr, string noclaim, string enqid, string pptype, string phtype = "")
        {
            return Motor_Helper.UpdateOtherDetails(isprevious, insurerid, insurername, policynumber, expdate, lastyr, noclaim, enqid, pptype, phtype);
        }
        public static IDictionary<int, string> GetPreviousInsurerCodes_GoDigit()
        {
            IDictionary<int, string> insurer = new Dictionary<int, string>();
            DataTable dtInsurer = Motor_Helper.GetPreviousInsurerCodes_GoDigit();

            if (dtInsurer != null && dtInsurer.Rows.Count > 0)
            {
                for (int i = 0; i < dtInsurer.Rows.Count; i++)
                {
                    insurer.Add(Convert.ToInt32(dtInsurer.Rows[i]["Code"].ToString()), dtInsurer.Rows[i]["Name"].ToString());
                }
            }

            return insurer;
        }
        public static IDictionary<int, string> GetNCBPercentage()
        {
            IDictionary<int, string> insurer = new Dictionary<int, string>();
            DataTable dtNcbPer = Motor_Helper.GetNCBPercentage();

            if (dtNcbPer != null && dtNcbPer.Rows.Count > 0)
            {
                for (int i = 0; i < dtNcbPer.Rows.Count; i++)
                {
                    insurer.Add(Convert.ToInt32(dtNcbPer.Rows[i]["ncbpercentage"].ToString()), dtNcbPer.Rows[i]["ncbgodigit"].ToString());
                }
            }

            return insurer;
        }
        public static object InsertAndUpdateMotoResponse(string enquiryid, string chainid, string proposalno, string suminsured, string productcode = "")
        {
            return Motor_Helper.InsertAndUpdateMotoResponse(enquiryid, chainid, proposalno, suminsured, productcode);
        }
        public static bool SaveMotorProposalData(MotorProposal motorProposalDetail)
        {
            return Motor_Helper.SaveMotorProposalData(motorProposalDetail);
        }
        public static bool SaveMotorNomineeData(string enquiry_id, string relation, string title, string nfirstname, string nlastname, string ndob)
        {
            return Motor_Helper.SaveMotorNomineeData(enquiry_id, relation, title, nfirstname, nlastname, ndob);
        }
        public static bool SaveVehicleHistoryData(string enquiry_id, string vehicleregno, string vehiclemfdyear, string engineno, string chasisno, string valuntarydeductible, string previousinsured, string islonelease, string policyholdertype = "", string gstno = "", string panno = "")
        {
            return Motor_Helper.SaveVehicleHistoryData(enquiry_id, vehicleregno, vehiclemfdyear, engineno, chasisno, valuntarydeductible, previousinsured, islonelease, policyholdertype, gstno, panno);
        }
        public static List<string> CheckAndGetDetails(string pincode)
        {
            return Motor_Helper.CheckAndGetDetails(pincode);
        }
        public static List<string> MotorNomineeRelation(string chainid)
        {
            List<string> relationList = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(chainid))
                {
                    DataTable dtRelation = Motor_Helper.MotorNomineeRelation(chainid == "1" ? "tbl_NomineeMaster_GoDigit" : "tbl_NomineeMaster_Shriram");
                    if (dtRelation != null && dtRelation.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtRelation.Rows.Count; i++)
                        {
                            if (chainid == "1")
                            {
                                relationList.Add(dtRelation.Rows[i]["Nominee_Relation"].ToString());
                            }
                            else
                            {
                                relationList.Add(dtRelation.Rows[i]["PC_DESC"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return relationList;
        }
        public static string GetVolunatryDeductible(string chainid, string motorType)
        {
            string volunatry = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(chainid))
                {
                    string wherecon = string.Empty;
                    string tableName = string.Empty;

                    switch (chainid)
                    {
                        case "1":
                            tableName = "tbl_VolunatryDeductibleMaster_Godigit";
                            break;
                        case "2":
                            tableName = "tbl_VoluntaryExcessCodes_Shriram";
                            wherecon = (motorType.ToLower() == "car" ? "VehicleType='PrivateCar'" : "VehicleType='TwoWheeler'");
                            break;
                        case "3":
                            tableName = "IFFCO_VolunatryDeductibleMaster";
                            wherecon = (motorType.ToLower() == "car" ? "Vehicletype='PCP'" : "Vehicletype='TWP'");
                            break;
                    }

                    DataTable dtVD = Motor_Helper.GetVolunatryDeductible(tableName, wherecon);
                    if (dtVD != null && dtVD.Rows.Count > 0)
                    {
                        volunatry = "<option value=''>SELECT VOLUNTARY DEDUCTIBLE</option>";

                        switch (chainid)
                        {
                            case "1":
                                for (int i = 0; i < dtVD.Rows.Count; i++)
                                {
                                    volunatry = volunatry + "<option value='" + dtVD.Rows[i]["Voluntary_Deductible"].ToString() + "'>" + dtVD.Rows[i]["Amount"].ToString() + "</option>";
                                }
                                break;
                            case "2":
                                for (int i = 0; i < dtVD.Rows.Count; i++)
                                {
                                    volunatry = volunatry + "<option value='" + dtVD.Rows[i]["Code"].ToString() + "'>" + dtVD.Rows[i]["Amount"].ToString() + "</option>";
                                }
                                break;
                            case "3":
                                for (int i = 0; i < dtVD.Rows.Count; i++)
                                {
                                    volunatry = volunatry + "<option value='" + dtVD.Rows[i]["Amount"].ToString() + "'>" + dtVD.Rows[i]["Amount"].ToString() + "</option>";
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return volunatry;
        }
        public static string GetPreviousInsured(string chainid)
        {
            string previousInsure = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(chainid))
                {
                    string tableName = string.Empty;

                    switch (chainid)
                    {
                        case "1":
                            tableName = "tbl_InsuranceCompanyMaster_GoDigit";
                            break;
                        case "2":
                            tableName = "tbl_PrevInsCompMaster_Shriram";
                            break;
                        case "3":
                            tableName = "IFFCO_PrevInsCompMaster";
                            break;
                    }

                    DataTable dtPI = Motor_Helper.MotorPreviousInsured(tableName, chainid);

                    if (dtPI != null && dtPI.Rows.Count > 0)
                    {
                        previousInsure = "<option value=''>SELECT PREVIOUS INSURED</option>";

                        switch (chainid)
                        {
                            case "1":
                                for (int i = 0; i < dtPI.Rows.Count; i++)
                                {
                                    previousInsure = previousInsure + "<option value='" + dtPI.Rows[i]["InsuranceCompanyCode"].ToString() + "'>" + dtPI.Rows[i]["NameoftheCompany"].ToString() + "</option>";
                                }
                                break;
                            case "2":
                                for (int i = 0; i < dtPI.Rows.Count; i++)
                                {
                                    previousInsure = previousInsure + "<option value='" + dtPI.Rows[i]["CC_DESC"].ToString() + "'>" + dtPI.Rows[i]["CC_DESC"].ToString() + "</option>";
                                }
                                break;
                            case "3":
                                for (int i = 0; i < dtPI.Rows.Count; i++)
                                {
                                    previousInsure = previousInsure + "<option value='" + dtPI.Rows[i]["PREVIOUSINSURERS"].ToString() + "'>" + dtPI.Rows[i]["PREVIOUSINSURERS"].ToString() + "</option>";
                                }
                                break;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return previousInsure;
        }
        public static string GetPreviousPolicyType(string insurancetype, string ptype)
        {
            string ppt = string.Empty;
            try
            {
                DataTable dtPI = Motor_Helper.MotorPreviousPolicyType("tblMaster_PreviousPolicyType", insurancetype, ptype);

                if (dtPI != null && dtPI.Rows.Count > 0)
                {
                    ppt = "<option value=''>SELECT PREVIOUS POLICY TYPE</option>";
                    for (int i = 0; i < dtPI.Rows.Count; i++)
                    {
                        ppt = ppt + "<option value='" + dtPI.Rows[i]["ProductID"].ToString() + "'>" + dtPI.Rows[i]["ProductName"].ToString() + "</option>";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return ppt;
        }
        public static List<object> MotorEnquiryDetails(string enquiry_id)
        {
            List<object> allDetails = new List<object>();

            object enquiryDetails = GetEnquiryDetails(enquiry_id);
            string agencyId = enquiryDetails.GetType().GetProperty("AgencyId").GetValue(enquiryDetails, null).ToString();
            object agencyDetails = Helper.FrontAgencyHelper.GetAgencyList(agencyId)[0];

            allDetails.Add(GetMotorProposal(enquiry_id));
            allDetails.Add(enquiryDetails);
            allDetails.Add(GetMotorEnquiryResponse(enquiry_id));
            allDetails.Add(agencyDetails);



            return allDetails;
        }
        public static bool UpdatePaymentTransId(string enquiryid, string transactionNumber, string policynumber, string status)
        {
            return Motor_Helper.UpdatePaymentTransId(enquiryid, transactionNumber, policynumber, status);
        }
        public static bool UpdatePolicyPdfLink(string enquiryid, string transactionNumber)
        {
            return Motor_Helper.UpdatePolicyPdfLink(enquiryid, transactionNumber);
        }
        public static bool UpdateCheckPaymentStatus(string enquiryId, string Req, string respo)
        {
            return Motor_Helper.UpdateCheckPaymentStatus(enquiryId, Req, respo);
        }
        public static string GetPdfDocumentByEnqId(string enquiryid)
        {
            return Motor_Helper.GetPdfDocumentByEnqId(enquiryid);
        }
        public static List<string> GetDetailsByTransId(string proposalSysID, string proposalNumber)
        {
            return Motor_Helper.GetDetailsByTransId(proposalSysID, proposalNumber);
        }
        public static string GetImageUrlByCHainId(string chainid)
        {
            return Motor_Helper.GetImageUrlByCHainId(chainid);
        }
        public static bool UpdatePerposalPremiumAmount(string enquiry_id, string grossPremium, string perposalNumber)
        {
            return Motor_Helper.UpdatePerposalPremiumAmount(enquiry_id, grossPremium, perposalNumber);
        }
        public static List<string> CheckPaymentStatusDetails(string enquiryid)
        {
            return Motor_Helper.CheckPaymentStatusDetails(enquiryid);
        }
        public static bool AddUpdateSelectedAddon(List<AddUpdateSelectedAddon> addon, string enqId)
        {
            return Motor_Helper.AddUpdateSelectedAddon(addon, enqId);
        }
        public static List<AddUpdateSelectedAddon> GetAddonList(string enquiryid)
        {
            return Motor_Helper.GetAddonList(enquiryid);
        }
        public static string GetMotor_BrochureLink(string productType, string policyType, string productName, string productCode, string policyCode, string linktype)
        {
            return Motor_Helper.GetMotor_BrochureLink(productType, policyType, productName, productCode, policyCode, linktype);
        }
        #region[Extranet]
        public static bool ExtranetSaveDetails_Motor(MotorModel enquiryDetails, MotorProposal proposal) 
        {
            return Motor_Helper.ExtranetSaveDetails_Motor(enquiryDetails, proposal);
        }       
        #endregion
    }
}