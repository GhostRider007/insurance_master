﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using insurance.Helper.DataBase;
using insurance.Models.Common;
using insurance.Models.Motor;
using insurance.Models.Motor.BikeModel;
using insurance.Models.Motor.PrivateBikeProposal;
using insurance.Models.Motor.PrivateCarProposal;
using insurance.Service.MotorService;
using Newtonsoft.Json;
using Post_Utility.Utility;

namespace insurance.MotorApi
{
    public static class MotorAPIHelper
    {
        #region [Private Car Section]
        public static MotorRoot GetMotorApiResponse(string userid, string password, MotorModel motorenq)
        {
            MotorRoot result = new MotorRoot();

            try
            {
                if (!string.IsNullOrEmpty(motorenq.policyexpirydate))
                {
                    motorenq.policyexpirydate = CommonClass.ConvertDateYYMMDD(motorenq.policyexpirydate);
                }

                string vehicleMaincode = Motor_Service.GetVehicleMainCode(motorenq.BrandName, motorenq.ModelName, motorenq.Fuel, motorenq.VarientName, false);

                MotorProposal proposal = Motor_Service.GetMotorProposal(motorenq.Enquiry_Id);

                string motorRequest = "{'enquiryId': '" + motorenq.Enquiry_Id + "', 'licensePlateNumber': '" + motorenq.VechileRegNo + "', 'pincode': '" + motorenq.PinCode + "', 'policyHolderType': ''," + "'registrationDate': '" + CommonClass.ConvertDateYYMMDD(motorenq.DateOfReg) + "','manufactureDate':'" + CommonClass.ConvertDateYYMMDD(motorenq.DateOfMfg) + "', 'vehicleMaincode': '" + vehicleMaincode + "', 'Rtocode': '" + motorenq.RtoId + "', 'isPreviousInsurerKnown':" + (motorenq.ispreviousinsurer == "yes" ? "true" : "false") + ", 'previousInsurerCode':'',"
                    + "'previousPolicyNumber':'" + motorenq.policynumber + "', 'previousPolicyExpiryDate':'" + motorenq.policyexpirydate + "', 'isClaimInLastYear':" + (motorenq.isclaiminlastyear == "yes" ? "true" : "false") + ","
                    + "'originalPreviousPolicyType':'" + motorenq.originalpreviouspolicytype + "', 'previousPolicyType':'', 'previousNoClaimBonus':'" + motorenq.previousyearnoclaim + "',"
                    + "'currentThirdPartyPolicy':'', 'PolicyType':'" + motorenq.insurancetype + "'}";


                //StringBuilder soadStr = new StringBuilder();
                //soadStr.Append("{'enquiryId': '"+ motorenq.Enquiry_Id + "', 'licensePlateNumber': '" + motorenq.VechileRegNo + "', 'pincode': '" + motorenq.PinCode + "', 'policyHolderType': 'INDIVIDUAL', 'registrationDate': '2020-03-10', 'vehicleMaincode': '19212767 ',");
                //soadStr.Append("'Rtocode': '" + motorenq.RtoId + "', 'isPreviousInsurerKnown': " + (motorenq.ispreviousinsurer == "yes" ? "true" : "false") + ", 'previousInsurerCode': null, 'previousPolicyNumber': " + (string.IsNullOrEmpty(motorenq.policynumber) ? "NULL" : motorenq.policynumber) + ", 'previousPolicyExpiryDate': '2021-05-25', 'isClaimInLastYear': " + (motorenq.isclaiminlastyear == "yes" ? "true" : "false") + ",");
                //soadStr.Append("'originalPreviousPolicyType': '3', 'previousPolicyType': null, 'previousNoClaimBonus': '0', 'previousNoClaimBonusValue': null, ");
                //soadStr.Append("'currentThirdPartyPolicys': {");
                //soadStr.Append("'isCurrentThirdPartyPolicyActive': null,");
                //soadStr.Append("'currentThirdPartyPolicyInsurerCode': '158',");
                //soadStr.Append("'currentThirdPartyPolicyNumber': 'D300073312',");
                //soadStr.Append("'currentThirdPartyPolicyStartDateTime': '2021-05-26T16:09:58',");
                //soadStr.Append("'currentThirdPartyPolicyExpiryDateTime': '2024-05-25T23:59:59'");
                //soadStr.Append("},");
                //soadStr.Append("'PolicyType':'SAOD'}");

                //string motorSOADRequest = soadStr.ToString();

                string respo = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "PrivateCarQuotes"), motorRequest.ToString(), userid, password);
                if (!string.IsNullOrEmpty(respo))
                {
                    var isSuccess = ConnectToDataBase.InsertAndUpdateMotoResponse(motorenq.Enquiry_Id, motorRequest, respo, "search", "motor");

                    if (Convert.ToBoolean(isSuccess))
                    {
                        result = JsonConvert.DeserializeObject<MotorRoot>(respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static PrivateCarProposal GetMotorApiPrivateCarProposalResponse(string userid, string password, string enquiryid, bool isBike)
        {
            PrivateCarProposal result = new PrivateCarProposal();

            try
            {
                MotorModel enq = Motor_Service.GetEnquiryDetails(enquiryid);
                MotorProposal proposal = Motor_Service.GetMotorProposal(enquiryid);
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                MotorRoot quoteRespo = JsonConvert.DeserializeObject<MotorRoot>(enqRespo.response);
                List<AddUpdateSelectedAddon> addonList = Motor_Service.GetAddonList(enquiryid);

                string vehicleMaincode = Motor_Service.GetVehicleMainCode(enq.BrandName, enq.ModelName, enq.Fuel, enq.VarientName, isBike);

                PrivateCarQuotesR pBikeDetail = quoteRespo.PrivateCarQuotesRS.Where(p => p.Chainid == enqRespo.chainid && p.ProductDetails.grossPremium == enqRespo.suminsured && p.ProductDetails.insuranceProductCode == enqRespo.productcode).FirstOrDefault();

                string vehicleIDV = string.Empty;
                if (pBikeDetail.vehicle != null)
                {
                    if (pBikeDetail.vehicle.vehicleIDV != null)
                    {
                        vehicleIDV = pBikeDetail.vehicle.vehicleIDV.idv.ToString();
                    }
                }

                string addons = string.Empty;
                string legallibilities = string.Empty;
                string acccovers = string.Empty;

                if (addonList != null && addonList.Count > 0)
                {
                    foreach (var item in addonList)
                    {

                        if (item.AddonName == "AddOnCovers")
                        {
                            addons = addons + "{'selection': true,'covername': '" + item.InnerName + "','netPremium': '" + item.InsuredAmt + "'},";
                        }
                        if (item.AddonName == "LegalLiabilitys")
                        {
                            legallibilities = legallibilities + "{'selection': true,'covername': '" + item.InnerName + "','netPremium': '" + item.InsuredAmt + "'},";
                        }
                        if (item.AddonName == "AccessoriesCovers")
                        {
                            acccovers = acccovers + "{'selection': true,'covername': '" + item.InnerName + "','insuredAmount': '" + item.InsuredAmt + "'},";
                        }

                    }
                }

                if (!string.IsNullOrEmpty(addons))
                {
                    addons = addons.Remove(addons.LastIndexOf(","), 1);
                }

                if (!string.IsNullOrEmpty(legallibilities))
                {
                    legallibilities = legallibilities.Remove(legallibilities.LastIndexOf(","), 1);
                }

                if (!string.IsNullOrEmpty(acccovers))
                {
                    acccovers = acccovers.Remove(acccovers.LastIndexOf(","), 1);
                }

                StringBuilder sbRequest = new StringBuilder();
                sbRequest.Append("{'PolicyType':'" + pBikeDetail.PolicyType + "','insuranceProductCode':'" + pBikeDetail.ProductDetails.insuranceProductCode + "','subInsuranceProductCode':'" + pBikeDetail.ProductDetails.subInsuranceProductCode + "','enquiryId': '" + enquiryid + "', 'Chainid': '" + enqRespo.chainid + "','Rtocode': '" + enq.RtoId + "','voluntaryDeductible': '" + proposal.valuntarydeductible + "', ");
                sbRequest.Append("'policyHolderType':'" + proposal.policyholdertype + "', 'coverTerm':1, 'coverAvailability': '', 'netPremium': '" + enqRespo.suminsured + "', ");
                sbRequest.Append("'vehicleMaincode': '" + vehicleMaincode + "', 'licensePlateNumber': '" + enq.VechileRegNo + "', ");
                sbRequest.Append("'vehicleIdentificationNumber': '" + proposal.chasisno + "', 'engineNumber': '" + proposal.engineno + "', 'registrationDate': '" + CommonClass.ConvertDateYYMMDD(enq.DateOfReg) + "','manufactureDate':'" + CommonClass.ConvertDateYYMMDD(enq.DateOfMfg) + "',");
                sbRequest.Append("'persons': { 'personType': '" + proposal.policyholdertype + "', 'firstName': '" + proposal.firstname + "', 'middleName': '', 'lastName': '" + proposal.lastname + "', ");
                sbRequest.Append("'dateOfBirth': '" + CommonClass.ConvertDateYYMMDD(proposal.dob) + "', 'gender': '" + proposal.gender + "', 'EmailID': '" + proposal.emailid + "', 'Mobile': ");
                sbRequest.Append("'" + proposal.mobileno + "', 'addressType': 'PRIMARY_RESIDENCE', 'flatNumber': '" + proposal.address1 + "', 'streetNumber': '" + proposal.address2 + "', ");
                sbRequest.Append("'street': '" + proposal.landmark + "', 'district': '', 'state': '" + proposal.statename + "', 'city': '" + proposal.cityname + "', 'pincode': '" + proposal.pincode + "' },");
                sbRequest.Append("'nominee': { 'firstName': '" + proposal.nfirstname + "', 'middleName': '', 'lastName': '" + proposal.nlastname + "', 'dateOfBirth': '" + CommonClass.ConvertDateYYMMDD(proposal.ndob) + "',");
                sbRequest.Append("'relation': '" + proposal.nrelation + "', 'personType': '" + proposal.policyholdertype + "' },");
                sbRequest.Append("'previouspolicydetail':{ 'isPreviousInsurerKnown':" + (enq.ispreviousinsurer == "yes" ? "true" : "false") + ", 'previousInsurerCode':'" + proposal.previousinsured + "', ");
                sbRequest.Append("'previousPolicyNumber':'" + enq.policynumber + "', 'previousPolicyExpiryDate':'" + CommonClass.ConvertDateYYMMDD(enq.policyexpirydate) + "', ");
                sbRequest.Append("'isClaimInLastYear':" + (enq.isclaiminlastyear == "yes" ? "true" : "false") + ", 'originalPreviousPolicyType':'" + enq.originalpreviouspolicytype + "', ");
                sbRequest.Append("'previousPolicyType':'" + proposal.policyholdertype + "', 'previousNoClaimBonus':'" + enq.previousyearnoclaim + "', ");
                sbRequest.Append("'currentThirdPartyPolicy':'' }, 'Invoicedocs': {'PanNo': 'CTEPK3025J','GSTNo': 'dsadasa3r33'},'VehicleIDV': '" + vehicleIDV + "',");
                sbRequest.Append("'AddOnCovers': [" + addons + "],");
                sbRequest.Append("'legalLiabilitys': [" + legallibilities + "],");
                sbRequest.Append("'AccessoriesCovers': [" + acccovers + "]}");

                string strReq = sbRequest.ToString();

                string respo = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "PrivateCarProposal"), strReq, userid, password);
                if (!string.IsNullOrEmpty(respo))
                {
                    result = JsonConvert.DeserializeObject<PrivateCarProposal>(respo);
                    var isSuccess = ConnectToDataBase.InsertAndUpdateMotoResponse(enquiryid, sbRequest.ToString(), respo, "perposal", "motor", "", "", "", result.applicationId);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion

        #region [Private Bike Section]
        public static BikeRoot GetBikeApiResponse(string userid, string password, MotorModel motorEnquiry)
        {
            BikeRoot result = new BikeRoot();

            try
            {
                if (!string.IsNullOrEmpty(motorEnquiry.policyexpirydate))
                {
                    motorEnquiry.policyexpirydate = CommonClass.ConvertDateYYMMDD(motorEnquiry.policyexpirydate);
                }

                string vehicleMaincode = Motor_Service.GetVehicleMainCode(motorEnquiry.BrandName, motorEnquiry.ModelName, motorEnquiry.Fuel, motorEnquiry.VarientName, true);
                //------------------------------------------------------------------
                //  Request =
                // {
                //  "enquiryId": "23353",
                //  "licensePlateNumber": "MH12AB1",
                //  "pincode": "110059",
                //  "policyHolderType": "INDIVIDUAL",
                //  "registrationDate": "2018-11-01",
                //  "manufactureDate":"2021-02-08",
                //  "vehicleMaincode": "1210210303",
                //  "Rtocode": "",
                //  "isPreviousInsurerKnown":true,
                //  "previousInsurerCode":"113",
                //  "previousPolicyNumber":"kjdg99u991",
                //  "previousPolicyExpiryDate":"2021-02-01",
                //  "isClaimInLastYear":false,
                //  "originalPreviousPolicyType":"INDIVIDUAL",
                //  "previousPolicyType":"INDIVIDUAL",
                //  "previousNoClaimBonus":"20",
                //  "currentThirdPartyPolicy":"",
                //  "PolicyType":"Renewal"
                // }
                //----------------------------------------------------------------------

                //MotorProposal proposal = Motor_Service.GetMotorProposal(motorEnquiry.Enquiry_Id);

                StringBuilder bikeRQ = new StringBuilder();
                bikeRQ.Append("{");
                bikeRQ.Append("'enquiryId': '" + motorEnquiry.Enquiry_Id + "',");
                bikeRQ.Append("'licensePlateNumber': '" + motorEnquiry.VechileRegNo + "',");
                bikeRQ.Append("'pincode': '" + motorEnquiry.PinCode + "',");
                bikeRQ.Append("'policyHolderType': '',");
                bikeRQ.Append("'registrationDate': '" + CommonClass.ConvertDateYYMMDD(motorEnquiry.DateOfReg) + "',");
                bikeRQ.Append("'manufactureDate': '" + CommonClass.ConvertDateYYMMDD(motorEnquiry.DateOfMfg) + "',");
                bikeRQ.Append("'vehicleMaincode': '" + vehicleMaincode + "',");
                bikeRQ.Append("'Rtocode': '" + motorEnquiry.RtoId + "',");
                bikeRQ.Append("'isPreviousInsurerKnown':" + (motorEnquiry.ispreviousinsurer.ToLower() == "yes" ? "true" : "false") + ",");
                //bikeRQ.Append("'previousInsurerCode':'113',"); //here                
                bikeRQ.Append("'previousInsurerCode':'',");
                bikeRQ.Append("'previousPolicyNumber':'" + motorEnquiry.policynumber + "',");
                bikeRQ.Append("'previousPolicyExpiryDate':'" + motorEnquiry.policyexpirydate + "',");
                bikeRQ.Append("'isClaimInLastYear':" + (motorEnquiry.isclaiminlastyear.ToLower() == "yes" ? "true" : "false") + ",");
                //bikeRQ.Append("'originalPreviousPolicyType':'INDIVIDUAL',");
                bikeRQ.Append("'originalPreviousPolicyType':'" + motorEnquiry.originalpreviouspolicytype + "',");
                //bikeRQ.Append("'previousPolicyType':'INDIVIDUAL',");  
                bikeRQ.Append("'previousPolicyType':'',");
                bikeRQ.Append("'previousNoClaimBonus':'" + motorEnquiry.previousyearnoclaim + "',");
                bikeRQ.Append("'currentThirdPartyPolicy': '',");
                bikeRQ.Append("'PolicyType':'" + motorEnquiry.insurancetype + "'");
                bikeRQ.Append("}");

                string bikeRequest = bikeRQ.ToString();


                string resp = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "TwoWheelerQuotes"), bikeRequest.ToString(), userid, password);
                if (!string.IsNullOrEmpty(resp))
                {
                    var isSuccess = ConnectToDataBase.InsertAndUpdateMotoResponse(motorEnquiry.Enquiry_Id, bikeRequest, resp, "search", "motor");

                    if (Convert.ToBoolean(isSuccess))
                    {
                        result = JsonConvert.DeserializeObject<BikeRoot>(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static PrivateBikeProposal GetMotorApiPrivateBikeProposalResponse(string userid, string password, string enquiryid, bool isBike)
        {
            PrivateBikeProposal result = new PrivateBikeProposal();

            try
            {
                MotorModel enq = Motor_Service.GetEnquiryDetails(enquiryid);
                MotorProposal proposal = Motor_Service.GetMotorProposal(enquiryid);
                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryid);
                BikeRoot quoteRespo = JsonConvert.DeserializeObject<BikeRoot>(enqRespo.response);
                List<AddUpdateSelectedAddon> addonList = Motor_Service.GetAddonList(enquiryid);

                string vehicleMaincode = Motor_Service.GetVehicleMainCode(enq.BrandName, enq.ModelName, enq.Fuel, enq.VarientName, isBike);

                TwoWheelerquoteR pBikeDetail = quoteRespo.TwoWheelerquoteRS.Where(p => p.Chainid == enqRespo.chainid && p.ProductDetails.grossPremium == enqRespo.suminsured && p.ProductDetails.insuranceProductCode == enqRespo.productcode).FirstOrDefault();

                if (!string.IsNullOrEmpty(enq.policyexpirydate))
                {
                    enq.policyexpirydate = CommonClass.ConvertDateYYMMDD(enq.policyexpirydate);
                }

                //{ 
                //'PolicyType':'Renewal',
                //'insuranceProductCode':'MOT-PLT-001',
                //'subInsuranceProductCode':'MOT-PRD-001',
                //'enquiryId': '202122672775', 'Chainid': '2',
                //'Rtocode': 'DL09',
                //'voluntaryDeductible': 'PCVE1',
                //'policyHolderType':'INDIVIDUAL', 
                //'coverTerm':1, 
                //'coverAvailability': '',
                //'netPremium': '9682.00',
                //'vehicleMaincode': '1111810116',
                //'licensePlateNumber': 'DL9CAA8994',
                //'vehicleIdentificationNumber': 'DF8ER798789',
                //'engineNumber': 'SD98F7S987', 
                //'registrationDate': '2018-08-23',
                //"manufactureDate": "2018-06-23",

                //'persons': { 'personType': 'INDIVIDUAL', 'firstName': 'krishna', 'middleName': '', 'lastName': 'kant', 'dateOfBirth': '1976-02-12', 'gender': 'MALE', 'EmailID': 'kk@gmail.com', 'Mobile': '9555266308', 'addressType': 'PRIMARY_RESIDENCE', 'flatNumber': 'testing', 'streetNumber': 'testing', 'street': 'landmark', 'district': '', 'state': 'Delhi', 'city': 'New Delhi', 'pincode': '110059' },

                //'nominee': { 'firstName': 'kks', 'middleName': '', 'lastName': 'kks', 'dateOfBirth': '1995-02-03','relation': 'Son', 'personType': 'INDIVIDUAL' },

                //'previouspolicydetail':{ 'isPreviousInsurerKnown':true, 'previousInsurerCode':'IFFCO TOKIO General Insurance Company Limited', 'previousPolicyNumber':'541545545', 'previousPolicyExpiryDate':'2021-02-26', 'isClaimInLastYear':false, 'originalPreviousPolicyType':'INDIVIDUAL', 'previousPolicyType':'INDIVIDUAL', 'previousNoClaimBonus':'20', 'currentThirdPartyPolicy':'' },

                // 'Invoicedocs': { 'PanNo': 'CTEPK3025J','GSTNo': 'dsadasa3r33'},
                //'VehicleIDV': '5000',
                //'AddOnCovers': [],
                //'legalLiabilitys': [],
                //'AccessoriesCovers': []
                //}

                string vehicleIDV = string.Empty;
                if (pBikeDetail != null && pBikeDetail.vehicle != null)
                {
                    if (pBikeDetail.vehicle.vehicleIDV != null)
                    {
                        vehicleIDV = pBikeDetail.vehicle.vehicleIDV.idv.ToString();
                    }
                }

                StringBuilder sbRequest = new StringBuilder();

                sbRequest.Append("{ ");
                sbRequest.Append("'PolicyType':'" + pBikeDetail.PolicyType + "',");
                sbRequest.Append("'insuranceProductCode':'" + pBikeDetail.ProductDetails.insuranceProductCode + "',");
                sbRequest.Append("'subInsuranceProductCode':'" + pBikeDetail.ProductDetails.subInsuranceProductCode + "',");
                sbRequest.Append("'enquiryId': '" + enquiryid + "',");
                sbRequest.Append("'Chainid': '" + enqRespo.chainid + "',");
                sbRequest.Append("'Rtocode': '" + enq.RtoId + "',");
                sbRequest.Append("'voluntaryDeductible': '" + proposal.valuntarydeductible + "',");
                sbRequest.Append("'policyHolderType':'" + proposal.policyholdertype + "', ");
                sbRequest.Append("'coverTerm':1, ");//here
                sbRequest.Append("'coverAvailability': '',");//here
                sbRequest.Append("'netPremium': '" + enqRespo.suminsured + "',");
                sbRequest.Append("'vehicleMaincode': '" + vehicleMaincode + "',");
                sbRequest.Append("'licensePlateNumber': '" + enq.VechileRegNo + "',");
                sbRequest.Append("'vehicleIdentificationNumber': '" + proposal.chasisno + "',");
                sbRequest.Append("'engineNumber': '" + proposal.engineno + "', ");
                sbRequest.Append("'registrationDate': '" + CommonClass.ConvertDateYYMMDD(enq.DateOfReg) + "',");
                sbRequest.Append("'manufactureDate': '" + CommonClass.ConvertDateYYMMDD(enq.DateOfMfg) + "',");


                sbRequest.Append("'persons': { 'personType': '" + proposal.policyholdertype + "', 'firstName': '" + proposal.firstname + "', 'middleName': '', 'lastName': '" + proposal.lastname + "', 'dateOfBirth': '" + CommonClass.ConvertDateYYMMDD(proposal.dob) + "', 'gender': '" + proposal.gender + "', 'EmailID': '" + proposal.emailid + "', 'Mobile': '" + proposal.mobileno + "', 'addressType': 'PRIMARY_RESIDENCE', 'flatNumber': '" + proposal.address1 + "', 'streetNumber': '" + proposal.address2 + "', 'street': '" + proposal.landmark + "', 'district': '','state': '" + proposal.statename + "', 'city': '" + proposal.cityname + "', 'pincode': '" + proposal.pincode + "' },");//addresstype,

                sbRequest.Append("'nominee': { 'firstName': '" + proposal.nfirstname + "', 'middleName': '', 'lastName': '" + proposal.nlastname + "', 'dateOfBirth': '" + CommonClass.ConvertDateYYMMDD(proposal.ndob) + "','relation': '" + proposal.nrelation + "', 'personType': '" + proposal.policyholdertype + "' },");

                sbRequest.Append("'previouspolicydetail':{ 'isPreviousInsurerKnown':" + (enq.ispreviousinsurer == "yes" ? "true" : "false") + ", 'previousInsurerCode':'" + proposal.previousinsured + "', 'previousPolicyNumber':'" + enq.policynumber + "','previousPolicyExpiryDate':'" + enq.policyexpirydate + "', 'isClaimInLastYear':" + (enq.isclaiminlastyear == "yes" ? "true" : "false") + ", 'originalPreviousPolicyType':'" + enq.originalpreviouspolicytype + "', 'previousPolicyType':'" + proposal.policyholdertype + "', 'previousNoClaimBonus':'" + enq.previousyearnoclaim + "', 'currentThirdPartyPolicy':'' },");

                sbRequest.Append("'Invoicedocs': { 'PanNo': 'CTEPK3025J','GSTNo': 'dsadasa3r33'},");
                sbRequest.Append("'VehicleIDV': '" + vehicleIDV + "',");

                string addons = string.Empty;
                string legallibilities = string.Empty;
                string acccovers = string.Empty;

                if (addonList != null && addonList.Count > 0)
                {
                    foreach (var item in addonList)
                    {

                        if (item.AddonName == "AddOnCovers")
                        {
                            addons = addons + "{'selection': true,'covername': '" + item.InnerName + "','netPremium': '" + item.InsuredAmt + "'},";
                        }
                        if (item.AddonName == "LegalLiabilitys")
                        {
                            legallibilities = legallibilities + "{'selection': true,'covername': '" + item.InnerName + "','netPremium': '" + item.InsuredAmt + "'},";
                        }
                        if (item.AddonName == "AccessoriesCovers")
                        {
                            acccovers = acccovers + "{'selection': true,'covername': '" + item.InnerName + "','maxallow': '" + item.MinAmt + "','minallow': '" + item.MaxAmt + "'},";
                        }

                    }
                }

                if (!string.IsNullOrEmpty(addons))
                {
                    addons = addons.Remove(addons.LastIndexOf(","), 1);
                }

                if (!string.IsNullOrEmpty(legallibilities))
                {
                    legallibilities = legallibilities.Remove(legallibilities.LastIndexOf(","), 1);
                }

                if (!string.IsNullOrEmpty(acccovers))
                {
                    acccovers = acccovers.Remove(acccovers.LastIndexOf(","), 1);
                }

                sbRequest.Append("'AddOnCovers': [" + addons + "],");
                sbRequest.Append("'legalLiabilitys': [" + legallibilities + "],");
                sbRequest.Append("'AccessoriesCovers': [" + acccovers + "]");
                sbRequest.Append("}");

                string bikeProposalReq = sbRequest.ToString();

                string respo = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "TwoWheelerProposal"), bikeProposalReq, userid, password);
                if (!string.IsNullOrEmpty(respo))
                {
                    result = JsonConvert.DeserializeObject<PrivateBikeProposal>(respo);
                    var isSuccess = ConnectToDataBase.InsertAndUpdateMotoResponse(enquiryid, sbRequest.ToString(), respo, "perposal", "motor", "", "", "", result.applicationId);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion

        public static MotorPayment GetMotorPaymentDetails(string enqId, string appid, string chainid, string userid, string password)
        {
            MotorPayment result = new MotorPayment();
            try
            {
                string paymentReq = "{'Chainid': '" + chainid + "','applicationId': '" + appid + "','enquiryId': '" + enqId + "','cancelReturnUrl': '" + Config.WebsiteUrl + "motor/paymentsuccess?enquiry=" + enqId + "','successReturnUrl': '" + Config.WebsiteUrl + "motor/paymentsuccess?enquiry=" + enqId + "'}";

                string respo = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "Payment"), paymentReq, userid, password);
                if (!string.IsNullOrEmpty(respo))
                {
                    if (ConnectToDataBase.UpdatePaymentDetailsEnquiryResponse(paymentReq, respo, enqId, "motor"))
                    {
                        result = JsonConvert.DeserializeObject<MotorPayment>(respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static string GetPerposalPdf(string userId, string password, string enquiryId)
        {
            string pdfLink = string.Empty;
            try
            {
                MotorGeneratePolicy gPolicy = new MotorGeneratePolicy();

                MotorEnquiryResponse enqRespo = Motor_Service.GetMotorEnquiryResponse(enquiryId);
                if (enqRespo.id > 0)
                {
                    string paymentReq = "{'policyId': '" + enqRespo.applicationid + "','Chainid': '" + enqRespo.chainid + "','enquiryId': '" + enqRespo.enquiry_id + "'}";
                    string respo = MotorApiUtility.PostMotorAPI("POST", (Config.MotorApiUrl + "generatePolicy"), paymentReq, userId, password);
                    if (!string.IsNullOrEmpty(respo))
                    {
                        gPolicy = JsonConvert.DeserializeObject<MotorGeneratePolicy>(respo);
                        if (!string.IsNullOrEmpty(gPolicy.schedulePath))
                        {
                            pdfLink = gPolicy.schedulePath;
                        }

                        bool isupdatePay = Motor_Service.UpdateCheckPaymentStatus(enquiryId, paymentReq, respo);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pdfLink;
        }
    }
}